﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// GameOneManager
struct GameOneManager_t2648179911;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameOneManager/<YSnowflakeSpawner>c__Iterator0
struct  U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958  : public Il2CppObject
{
public:
	// System.Single GameOneManager/<YSnowflakeSpawner>c__Iterator0::startTime
	float ___startTime_0;
	// System.Single GameOneManager/<YSnowflakeSpawner>c__Iterator0::<timer>__0
	float ___U3CtimerU3E__0_1;
	// UnityEngine.Transform GameOneManager/<YSnowflakeSpawner>c__Iterator0::holder
	Transform_t3275118058 * ___holder_2;
	// GameOneManager GameOneManager/<YSnowflakeSpawner>c__Iterator0::$this
	GameOneManager_t2648179911 * ___U24this_3;
	// System.Object GameOneManager/<YSnowflakeSpawner>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean GameOneManager/<YSnowflakeSpawner>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 GameOneManager/<YSnowflakeSpawner>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_startTime_0() { return static_cast<int32_t>(offsetof(U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958, ___startTime_0)); }
	inline float get_startTime_0() const { return ___startTime_0; }
	inline float* get_address_of_startTime_0() { return &___startTime_0; }
	inline void set_startTime_0(float value)
	{
		___startTime_0 = value;
	}

	inline static int32_t get_offset_of_U3CtimerU3E__0_1() { return static_cast<int32_t>(offsetof(U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958, ___U3CtimerU3E__0_1)); }
	inline float get_U3CtimerU3E__0_1() const { return ___U3CtimerU3E__0_1; }
	inline float* get_address_of_U3CtimerU3E__0_1() { return &___U3CtimerU3E__0_1; }
	inline void set_U3CtimerU3E__0_1(float value)
	{
		___U3CtimerU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_holder_2() { return static_cast<int32_t>(offsetof(U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958, ___holder_2)); }
	inline Transform_t3275118058 * get_holder_2() const { return ___holder_2; }
	inline Transform_t3275118058 ** get_address_of_holder_2() { return &___holder_2; }
	inline void set_holder_2(Transform_t3275118058 * value)
	{
		___holder_2 = value;
		Il2CppCodeGenWriteBarrier(&___holder_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958, ___U24this_3)); }
	inline GameOneManager_t2648179911 * get_U24this_3() const { return ___U24this_3; }
	inline GameOneManager_t2648179911 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(GameOneManager_t2648179911 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
