﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// MemoryGamePiece
struct MemoryGamePiece_t2964149615;
// MemoryGame
struct MemoryGame_t4018179393;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MemoryGame/<flipToBack>c__Iterator1
struct  U3CflipToBackU3Ec__Iterator1_t2557887515  : public Il2CppObject
{
public:
	// MemoryGamePiece MemoryGame/<flipToBack>c__Iterator1::clicked
	MemoryGamePiece_t2964149615 * ___clicked_0;
	// System.Boolean MemoryGame/<flipToBack>c__Iterator1::enableCanClickWhenDone
	bool ___enableCanClickWhenDone_1;
	// MemoryGame MemoryGame/<flipToBack>c__Iterator1::$this
	MemoryGame_t4018179393 * ___U24this_2;
	// System.Object MemoryGame/<flipToBack>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean MemoryGame/<flipToBack>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 MemoryGame/<flipToBack>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_clicked_0() { return static_cast<int32_t>(offsetof(U3CflipToBackU3Ec__Iterator1_t2557887515, ___clicked_0)); }
	inline MemoryGamePiece_t2964149615 * get_clicked_0() const { return ___clicked_0; }
	inline MemoryGamePiece_t2964149615 ** get_address_of_clicked_0() { return &___clicked_0; }
	inline void set_clicked_0(MemoryGamePiece_t2964149615 * value)
	{
		___clicked_0 = value;
		Il2CppCodeGenWriteBarrier(&___clicked_0, value);
	}

	inline static int32_t get_offset_of_enableCanClickWhenDone_1() { return static_cast<int32_t>(offsetof(U3CflipToBackU3Ec__Iterator1_t2557887515, ___enableCanClickWhenDone_1)); }
	inline bool get_enableCanClickWhenDone_1() const { return ___enableCanClickWhenDone_1; }
	inline bool* get_address_of_enableCanClickWhenDone_1() { return &___enableCanClickWhenDone_1; }
	inline void set_enableCanClickWhenDone_1(bool value)
	{
		___enableCanClickWhenDone_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CflipToBackU3Ec__Iterator1_t2557887515, ___U24this_2)); }
	inline MemoryGame_t4018179393 * get_U24this_2() const { return ___U24this_2; }
	inline MemoryGame_t4018179393 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MemoryGame_t4018179393 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CflipToBackU3Ec__Iterator1_t2557887515, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CflipToBackU3Ec__Iterator1_t2557887515, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CflipToBackU3Ec__Iterator1_t2557887515, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
