﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_DDManagerBase_1_gen1073359448.h"

// SoundClip
struct SoundClip_t2598932235;
// SoundClip[]
struct SoundClipU5BU5D_t2486522986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t654432262  : public DDManagerBase_1_t1073359448
{
public:
	// SoundClip SoundManager::tempSound
	SoundClip_t2598932235 * ___tempSound_4;
	// SoundClip SoundManager::buttonAppear
	SoundClip_t2598932235 * ___buttonAppear_5;
	// SoundClip SoundManager::candyAppear
	SoundClip_t2598932235 * ___candyAppear_6;
	// SoundClip SoundManager::candyDestroy
	SoundClip_t2598932235 * ___candyDestroy_7;
	// SoundClip SoundManager::correctChoice
	SoundClip_t2598932235 * ___correctChoice_8;
	// SoundClip SoundManager::incorrectChoice
	SoundClip_t2598932235 * ___incorrectChoice_9;
	// SoundClip SoundManager::centerLetterChange
	SoundClip_t2598932235 * ___centerLetterChange_10;
	// SoundClip SoundManager::digSound
	SoundClip_t2598932235 * ___digSound_11;
	// SoundClip SoundManager::itemFound
	SoundClip_t2598932235 * ___itemFound_12;
	// SoundClip SoundManager::hatRise
	SoundClip_t2598932235 * ___hatRise_13;
	// SoundClip SoundManager::ringThrow
	SoundClip_t2598932235 * ___ringThrow_14;
	// SoundClip SoundManager::correctThrow
	SoundClip_t2598932235 * ___correctThrow_15;
	// SoundClip SoundManager::drawingLoop
	SoundClip_t2598932235 * ___drawingLoop_16;
	// SoundClip SoundManager::correctDraw
	SoundClip_t2598932235 * ___correctDraw_17;
	// SoundClip SoundManager::drawDelete
	SoundClip_t2598932235 * ___drawDelete_18;
	// SoundClip SoundManager::cleaningSoundLoop
	SoundClip_t2598932235 * ___cleaningSoundLoop_19;
	// SoundClip SoundManager::dirtCleaned
	SoundClip_t2598932235 * ___dirtCleaned_20;
	// SoundClip SoundManager::goodCollect
	SoundClip_t2598932235 * ___goodCollect_21;
	// SoundClip SoundManager::badCollect
	SoundClip_t2598932235 * ___badCollect_22;
	// SoundClip SoundManager::snowmanRise
	SoundClip_t2598932235 * ___snowmanRise_23;
	// SoundClip SoundManager::goodSnowballThrow
	SoundClip_t2598932235 * ___goodSnowballThrow_24;
	// SoundClip SoundManager::badSnowballThrow
	SoundClip_t2598932235 * ___badSnowballThrow_25;
	// SoundClip SoundManager::snowballThrow
	SoundClip_t2598932235 * ___snowballThrow_26;
	// SoundClip SoundManager::snowballSplash
	SoundClip_t2598932235 * ___snowballSplash_27;
	// SoundClip SoundManager::levelStartShake
	SoundClip_t2598932235 * ___levelStartShake_28;
	// SoundClip SoundManager::correctPlacement
	SoundClip_t2598932235 * ___correctPlacement_29;
	// SoundClip SoundManager::incorrectPlacement
	SoundClip_t2598932235 * ___incorrectPlacement_30;
	// SoundClip SoundManager::levelFinished
	SoundClip_t2598932235 * ___levelFinished_31;
	// SoundClip SoundManager::featureChange
	SoundClip_t2598932235 * ___featureChange_32;
	// SoundClip SoundManager::bubbleTap
	SoundClip_t2598932235 * ___bubbleTap_33;
	// SoundClip SoundManager::lineDrawingLoop
	SoundClip_t2598932235 * ___lineDrawingLoop_34;
	// SoundClip SoundManager::correctLine
	SoundClip_t2598932235 * ___correctLine_35;
	// SoundClip SoundManager::incorrectLine
	SoundClip_t2598932235 * ___incorrectLine_36;
	// SoundClip SoundManager::waterToSnowChange
	SoundClip_t2598932235 * ___waterToSnowChange_37;
	// SoundClip SoundManager::clockTicking
	SoundClip_t2598932235 * ___clockTicking_38;
	// SoundClip SoundManager::clockTickingFast
	SoundClip_t2598932235 * ___clockTickingFast_39;
	// SoundClip SoundManager::correctFound
	SoundClip_t2598932235 * ___correctFound_40;
	// SoundClip SoundManager::incorrectTap
	SoundClip_t2598932235 * ___incorrectTap_41;
	// SoundClip SoundManager::snowmanAppearingSound
	SoundClip_t2598932235 * ___snowmanAppearingSound_42;
	// SoundClip SoundManager::correctSnowman
	SoundClip_t2598932235 * ___correctSnowman_43;
	// SoundClip SoundManager::flipSound
	SoundClip_t2598932235 * ___flipSound_44;
	// SoundClip SoundManager::badMatch
	SoundClip_t2598932235 * ___badMatch_45;
	// SoundClip SoundManager::goodMatch
	SoundClip_t2598932235 * ___goodMatch_46;
	// SoundClip SoundManager::gridLevelEnd
	SoundClip_t2598932235 * ___gridLevelEnd_47;
	// SoundClip SoundManager::bulbTapSound
	SoundClip_t2598932235 * ___bulbTapSound_48;
	// SoundClip SoundManager::lineWin
	SoundClip_t2598932235 * ___lineWin_49;
	// SoundClip SoundManager::roundWin
	SoundClip_t2598932235 * ___roundWin_50;
	// SoundClip SoundManager::moveSound
	SoundClip_t2598932235 * ___moveSound_51;
	// SoundClip SoundManager::fallSound
	SoundClip_t2598932235 * ___fallSound_52;
	// SoundClip SoundManager::rotateSound
	SoundClip_t2598932235 * ___rotateSound_53;
	// SoundClip SoundManager::lineSound
	SoundClip_t2598932235 * ___lineSound_54;
	// SoundClip SoundManager::foundSound
	SoundClip_t2598932235 * ___foundSound_55;
	// SoundClip[] SoundManager::sounds
	SoundClipU5BU5D_t2486522986* ___sounds_56;
	// SoundClip SoundManager::badSound
	SoundClip_t2598932235 * ___badSound_57;
	// SoundClip SoundManager::correctSound
	SoundClip_t2598932235 * ___correctSound_58;
	// SoundClip[] SoundManager::noteSounds
	SoundClipU5BU5D_t2486522986* ___noteSounds_59;
	// SoundClip SoundManager::endHappyElf
	SoundClip_t2598932235 * ___endHappyElf_60;
	// SoundClip SoundManager::correctDrawn
	SoundClip_t2598932235 * ___correctDrawn_61;
	// SoundClip SoundManager::incorrectDrawn
	SoundClip_t2598932235 * ___incorrectDrawn_62;
	// SoundClip SoundManager::wrappingSoundLoop
	SoundClip_t2598932235 * ___wrappingSoundLoop_63;
	// SoundClip SoundManager::correctResult
	SoundClip_t2598932235 * ___correctResult_64;
	// SoundClip SoundManager::incorrectResult
	SoundClip_t2598932235 * ___incorrectResult_65;
	// SoundClip SoundManager::tapSound
	SoundClip_t2598932235 * ___tapSound_66;
	// SoundClip SoundManager::giftSpawnSound
	SoundClip_t2598932235 * ___giftSpawnSound_67;
	// SoundClip SoundManager::hitSound
	SoundClip_t2598932235 * ___hitSound_68;

public:
	inline static int32_t get_offset_of_tempSound_4() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___tempSound_4)); }
	inline SoundClip_t2598932235 * get_tempSound_4() const { return ___tempSound_4; }
	inline SoundClip_t2598932235 ** get_address_of_tempSound_4() { return &___tempSound_4; }
	inline void set_tempSound_4(SoundClip_t2598932235 * value)
	{
		___tempSound_4 = value;
		Il2CppCodeGenWriteBarrier(&___tempSound_4, value);
	}

	inline static int32_t get_offset_of_buttonAppear_5() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___buttonAppear_5)); }
	inline SoundClip_t2598932235 * get_buttonAppear_5() const { return ___buttonAppear_5; }
	inline SoundClip_t2598932235 ** get_address_of_buttonAppear_5() { return &___buttonAppear_5; }
	inline void set_buttonAppear_5(SoundClip_t2598932235 * value)
	{
		___buttonAppear_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonAppear_5, value);
	}

	inline static int32_t get_offset_of_candyAppear_6() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___candyAppear_6)); }
	inline SoundClip_t2598932235 * get_candyAppear_6() const { return ___candyAppear_6; }
	inline SoundClip_t2598932235 ** get_address_of_candyAppear_6() { return &___candyAppear_6; }
	inline void set_candyAppear_6(SoundClip_t2598932235 * value)
	{
		___candyAppear_6 = value;
		Il2CppCodeGenWriteBarrier(&___candyAppear_6, value);
	}

	inline static int32_t get_offset_of_candyDestroy_7() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___candyDestroy_7)); }
	inline SoundClip_t2598932235 * get_candyDestroy_7() const { return ___candyDestroy_7; }
	inline SoundClip_t2598932235 ** get_address_of_candyDestroy_7() { return &___candyDestroy_7; }
	inline void set_candyDestroy_7(SoundClip_t2598932235 * value)
	{
		___candyDestroy_7 = value;
		Il2CppCodeGenWriteBarrier(&___candyDestroy_7, value);
	}

	inline static int32_t get_offset_of_correctChoice_8() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___correctChoice_8)); }
	inline SoundClip_t2598932235 * get_correctChoice_8() const { return ___correctChoice_8; }
	inline SoundClip_t2598932235 ** get_address_of_correctChoice_8() { return &___correctChoice_8; }
	inline void set_correctChoice_8(SoundClip_t2598932235 * value)
	{
		___correctChoice_8 = value;
		Il2CppCodeGenWriteBarrier(&___correctChoice_8, value);
	}

	inline static int32_t get_offset_of_incorrectChoice_9() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___incorrectChoice_9)); }
	inline SoundClip_t2598932235 * get_incorrectChoice_9() const { return ___incorrectChoice_9; }
	inline SoundClip_t2598932235 ** get_address_of_incorrectChoice_9() { return &___incorrectChoice_9; }
	inline void set_incorrectChoice_9(SoundClip_t2598932235 * value)
	{
		___incorrectChoice_9 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectChoice_9, value);
	}

	inline static int32_t get_offset_of_centerLetterChange_10() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___centerLetterChange_10)); }
	inline SoundClip_t2598932235 * get_centerLetterChange_10() const { return ___centerLetterChange_10; }
	inline SoundClip_t2598932235 ** get_address_of_centerLetterChange_10() { return &___centerLetterChange_10; }
	inline void set_centerLetterChange_10(SoundClip_t2598932235 * value)
	{
		___centerLetterChange_10 = value;
		Il2CppCodeGenWriteBarrier(&___centerLetterChange_10, value);
	}

	inline static int32_t get_offset_of_digSound_11() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___digSound_11)); }
	inline SoundClip_t2598932235 * get_digSound_11() const { return ___digSound_11; }
	inline SoundClip_t2598932235 ** get_address_of_digSound_11() { return &___digSound_11; }
	inline void set_digSound_11(SoundClip_t2598932235 * value)
	{
		___digSound_11 = value;
		Il2CppCodeGenWriteBarrier(&___digSound_11, value);
	}

	inline static int32_t get_offset_of_itemFound_12() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___itemFound_12)); }
	inline SoundClip_t2598932235 * get_itemFound_12() const { return ___itemFound_12; }
	inline SoundClip_t2598932235 ** get_address_of_itemFound_12() { return &___itemFound_12; }
	inline void set_itemFound_12(SoundClip_t2598932235 * value)
	{
		___itemFound_12 = value;
		Il2CppCodeGenWriteBarrier(&___itemFound_12, value);
	}

	inline static int32_t get_offset_of_hatRise_13() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___hatRise_13)); }
	inline SoundClip_t2598932235 * get_hatRise_13() const { return ___hatRise_13; }
	inline SoundClip_t2598932235 ** get_address_of_hatRise_13() { return &___hatRise_13; }
	inline void set_hatRise_13(SoundClip_t2598932235 * value)
	{
		___hatRise_13 = value;
		Il2CppCodeGenWriteBarrier(&___hatRise_13, value);
	}

	inline static int32_t get_offset_of_ringThrow_14() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___ringThrow_14)); }
	inline SoundClip_t2598932235 * get_ringThrow_14() const { return ___ringThrow_14; }
	inline SoundClip_t2598932235 ** get_address_of_ringThrow_14() { return &___ringThrow_14; }
	inline void set_ringThrow_14(SoundClip_t2598932235 * value)
	{
		___ringThrow_14 = value;
		Il2CppCodeGenWriteBarrier(&___ringThrow_14, value);
	}

	inline static int32_t get_offset_of_correctThrow_15() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___correctThrow_15)); }
	inline SoundClip_t2598932235 * get_correctThrow_15() const { return ___correctThrow_15; }
	inline SoundClip_t2598932235 ** get_address_of_correctThrow_15() { return &___correctThrow_15; }
	inline void set_correctThrow_15(SoundClip_t2598932235 * value)
	{
		___correctThrow_15 = value;
		Il2CppCodeGenWriteBarrier(&___correctThrow_15, value);
	}

	inline static int32_t get_offset_of_drawingLoop_16() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___drawingLoop_16)); }
	inline SoundClip_t2598932235 * get_drawingLoop_16() const { return ___drawingLoop_16; }
	inline SoundClip_t2598932235 ** get_address_of_drawingLoop_16() { return &___drawingLoop_16; }
	inline void set_drawingLoop_16(SoundClip_t2598932235 * value)
	{
		___drawingLoop_16 = value;
		Il2CppCodeGenWriteBarrier(&___drawingLoop_16, value);
	}

	inline static int32_t get_offset_of_correctDraw_17() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___correctDraw_17)); }
	inline SoundClip_t2598932235 * get_correctDraw_17() const { return ___correctDraw_17; }
	inline SoundClip_t2598932235 ** get_address_of_correctDraw_17() { return &___correctDraw_17; }
	inline void set_correctDraw_17(SoundClip_t2598932235 * value)
	{
		___correctDraw_17 = value;
		Il2CppCodeGenWriteBarrier(&___correctDraw_17, value);
	}

	inline static int32_t get_offset_of_drawDelete_18() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___drawDelete_18)); }
	inline SoundClip_t2598932235 * get_drawDelete_18() const { return ___drawDelete_18; }
	inline SoundClip_t2598932235 ** get_address_of_drawDelete_18() { return &___drawDelete_18; }
	inline void set_drawDelete_18(SoundClip_t2598932235 * value)
	{
		___drawDelete_18 = value;
		Il2CppCodeGenWriteBarrier(&___drawDelete_18, value);
	}

	inline static int32_t get_offset_of_cleaningSoundLoop_19() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___cleaningSoundLoop_19)); }
	inline SoundClip_t2598932235 * get_cleaningSoundLoop_19() const { return ___cleaningSoundLoop_19; }
	inline SoundClip_t2598932235 ** get_address_of_cleaningSoundLoop_19() { return &___cleaningSoundLoop_19; }
	inline void set_cleaningSoundLoop_19(SoundClip_t2598932235 * value)
	{
		___cleaningSoundLoop_19 = value;
		Il2CppCodeGenWriteBarrier(&___cleaningSoundLoop_19, value);
	}

	inline static int32_t get_offset_of_dirtCleaned_20() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___dirtCleaned_20)); }
	inline SoundClip_t2598932235 * get_dirtCleaned_20() const { return ___dirtCleaned_20; }
	inline SoundClip_t2598932235 ** get_address_of_dirtCleaned_20() { return &___dirtCleaned_20; }
	inline void set_dirtCleaned_20(SoundClip_t2598932235 * value)
	{
		___dirtCleaned_20 = value;
		Il2CppCodeGenWriteBarrier(&___dirtCleaned_20, value);
	}

	inline static int32_t get_offset_of_goodCollect_21() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___goodCollect_21)); }
	inline SoundClip_t2598932235 * get_goodCollect_21() const { return ___goodCollect_21; }
	inline SoundClip_t2598932235 ** get_address_of_goodCollect_21() { return &___goodCollect_21; }
	inline void set_goodCollect_21(SoundClip_t2598932235 * value)
	{
		___goodCollect_21 = value;
		Il2CppCodeGenWriteBarrier(&___goodCollect_21, value);
	}

	inline static int32_t get_offset_of_badCollect_22() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___badCollect_22)); }
	inline SoundClip_t2598932235 * get_badCollect_22() const { return ___badCollect_22; }
	inline SoundClip_t2598932235 ** get_address_of_badCollect_22() { return &___badCollect_22; }
	inline void set_badCollect_22(SoundClip_t2598932235 * value)
	{
		___badCollect_22 = value;
		Il2CppCodeGenWriteBarrier(&___badCollect_22, value);
	}

	inline static int32_t get_offset_of_snowmanRise_23() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___snowmanRise_23)); }
	inline SoundClip_t2598932235 * get_snowmanRise_23() const { return ___snowmanRise_23; }
	inline SoundClip_t2598932235 ** get_address_of_snowmanRise_23() { return &___snowmanRise_23; }
	inline void set_snowmanRise_23(SoundClip_t2598932235 * value)
	{
		___snowmanRise_23 = value;
		Il2CppCodeGenWriteBarrier(&___snowmanRise_23, value);
	}

	inline static int32_t get_offset_of_goodSnowballThrow_24() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___goodSnowballThrow_24)); }
	inline SoundClip_t2598932235 * get_goodSnowballThrow_24() const { return ___goodSnowballThrow_24; }
	inline SoundClip_t2598932235 ** get_address_of_goodSnowballThrow_24() { return &___goodSnowballThrow_24; }
	inline void set_goodSnowballThrow_24(SoundClip_t2598932235 * value)
	{
		___goodSnowballThrow_24 = value;
		Il2CppCodeGenWriteBarrier(&___goodSnowballThrow_24, value);
	}

	inline static int32_t get_offset_of_badSnowballThrow_25() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___badSnowballThrow_25)); }
	inline SoundClip_t2598932235 * get_badSnowballThrow_25() const { return ___badSnowballThrow_25; }
	inline SoundClip_t2598932235 ** get_address_of_badSnowballThrow_25() { return &___badSnowballThrow_25; }
	inline void set_badSnowballThrow_25(SoundClip_t2598932235 * value)
	{
		___badSnowballThrow_25 = value;
		Il2CppCodeGenWriteBarrier(&___badSnowballThrow_25, value);
	}

	inline static int32_t get_offset_of_snowballThrow_26() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___snowballThrow_26)); }
	inline SoundClip_t2598932235 * get_snowballThrow_26() const { return ___snowballThrow_26; }
	inline SoundClip_t2598932235 ** get_address_of_snowballThrow_26() { return &___snowballThrow_26; }
	inline void set_snowballThrow_26(SoundClip_t2598932235 * value)
	{
		___snowballThrow_26 = value;
		Il2CppCodeGenWriteBarrier(&___snowballThrow_26, value);
	}

	inline static int32_t get_offset_of_snowballSplash_27() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___snowballSplash_27)); }
	inline SoundClip_t2598932235 * get_snowballSplash_27() const { return ___snowballSplash_27; }
	inline SoundClip_t2598932235 ** get_address_of_snowballSplash_27() { return &___snowballSplash_27; }
	inline void set_snowballSplash_27(SoundClip_t2598932235 * value)
	{
		___snowballSplash_27 = value;
		Il2CppCodeGenWriteBarrier(&___snowballSplash_27, value);
	}

	inline static int32_t get_offset_of_levelStartShake_28() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___levelStartShake_28)); }
	inline SoundClip_t2598932235 * get_levelStartShake_28() const { return ___levelStartShake_28; }
	inline SoundClip_t2598932235 ** get_address_of_levelStartShake_28() { return &___levelStartShake_28; }
	inline void set_levelStartShake_28(SoundClip_t2598932235 * value)
	{
		___levelStartShake_28 = value;
		Il2CppCodeGenWriteBarrier(&___levelStartShake_28, value);
	}

	inline static int32_t get_offset_of_correctPlacement_29() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___correctPlacement_29)); }
	inline SoundClip_t2598932235 * get_correctPlacement_29() const { return ___correctPlacement_29; }
	inline SoundClip_t2598932235 ** get_address_of_correctPlacement_29() { return &___correctPlacement_29; }
	inline void set_correctPlacement_29(SoundClip_t2598932235 * value)
	{
		___correctPlacement_29 = value;
		Il2CppCodeGenWriteBarrier(&___correctPlacement_29, value);
	}

	inline static int32_t get_offset_of_incorrectPlacement_30() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___incorrectPlacement_30)); }
	inline SoundClip_t2598932235 * get_incorrectPlacement_30() const { return ___incorrectPlacement_30; }
	inline SoundClip_t2598932235 ** get_address_of_incorrectPlacement_30() { return &___incorrectPlacement_30; }
	inline void set_incorrectPlacement_30(SoundClip_t2598932235 * value)
	{
		___incorrectPlacement_30 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectPlacement_30, value);
	}

	inline static int32_t get_offset_of_levelFinished_31() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___levelFinished_31)); }
	inline SoundClip_t2598932235 * get_levelFinished_31() const { return ___levelFinished_31; }
	inline SoundClip_t2598932235 ** get_address_of_levelFinished_31() { return &___levelFinished_31; }
	inline void set_levelFinished_31(SoundClip_t2598932235 * value)
	{
		___levelFinished_31 = value;
		Il2CppCodeGenWriteBarrier(&___levelFinished_31, value);
	}

	inline static int32_t get_offset_of_featureChange_32() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___featureChange_32)); }
	inline SoundClip_t2598932235 * get_featureChange_32() const { return ___featureChange_32; }
	inline SoundClip_t2598932235 ** get_address_of_featureChange_32() { return &___featureChange_32; }
	inline void set_featureChange_32(SoundClip_t2598932235 * value)
	{
		___featureChange_32 = value;
		Il2CppCodeGenWriteBarrier(&___featureChange_32, value);
	}

	inline static int32_t get_offset_of_bubbleTap_33() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___bubbleTap_33)); }
	inline SoundClip_t2598932235 * get_bubbleTap_33() const { return ___bubbleTap_33; }
	inline SoundClip_t2598932235 ** get_address_of_bubbleTap_33() { return &___bubbleTap_33; }
	inline void set_bubbleTap_33(SoundClip_t2598932235 * value)
	{
		___bubbleTap_33 = value;
		Il2CppCodeGenWriteBarrier(&___bubbleTap_33, value);
	}

	inline static int32_t get_offset_of_lineDrawingLoop_34() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___lineDrawingLoop_34)); }
	inline SoundClip_t2598932235 * get_lineDrawingLoop_34() const { return ___lineDrawingLoop_34; }
	inline SoundClip_t2598932235 ** get_address_of_lineDrawingLoop_34() { return &___lineDrawingLoop_34; }
	inline void set_lineDrawingLoop_34(SoundClip_t2598932235 * value)
	{
		___lineDrawingLoop_34 = value;
		Il2CppCodeGenWriteBarrier(&___lineDrawingLoop_34, value);
	}

	inline static int32_t get_offset_of_correctLine_35() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___correctLine_35)); }
	inline SoundClip_t2598932235 * get_correctLine_35() const { return ___correctLine_35; }
	inline SoundClip_t2598932235 ** get_address_of_correctLine_35() { return &___correctLine_35; }
	inline void set_correctLine_35(SoundClip_t2598932235 * value)
	{
		___correctLine_35 = value;
		Il2CppCodeGenWriteBarrier(&___correctLine_35, value);
	}

	inline static int32_t get_offset_of_incorrectLine_36() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___incorrectLine_36)); }
	inline SoundClip_t2598932235 * get_incorrectLine_36() const { return ___incorrectLine_36; }
	inline SoundClip_t2598932235 ** get_address_of_incorrectLine_36() { return &___incorrectLine_36; }
	inline void set_incorrectLine_36(SoundClip_t2598932235 * value)
	{
		___incorrectLine_36 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectLine_36, value);
	}

	inline static int32_t get_offset_of_waterToSnowChange_37() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___waterToSnowChange_37)); }
	inline SoundClip_t2598932235 * get_waterToSnowChange_37() const { return ___waterToSnowChange_37; }
	inline SoundClip_t2598932235 ** get_address_of_waterToSnowChange_37() { return &___waterToSnowChange_37; }
	inline void set_waterToSnowChange_37(SoundClip_t2598932235 * value)
	{
		___waterToSnowChange_37 = value;
		Il2CppCodeGenWriteBarrier(&___waterToSnowChange_37, value);
	}

	inline static int32_t get_offset_of_clockTicking_38() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___clockTicking_38)); }
	inline SoundClip_t2598932235 * get_clockTicking_38() const { return ___clockTicking_38; }
	inline SoundClip_t2598932235 ** get_address_of_clockTicking_38() { return &___clockTicking_38; }
	inline void set_clockTicking_38(SoundClip_t2598932235 * value)
	{
		___clockTicking_38 = value;
		Il2CppCodeGenWriteBarrier(&___clockTicking_38, value);
	}

	inline static int32_t get_offset_of_clockTickingFast_39() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___clockTickingFast_39)); }
	inline SoundClip_t2598932235 * get_clockTickingFast_39() const { return ___clockTickingFast_39; }
	inline SoundClip_t2598932235 ** get_address_of_clockTickingFast_39() { return &___clockTickingFast_39; }
	inline void set_clockTickingFast_39(SoundClip_t2598932235 * value)
	{
		___clockTickingFast_39 = value;
		Il2CppCodeGenWriteBarrier(&___clockTickingFast_39, value);
	}

	inline static int32_t get_offset_of_correctFound_40() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___correctFound_40)); }
	inline SoundClip_t2598932235 * get_correctFound_40() const { return ___correctFound_40; }
	inline SoundClip_t2598932235 ** get_address_of_correctFound_40() { return &___correctFound_40; }
	inline void set_correctFound_40(SoundClip_t2598932235 * value)
	{
		___correctFound_40 = value;
		Il2CppCodeGenWriteBarrier(&___correctFound_40, value);
	}

	inline static int32_t get_offset_of_incorrectTap_41() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___incorrectTap_41)); }
	inline SoundClip_t2598932235 * get_incorrectTap_41() const { return ___incorrectTap_41; }
	inline SoundClip_t2598932235 ** get_address_of_incorrectTap_41() { return &___incorrectTap_41; }
	inline void set_incorrectTap_41(SoundClip_t2598932235 * value)
	{
		___incorrectTap_41 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectTap_41, value);
	}

	inline static int32_t get_offset_of_snowmanAppearingSound_42() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___snowmanAppearingSound_42)); }
	inline SoundClip_t2598932235 * get_snowmanAppearingSound_42() const { return ___snowmanAppearingSound_42; }
	inline SoundClip_t2598932235 ** get_address_of_snowmanAppearingSound_42() { return &___snowmanAppearingSound_42; }
	inline void set_snowmanAppearingSound_42(SoundClip_t2598932235 * value)
	{
		___snowmanAppearingSound_42 = value;
		Il2CppCodeGenWriteBarrier(&___snowmanAppearingSound_42, value);
	}

	inline static int32_t get_offset_of_correctSnowman_43() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___correctSnowman_43)); }
	inline SoundClip_t2598932235 * get_correctSnowman_43() const { return ___correctSnowman_43; }
	inline SoundClip_t2598932235 ** get_address_of_correctSnowman_43() { return &___correctSnowman_43; }
	inline void set_correctSnowman_43(SoundClip_t2598932235 * value)
	{
		___correctSnowman_43 = value;
		Il2CppCodeGenWriteBarrier(&___correctSnowman_43, value);
	}

	inline static int32_t get_offset_of_flipSound_44() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___flipSound_44)); }
	inline SoundClip_t2598932235 * get_flipSound_44() const { return ___flipSound_44; }
	inline SoundClip_t2598932235 ** get_address_of_flipSound_44() { return &___flipSound_44; }
	inline void set_flipSound_44(SoundClip_t2598932235 * value)
	{
		___flipSound_44 = value;
		Il2CppCodeGenWriteBarrier(&___flipSound_44, value);
	}

	inline static int32_t get_offset_of_badMatch_45() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___badMatch_45)); }
	inline SoundClip_t2598932235 * get_badMatch_45() const { return ___badMatch_45; }
	inline SoundClip_t2598932235 ** get_address_of_badMatch_45() { return &___badMatch_45; }
	inline void set_badMatch_45(SoundClip_t2598932235 * value)
	{
		___badMatch_45 = value;
		Il2CppCodeGenWriteBarrier(&___badMatch_45, value);
	}

	inline static int32_t get_offset_of_goodMatch_46() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___goodMatch_46)); }
	inline SoundClip_t2598932235 * get_goodMatch_46() const { return ___goodMatch_46; }
	inline SoundClip_t2598932235 ** get_address_of_goodMatch_46() { return &___goodMatch_46; }
	inline void set_goodMatch_46(SoundClip_t2598932235 * value)
	{
		___goodMatch_46 = value;
		Il2CppCodeGenWriteBarrier(&___goodMatch_46, value);
	}

	inline static int32_t get_offset_of_gridLevelEnd_47() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___gridLevelEnd_47)); }
	inline SoundClip_t2598932235 * get_gridLevelEnd_47() const { return ___gridLevelEnd_47; }
	inline SoundClip_t2598932235 ** get_address_of_gridLevelEnd_47() { return &___gridLevelEnd_47; }
	inline void set_gridLevelEnd_47(SoundClip_t2598932235 * value)
	{
		___gridLevelEnd_47 = value;
		Il2CppCodeGenWriteBarrier(&___gridLevelEnd_47, value);
	}

	inline static int32_t get_offset_of_bulbTapSound_48() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___bulbTapSound_48)); }
	inline SoundClip_t2598932235 * get_bulbTapSound_48() const { return ___bulbTapSound_48; }
	inline SoundClip_t2598932235 ** get_address_of_bulbTapSound_48() { return &___bulbTapSound_48; }
	inline void set_bulbTapSound_48(SoundClip_t2598932235 * value)
	{
		___bulbTapSound_48 = value;
		Il2CppCodeGenWriteBarrier(&___bulbTapSound_48, value);
	}

	inline static int32_t get_offset_of_lineWin_49() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___lineWin_49)); }
	inline SoundClip_t2598932235 * get_lineWin_49() const { return ___lineWin_49; }
	inline SoundClip_t2598932235 ** get_address_of_lineWin_49() { return &___lineWin_49; }
	inline void set_lineWin_49(SoundClip_t2598932235 * value)
	{
		___lineWin_49 = value;
		Il2CppCodeGenWriteBarrier(&___lineWin_49, value);
	}

	inline static int32_t get_offset_of_roundWin_50() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___roundWin_50)); }
	inline SoundClip_t2598932235 * get_roundWin_50() const { return ___roundWin_50; }
	inline SoundClip_t2598932235 ** get_address_of_roundWin_50() { return &___roundWin_50; }
	inline void set_roundWin_50(SoundClip_t2598932235 * value)
	{
		___roundWin_50 = value;
		Il2CppCodeGenWriteBarrier(&___roundWin_50, value);
	}

	inline static int32_t get_offset_of_moveSound_51() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___moveSound_51)); }
	inline SoundClip_t2598932235 * get_moveSound_51() const { return ___moveSound_51; }
	inline SoundClip_t2598932235 ** get_address_of_moveSound_51() { return &___moveSound_51; }
	inline void set_moveSound_51(SoundClip_t2598932235 * value)
	{
		___moveSound_51 = value;
		Il2CppCodeGenWriteBarrier(&___moveSound_51, value);
	}

	inline static int32_t get_offset_of_fallSound_52() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___fallSound_52)); }
	inline SoundClip_t2598932235 * get_fallSound_52() const { return ___fallSound_52; }
	inline SoundClip_t2598932235 ** get_address_of_fallSound_52() { return &___fallSound_52; }
	inline void set_fallSound_52(SoundClip_t2598932235 * value)
	{
		___fallSound_52 = value;
		Il2CppCodeGenWriteBarrier(&___fallSound_52, value);
	}

	inline static int32_t get_offset_of_rotateSound_53() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___rotateSound_53)); }
	inline SoundClip_t2598932235 * get_rotateSound_53() const { return ___rotateSound_53; }
	inline SoundClip_t2598932235 ** get_address_of_rotateSound_53() { return &___rotateSound_53; }
	inline void set_rotateSound_53(SoundClip_t2598932235 * value)
	{
		___rotateSound_53 = value;
		Il2CppCodeGenWriteBarrier(&___rotateSound_53, value);
	}

	inline static int32_t get_offset_of_lineSound_54() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___lineSound_54)); }
	inline SoundClip_t2598932235 * get_lineSound_54() const { return ___lineSound_54; }
	inline SoundClip_t2598932235 ** get_address_of_lineSound_54() { return &___lineSound_54; }
	inline void set_lineSound_54(SoundClip_t2598932235 * value)
	{
		___lineSound_54 = value;
		Il2CppCodeGenWriteBarrier(&___lineSound_54, value);
	}

	inline static int32_t get_offset_of_foundSound_55() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___foundSound_55)); }
	inline SoundClip_t2598932235 * get_foundSound_55() const { return ___foundSound_55; }
	inline SoundClip_t2598932235 ** get_address_of_foundSound_55() { return &___foundSound_55; }
	inline void set_foundSound_55(SoundClip_t2598932235 * value)
	{
		___foundSound_55 = value;
		Il2CppCodeGenWriteBarrier(&___foundSound_55, value);
	}

	inline static int32_t get_offset_of_sounds_56() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___sounds_56)); }
	inline SoundClipU5BU5D_t2486522986* get_sounds_56() const { return ___sounds_56; }
	inline SoundClipU5BU5D_t2486522986** get_address_of_sounds_56() { return &___sounds_56; }
	inline void set_sounds_56(SoundClipU5BU5D_t2486522986* value)
	{
		___sounds_56 = value;
		Il2CppCodeGenWriteBarrier(&___sounds_56, value);
	}

	inline static int32_t get_offset_of_badSound_57() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___badSound_57)); }
	inline SoundClip_t2598932235 * get_badSound_57() const { return ___badSound_57; }
	inline SoundClip_t2598932235 ** get_address_of_badSound_57() { return &___badSound_57; }
	inline void set_badSound_57(SoundClip_t2598932235 * value)
	{
		___badSound_57 = value;
		Il2CppCodeGenWriteBarrier(&___badSound_57, value);
	}

	inline static int32_t get_offset_of_correctSound_58() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___correctSound_58)); }
	inline SoundClip_t2598932235 * get_correctSound_58() const { return ___correctSound_58; }
	inline SoundClip_t2598932235 ** get_address_of_correctSound_58() { return &___correctSound_58; }
	inline void set_correctSound_58(SoundClip_t2598932235 * value)
	{
		___correctSound_58 = value;
		Il2CppCodeGenWriteBarrier(&___correctSound_58, value);
	}

	inline static int32_t get_offset_of_noteSounds_59() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___noteSounds_59)); }
	inline SoundClipU5BU5D_t2486522986* get_noteSounds_59() const { return ___noteSounds_59; }
	inline SoundClipU5BU5D_t2486522986** get_address_of_noteSounds_59() { return &___noteSounds_59; }
	inline void set_noteSounds_59(SoundClipU5BU5D_t2486522986* value)
	{
		___noteSounds_59 = value;
		Il2CppCodeGenWriteBarrier(&___noteSounds_59, value);
	}

	inline static int32_t get_offset_of_endHappyElf_60() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___endHappyElf_60)); }
	inline SoundClip_t2598932235 * get_endHappyElf_60() const { return ___endHappyElf_60; }
	inline SoundClip_t2598932235 ** get_address_of_endHappyElf_60() { return &___endHappyElf_60; }
	inline void set_endHappyElf_60(SoundClip_t2598932235 * value)
	{
		___endHappyElf_60 = value;
		Il2CppCodeGenWriteBarrier(&___endHappyElf_60, value);
	}

	inline static int32_t get_offset_of_correctDrawn_61() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___correctDrawn_61)); }
	inline SoundClip_t2598932235 * get_correctDrawn_61() const { return ___correctDrawn_61; }
	inline SoundClip_t2598932235 ** get_address_of_correctDrawn_61() { return &___correctDrawn_61; }
	inline void set_correctDrawn_61(SoundClip_t2598932235 * value)
	{
		___correctDrawn_61 = value;
		Il2CppCodeGenWriteBarrier(&___correctDrawn_61, value);
	}

	inline static int32_t get_offset_of_incorrectDrawn_62() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___incorrectDrawn_62)); }
	inline SoundClip_t2598932235 * get_incorrectDrawn_62() const { return ___incorrectDrawn_62; }
	inline SoundClip_t2598932235 ** get_address_of_incorrectDrawn_62() { return &___incorrectDrawn_62; }
	inline void set_incorrectDrawn_62(SoundClip_t2598932235 * value)
	{
		___incorrectDrawn_62 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectDrawn_62, value);
	}

	inline static int32_t get_offset_of_wrappingSoundLoop_63() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___wrappingSoundLoop_63)); }
	inline SoundClip_t2598932235 * get_wrappingSoundLoop_63() const { return ___wrappingSoundLoop_63; }
	inline SoundClip_t2598932235 ** get_address_of_wrappingSoundLoop_63() { return &___wrappingSoundLoop_63; }
	inline void set_wrappingSoundLoop_63(SoundClip_t2598932235 * value)
	{
		___wrappingSoundLoop_63 = value;
		Il2CppCodeGenWriteBarrier(&___wrappingSoundLoop_63, value);
	}

	inline static int32_t get_offset_of_correctResult_64() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___correctResult_64)); }
	inline SoundClip_t2598932235 * get_correctResult_64() const { return ___correctResult_64; }
	inline SoundClip_t2598932235 ** get_address_of_correctResult_64() { return &___correctResult_64; }
	inline void set_correctResult_64(SoundClip_t2598932235 * value)
	{
		___correctResult_64 = value;
		Il2CppCodeGenWriteBarrier(&___correctResult_64, value);
	}

	inline static int32_t get_offset_of_incorrectResult_65() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___incorrectResult_65)); }
	inline SoundClip_t2598932235 * get_incorrectResult_65() const { return ___incorrectResult_65; }
	inline SoundClip_t2598932235 ** get_address_of_incorrectResult_65() { return &___incorrectResult_65; }
	inline void set_incorrectResult_65(SoundClip_t2598932235 * value)
	{
		___incorrectResult_65 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectResult_65, value);
	}

	inline static int32_t get_offset_of_tapSound_66() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___tapSound_66)); }
	inline SoundClip_t2598932235 * get_tapSound_66() const { return ___tapSound_66; }
	inline SoundClip_t2598932235 ** get_address_of_tapSound_66() { return &___tapSound_66; }
	inline void set_tapSound_66(SoundClip_t2598932235 * value)
	{
		___tapSound_66 = value;
		Il2CppCodeGenWriteBarrier(&___tapSound_66, value);
	}

	inline static int32_t get_offset_of_giftSpawnSound_67() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___giftSpawnSound_67)); }
	inline SoundClip_t2598932235 * get_giftSpawnSound_67() const { return ___giftSpawnSound_67; }
	inline SoundClip_t2598932235 ** get_address_of_giftSpawnSound_67() { return &___giftSpawnSound_67; }
	inline void set_giftSpawnSound_67(SoundClip_t2598932235 * value)
	{
		___giftSpawnSound_67 = value;
		Il2CppCodeGenWriteBarrier(&___giftSpawnSound_67, value);
	}

	inline static int32_t get_offset_of_hitSound_68() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___hitSound_68)); }
	inline SoundClip_t2598932235 * get_hitSound_68() const { return ___hitSound_68; }
	inline SoundClip_t2598932235 ** get_address_of_hitSound_68() { return &___hitSound_68; }
	inline void set_hitSound_68(SoundClip_t2598932235 * value)
	{
		___hitSound_68 = value;
		Il2CppCodeGenWriteBarrier(&___hitSound_68, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
