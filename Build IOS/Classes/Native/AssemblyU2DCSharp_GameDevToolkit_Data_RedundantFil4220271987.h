﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// GameDevToolkit.Data.RedundantFileWriter
struct RedundantFileWriter_t3761749544;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0
struct  U3CDelayWriteU3Ec__Iterator0_t4220271987  : public Il2CppObject
{
public:
	// System.String GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0::path
	String_t* ___path_0;
	// System.String GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0::contents
	String_t* ___contents_1;
	// System.Boolean GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0::testJSON
	bool ___testJSON_2;
	// System.Boolean GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0::encrypted
	bool ___encrypted_3;
	// System.String GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0::encryptionKey
	String_t* ___encryptionKey_4;
	// System.Int32 GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0::retryCount
	int32_t ___retryCount_5;
	// GameDevToolkit.Data.RedundantFileWriter GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0::$this
	RedundantFileWriter_t3761749544 * ___U24this_6;
	// System.Object GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 GameDevToolkit.Data.RedundantFileWriter/<DelayWrite>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CDelayWriteU3Ec__Iterator0_t4220271987, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier(&___path_0, value);
	}

	inline static int32_t get_offset_of_contents_1() { return static_cast<int32_t>(offsetof(U3CDelayWriteU3Ec__Iterator0_t4220271987, ___contents_1)); }
	inline String_t* get_contents_1() const { return ___contents_1; }
	inline String_t** get_address_of_contents_1() { return &___contents_1; }
	inline void set_contents_1(String_t* value)
	{
		___contents_1 = value;
		Il2CppCodeGenWriteBarrier(&___contents_1, value);
	}

	inline static int32_t get_offset_of_testJSON_2() { return static_cast<int32_t>(offsetof(U3CDelayWriteU3Ec__Iterator0_t4220271987, ___testJSON_2)); }
	inline bool get_testJSON_2() const { return ___testJSON_2; }
	inline bool* get_address_of_testJSON_2() { return &___testJSON_2; }
	inline void set_testJSON_2(bool value)
	{
		___testJSON_2 = value;
	}

	inline static int32_t get_offset_of_encrypted_3() { return static_cast<int32_t>(offsetof(U3CDelayWriteU3Ec__Iterator0_t4220271987, ___encrypted_3)); }
	inline bool get_encrypted_3() const { return ___encrypted_3; }
	inline bool* get_address_of_encrypted_3() { return &___encrypted_3; }
	inline void set_encrypted_3(bool value)
	{
		___encrypted_3 = value;
	}

	inline static int32_t get_offset_of_encryptionKey_4() { return static_cast<int32_t>(offsetof(U3CDelayWriteU3Ec__Iterator0_t4220271987, ___encryptionKey_4)); }
	inline String_t* get_encryptionKey_4() const { return ___encryptionKey_4; }
	inline String_t** get_address_of_encryptionKey_4() { return &___encryptionKey_4; }
	inline void set_encryptionKey_4(String_t* value)
	{
		___encryptionKey_4 = value;
		Il2CppCodeGenWriteBarrier(&___encryptionKey_4, value);
	}

	inline static int32_t get_offset_of_retryCount_5() { return static_cast<int32_t>(offsetof(U3CDelayWriteU3Ec__Iterator0_t4220271987, ___retryCount_5)); }
	inline int32_t get_retryCount_5() const { return ___retryCount_5; }
	inline int32_t* get_address_of_retryCount_5() { return &___retryCount_5; }
	inline void set_retryCount_5(int32_t value)
	{
		___retryCount_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CDelayWriteU3Ec__Iterator0_t4220271987, ___U24this_6)); }
	inline RedundantFileWriter_t3761749544 * get_U24this_6() const { return ___U24this_6; }
	inline RedundantFileWriter_t3761749544 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(RedundantFileWriter_t3761749544 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CDelayWriteU3Ec__Iterator0_t4220271987, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CDelayWriteU3Ec__Iterator0_t4220271987, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CDelayWriteU3Ec__Iterator0_t4220271987, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
