﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GiftSpawner
struct  GiftSpawner_t1625374518  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject GiftSpawner::prefab
	GameObject_t1756533147 * ___prefab_2;
	// System.Int32 GiftSpawner::spawnNum
	int32_t ___spawnNum_3;
	// System.Int32 GiftSpawner::curspawn
	int32_t ___curspawn_4;
	// UnityEngine.GameObject GiftSpawner::temp
	GameObject_t1756533147 * ___temp_5;

public:
	inline static int32_t get_offset_of_prefab_2() { return static_cast<int32_t>(offsetof(GiftSpawner_t1625374518, ___prefab_2)); }
	inline GameObject_t1756533147 * get_prefab_2() const { return ___prefab_2; }
	inline GameObject_t1756533147 ** get_address_of_prefab_2() { return &___prefab_2; }
	inline void set_prefab_2(GameObject_t1756533147 * value)
	{
		___prefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_2, value);
	}

	inline static int32_t get_offset_of_spawnNum_3() { return static_cast<int32_t>(offsetof(GiftSpawner_t1625374518, ___spawnNum_3)); }
	inline int32_t get_spawnNum_3() const { return ___spawnNum_3; }
	inline int32_t* get_address_of_spawnNum_3() { return &___spawnNum_3; }
	inline void set_spawnNum_3(int32_t value)
	{
		___spawnNum_3 = value;
	}

	inline static int32_t get_offset_of_curspawn_4() { return static_cast<int32_t>(offsetof(GiftSpawner_t1625374518, ___curspawn_4)); }
	inline int32_t get_curspawn_4() const { return ___curspawn_4; }
	inline int32_t* get_address_of_curspawn_4() { return &___curspawn_4; }
	inline void set_curspawn_4(int32_t value)
	{
		___curspawn_4 = value;
	}

	inline static int32_t get_offset_of_temp_5() { return static_cast<int32_t>(offsetof(GiftSpawner_t1625374518, ___temp_5)); }
	inline GameObject_t1756533147 * get_temp_5() const { return ___temp_5; }
	inline GameObject_t1756533147 ** get_address_of_temp_5() { return &___temp_5; }
	inline void set_temp_5(GameObject_t1756533147 * value)
	{
		___temp_5 = value;
		Il2CppCodeGenWriteBarrier(&___temp_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
