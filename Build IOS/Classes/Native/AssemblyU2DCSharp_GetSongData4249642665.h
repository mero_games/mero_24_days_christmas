﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetSongData
struct  GetSongData_t4249642665  : public MonoBehaviour_t1158329972
{
public:
	// System.Single GetSongData::rmsValue
	float ___rmsValue_3;
	// System.Single GetSongData::dbValue
	float ___dbValue_4;
	// System.Single GetSongData::pitchValue
	float ___pitchValue_5;
	// UnityEngine.AudioSource GetSongData::source
	AudioSource_t1135106623 * ___source_6;
	// System.Single[] GetSongData::samples
	SingleU5BU5D_t577127397* ___samples_7;
	// System.Single[] GetSongData::spectrum
	SingleU5BU5D_t577127397* ___spectrum_8;
	// System.Single GetSongData::sampleRate
	float ___sampleRate_9;

public:
	inline static int32_t get_offset_of_rmsValue_3() { return static_cast<int32_t>(offsetof(GetSongData_t4249642665, ___rmsValue_3)); }
	inline float get_rmsValue_3() const { return ___rmsValue_3; }
	inline float* get_address_of_rmsValue_3() { return &___rmsValue_3; }
	inline void set_rmsValue_3(float value)
	{
		___rmsValue_3 = value;
	}

	inline static int32_t get_offset_of_dbValue_4() { return static_cast<int32_t>(offsetof(GetSongData_t4249642665, ___dbValue_4)); }
	inline float get_dbValue_4() const { return ___dbValue_4; }
	inline float* get_address_of_dbValue_4() { return &___dbValue_4; }
	inline void set_dbValue_4(float value)
	{
		___dbValue_4 = value;
	}

	inline static int32_t get_offset_of_pitchValue_5() { return static_cast<int32_t>(offsetof(GetSongData_t4249642665, ___pitchValue_5)); }
	inline float get_pitchValue_5() const { return ___pitchValue_5; }
	inline float* get_address_of_pitchValue_5() { return &___pitchValue_5; }
	inline void set_pitchValue_5(float value)
	{
		___pitchValue_5 = value;
	}

	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(GetSongData_t4249642665, ___source_6)); }
	inline AudioSource_t1135106623 * get_source_6() const { return ___source_6; }
	inline AudioSource_t1135106623 ** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(AudioSource_t1135106623 * value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier(&___source_6, value);
	}

	inline static int32_t get_offset_of_samples_7() { return static_cast<int32_t>(offsetof(GetSongData_t4249642665, ___samples_7)); }
	inline SingleU5BU5D_t577127397* get_samples_7() const { return ___samples_7; }
	inline SingleU5BU5D_t577127397** get_address_of_samples_7() { return &___samples_7; }
	inline void set_samples_7(SingleU5BU5D_t577127397* value)
	{
		___samples_7 = value;
		Il2CppCodeGenWriteBarrier(&___samples_7, value);
	}

	inline static int32_t get_offset_of_spectrum_8() { return static_cast<int32_t>(offsetof(GetSongData_t4249642665, ___spectrum_8)); }
	inline SingleU5BU5D_t577127397* get_spectrum_8() const { return ___spectrum_8; }
	inline SingleU5BU5D_t577127397** get_address_of_spectrum_8() { return &___spectrum_8; }
	inline void set_spectrum_8(SingleU5BU5D_t577127397* value)
	{
		___spectrum_8 = value;
		Il2CppCodeGenWriteBarrier(&___spectrum_8, value);
	}

	inline static int32_t get_offset_of_sampleRate_9() { return static_cast<int32_t>(offsetof(GetSongData_t4249642665, ___sampleRate_9)); }
	inline float get_sampleRate_9() const { return ___sampleRate_9; }
	inline float* get_address_of_sampleRate_9() { return &___sampleRate_9; }
	inline void set_sampleRate_9(float value)
	{
		___sampleRate_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
