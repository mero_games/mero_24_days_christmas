﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen2731324368.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// DraggableOrnament[]
struct DraggableOrnamentU5BU5D_t18558430;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTwentytwoManager
struct  GameTwentytwoManager_t3731809278  : public SingletonBase_1_t2731324368
{
public:
	// UnityEngine.UI.Text GameTwentytwoManager::numberOneText
	Text_t356221433 * ___numberOneText_4;
	// UnityEngine.UI.Text GameTwentytwoManager::numberTwoText
	Text_t356221433 * ___numberTwoText_5;
	// UnityEngine.UI.Text GameTwentytwoManager::operatorText
	Text_t356221433 * ___operatorText_6;
	// DraggableOrnament[] GameTwentytwoManager::ornaments
	DraggableOrnamentU5BU5D_t18558430* ___ornaments_7;
	// DraggableOrnament[] GameTwentytwoManager::junkOrnaments
	DraggableOrnamentU5BU5D_t18558430* ___junkOrnaments_8;
	// UnityEngine.Transform GameTwentytwoManager::ornamentsHolder
	Transform_t3275118058 * ___ornamentsHolder_9;
	// UnityEngine.Transform GameTwentytwoManager::targetSockets
	Transform_t3275118058 * ___targetSockets_10;
	// UnityEngine.GameObject GameTwentytwoManager::star
	GameObject_t1756533147 * ___star_11;
	// System.Int32 GameTwentytwoManager::result
	int32_t ___result_12;
	// System.Int32 GameTwentytwoManager::numberOne
	int32_t ___numberOne_13;
	// System.Int32 GameTwentytwoManager::numberTwo
	int32_t ___numberTwo_14;

public:
	inline static int32_t get_offset_of_numberOneText_4() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___numberOneText_4)); }
	inline Text_t356221433 * get_numberOneText_4() const { return ___numberOneText_4; }
	inline Text_t356221433 ** get_address_of_numberOneText_4() { return &___numberOneText_4; }
	inline void set_numberOneText_4(Text_t356221433 * value)
	{
		___numberOneText_4 = value;
		Il2CppCodeGenWriteBarrier(&___numberOneText_4, value);
	}

	inline static int32_t get_offset_of_numberTwoText_5() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___numberTwoText_5)); }
	inline Text_t356221433 * get_numberTwoText_5() const { return ___numberTwoText_5; }
	inline Text_t356221433 ** get_address_of_numberTwoText_5() { return &___numberTwoText_5; }
	inline void set_numberTwoText_5(Text_t356221433 * value)
	{
		___numberTwoText_5 = value;
		Il2CppCodeGenWriteBarrier(&___numberTwoText_5, value);
	}

	inline static int32_t get_offset_of_operatorText_6() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___operatorText_6)); }
	inline Text_t356221433 * get_operatorText_6() const { return ___operatorText_6; }
	inline Text_t356221433 ** get_address_of_operatorText_6() { return &___operatorText_6; }
	inline void set_operatorText_6(Text_t356221433 * value)
	{
		___operatorText_6 = value;
		Il2CppCodeGenWriteBarrier(&___operatorText_6, value);
	}

	inline static int32_t get_offset_of_ornaments_7() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___ornaments_7)); }
	inline DraggableOrnamentU5BU5D_t18558430* get_ornaments_7() const { return ___ornaments_7; }
	inline DraggableOrnamentU5BU5D_t18558430** get_address_of_ornaments_7() { return &___ornaments_7; }
	inline void set_ornaments_7(DraggableOrnamentU5BU5D_t18558430* value)
	{
		___ornaments_7 = value;
		Il2CppCodeGenWriteBarrier(&___ornaments_7, value);
	}

	inline static int32_t get_offset_of_junkOrnaments_8() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___junkOrnaments_8)); }
	inline DraggableOrnamentU5BU5D_t18558430* get_junkOrnaments_8() const { return ___junkOrnaments_8; }
	inline DraggableOrnamentU5BU5D_t18558430** get_address_of_junkOrnaments_8() { return &___junkOrnaments_8; }
	inline void set_junkOrnaments_8(DraggableOrnamentU5BU5D_t18558430* value)
	{
		___junkOrnaments_8 = value;
		Il2CppCodeGenWriteBarrier(&___junkOrnaments_8, value);
	}

	inline static int32_t get_offset_of_ornamentsHolder_9() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___ornamentsHolder_9)); }
	inline Transform_t3275118058 * get_ornamentsHolder_9() const { return ___ornamentsHolder_9; }
	inline Transform_t3275118058 ** get_address_of_ornamentsHolder_9() { return &___ornamentsHolder_9; }
	inline void set_ornamentsHolder_9(Transform_t3275118058 * value)
	{
		___ornamentsHolder_9 = value;
		Il2CppCodeGenWriteBarrier(&___ornamentsHolder_9, value);
	}

	inline static int32_t get_offset_of_targetSockets_10() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___targetSockets_10)); }
	inline Transform_t3275118058 * get_targetSockets_10() const { return ___targetSockets_10; }
	inline Transform_t3275118058 ** get_address_of_targetSockets_10() { return &___targetSockets_10; }
	inline void set_targetSockets_10(Transform_t3275118058 * value)
	{
		___targetSockets_10 = value;
		Il2CppCodeGenWriteBarrier(&___targetSockets_10, value);
	}

	inline static int32_t get_offset_of_star_11() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___star_11)); }
	inline GameObject_t1756533147 * get_star_11() const { return ___star_11; }
	inline GameObject_t1756533147 ** get_address_of_star_11() { return &___star_11; }
	inline void set_star_11(GameObject_t1756533147 * value)
	{
		___star_11 = value;
		Il2CppCodeGenWriteBarrier(&___star_11, value);
	}

	inline static int32_t get_offset_of_result_12() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___result_12)); }
	inline int32_t get_result_12() const { return ___result_12; }
	inline int32_t* get_address_of_result_12() { return &___result_12; }
	inline void set_result_12(int32_t value)
	{
		___result_12 = value;
	}

	inline static int32_t get_offset_of_numberOne_13() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___numberOne_13)); }
	inline int32_t get_numberOne_13() const { return ___numberOne_13; }
	inline int32_t* get_address_of_numberOne_13() { return &___numberOne_13; }
	inline void set_numberOne_13(int32_t value)
	{
		___numberOne_13 = value;
	}

	inline static int32_t get_offset_of_numberTwo_14() { return static_cast<int32_t>(offsetof(GameTwentytwoManager_t3731809278, ___numberTwo_14)); }
	inline int32_t get_numberTwo_14() const { return ___numberTwo_14; }
	inline int32_t* get_address_of_numberTwo_14() { return &___numberTwo_14; }
	inline void set_numberTwo_14(int32_t value)
	{
		___numberTwo_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
