﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Camera
struct Camera_t189460977;
// GameDevToolkit.Common.EDUCam
struct EDUCam_t524974491;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Common.EDUCam
struct  EDUCam_t524974491  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera GameDevToolkit.Common.EDUCam::attachedCamera
	Camera_t189460977 * ___attachedCamera_2;
	// System.Single GameDevToolkit.Common.EDUCam::targetaspect
	float ___targetaspect_3;
	// System.Boolean GameDevToolkit.Common.EDUCam::levelLoadRequested
	bool ___levelLoadRequested_4;

public:
	inline static int32_t get_offset_of_attachedCamera_2() { return static_cast<int32_t>(offsetof(EDUCam_t524974491, ___attachedCamera_2)); }
	inline Camera_t189460977 * get_attachedCamera_2() const { return ___attachedCamera_2; }
	inline Camera_t189460977 ** get_address_of_attachedCamera_2() { return &___attachedCamera_2; }
	inline void set_attachedCamera_2(Camera_t189460977 * value)
	{
		___attachedCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___attachedCamera_2, value);
	}

	inline static int32_t get_offset_of_targetaspect_3() { return static_cast<int32_t>(offsetof(EDUCam_t524974491, ___targetaspect_3)); }
	inline float get_targetaspect_3() const { return ___targetaspect_3; }
	inline float* get_address_of_targetaspect_3() { return &___targetaspect_3; }
	inline void set_targetaspect_3(float value)
	{
		___targetaspect_3 = value;
	}

	inline static int32_t get_offset_of_levelLoadRequested_4() { return static_cast<int32_t>(offsetof(EDUCam_t524974491, ___levelLoadRequested_4)); }
	inline bool get_levelLoadRequested_4() const { return ___levelLoadRequested_4; }
	inline bool* get_address_of_levelLoadRequested_4() { return &___levelLoadRequested_4; }
	inline void set_levelLoadRequested_4(bool value)
	{
		___levelLoadRequested_4 = value;
	}
};

struct EDUCam_t524974491_StaticFields
{
public:
	// GameDevToolkit.Common.EDUCam GameDevToolkit.Common.EDUCam::Instance
	EDUCam_t524974491 * ___Instance_5;

public:
	inline static int32_t get_offset_of_Instance_5() { return static_cast<int32_t>(offsetof(EDUCam_t524974491_StaticFields, ___Instance_5)); }
	inline EDUCam_t524974491 * get_Instance_5() const { return ___Instance_5; }
	inline EDUCam_t524974491 ** get_address_of_Instance_5() { return &___Instance_5; }
	inline void set_Instance_5(EDUCam_t524974491 * value)
	{
		___Instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
