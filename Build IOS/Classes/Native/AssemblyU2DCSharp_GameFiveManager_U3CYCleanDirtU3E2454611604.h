﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Dirt
struct Dirt_t450896597;
// GameFiveManager
struct GameFiveManager_t3955517211;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameFiveManager/<YCleanDirt>c__Iterator0
struct  U3CYCleanDirtU3Ec__Iterator0_t2454611604  : public Il2CppObject
{
public:
	// Dirt GameFiveManager/<YCleanDirt>c__Iterator0::dirt
	Dirt_t450896597 * ___dirt_0;
	// GameFiveManager GameFiveManager/<YCleanDirt>c__Iterator0::$this
	GameFiveManager_t3955517211 * ___U24this_1;
	// System.Object GameFiveManager/<YCleanDirt>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean GameFiveManager/<YCleanDirt>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 GameFiveManager/<YCleanDirt>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_dirt_0() { return static_cast<int32_t>(offsetof(U3CYCleanDirtU3Ec__Iterator0_t2454611604, ___dirt_0)); }
	inline Dirt_t450896597 * get_dirt_0() const { return ___dirt_0; }
	inline Dirt_t450896597 ** get_address_of_dirt_0() { return &___dirt_0; }
	inline void set_dirt_0(Dirt_t450896597 * value)
	{
		___dirt_0 = value;
		Il2CppCodeGenWriteBarrier(&___dirt_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CYCleanDirtU3Ec__Iterator0_t2454611604, ___U24this_1)); }
	inline GameFiveManager_t3955517211 * get_U24this_1() const { return ___U24this_1; }
	inline GameFiveManager_t3955517211 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GameFiveManager_t3955517211 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CYCleanDirtU3Ec__Iterator0_t2454611604, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CYCleanDirtU3Ec__Iterator0_t2454611604, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CYCleanDirtU3Ec__Iterator0_t2454611604, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
