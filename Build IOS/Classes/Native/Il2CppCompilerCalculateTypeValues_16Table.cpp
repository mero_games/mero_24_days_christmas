﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection301283622.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3517219175.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2252784345.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2167079021.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection536719976.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2291506050.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1899782350.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Rend984453155.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "DOTween_U3CModuleU3E3783534214.h"
#include "DOTween_DG_Tweening_AutoPlay2503223703.h"
#include "DOTween_DG_Tweening_AxisConstraint1244566668.h"
#include "DOTween_DG_Tweening_Color2232726623.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134.h"
#include "DOTween_DG_Tweening_EaseFunction3306356708.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_PathMode1545785466.h"
#include "DOTween_DG_Tweening_PathType2815988833.h"
#include "DOTween_DG_Tweening_RotateMode1177727514.h"
#include "DOTween_DG_Tweening_ScrambleMode385206138.h"
#include "DOTween_DG_Tweening_TweenExtensions405253783.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_Sequence110643099.h"
#include "DOTween_DG_Tweening_ShortcutExtensions3524050470.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843029.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842932.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842734.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842800.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843227.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843130.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628352.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341435.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678340.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978441.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678433.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653536.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016623.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678530.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691466.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341470.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391458.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691307.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366305.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628228.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341311.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391299.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978317.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691400.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366398.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341404.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016402.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653412.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366495.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830.h"
#include "DOTween_DG_Tweening_LogBehaviour3505725029.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "DOTween_DG_Tweening_TweenType169444141.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"
#include "DOTween_DG_Tweening_Plugins_Color2Plugin3433430606.h"
#include "DOTween_DG_Tweening_Plugins_DoublePlugin266400784.h"
#include "DOTween_DG_Tweening_Plugins_LongPlugin1941283029.h"
#include "DOTween_DG_Tweening_Plugins_UlongPlugin3231465400.h"
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPlugin2378569512.h"
#include "DOTween_DG_Tweening_Plugins_PathPlugin4171842066.h"
#include "DOTween_DG_Tweening_Plugins_ColorPlugin4063724482.h"
#include "DOTween_DG_Tweening_Plugins_IntPlugin180838436.h"
#include "DOTween_DG_Tweening_Plugins_QuaternionPlugin1696644323.h"
#include "DOTween_DG_Tweening_Plugins_RectOffsetPlugin664509336.h"
#include "DOTween_DG_Tweening_Plugins_RectPlugin391797831.h"
#include "DOTween_DG_Tweening_Plugins_UintPlugin1040977389.h"
#include "DOTween_DG_Tweening_Plugins_Vector2Plugin2164285386.h"
#include "DOTween_DG_Tweening_Plugins_Vector4Plugin2164361360.h"
#include "DOTween_DG_Tweening_Plugins_StringPlugin3620786088.h"
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensions3910942986.h"
#include "DOTween_DG_Tweening_Plugins_FloatPlugin3639480371.h"
#include "DOTween_DG_Tweening_Plugins_Vector3Plugin2164530409.h"
#include "DOTween_DG_Tweening_Plugins_Options_OrientType1755667719.h"
#include "DOTween_DG_Tweening_Plugins_Options_PathOptions2659884781.h"
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptio466049668.h"
#include "DOTween_DG_Tweening_Plugins_Options_UintOptions2267095136.h"
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOp2672570171.h"
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions2508431845.h"
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions2213017305.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1602[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (MessageEventArgs_t301283622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[2] = 
{
	MessageEventArgs_t301283622::get_offset_of_playerId_0(),
	MessageEventArgs_t301283622::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (PlayerConnection_t3517219175), -1, sizeof(PlayerConnection_t3517219175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1605[3] = 
{
	PlayerConnection_t3517219175::get_offset_of_m_PlayerEditorConnectionEvents_2(),
	PlayerConnection_t3517219175::get_offset_of_m_connectedPlayers_3(),
	PlayerConnection_t3517219175_StaticFields::get_offset_of_s_Instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (PlayerEditorConnectionEvents_t2252784345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1606[3] = 
{
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_messageTypeSubscribers_0(),
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_connectionEvent_1(),
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_disconnectionEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (MessageEvent_t2167079021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (ConnectionChangeEvent_t536719976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (MessageTypeSubscribers_t2291506050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[3] = 
{
	MessageTypeSubscribers_t2291506050::get_offset_of_m_messageTypeId_0(),
	MessageTypeSubscribers_t2291506050::get_offset_of_subscriberCount_1(),
	MessageTypeSubscribers_t2291506050::get_offset_of_messageCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[1] = 
{
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350::get_offset_of_messageId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (RenderPipelineManager_t984453155), -1, sizeof(RenderPipelineManager_t984453155_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1613[2] = 
{
	RenderPipelineManager_t984453155_StaticFields::get_offset_of_s_CurrentPipelineAsset_0(),
	RenderPipelineManager_t984453155_StaticFields::get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1616[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1617[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1618[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (AutoPlay_t2503223703)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1622[5] = 
{
	AutoPlay_t2503223703::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (AxisConstraint_t1244566668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1623[6] = 
{
	AxisConstraint_t1244566668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (Color2_t232726623)+ sizeof (Il2CppObject), sizeof(Color2_t232726623 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1624[2] = 
{
	Color2_t232726623::get_offset_of_ca_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color2_t232726623::get_offset_of_cb_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (TweenCallback_t3697142134), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (EaseFunction_t3306356708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (DOTween_t2276353038), -1, sizeof(DOTween_t2276353038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1628[24] = 
{
	DOTween_t2276353038_StaticFields::get_offset_of_Version_0(),
	DOTween_t2276353038_StaticFields::get_offset_of_useSafeMode_1(),
	DOTween_t2276353038_StaticFields::get_offset_of_showUnityEditorReport_2(),
	DOTween_t2276353038_StaticFields::get_offset_of_timeScale_3(),
	DOTween_t2276353038_StaticFields::get_offset_of_useSmoothDeltaTime_4(),
	DOTween_t2276353038_StaticFields::get_offset_of_maxSmoothUnscaledTime_5(),
	DOTween_t2276353038_StaticFields::get_offset_of__logBehaviour_6(),
	DOTween_t2276353038_StaticFields::get_offset_of_drawGizmos_7(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultUpdateType_8(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultTimeScaleIndependent_9(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultAutoPlay_10(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultAutoKill_11(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultLoopType_12(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultRecyclable_13(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEaseType_14(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEaseOvershootOrAmplitude_15(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEasePeriod_16(),
	DOTween_t2276353038_StaticFields::get_offset_of_instance_17(),
	DOTween_t2276353038_StaticFields::get_offset_of_isUnityEditor_18(),
	DOTween_t2276353038_StaticFields::get_offset_of_maxActiveTweenersReached_19(),
	DOTween_t2276353038_StaticFields::get_offset_of_maxActiveSequencesReached_20(),
	DOTween_t2276353038_StaticFields::get_offset_of_GizmosDelegates_21(),
	DOTween_t2276353038_StaticFields::get_offset_of_initialized_22(),
	DOTween_t2276353038_StaticFields::get_offset_of_isQuitting_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (Ease_t2502520296)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1629[39] = 
{
	Ease_t2502520296::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (PathMode_t1545785466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1631[5] = 
{
	PathMode_t1545785466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (PathType_t2815988833)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1632[3] = 
{
	PathType_t2815988833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (RotateMode_t1177727514)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1633[5] = 
{
	RotateMode_t1177727514::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (ScrambleMode_t385206138)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1634[7] = 
{
	ScrambleMode_t385206138::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (TweenExtensions_t405253783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (LoopType_t2249218064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1636[4] = 
{
	LoopType_t2249218064::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (Sequence_t110643099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1637[3] = 
{
	Sequence_t110643099::get_offset_of_sequencedTweens_51(),
	Sequence_t110643099::get_offset_of__sequencedObjs_52(),
	Sequence_t110643099::get_offset_of_lastTweenInsertTime_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (ShortcutExtensions_t3524050470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (U3CU3Ec__DisplayClass2_0_t964843029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1639[1] = 
{
	U3CU3Ec__DisplayClass2_0_t964843029::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (U3CU3Ec__DisplayClass3_0_t964842932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1640[1] = 
{
	U3CU3Ec__DisplayClass3_0_t964842932::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (U3CU3Ec__DisplayClass5_0_t964842734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1641[1] = 
{
	U3CU3Ec__DisplayClass5_0_t964842734::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (U3CU3Ec__DisplayClass7_0_t964842800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1642[1] = 
{
	U3CU3Ec__DisplayClass7_0_t964842800::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (U3CU3Ec__DisplayClass8_0_t964843227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1643[1] = 
{
	U3CU3Ec__DisplayClass8_0_t964843227::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (U3CU3Ec__DisplayClass9_0_t964843130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[1] = 
{
	U3CU3Ec__DisplayClass9_0_t964843130::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (U3CU3Ec__DisplayClass14_0_t3575628352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1645[1] = 
{
	U3CU3Ec__DisplayClass14_0_t3575628352::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (U3CU3Ec__DisplayClass15_0_t1989341435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1646[1] = 
{
	U3CU3Ec__DisplayClass15_0_t1989341435::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (U3CU3Ec__DisplayClass18_0_t1881678340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1647[1] = 
{
	U3CU3Ec__DisplayClass18_0_t1881678340::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (U3CU3Ec__DisplayClass20_0_t3010978441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1648[1] = 
{
	U3CU3Ec__DisplayClass20_0_t3010978441::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (U3CU3Ec__DisplayClass28_0_t1881678433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1649[1] = 
{
	U3CU3Ec__DisplayClass28_0_t1881678433::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (U3CU3Ec__DisplayClass32_0_t2728653536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1650[1] = 
{
	U3CU3Ec__DisplayClass32_0_t2728653536::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (U3CU3Ec__DisplayClass37_0_t1707016623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1651[1] = 
{
	U3CU3Ec__DisplayClass37_0_t1707016623::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (U3CU3Ec__DisplayClass38_0_t1881678530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[2] = 
{
	U3CU3Ec__DisplayClass38_0_t1881678530::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass38_0_t1881678530::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (U3CU3Ec__DisplayClass41_0_t1424691466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[1] = 
{
	U3CU3Ec__DisplayClass41_0_t1424691466::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (U3CU3Ec__DisplayClass45_0_t1989341470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[1] = 
{
	U3CU3Ec__DisplayClass45_0_t1989341470::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (U3CU3Ec__DisplayClass49_0_t295391458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[1] = 
{
	U3CU3Ec__DisplayClass49_0_t295391458::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (U3CU3Ec__DisplayClass51_0_t1424691307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[1] = 
{
	U3CU3Ec__DisplayClass51_0_t1424691307::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (U3CU3Ec__DisplayClass53_0_t1142366305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1657[1] = 
{
	U3CU3Ec__DisplayClass53_0_t1142366305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (U3CU3Ec__DisplayClass54_0_t3575628228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[1] = 
{
	U3CU3Ec__DisplayClass54_0_t3575628228::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (U3CU3Ec__DisplayClass55_0_t1989341311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1659[1] = 
{
	U3CU3Ec__DisplayClass55_0_t1989341311::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (U3CU3Ec__DisplayClass59_0_t295391299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1660[1] = 
{
	U3CU3Ec__DisplayClass59_0_t295391299::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (U3CU3Ec__DisplayClass60_0_t3010978317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[1] = 
{
	U3CU3Ec__DisplayClass60_0_t3010978317::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (U3CU3Ec__DisplayClass61_0_t1424691400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1662[1] = 
{
	U3CU3Ec__DisplayClass61_0_t1424691400::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (U3CU3Ec__DisplayClass63_0_t1142366398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[1] = 
{
	U3CU3Ec__DisplayClass63_0_t1142366398::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (U3CU3Ec__DisplayClass65_0_t1989341404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[1] = 
{
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (U3CU3Ec__DisplayClass67_0_t1707016402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[1] = 
{
	U3CU3Ec__DisplayClass67_0_t1707016402::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (U3CU3Ec__DisplayClass72_0_t2728653412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[1] = 
{
	U3CU3Ec__DisplayClass72_0_t2728653412::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (U3CU3Ec__DisplayClass73_0_t1142366495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[1] = 
{
	U3CU3Ec__DisplayClass73_0_t1142366495::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (TweenSettingsExtensions_t2285462830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (LogBehaviour_t3505725029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1669[4] = 
{
	LogBehaviour_t3505725029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (Tween_t278478013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1670[47] = 
{
	Tween_t278478013::get_offset_of_timeScale_4(),
	Tween_t278478013::get_offset_of_isBackwards_5(),
	Tween_t278478013::get_offset_of_id_6(),
	Tween_t278478013::get_offset_of_target_7(),
	Tween_t278478013::get_offset_of_updateType_8(),
	Tween_t278478013::get_offset_of_isIndependentUpdate_9(),
	Tween_t278478013::get_offset_of_onPlay_10(),
	Tween_t278478013::get_offset_of_onPause_11(),
	Tween_t278478013::get_offset_of_onRewind_12(),
	Tween_t278478013::get_offset_of_onUpdate_13(),
	Tween_t278478013::get_offset_of_onStepComplete_14(),
	Tween_t278478013::get_offset_of_onComplete_15(),
	Tween_t278478013::get_offset_of_onKill_16(),
	Tween_t278478013::get_offset_of_onWaypointChange_17(),
	Tween_t278478013::get_offset_of_isFrom_18(),
	Tween_t278478013::get_offset_of_isBlendable_19(),
	Tween_t278478013::get_offset_of_isRecyclable_20(),
	Tween_t278478013::get_offset_of_isSpeedBased_21(),
	Tween_t278478013::get_offset_of_autoKill_22(),
	Tween_t278478013::get_offset_of_duration_23(),
	Tween_t278478013::get_offset_of_loops_24(),
	Tween_t278478013::get_offset_of_loopType_25(),
	Tween_t278478013::get_offset_of_delay_26(),
	Tween_t278478013::get_offset_of_isRelative_27(),
	Tween_t278478013::get_offset_of_easeType_28(),
	Tween_t278478013::get_offset_of_customEase_29(),
	Tween_t278478013::get_offset_of_easeOvershootOrAmplitude_30(),
	Tween_t278478013::get_offset_of_easePeriod_31(),
	Tween_t278478013::get_offset_of_typeofT1_32(),
	Tween_t278478013::get_offset_of_typeofT2_33(),
	Tween_t278478013::get_offset_of_typeofTPlugOptions_34(),
	Tween_t278478013::get_offset_of_active_35(),
	Tween_t278478013::get_offset_of_isSequenced_36(),
	Tween_t278478013::get_offset_of_sequenceParent_37(),
	Tween_t278478013::get_offset_of_activeId_38(),
	Tween_t278478013::get_offset_of_specialStartupMode_39(),
	Tween_t278478013::get_offset_of_creationLocked_40(),
	Tween_t278478013::get_offset_of_startupDone_41(),
	Tween_t278478013::get_offset_of_playedOnce_42(),
	Tween_t278478013::get_offset_of_position_43(),
	Tween_t278478013::get_offset_of_fullDuration_44(),
	Tween_t278478013::get_offset_of_completedLoops_45(),
	Tween_t278478013::get_offset_of_isPlaying_46(),
	Tween_t278478013::get_offset_of_isComplete_47(),
	Tween_t278478013::get_offset_of_elapsedDelay_48(),
	Tween_t278478013::get_offset_of_delayComplete_49(),
	Tween_t278478013::get_offset_of_miscInt_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (Tweener_t760404022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1671[2] = 
{
	Tweener_t760404022::get_offset_of_hasManuallySetStartValue_51(),
	Tweener_t760404022::get_offset_of_isFromAllowed_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (TweenType_t169444141)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1672[4] = 
{
	TweenType_t169444141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (UpdateType_t3357224513)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1673[4] = 
{
	UpdateType_t3357224513::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (Color2Plugin_t3433430606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (DoublePlugin_t266400784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (LongPlugin_t1941283029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (UlongPlugin_t3231465400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (Vector3ArrayPlugin_t2378569512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (PathPlugin_t4171842066), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (ColorPlugin_t4063724482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (IntPlugin_t180838436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (QuaternionPlugin_t1696644323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (RectOffsetPlugin_t664509336), -1, sizeof(RectOffsetPlugin_t664509336_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1683[1] = 
{
	RectOffsetPlugin_t664509336_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (RectPlugin_t391797831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (UintPlugin_t1040977389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (Vector2Plugin_t2164285386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (Vector4Plugin_t2164361360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (StringPlugin_t3620786088), -1, sizeof(StringPlugin_t3620786088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1688[2] = 
{
	StringPlugin_t3620786088_StaticFields::get_offset_of__Buffer_0(),
	StringPlugin_t3620786088_StaticFields::get_offset_of__OpenedTags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (StringPluginExtensions_t3910942986), -1, sizeof(StringPluginExtensions_t3910942986_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1689[5] = 
{
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsAll_0(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsUppercase_1(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsLowercase_2(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsNumerals_3(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of__lastRndSeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (FloatPlugin_t3639480371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (Vector3Plugin_t2164530409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (OrientType_t1755667719)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1693[5] = 
{
	OrientType_t1755667719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (PathOptions_t2659884781)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[15] = 
{
	PathOptions_t2659884781::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_orientType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lockPositionAxis_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lockRotationAxis_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_isClosedPath_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAtPosition_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAtTransform_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAhead_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_hasCustomForwardDirection_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_forward_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_useLocalPosition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_parent_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_isRigidbody_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_startupRot_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_startupZRot_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (QuaternionOptions_t466049668)+ sizeof (Il2CppObject), sizeof(QuaternionOptions_t466049668 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1695[3] = 
{
	QuaternionOptions_t466049668::get_offset_of_rotateMode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QuaternionOptions_t466049668::get_offset_of_axisConstraint_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QuaternionOptions_t466049668::get_offset_of_up_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (UintOptions_t2267095136)+ sizeof (Il2CppObject), sizeof(UintOptions_t2267095136_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1696[1] = 
{
	UintOptions_t2267095136::get_offset_of_isNegativeChangeValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (Vector3ArrayOptions_t2672570171)+ sizeof (Il2CppObject), sizeof(Vector3ArrayOptions_t2672570171_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1697[3] = 
{
	Vector3ArrayOptions_t2672570171::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_durations_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (NoOptions_t2508431845)+ sizeof (Il2CppObject), sizeof(NoOptions_t2508431845 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (ColorOptions_t2213017305)+ sizeof (Il2CppObject), sizeof(ColorOptions_t2213017305_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1699[1] = 
{
	ColorOptions_t2213017305::get_offset_of_alphaOnly_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
