﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Common.ArrayUtils
struct  ArrayUtils_t1556298404  : public Il2CppObject
{
public:

public:
};

struct ArrayUtils_t1556298404_StaticFields
{
public:
	// System.Int32[] GameDevToolkit.Common.ArrayUtils::lastOrder
	Int32U5BU5D_t3030399641* ___lastOrder_0;

public:
	inline static int32_t get_offset_of_lastOrder_0() { return static_cast<int32_t>(offsetof(ArrayUtils_t1556298404_StaticFields, ___lastOrder_0)); }
	inline Int32U5BU5D_t3030399641* get_lastOrder_0() const { return ___lastOrder_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_lastOrder_0() { return &___lastOrder_0; }
	inline void set_lastOrder_0(Int32U5BU5D_t3030399641* value)
	{
		___lastOrder_0 = value;
		Il2CppCodeGenWriteBarrier(&___lastOrder_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
