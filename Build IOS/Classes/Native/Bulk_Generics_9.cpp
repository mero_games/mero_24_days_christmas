﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Comparison_1_gen3282130926.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Comparison_1_gen2136256369.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Comparison_1_gen1282925227.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Comparison_1_gen3545134003.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "mscorlib_System_Comparison_1_gen1348919171.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "mscorlib_System_Comparison_1_gen1030680329.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "mscorlib_System_Comparison_1_gen23408355.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Comparison_1_gen588049429.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Comparison_1_gen2465997669.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Comparison_1_gen3505446430.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Comparison_1_gen3505446431.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Comparison_1_gen3505446432.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_Converter_2_gen106372939.h"
#include "System_Core_System_Func_1_gen1485000104.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "System_Core_System_Func_1_gen348874681.h"
#include "System_Core_System_Func_2_gen3201915814.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "System_Core_System_Func_2_gen3408749018.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23653809873.h"
#include "System_Core_System_Func_2_gen1866569452.h"
#include "System_Core_System_Func_2_gen1628330325.h"
#include "mscorlib_System_Single2076509932.h"
#include "System_Core_System_Func_2_gen3961629604.h"
#include "System_Core_System_Func_2_gen2825504181.h"
#include "System_Core_System_Func_2_gen2212564818.h"

// System.Comparison`1<UnityEngine.Color>
struct Comparison_1_t3282130926;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Comparison`1<UnityEngine.Color32>
struct Comparison_1_t2136256369;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t1282925227;
// System.Comparison`1<UnityEngine.KeyCode>
struct Comparison_1_t3545134003;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t1348919171;
// System.Comparison`1<UnityEngine.RaycastHit2D>
struct Comparison_1_t1030680329;
// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t23408355;
// System.Comparison`1<UnityEngine.UILineInfo>
struct Comparison_1_t588049429;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t2465997669;
// System.Comparison`1<UnityEngine.Vector2>
struct Comparison_1_t3505446430;
// System.Comparison`1<UnityEngine.Vector3>
struct Comparison_1_t3505446431;
// System.Comparison`1<UnityEngine.Vector4>
struct Comparison_1_t3505446432;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t106372939;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;
// System.Func`1<System.Object>
struct Func_1_t348874681;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>
struct Func_2_t3201915814;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,System.Boolean>
struct Func_2_t3408749018;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,UnityEngine.KeyCode>
struct Func_2_t1866569452;
// System.Func`2<System.Int32,System.Single>
struct Func_2_t1628330325;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Func`2<System.Object,System.Single>
struct Func_2_t2212564818;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m3193235346_MetadataUsageId;
extern Il2CppClass* Color32_t874517518_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m3266062717_MetadataUsageId;
extern Il2CppClass* RaycastResult_t21186376_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m2263530995_MetadataUsageId;
extern Il2CppClass* KeyCode_t2283395152_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m2168077883_MetadataUsageId;
extern Il2CppClass* RaycastHit_t87180320_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m2330243615_MetadataUsageId;
extern Il2CppClass* RaycastHit2D_t4063908774_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m4228229373_MetadataUsageId;
extern Il2CppClass* UICharInfo_t3056636800_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m2910474027_MetadataUsageId;
extern Il2CppClass* UILineInfo_t3621277874_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m4142385273_MetadataUsageId;
extern Il2CppClass* UIVertex_t1204258818_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m4224593217_MetadataUsageId;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m4030535530_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m1103040431_MetadataUsageId;
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t Comparison_1_BeginInvoke_m4056757384_MetadataUsageId;
extern Il2CppClass* KeyValuePair_2_t38854645_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2337603003_MetadataUsageId;
extern Il2CppClass* KeyValuePair_2_t3653809873_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2358111867_MetadataUsageId;
extern const uint32_t Func_2_BeginInvoke_m4234815605_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m3638876405_MetadataUsageId;



// System.Int32 System.Comparison`1<UnityEngine.Color>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m4167024709_gshared (Comparison_1_t3282130926 * __this, Color_t2020392075  ___x0, Color_t2020392075  ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.Color32>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m4190993814_gshared (Comparison_1_t2136256369 * __this, Color32_t874517518  ___x0, Color32_t874517518  ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m345024424_gshared (Comparison_1_t1282925227 * __this, RaycastResult_t21186376  ___x0, RaycastResult_t21186376  ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.KeyCode>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m3099237362_gshared (Comparison_1_t3545134003 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.RaycastHit>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1670081898_gshared (Comparison_1_t1348919171 * __this, RaycastHit_t87180320  ___x0, RaycastHit_t87180320  ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.RaycastHit2D>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m4042012016_gshared (Comparison_1_t1030680329 * __this, RaycastHit2D_t4063908774  ___x0, RaycastHit2D_t4063908774  ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m2645957248_gshared (Comparison_1_t23408355 * __this, UICharInfo_t3056636800  ___x0, UICharInfo_t3056636800  ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.UILineInfo>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m353639462_gshared (Comparison_1_t588049429 * __this, UILineInfo_t3621277874  ___x0, UILineInfo_t3621277874  ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.UIVertex>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m897835902_gshared (Comparison_1_t2465997669 * __this, UIVertex_t1204258818  ___x0, UIVertex_t1204258818  ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.Vector2>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m2664841287_gshared (Comparison_1_t3505446430 * __this, Vector2_t2243707579  ___x0, Vector2_t2243707579  ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.Vector3>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m4047872872_gshared (Comparison_1_t3505446431 * __this, Vector3_t2243707580  ___x0, Vector3_t2243707580  ___y1, const MethodInfo* method);
// System.Int32 System.Comparison`1<UnityEngine.Vector4>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1081247749_gshared (Comparison_1_t3505446432 * __this, Vector4_t2243707581  ___x0, Vector4_t2243707581  ___y1, const MethodInfo* method);
// TOutput System.Converter`2<System.Object,System.Object>::Invoke(TInput)
extern "C"  Il2CppObject * Converter_2_Invoke_m77799585_gshared (Converter_2_t106372939 * __this, Il2CppObject * ___input0, const MethodInfo* method);
// TResult System.Func`1<System.Boolean>::Invoke()
extern "C"  bool Func_1_Invoke_m250581785_gshared (Func_1_t1485000104 * __this, const MethodInfo* method);
// TResult System.Func`1<System.Object>::Invoke()
extern "C"  Il2CppObject * Func_1_Invoke_m846324009_gshared (Func_1_t348874681 * __this, const MethodInfo* method);
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m174842950_gshared (Func_2_t3201915814 * __this, KeyValuePair_2_t38854645  ___arg10, const MethodInfo* method);
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m1584973958_gshared (Func_2_t3408749018 * __this, KeyValuePair_2_t3653809873  ___arg10, const MethodInfo* method);
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,UnityEngine.KeyCode>::Invoke(T)
extern "C"  int32_t Func_2_Invoke_m214125350_gshared (Func_2_t1866569452 * __this, KeyValuePair_2_t3653809873  ___arg10, const MethodInfo* method);
// TResult System.Func`2<System.Int32,System.Single>::Invoke(T)
extern "C"  float Func_2_Invoke_m1703281116_gshared (Func_2_t1628330325 * __this, int32_t ___arg10, const MethodInfo* method);
// TResult System.Func`2<System.Object,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m2968608789_gshared (Func_2_t3961629604 * __this, Il2CppObject * ___arg10, const MethodInfo* method);
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m3288232740_gshared (Func_2_t2825504181 * __this, Il2CppObject * ___arg10, const MethodInfo* method);
// TResult System.Func`2<System.Object,System.Single>::Invoke(T)
extern "C"  float Func_2_Invoke_m4121137703_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___arg10, const MethodInfo* method);

// System.Int32 System.Comparison`1<UnityEngine.Color>::Invoke(T,T)
#define Comparison_1_Invoke_m4167024709(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3282130926 *, Color_t2020392075 , Color_t2020392075 , const MethodInfo*))Comparison_1_Invoke_m4167024709_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.Color32>::Invoke(T,T)
#define Comparison_1_Invoke_m4190993814(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t2136256369 *, Color32_t874517518 , Color32_t874517518 , const MethodInfo*))Comparison_1_Invoke_m4190993814_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T,T)
#define Comparison_1_Invoke_m345024424(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1282925227 *, RaycastResult_t21186376 , RaycastResult_t21186376 , const MethodInfo*))Comparison_1_Invoke_m345024424_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.KeyCode>::Invoke(T,T)
#define Comparison_1_Invoke_m3099237362(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3545134003 *, int32_t, int32_t, const MethodInfo*))Comparison_1_Invoke_m3099237362_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.RaycastHit>::Invoke(T,T)
#define Comparison_1_Invoke_m1670081898(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1348919171 *, RaycastHit_t87180320 , RaycastHit_t87180320 , const MethodInfo*))Comparison_1_Invoke_m1670081898_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.RaycastHit2D>::Invoke(T,T)
#define Comparison_1_Invoke_m4042012016(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1030680329 *, RaycastHit2D_t4063908774 , RaycastHit2D_t4063908774 , const MethodInfo*))Comparison_1_Invoke_m4042012016_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::Invoke(T,T)
#define Comparison_1_Invoke_m2645957248(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t23408355 *, UICharInfo_t3056636800 , UICharInfo_t3056636800 , const MethodInfo*))Comparison_1_Invoke_m2645957248_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.UILineInfo>::Invoke(T,T)
#define Comparison_1_Invoke_m353639462(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t588049429 *, UILineInfo_t3621277874 , UILineInfo_t3621277874 , const MethodInfo*))Comparison_1_Invoke_m353639462_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.UIVertex>::Invoke(T,T)
#define Comparison_1_Invoke_m897835902(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t2465997669 *, UIVertex_t1204258818 , UIVertex_t1204258818 , const MethodInfo*))Comparison_1_Invoke_m897835902_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.Vector2>::Invoke(T,T)
#define Comparison_1_Invoke_m2664841287(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3505446430 *, Vector2_t2243707579 , Vector2_t2243707579 , const MethodInfo*))Comparison_1_Invoke_m2664841287_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.Vector3>::Invoke(T,T)
#define Comparison_1_Invoke_m4047872872(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3505446431 *, Vector3_t2243707580 , Vector3_t2243707580 , const MethodInfo*))Comparison_1_Invoke_m4047872872_gshared)(__this, ___x0, ___y1, method)
// System.Int32 System.Comparison`1<UnityEngine.Vector4>::Invoke(T,T)
#define Comparison_1_Invoke_m1081247749(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3505446432 *, Vector4_t2243707581 , Vector4_t2243707581 , const MethodInfo*))Comparison_1_Invoke_m1081247749_gshared)(__this, ___x0, ___y1, method)
// TOutput System.Converter`2<System.Object,System.Object>::Invoke(TInput)
#define Converter_2_Invoke_m77799585(__this, ___input0, method) ((  Il2CppObject * (*) (Converter_2_t106372939 *, Il2CppObject *, const MethodInfo*))Converter_2_Invoke_m77799585_gshared)(__this, ___input0, method)
// TResult System.Func`1<System.Boolean>::Invoke()
#define Func_1_Invoke_m250581785(__this, method) ((  bool (*) (Func_1_t1485000104 *, const MethodInfo*))Func_1_Invoke_m250581785_gshared)(__this, method)
// TResult System.Func`1<System.Object>::Invoke()
#define Func_1_Invoke_m846324009(__this, method) ((  Il2CppObject * (*) (Func_1_t348874681 *, const MethodInfo*))Func_1_Invoke_m846324009_gshared)(__this, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m174842950(__this, ___arg10, method) ((  bool (*) (Func_2_t3201915814 *, KeyValuePair_2_t38854645 , const MethodInfo*))Func_2_Invoke_m174842950_gshared)(__this, ___arg10, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m1584973958(__this, ___arg10, method) ((  bool (*) (Func_2_t3408749018 *, KeyValuePair_2_t3653809873 , const MethodInfo*))Func_2_Invoke_m1584973958_gshared)(__this, ___arg10, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,UnityEngine.KeyCode>::Invoke(T)
#define Func_2_Invoke_m214125350(__this, ___arg10, method) ((  int32_t (*) (Func_2_t1866569452 *, KeyValuePair_2_t3653809873 , const MethodInfo*))Func_2_Invoke_m214125350_gshared)(__this, ___arg10, method)
// TResult System.Func`2<System.Int32,System.Single>::Invoke(T)
#define Func_2_Invoke_m1703281116(__this, ___arg10, method) ((  float (*) (Func_2_t1628330325 *, int32_t, const MethodInfo*))Func_2_Invoke_m1703281116_gshared)(__this, ___arg10, method)
// TResult System.Func`2<System.Object,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m2968608789(__this, ___arg10, method) ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m2968608789_gshared)(__this, ___arg10, method)
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
#define Func_2_Invoke_m3288232740(__this, ___arg10, method) ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m3288232740_gshared)(__this, ___arg10, method)
// TResult System.Func`2<System.Object,System.Single>::Invoke(T)
#define Func_2_Invoke_m4121137703(__this, ___arg10, method) ((  float (*) (Func_2_t2212564818 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m4121137703_gshared)(__this, ___arg10, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Comparison`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m1357820959_gshared (Comparison_1_t3282130926 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.Color>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m4167024709_gshared (Comparison_1_t3282130926 * __this, Color_t2020392075  ___x0, Color_t2020392075  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m4167024709((Comparison_1_t3282130926 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___x0, Color_t2020392075  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Color_t2020392075  ___x0, Color_t2020392075  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.Color>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m3193235346_gshared (Comparison_1_t3282130926 * __this, Color_t2020392075  ___x0, Color_t2020392075  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m3193235346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m539126851_gshared (Comparison_1_t3282130926 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m2942822710_gshared (Comparison_1_t2136256369 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.Color32>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m4190993814_gshared (Comparison_1_t2136256369 * __this, Color32_t874517518  ___x0, Color32_t874517518  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m4190993814((Comparison_1_t2136256369 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t874517518  ___x0, Color32_t874517518  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Color32_t874517518  ___x0, Color32_t874517518  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.Color32>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m3266062717_gshared (Comparison_1_t2136256369 * __this, Color32_t874517518  ___x0, Color32_t874517518  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m3266062717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Color32_t874517518_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(Color32_t874517518_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m2033936832_gshared (Comparison_1_t2136256369 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m1414815602_gshared (Comparison_1_t1282925227 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m345024424_gshared (Comparison_1_t1282925227 * __this, RaycastResult_t21186376  ___x0, RaycastResult_t21186376  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m345024424((Comparison_1_t1282925227 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastResult_t21186376  ___x0, RaycastResult_t21186376  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, RaycastResult_t21186376  ___x0, RaycastResult_t21186376  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m2263530995_gshared (Comparison_1_t1282925227 * __this, RaycastResult_t21186376  ___x0, RaycastResult_t21186376  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m2263530995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(RaycastResult_t21186376_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(RaycastResult_t21186376_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m4098579094_gshared (Comparison_1_t1282925227 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.KeyCode>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m958543642_gshared (Comparison_1_t3545134003 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.KeyCode>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m3099237362_gshared (Comparison_1_t3545134003 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m3099237362((Comparison_1_t3545134003 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.KeyCode>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m2168077883_gshared (Comparison_1_t3545134003 * __this, int32_t ___x0, int32_t ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m2168077883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(KeyCode_t2283395152_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(KeyCode_t2283395152_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.KeyCode>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m2442500220_gshared (Comparison_1_t3545134003 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.RaycastHit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m1178069812_gshared (Comparison_1_t1348919171 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.RaycastHit>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1670081898_gshared (Comparison_1_t1348919171 * __this, RaycastHit_t87180320  ___x0, RaycastHit_t87180320  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m1670081898((Comparison_1_t1348919171 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastHit_t87180320  ___x0, RaycastHit_t87180320  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, RaycastHit_t87180320  ___x0, RaycastHit_t87180320  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.RaycastHit>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m2330243615_gshared (Comparison_1_t1348919171 * __this, RaycastHit_t87180320  ___x0, RaycastHit_t87180320  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m2330243615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(RaycastHit_t87180320_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(RaycastHit_t87180320_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.RaycastHit>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m3762228136_gshared (Comparison_1_t1348919171 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.RaycastHit2D>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m2227159381_gshared (Comparison_1_t1030680329 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.RaycastHit2D>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m4042012016_gshared (Comparison_1_t1030680329 * __this, RaycastHit2D_t4063908774  ___x0, RaycastHit2D_t4063908774  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m4042012016((Comparison_1_t1030680329 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastHit2D_t4063908774  ___x0, RaycastHit2D_t4063908774  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, RaycastHit2D_t4063908774  ___x0, RaycastHit2D_t4063908774  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.RaycastHit2D>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m4228229373_gshared (Comparison_1_t1030680329 * __this, RaycastHit2D_t4063908774  ___x0, RaycastHit2D_t4063908774  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m4228229373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(RaycastHit2D_t4063908774_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(RaycastHit2D_t4063908774_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.RaycastHit2D>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m2460758186_gshared (Comparison_1_t1030680329 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m3767256160_gshared (Comparison_1_t23408355 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m2645957248_gshared (Comparison_1_t23408355 * __this, UICharInfo_t3056636800  ___x0, UICharInfo_t3056636800  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m2645957248((Comparison_1_t23408355 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, UICharInfo_t3056636800  ___x0, UICharInfo_t3056636800  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, UICharInfo_t3056636800  ___x0, UICharInfo_t3056636800  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.UICharInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m2910474027_gshared (Comparison_1_t23408355 * __this, UICharInfo_t3056636800  ___x0, UICharInfo_t3056636800  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m2910474027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UICharInfo_t3056636800_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(UICharInfo_t3056636800_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m4170692898_gshared (Comparison_1_t23408355 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m1832890678_gshared (Comparison_1_t588049429 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.UILineInfo>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m353639462_gshared (Comparison_1_t588049429 * __this, UILineInfo_t3621277874  ___x0, UILineInfo_t3621277874  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m353639462((Comparison_1_t588049429 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, UILineInfo_t3621277874  ___x0, UILineInfo_t3621277874  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, UILineInfo_t3621277874  ___x0, UILineInfo_t3621277874  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.UILineInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m4142385273_gshared (Comparison_1_t588049429 * __this, UILineInfo_t3621277874  ___x0, UILineInfo_t3621277874  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m4142385273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UILineInfo_t3621277874_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(UILineInfo_t3621277874_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m4174686984_gshared (Comparison_1_t588049429 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m852795630_gshared (Comparison_1_t2465997669 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.UIVertex>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m897835902_gshared (Comparison_1_t2465997669 * __this, UIVertex_t1204258818  ___x0, UIVertex_t1204258818  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m897835902((Comparison_1_t2465997669 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, UIVertex_t1204258818  ___x0, UIVertex_t1204258818  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, UIVertex_t1204258818  ___x0, UIVertex_t1204258818  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.UIVertex>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m4224593217_gshared (Comparison_1_t2465997669 * __this, UIVertex_t1204258818  ___x0, UIVertex_t1204258818  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m4224593217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIVertex_t1204258818_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(UIVertex_t1204258818_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m1074531304_gshared (Comparison_1_t2465997669 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m883164393_gshared (Comparison_1_t3505446430 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.Vector2>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m2664841287_gshared (Comparison_1_t3505446430 * __this, Vector2_t2243707579  ___x0, Vector2_t2243707579  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m2664841287((Comparison_1_t3505446430 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___x0, Vector2_t2243707579  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___x0, Vector2_t2243707579  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.Vector2>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m4030535530_gshared (Comparison_1_t3505446430 * __this, Vector2_t2243707579  ___x0, Vector2_t2243707579  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m4030535530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m153558673_gshared (Comparison_1_t3505446430 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m3438229060_gshared (Comparison_1_t3505446431 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.Vector3>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m4047872872_gshared (Comparison_1_t3505446431 * __this, Vector3_t2243707580  ___x0, Vector3_t2243707580  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m4047872872((Comparison_1_t3505446431 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___x0, Vector3_t2243707580  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___x0, Vector3_t2243707580  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.Vector3>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m1103040431_gshared (Comparison_1_t3505446431 * __this, Vector3_t2243707580  ___x0, Vector3_t2243707580  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m1103040431_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m2678763282_gshared (Comparison_1_t3505446431 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Comparison`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m2159122699_gshared (Comparison_1_t3505446432 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<UnityEngine.Vector4>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1081247749_gshared (Comparison_1_t3505446432 * __this, Vector4_t2243707581  ___x0, Vector4_t2243707581  ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m1081247749((Comparison_1_t3505446432 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___x0, Vector4_t2243707581  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___x0, Vector4_t2243707581  ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<UnityEngine.Vector4>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m4056757384_gshared (Comparison_1_t3505446432 * __this, Vector4_t2243707581  ___x0, Vector4_t2243707581  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparison_1_BeginInvoke_m4056757384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___x0);
	__d_args[1] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___y1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m3572773391_gshared (Comparison_1_t3505446432 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Converter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Converter_2__ctor_m2798627395_gshared (Converter_2_t106372939 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TOutput System.Converter`2<System.Object,System.Object>::Invoke(TInput)
extern "C"  Il2CppObject * Converter_2_Invoke_m77799585_gshared (Converter_2_t106372939 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Converter_2_Invoke_m77799585((Converter_2_t106372939 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Converter`2<System.Object,System.Object>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Converter_2_BeginInvoke_m898151494_gshared (Converter_2_t106372939 * __this, Il2CppObject * ___input0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TOutput System.Converter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Converter_2_EndInvoke_m1606718561_gshared (Converter_2_t106372939 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_1__ctor_m3694154362_gshared (Func_1_t1485000104 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`1<System.Boolean>::Invoke()
extern "C"  bool Func_1_Invoke_m250581785_gshared (Func_1_t1485000104 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_1_Invoke_m250581785((Func_1_t1485000104 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`1<System.Boolean>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_1_BeginInvoke_m2842480425_gshared (Func_1_t1485000104 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// TResult System.Func`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_1_EndInvoke_m2039491024_gshared (Func_1_t1485000104 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_1__ctor_m3570736155_gshared (Func_1_t348874681 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`1<System.Object>::Invoke()
extern "C"  Il2CppObject * Func_1_Invoke_m846324009_gshared (Func_1_t348874681 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_1_Invoke_m846324009((Func_1_t348874681 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_1_BeginInvoke_m478378364_gshared (Func_1_t348874681 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// TResult System.Func`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_1_EndInvoke_m1965878547_gshared (Func_1_t348874681 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m809665740_gshared (Func_2_t3201915814 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m174842950_gshared (Func_2_t3201915814 * __this, KeyValuePair_2_t38854645  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m174842950((Func_2_t3201915814 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t38854645  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, KeyValuePair_2_t38854645  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2337603003_gshared (Func_2_t3201915814 * __this, KeyValuePair_2_t38854645  ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2337603003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t38854645_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m4101312048_gshared (Func_2_t3201915814 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m2183979442_gshared (Func_2_t3408749018 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m1584973958_gshared (Func_2_t3408749018 * __this, KeyValuePair_2_t3653809873  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1584973958((Func_2_t3408749018 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3653809873  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, KeyValuePair_2_t3653809873  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2358111867_gshared (Func_2_t3408749018 * __this, KeyValuePair_2_t3653809873  ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2358111867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3653809873_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m651448816_gshared (Func_2_t3408749018 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,UnityEngine.KeyCode>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m2439909586_gshared (Func_2_t1866569452 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,UnityEngine.KeyCode>::Invoke(T)
extern "C"  int32_t Func_2_Invoke_m214125350_gshared (Func_2_t1866569452 * __this, KeyValuePair_2_t3653809873  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m214125350((Func_2_t1866569452 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3653809873  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, KeyValuePair_2_t3653809873  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,UnityEngine.KeyCode>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m4234815605_gshared (Func_2_t1866569452 * __this, KeyValuePair_2_t3653809873  ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m4234815605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3653809873_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,UnityEngine.KeyCode>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Func_2_EndInvoke_m1648143304_gshared (Func_2_t1866569452 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Int32,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m752820952_gshared (Func_2_t1628330325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Int32,System.Single>::Invoke(T)
extern "C"  float Func_2_Invoke_m1703281116_gshared (Func_2_t1628330325 * __this, int32_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1703281116((Func_2_t1628330325 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Int32,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3638876405_gshared (Func_2_t1628330325 * __this, int32_t ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m3638876405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Int32,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_2_EndInvoke_m2813932450_gshared (Func_2_t1628330325 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1354888807_gshared (Func_2_t3961629604 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m2968608789_gshared (Func_2_t3961629604 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m2968608789((Func_2_t3961629604 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m1429757044_gshared (Func_2_t3961629604 * __this, Il2CppObject * ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m924416567_gshared (Func_2_t3961629604 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1684831714_gshared (Func_2_t2825504181 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m3288232740_gshared (Func_2_t2825504181 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3288232740((Func_2_t2825504181 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m4034295761_gshared (Func_2_t2825504181 * __this, Il2CppObject * ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m1674435418_gshared (Func_2_t2825504181 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`2<System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1874497973_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Single>::Invoke(T)
extern "C"  float Func_2_Invoke_m4121137703_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m4121137703((Func_2_t2212564818 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m669892004_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_2_EndInvoke_m971580865_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
