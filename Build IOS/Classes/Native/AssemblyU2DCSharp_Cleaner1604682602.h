﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cleaner
struct  Cleaner_t1604682602  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource Cleaner::cleanSound
	AudioSource_t1135106623 * ___cleanSound_2;
	// UnityEngine.Collider2D Cleaner::currentCollider
	Collider2D_t646061738 * ___currentCollider_3;
	// UnityEngine.ParticleSystem Cleaner::smokePuff
	ParticleSystem_t3394631041 * ___smokePuff_4;

public:
	inline static int32_t get_offset_of_cleanSound_2() { return static_cast<int32_t>(offsetof(Cleaner_t1604682602, ___cleanSound_2)); }
	inline AudioSource_t1135106623 * get_cleanSound_2() const { return ___cleanSound_2; }
	inline AudioSource_t1135106623 ** get_address_of_cleanSound_2() { return &___cleanSound_2; }
	inline void set_cleanSound_2(AudioSource_t1135106623 * value)
	{
		___cleanSound_2 = value;
		Il2CppCodeGenWriteBarrier(&___cleanSound_2, value);
	}

	inline static int32_t get_offset_of_currentCollider_3() { return static_cast<int32_t>(offsetof(Cleaner_t1604682602, ___currentCollider_3)); }
	inline Collider2D_t646061738 * get_currentCollider_3() const { return ___currentCollider_3; }
	inline Collider2D_t646061738 ** get_address_of_currentCollider_3() { return &___currentCollider_3; }
	inline void set_currentCollider_3(Collider2D_t646061738 * value)
	{
		___currentCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentCollider_3, value);
	}

	inline static int32_t get_offset_of_smokePuff_4() { return static_cast<int32_t>(offsetof(Cleaner_t1604682602, ___smokePuff_4)); }
	inline ParticleSystem_t3394631041 * get_smokePuff_4() const { return ___smokePuff_4; }
	inline ParticleSystem_t3394631041 ** get_address_of_smokePuff_4() { return &___smokePuff_4; }
	inline void set_smokePuff_4(ParticleSystem_t3394631041 * value)
	{
		___smokePuff_4 = value;
		Il2CppCodeGenWriteBarrier(&___smokePuff_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
