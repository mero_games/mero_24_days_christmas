﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DraggableOrnament
struct  DraggableOrnament_t3124044711  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean DraggableOrnament::beenUsed
	bool ___beenUsed_2;
	// System.Int32 DraggableOrnament::number
	int32_t ___number_3;
	// UnityEngine.Collider2D DraggableOrnament::overlapCollider
	Collider2D_t646061738 * ___overlapCollider_4;
	// System.Boolean DraggableOrnament::canClick
	bool ___canClick_5;
	// UnityEngine.SpriteRenderer DraggableOrnament::light
	SpriteRenderer_t1209076198 * ___light_6;

public:
	inline static int32_t get_offset_of_beenUsed_2() { return static_cast<int32_t>(offsetof(DraggableOrnament_t3124044711, ___beenUsed_2)); }
	inline bool get_beenUsed_2() const { return ___beenUsed_2; }
	inline bool* get_address_of_beenUsed_2() { return &___beenUsed_2; }
	inline void set_beenUsed_2(bool value)
	{
		___beenUsed_2 = value;
	}

	inline static int32_t get_offset_of_number_3() { return static_cast<int32_t>(offsetof(DraggableOrnament_t3124044711, ___number_3)); }
	inline int32_t get_number_3() const { return ___number_3; }
	inline int32_t* get_address_of_number_3() { return &___number_3; }
	inline void set_number_3(int32_t value)
	{
		___number_3 = value;
	}

	inline static int32_t get_offset_of_overlapCollider_4() { return static_cast<int32_t>(offsetof(DraggableOrnament_t3124044711, ___overlapCollider_4)); }
	inline Collider2D_t646061738 * get_overlapCollider_4() const { return ___overlapCollider_4; }
	inline Collider2D_t646061738 ** get_address_of_overlapCollider_4() { return &___overlapCollider_4; }
	inline void set_overlapCollider_4(Collider2D_t646061738 * value)
	{
		___overlapCollider_4 = value;
		Il2CppCodeGenWriteBarrier(&___overlapCollider_4, value);
	}

	inline static int32_t get_offset_of_canClick_5() { return static_cast<int32_t>(offsetof(DraggableOrnament_t3124044711, ___canClick_5)); }
	inline bool get_canClick_5() const { return ___canClick_5; }
	inline bool* get_address_of_canClick_5() { return &___canClick_5; }
	inline void set_canClick_5(bool value)
	{
		___canClick_5 = value;
	}

	inline static int32_t get_offset_of_light_6() { return static_cast<int32_t>(offsetof(DraggableOrnament_t3124044711, ___light_6)); }
	inline SpriteRenderer_t1209076198 * get_light_6() const { return ___light_6; }
	inline SpriteRenderer_t1209076198 ** get_address_of_light_6() { return &___light_6; }
	inline void set_light_6(SpriteRenderer_t1209076198 * value)
	{
		___light_6 = value;
		Il2CppCodeGenWriteBarrier(&___light_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
