﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Collider2D
struct Collider2D_t646061738;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Characters.ArrowKeyMover
struct  ArrowKeyMover_t1318051673  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GameDevToolkit.Characters.ArrowKeyMover::canMove
	bool ___canMove_2;
	// System.Boolean GameDevToolkit.Characters.ArrowKeyMover::canMoveUp
	bool ___canMoveUp_3;
	// System.Boolean GameDevToolkit.Characters.ArrowKeyMover::canMoveDown
	bool ___canMoveDown_4;
	// System.Boolean GameDevToolkit.Characters.ArrowKeyMover::canMoveRight
	bool ___canMoveRight_5;
	// System.Boolean GameDevToolkit.Characters.ArrowKeyMover::canMoveLeft
	bool ___canMoveLeft_6;
	// System.Single GameDevToolkit.Characters.ArrowKeyMover::horizontalSpeed
	float ___horizontalSpeed_7;
	// System.Single GameDevToolkit.Characters.ArrowKeyMover::verticalSpeed
	float ___verticalSpeed_8;
	// System.Single GameDevToolkit.Characters.ArrowKeyMover::minX
	float ___minX_9;
	// System.Single GameDevToolkit.Characters.ArrowKeyMover::maxX
	float ___maxX_10;
	// System.Single GameDevToolkit.Characters.ArrowKeyMover::minY
	float ___minY_11;
	// System.Single GameDevToolkit.Characters.ArrowKeyMover::maxY
	float ___maxY_12;
	// UnityEngine.Collider2D GameDevToolkit.Characters.ArrowKeyMover::upArrow
	Collider2D_t646061738 * ___upArrow_13;
	// UnityEngine.Collider2D GameDevToolkit.Characters.ArrowKeyMover::downArrow
	Collider2D_t646061738 * ___downArrow_14;
	// UnityEngine.Collider2D GameDevToolkit.Characters.ArrowKeyMover::leftArrow
	Collider2D_t646061738 * ___leftArrow_15;
	// UnityEngine.Collider2D GameDevToolkit.Characters.ArrowKeyMover::rightArrow
	Collider2D_t646061738 * ___rightArrow_16;
	// System.Boolean GameDevToolkit.Characters.ArrowKeyMover::goLeft
	bool ___goLeft_17;
	// System.Boolean GameDevToolkit.Characters.ArrowKeyMover::goRight
	bool ___goRight_18;
	// System.Boolean GameDevToolkit.Characters.ArrowKeyMover::goUp
	bool ___goUp_19;
	// System.Boolean GameDevToolkit.Characters.ArrowKeyMover::goDown
	bool ___goDown_20;

public:
	inline static int32_t get_offset_of_canMove_2() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___canMove_2)); }
	inline bool get_canMove_2() const { return ___canMove_2; }
	inline bool* get_address_of_canMove_2() { return &___canMove_2; }
	inline void set_canMove_2(bool value)
	{
		___canMove_2 = value;
	}

	inline static int32_t get_offset_of_canMoveUp_3() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___canMoveUp_3)); }
	inline bool get_canMoveUp_3() const { return ___canMoveUp_3; }
	inline bool* get_address_of_canMoveUp_3() { return &___canMoveUp_3; }
	inline void set_canMoveUp_3(bool value)
	{
		___canMoveUp_3 = value;
	}

	inline static int32_t get_offset_of_canMoveDown_4() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___canMoveDown_4)); }
	inline bool get_canMoveDown_4() const { return ___canMoveDown_4; }
	inline bool* get_address_of_canMoveDown_4() { return &___canMoveDown_4; }
	inline void set_canMoveDown_4(bool value)
	{
		___canMoveDown_4 = value;
	}

	inline static int32_t get_offset_of_canMoveRight_5() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___canMoveRight_5)); }
	inline bool get_canMoveRight_5() const { return ___canMoveRight_5; }
	inline bool* get_address_of_canMoveRight_5() { return &___canMoveRight_5; }
	inline void set_canMoveRight_5(bool value)
	{
		___canMoveRight_5 = value;
	}

	inline static int32_t get_offset_of_canMoveLeft_6() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___canMoveLeft_6)); }
	inline bool get_canMoveLeft_6() const { return ___canMoveLeft_6; }
	inline bool* get_address_of_canMoveLeft_6() { return &___canMoveLeft_6; }
	inline void set_canMoveLeft_6(bool value)
	{
		___canMoveLeft_6 = value;
	}

	inline static int32_t get_offset_of_horizontalSpeed_7() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___horizontalSpeed_7)); }
	inline float get_horizontalSpeed_7() const { return ___horizontalSpeed_7; }
	inline float* get_address_of_horizontalSpeed_7() { return &___horizontalSpeed_7; }
	inline void set_horizontalSpeed_7(float value)
	{
		___horizontalSpeed_7 = value;
	}

	inline static int32_t get_offset_of_verticalSpeed_8() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___verticalSpeed_8)); }
	inline float get_verticalSpeed_8() const { return ___verticalSpeed_8; }
	inline float* get_address_of_verticalSpeed_8() { return &___verticalSpeed_8; }
	inline void set_verticalSpeed_8(float value)
	{
		___verticalSpeed_8 = value;
	}

	inline static int32_t get_offset_of_minX_9() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___minX_9)); }
	inline float get_minX_9() const { return ___minX_9; }
	inline float* get_address_of_minX_9() { return &___minX_9; }
	inline void set_minX_9(float value)
	{
		___minX_9 = value;
	}

	inline static int32_t get_offset_of_maxX_10() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___maxX_10)); }
	inline float get_maxX_10() const { return ___maxX_10; }
	inline float* get_address_of_maxX_10() { return &___maxX_10; }
	inline void set_maxX_10(float value)
	{
		___maxX_10 = value;
	}

	inline static int32_t get_offset_of_minY_11() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___minY_11)); }
	inline float get_minY_11() const { return ___minY_11; }
	inline float* get_address_of_minY_11() { return &___minY_11; }
	inline void set_minY_11(float value)
	{
		___minY_11 = value;
	}

	inline static int32_t get_offset_of_maxY_12() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___maxY_12)); }
	inline float get_maxY_12() const { return ___maxY_12; }
	inline float* get_address_of_maxY_12() { return &___maxY_12; }
	inline void set_maxY_12(float value)
	{
		___maxY_12 = value;
	}

	inline static int32_t get_offset_of_upArrow_13() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___upArrow_13)); }
	inline Collider2D_t646061738 * get_upArrow_13() const { return ___upArrow_13; }
	inline Collider2D_t646061738 ** get_address_of_upArrow_13() { return &___upArrow_13; }
	inline void set_upArrow_13(Collider2D_t646061738 * value)
	{
		___upArrow_13 = value;
		Il2CppCodeGenWriteBarrier(&___upArrow_13, value);
	}

	inline static int32_t get_offset_of_downArrow_14() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___downArrow_14)); }
	inline Collider2D_t646061738 * get_downArrow_14() const { return ___downArrow_14; }
	inline Collider2D_t646061738 ** get_address_of_downArrow_14() { return &___downArrow_14; }
	inline void set_downArrow_14(Collider2D_t646061738 * value)
	{
		___downArrow_14 = value;
		Il2CppCodeGenWriteBarrier(&___downArrow_14, value);
	}

	inline static int32_t get_offset_of_leftArrow_15() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___leftArrow_15)); }
	inline Collider2D_t646061738 * get_leftArrow_15() const { return ___leftArrow_15; }
	inline Collider2D_t646061738 ** get_address_of_leftArrow_15() { return &___leftArrow_15; }
	inline void set_leftArrow_15(Collider2D_t646061738 * value)
	{
		___leftArrow_15 = value;
		Il2CppCodeGenWriteBarrier(&___leftArrow_15, value);
	}

	inline static int32_t get_offset_of_rightArrow_16() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___rightArrow_16)); }
	inline Collider2D_t646061738 * get_rightArrow_16() const { return ___rightArrow_16; }
	inline Collider2D_t646061738 ** get_address_of_rightArrow_16() { return &___rightArrow_16; }
	inline void set_rightArrow_16(Collider2D_t646061738 * value)
	{
		___rightArrow_16 = value;
		Il2CppCodeGenWriteBarrier(&___rightArrow_16, value);
	}

	inline static int32_t get_offset_of_goLeft_17() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___goLeft_17)); }
	inline bool get_goLeft_17() const { return ___goLeft_17; }
	inline bool* get_address_of_goLeft_17() { return &___goLeft_17; }
	inline void set_goLeft_17(bool value)
	{
		___goLeft_17 = value;
	}

	inline static int32_t get_offset_of_goRight_18() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___goRight_18)); }
	inline bool get_goRight_18() const { return ___goRight_18; }
	inline bool* get_address_of_goRight_18() { return &___goRight_18; }
	inline void set_goRight_18(bool value)
	{
		___goRight_18 = value;
	}

	inline static int32_t get_offset_of_goUp_19() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___goUp_19)); }
	inline bool get_goUp_19() const { return ___goUp_19; }
	inline bool* get_address_of_goUp_19() { return &___goUp_19; }
	inline void set_goUp_19(bool value)
	{
		___goUp_19 = value;
	}

	inline static int32_t get_offset_of_goDown_20() { return static_cast<int32_t>(offsetof(ArrowKeyMover_t1318051673, ___goDown_20)); }
	inline bool get_goDown_20() const { return ___goDown_20; }
	inline bool* get_address_of_goDown_20() { return &___goDown_20; }
	inline void set_goDown_20(bool value)
	{
		___goDown_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
