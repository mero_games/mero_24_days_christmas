﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen2231141178.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// SnowManMatch[]
struct SnowManMatchU5BU5D_t2589032331;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.Sprite
struct Sprite_t309593783;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameThirteenManager
struct  GameThirteenManager_t3231626088  : public SingletonBase_1_t2231141178
{
public:
	// System.Int32 GameThirteenManager::numberOfSetsToDo
	int32_t ___numberOfSetsToDo_4;
	// System.Int32 GameThirteenManager::setsDone
	int32_t ___setsDone_5;
	// UnityEngine.Vector3 GameThirteenManager::snowManSize
	Vector3_t2243707580  ___snowManSize_6;
	// UnityEngine.GameObject GameThirteenManager::exampleSnowMan
	GameObject_t1756533147 * ___exampleSnowMan_7;
	// SnowManMatch[] GameThirteenManager::snowMen
	SnowManMatchU5BU5D_t2589032331* ___snowMen_8;
	// UnityEngine.Sprite[] GameThirteenManager::snowMenParts
	SpriteU5BU5D_t3359083662* ___snowMenParts_9;
	// System.Boolean GameThirteenManager::canClick
	bool ___canClick_10;
	// UnityEngine.Sprite GameThirteenManager::originalHead
	Sprite_t309593783 * ___originalHead_11;
	// UnityEngine.Sprite GameThirteenManager::originalBody
	Sprite_t309593783 * ___originalBody_12;
	// System.Int32 GameThirteenManager::spriteOneCount
	int32_t ___spriteOneCount_13;
	// System.Int32 GameThirteenManager::spriteTwoCount
	int32_t ___spriteTwoCount_14;

public:
	inline static int32_t get_offset_of_numberOfSetsToDo_4() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___numberOfSetsToDo_4)); }
	inline int32_t get_numberOfSetsToDo_4() const { return ___numberOfSetsToDo_4; }
	inline int32_t* get_address_of_numberOfSetsToDo_4() { return &___numberOfSetsToDo_4; }
	inline void set_numberOfSetsToDo_4(int32_t value)
	{
		___numberOfSetsToDo_4 = value;
	}

	inline static int32_t get_offset_of_setsDone_5() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___setsDone_5)); }
	inline int32_t get_setsDone_5() const { return ___setsDone_5; }
	inline int32_t* get_address_of_setsDone_5() { return &___setsDone_5; }
	inline void set_setsDone_5(int32_t value)
	{
		___setsDone_5 = value;
	}

	inline static int32_t get_offset_of_snowManSize_6() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___snowManSize_6)); }
	inline Vector3_t2243707580  get_snowManSize_6() const { return ___snowManSize_6; }
	inline Vector3_t2243707580 * get_address_of_snowManSize_6() { return &___snowManSize_6; }
	inline void set_snowManSize_6(Vector3_t2243707580  value)
	{
		___snowManSize_6 = value;
	}

	inline static int32_t get_offset_of_exampleSnowMan_7() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___exampleSnowMan_7)); }
	inline GameObject_t1756533147 * get_exampleSnowMan_7() const { return ___exampleSnowMan_7; }
	inline GameObject_t1756533147 ** get_address_of_exampleSnowMan_7() { return &___exampleSnowMan_7; }
	inline void set_exampleSnowMan_7(GameObject_t1756533147 * value)
	{
		___exampleSnowMan_7 = value;
		Il2CppCodeGenWriteBarrier(&___exampleSnowMan_7, value);
	}

	inline static int32_t get_offset_of_snowMen_8() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___snowMen_8)); }
	inline SnowManMatchU5BU5D_t2589032331* get_snowMen_8() const { return ___snowMen_8; }
	inline SnowManMatchU5BU5D_t2589032331** get_address_of_snowMen_8() { return &___snowMen_8; }
	inline void set_snowMen_8(SnowManMatchU5BU5D_t2589032331* value)
	{
		___snowMen_8 = value;
		Il2CppCodeGenWriteBarrier(&___snowMen_8, value);
	}

	inline static int32_t get_offset_of_snowMenParts_9() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___snowMenParts_9)); }
	inline SpriteU5BU5D_t3359083662* get_snowMenParts_9() const { return ___snowMenParts_9; }
	inline SpriteU5BU5D_t3359083662** get_address_of_snowMenParts_9() { return &___snowMenParts_9; }
	inline void set_snowMenParts_9(SpriteU5BU5D_t3359083662* value)
	{
		___snowMenParts_9 = value;
		Il2CppCodeGenWriteBarrier(&___snowMenParts_9, value);
	}

	inline static int32_t get_offset_of_canClick_10() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___canClick_10)); }
	inline bool get_canClick_10() const { return ___canClick_10; }
	inline bool* get_address_of_canClick_10() { return &___canClick_10; }
	inline void set_canClick_10(bool value)
	{
		___canClick_10 = value;
	}

	inline static int32_t get_offset_of_originalHead_11() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___originalHead_11)); }
	inline Sprite_t309593783 * get_originalHead_11() const { return ___originalHead_11; }
	inline Sprite_t309593783 ** get_address_of_originalHead_11() { return &___originalHead_11; }
	inline void set_originalHead_11(Sprite_t309593783 * value)
	{
		___originalHead_11 = value;
		Il2CppCodeGenWriteBarrier(&___originalHead_11, value);
	}

	inline static int32_t get_offset_of_originalBody_12() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___originalBody_12)); }
	inline Sprite_t309593783 * get_originalBody_12() const { return ___originalBody_12; }
	inline Sprite_t309593783 ** get_address_of_originalBody_12() { return &___originalBody_12; }
	inline void set_originalBody_12(Sprite_t309593783 * value)
	{
		___originalBody_12 = value;
		Il2CppCodeGenWriteBarrier(&___originalBody_12, value);
	}

	inline static int32_t get_offset_of_spriteOneCount_13() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___spriteOneCount_13)); }
	inline int32_t get_spriteOneCount_13() const { return ___spriteOneCount_13; }
	inline int32_t* get_address_of_spriteOneCount_13() { return &___spriteOneCount_13; }
	inline void set_spriteOneCount_13(int32_t value)
	{
		___spriteOneCount_13 = value;
	}

	inline static int32_t get_offset_of_spriteTwoCount_14() { return static_cast<int32_t>(offsetof(GameThirteenManager_t3231626088, ___spriteTwoCount_14)); }
	inline int32_t get_spriteTwoCount_14() const { return ___spriteTwoCount_14; }
	inline int32_t* get_address_of_spriteTwoCount_14() { return &___spriteTwoCount_14; }
	inline void set_spriteTwoCount_14(int32_t value)
	{
		___spriteTwoCount_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
