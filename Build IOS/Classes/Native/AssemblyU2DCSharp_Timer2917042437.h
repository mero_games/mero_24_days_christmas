﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Timer
struct  Timer_t2917042437  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Timer::minutesToPlay
	float ___minutesToPlay_2;
	// System.Single Timer::timeToPlay
	float ___timeToPlay_3;
	// System.Single Timer::timer
	float ___timer_4;

public:
	inline static int32_t get_offset_of_minutesToPlay_2() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ___minutesToPlay_2)); }
	inline float get_minutesToPlay_2() const { return ___minutesToPlay_2; }
	inline float* get_address_of_minutesToPlay_2() { return &___minutesToPlay_2; }
	inline void set_minutesToPlay_2(float value)
	{
		___minutesToPlay_2 = value;
	}

	inline static int32_t get_offset_of_timeToPlay_3() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ___timeToPlay_3)); }
	inline float get_timeToPlay_3() const { return ___timeToPlay_3; }
	inline float* get_address_of_timeToPlay_3() { return &___timeToPlay_3; }
	inline void set_timeToPlay_3(float value)
	{
		___timeToPlay_3 = value;
	}

	inline static int32_t get_offset_of_timer_4() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ___timer_4)); }
	inline float get_timer_4() const { return ___timer_4; }
	inline float* get_address_of_timer_4() { return &___timer_4; }
	inline void set_timer_4(float value)
	{
		___timer_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
