﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Difference
struct Difference_t980400781;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Twin
struct  Twin_t1055953070  : public MonoBehaviour_t1158329972
{
public:
	// Difference Twin::twin
	Difference_t980400781 * ___twin_2;

public:
	inline static int32_t get_offset_of_twin_2() { return static_cast<int32_t>(offsetof(Twin_t1055953070, ___twin_2)); }
	inline Difference_t980400781 * get_twin_2() const { return ___twin_2; }
	inline Difference_t980400781 ** get_address_of_twin_2() { return &___twin_2; }
	inline void set_twin_2(Difference_t980400781 * value)
	{
		___twin_2 = value;
		Il2CppCodeGenWriteBarrier(&___twin_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
