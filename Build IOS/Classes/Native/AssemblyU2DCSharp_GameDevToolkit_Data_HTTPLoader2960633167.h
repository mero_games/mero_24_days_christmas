﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameDevToolkit.Data.HTTPLoader/LoadCompleteListener
struct LoadCompleteListener_t2670511940;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Data.HTTPLoader
struct  HTTPLoader_t2960633167  : public Il2CppObject
{
public:
	// GameDevToolkit.Data.HTTPLoader/LoadCompleteListener GameDevToolkit.Data.HTTPLoader::LoadComplete
	LoadCompleteListener_t2670511940 * ___LoadComplete_1;
	// UnityEngine.WWWForm GameDevToolkit.Data.HTTPLoader::formData
	WWWForm_t3950226929 * ___formData_2;
	// System.String GameDevToolkit.Data.HTTPLoader::url
	String_t* ___url_3;
	// UnityEngine.WWW GameDevToolkit.Data.HTTPLoader::loader
	WWW_t2919945039 * ___loader_4;

public:
	inline static int32_t get_offset_of_LoadComplete_1() { return static_cast<int32_t>(offsetof(HTTPLoader_t2960633167, ___LoadComplete_1)); }
	inline LoadCompleteListener_t2670511940 * get_LoadComplete_1() const { return ___LoadComplete_1; }
	inline LoadCompleteListener_t2670511940 ** get_address_of_LoadComplete_1() { return &___LoadComplete_1; }
	inline void set_LoadComplete_1(LoadCompleteListener_t2670511940 * value)
	{
		___LoadComplete_1 = value;
		Il2CppCodeGenWriteBarrier(&___LoadComplete_1, value);
	}

	inline static int32_t get_offset_of_formData_2() { return static_cast<int32_t>(offsetof(HTTPLoader_t2960633167, ___formData_2)); }
	inline WWWForm_t3950226929 * get_formData_2() const { return ___formData_2; }
	inline WWWForm_t3950226929 ** get_address_of_formData_2() { return &___formData_2; }
	inline void set_formData_2(WWWForm_t3950226929 * value)
	{
		___formData_2 = value;
		Il2CppCodeGenWriteBarrier(&___formData_2, value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(HTTPLoader_t2960633167, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier(&___url_3, value);
	}

	inline static int32_t get_offset_of_loader_4() { return static_cast<int32_t>(offsetof(HTTPLoader_t2960633167, ___loader_4)); }
	inline WWW_t2919945039 * get_loader_4() const { return ___loader_4; }
	inline WWW_t2919945039 ** get_address_of_loader_4() { return &___loader_4; }
	inline void set_loader_4(WWW_t2919945039 * value)
	{
		___loader_4 = value;
		Il2CppCodeGenWriteBarrier(&___loader_4, value);
	}
};

struct HTTPLoader_t2960633167_StaticFields
{
public:
	// System.Boolean GameDevToolkit.Data.HTTPLoader::forceNoCache
	bool ___forceNoCache_0;

public:
	inline static int32_t get_offset_of_forceNoCache_0() { return static_cast<int32_t>(offsetof(HTTPLoader_t2960633167_StaticFields, ___forceNoCache_0)); }
	inline bool get_forceNoCache_0() const { return ___forceNoCache_0; }
	inline bool* get_address_of_forceNoCache_0() { return &___forceNoCache_0; }
	inline void set_forceNoCache_0(bool value)
	{
		___forceNoCache_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
