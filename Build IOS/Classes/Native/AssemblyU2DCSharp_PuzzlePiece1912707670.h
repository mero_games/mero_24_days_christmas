﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Collider2D
struct Collider2D_t646061738;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzlePiece
struct  PuzzlePiece_t1912707670  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 PuzzlePiece::startSlotPosition
	Vector3_t2243707580  ___startSlotPosition_2;
	// System.Boolean PuzzlePiece::isDragging
	bool ___isDragging_3;
	// System.Boolean PuzzlePiece::beenDropped
	bool ___beenDropped_4;
	// UnityEngine.Collider2D PuzzlePiece::col
	Collider2D_t646061738 * ___col_5;
	// UnityEngine.Collider2D PuzzlePiece::overlapCollider
	Collider2D_t646061738 * ___overlapCollider_6;

public:
	inline static int32_t get_offset_of_startSlotPosition_2() { return static_cast<int32_t>(offsetof(PuzzlePiece_t1912707670, ___startSlotPosition_2)); }
	inline Vector3_t2243707580  get_startSlotPosition_2() const { return ___startSlotPosition_2; }
	inline Vector3_t2243707580 * get_address_of_startSlotPosition_2() { return &___startSlotPosition_2; }
	inline void set_startSlotPosition_2(Vector3_t2243707580  value)
	{
		___startSlotPosition_2 = value;
	}

	inline static int32_t get_offset_of_isDragging_3() { return static_cast<int32_t>(offsetof(PuzzlePiece_t1912707670, ___isDragging_3)); }
	inline bool get_isDragging_3() const { return ___isDragging_3; }
	inline bool* get_address_of_isDragging_3() { return &___isDragging_3; }
	inline void set_isDragging_3(bool value)
	{
		___isDragging_3 = value;
	}

	inline static int32_t get_offset_of_beenDropped_4() { return static_cast<int32_t>(offsetof(PuzzlePiece_t1912707670, ___beenDropped_4)); }
	inline bool get_beenDropped_4() const { return ___beenDropped_4; }
	inline bool* get_address_of_beenDropped_4() { return &___beenDropped_4; }
	inline void set_beenDropped_4(bool value)
	{
		___beenDropped_4 = value;
	}

	inline static int32_t get_offset_of_col_5() { return static_cast<int32_t>(offsetof(PuzzlePiece_t1912707670, ___col_5)); }
	inline Collider2D_t646061738 * get_col_5() const { return ___col_5; }
	inline Collider2D_t646061738 ** get_address_of_col_5() { return &___col_5; }
	inline void set_col_5(Collider2D_t646061738 * value)
	{
		___col_5 = value;
		Il2CppCodeGenWriteBarrier(&___col_5, value);
	}

	inline static int32_t get_offset_of_overlapCollider_6() { return static_cast<int32_t>(offsetof(PuzzlePiece_t1912707670, ___overlapCollider_6)); }
	inline Collider2D_t646061738 * get_overlapCollider_6() const { return ___overlapCollider_6; }
	inline Collider2D_t646061738 ** get_address_of_overlapCollider_6() { return &___overlapCollider_6; }
	inline void set_overlapCollider_6(Collider2D_t646061738 * value)
	{
		___overlapCollider_6 = value;
		Il2CppCodeGenWriteBarrier(&___overlapCollider_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
