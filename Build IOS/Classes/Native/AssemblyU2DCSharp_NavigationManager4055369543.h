﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_DDManagerBase_1_gen179329433.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NavigationManager
struct  NavigationManager_t4055369543  : public DDManagerBase_1_t179329433
{
public:
	// System.Int32 NavigationManager::mainInterfaceBuildIndex
	int32_t ___mainInterfaceBuildIndex_4;
	// System.Int32 NavigationManager::yearNumberStart
	int32_t ___yearNumberStart_5;
	// UnityEngine.GameObject NavigationManager::endScreen
	GameObject_t1756533147 * ___endScreen_6;

public:
	inline static int32_t get_offset_of_mainInterfaceBuildIndex_4() { return static_cast<int32_t>(offsetof(NavigationManager_t4055369543, ___mainInterfaceBuildIndex_4)); }
	inline int32_t get_mainInterfaceBuildIndex_4() const { return ___mainInterfaceBuildIndex_4; }
	inline int32_t* get_address_of_mainInterfaceBuildIndex_4() { return &___mainInterfaceBuildIndex_4; }
	inline void set_mainInterfaceBuildIndex_4(int32_t value)
	{
		___mainInterfaceBuildIndex_4 = value;
	}

	inline static int32_t get_offset_of_yearNumberStart_5() { return static_cast<int32_t>(offsetof(NavigationManager_t4055369543, ___yearNumberStart_5)); }
	inline int32_t get_yearNumberStart_5() const { return ___yearNumberStart_5; }
	inline int32_t* get_address_of_yearNumberStart_5() { return &___yearNumberStart_5; }
	inline void set_yearNumberStart_5(int32_t value)
	{
		___yearNumberStart_5 = value;
	}

	inline static int32_t get_offset_of_endScreen_6() { return static_cast<int32_t>(offsetof(NavigationManager_t4055369543, ___endScreen_6)); }
	inline GameObject_t1756533147 * get_endScreen_6() const { return ___endScreen_6; }
	inline GameObject_t1756533147 ** get_address_of_endScreen_6() { return &___endScreen_6; }
	inline void set_endScreen_6(GameObject_t1756533147 * value)
	{
		___endScreen_6 = value;
		Il2CppCodeGenWriteBarrier(&___endScreen_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
