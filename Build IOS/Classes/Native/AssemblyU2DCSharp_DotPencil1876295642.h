﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DotPencil
struct  DotPencil_t1876295642  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean DotPencil::toMove
	bool ___toMove_2;

public:
	inline static int32_t get_offset_of_toMove_2() { return static_cast<int32_t>(offsetof(DotPencil_t1876295642, ___toMove_2)); }
	inline bool get_toMove_2() const { return ___toMove_2; }
	inline bool* get_address_of_toMove_2() { return &___toMove_2; }
	inline void set_toMove_2(bool value)
	{
		___toMove_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
