﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Characters.FlappyChar
struct  FlappyChar_t3600499100  : public MonoBehaviour_t1158329972
{
public:
	// System.String[] GameDevToolkit.Characters.FlappyChar::hitExceptionScriptNames
	StringU5BU5D_t1642385972* ___hitExceptionScriptNames_2;
	// System.Single GameDevToolkit.Characters.FlappyChar::forceToAdd
	float ___forceToAdd_3;
	// System.Single GameDevToolkit.Characters.FlappyChar::speed
	float ___speed_4;
	// System.Boolean GameDevToolkit.Characters.FlappyChar::dead
	bool ___dead_5;
	// UnityEngine.Rigidbody2D GameDevToolkit.Characters.FlappyChar::body
	Rigidbody2D_t502193897 * ___body_6;

public:
	inline static int32_t get_offset_of_hitExceptionScriptNames_2() { return static_cast<int32_t>(offsetof(FlappyChar_t3600499100, ___hitExceptionScriptNames_2)); }
	inline StringU5BU5D_t1642385972* get_hitExceptionScriptNames_2() const { return ___hitExceptionScriptNames_2; }
	inline StringU5BU5D_t1642385972** get_address_of_hitExceptionScriptNames_2() { return &___hitExceptionScriptNames_2; }
	inline void set_hitExceptionScriptNames_2(StringU5BU5D_t1642385972* value)
	{
		___hitExceptionScriptNames_2 = value;
		Il2CppCodeGenWriteBarrier(&___hitExceptionScriptNames_2, value);
	}

	inline static int32_t get_offset_of_forceToAdd_3() { return static_cast<int32_t>(offsetof(FlappyChar_t3600499100, ___forceToAdd_3)); }
	inline float get_forceToAdd_3() const { return ___forceToAdd_3; }
	inline float* get_address_of_forceToAdd_3() { return &___forceToAdd_3; }
	inline void set_forceToAdd_3(float value)
	{
		___forceToAdd_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(FlappyChar_t3600499100, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_dead_5() { return static_cast<int32_t>(offsetof(FlappyChar_t3600499100, ___dead_5)); }
	inline bool get_dead_5() const { return ___dead_5; }
	inline bool* get_address_of_dead_5() { return &___dead_5; }
	inline void set_dead_5(bool value)
	{
		___dead_5 = value;
	}

	inline static int32_t get_offset_of_body_6() { return static_cast<int32_t>(offsetof(FlappyChar_t3600499100, ___body_6)); }
	inline Rigidbody2D_t502193897 * get_body_6() const { return ___body_6; }
	inline Rigidbody2D_t502193897 ** get_address_of_body_6() { return &___body_6; }
	inline void set_body_6(Rigidbody2D_t502193897 * value)
	{
		___body_6 = value;
		Il2CppCodeGenWriteBarrier(&___body_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
