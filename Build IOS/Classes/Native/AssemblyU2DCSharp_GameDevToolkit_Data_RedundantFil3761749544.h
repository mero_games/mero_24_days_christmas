﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<GameDevToolkit.Data.RedundantFileWriter/DelayedWriteReference>
struct List_1_t302577030;
// GameDevToolkit.Data.RedundantFileWriter
struct RedundantFileWriter_t3761749544;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Data.RedundantFileWriter
struct  RedundantFileWriter_t3761749544  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<GameDevToolkit.Data.RedundantFileWriter/DelayedWriteReference> GameDevToolkit.Data.RedundantFileWriter::writes
	List_1_t302577030 * ___writes_2;

public:
	inline static int32_t get_offset_of_writes_2() { return static_cast<int32_t>(offsetof(RedundantFileWriter_t3761749544, ___writes_2)); }
	inline List_1_t302577030 * get_writes_2() const { return ___writes_2; }
	inline List_1_t302577030 ** get_address_of_writes_2() { return &___writes_2; }
	inline void set_writes_2(List_1_t302577030 * value)
	{
		___writes_2 = value;
		Il2CppCodeGenWriteBarrier(&___writes_2, value);
	}
};

struct RedundantFileWriter_t3761749544_StaticFields
{
public:
	// GameDevToolkit.Data.RedundantFileWriter GameDevToolkit.Data.RedundantFileWriter::instance
	RedundantFileWriter_t3761749544 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(RedundantFileWriter_t3761749544_StaticFields, ___instance_3)); }
	inline RedundantFileWriter_t3761749544 * get_instance_3() const { return ___instance_3; }
	inline RedundantFileWriter_t3761749544 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(RedundantFileWriter_t3761749544 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
