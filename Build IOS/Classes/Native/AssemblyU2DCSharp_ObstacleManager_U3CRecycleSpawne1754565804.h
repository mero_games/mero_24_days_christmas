﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;
// ObstacleManager/<RecycleSpawnedObstacle>c__Iterator0/<RecycleSpawnedObstacle>c__AnonStorey1
struct U3CRecycleSpawnedObstacleU3Ec__AnonStorey1_t840817611;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObstacleManager/<RecycleSpawnedObstacle>c__Iterator0
struct  U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804  : public Il2CppObject
{
public:
	// UnityEngine.GameObject ObstacleManager/<RecycleSpawnedObstacle>c__Iterator0::obj
	GameObject_t1756533147 * ___obj_0;
	// System.Object ObstacleManager/<RecycleSpawnedObstacle>c__Iterator0::$current
	Il2CppObject * ___U24current_1;
	// System.Boolean ObstacleManager/<RecycleSpawnedObstacle>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ObstacleManager/<RecycleSpawnedObstacle>c__Iterator0::$PC
	int32_t ___U24PC_3;
	// ObstacleManager/<RecycleSpawnedObstacle>c__Iterator0/<RecycleSpawnedObstacle>c__AnonStorey1 ObstacleManager/<RecycleSpawnedObstacle>c__Iterator0::$locvar0
	U3CRecycleSpawnedObstacleU3Ec__AnonStorey1_t840817611 * ___U24locvar0_4;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804, ___obj_0)); }
	inline GameObject_t1756533147 * get_obj_0() const { return ___obj_0; }
	inline GameObject_t1756533147 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(GameObject_t1756533147 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier(&___obj_0, value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804, ___U24current_1)); }
	inline Il2CppObject * get_U24current_1() const { return ___U24current_1; }
	inline Il2CppObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(Il2CppObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_1, value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_4() { return static_cast<int32_t>(offsetof(U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804, ___U24locvar0_4)); }
	inline U3CRecycleSpawnedObstacleU3Ec__AnonStorey1_t840817611 * get_U24locvar0_4() const { return ___U24locvar0_4; }
	inline U3CRecycleSpawnedObstacleU3Ec__AnonStorey1_t840817611 ** get_address_of_U24locvar0_4() { return &___U24locvar0_4; }
	inline void set_U24locvar0_4(U3CRecycleSpawnedObstacleU3Ec__AnonStorey1_t840817611 * value)
	{
		___U24locvar0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
