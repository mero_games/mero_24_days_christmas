﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Light494725636.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Mathf2336485820.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel3331827198.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2178520045.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"
#include "UnityEngine_UnityEngine_NetworkViewID3942988548.h"
#include "UnityEngine_UnityEngine_NetworkView172525251.h"
#include "UnityEngine_UnityEngine_BitStream1979465639.h"
#include "UnityEngine_UnityEngine_RPC3323229423.h"
#include "UnityEngine_UnityEngine_HostData3480691970.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo614064059.h"
#include "UnityEngine_UnityEngine_Random1170710517.h"
#include "UnityEngine_UnityEngine_ResourceRequest2560315377.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_SortingLayer221838959.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility4181890362.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_Time31991979.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "UnityEngine_UnityEngine_WWWTranscoder1124214756.h"
#include "UnityEngine_UnityEngine_YieldInstruction3462875981.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3250302433.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play1502856514.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Scri4067966717.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier259698391.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit4134400622.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification317971878.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification2254252895.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Script4271526.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator1251553160.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties2488747555.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker154385424.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive2020713228.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge3306019089.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis3420330537.h"
#include "UnityEngine_UnityEngine_Rendering_SortingGroup3832012067.h"
#include "UnityEngine_UnityEngine_ParticleSystemCurveMode1659312557.h"
#include "UnityEngine_UnityEngine_ParticleSystemStopBehavior3921148531.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MinMaxCurve122563058.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MainModule6751348.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel2419492168.h"
#include "UnityEngine_UnityEngine_ParticleSystem_U3CStopU3Ec1810852071.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_Physics634932869.h"
#include "UnityEngine_UnityEngine_ContactPoint1376425630.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467.h"
#include "UnityEngine_UnityEngine_ForceMode2D4177575466.h"
#include "UnityEngine_UnityEngine_RigidbodyType2D1116729369.h"
#include "UnityEngine_UnityEngine_ContactFilter2D1672660996.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_CircleCollider2D13116344.h"
#include "UnityEngine_UnityEngine_BoxCollider2D948534547.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3659330976.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_Effector2D2551982566.h"
#include "UnityEngine_UnityEngine_PlatformEffector2D3299381139.h"
#include "UnityEngine_UnityEngine_AudioSettings3144015719.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu3743753033.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac3007145346.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCal421863554.h"
#include "UnityEngine_UnityEngine_FFTWindow2870052902.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1565472209.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour2151245329.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3560017945.h"
#include "UnityEngine_UnityEngine_AnimationEvent2428323300.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (LayerMask_t3188175821)+ sizeof (Il2CppObject), sizeof(LayerMask_t3188175821 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1300[1] = 
{
	LayerMask_t3188175821::get_offset_of_m_Mask_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (Light_t494725636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (Vector3_t2243707580)+ sizeof (Il2CppObject), sizeof(Vector3_t2243707580 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1302[4] = 
{
	0,
	Vector3_t2243707580::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t2243707580::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t2243707580::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (Quaternion_t4030073918)+ sizeof (Il2CppObject), sizeof(Quaternion_t4030073918 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1303[5] = 
{
	Quaternion_t4030073918::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t4030073918::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t4030073918::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t4030073918::get_offset_of_w_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (Matrix4x4_t2933234003)+ sizeof (Il2CppObject), sizeof(Matrix4x4_t2933234003 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1304[16] = 
{
	Matrix4x4_t2933234003::get_offset_of_m00_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m10_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m20_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m30_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m01_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m11_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m21_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m31_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m02_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m12_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m22_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m32_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m03_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m13_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m23_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m33_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (Bounds_t3033363703)+ sizeof (Il2CppObject), sizeof(Bounds_t3033363703 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1305[2] = 
{
	Bounds_t3033363703::get_offset_of_m_Center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Bounds_t3033363703::get_offset_of_m_Extents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (Mathf_t2336485820)+ sizeof (Il2CppObject), sizeof(Mathf_t2336485820 ), sizeof(Mathf_t2336485820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1306[1] = 
{
	Mathf_t2336485820_StaticFields::get_offset_of_Epsilon_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (Keyframe_t1449471340)+ sizeof (Il2CppObject), sizeof(Keyframe_t1449471340 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1307[4] = 
{
	Keyframe_t1449471340::get_offset_of_m_Time_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t1449471340::get_offset_of_m_Value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t1449471340::get_offset_of_m_InTangent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t1449471340::get_offset_of_m_OutTangent_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (AnimationCurve_t3306541151), sizeof(AnimationCurve_t3306541151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1308[1] = 
{
	AnimationCurve_t3306541151::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (Mesh_t1356156583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (InternalShaderChannel_t3331827198)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1310[9] = 
{
	InternalShaderChannel_t3331827198::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (InternalVertexChannelType_t2178520045)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1311[3] = 
{
	InternalVertexChannelType_t2178520045::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (MonoBehaviour_t1158329972), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (NetworkPlayer_t1243528291)+ sizeof (Il2CppObject), sizeof(NetworkPlayer_t1243528291 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1313[1] = 
{
	NetworkPlayer_t1243528291::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (NetworkViewID_t3942988548)+ sizeof (Il2CppObject), sizeof(NetworkViewID_t3942988548 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1314[3] = 
{
	NetworkViewID_t3942988548::get_offset_of_a_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkViewID_t3942988548::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkViewID_t3942988548::get_offset_of_c_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (NetworkView_t172525251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (BitStream_t1979465639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1316[1] = 
{
	BitStream_t1979465639::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (RPC_t3323229423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (HostData_t3480691970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1318[10] = 
{
	HostData_t3480691970::get_offset_of_m_Nat_0(),
	HostData_t3480691970::get_offset_of_m_GameType_1(),
	HostData_t3480691970::get_offset_of_m_GameName_2(),
	HostData_t3480691970::get_offset_of_m_ConnectedPlayers_3(),
	HostData_t3480691970::get_offset_of_m_PlayerLimit_4(),
	HostData_t3480691970::get_offset_of_m_IP_5(),
	HostData_t3480691970::get_offset_of_m_Port_6(),
	HostData_t3480691970::get_offset_of_m_PasswordProtected_7(),
	HostData_t3480691970::get_offset_of_m_Comment_8(),
	HostData_t3480691970::get_offset_of_m_GUID_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (NetworkMessageInfo_t614064059)+ sizeof (Il2CppObject), sizeof(NetworkMessageInfo_t614064059 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1319[3] = 
{
	NetworkMessageInfo_t614064059::get_offset_of_m_TimeStamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkMessageInfo_t614064059::get_offset_of_m_Sender_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkMessageInfo_t614064059::get_offset_of_m_ViewID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (Random_t1170710517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (ResourceRequest_t2560315377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1321[2] = 
{
	ResourceRequest_t2560315377::get_offset_of_m_Path_1(),
	ResourceRequest_t2560315377::get_offset_of_m_Type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (Resources_t339470017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (Shader_t2430389951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (Material_t193706927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (SortingLayer_t221838959)+ sizeof (Il2CppObject), sizeof(SortingLayer_t221838959 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1325[1] = 
{
	SortingLayer_t221838959::get_offset_of_m_Id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (Sprite_t309593783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (SpriteRenderer_t1209076198), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (DataUtility_t4181890362), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (Texture_t2243626319), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (Texture2D_t3542995729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (RenderTexture_t2666733923), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (Time_t31991979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (HideFlags_t1434274199)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1333[10] = 
{
	HideFlags_t1434274199::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (Object_t1021602117), sizeof(Object_t1021602117_marshaled_pinvoke), sizeof(Object_t1021602117_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1334[2] = 
{
	Object_t1021602117::get_offset_of_m_CachedPtr_0(),
	Object_t1021602117_StaticFields::get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (WWW_t2919945039), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (WWWForm_t3950226929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1336[6] = 
{
	WWWForm_t3950226929::get_offset_of_formData_0(),
	WWWForm_t3950226929::get_offset_of_fieldNames_1(),
	WWWForm_t3950226929::get_offset_of_fileNames_2(),
	WWWForm_t3950226929::get_offset_of_types_3(),
	WWWForm_t3950226929::get_offset_of_boundary_4(),
	WWWForm_t3950226929::get_offset_of_containsFiles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (WWWTranscoder_t1124214756), -1, sizeof(WWWTranscoder_t1124214756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1337[8] = 
{
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_ucHexChars_0(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_lcHexChars_1(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_urlEscapeChar_2(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_urlSpace_3(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_urlForbidden_4(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_qpEscapeChar_5(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_qpSpace_6(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_qpForbidden_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (YieldInstruction_t3462875981), sizeof(YieldInstruction_t3462875981_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (PlayState_t3250302433)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1339[3] = 
{
	PlayState_t3250302433::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (PlayableHandle_t1502856514)+ sizeof (Il2CppObject), sizeof(PlayableHandle_t1502856514 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1340[2] = 
{
	PlayableHandle_t1502856514::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayableHandle_t1502856514::get_offset_of_m_Version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (Playable_t3667545548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1341[1] = 
{
	Playable_t3667545548::get_offset_of_handle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (ScriptPlayable_t4067966717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (CalendarIdentifier_t259698391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1343[12] = 
{
	CalendarIdentifier_t259698391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (CalendarUnit_t4134400622)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1344[12] = 
{
	CalendarUnit_t4134400622::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (LocalNotification_t317971878), -1, sizeof(LocalNotification_t317971878_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1345[2] = 
{
	LocalNotification_t317971878::get_offset_of_notificationWrapper_0(),
	LocalNotification_t317971878_StaticFields::get_offset_of_m_NSReferenceDateTicks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (RemoteNotification_t2254252895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1346[1] = 
{
	RemoteNotification_t2254252895::get_offset_of_notificationWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (Scene_t1684909666)+ sizeof (Il2CppObject), sizeof(Scene_t1684909666 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1347[1] = 
{
	Scene_t1684909666::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (LoadSceneMode_t2981886439)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1348[3] = 
{
	LoadSceneMode_t2981886439::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (SceneManager_t90660965), -1, sizeof(SceneManager_t90660965_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1349[3] = 
{
	SceneManager_t90660965_StaticFields::get_offset_of_sceneLoaded_0(),
	SceneManager_t90660965_StaticFields::get_offset_of_sceneUnloaded_1(),
	SceneManager_t90660965_StaticFields::get_offset_of_activeSceneChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (ScriptableRenderContext_t4271526)+ sizeof (Il2CppObject), sizeof(ScriptableRenderContext_t4271526 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1350[1] = 
{
	ScriptableRenderContext_t4271526::get_offset_of_m_Ptr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (Transform_t3275118058), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (Enumerator_t1251553160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1352[2] = 
{
	Enumerator_t1251553160::get_offset_of_outer_0(),
	Enumerator_t1251553160::get_offset_of_currentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (DrivenTransformProperties_t2488747555)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1353[26] = 
{
	DrivenTransformProperties_t2488747555::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (DrivenRectTransformTracker_t154385424)+ sizeof (Il2CppObject), sizeof(DrivenRectTransformTracker_t154385424 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (RectTransform_t3349966182), -1, sizeof(RectTransform_t3349966182_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1355[1] = 
{
	RectTransform_t3349966182_StaticFields::get_offset_of_reapplyDrivenProperties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (ReapplyDrivenProperties_t2020713228), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (Edge_t3306019089)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1357[5] = 
{
	Edge_t3306019089::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (Axis_t3420330537)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1358[3] = 
{
	Axis_t3420330537::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (SortingGroup_t3832012067), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (ParticleSystemCurveMode_t1659312557)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1360[5] = 
{
	ParticleSystemCurveMode_t1659312557::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (ParticleSystemStopBehavior_t3921148531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1361[3] = 
{
	ParticleSystemStopBehavior_t3921148531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (ParticleSystem_t3394631041), -1, sizeof(ParticleSystem_t3394631041_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1362[4] = 
{
	ParticleSystem_t3394631041_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	ParticleSystem_t3394631041_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	ParticleSystem_t3394631041_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ParticleSystem_t3394631041_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (MinMaxCurve_t122563058)+ sizeof (Il2CppObject), sizeof(MinMaxCurve_t122563058_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1363[6] = 
{
	MinMaxCurve_t122563058::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MinMaxCurve_t122563058::get_offset_of_m_CurveMultiplier_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MinMaxCurve_t122563058::get_offset_of_m_CurveMin_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MinMaxCurve_t122563058::get_offset_of_m_CurveMax_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MinMaxCurve_t122563058::get_offset_of_m_ConstantMin_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MinMaxCurve_t122563058::get_offset_of_m_ConstantMax_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (MainModule_t6751348)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1364[1] = 
{
	MainModule_t6751348::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (IteratorDelegate_t2419492168), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (U3CStopU3Ec__AnonStorey1_t1810852071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1366[1] = 
{
	U3CStopU3Ec__AnonStorey1_t1810852071::get_offset_of_stopBehavior_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (ControllerColliderHit_t4070855101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1367[7] = 
{
	ControllerColliderHit_t4070855101::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Point_2(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t4070855101::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t4070855101::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (Collision_t2876846408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1368[5] = 
{
	Collision_t2876846408::get_offset_of_m_Impulse_0(),
	Collision_t2876846408::get_offset_of_m_RelativeVelocity_1(),
	Collision_t2876846408::get_offset_of_m_Rigidbody_2(),
	Collision_t2876846408::get_offset_of_m_Collider_3(),
	Collision_t2876846408::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (QueryTriggerInteraction_t478029726)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1369[4] = 
{
	QueryTriggerInteraction_t478029726::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (Physics_t634932869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (ContactPoint_t1376425630)+ sizeof (Il2CppObject), sizeof(ContactPoint_t1376425630 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1371[5] = 
{
	ContactPoint_t1376425630::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_Separation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (Rigidbody_t4233889191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (Collider_t3497673348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (RaycastHit_t87180320)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1374[6] = 
{
	RaycastHit_t87180320::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (CharacterController_t4094781467), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { sizeof (RaycastHit2D_t4063908774)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1376[6] = 
{
	RaycastHit2D_t4063908774::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { sizeof (Physics2D_t2540166467), -1, sizeof(Physics2D_t2540166467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1377[1] = 
{
	Physics2D_t2540166467_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (ForceMode2D_t4177575466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1378[3] = 
{
	ForceMode2D_t4177575466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { sizeof (RigidbodyType2D_t1116729369)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1379[4] = 
{
	RigidbodyType2D_t1116729369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (ContactFilter2D_t1672660996)+ sizeof (Il2CppObject), sizeof(ContactFilter2D_t1672660996_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1380[11] = 
{
	ContactFilter2D_t1672660996::get_offset_of_useTriggers_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_useLayerMask_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_useDepth_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_useOutsideDepth_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_useNormalAngle_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_useOutsideNormalAngle_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_layerMask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_minDepth_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_maxDepth_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_minNormalAngle_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t1672660996::get_offset_of_maxNormalAngle_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (Rigidbody2D_t502193897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (Collider2D_t646061738), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (CircleCollider2D_t13116344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (BoxCollider2D_t948534547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (ContactPoint2D_t3659330976)+ sizeof (Il2CppObject), sizeof(ContactPoint2D_t3659330976 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1385[11] = 
{
	ContactPoint2D_t3659330976::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_RelativeVelocity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Separation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_NormalImpulse_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_TangentImpulse_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Collider_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_OtherCollider_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Rigidbody_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_OtherRigidbody_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Enabled_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (Collision2D_t1539500754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1386[7] = 
{
	Collision2D_t1539500754::get_offset_of_m_Collider_0(),
	Collision2D_t1539500754::get_offset_of_m_OtherCollider_1(),
	Collision2D_t1539500754::get_offset_of_m_Rigidbody_2(),
	Collision2D_t1539500754::get_offset_of_m_OtherRigidbody_3(),
	Collision2D_t1539500754::get_offset_of_m_Contacts_4(),
	Collision2D_t1539500754::get_offset_of_m_RelativeVelocity_5(),
	Collision2D_t1539500754::get_offset_of_m_Enabled_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (Effector2D_t2551982566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (PlatformEffector2D_t3299381139), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (AudioSettings_t3144015719), -1, sizeof(AudioSettings_t3144015719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1389[1] = 
{
	AudioSettings_t3144015719_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (AudioConfigurationChangeHandler_t3743753033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (AudioClip_t1932558630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1391[2] = 
{
	AudioClip_t1932558630::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t1932558630::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (PCMReaderCallback_t3007145346), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (PCMSetPositionCallback_t421863554), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (FFTWindow_t2870052902)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1394[7] = 
{
	FFTWindow_t2870052902::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (AudioSource_t1135106623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (SharedBetweenAnimatorsAttribute_t1565472209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (StateMachineBehaviour_t2151245329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (AnimationEventSource_t3560017945)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1398[4] = 
{
	AnimationEventSource_t3560017945::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (AnimationEvent_t2428323300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1399[11] = 
{
	AnimationEvent_t2428323300::get_offset_of_m_Time_0(),
	AnimationEvent_t2428323300::get_offset_of_m_FunctionName_1(),
	AnimationEvent_t2428323300::get_offset_of_m_StringParameter_2(),
	AnimationEvent_t2428323300::get_offset_of_m_ObjectReferenceParameter_3(),
	AnimationEvent_t2428323300::get_offset_of_m_FloatParameter_4(),
	AnimationEvent_t2428323300::get_offset_of_m_IntParameter_5(),
	AnimationEvent_t2428323300::get_offset_of_m_MessageOptions_6(),
	AnimationEvent_t2428323300::get_offset_of_m_Source_7(),
	AnimationEvent_t2428323300::get_offset_of_m_StateSender_8(),
	AnimationEvent_t2428323300::get_offset_of_m_AnimatorStateInfo_9(),
	AnimationEvent_t2428323300::get_offset_of_m_AnimatorClipInfo_10(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
