﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen3653276305.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSixteenManager
struct  GameSixteenManager_t358793919  : public SingletonBase_1_t3653276305
{
public:
	// UnityEngine.Transform GameSixteenManager::endScreenContainer
	Transform_t3275118058 * ___endScreenContainer_4;
	// UnityEngine.GameObject GameSixteenManager::gridPrefab
	GameObject_t1756533147 * ___gridPrefab_5;
	// System.Int32 GameSixteenManager::gridWidth
	int32_t ___gridWidth_6;
	// System.Int32 GameSixteenManager::gridHeight
	int32_t ___gridHeight_7;
	// UnityEngine.Vector2 GameSixteenManager::gridSquareSize
	Vector2_t2243707579  ___gridSquareSize_8;

public:
	inline static int32_t get_offset_of_endScreenContainer_4() { return static_cast<int32_t>(offsetof(GameSixteenManager_t358793919, ___endScreenContainer_4)); }
	inline Transform_t3275118058 * get_endScreenContainer_4() const { return ___endScreenContainer_4; }
	inline Transform_t3275118058 ** get_address_of_endScreenContainer_4() { return &___endScreenContainer_4; }
	inline void set_endScreenContainer_4(Transform_t3275118058 * value)
	{
		___endScreenContainer_4 = value;
		Il2CppCodeGenWriteBarrier(&___endScreenContainer_4, value);
	}

	inline static int32_t get_offset_of_gridPrefab_5() { return static_cast<int32_t>(offsetof(GameSixteenManager_t358793919, ___gridPrefab_5)); }
	inline GameObject_t1756533147 * get_gridPrefab_5() const { return ___gridPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_gridPrefab_5() { return &___gridPrefab_5; }
	inline void set_gridPrefab_5(GameObject_t1756533147 * value)
	{
		___gridPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___gridPrefab_5, value);
	}

	inline static int32_t get_offset_of_gridWidth_6() { return static_cast<int32_t>(offsetof(GameSixteenManager_t358793919, ___gridWidth_6)); }
	inline int32_t get_gridWidth_6() const { return ___gridWidth_6; }
	inline int32_t* get_address_of_gridWidth_6() { return &___gridWidth_6; }
	inline void set_gridWidth_6(int32_t value)
	{
		___gridWidth_6 = value;
	}

	inline static int32_t get_offset_of_gridHeight_7() { return static_cast<int32_t>(offsetof(GameSixteenManager_t358793919, ___gridHeight_7)); }
	inline int32_t get_gridHeight_7() const { return ___gridHeight_7; }
	inline int32_t* get_address_of_gridHeight_7() { return &___gridHeight_7; }
	inline void set_gridHeight_7(int32_t value)
	{
		___gridHeight_7 = value;
	}

	inline static int32_t get_offset_of_gridSquareSize_8() { return static_cast<int32_t>(offsetof(GameSixteenManager_t358793919, ___gridSquareSize_8)); }
	inline Vector2_t2243707579  get_gridSquareSize_8() const { return ___gridSquareSize_8; }
	inline Vector2_t2243707579 * get_address_of_gridSquareSize_8() { return &___gridSquareSize_8; }
	inline void set_gridSquareSize_8(Vector2_t2243707579  value)
	{
		___gridSquareSize_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
