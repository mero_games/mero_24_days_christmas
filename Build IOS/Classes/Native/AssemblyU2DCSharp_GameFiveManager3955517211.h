﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen2955032301.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// Cleaner
struct Cleaner_t1604682602;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameFiveManager
struct  GameFiveManager_t3955517211  : public SingletonBase_1_t2955032301
{
public:
	// System.Boolean GameFiveManager::isPlaying
	bool ___isPlaying_4;
	// UnityEngine.Color[] GameFiveManager::bootColorsMale
	ColorU5BU5D_t672350442* ___bootColorsMale_5;
	// UnityEngine.Color[] GameFiveManager::bootColorsFemale
	ColorU5BU5D_t672350442* ___bootColorsFemale_6;
	// UnityEngine.SpriteRenderer GameFiveManager::femaleBoot
	SpriteRenderer_t1209076198 * ___femaleBoot_7;
	// UnityEngine.SpriteRenderer GameFiveManager::maleBoot
	SpriteRenderer_t1209076198 * ___maleBoot_8;
	// System.Int32 GameFiveManager::numberOfDirtFlecks
	int32_t ___numberOfDirtFlecks_9;
	// System.Int32 GameFiveManager::dirtFlecksCleaned
	int32_t ___dirtFlecksCleaned_10;
	// Cleaner GameFiveManager::cleaner
	Cleaner_t1604682602 * ___cleaner_11;
	// Cleaner GameFiveManager::iCleaner
	Cleaner_t1604682602 * ___iCleaner_12;
	// UnityEngine.Vector3 GameFiveManager::lastMousePos
	Vector3_t2243707580  ___lastMousePos_13;

public:
	inline static int32_t get_offset_of_isPlaying_4() { return static_cast<int32_t>(offsetof(GameFiveManager_t3955517211, ___isPlaying_4)); }
	inline bool get_isPlaying_4() const { return ___isPlaying_4; }
	inline bool* get_address_of_isPlaying_4() { return &___isPlaying_4; }
	inline void set_isPlaying_4(bool value)
	{
		___isPlaying_4 = value;
	}

	inline static int32_t get_offset_of_bootColorsMale_5() { return static_cast<int32_t>(offsetof(GameFiveManager_t3955517211, ___bootColorsMale_5)); }
	inline ColorU5BU5D_t672350442* get_bootColorsMale_5() const { return ___bootColorsMale_5; }
	inline ColorU5BU5D_t672350442** get_address_of_bootColorsMale_5() { return &___bootColorsMale_5; }
	inline void set_bootColorsMale_5(ColorU5BU5D_t672350442* value)
	{
		___bootColorsMale_5 = value;
		Il2CppCodeGenWriteBarrier(&___bootColorsMale_5, value);
	}

	inline static int32_t get_offset_of_bootColorsFemale_6() { return static_cast<int32_t>(offsetof(GameFiveManager_t3955517211, ___bootColorsFemale_6)); }
	inline ColorU5BU5D_t672350442* get_bootColorsFemale_6() const { return ___bootColorsFemale_6; }
	inline ColorU5BU5D_t672350442** get_address_of_bootColorsFemale_6() { return &___bootColorsFemale_6; }
	inline void set_bootColorsFemale_6(ColorU5BU5D_t672350442* value)
	{
		___bootColorsFemale_6 = value;
		Il2CppCodeGenWriteBarrier(&___bootColorsFemale_6, value);
	}

	inline static int32_t get_offset_of_femaleBoot_7() { return static_cast<int32_t>(offsetof(GameFiveManager_t3955517211, ___femaleBoot_7)); }
	inline SpriteRenderer_t1209076198 * get_femaleBoot_7() const { return ___femaleBoot_7; }
	inline SpriteRenderer_t1209076198 ** get_address_of_femaleBoot_7() { return &___femaleBoot_7; }
	inline void set_femaleBoot_7(SpriteRenderer_t1209076198 * value)
	{
		___femaleBoot_7 = value;
		Il2CppCodeGenWriteBarrier(&___femaleBoot_7, value);
	}

	inline static int32_t get_offset_of_maleBoot_8() { return static_cast<int32_t>(offsetof(GameFiveManager_t3955517211, ___maleBoot_8)); }
	inline SpriteRenderer_t1209076198 * get_maleBoot_8() const { return ___maleBoot_8; }
	inline SpriteRenderer_t1209076198 ** get_address_of_maleBoot_8() { return &___maleBoot_8; }
	inline void set_maleBoot_8(SpriteRenderer_t1209076198 * value)
	{
		___maleBoot_8 = value;
		Il2CppCodeGenWriteBarrier(&___maleBoot_8, value);
	}

	inline static int32_t get_offset_of_numberOfDirtFlecks_9() { return static_cast<int32_t>(offsetof(GameFiveManager_t3955517211, ___numberOfDirtFlecks_9)); }
	inline int32_t get_numberOfDirtFlecks_9() const { return ___numberOfDirtFlecks_9; }
	inline int32_t* get_address_of_numberOfDirtFlecks_9() { return &___numberOfDirtFlecks_9; }
	inline void set_numberOfDirtFlecks_9(int32_t value)
	{
		___numberOfDirtFlecks_9 = value;
	}

	inline static int32_t get_offset_of_dirtFlecksCleaned_10() { return static_cast<int32_t>(offsetof(GameFiveManager_t3955517211, ___dirtFlecksCleaned_10)); }
	inline int32_t get_dirtFlecksCleaned_10() const { return ___dirtFlecksCleaned_10; }
	inline int32_t* get_address_of_dirtFlecksCleaned_10() { return &___dirtFlecksCleaned_10; }
	inline void set_dirtFlecksCleaned_10(int32_t value)
	{
		___dirtFlecksCleaned_10 = value;
	}

	inline static int32_t get_offset_of_cleaner_11() { return static_cast<int32_t>(offsetof(GameFiveManager_t3955517211, ___cleaner_11)); }
	inline Cleaner_t1604682602 * get_cleaner_11() const { return ___cleaner_11; }
	inline Cleaner_t1604682602 ** get_address_of_cleaner_11() { return &___cleaner_11; }
	inline void set_cleaner_11(Cleaner_t1604682602 * value)
	{
		___cleaner_11 = value;
		Il2CppCodeGenWriteBarrier(&___cleaner_11, value);
	}

	inline static int32_t get_offset_of_iCleaner_12() { return static_cast<int32_t>(offsetof(GameFiveManager_t3955517211, ___iCleaner_12)); }
	inline Cleaner_t1604682602 * get_iCleaner_12() const { return ___iCleaner_12; }
	inline Cleaner_t1604682602 ** get_address_of_iCleaner_12() { return &___iCleaner_12; }
	inline void set_iCleaner_12(Cleaner_t1604682602 * value)
	{
		___iCleaner_12 = value;
		Il2CppCodeGenWriteBarrier(&___iCleaner_12, value);
	}

	inline static int32_t get_offset_of_lastMousePos_13() { return static_cast<int32_t>(offsetof(GameFiveManager_t3955517211, ___lastMousePos_13)); }
	inline Vector3_t2243707580  get_lastMousePos_13() const { return ___lastMousePos_13; }
	inline Vector3_t2243707580 * get_address_of_lastMousePos_13() { return &___lastMousePos_13; }
	inline void set_lastMousePos_13(Vector3_t2243707580  value)
	{
		___lastMousePos_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
