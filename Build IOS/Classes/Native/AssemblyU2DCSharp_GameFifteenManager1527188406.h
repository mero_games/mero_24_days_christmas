﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen526703496.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"

// GameFifteenManager/LightColors[]
struct LightColorsU5BU5D_t983402922;
// UnityEngine.Transform
struct Transform_t3275118058;
// GameFifteenManager/LightPuzzle[]
struct LightPuzzleU5BU5D_t3060589208;
// GameFifteenManager/LightCompleted[]
struct LightCompletedU5BU5D_t3719918727;
// System.Collections.Generic.List`1<LightBulb>
struct List_1_t3367677205;
// LightBulb
struct LightBulb_t3998556073;
// System.Func`2<System.Int32,System.Single>
struct Func_2_t1628330325;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameFifteenManager
struct  GameFifteenManager_t1527188406  : public SingletonBase_1_t526703496
{
public:
	// System.Int32 GameFifteenManager::rounds
	int32_t ___rounds_4;
	// GameFifteenManager/LightColors[] GameFifteenManager::lights
	LightColorsU5BU5D_t983402922* ___lights_5;
	// UnityEngine.Color GameFifteenManager::offLight
	Color_t2020392075  ___offLight_6;
	// System.Single GameFifteenManager::roundDelay
	float ___roundDelay_7;
	// System.Single GameFifteenManager::waitTime
	float ___waitTime_8;
	// System.Single GameFifteenManager::newWaitTime
	float ___newWaitTime_9;
	// System.Single GameFifteenManager::speed
	float ___speed_10;
	// System.Single GameFifteenManager::newspeed
	float ___newspeed_11;
	// UnityEngine.Transform GameFifteenManager::lightsParent
	Transform_t3275118058 * ___lightsParent_12;
	// UnityEngine.LayerMask GameFifteenManager::mask
	LayerMask_t3188175821  ___mask_13;
	// GameFifteenManager/LightPuzzle[] GameFifteenManager::puzzle
	LightPuzzleU5BU5D_t3060589208* ___puzzle_14;
	// GameFifteenManager/LightCompleted[] GameFifteenManager::completed
	LightCompletedU5BU5D_t3719918727* ___completed_15;
	// System.Int32 GameFifteenManager::lines
	int32_t ___lines_16;
	// System.Boolean GameFifteenManager::canClick
	bool ___canClick_17;
	// System.Collections.Generic.List`1<LightBulb> GameFifteenManager::list
	List_1_t3367677205 * ___list_18;
	// LightBulb GameFifteenManager::curBulb
	LightBulb_t3998556073 * ___curBulb_19;
	// System.Int32 GameFifteenManager::curRound
	int32_t ___curRound_20;
	// System.Boolean GameFifteenManager::canGlow
	bool ___canGlow_21;

public:
	inline static int32_t get_offset_of_rounds_4() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___rounds_4)); }
	inline int32_t get_rounds_4() const { return ___rounds_4; }
	inline int32_t* get_address_of_rounds_4() { return &___rounds_4; }
	inline void set_rounds_4(int32_t value)
	{
		___rounds_4 = value;
	}

	inline static int32_t get_offset_of_lights_5() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___lights_5)); }
	inline LightColorsU5BU5D_t983402922* get_lights_5() const { return ___lights_5; }
	inline LightColorsU5BU5D_t983402922** get_address_of_lights_5() { return &___lights_5; }
	inline void set_lights_5(LightColorsU5BU5D_t983402922* value)
	{
		___lights_5 = value;
		Il2CppCodeGenWriteBarrier(&___lights_5, value);
	}

	inline static int32_t get_offset_of_offLight_6() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___offLight_6)); }
	inline Color_t2020392075  get_offLight_6() const { return ___offLight_6; }
	inline Color_t2020392075 * get_address_of_offLight_6() { return &___offLight_6; }
	inline void set_offLight_6(Color_t2020392075  value)
	{
		___offLight_6 = value;
	}

	inline static int32_t get_offset_of_roundDelay_7() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___roundDelay_7)); }
	inline float get_roundDelay_7() const { return ___roundDelay_7; }
	inline float* get_address_of_roundDelay_7() { return &___roundDelay_7; }
	inline void set_roundDelay_7(float value)
	{
		___roundDelay_7 = value;
	}

	inline static int32_t get_offset_of_waitTime_8() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___waitTime_8)); }
	inline float get_waitTime_8() const { return ___waitTime_8; }
	inline float* get_address_of_waitTime_8() { return &___waitTime_8; }
	inline void set_waitTime_8(float value)
	{
		___waitTime_8 = value;
	}

	inline static int32_t get_offset_of_newWaitTime_9() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___newWaitTime_9)); }
	inline float get_newWaitTime_9() const { return ___newWaitTime_9; }
	inline float* get_address_of_newWaitTime_9() { return &___newWaitTime_9; }
	inline void set_newWaitTime_9(float value)
	{
		___newWaitTime_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_newspeed_11() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___newspeed_11)); }
	inline float get_newspeed_11() const { return ___newspeed_11; }
	inline float* get_address_of_newspeed_11() { return &___newspeed_11; }
	inline void set_newspeed_11(float value)
	{
		___newspeed_11 = value;
	}

	inline static int32_t get_offset_of_lightsParent_12() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___lightsParent_12)); }
	inline Transform_t3275118058 * get_lightsParent_12() const { return ___lightsParent_12; }
	inline Transform_t3275118058 ** get_address_of_lightsParent_12() { return &___lightsParent_12; }
	inline void set_lightsParent_12(Transform_t3275118058 * value)
	{
		___lightsParent_12 = value;
		Il2CppCodeGenWriteBarrier(&___lightsParent_12, value);
	}

	inline static int32_t get_offset_of_mask_13() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___mask_13)); }
	inline LayerMask_t3188175821  get_mask_13() const { return ___mask_13; }
	inline LayerMask_t3188175821 * get_address_of_mask_13() { return &___mask_13; }
	inline void set_mask_13(LayerMask_t3188175821  value)
	{
		___mask_13 = value;
	}

	inline static int32_t get_offset_of_puzzle_14() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___puzzle_14)); }
	inline LightPuzzleU5BU5D_t3060589208* get_puzzle_14() const { return ___puzzle_14; }
	inline LightPuzzleU5BU5D_t3060589208** get_address_of_puzzle_14() { return &___puzzle_14; }
	inline void set_puzzle_14(LightPuzzleU5BU5D_t3060589208* value)
	{
		___puzzle_14 = value;
		Il2CppCodeGenWriteBarrier(&___puzzle_14, value);
	}

	inline static int32_t get_offset_of_completed_15() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___completed_15)); }
	inline LightCompletedU5BU5D_t3719918727* get_completed_15() const { return ___completed_15; }
	inline LightCompletedU5BU5D_t3719918727** get_address_of_completed_15() { return &___completed_15; }
	inline void set_completed_15(LightCompletedU5BU5D_t3719918727* value)
	{
		___completed_15 = value;
		Il2CppCodeGenWriteBarrier(&___completed_15, value);
	}

	inline static int32_t get_offset_of_lines_16() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___lines_16)); }
	inline int32_t get_lines_16() const { return ___lines_16; }
	inline int32_t* get_address_of_lines_16() { return &___lines_16; }
	inline void set_lines_16(int32_t value)
	{
		___lines_16 = value;
	}

	inline static int32_t get_offset_of_canClick_17() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___canClick_17)); }
	inline bool get_canClick_17() const { return ___canClick_17; }
	inline bool* get_address_of_canClick_17() { return &___canClick_17; }
	inline void set_canClick_17(bool value)
	{
		___canClick_17 = value;
	}

	inline static int32_t get_offset_of_list_18() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___list_18)); }
	inline List_1_t3367677205 * get_list_18() const { return ___list_18; }
	inline List_1_t3367677205 ** get_address_of_list_18() { return &___list_18; }
	inline void set_list_18(List_1_t3367677205 * value)
	{
		___list_18 = value;
		Il2CppCodeGenWriteBarrier(&___list_18, value);
	}

	inline static int32_t get_offset_of_curBulb_19() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___curBulb_19)); }
	inline LightBulb_t3998556073 * get_curBulb_19() const { return ___curBulb_19; }
	inline LightBulb_t3998556073 ** get_address_of_curBulb_19() { return &___curBulb_19; }
	inline void set_curBulb_19(LightBulb_t3998556073 * value)
	{
		___curBulb_19 = value;
		Il2CppCodeGenWriteBarrier(&___curBulb_19, value);
	}

	inline static int32_t get_offset_of_curRound_20() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___curRound_20)); }
	inline int32_t get_curRound_20() const { return ___curRound_20; }
	inline int32_t* get_address_of_curRound_20() { return &___curRound_20; }
	inline void set_curRound_20(int32_t value)
	{
		___curRound_20 = value;
	}

	inline static int32_t get_offset_of_canGlow_21() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406, ___canGlow_21)); }
	inline bool get_canGlow_21() const { return ___canGlow_21; }
	inline bool* get_address_of_canGlow_21() { return &___canGlow_21; }
	inline void set_canGlow_21(bool value)
	{
		___canGlow_21 = value;
	}
};

struct GameFifteenManager_t1527188406_StaticFields
{
public:
	// System.Func`2<System.Int32,System.Single> GameFifteenManager::<>f__am$cache0
	Func_2_t1628330325 * ___U3CU3Ef__amU24cache0_22;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(GameFifteenManager_t1527188406_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Func_2_t1628330325 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Func_2_t1628330325 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Func_2_t1628330325 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
