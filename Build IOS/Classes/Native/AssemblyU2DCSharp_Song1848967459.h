﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Song
struct  Song_t1848967459  : public Il2CppObject
{
public:
	// System.Int32[] Song::songSequence
	Int32U5BU5D_t3030399641* ___songSequence_0;

public:
	inline static int32_t get_offset_of_songSequence_0() { return static_cast<int32_t>(offsetof(Song_t1848967459, ___songSequence_0)); }
	inline Int32U5BU5D_t3030399641* get_songSequence_0() const { return ___songSequence_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_songSequence_0() { return &___songSequence_0; }
	inline void set_songSequence_0(Int32U5BU5D_t3030399641* value)
	{
		___songSequence_0 = value;
		Il2CppCodeGenWriteBarrier(&___songSequence_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
