﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<Gift>
struct List_1_t4114984978;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GiftManager
struct  GiftManager_t2451335245  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform GiftManager::spawnPostion
	Transform_t3275118058 * ___spawnPostion_2;
	// UnityEngine.GameObject GiftManager::yeyPrefab
	GameObject_t1756533147 * ___yeyPrefab_3;
	// System.Collections.Generic.List`1<Gift> GiftManager::gifts
	List_1_t4114984978 * ___gifts_4;
	// System.Boolean GiftManager::canGiveGift
	bool ___canGiveGift_5;
	// System.Int32 GiftManager::currentIndex
	int32_t ___currentIndex_6;

public:
	inline static int32_t get_offset_of_spawnPostion_2() { return static_cast<int32_t>(offsetof(GiftManager_t2451335245, ___spawnPostion_2)); }
	inline Transform_t3275118058 * get_spawnPostion_2() const { return ___spawnPostion_2; }
	inline Transform_t3275118058 ** get_address_of_spawnPostion_2() { return &___spawnPostion_2; }
	inline void set_spawnPostion_2(Transform_t3275118058 * value)
	{
		___spawnPostion_2 = value;
		Il2CppCodeGenWriteBarrier(&___spawnPostion_2, value);
	}

	inline static int32_t get_offset_of_yeyPrefab_3() { return static_cast<int32_t>(offsetof(GiftManager_t2451335245, ___yeyPrefab_3)); }
	inline GameObject_t1756533147 * get_yeyPrefab_3() const { return ___yeyPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_yeyPrefab_3() { return &___yeyPrefab_3; }
	inline void set_yeyPrefab_3(GameObject_t1756533147 * value)
	{
		___yeyPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___yeyPrefab_3, value);
	}

	inline static int32_t get_offset_of_gifts_4() { return static_cast<int32_t>(offsetof(GiftManager_t2451335245, ___gifts_4)); }
	inline List_1_t4114984978 * get_gifts_4() const { return ___gifts_4; }
	inline List_1_t4114984978 ** get_address_of_gifts_4() { return &___gifts_4; }
	inline void set_gifts_4(List_1_t4114984978 * value)
	{
		___gifts_4 = value;
		Il2CppCodeGenWriteBarrier(&___gifts_4, value);
	}

	inline static int32_t get_offset_of_canGiveGift_5() { return static_cast<int32_t>(offsetof(GiftManager_t2451335245, ___canGiveGift_5)); }
	inline bool get_canGiveGift_5() const { return ___canGiveGift_5; }
	inline bool* get_address_of_canGiveGift_5() { return &___canGiveGift_5; }
	inline void set_canGiveGift_5(bool value)
	{
		___canGiveGift_5 = value;
	}

	inline static int32_t get_offset_of_currentIndex_6() { return static_cast<int32_t>(offsetof(GiftManager_t2451335245, ___currentIndex_6)); }
	inline int32_t get_currentIndex_6() const { return ___currentIndex_6; }
	inline int32_t* get_address_of_currentIndex_6() { return &___currentIndex_6; }
	inline void set_currentIndex_6(int32_t value)
	{
		___currentIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
