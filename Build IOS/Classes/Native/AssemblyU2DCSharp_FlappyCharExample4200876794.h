﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Characters.FlappyChar
struct FlappyChar_t3600499100;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlappyCharExample
struct  FlappyCharExample_t4200876794  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Characters.FlappyChar FlappyCharExample::character
	FlappyChar_t3600499100 * ___character_2;

public:
	inline static int32_t get_offset_of_character_2() { return static_cast<int32_t>(offsetof(FlappyCharExample_t4200876794, ___character_2)); }
	inline FlappyChar_t3600499100 * get_character_2() const { return ___character_2; }
	inline FlappyChar_t3600499100 ** get_address_of_character_2() { return &___character_2; }
	inline void set_character_2(FlappyChar_t3600499100 * value)
	{
		___character_2 = value;
		Il2CppCodeGenWriteBarrier(&___character_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
