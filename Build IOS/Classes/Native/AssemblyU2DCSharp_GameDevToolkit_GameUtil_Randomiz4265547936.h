﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.GameUtil.RandomizerRules.DefaultRandomRule
struct  DefaultRandomRule_t4265547936  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> GameDevToolkit.GameUtil.RandomizerRules.DefaultRandomRule::asked
	List_1_t1440998580 * ___asked_0;
	// System.Int32 GameDevToolkit.GameUtil.RandomizerRules.DefaultRandomRule::nrLeft
	int32_t ___nrLeft_1;
	// System.Int32 GameDevToolkit.GameUtil.RandomizerRules.DefaultRandomRule::initialNrLeft
	int32_t ___initialNrLeft_2;
	// System.Int32 GameDevToolkit.GameUtil.RandomizerRules.DefaultRandomRule::lastBlock
	int32_t ___lastBlock_3;

public:
	inline static int32_t get_offset_of_asked_0() { return static_cast<int32_t>(offsetof(DefaultRandomRule_t4265547936, ___asked_0)); }
	inline List_1_t1440998580 * get_asked_0() const { return ___asked_0; }
	inline List_1_t1440998580 ** get_address_of_asked_0() { return &___asked_0; }
	inline void set_asked_0(List_1_t1440998580 * value)
	{
		___asked_0 = value;
		Il2CppCodeGenWriteBarrier(&___asked_0, value);
	}

	inline static int32_t get_offset_of_nrLeft_1() { return static_cast<int32_t>(offsetof(DefaultRandomRule_t4265547936, ___nrLeft_1)); }
	inline int32_t get_nrLeft_1() const { return ___nrLeft_1; }
	inline int32_t* get_address_of_nrLeft_1() { return &___nrLeft_1; }
	inline void set_nrLeft_1(int32_t value)
	{
		___nrLeft_1 = value;
	}

	inline static int32_t get_offset_of_initialNrLeft_2() { return static_cast<int32_t>(offsetof(DefaultRandomRule_t4265547936, ___initialNrLeft_2)); }
	inline int32_t get_initialNrLeft_2() const { return ___initialNrLeft_2; }
	inline int32_t* get_address_of_initialNrLeft_2() { return &___initialNrLeft_2; }
	inline void set_initialNrLeft_2(int32_t value)
	{
		___initialNrLeft_2 = value;
	}

	inline static int32_t get_offset_of_lastBlock_3() { return static_cast<int32_t>(offsetof(DefaultRandomRule_t4265547936, ___lastBlock_3)); }
	inline int32_t get_lastBlock_3() const { return ___lastBlock_3; }
	inline int32_t* get_address_of_lastBlock_3() { return &___lastBlock_3; }
	inline void set_lastBlock_3(int32_t value)
	{
		___lastBlock_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
