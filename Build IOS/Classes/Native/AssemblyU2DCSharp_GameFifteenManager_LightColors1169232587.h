﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameFifteenManager/LightColors
struct  LightColors_t1169232587 
{
public:
	// UnityEngine.Color GameFifteenManager/LightColors::newcolor
	Color_t2020392075  ___newcolor_0;
	// UnityEngine.Color GameFifteenManager/LightColors::color
	Color_t2020392075  ___color_1;

public:
	inline static int32_t get_offset_of_newcolor_0() { return static_cast<int32_t>(offsetof(LightColors_t1169232587, ___newcolor_0)); }
	inline Color_t2020392075  get_newcolor_0() const { return ___newcolor_0; }
	inline Color_t2020392075 * get_address_of_newcolor_0() { return &___newcolor_0; }
	inline void set_newcolor_0(Color_t2020392075  value)
	{
		___newcolor_0 = value;
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(LightColors_t1169232587, ___color_1)); }
	inline Color_t2020392075  get_color_1() const { return ___color_1; }
	inline Color_t2020392075 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t2020392075  value)
	{
		___color_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
