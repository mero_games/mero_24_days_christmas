﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.GameUtil.GridRep
struct GridRep_t2017325921;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GridExamples
struct  GridExamples_t561734721  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.GameUtil.GridRep GridExamples::grid
	GridRep_t2017325921 * ___grid_2;

public:
	inline static int32_t get_offset_of_grid_2() { return static_cast<int32_t>(offsetof(GridExamples_t561734721, ___grid_2)); }
	inline GridRep_t2017325921 * get_grid_2() const { return ___grid_2; }
	inline GridRep_t2017325921 ** get_address_of_grid_2() { return &___grid_2; }
	inline void set_grid_2(GridRep_t2017325921 * value)
	{
		___grid_2 = value;
		Il2CppCodeGenWriteBarrier(&___grid_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
