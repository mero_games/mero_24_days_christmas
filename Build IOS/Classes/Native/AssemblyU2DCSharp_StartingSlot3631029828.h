﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartingSlot
struct  StartingSlot_t3631029828  : public Il2CppObject
{
public:
	// UnityEngine.Transform StartingSlot::slotTransform
	Transform_t3275118058 * ___slotTransform_0;
	// System.Boolean StartingSlot::isTaken
	bool ___isTaken_1;

public:
	inline static int32_t get_offset_of_slotTransform_0() { return static_cast<int32_t>(offsetof(StartingSlot_t3631029828, ___slotTransform_0)); }
	inline Transform_t3275118058 * get_slotTransform_0() const { return ___slotTransform_0; }
	inline Transform_t3275118058 ** get_address_of_slotTransform_0() { return &___slotTransform_0; }
	inline void set_slotTransform_0(Transform_t3275118058 * value)
	{
		___slotTransform_0 = value;
		Il2CppCodeGenWriteBarrier(&___slotTransform_0, value);
	}

	inline static int32_t get_offset_of_isTaken_1() { return static_cast<int32_t>(offsetof(StartingSlot_t3631029828, ___isTaken_1)); }
	inline bool get_isTaken_1() const { return ___isTaken_1; }
	inline bool* get_address_of_isTaken_1() { return &___isTaken_1; }
	inline void set_isTaken_1(bool value)
	{
		___isTaken_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
