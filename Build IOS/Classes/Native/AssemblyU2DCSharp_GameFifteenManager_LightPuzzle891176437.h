﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameFifteenManager/LightPuzzle
struct  LightPuzzle_t891176437 
{
public:
	// System.Int32 GameFifteenManager/LightPuzzle::showLights
	int32_t ___showLights_0;
	// System.Int32[] GameFifteenManager/LightPuzzle::scheme
	Int32U5BU5D_t3030399641* ___scheme_1;

public:
	inline static int32_t get_offset_of_showLights_0() { return static_cast<int32_t>(offsetof(LightPuzzle_t891176437, ___showLights_0)); }
	inline int32_t get_showLights_0() const { return ___showLights_0; }
	inline int32_t* get_address_of_showLights_0() { return &___showLights_0; }
	inline void set_showLights_0(int32_t value)
	{
		___showLights_0 = value;
	}

	inline static int32_t get_offset_of_scheme_1() { return static_cast<int32_t>(offsetof(LightPuzzle_t891176437, ___scheme_1)); }
	inline Int32U5BU5D_t3030399641* get_scheme_1() const { return ___scheme_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_scheme_1() { return &___scheme_1; }
	inline void set_scheme_1(Int32U5BU5D_t3030399641* value)
	{
		___scheme_1 = value;
		Il2CppCodeGenWriteBarrier(&___scheme_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GameFifteenManager/LightPuzzle
struct LightPuzzle_t891176437_marshaled_pinvoke
{
	int32_t ___showLights_0;
	int32_t* ___scheme_1;
};
// Native definition for COM marshalling of GameFifteenManager/LightPuzzle
struct LightPuzzle_t891176437_marshaled_com
{
	int32_t ___showLights_0;
	int32_t* ___scheme_1;
};
