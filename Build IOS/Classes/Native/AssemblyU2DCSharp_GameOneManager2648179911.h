﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen1647695001.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameOneManager
struct  GameOneManager_t2648179911  : public SingletonBase_1_t1647695001
{
public:
	// System.Boolean GameOneManager::isPlaying
	bool ___isPlaying_4;
	// System.Single GameOneManager::centerScaleFactor
	float ___centerScaleFactor_5;
	// System.Single GameOneManager::movingScaleFactor
	float ___movingScaleFactor_6;
	// System.Single GameOneManager::fallingFlakeLifetime
	float ___fallingFlakeLifetime_7;
	// System.Single GameOneManager::snowflakeSpawnRate
	float ___snowflakeSpawnRate_8;
	// System.Int32 GameOneManager::chanceOfCorrectSpawn
	int32_t ___chanceOfCorrectSpawn_9;
	// System.Int32 GameOneManager::correctChoicesToMake
	int32_t ___correctChoicesToMake_10;
	// System.Int32 GameOneManager::nrOfLettersToDo
	int32_t ___nrOfLettersToDo_11;
	// UnityEngine.Transform GameOneManager::centerPieceHolder
	Transform_t3275118058 * ___centerPieceHolder_12;
	// UnityEngine.Transform GameOneManager::leftSpawnPoint
	Transform_t3275118058 * ___leftSpawnPoint_13;
	// UnityEngine.Transform GameOneManager::rightSpawnPoint
	Transform_t3275118058 * ___rightSpawnPoint_14;
	// UnityEngine.GameObject GameOneManager::currentCenterLetter
	GameObject_t1756533147 * ___currentCenterLetter_15;
	// UnityEngine.GameObject[] GameOneManager::snowFlakes
	GameObjectU5BU5D_t3057952154* ___snowFlakes_16;
	// UnityEngine.GameObject[] GameOneManager::letters
	GameObjectU5BU5D_t3057952154* ___letters_17;
	// UnityEngine.ParticleSystem GameOneManager::snowPopParticle
	ParticleSystem_t3394631041 * ___snowPopParticle_18;
	// System.Int32 GameOneManager::correctChoiceCounter
	int32_t ___correctChoiceCounter_19;
	// System.Int32 GameOneManager::lettersDone
	int32_t ___lettersDone_20;
	// System.Boolean[] GameOneManager::usedLetters
	BooleanU5BU5D_t3568034315* ___usedLetters_21;

public:
	inline static int32_t get_offset_of_isPlaying_4() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___isPlaying_4)); }
	inline bool get_isPlaying_4() const { return ___isPlaying_4; }
	inline bool* get_address_of_isPlaying_4() { return &___isPlaying_4; }
	inline void set_isPlaying_4(bool value)
	{
		___isPlaying_4 = value;
	}

	inline static int32_t get_offset_of_centerScaleFactor_5() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___centerScaleFactor_5)); }
	inline float get_centerScaleFactor_5() const { return ___centerScaleFactor_5; }
	inline float* get_address_of_centerScaleFactor_5() { return &___centerScaleFactor_5; }
	inline void set_centerScaleFactor_5(float value)
	{
		___centerScaleFactor_5 = value;
	}

	inline static int32_t get_offset_of_movingScaleFactor_6() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___movingScaleFactor_6)); }
	inline float get_movingScaleFactor_6() const { return ___movingScaleFactor_6; }
	inline float* get_address_of_movingScaleFactor_6() { return &___movingScaleFactor_6; }
	inline void set_movingScaleFactor_6(float value)
	{
		___movingScaleFactor_6 = value;
	}

	inline static int32_t get_offset_of_fallingFlakeLifetime_7() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___fallingFlakeLifetime_7)); }
	inline float get_fallingFlakeLifetime_7() const { return ___fallingFlakeLifetime_7; }
	inline float* get_address_of_fallingFlakeLifetime_7() { return &___fallingFlakeLifetime_7; }
	inline void set_fallingFlakeLifetime_7(float value)
	{
		___fallingFlakeLifetime_7 = value;
	}

	inline static int32_t get_offset_of_snowflakeSpawnRate_8() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___snowflakeSpawnRate_8)); }
	inline float get_snowflakeSpawnRate_8() const { return ___snowflakeSpawnRate_8; }
	inline float* get_address_of_snowflakeSpawnRate_8() { return &___snowflakeSpawnRate_8; }
	inline void set_snowflakeSpawnRate_8(float value)
	{
		___snowflakeSpawnRate_8 = value;
	}

	inline static int32_t get_offset_of_chanceOfCorrectSpawn_9() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___chanceOfCorrectSpawn_9)); }
	inline int32_t get_chanceOfCorrectSpawn_9() const { return ___chanceOfCorrectSpawn_9; }
	inline int32_t* get_address_of_chanceOfCorrectSpawn_9() { return &___chanceOfCorrectSpawn_9; }
	inline void set_chanceOfCorrectSpawn_9(int32_t value)
	{
		___chanceOfCorrectSpawn_9 = value;
	}

	inline static int32_t get_offset_of_correctChoicesToMake_10() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___correctChoicesToMake_10)); }
	inline int32_t get_correctChoicesToMake_10() const { return ___correctChoicesToMake_10; }
	inline int32_t* get_address_of_correctChoicesToMake_10() { return &___correctChoicesToMake_10; }
	inline void set_correctChoicesToMake_10(int32_t value)
	{
		___correctChoicesToMake_10 = value;
	}

	inline static int32_t get_offset_of_nrOfLettersToDo_11() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___nrOfLettersToDo_11)); }
	inline int32_t get_nrOfLettersToDo_11() const { return ___nrOfLettersToDo_11; }
	inline int32_t* get_address_of_nrOfLettersToDo_11() { return &___nrOfLettersToDo_11; }
	inline void set_nrOfLettersToDo_11(int32_t value)
	{
		___nrOfLettersToDo_11 = value;
	}

	inline static int32_t get_offset_of_centerPieceHolder_12() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___centerPieceHolder_12)); }
	inline Transform_t3275118058 * get_centerPieceHolder_12() const { return ___centerPieceHolder_12; }
	inline Transform_t3275118058 ** get_address_of_centerPieceHolder_12() { return &___centerPieceHolder_12; }
	inline void set_centerPieceHolder_12(Transform_t3275118058 * value)
	{
		___centerPieceHolder_12 = value;
		Il2CppCodeGenWriteBarrier(&___centerPieceHolder_12, value);
	}

	inline static int32_t get_offset_of_leftSpawnPoint_13() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___leftSpawnPoint_13)); }
	inline Transform_t3275118058 * get_leftSpawnPoint_13() const { return ___leftSpawnPoint_13; }
	inline Transform_t3275118058 ** get_address_of_leftSpawnPoint_13() { return &___leftSpawnPoint_13; }
	inline void set_leftSpawnPoint_13(Transform_t3275118058 * value)
	{
		___leftSpawnPoint_13 = value;
		Il2CppCodeGenWriteBarrier(&___leftSpawnPoint_13, value);
	}

	inline static int32_t get_offset_of_rightSpawnPoint_14() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___rightSpawnPoint_14)); }
	inline Transform_t3275118058 * get_rightSpawnPoint_14() const { return ___rightSpawnPoint_14; }
	inline Transform_t3275118058 ** get_address_of_rightSpawnPoint_14() { return &___rightSpawnPoint_14; }
	inline void set_rightSpawnPoint_14(Transform_t3275118058 * value)
	{
		___rightSpawnPoint_14 = value;
		Il2CppCodeGenWriteBarrier(&___rightSpawnPoint_14, value);
	}

	inline static int32_t get_offset_of_currentCenterLetter_15() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___currentCenterLetter_15)); }
	inline GameObject_t1756533147 * get_currentCenterLetter_15() const { return ___currentCenterLetter_15; }
	inline GameObject_t1756533147 ** get_address_of_currentCenterLetter_15() { return &___currentCenterLetter_15; }
	inline void set_currentCenterLetter_15(GameObject_t1756533147 * value)
	{
		___currentCenterLetter_15 = value;
		Il2CppCodeGenWriteBarrier(&___currentCenterLetter_15, value);
	}

	inline static int32_t get_offset_of_snowFlakes_16() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___snowFlakes_16)); }
	inline GameObjectU5BU5D_t3057952154* get_snowFlakes_16() const { return ___snowFlakes_16; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_snowFlakes_16() { return &___snowFlakes_16; }
	inline void set_snowFlakes_16(GameObjectU5BU5D_t3057952154* value)
	{
		___snowFlakes_16 = value;
		Il2CppCodeGenWriteBarrier(&___snowFlakes_16, value);
	}

	inline static int32_t get_offset_of_letters_17() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___letters_17)); }
	inline GameObjectU5BU5D_t3057952154* get_letters_17() const { return ___letters_17; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_letters_17() { return &___letters_17; }
	inline void set_letters_17(GameObjectU5BU5D_t3057952154* value)
	{
		___letters_17 = value;
		Il2CppCodeGenWriteBarrier(&___letters_17, value);
	}

	inline static int32_t get_offset_of_snowPopParticle_18() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___snowPopParticle_18)); }
	inline ParticleSystem_t3394631041 * get_snowPopParticle_18() const { return ___snowPopParticle_18; }
	inline ParticleSystem_t3394631041 ** get_address_of_snowPopParticle_18() { return &___snowPopParticle_18; }
	inline void set_snowPopParticle_18(ParticleSystem_t3394631041 * value)
	{
		___snowPopParticle_18 = value;
		Il2CppCodeGenWriteBarrier(&___snowPopParticle_18, value);
	}

	inline static int32_t get_offset_of_correctChoiceCounter_19() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___correctChoiceCounter_19)); }
	inline int32_t get_correctChoiceCounter_19() const { return ___correctChoiceCounter_19; }
	inline int32_t* get_address_of_correctChoiceCounter_19() { return &___correctChoiceCounter_19; }
	inline void set_correctChoiceCounter_19(int32_t value)
	{
		___correctChoiceCounter_19 = value;
	}

	inline static int32_t get_offset_of_lettersDone_20() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___lettersDone_20)); }
	inline int32_t get_lettersDone_20() const { return ___lettersDone_20; }
	inline int32_t* get_address_of_lettersDone_20() { return &___lettersDone_20; }
	inline void set_lettersDone_20(int32_t value)
	{
		___lettersDone_20 = value;
	}

	inline static int32_t get_offset_of_usedLetters_21() { return static_cast<int32_t>(offsetof(GameOneManager_t2648179911, ___usedLetters_21)); }
	inline BooleanU5BU5D_t3568034315* get_usedLetters_21() const { return ___usedLetters_21; }
	inline BooleanU5BU5D_t3568034315** get_address_of_usedLetters_21() { return &___usedLetters_21; }
	inline void set_usedLetters_21(BooleanU5BU5D_t3568034315* value)
	{
		___usedLetters_21 = value;
		Il2CppCodeGenWriteBarrier(&___usedLetters_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
