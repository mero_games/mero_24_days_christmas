﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.GameUtil.LookAtMouse
struct  LookAtMouse_t3531508735  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera GameDevToolkit.GameUtil.LookAtMouse::cam
	Camera_t189460977 * ___cam_2;
	// UnityEngine.Transform GameDevToolkit.GameUtil.LookAtMouse::my
	Transform_t3275118058 * ___my_3;
	// System.Boolean GameDevToolkit.GameUtil.LookAtMouse::_isSleeping
	bool ____isSleeping_4;
	// UnityEngine.Coroutine GameDevToolkit.GameUtil.LookAtMouse::sleepRoutine
	Coroutine_t2299508840 * ___sleepRoutine_5;

public:
	inline static int32_t get_offset_of_cam_2() { return static_cast<int32_t>(offsetof(LookAtMouse_t3531508735, ___cam_2)); }
	inline Camera_t189460977 * get_cam_2() const { return ___cam_2; }
	inline Camera_t189460977 ** get_address_of_cam_2() { return &___cam_2; }
	inline void set_cam_2(Camera_t189460977 * value)
	{
		___cam_2 = value;
		Il2CppCodeGenWriteBarrier(&___cam_2, value);
	}

	inline static int32_t get_offset_of_my_3() { return static_cast<int32_t>(offsetof(LookAtMouse_t3531508735, ___my_3)); }
	inline Transform_t3275118058 * get_my_3() const { return ___my_3; }
	inline Transform_t3275118058 ** get_address_of_my_3() { return &___my_3; }
	inline void set_my_3(Transform_t3275118058 * value)
	{
		___my_3 = value;
		Il2CppCodeGenWriteBarrier(&___my_3, value);
	}

	inline static int32_t get_offset_of__isSleeping_4() { return static_cast<int32_t>(offsetof(LookAtMouse_t3531508735, ____isSleeping_4)); }
	inline bool get__isSleeping_4() const { return ____isSleeping_4; }
	inline bool* get_address_of__isSleeping_4() { return &____isSleeping_4; }
	inline void set__isSleeping_4(bool value)
	{
		____isSleeping_4 = value;
	}

	inline static int32_t get_offset_of_sleepRoutine_5() { return static_cast<int32_t>(offsetof(LookAtMouse_t3531508735, ___sleepRoutine_5)); }
	inline Coroutine_t2299508840 * get_sleepRoutine_5() const { return ___sleepRoutine_5; }
	inline Coroutine_t2299508840 ** get_address_of_sleepRoutine_5() { return &___sleepRoutine_5; }
	inline void set_sleepRoutine_5(Coroutine_t2299508840 * value)
	{
		___sleepRoutine_5 = value;
		Il2CppCodeGenWriteBarrier(&___sleepRoutine_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
