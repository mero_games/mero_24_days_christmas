﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Spawner
struct Spawner_t534830648;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NextSpawner
struct  NextSpawner_t3258520277  : public MonoBehaviour_t1158329972
{
public:
	// Spawner NextSpawner::spawner
	Spawner_t534830648 * ___spawner_2;
	// UnityEngine.GameObject NextSpawner::currentPieceObject
	GameObject_t1756533147 * ___currentPieceObject_3;
	// System.Int32 NextSpawner::currentPieceId
	int32_t ___currentPieceId_4;

public:
	inline static int32_t get_offset_of_spawner_2() { return static_cast<int32_t>(offsetof(NextSpawner_t3258520277, ___spawner_2)); }
	inline Spawner_t534830648 * get_spawner_2() const { return ___spawner_2; }
	inline Spawner_t534830648 ** get_address_of_spawner_2() { return &___spawner_2; }
	inline void set_spawner_2(Spawner_t534830648 * value)
	{
		___spawner_2 = value;
		Il2CppCodeGenWriteBarrier(&___spawner_2, value);
	}

	inline static int32_t get_offset_of_currentPieceObject_3() { return static_cast<int32_t>(offsetof(NextSpawner_t3258520277, ___currentPieceObject_3)); }
	inline GameObject_t1756533147 * get_currentPieceObject_3() const { return ___currentPieceObject_3; }
	inline GameObject_t1756533147 ** get_address_of_currentPieceObject_3() { return &___currentPieceObject_3; }
	inline void set_currentPieceObject_3(GameObject_t1756533147 * value)
	{
		___currentPieceObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentPieceObject_3, value);
	}

	inline static int32_t get_offset_of_currentPieceId_4() { return static_cast<int32_t>(offsetof(NextSpawner_t3258520277, ___currentPieceId_4)); }
	inline int32_t get_currentPieceId_4() const { return ___currentPieceId_4; }
	inline int32_t* get_address_of_currentPieceId_4() { return &___currentPieceId_4; }
	inline void set_currentPieceId_4(int32_t value)
	{
		___currentPieceId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
