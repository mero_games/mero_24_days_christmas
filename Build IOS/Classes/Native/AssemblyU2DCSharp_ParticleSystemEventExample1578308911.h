﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Events.ParticleSystemCleaner
struct ParticleSystemCleaner_t2045555809;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleSystemEventExample
struct  ParticleSystemEventExample_t1578308911  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Events.ParticleSystemCleaner ParticleSystemEventExample::pSystem
	ParticleSystemCleaner_t2045555809 * ___pSystem_2;

public:
	inline static int32_t get_offset_of_pSystem_2() { return static_cast<int32_t>(offsetof(ParticleSystemEventExample_t1578308911, ___pSystem_2)); }
	inline ParticleSystemCleaner_t2045555809 * get_pSystem_2() const { return ___pSystem_2; }
	inline ParticleSystemCleaner_t2045555809 ** get_address_of_pSystem_2() { return &___pSystem_2; }
	inline void set_pSystem_2(ParticleSystemCleaner_t2045555809 * value)
	{
		___pSystem_2 = value;
		Il2CppCodeGenWriteBarrier(&___pSystem_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
