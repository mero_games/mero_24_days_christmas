﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetMidPontExample
struct  GetMidPontExample_t420734737  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform GetMidPontExample::point1
	Transform_t3275118058 * ___point1_2;
	// UnityEngine.Transform GetMidPontExample::point2
	Transform_t3275118058 * ___point2_3;

public:
	inline static int32_t get_offset_of_point1_2() { return static_cast<int32_t>(offsetof(GetMidPontExample_t420734737, ___point1_2)); }
	inline Transform_t3275118058 * get_point1_2() const { return ___point1_2; }
	inline Transform_t3275118058 ** get_address_of_point1_2() { return &___point1_2; }
	inline void set_point1_2(Transform_t3275118058 * value)
	{
		___point1_2 = value;
		Il2CppCodeGenWriteBarrier(&___point1_2, value);
	}

	inline static int32_t get_offset_of_point2_3() { return static_cast<int32_t>(offsetof(GetMidPontExample_t420734737, ___point2_3)); }
	inline Transform_t3275118058 * get_point2_3() const { return ___point2_3; }
	inline Transform_t3275118058 ** get_address_of_point2_3() { return &___point2_3; }
	inline void set_point2_3(Transform_t3275118058 * value)
	{
		___point2_3 = value;
		Il2CppCodeGenWriteBarrier(&___point2_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
