﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Visual.ObjectSequence
struct ObjectSequence_t1807435812;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MemoryGamePiece
struct  MemoryGamePiece_t2964149615  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Visual.ObjectSequence MemoryGamePiece::back
	ObjectSequence_t1807435812 * ___back_2;
	// GameDevToolkit.Visual.ObjectSequence MemoryGamePiece::front
	ObjectSequence_t1807435812 * ___front_3;

public:
	inline static int32_t get_offset_of_back_2() { return static_cast<int32_t>(offsetof(MemoryGamePiece_t2964149615, ___back_2)); }
	inline ObjectSequence_t1807435812 * get_back_2() const { return ___back_2; }
	inline ObjectSequence_t1807435812 ** get_address_of_back_2() { return &___back_2; }
	inline void set_back_2(ObjectSequence_t1807435812 * value)
	{
		___back_2 = value;
		Il2CppCodeGenWriteBarrier(&___back_2, value);
	}

	inline static int32_t get_offset_of_front_3() { return static_cast<int32_t>(offsetof(MemoryGamePiece_t2964149615, ___front_3)); }
	inline ObjectSequence_t1807435812 * get_front_3() const { return ___front_3; }
	inline ObjectSequence_t1807435812 ** get_address_of_front_3() { return &___front_3; }
	inline void set_front_3(ObjectSequence_t1807435812 * value)
	{
		___front_3 = value;
		Il2CppCodeGenWriteBarrier(&___front_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
