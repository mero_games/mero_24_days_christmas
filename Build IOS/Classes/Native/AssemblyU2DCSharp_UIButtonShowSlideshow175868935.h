﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_UIButton3377238306.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonShowSlideshow
struct  UIButtonShowSlideshow_t175868935  : public UIButton_t3377238306
{
public:
	// UnityEngine.GameObject UIButtonShowSlideshow::slideShow
	GameObject_t1756533147 * ___slideShow_4;
	// UnityEngine.Transform UIButtonShowSlideshow::slidesHolder
	Transform_t3275118058 * ___slidesHolder_5;

public:
	inline static int32_t get_offset_of_slideShow_4() { return static_cast<int32_t>(offsetof(UIButtonShowSlideshow_t175868935, ___slideShow_4)); }
	inline GameObject_t1756533147 * get_slideShow_4() const { return ___slideShow_4; }
	inline GameObject_t1756533147 ** get_address_of_slideShow_4() { return &___slideShow_4; }
	inline void set_slideShow_4(GameObject_t1756533147 * value)
	{
		___slideShow_4 = value;
		Il2CppCodeGenWriteBarrier(&___slideShow_4, value);
	}

	inline static int32_t get_offset_of_slidesHolder_5() { return static_cast<int32_t>(offsetof(UIButtonShowSlideshow_t175868935, ___slidesHolder_5)); }
	inline Transform_t3275118058 * get_slidesHolder_5() const { return ___slidesHolder_5; }
	inline Transform_t3275118058 ** get_address_of_slidesHolder_5() { return &___slidesHolder_5; }
	inline void set_slidesHolder_5(Transform_t3275118058 * value)
	{
		___slidesHolder_5 = value;
		Il2CppCodeGenWriteBarrier(&___slidesHolder_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
