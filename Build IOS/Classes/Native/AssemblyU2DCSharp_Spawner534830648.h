﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spawner
struct  Spawner_t534830648  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] Spawner::pieces
	GameObjectU5BU5D_t3057952154* ___pieces_2;
	// System.Int32 Spawner::nextId
	int32_t ___nextId_3;

public:
	inline static int32_t get_offset_of_pieces_2() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___pieces_2)); }
	inline GameObjectU5BU5D_t3057952154* get_pieces_2() const { return ___pieces_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_pieces_2() { return &___pieces_2; }
	inline void set_pieces_2(GameObjectU5BU5D_t3057952154* value)
	{
		___pieces_2 = value;
		Il2CppCodeGenWriteBarrier(&___pieces_2, value);
	}

	inline static int32_t get_offset_of_nextId_3() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___nextId_3)); }
	inline int32_t get_nextId_3() const { return ___nextId_3; }
	inline int32_t* get_address_of_nextId_3() { return &___nextId_3; }
	inline void set_nextId_3(int32_t value)
	{
		___nextId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
