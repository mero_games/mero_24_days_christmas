﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.AudioClip
struct AudioClip_t1932558630;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonPlaySoundOnStart
struct  ButtonPlaySoundOnStart_t3790818244  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip ButtonPlaySoundOnStart::sound
	AudioClip_t1932558630 * ___sound_2;
	// System.Single ButtonPlaySoundOnStart::myTime
	float ___myTime_3;
	// UnityEngine.Color ButtonPlaySoundOnStart::myColor
	Color_t2020392075  ___myColor_4;

public:
	inline static int32_t get_offset_of_sound_2() { return static_cast<int32_t>(offsetof(ButtonPlaySoundOnStart_t3790818244, ___sound_2)); }
	inline AudioClip_t1932558630 * get_sound_2() const { return ___sound_2; }
	inline AudioClip_t1932558630 ** get_address_of_sound_2() { return &___sound_2; }
	inline void set_sound_2(AudioClip_t1932558630 * value)
	{
		___sound_2 = value;
		Il2CppCodeGenWriteBarrier(&___sound_2, value);
	}

	inline static int32_t get_offset_of_myTime_3() { return static_cast<int32_t>(offsetof(ButtonPlaySoundOnStart_t3790818244, ___myTime_3)); }
	inline float get_myTime_3() const { return ___myTime_3; }
	inline float* get_address_of_myTime_3() { return &___myTime_3; }
	inline void set_myTime_3(float value)
	{
		___myTime_3 = value;
	}

	inline static int32_t get_offset_of_myColor_4() { return static_cast<int32_t>(offsetof(ButtonPlaySoundOnStart_t3790818244, ___myColor_4)); }
	inline Color_t2020392075  get_myColor_4() const { return ___myColor_4; }
	inline Color_t2020392075 * get_address_of_myColor_4() { return &___myColor_4; }
	inline void set_myColor_4(Color_t2020392075  value)
	{
		___myColor_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
