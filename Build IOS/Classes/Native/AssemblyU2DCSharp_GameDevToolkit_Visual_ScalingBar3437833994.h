﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_ScalingBar3059270671.h"

// System.Collections.Generic.Dictionary`2<GameDevToolkit.Visual.ScalingBar/ScaleOn,System.Single>
struct Dictionary_2_t3500712460;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Visual.ScalingBar
struct  ScalingBar_t3437833994  : public MonoBehaviour_t1158329972
{
public:
	// System.Single GameDevToolkit.Visual.ScalingBar::_precentage
	float ____precentage_2;
	// GameDevToolkit.Visual.ScalingBar/ScaleOn GameDevToolkit.Visual.ScalingBar::scaleType
	int32_t ___scaleType_3;
	// System.Collections.Generic.Dictionary`2<GameDevToolkit.Visual.ScalingBar/ScaleOn,System.Single> GameDevToolkit.Visual.ScalingBar::originalScale
	Dictionary_2_t3500712460 * ___originalScale_4;
	// System.Single GameDevToolkit.Visual.ScalingBar::oldPrecentage
	float ___oldPrecentage_5;

public:
	inline static int32_t get_offset_of__precentage_2() { return static_cast<int32_t>(offsetof(ScalingBar_t3437833994, ____precentage_2)); }
	inline float get__precentage_2() const { return ____precentage_2; }
	inline float* get_address_of__precentage_2() { return &____precentage_2; }
	inline void set__precentage_2(float value)
	{
		____precentage_2 = value;
	}

	inline static int32_t get_offset_of_scaleType_3() { return static_cast<int32_t>(offsetof(ScalingBar_t3437833994, ___scaleType_3)); }
	inline int32_t get_scaleType_3() const { return ___scaleType_3; }
	inline int32_t* get_address_of_scaleType_3() { return &___scaleType_3; }
	inline void set_scaleType_3(int32_t value)
	{
		___scaleType_3 = value;
	}

	inline static int32_t get_offset_of_originalScale_4() { return static_cast<int32_t>(offsetof(ScalingBar_t3437833994, ___originalScale_4)); }
	inline Dictionary_2_t3500712460 * get_originalScale_4() const { return ___originalScale_4; }
	inline Dictionary_2_t3500712460 ** get_address_of_originalScale_4() { return &___originalScale_4; }
	inline void set_originalScale_4(Dictionary_2_t3500712460 * value)
	{
		___originalScale_4 = value;
		Il2CppCodeGenWriteBarrier(&___originalScale_4, value);
	}

	inline static int32_t get_offset_of_oldPrecentage_5() { return static_cast<int32_t>(offsetof(ScalingBar_t3437833994, ___oldPrecentage_5)); }
	inline float get_oldPrecentage_5() const { return ___oldPrecentage_5; }
	inline float* get_address_of_oldPrecentage_5() { return &___oldPrecentage_5; }
	inline void set_oldPrecentage_5(float value)
	{
		___oldPrecentage_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
