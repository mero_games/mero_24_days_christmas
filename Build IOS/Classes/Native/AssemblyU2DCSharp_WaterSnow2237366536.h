﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaterSnow
struct  WaterSnow_t2237366536  : public MonoBehaviour_t1158329972
{
public:
	// System.Single WaterSnow::snowFlakeSpeed
	float ___snowFlakeSpeed_2;
	// System.Boolean WaterSnow::isSnow
	bool ___isSnow_3;
	// UnityEngine.GameObject WaterSnow::waterDrop
	GameObject_t1756533147 * ___waterDrop_4;
	// UnityEngine.GameObject WaterSnow::snowFlake
	GameObject_t1756533147 * ___snowFlake_5;
	// UnityEngine.Rigidbody2D WaterSnow::rb2d
	Rigidbody2D_t502193897 * ___rb2d_6;

public:
	inline static int32_t get_offset_of_snowFlakeSpeed_2() { return static_cast<int32_t>(offsetof(WaterSnow_t2237366536, ___snowFlakeSpeed_2)); }
	inline float get_snowFlakeSpeed_2() const { return ___snowFlakeSpeed_2; }
	inline float* get_address_of_snowFlakeSpeed_2() { return &___snowFlakeSpeed_2; }
	inline void set_snowFlakeSpeed_2(float value)
	{
		___snowFlakeSpeed_2 = value;
	}

	inline static int32_t get_offset_of_isSnow_3() { return static_cast<int32_t>(offsetof(WaterSnow_t2237366536, ___isSnow_3)); }
	inline bool get_isSnow_3() const { return ___isSnow_3; }
	inline bool* get_address_of_isSnow_3() { return &___isSnow_3; }
	inline void set_isSnow_3(bool value)
	{
		___isSnow_3 = value;
	}

	inline static int32_t get_offset_of_waterDrop_4() { return static_cast<int32_t>(offsetof(WaterSnow_t2237366536, ___waterDrop_4)); }
	inline GameObject_t1756533147 * get_waterDrop_4() const { return ___waterDrop_4; }
	inline GameObject_t1756533147 ** get_address_of_waterDrop_4() { return &___waterDrop_4; }
	inline void set_waterDrop_4(GameObject_t1756533147 * value)
	{
		___waterDrop_4 = value;
		Il2CppCodeGenWriteBarrier(&___waterDrop_4, value);
	}

	inline static int32_t get_offset_of_snowFlake_5() { return static_cast<int32_t>(offsetof(WaterSnow_t2237366536, ___snowFlake_5)); }
	inline GameObject_t1756533147 * get_snowFlake_5() const { return ___snowFlake_5; }
	inline GameObject_t1756533147 ** get_address_of_snowFlake_5() { return &___snowFlake_5; }
	inline void set_snowFlake_5(GameObject_t1756533147 * value)
	{
		___snowFlake_5 = value;
		Il2CppCodeGenWriteBarrier(&___snowFlake_5, value);
	}

	inline static int32_t get_offset_of_rb2d_6() { return static_cast<int32_t>(offsetof(WaterSnow_t2237366536, ___rb2d_6)); }
	inline Rigidbody2D_t502193897 * get_rb2d_6() const { return ___rb2d_6; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb2d_6() { return &___rb2d_6; }
	inline void set_rb2d_6(Rigidbody2D_t502193897 * value)
	{
		___rb2d_6 = value;
		Il2CppCodeGenWriteBarrier(&___rb2d_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
