﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CFX_InspectorHelp3280468206.h"
#include "AssemblyU2DCSharp_CFX_LightIntensityFade4221734619.h"
#include "AssemblyU2DCSharp_CFX_ShurikenThreadFix3286853382.h"
#include "AssemblyU2DCSharp_CFX_ShurikenThreadFix_U3CWaitFram410180078.h"
#include "AssemblyU2DCSharp_CFX_SpawnSystem3600628354.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068664.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (CFX_InspectorHelp_t3280468206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[4] = 
{
	CFX_InspectorHelp_t3280468206::get_offset_of_Locked_2(),
	CFX_InspectorHelp_t3280468206::get_offset_of_Title_3(),
	CFX_InspectorHelp_t3280468206::get_offset_of_HelpText_4(),
	CFX_InspectorHelp_t3280468206::get_offset_of_MsgType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (CFX_LightIntensityFade_t4221734619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[7] = 
{
	CFX_LightIntensityFade_t4221734619::get_offset_of_duration_2(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_delay_3(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_finalIntensity_4(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_baseIntensity_5(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_autodestruct_6(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_p_lifetime_7(),
	CFX_LightIntensityFade_t4221734619::get_offset_of_p_delay_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (CFX_ShurikenThreadFix_t3286853382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[1] = 
{
	CFX_ShurikenThreadFix_t3286853382::get_offset_of_systems_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (U3CWaitFrameU3Ec__Iterator0_t410180078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[6] = 
{
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24locvar0_0(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24locvar1_1(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24this_2(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24current_3(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24disposing_4(),
	U3CWaitFrameU3Ec__Iterator0_t410180078::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (CFX_SpawnSystem_t3600628354), -1, sizeof(CFX_SpawnSystem_t3600628354_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2304[10] = 
{
	CFX_SpawnSystem_t3600628354_StaticFields::get_offset_of_instance_2(),
	CFX_SpawnSystem_t3600628354::get_offset_of_objectsToPreload_3(),
	CFX_SpawnSystem_t3600628354::get_offset_of_objectsToPreloadTimes_4(),
	CFX_SpawnSystem_t3600628354::get_offset_of_hideObjectsInHierarchy_5(),
	CFX_SpawnSystem_t3600628354::get_offset_of_spawnAsChildren_6(),
	CFX_SpawnSystem_t3600628354::get_offset_of_onlyGetInactiveObjects_7(),
	CFX_SpawnSystem_t3600628354::get_offset_of_instantiateIfNeeded_8(),
	CFX_SpawnSystem_t3600628354::get_offset_of_allObjectsLoaded_9(),
	CFX_SpawnSystem_t3600628354::get_offset_of_instantiatedObjects_10(),
	CFX_SpawnSystem_t3600628354::get_offset_of_poolCursors_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2305[7] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DE92C9CC81AB855F207D937C85007F805F7AAA7C3_1(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1456763F890A84558F99AFA687C36B9037697848_2(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6A8E8EDE45BAE83FCA5AA789D832DBC778F9C098_3(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8D473A33065BA6399D0BD4F762C6D4013D8B3FE2_4(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DFE67944097A016D9C9AAE34464A3653EF9853450_5(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DCDD4E374455AB771221E3A742261291A0B27D864_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
