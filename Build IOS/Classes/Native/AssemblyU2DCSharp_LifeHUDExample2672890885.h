﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Visual.LifeHUD
struct LifeHUD_t785887741;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LifeHUDExample
struct  LifeHUDExample_t2672890885  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Visual.LifeHUD LifeHUDExample::lifes
	LifeHUD_t785887741 * ___lifes_2;

public:
	inline static int32_t get_offset_of_lifes_2() { return static_cast<int32_t>(offsetof(LifeHUDExample_t2672890885, ___lifes_2)); }
	inline LifeHUD_t785887741 * get_lifes_2() const { return ___lifes_2; }
	inline LifeHUD_t785887741 ** get_address_of_lifes_2() { return &___lifes_2; }
	inline void set_lifes_2(LifeHUD_t785887741 * value)
	{
		___lifes_2 = value;
		Il2CppCodeGenWriteBarrier(&___lifes_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
