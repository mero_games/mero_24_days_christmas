﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Visual.ObjectSequence
struct ObjectSequence_t1807435812;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectSequenceExample
struct  ObjectSequenceExample_t3751909220  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Visual.ObjectSequence ObjectSequenceExample::manualChange
	ObjectSequence_t1807435812 * ___manualChange_2;

public:
	inline static int32_t get_offset_of_manualChange_2() { return static_cast<int32_t>(offsetof(ObjectSequenceExample_t3751909220, ___manualChange_2)); }
	inline ObjectSequence_t1807435812 * get_manualChange_2() const { return ___manualChange_2; }
	inline ObjectSequence_t1807435812 ** get_address_of_manualChange_2() { return &___manualChange_2; }
	inline void set_manualChange_2(ObjectSequence_t1807435812 * value)
	{
		___manualChange_2 = value;
		Il2CppCodeGenWriteBarrier(&___manualChange_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
