﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventManager
struct  EventManager_t2792515701  : public Il2CppObject
{
public:

public:
};

struct EventManager_t2792515701_StaticFields
{
public:
	// System.Action EventManager::OnMouseDown
	Action_t3226471752 * ___OnMouseDown_0;
	// System.Action EventManager::OnMouseUp
	Action_t3226471752 * ___OnMouseUp_1;
	// System.Action EventManager::OnWindowClicked
	Action_t3226471752 * ___OnWindowClicked_2;

public:
	inline static int32_t get_offset_of_OnMouseDown_0() { return static_cast<int32_t>(offsetof(EventManager_t2792515701_StaticFields, ___OnMouseDown_0)); }
	inline Action_t3226471752 * get_OnMouseDown_0() const { return ___OnMouseDown_0; }
	inline Action_t3226471752 ** get_address_of_OnMouseDown_0() { return &___OnMouseDown_0; }
	inline void set_OnMouseDown_0(Action_t3226471752 * value)
	{
		___OnMouseDown_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnMouseDown_0, value);
	}

	inline static int32_t get_offset_of_OnMouseUp_1() { return static_cast<int32_t>(offsetof(EventManager_t2792515701_StaticFields, ___OnMouseUp_1)); }
	inline Action_t3226471752 * get_OnMouseUp_1() const { return ___OnMouseUp_1; }
	inline Action_t3226471752 ** get_address_of_OnMouseUp_1() { return &___OnMouseUp_1; }
	inline void set_OnMouseUp_1(Action_t3226471752 * value)
	{
		___OnMouseUp_1 = value;
		Il2CppCodeGenWriteBarrier(&___OnMouseUp_1, value);
	}

	inline static int32_t get_offset_of_OnWindowClicked_2() { return static_cast<int32_t>(offsetof(EventManager_t2792515701_StaticFields, ___OnWindowClicked_2)); }
	inline Action_t3226471752 * get_OnWindowClicked_2() const { return ___OnWindowClicked_2; }
	inline Action_t3226471752 ** get_address_of_OnWindowClicked_2() { return &___OnWindowClicked_2; }
	inline void set_OnWindowClicked_2(Action_t3226471752 * value)
	{
		___OnWindowClicked_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnWindowClicked_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
