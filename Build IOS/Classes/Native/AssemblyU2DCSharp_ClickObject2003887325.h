﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_ClickObject_ClickType4062750492.h"

// UnityEngine.Rendering.SortingGroup
struct SortingGroup_t3832012067;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickObject
struct  ClickObject_t2003887325  : public MonoBehaviour_t1158329972
{
public:
	// ClickObject/ClickType ClickObject::type
	int32_t ___type_2;
	// System.Int32 ClickObject::num
	int32_t ___num_3;
	// UnityEngine.Rendering.SortingGroup ClickObject::_order
	SortingGroup_t3832012067 * ____order_4;
	// UnityEngine.SpriteRenderer ClickObject::_sprite
	SpriteRenderer_t1209076198 * ____sprite_5;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(ClickObject_t2003887325, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_num_3() { return static_cast<int32_t>(offsetof(ClickObject_t2003887325, ___num_3)); }
	inline int32_t get_num_3() const { return ___num_3; }
	inline int32_t* get_address_of_num_3() { return &___num_3; }
	inline void set_num_3(int32_t value)
	{
		___num_3 = value;
	}

	inline static int32_t get_offset_of__order_4() { return static_cast<int32_t>(offsetof(ClickObject_t2003887325, ____order_4)); }
	inline SortingGroup_t3832012067 * get__order_4() const { return ____order_4; }
	inline SortingGroup_t3832012067 ** get_address_of__order_4() { return &____order_4; }
	inline void set__order_4(SortingGroup_t3832012067 * value)
	{
		____order_4 = value;
		Il2CppCodeGenWriteBarrier(&____order_4, value);
	}

	inline static int32_t get_offset_of__sprite_5() { return static_cast<int32_t>(offsetof(ClickObject_t2003887325, ____sprite_5)); }
	inline SpriteRenderer_t1209076198 * get__sprite_5() const { return ____sprite_5; }
	inline SpriteRenderer_t1209076198 ** get_address_of__sprite_5() { return &____sprite_5; }
	inline void set__sprite_5(SpriteRenderer_t1209076198 * value)
	{
		____sprite_5 = value;
		Il2CppCodeGenWriteBarrier(&____sprite_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
