﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameDevToolkit.Sound.SoundLayer/VolumeDelegate
struct VolumeDelegate_t2775777142;
// System.String
struct String_t;
// System.Collections.Generic.List`1<GameDevToolkit.Sound.SoundClip>
struct List_1_t3113068555;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Sound.SoundLayer
struct  SoundLayer_t2188462586  : public Il2CppObject
{
public:
	// GameDevToolkit.Sound.SoundLayer/VolumeDelegate GameDevToolkit.Sound.SoundLayer::VolumeChangedEvent
	VolumeDelegate_t2775777142 * ___VolumeChangedEvent_0;
	// System.String GameDevToolkit.Sound.SoundLayer::_name
	String_t* ____name_1;
	// System.Single GameDevToolkit.Sound.SoundLayer::_volume
	float ____volume_2;
	// System.Collections.Generic.List`1<GameDevToolkit.Sound.SoundClip> GameDevToolkit.Sound.SoundLayer::sounds
	List_1_t3113068555 * ___sounds_3;

public:
	inline static int32_t get_offset_of_VolumeChangedEvent_0() { return static_cast<int32_t>(offsetof(SoundLayer_t2188462586, ___VolumeChangedEvent_0)); }
	inline VolumeDelegate_t2775777142 * get_VolumeChangedEvent_0() const { return ___VolumeChangedEvent_0; }
	inline VolumeDelegate_t2775777142 ** get_address_of_VolumeChangedEvent_0() { return &___VolumeChangedEvent_0; }
	inline void set_VolumeChangedEvent_0(VolumeDelegate_t2775777142 * value)
	{
		___VolumeChangedEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___VolumeChangedEvent_0, value);
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(SoundLayer_t2188462586, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier(&____name_1, value);
	}

	inline static int32_t get_offset_of__volume_2() { return static_cast<int32_t>(offsetof(SoundLayer_t2188462586, ____volume_2)); }
	inline float get__volume_2() const { return ____volume_2; }
	inline float* get_address_of__volume_2() { return &____volume_2; }
	inline void set__volume_2(float value)
	{
		____volume_2 = value;
	}

	inline static int32_t get_offset_of_sounds_3() { return static_cast<int32_t>(offsetof(SoundLayer_t2188462586, ___sounds_3)); }
	inline List_1_t3113068555 * get_sounds_3() const { return ___sounds_3; }
	inline List_1_t3113068555 ** get_address_of_sounds_3() { return &___sounds_3; }
	inline void set_sounds_3(List_1_t3113068555 * value)
	{
		___sounds_3 = value;
		Il2CppCodeGenWriteBarrier(&___sounds_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
