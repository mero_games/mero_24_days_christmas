﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "DOTweenPro_DG_Tweening_Core_ABSAnimationComponent2205594551.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_Plugins_Options_OrientType1755667719.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "DOTween_DG_Tweening_PathMode1545785466.h"
#include "DOTween_DG_Tweening_AxisConstraint1244566668.h"
#include "DOTweenPro_DG_Tweening_DOTweenInspectorMode2739551672.h"
#include "DOTween_DG_Tweening_PathType2815988833.h"
#include "DOTweenPro_DG_Tweening_HandlesType3201532857.h"
#include "DOTweenPro_DG_Tweening_HandlesDrawMode3273484032.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t2828565993;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenPath
struct  DOTweenPath_t1397145371  : public ABSAnimationComponent_t2205594551
{
public:
	// System.Single DG.Tweening.DOTweenPath::delay
	float ___delay_19;
	// System.Single DG.Tweening.DOTweenPath::duration
	float ___duration_20;
	// DG.Tweening.Ease DG.Tweening.DOTweenPath::easeType
	int32_t ___easeType_21;
	// UnityEngine.AnimationCurve DG.Tweening.DOTweenPath::easeCurve
	AnimationCurve_t3306541151 * ___easeCurve_22;
	// System.Int32 DG.Tweening.DOTweenPath::loops
	int32_t ___loops_23;
	// System.String DG.Tweening.DOTweenPath::id
	String_t* ___id_24;
	// DG.Tweening.LoopType DG.Tweening.DOTweenPath::loopType
	int32_t ___loopType_25;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.DOTweenPath::orientType
	int32_t ___orientType_26;
	// UnityEngine.Transform DG.Tweening.DOTweenPath::lookAtTransform
	Transform_t3275118058 * ___lookAtTransform_27;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::lookAtPosition
	Vector3_t2243707580  ___lookAtPosition_28;
	// System.Single DG.Tweening.DOTweenPath::lookAhead
	float ___lookAhead_29;
	// System.Boolean DG.Tweening.DOTweenPath::autoPlay
	bool ___autoPlay_30;
	// System.Boolean DG.Tweening.DOTweenPath::autoKill
	bool ___autoKill_31;
	// System.Boolean DG.Tweening.DOTweenPath::relative
	bool ___relative_32;
	// System.Boolean DG.Tweening.DOTweenPath::isLocal
	bool ___isLocal_33;
	// System.Boolean DG.Tweening.DOTweenPath::isClosedPath
	bool ___isClosedPath_34;
	// System.Int32 DG.Tweening.DOTweenPath::pathResolution
	int32_t ___pathResolution_35;
	// DG.Tweening.PathMode DG.Tweening.DOTweenPath::pathMode
	int32_t ___pathMode_36;
	// DG.Tweening.AxisConstraint DG.Tweening.DOTweenPath::lockRotation
	int32_t ___lockRotation_37;
	// System.Boolean DG.Tweening.DOTweenPath::assignForwardAndUp
	bool ___assignForwardAndUp_38;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::forwardDirection
	Vector3_t2243707580  ___forwardDirection_39;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::upDirection
	Vector3_t2243707580  ___upDirection_40;
	// System.Boolean DG.Tweening.DOTweenPath::tweenRigidbody
	bool ___tweenRigidbody_41;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DG.Tweening.DOTweenPath::wps
	List_1_t1612828712 * ___wps_42;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DG.Tweening.DOTweenPath::fullWps
	List_1_t1612828712 * ___fullWps_43;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.DOTweenPath::path
	Path_t2828565993 * ___path_44;
	// DG.Tweening.DOTweenInspectorMode DG.Tweening.DOTweenPath::inspectorMode
	int32_t ___inspectorMode_45;
	// DG.Tweening.PathType DG.Tweening.DOTweenPath::pathType
	int32_t ___pathType_46;
	// DG.Tweening.HandlesType DG.Tweening.DOTweenPath::handlesType
	int32_t ___handlesType_47;
	// System.Boolean DG.Tweening.DOTweenPath::livePreview
	bool ___livePreview_48;
	// DG.Tweening.HandlesDrawMode DG.Tweening.DOTweenPath::handlesDrawMode
	int32_t ___handlesDrawMode_49;
	// System.Single DG.Tweening.DOTweenPath::perspectiveHandleSize
	float ___perspectiveHandleSize_50;
	// System.Boolean DG.Tweening.DOTweenPath::showIndexes
	bool ___showIndexes_51;
	// System.Boolean DG.Tweening.DOTweenPath::showWpLength
	bool ___showWpLength_52;
	// UnityEngine.Color DG.Tweening.DOTweenPath::pathColor
	Color_t2020392075  ___pathColor_53;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::lastSrcPosition
	Vector3_t2243707580  ___lastSrcPosition_54;
	// System.Boolean DG.Tweening.DOTweenPath::wpsDropdown
	bool ___wpsDropdown_55;
	// System.Single DG.Tweening.DOTweenPath::dropToFloorOffset
	float ___dropToFloorOffset_56;

public:
	inline static int32_t get_offset_of_delay_19() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___delay_19)); }
	inline float get_delay_19() const { return ___delay_19; }
	inline float* get_address_of_delay_19() { return &___delay_19; }
	inline void set_delay_19(float value)
	{
		___delay_19 = value;
	}

	inline static int32_t get_offset_of_duration_20() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___duration_20)); }
	inline float get_duration_20() const { return ___duration_20; }
	inline float* get_address_of_duration_20() { return &___duration_20; }
	inline void set_duration_20(float value)
	{
		___duration_20 = value;
	}

	inline static int32_t get_offset_of_easeType_21() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___easeType_21)); }
	inline int32_t get_easeType_21() const { return ___easeType_21; }
	inline int32_t* get_address_of_easeType_21() { return &___easeType_21; }
	inline void set_easeType_21(int32_t value)
	{
		___easeType_21 = value;
	}

	inline static int32_t get_offset_of_easeCurve_22() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___easeCurve_22)); }
	inline AnimationCurve_t3306541151 * get_easeCurve_22() const { return ___easeCurve_22; }
	inline AnimationCurve_t3306541151 ** get_address_of_easeCurve_22() { return &___easeCurve_22; }
	inline void set_easeCurve_22(AnimationCurve_t3306541151 * value)
	{
		___easeCurve_22 = value;
		Il2CppCodeGenWriteBarrier(&___easeCurve_22, value);
	}

	inline static int32_t get_offset_of_loops_23() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___loops_23)); }
	inline int32_t get_loops_23() const { return ___loops_23; }
	inline int32_t* get_address_of_loops_23() { return &___loops_23; }
	inline void set_loops_23(int32_t value)
	{
		___loops_23 = value;
	}

	inline static int32_t get_offset_of_id_24() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___id_24)); }
	inline String_t* get_id_24() const { return ___id_24; }
	inline String_t** get_address_of_id_24() { return &___id_24; }
	inline void set_id_24(String_t* value)
	{
		___id_24 = value;
		Il2CppCodeGenWriteBarrier(&___id_24, value);
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_orientType_26() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___orientType_26)); }
	inline int32_t get_orientType_26() const { return ___orientType_26; }
	inline int32_t* get_address_of_orientType_26() { return &___orientType_26; }
	inline void set_orientType_26(int32_t value)
	{
		___orientType_26 = value;
	}

	inline static int32_t get_offset_of_lookAtTransform_27() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___lookAtTransform_27)); }
	inline Transform_t3275118058 * get_lookAtTransform_27() const { return ___lookAtTransform_27; }
	inline Transform_t3275118058 ** get_address_of_lookAtTransform_27() { return &___lookAtTransform_27; }
	inline void set_lookAtTransform_27(Transform_t3275118058 * value)
	{
		___lookAtTransform_27 = value;
		Il2CppCodeGenWriteBarrier(&___lookAtTransform_27, value);
	}

	inline static int32_t get_offset_of_lookAtPosition_28() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___lookAtPosition_28)); }
	inline Vector3_t2243707580  get_lookAtPosition_28() const { return ___lookAtPosition_28; }
	inline Vector3_t2243707580 * get_address_of_lookAtPosition_28() { return &___lookAtPosition_28; }
	inline void set_lookAtPosition_28(Vector3_t2243707580  value)
	{
		___lookAtPosition_28 = value;
	}

	inline static int32_t get_offset_of_lookAhead_29() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___lookAhead_29)); }
	inline float get_lookAhead_29() const { return ___lookAhead_29; }
	inline float* get_address_of_lookAhead_29() { return &___lookAhead_29; }
	inline void set_lookAhead_29(float value)
	{
		___lookAhead_29 = value;
	}

	inline static int32_t get_offset_of_autoPlay_30() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___autoPlay_30)); }
	inline bool get_autoPlay_30() const { return ___autoPlay_30; }
	inline bool* get_address_of_autoPlay_30() { return &___autoPlay_30; }
	inline void set_autoPlay_30(bool value)
	{
		___autoPlay_30 = value;
	}

	inline static int32_t get_offset_of_autoKill_31() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___autoKill_31)); }
	inline bool get_autoKill_31() const { return ___autoKill_31; }
	inline bool* get_address_of_autoKill_31() { return &___autoKill_31; }
	inline void set_autoKill_31(bool value)
	{
		___autoKill_31 = value;
	}

	inline static int32_t get_offset_of_relative_32() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___relative_32)); }
	inline bool get_relative_32() const { return ___relative_32; }
	inline bool* get_address_of_relative_32() { return &___relative_32; }
	inline void set_relative_32(bool value)
	{
		___relative_32 = value;
	}

	inline static int32_t get_offset_of_isLocal_33() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___isLocal_33)); }
	inline bool get_isLocal_33() const { return ___isLocal_33; }
	inline bool* get_address_of_isLocal_33() { return &___isLocal_33; }
	inline void set_isLocal_33(bool value)
	{
		___isLocal_33 = value;
	}

	inline static int32_t get_offset_of_isClosedPath_34() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___isClosedPath_34)); }
	inline bool get_isClosedPath_34() const { return ___isClosedPath_34; }
	inline bool* get_address_of_isClosedPath_34() { return &___isClosedPath_34; }
	inline void set_isClosedPath_34(bool value)
	{
		___isClosedPath_34 = value;
	}

	inline static int32_t get_offset_of_pathResolution_35() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___pathResolution_35)); }
	inline int32_t get_pathResolution_35() const { return ___pathResolution_35; }
	inline int32_t* get_address_of_pathResolution_35() { return &___pathResolution_35; }
	inline void set_pathResolution_35(int32_t value)
	{
		___pathResolution_35 = value;
	}

	inline static int32_t get_offset_of_pathMode_36() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___pathMode_36)); }
	inline int32_t get_pathMode_36() const { return ___pathMode_36; }
	inline int32_t* get_address_of_pathMode_36() { return &___pathMode_36; }
	inline void set_pathMode_36(int32_t value)
	{
		___pathMode_36 = value;
	}

	inline static int32_t get_offset_of_lockRotation_37() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___lockRotation_37)); }
	inline int32_t get_lockRotation_37() const { return ___lockRotation_37; }
	inline int32_t* get_address_of_lockRotation_37() { return &___lockRotation_37; }
	inline void set_lockRotation_37(int32_t value)
	{
		___lockRotation_37 = value;
	}

	inline static int32_t get_offset_of_assignForwardAndUp_38() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___assignForwardAndUp_38)); }
	inline bool get_assignForwardAndUp_38() const { return ___assignForwardAndUp_38; }
	inline bool* get_address_of_assignForwardAndUp_38() { return &___assignForwardAndUp_38; }
	inline void set_assignForwardAndUp_38(bool value)
	{
		___assignForwardAndUp_38 = value;
	}

	inline static int32_t get_offset_of_forwardDirection_39() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___forwardDirection_39)); }
	inline Vector3_t2243707580  get_forwardDirection_39() const { return ___forwardDirection_39; }
	inline Vector3_t2243707580 * get_address_of_forwardDirection_39() { return &___forwardDirection_39; }
	inline void set_forwardDirection_39(Vector3_t2243707580  value)
	{
		___forwardDirection_39 = value;
	}

	inline static int32_t get_offset_of_upDirection_40() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___upDirection_40)); }
	inline Vector3_t2243707580  get_upDirection_40() const { return ___upDirection_40; }
	inline Vector3_t2243707580 * get_address_of_upDirection_40() { return &___upDirection_40; }
	inline void set_upDirection_40(Vector3_t2243707580  value)
	{
		___upDirection_40 = value;
	}

	inline static int32_t get_offset_of_tweenRigidbody_41() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___tweenRigidbody_41)); }
	inline bool get_tweenRigidbody_41() const { return ___tweenRigidbody_41; }
	inline bool* get_address_of_tweenRigidbody_41() { return &___tweenRigidbody_41; }
	inline void set_tweenRigidbody_41(bool value)
	{
		___tweenRigidbody_41 = value;
	}

	inline static int32_t get_offset_of_wps_42() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___wps_42)); }
	inline List_1_t1612828712 * get_wps_42() const { return ___wps_42; }
	inline List_1_t1612828712 ** get_address_of_wps_42() { return &___wps_42; }
	inline void set_wps_42(List_1_t1612828712 * value)
	{
		___wps_42 = value;
		Il2CppCodeGenWriteBarrier(&___wps_42, value);
	}

	inline static int32_t get_offset_of_fullWps_43() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___fullWps_43)); }
	inline List_1_t1612828712 * get_fullWps_43() const { return ___fullWps_43; }
	inline List_1_t1612828712 ** get_address_of_fullWps_43() { return &___fullWps_43; }
	inline void set_fullWps_43(List_1_t1612828712 * value)
	{
		___fullWps_43 = value;
		Il2CppCodeGenWriteBarrier(&___fullWps_43, value);
	}

	inline static int32_t get_offset_of_path_44() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___path_44)); }
	inline Path_t2828565993 * get_path_44() const { return ___path_44; }
	inline Path_t2828565993 ** get_address_of_path_44() { return &___path_44; }
	inline void set_path_44(Path_t2828565993 * value)
	{
		___path_44 = value;
		Il2CppCodeGenWriteBarrier(&___path_44, value);
	}

	inline static int32_t get_offset_of_inspectorMode_45() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___inspectorMode_45)); }
	inline int32_t get_inspectorMode_45() const { return ___inspectorMode_45; }
	inline int32_t* get_address_of_inspectorMode_45() { return &___inspectorMode_45; }
	inline void set_inspectorMode_45(int32_t value)
	{
		___inspectorMode_45 = value;
	}

	inline static int32_t get_offset_of_pathType_46() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___pathType_46)); }
	inline int32_t get_pathType_46() const { return ___pathType_46; }
	inline int32_t* get_address_of_pathType_46() { return &___pathType_46; }
	inline void set_pathType_46(int32_t value)
	{
		___pathType_46 = value;
	}

	inline static int32_t get_offset_of_handlesType_47() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___handlesType_47)); }
	inline int32_t get_handlesType_47() const { return ___handlesType_47; }
	inline int32_t* get_address_of_handlesType_47() { return &___handlesType_47; }
	inline void set_handlesType_47(int32_t value)
	{
		___handlesType_47 = value;
	}

	inline static int32_t get_offset_of_livePreview_48() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___livePreview_48)); }
	inline bool get_livePreview_48() const { return ___livePreview_48; }
	inline bool* get_address_of_livePreview_48() { return &___livePreview_48; }
	inline void set_livePreview_48(bool value)
	{
		___livePreview_48 = value;
	}

	inline static int32_t get_offset_of_handlesDrawMode_49() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___handlesDrawMode_49)); }
	inline int32_t get_handlesDrawMode_49() const { return ___handlesDrawMode_49; }
	inline int32_t* get_address_of_handlesDrawMode_49() { return &___handlesDrawMode_49; }
	inline void set_handlesDrawMode_49(int32_t value)
	{
		___handlesDrawMode_49 = value;
	}

	inline static int32_t get_offset_of_perspectiveHandleSize_50() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___perspectiveHandleSize_50)); }
	inline float get_perspectiveHandleSize_50() const { return ___perspectiveHandleSize_50; }
	inline float* get_address_of_perspectiveHandleSize_50() { return &___perspectiveHandleSize_50; }
	inline void set_perspectiveHandleSize_50(float value)
	{
		___perspectiveHandleSize_50 = value;
	}

	inline static int32_t get_offset_of_showIndexes_51() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___showIndexes_51)); }
	inline bool get_showIndexes_51() const { return ___showIndexes_51; }
	inline bool* get_address_of_showIndexes_51() { return &___showIndexes_51; }
	inline void set_showIndexes_51(bool value)
	{
		___showIndexes_51 = value;
	}

	inline static int32_t get_offset_of_showWpLength_52() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___showWpLength_52)); }
	inline bool get_showWpLength_52() const { return ___showWpLength_52; }
	inline bool* get_address_of_showWpLength_52() { return &___showWpLength_52; }
	inline void set_showWpLength_52(bool value)
	{
		___showWpLength_52 = value;
	}

	inline static int32_t get_offset_of_pathColor_53() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___pathColor_53)); }
	inline Color_t2020392075  get_pathColor_53() const { return ___pathColor_53; }
	inline Color_t2020392075 * get_address_of_pathColor_53() { return &___pathColor_53; }
	inline void set_pathColor_53(Color_t2020392075  value)
	{
		___pathColor_53 = value;
	}

	inline static int32_t get_offset_of_lastSrcPosition_54() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___lastSrcPosition_54)); }
	inline Vector3_t2243707580  get_lastSrcPosition_54() const { return ___lastSrcPosition_54; }
	inline Vector3_t2243707580 * get_address_of_lastSrcPosition_54() { return &___lastSrcPosition_54; }
	inline void set_lastSrcPosition_54(Vector3_t2243707580  value)
	{
		___lastSrcPosition_54 = value;
	}

	inline static int32_t get_offset_of_wpsDropdown_55() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___wpsDropdown_55)); }
	inline bool get_wpsDropdown_55() const { return ___wpsDropdown_55; }
	inline bool* get_address_of_wpsDropdown_55() { return &___wpsDropdown_55; }
	inline void set_wpsDropdown_55(bool value)
	{
		___wpsDropdown_55 = value;
	}

	inline static int32_t get_offset_of_dropToFloorOffset_56() { return static_cast<int32_t>(offsetof(DOTweenPath_t1397145371, ___dropToFloorOffset_56)); }
	inline float get_dropToFloorOffset_56() const { return ___dropToFloorOffset_56; }
	inline float* get_address_of_dropToFloorOffset_56() { return &___dropToFloorOffset_56; }
	inline void set_dropToFloorOffset_56(float value)
	{
		___dropToFloorOffset_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
