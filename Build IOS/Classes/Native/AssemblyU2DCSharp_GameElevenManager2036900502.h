﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen1036415592.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameElevenManager
struct  GameElevenManager_t2036900502  : public SingletonBase_1_t1036415592
{
public:
	// System.Boolean GameElevenManager::isPlaying
	bool ___isPlaying_4;
	// System.Single GameElevenManager::riseAmount
	float ___riseAmount_5;
	// System.Single GameElevenManager::riseSpeed
	float ___riseSpeed_6;
	// System.Single GameElevenManager::spawnRate
	float ___spawnRate_7;
	// System.Single GameElevenManager::xThreshold
	float ___xThreshold_8;
	// System.Int32 GameElevenManager::flakesToHit
	int32_t ___flakesToHit_9;
	// UnityEngine.Transform GameElevenManager::snowPile
	Transform_t3275118058 * ___snowPile_10;
	// UnityEngine.GameObject GameElevenManager::waterSnow
	GameObject_t1756533147 * ___waterSnow_11;
	// UnityEngine.Sprite[] GameElevenManager::snowflakeSprites
	SpriteU5BU5D_t3359083662* ___snowflakeSprites_12;
	// System.Int32 GameElevenManager::snowHitCount
	int32_t ___snowHitCount_13;

public:
	inline static int32_t get_offset_of_isPlaying_4() { return static_cast<int32_t>(offsetof(GameElevenManager_t2036900502, ___isPlaying_4)); }
	inline bool get_isPlaying_4() const { return ___isPlaying_4; }
	inline bool* get_address_of_isPlaying_4() { return &___isPlaying_4; }
	inline void set_isPlaying_4(bool value)
	{
		___isPlaying_4 = value;
	}

	inline static int32_t get_offset_of_riseAmount_5() { return static_cast<int32_t>(offsetof(GameElevenManager_t2036900502, ___riseAmount_5)); }
	inline float get_riseAmount_5() const { return ___riseAmount_5; }
	inline float* get_address_of_riseAmount_5() { return &___riseAmount_5; }
	inline void set_riseAmount_5(float value)
	{
		___riseAmount_5 = value;
	}

	inline static int32_t get_offset_of_riseSpeed_6() { return static_cast<int32_t>(offsetof(GameElevenManager_t2036900502, ___riseSpeed_6)); }
	inline float get_riseSpeed_6() const { return ___riseSpeed_6; }
	inline float* get_address_of_riseSpeed_6() { return &___riseSpeed_6; }
	inline void set_riseSpeed_6(float value)
	{
		___riseSpeed_6 = value;
	}

	inline static int32_t get_offset_of_spawnRate_7() { return static_cast<int32_t>(offsetof(GameElevenManager_t2036900502, ___spawnRate_7)); }
	inline float get_spawnRate_7() const { return ___spawnRate_7; }
	inline float* get_address_of_spawnRate_7() { return &___spawnRate_7; }
	inline void set_spawnRate_7(float value)
	{
		___spawnRate_7 = value;
	}

	inline static int32_t get_offset_of_xThreshold_8() { return static_cast<int32_t>(offsetof(GameElevenManager_t2036900502, ___xThreshold_8)); }
	inline float get_xThreshold_8() const { return ___xThreshold_8; }
	inline float* get_address_of_xThreshold_8() { return &___xThreshold_8; }
	inline void set_xThreshold_8(float value)
	{
		___xThreshold_8 = value;
	}

	inline static int32_t get_offset_of_flakesToHit_9() { return static_cast<int32_t>(offsetof(GameElevenManager_t2036900502, ___flakesToHit_9)); }
	inline int32_t get_flakesToHit_9() const { return ___flakesToHit_9; }
	inline int32_t* get_address_of_flakesToHit_9() { return &___flakesToHit_9; }
	inline void set_flakesToHit_9(int32_t value)
	{
		___flakesToHit_9 = value;
	}

	inline static int32_t get_offset_of_snowPile_10() { return static_cast<int32_t>(offsetof(GameElevenManager_t2036900502, ___snowPile_10)); }
	inline Transform_t3275118058 * get_snowPile_10() const { return ___snowPile_10; }
	inline Transform_t3275118058 ** get_address_of_snowPile_10() { return &___snowPile_10; }
	inline void set_snowPile_10(Transform_t3275118058 * value)
	{
		___snowPile_10 = value;
		Il2CppCodeGenWriteBarrier(&___snowPile_10, value);
	}

	inline static int32_t get_offset_of_waterSnow_11() { return static_cast<int32_t>(offsetof(GameElevenManager_t2036900502, ___waterSnow_11)); }
	inline GameObject_t1756533147 * get_waterSnow_11() const { return ___waterSnow_11; }
	inline GameObject_t1756533147 ** get_address_of_waterSnow_11() { return &___waterSnow_11; }
	inline void set_waterSnow_11(GameObject_t1756533147 * value)
	{
		___waterSnow_11 = value;
		Il2CppCodeGenWriteBarrier(&___waterSnow_11, value);
	}

	inline static int32_t get_offset_of_snowflakeSprites_12() { return static_cast<int32_t>(offsetof(GameElevenManager_t2036900502, ___snowflakeSprites_12)); }
	inline SpriteU5BU5D_t3359083662* get_snowflakeSprites_12() const { return ___snowflakeSprites_12; }
	inline SpriteU5BU5D_t3359083662** get_address_of_snowflakeSprites_12() { return &___snowflakeSprites_12; }
	inline void set_snowflakeSprites_12(SpriteU5BU5D_t3359083662* value)
	{
		___snowflakeSprites_12 = value;
		Il2CppCodeGenWriteBarrier(&___snowflakeSprites_12, value);
	}

	inline static int32_t get_offset_of_snowHitCount_13() { return static_cast<int32_t>(offsetof(GameElevenManager_t2036900502, ___snowHitCount_13)); }
	inline int32_t get_snowHitCount_13() const { return ___snowHitCount_13; }
	inline int32_t* get_address_of_snowHitCount_13() { return &___snowHitCount_13; }
	inline void set_snowHitCount_13(int32_t value)
	{
		___snowHitCount_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
