﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GiftSlot
struct  GiftSlot_t3101111170  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 GiftSlot::slotIndex
	int32_t ___slotIndex_2;

public:
	inline static int32_t get_offset_of_slotIndex_2() { return static_cast<int32_t>(offsetof(GiftSlot_t3101111170, ___slotIndex_2)); }
	inline int32_t get_slotIndex_2() const { return ___slotIndex_2; }
	inline int32_t* get_address_of_slotIndex_2() { return &___slotIndex_2; }
	inline void set_slotIndex_2(int32_t value)
	{
		___slotIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
