﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen1785820572.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t1098056643;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTwelveManager
struct  GameTwelveManager_t2786305482  : public SingletonBase_1_t1785820572
{
public:
	// UnityEngine.AudioSource GameTwelveManager::tickingSound
	AudioSource_t1135106623 * ___tickingSound_4;
	// UnityEngine.AudioSource GameTwelveManager::tickingSoundFast
	AudioSource_t1135106623 * ___tickingSoundFast_5;
	// System.Int32 GameTwelveManager::itemsToFind
	int32_t ___itemsToFind_6;
	// System.Int32 GameTwelveManager::itemsFound
	int32_t ___itemsFound_7;
	// UnityEngine.UI.Text GameTwelveManager::textTimer
	Text_t356221433 * ___textTimer_8;
	// System.Single GameTwelveManager::secondsToPlay
	float ___secondsToPlay_9;
	// UnityEngine.Vector3 GameTwelveManager::exampleSize
	Vector3_t2243707580  ___exampleSize_10;
	// UnityEngine.SpriteRenderer[] GameTwelveManager::itemSprites
	SpriteRendererU5BU5D_t1098056643* ___itemSprites_11;
	// System.Boolean[] GameTwelveManager::spriteUsed
	BooleanU5BU5D_t3568034315* ___spriteUsed_12;
	// UnityEngine.SpriteRenderer GameTwelveManager::exampleItem
	SpriteRenderer_t1209076198 * ___exampleItem_13;
	// System.Boolean GameTwelveManager::canClick
	bool ___canClick_14;

public:
	inline static int32_t get_offset_of_tickingSound_4() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___tickingSound_4)); }
	inline AudioSource_t1135106623 * get_tickingSound_4() const { return ___tickingSound_4; }
	inline AudioSource_t1135106623 ** get_address_of_tickingSound_4() { return &___tickingSound_4; }
	inline void set_tickingSound_4(AudioSource_t1135106623 * value)
	{
		___tickingSound_4 = value;
		Il2CppCodeGenWriteBarrier(&___tickingSound_4, value);
	}

	inline static int32_t get_offset_of_tickingSoundFast_5() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___tickingSoundFast_5)); }
	inline AudioSource_t1135106623 * get_tickingSoundFast_5() const { return ___tickingSoundFast_5; }
	inline AudioSource_t1135106623 ** get_address_of_tickingSoundFast_5() { return &___tickingSoundFast_5; }
	inline void set_tickingSoundFast_5(AudioSource_t1135106623 * value)
	{
		___tickingSoundFast_5 = value;
		Il2CppCodeGenWriteBarrier(&___tickingSoundFast_5, value);
	}

	inline static int32_t get_offset_of_itemsToFind_6() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___itemsToFind_6)); }
	inline int32_t get_itemsToFind_6() const { return ___itemsToFind_6; }
	inline int32_t* get_address_of_itemsToFind_6() { return &___itemsToFind_6; }
	inline void set_itemsToFind_6(int32_t value)
	{
		___itemsToFind_6 = value;
	}

	inline static int32_t get_offset_of_itemsFound_7() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___itemsFound_7)); }
	inline int32_t get_itemsFound_7() const { return ___itemsFound_7; }
	inline int32_t* get_address_of_itemsFound_7() { return &___itemsFound_7; }
	inline void set_itemsFound_7(int32_t value)
	{
		___itemsFound_7 = value;
	}

	inline static int32_t get_offset_of_textTimer_8() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___textTimer_8)); }
	inline Text_t356221433 * get_textTimer_8() const { return ___textTimer_8; }
	inline Text_t356221433 ** get_address_of_textTimer_8() { return &___textTimer_8; }
	inline void set_textTimer_8(Text_t356221433 * value)
	{
		___textTimer_8 = value;
		Il2CppCodeGenWriteBarrier(&___textTimer_8, value);
	}

	inline static int32_t get_offset_of_secondsToPlay_9() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___secondsToPlay_9)); }
	inline float get_secondsToPlay_9() const { return ___secondsToPlay_9; }
	inline float* get_address_of_secondsToPlay_9() { return &___secondsToPlay_9; }
	inline void set_secondsToPlay_9(float value)
	{
		___secondsToPlay_9 = value;
	}

	inline static int32_t get_offset_of_exampleSize_10() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___exampleSize_10)); }
	inline Vector3_t2243707580  get_exampleSize_10() const { return ___exampleSize_10; }
	inline Vector3_t2243707580 * get_address_of_exampleSize_10() { return &___exampleSize_10; }
	inline void set_exampleSize_10(Vector3_t2243707580  value)
	{
		___exampleSize_10 = value;
	}

	inline static int32_t get_offset_of_itemSprites_11() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___itemSprites_11)); }
	inline SpriteRendererU5BU5D_t1098056643* get_itemSprites_11() const { return ___itemSprites_11; }
	inline SpriteRendererU5BU5D_t1098056643** get_address_of_itemSprites_11() { return &___itemSprites_11; }
	inline void set_itemSprites_11(SpriteRendererU5BU5D_t1098056643* value)
	{
		___itemSprites_11 = value;
		Il2CppCodeGenWriteBarrier(&___itemSprites_11, value);
	}

	inline static int32_t get_offset_of_spriteUsed_12() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___spriteUsed_12)); }
	inline BooleanU5BU5D_t3568034315* get_spriteUsed_12() const { return ___spriteUsed_12; }
	inline BooleanU5BU5D_t3568034315** get_address_of_spriteUsed_12() { return &___spriteUsed_12; }
	inline void set_spriteUsed_12(BooleanU5BU5D_t3568034315* value)
	{
		___spriteUsed_12 = value;
		Il2CppCodeGenWriteBarrier(&___spriteUsed_12, value);
	}

	inline static int32_t get_offset_of_exampleItem_13() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___exampleItem_13)); }
	inline SpriteRenderer_t1209076198 * get_exampleItem_13() const { return ___exampleItem_13; }
	inline SpriteRenderer_t1209076198 ** get_address_of_exampleItem_13() { return &___exampleItem_13; }
	inline void set_exampleItem_13(SpriteRenderer_t1209076198 * value)
	{
		___exampleItem_13 = value;
		Il2CppCodeGenWriteBarrier(&___exampleItem_13, value);
	}

	inline static int32_t get_offset_of_canClick_14() { return static_cast<int32_t>(offsetof(GameTwelveManager_t2786305482, ___canClick_14)); }
	inline bool get_canClick_14() const { return ___canClick_14; }
	inline bool* get_address_of_canClick_14() { return &___canClick_14; }
	inline void set_canClick_14(bool value)
	{
		___canClick_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
