﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Data.RedundantFileWriter/DelayedWriteReference
struct  DelayedWriteReference_t933455898  : public Il2CppObject
{
public:
	// UnityEngine.Coroutine GameDevToolkit.Data.RedundantFileWriter/DelayedWriteReference::delay
	Coroutine_t2299508840 * ___delay_0;
	// System.String GameDevToolkit.Data.RedundantFileWriter/DelayedWriteReference::path
	String_t* ___path_1;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(DelayedWriteReference_t933455898, ___delay_0)); }
	inline Coroutine_t2299508840 * get_delay_0() const { return ___delay_0; }
	inline Coroutine_t2299508840 ** get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(Coroutine_t2299508840 * value)
	{
		___delay_0 = value;
		Il2CppCodeGenWriteBarrier(&___delay_0, value);
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(DelayedWriteReference_t933455898, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier(&___path_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
