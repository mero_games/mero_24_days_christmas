﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GiftDrawer
struct  GiftDrawer_t3411291385  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture GiftDrawer::wrapSprite
	Texture_t2243626319 * ___wrapSprite_2;
	// UnityEngine.Texture GiftDrawer::giftSprite
	Texture_t2243626319 * ___giftSprite_3;
	// UnityEngine.UI.RawImage GiftDrawer::img
	RawImage_t2749640213 * ___img_4;
	// UnityEngine.UI.RawImage GiftDrawer::img2
	RawImage_t2749640213 * ___img2_5;
	// UnityEngine.Texture GiftDrawer::clear
	Texture_t2243626319 * ___clear_6;
	// UnityEngine.Material GiftDrawer::clearMaterial
	Material_t193706927 * ___clearMaterial_7;
	// System.Single GiftDrawer::time
	float ___time_8;
	// UnityEngine.Vector2 GiftDrawer::startPos
	Vector2_t2243707579  ___startPos_9;
	// UnityEngine.Vector2 GiftDrawer::endPos
	Vector2_t2243707579  ___endPos_10;
	// System.Boolean GiftDrawer::<canDraw>k__BackingField
	bool ___U3CcanDrawU3Ek__BackingField_11;
	// System.Int32 GiftDrawer::texwidth
	int32_t ___texwidth_12;
	// System.Int32 GiftDrawer::texheight
	int32_t ___texheight_13;
	// UnityEngine.Vector2 GiftDrawer::size
	Vector2_t2243707579  ___size_14;
	// System.Single GiftDrawer::width
	float ___width_15;
	// System.Single GiftDrawer::height
	float ___height_16;
	// System.Single GiftDrawer::minbordx
	float ___minbordx_17;
	// System.Single GiftDrawer::minbordy
	float ___minbordy_18;
	// System.Single GiftDrawer::maxbordx
	float ___maxbordx_19;
	// System.Single GiftDrawer::maxbordy
	float ___maxbordy_20;
	// UnityEngine.RenderTexture GiftDrawer::texture
	RenderTexture_t2666733923 * ___texture_21;
	// UnityEngine.Vector2 GiftDrawer::oldPos
	Vector2_t2243707579  ___oldPos_22;
	// UnityEngine.Vector2 GiftDrawer::pos
	Vector2_t2243707579  ___pos_23;
	// System.Single GiftDrawer::count
	float ___count_24;
	// UnityEngine.Texture2D GiftDrawer::testTex
	Texture2D_t3542995729 * ___testTex_25;

public:
	inline static int32_t get_offset_of_wrapSprite_2() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___wrapSprite_2)); }
	inline Texture_t2243626319 * get_wrapSprite_2() const { return ___wrapSprite_2; }
	inline Texture_t2243626319 ** get_address_of_wrapSprite_2() { return &___wrapSprite_2; }
	inline void set_wrapSprite_2(Texture_t2243626319 * value)
	{
		___wrapSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___wrapSprite_2, value);
	}

	inline static int32_t get_offset_of_giftSprite_3() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___giftSprite_3)); }
	inline Texture_t2243626319 * get_giftSprite_3() const { return ___giftSprite_3; }
	inline Texture_t2243626319 ** get_address_of_giftSprite_3() { return &___giftSprite_3; }
	inline void set_giftSprite_3(Texture_t2243626319 * value)
	{
		___giftSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___giftSprite_3, value);
	}

	inline static int32_t get_offset_of_img_4() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___img_4)); }
	inline RawImage_t2749640213 * get_img_4() const { return ___img_4; }
	inline RawImage_t2749640213 ** get_address_of_img_4() { return &___img_4; }
	inline void set_img_4(RawImage_t2749640213 * value)
	{
		___img_4 = value;
		Il2CppCodeGenWriteBarrier(&___img_4, value);
	}

	inline static int32_t get_offset_of_img2_5() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___img2_5)); }
	inline RawImage_t2749640213 * get_img2_5() const { return ___img2_5; }
	inline RawImage_t2749640213 ** get_address_of_img2_5() { return &___img2_5; }
	inline void set_img2_5(RawImage_t2749640213 * value)
	{
		___img2_5 = value;
		Il2CppCodeGenWriteBarrier(&___img2_5, value);
	}

	inline static int32_t get_offset_of_clear_6() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___clear_6)); }
	inline Texture_t2243626319 * get_clear_6() const { return ___clear_6; }
	inline Texture_t2243626319 ** get_address_of_clear_6() { return &___clear_6; }
	inline void set_clear_6(Texture_t2243626319 * value)
	{
		___clear_6 = value;
		Il2CppCodeGenWriteBarrier(&___clear_6, value);
	}

	inline static int32_t get_offset_of_clearMaterial_7() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___clearMaterial_7)); }
	inline Material_t193706927 * get_clearMaterial_7() const { return ___clearMaterial_7; }
	inline Material_t193706927 ** get_address_of_clearMaterial_7() { return &___clearMaterial_7; }
	inline void set_clearMaterial_7(Material_t193706927 * value)
	{
		___clearMaterial_7 = value;
		Il2CppCodeGenWriteBarrier(&___clearMaterial_7, value);
	}

	inline static int32_t get_offset_of_time_8() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___time_8)); }
	inline float get_time_8() const { return ___time_8; }
	inline float* get_address_of_time_8() { return &___time_8; }
	inline void set_time_8(float value)
	{
		___time_8 = value;
	}

	inline static int32_t get_offset_of_startPos_9() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___startPos_9)); }
	inline Vector2_t2243707579  get_startPos_9() const { return ___startPos_9; }
	inline Vector2_t2243707579 * get_address_of_startPos_9() { return &___startPos_9; }
	inline void set_startPos_9(Vector2_t2243707579  value)
	{
		___startPos_9 = value;
	}

	inline static int32_t get_offset_of_endPos_10() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___endPos_10)); }
	inline Vector2_t2243707579  get_endPos_10() const { return ___endPos_10; }
	inline Vector2_t2243707579 * get_address_of_endPos_10() { return &___endPos_10; }
	inline void set_endPos_10(Vector2_t2243707579  value)
	{
		___endPos_10 = value;
	}

	inline static int32_t get_offset_of_U3CcanDrawU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___U3CcanDrawU3Ek__BackingField_11)); }
	inline bool get_U3CcanDrawU3Ek__BackingField_11() const { return ___U3CcanDrawU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CcanDrawU3Ek__BackingField_11() { return &___U3CcanDrawU3Ek__BackingField_11; }
	inline void set_U3CcanDrawU3Ek__BackingField_11(bool value)
	{
		___U3CcanDrawU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_texwidth_12() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___texwidth_12)); }
	inline int32_t get_texwidth_12() const { return ___texwidth_12; }
	inline int32_t* get_address_of_texwidth_12() { return &___texwidth_12; }
	inline void set_texwidth_12(int32_t value)
	{
		___texwidth_12 = value;
	}

	inline static int32_t get_offset_of_texheight_13() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___texheight_13)); }
	inline int32_t get_texheight_13() const { return ___texheight_13; }
	inline int32_t* get_address_of_texheight_13() { return &___texheight_13; }
	inline void set_texheight_13(int32_t value)
	{
		___texheight_13 = value;
	}

	inline static int32_t get_offset_of_size_14() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___size_14)); }
	inline Vector2_t2243707579  get_size_14() const { return ___size_14; }
	inline Vector2_t2243707579 * get_address_of_size_14() { return &___size_14; }
	inline void set_size_14(Vector2_t2243707579  value)
	{
		___size_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_height_16() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___height_16)); }
	inline float get_height_16() const { return ___height_16; }
	inline float* get_address_of_height_16() { return &___height_16; }
	inline void set_height_16(float value)
	{
		___height_16 = value;
	}

	inline static int32_t get_offset_of_minbordx_17() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___minbordx_17)); }
	inline float get_minbordx_17() const { return ___minbordx_17; }
	inline float* get_address_of_minbordx_17() { return &___minbordx_17; }
	inline void set_minbordx_17(float value)
	{
		___minbordx_17 = value;
	}

	inline static int32_t get_offset_of_minbordy_18() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___minbordy_18)); }
	inline float get_minbordy_18() const { return ___minbordy_18; }
	inline float* get_address_of_minbordy_18() { return &___minbordy_18; }
	inline void set_minbordy_18(float value)
	{
		___minbordy_18 = value;
	}

	inline static int32_t get_offset_of_maxbordx_19() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___maxbordx_19)); }
	inline float get_maxbordx_19() const { return ___maxbordx_19; }
	inline float* get_address_of_maxbordx_19() { return &___maxbordx_19; }
	inline void set_maxbordx_19(float value)
	{
		___maxbordx_19 = value;
	}

	inline static int32_t get_offset_of_maxbordy_20() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___maxbordy_20)); }
	inline float get_maxbordy_20() const { return ___maxbordy_20; }
	inline float* get_address_of_maxbordy_20() { return &___maxbordy_20; }
	inline void set_maxbordy_20(float value)
	{
		___maxbordy_20 = value;
	}

	inline static int32_t get_offset_of_texture_21() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___texture_21)); }
	inline RenderTexture_t2666733923 * get_texture_21() const { return ___texture_21; }
	inline RenderTexture_t2666733923 ** get_address_of_texture_21() { return &___texture_21; }
	inline void set_texture_21(RenderTexture_t2666733923 * value)
	{
		___texture_21 = value;
		Il2CppCodeGenWriteBarrier(&___texture_21, value);
	}

	inline static int32_t get_offset_of_oldPos_22() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___oldPos_22)); }
	inline Vector2_t2243707579  get_oldPos_22() const { return ___oldPos_22; }
	inline Vector2_t2243707579 * get_address_of_oldPos_22() { return &___oldPos_22; }
	inline void set_oldPos_22(Vector2_t2243707579  value)
	{
		___oldPos_22 = value;
	}

	inline static int32_t get_offset_of_pos_23() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___pos_23)); }
	inline Vector2_t2243707579  get_pos_23() const { return ___pos_23; }
	inline Vector2_t2243707579 * get_address_of_pos_23() { return &___pos_23; }
	inline void set_pos_23(Vector2_t2243707579  value)
	{
		___pos_23 = value;
	}

	inline static int32_t get_offset_of_count_24() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___count_24)); }
	inline float get_count_24() const { return ___count_24; }
	inline float* get_address_of_count_24() { return &___count_24; }
	inline void set_count_24(float value)
	{
		___count_24 = value;
	}

	inline static int32_t get_offset_of_testTex_25() { return static_cast<int32_t>(offsetof(GiftDrawer_t3411291385, ___testTex_25)); }
	inline Texture2D_t3542995729 * get_testTex_25() const { return ___testTex_25; }
	inline Texture2D_t3542995729 ** get_address_of_testTex_25() { return &___testTex_25; }
	inline void set_testTex_25(Texture2D_t3542995729 * value)
	{
		___testTex_25 = value;
		Il2CppCodeGenWriteBarrier(&___testTex_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
