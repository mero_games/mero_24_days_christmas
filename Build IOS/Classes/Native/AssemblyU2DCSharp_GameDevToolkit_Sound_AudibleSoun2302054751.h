﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameDevToolkit.Sound.AudibleSound/AudibleSoundDelegate
struct AudibleSoundDelegate_t3383377214;
// GameDevToolkit.Sound.SoundClip
struct SoundClip_t3743947423;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// GameDevToolkit.Sound.SoundManagerEventProccessor
struct SoundManagerEventProccessor_t3825530019;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Sound.AudibleSound
struct  AudibleSound_t2302054751  : public Il2CppObject
{
public:
	// GameDevToolkit.Sound.AudibleSound/AudibleSoundDelegate GameDevToolkit.Sound.AudibleSound::SoundComplete
	AudibleSoundDelegate_t3383377214 * ___SoundComplete_0;
	// GameDevToolkit.Sound.SoundClip GameDevToolkit.Sound.AudibleSound::_sound
	SoundClip_t3743947423 * ____sound_1;
	// System.Boolean GameDevToolkit.Sound.AudibleSound::_updateVolumeFromLayer
	bool ____updateVolumeFromLayer_2;
	// UnityEngine.Coroutine GameDevToolkit.Sound.AudibleSound::dispatcher
	Coroutine_t2299508840 * ___dispatcher_3;
	// UnityEngine.AudioSource GameDevToolkit.Sound.AudibleSound::source
	AudioSource_t1135106623 * ___source_4;
	// UnityEngine.GameObject GameDevToolkit.Sound.AudibleSound::targetGameObject
	GameObject_t1756533147 * ___targetGameObject_5;
	// GameDevToolkit.Sound.SoundManagerEventProccessor GameDevToolkit.Sound.AudibleSound::proccessor
	SoundManagerEventProccessor_t3825530019 * ___proccessor_6;

public:
	inline static int32_t get_offset_of_SoundComplete_0() { return static_cast<int32_t>(offsetof(AudibleSound_t2302054751, ___SoundComplete_0)); }
	inline AudibleSoundDelegate_t3383377214 * get_SoundComplete_0() const { return ___SoundComplete_0; }
	inline AudibleSoundDelegate_t3383377214 ** get_address_of_SoundComplete_0() { return &___SoundComplete_0; }
	inline void set_SoundComplete_0(AudibleSoundDelegate_t3383377214 * value)
	{
		___SoundComplete_0 = value;
		Il2CppCodeGenWriteBarrier(&___SoundComplete_0, value);
	}

	inline static int32_t get_offset_of__sound_1() { return static_cast<int32_t>(offsetof(AudibleSound_t2302054751, ____sound_1)); }
	inline SoundClip_t3743947423 * get__sound_1() const { return ____sound_1; }
	inline SoundClip_t3743947423 ** get_address_of__sound_1() { return &____sound_1; }
	inline void set__sound_1(SoundClip_t3743947423 * value)
	{
		____sound_1 = value;
		Il2CppCodeGenWriteBarrier(&____sound_1, value);
	}

	inline static int32_t get_offset_of__updateVolumeFromLayer_2() { return static_cast<int32_t>(offsetof(AudibleSound_t2302054751, ____updateVolumeFromLayer_2)); }
	inline bool get__updateVolumeFromLayer_2() const { return ____updateVolumeFromLayer_2; }
	inline bool* get_address_of__updateVolumeFromLayer_2() { return &____updateVolumeFromLayer_2; }
	inline void set__updateVolumeFromLayer_2(bool value)
	{
		____updateVolumeFromLayer_2 = value;
	}

	inline static int32_t get_offset_of_dispatcher_3() { return static_cast<int32_t>(offsetof(AudibleSound_t2302054751, ___dispatcher_3)); }
	inline Coroutine_t2299508840 * get_dispatcher_3() const { return ___dispatcher_3; }
	inline Coroutine_t2299508840 ** get_address_of_dispatcher_3() { return &___dispatcher_3; }
	inline void set_dispatcher_3(Coroutine_t2299508840 * value)
	{
		___dispatcher_3 = value;
		Il2CppCodeGenWriteBarrier(&___dispatcher_3, value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(AudibleSound_t2302054751, ___source_4)); }
	inline AudioSource_t1135106623 * get_source_4() const { return ___source_4; }
	inline AudioSource_t1135106623 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(AudioSource_t1135106623 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier(&___source_4, value);
	}

	inline static int32_t get_offset_of_targetGameObject_5() { return static_cast<int32_t>(offsetof(AudibleSound_t2302054751, ___targetGameObject_5)); }
	inline GameObject_t1756533147 * get_targetGameObject_5() const { return ___targetGameObject_5; }
	inline GameObject_t1756533147 ** get_address_of_targetGameObject_5() { return &___targetGameObject_5; }
	inline void set_targetGameObject_5(GameObject_t1756533147 * value)
	{
		___targetGameObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___targetGameObject_5, value);
	}

	inline static int32_t get_offset_of_proccessor_6() { return static_cast<int32_t>(offsetof(AudibleSound_t2302054751, ___proccessor_6)); }
	inline SoundManagerEventProccessor_t3825530019 * get_proccessor_6() const { return ___proccessor_6; }
	inline SoundManagerEventProccessor_t3825530019 ** get_address_of_proccessor_6() { return &___proccessor_6; }
	inline void set_proccessor_6(SoundManagerEventProccessor_t3825530019 * value)
	{
		___proccessor_6 = value;
		Il2CppCodeGenWriteBarrier(&___proccessor_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
