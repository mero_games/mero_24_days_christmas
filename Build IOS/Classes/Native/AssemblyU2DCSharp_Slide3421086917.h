﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Slide
struct  Slide_t3421086917  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.SpriteRenderer Slide::mySpriteRenderer
	SpriteRenderer_t1209076198 * ___mySpriteRenderer_2;

public:
	inline static int32_t get_offset_of_mySpriteRenderer_2() { return static_cast<int32_t>(offsetof(Slide_t3421086917, ___mySpriteRenderer_2)); }
	inline SpriteRenderer_t1209076198 * get_mySpriteRenderer_2() const { return ___mySpriteRenderer_2; }
	inline SpriteRenderer_t1209076198 ** get_address_of_mySpriteRenderer_2() { return &___mySpriteRenderer_2; }
	inline void set_mySpriteRenderer_2(SpriteRenderer_t1209076198 * value)
	{
		___mySpriteRenderer_2 = value;
		Il2CppCodeGenWriteBarrier(&___mySpriteRenderer_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
