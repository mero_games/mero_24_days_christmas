﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen576649606.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t1098056643;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTenManager
struct  GameTenManager_t1577134516  : public SingletonBase_1_t576649606
{
public:
	// System.Int32 GameTenManager::correctCounter
	int32_t ___correctCounter_4;
	// UnityEngine.Transform GameTenManager::drawings
	Transform_t3275118058 * ___drawings_5;
	// UnityEngine.Collider2D GameTenManager::startCollider
	Collider2D_t646061738 * ___startCollider_6;
	// UnityEngine.Color[] GameTenManager::drawColors
	ColorU5BU5D_t672350442* ___drawColors_7;
	// UnityEngine.Sprite[] GameTenManager::unwrappedGifts
	SpriteU5BU5D_t3359083662* ___unwrappedGifts_8;
	// UnityEngine.Sprite[] GameTenManager::wrappedGifts
	SpriteU5BU5D_t3359083662* ___wrappedGifts_9;
	// UnityEngine.SpriteRenderer[] GameTenManager::topSlots
	SpriteRendererU5BU5D_t1098056643* ___topSlots_10;
	// UnityEngine.SpriteRenderer[] GameTenManager::bottomSlots
	SpriteRendererU5BU5D_t1098056643* ___bottomSlots_11;
	// UnityEngine.GameObject GameTenManager::pencil
	GameObject_t1756533147 * ___pencil_12;
	// UnityEngine.GameObject GameTenManager::iPencil
	GameObject_t1756533147 * ___iPencil_13;
	// System.Boolean[] GameTenManager::beenUsed
	BooleanU5BU5D_t3568034315* ___beenUsed_14;

public:
	inline static int32_t get_offset_of_correctCounter_4() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___correctCounter_4)); }
	inline int32_t get_correctCounter_4() const { return ___correctCounter_4; }
	inline int32_t* get_address_of_correctCounter_4() { return &___correctCounter_4; }
	inline void set_correctCounter_4(int32_t value)
	{
		___correctCounter_4 = value;
	}

	inline static int32_t get_offset_of_drawings_5() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___drawings_5)); }
	inline Transform_t3275118058 * get_drawings_5() const { return ___drawings_5; }
	inline Transform_t3275118058 ** get_address_of_drawings_5() { return &___drawings_5; }
	inline void set_drawings_5(Transform_t3275118058 * value)
	{
		___drawings_5 = value;
		Il2CppCodeGenWriteBarrier(&___drawings_5, value);
	}

	inline static int32_t get_offset_of_startCollider_6() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___startCollider_6)); }
	inline Collider2D_t646061738 * get_startCollider_6() const { return ___startCollider_6; }
	inline Collider2D_t646061738 ** get_address_of_startCollider_6() { return &___startCollider_6; }
	inline void set_startCollider_6(Collider2D_t646061738 * value)
	{
		___startCollider_6 = value;
		Il2CppCodeGenWriteBarrier(&___startCollider_6, value);
	}

	inline static int32_t get_offset_of_drawColors_7() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___drawColors_7)); }
	inline ColorU5BU5D_t672350442* get_drawColors_7() const { return ___drawColors_7; }
	inline ColorU5BU5D_t672350442** get_address_of_drawColors_7() { return &___drawColors_7; }
	inline void set_drawColors_7(ColorU5BU5D_t672350442* value)
	{
		___drawColors_7 = value;
		Il2CppCodeGenWriteBarrier(&___drawColors_7, value);
	}

	inline static int32_t get_offset_of_unwrappedGifts_8() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___unwrappedGifts_8)); }
	inline SpriteU5BU5D_t3359083662* get_unwrappedGifts_8() const { return ___unwrappedGifts_8; }
	inline SpriteU5BU5D_t3359083662** get_address_of_unwrappedGifts_8() { return &___unwrappedGifts_8; }
	inline void set_unwrappedGifts_8(SpriteU5BU5D_t3359083662* value)
	{
		___unwrappedGifts_8 = value;
		Il2CppCodeGenWriteBarrier(&___unwrappedGifts_8, value);
	}

	inline static int32_t get_offset_of_wrappedGifts_9() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___wrappedGifts_9)); }
	inline SpriteU5BU5D_t3359083662* get_wrappedGifts_9() const { return ___wrappedGifts_9; }
	inline SpriteU5BU5D_t3359083662** get_address_of_wrappedGifts_9() { return &___wrappedGifts_9; }
	inline void set_wrappedGifts_9(SpriteU5BU5D_t3359083662* value)
	{
		___wrappedGifts_9 = value;
		Il2CppCodeGenWriteBarrier(&___wrappedGifts_9, value);
	}

	inline static int32_t get_offset_of_topSlots_10() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___topSlots_10)); }
	inline SpriteRendererU5BU5D_t1098056643* get_topSlots_10() const { return ___topSlots_10; }
	inline SpriteRendererU5BU5D_t1098056643** get_address_of_topSlots_10() { return &___topSlots_10; }
	inline void set_topSlots_10(SpriteRendererU5BU5D_t1098056643* value)
	{
		___topSlots_10 = value;
		Il2CppCodeGenWriteBarrier(&___topSlots_10, value);
	}

	inline static int32_t get_offset_of_bottomSlots_11() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___bottomSlots_11)); }
	inline SpriteRendererU5BU5D_t1098056643* get_bottomSlots_11() const { return ___bottomSlots_11; }
	inline SpriteRendererU5BU5D_t1098056643** get_address_of_bottomSlots_11() { return &___bottomSlots_11; }
	inline void set_bottomSlots_11(SpriteRendererU5BU5D_t1098056643* value)
	{
		___bottomSlots_11 = value;
		Il2CppCodeGenWriteBarrier(&___bottomSlots_11, value);
	}

	inline static int32_t get_offset_of_pencil_12() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___pencil_12)); }
	inline GameObject_t1756533147 * get_pencil_12() const { return ___pencil_12; }
	inline GameObject_t1756533147 ** get_address_of_pencil_12() { return &___pencil_12; }
	inline void set_pencil_12(GameObject_t1756533147 * value)
	{
		___pencil_12 = value;
		Il2CppCodeGenWriteBarrier(&___pencil_12, value);
	}

	inline static int32_t get_offset_of_iPencil_13() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___iPencil_13)); }
	inline GameObject_t1756533147 * get_iPencil_13() const { return ___iPencil_13; }
	inline GameObject_t1756533147 ** get_address_of_iPencil_13() { return &___iPencil_13; }
	inline void set_iPencil_13(GameObject_t1756533147 * value)
	{
		___iPencil_13 = value;
		Il2CppCodeGenWriteBarrier(&___iPencil_13, value);
	}

	inline static int32_t get_offset_of_beenUsed_14() { return static_cast<int32_t>(offsetof(GameTenManager_t1577134516, ___beenUsed_14)); }
	inline BooleanU5BU5D_t3568034315* get_beenUsed_14() const { return ___beenUsed_14; }
	inline BooleanU5BU5D_t3568034315** get_address_of_beenUsed_14() { return &___beenUsed_14; }
	inline void set_beenUsed_14(BooleanU5BU5D_t3568034315* value)
	{
		___beenUsed_14 = value;
		Il2CppCodeGenWriteBarrier(&___beenUsed_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
