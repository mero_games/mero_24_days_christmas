﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen2632461821.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// SongBubble
struct SongBubble_t2997910009;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Animator
struct Animator_t69676727;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameNineManager
struct  GameNineManager_t3632946731  : public SingletonBase_1_t2632461821
{
public:
	// System.Boolean GameNineManager::isPlaying
	bool ___isPlaying_4;
	// System.Int32 GameNineManager::faceIndex
	int32_t ___faceIndex_5;
	// System.Int32 GameNineManager::neckIndex
	int32_t ___neckIndex_6;
	// System.Int32 GameNineManager::eyeIndex
	int32_t ___eyeIndex_7;
	// System.Int32 GameNineManager::hairIndex
	int32_t ___hairIndex_8;
	// System.Single GameNineManager::songNoteSpawnInterval
	float ___songNoteSpawnInterval_9;
	// System.Single GameNineManager::elfShrinkTime
	float ___elfShrinkTime_10;
	// UnityEngine.Vector3 GameNineManager::elfTargetScale
	Vector3_t2243707580  ___elfTargetScale_11;
	// UnityEngine.Vector3 GameNineManager::elfTargetPosition
	Vector3_t2243707580  ___elfTargetPosition_12;
	// SongBubble GameNineManager::songBubble
	SongBubble_t2997910009 * ___songBubble_13;
	// UnityEngine.GameObject GameNineManager::elf
	GameObject_t1756533147 * ___elf_14;
	// System.String[] GameNineManager::animationNames
	StringU5BU5D_t1642385972* ___animationNames_15;
	// UnityEngine.Sprite[] GameNineManager::faces
	SpriteU5BU5D_t3359083662* ___faces_16;
	// UnityEngine.Sprite[] GameNineManager::necks
	SpriteU5BU5D_t3359083662* ___necks_17;
	// UnityEngine.Sprite[] GameNineManager::eyes
	SpriteU5BU5D_t3359083662* ___eyes_18;
	// UnityEngine.Sprite[] GameNineManager::hairs
	SpriteU5BU5D_t3359083662* ___hairs_19;
	// UnityEngine.SpriteRenderer GameNineManager::face
	SpriteRenderer_t1209076198 * ___face_20;
	// UnityEngine.SpriteRenderer GameNineManager::neck
	SpriteRenderer_t1209076198 * ___neck_21;
	// UnityEngine.SpriteRenderer GameNineManager::eye
	SpriteRenderer_t1209076198 * ___eye_22;
	// UnityEngine.SpriteRenderer GameNineManager::hair
	SpriteRenderer_t1209076198 * ___hair_23;
	// UnityEngine.Animator GameNineManager::elfAnimator
	Animator_t69676727 * ___elfAnimator_24;

public:
	inline static int32_t get_offset_of_isPlaying_4() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___isPlaying_4)); }
	inline bool get_isPlaying_4() const { return ___isPlaying_4; }
	inline bool* get_address_of_isPlaying_4() { return &___isPlaying_4; }
	inline void set_isPlaying_4(bool value)
	{
		___isPlaying_4 = value;
	}

	inline static int32_t get_offset_of_faceIndex_5() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___faceIndex_5)); }
	inline int32_t get_faceIndex_5() const { return ___faceIndex_5; }
	inline int32_t* get_address_of_faceIndex_5() { return &___faceIndex_5; }
	inline void set_faceIndex_5(int32_t value)
	{
		___faceIndex_5 = value;
	}

	inline static int32_t get_offset_of_neckIndex_6() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___neckIndex_6)); }
	inline int32_t get_neckIndex_6() const { return ___neckIndex_6; }
	inline int32_t* get_address_of_neckIndex_6() { return &___neckIndex_6; }
	inline void set_neckIndex_6(int32_t value)
	{
		___neckIndex_6 = value;
	}

	inline static int32_t get_offset_of_eyeIndex_7() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___eyeIndex_7)); }
	inline int32_t get_eyeIndex_7() const { return ___eyeIndex_7; }
	inline int32_t* get_address_of_eyeIndex_7() { return &___eyeIndex_7; }
	inline void set_eyeIndex_7(int32_t value)
	{
		___eyeIndex_7 = value;
	}

	inline static int32_t get_offset_of_hairIndex_8() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___hairIndex_8)); }
	inline int32_t get_hairIndex_8() const { return ___hairIndex_8; }
	inline int32_t* get_address_of_hairIndex_8() { return &___hairIndex_8; }
	inline void set_hairIndex_8(int32_t value)
	{
		___hairIndex_8 = value;
	}

	inline static int32_t get_offset_of_songNoteSpawnInterval_9() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___songNoteSpawnInterval_9)); }
	inline float get_songNoteSpawnInterval_9() const { return ___songNoteSpawnInterval_9; }
	inline float* get_address_of_songNoteSpawnInterval_9() { return &___songNoteSpawnInterval_9; }
	inline void set_songNoteSpawnInterval_9(float value)
	{
		___songNoteSpawnInterval_9 = value;
	}

	inline static int32_t get_offset_of_elfShrinkTime_10() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___elfShrinkTime_10)); }
	inline float get_elfShrinkTime_10() const { return ___elfShrinkTime_10; }
	inline float* get_address_of_elfShrinkTime_10() { return &___elfShrinkTime_10; }
	inline void set_elfShrinkTime_10(float value)
	{
		___elfShrinkTime_10 = value;
	}

	inline static int32_t get_offset_of_elfTargetScale_11() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___elfTargetScale_11)); }
	inline Vector3_t2243707580  get_elfTargetScale_11() const { return ___elfTargetScale_11; }
	inline Vector3_t2243707580 * get_address_of_elfTargetScale_11() { return &___elfTargetScale_11; }
	inline void set_elfTargetScale_11(Vector3_t2243707580  value)
	{
		___elfTargetScale_11 = value;
	}

	inline static int32_t get_offset_of_elfTargetPosition_12() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___elfTargetPosition_12)); }
	inline Vector3_t2243707580  get_elfTargetPosition_12() const { return ___elfTargetPosition_12; }
	inline Vector3_t2243707580 * get_address_of_elfTargetPosition_12() { return &___elfTargetPosition_12; }
	inline void set_elfTargetPosition_12(Vector3_t2243707580  value)
	{
		___elfTargetPosition_12 = value;
	}

	inline static int32_t get_offset_of_songBubble_13() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___songBubble_13)); }
	inline SongBubble_t2997910009 * get_songBubble_13() const { return ___songBubble_13; }
	inline SongBubble_t2997910009 ** get_address_of_songBubble_13() { return &___songBubble_13; }
	inline void set_songBubble_13(SongBubble_t2997910009 * value)
	{
		___songBubble_13 = value;
		Il2CppCodeGenWriteBarrier(&___songBubble_13, value);
	}

	inline static int32_t get_offset_of_elf_14() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___elf_14)); }
	inline GameObject_t1756533147 * get_elf_14() const { return ___elf_14; }
	inline GameObject_t1756533147 ** get_address_of_elf_14() { return &___elf_14; }
	inline void set_elf_14(GameObject_t1756533147 * value)
	{
		___elf_14 = value;
		Il2CppCodeGenWriteBarrier(&___elf_14, value);
	}

	inline static int32_t get_offset_of_animationNames_15() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___animationNames_15)); }
	inline StringU5BU5D_t1642385972* get_animationNames_15() const { return ___animationNames_15; }
	inline StringU5BU5D_t1642385972** get_address_of_animationNames_15() { return &___animationNames_15; }
	inline void set_animationNames_15(StringU5BU5D_t1642385972* value)
	{
		___animationNames_15 = value;
		Il2CppCodeGenWriteBarrier(&___animationNames_15, value);
	}

	inline static int32_t get_offset_of_faces_16() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___faces_16)); }
	inline SpriteU5BU5D_t3359083662* get_faces_16() const { return ___faces_16; }
	inline SpriteU5BU5D_t3359083662** get_address_of_faces_16() { return &___faces_16; }
	inline void set_faces_16(SpriteU5BU5D_t3359083662* value)
	{
		___faces_16 = value;
		Il2CppCodeGenWriteBarrier(&___faces_16, value);
	}

	inline static int32_t get_offset_of_necks_17() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___necks_17)); }
	inline SpriteU5BU5D_t3359083662* get_necks_17() const { return ___necks_17; }
	inline SpriteU5BU5D_t3359083662** get_address_of_necks_17() { return &___necks_17; }
	inline void set_necks_17(SpriteU5BU5D_t3359083662* value)
	{
		___necks_17 = value;
		Il2CppCodeGenWriteBarrier(&___necks_17, value);
	}

	inline static int32_t get_offset_of_eyes_18() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___eyes_18)); }
	inline SpriteU5BU5D_t3359083662* get_eyes_18() const { return ___eyes_18; }
	inline SpriteU5BU5D_t3359083662** get_address_of_eyes_18() { return &___eyes_18; }
	inline void set_eyes_18(SpriteU5BU5D_t3359083662* value)
	{
		___eyes_18 = value;
		Il2CppCodeGenWriteBarrier(&___eyes_18, value);
	}

	inline static int32_t get_offset_of_hairs_19() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___hairs_19)); }
	inline SpriteU5BU5D_t3359083662* get_hairs_19() const { return ___hairs_19; }
	inline SpriteU5BU5D_t3359083662** get_address_of_hairs_19() { return &___hairs_19; }
	inline void set_hairs_19(SpriteU5BU5D_t3359083662* value)
	{
		___hairs_19 = value;
		Il2CppCodeGenWriteBarrier(&___hairs_19, value);
	}

	inline static int32_t get_offset_of_face_20() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___face_20)); }
	inline SpriteRenderer_t1209076198 * get_face_20() const { return ___face_20; }
	inline SpriteRenderer_t1209076198 ** get_address_of_face_20() { return &___face_20; }
	inline void set_face_20(SpriteRenderer_t1209076198 * value)
	{
		___face_20 = value;
		Il2CppCodeGenWriteBarrier(&___face_20, value);
	}

	inline static int32_t get_offset_of_neck_21() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___neck_21)); }
	inline SpriteRenderer_t1209076198 * get_neck_21() const { return ___neck_21; }
	inline SpriteRenderer_t1209076198 ** get_address_of_neck_21() { return &___neck_21; }
	inline void set_neck_21(SpriteRenderer_t1209076198 * value)
	{
		___neck_21 = value;
		Il2CppCodeGenWriteBarrier(&___neck_21, value);
	}

	inline static int32_t get_offset_of_eye_22() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___eye_22)); }
	inline SpriteRenderer_t1209076198 * get_eye_22() const { return ___eye_22; }
	inline SpriteRenderer_t1209076198 ** get_address_of_eye_22() { return &___eye_22; }
	inline void set_eye_22(SpriteRenderer_t1209076198 * value)
	{
		___eye_22 = value;
		Il2CppCodeGenWriteBarrier(&___eye_22, value);
	}

	inline static int32_t get_offset_of_hair_23() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___hair_23)); }
	inline SpriteRenderer_t1209076198 * get_hair_23() const { return ___hair_23; }
	inline SpriteRenderer_t1209076198 ** get_address_of_hair_23() { return &___hair_23; }
	inline void set_hair_23(SpriteRenderer_t1209076198 * value)
	{
		___hair_23 = value;
		Il2CppCodeGenWriteBarrier(&___hair_23, value);
	}

	inline static int32_t get_offset_of_elfAnimator_24() { return static_cast<int32_t>(offsetof(GameNineManager_t3632946731, ___elfAnimator_24)); }
	inline Animator_t69676727 * get_elfAnimator_24() const { return ___elfAnimator_24; }
	inline Animator_t69676727 ** get_address_of_elfAnimator_24() { return &___elfAnimator_24; }
	inline void set_elfAnimator_24(Animator_t69676727 * value)
	{
		___elfAnimator_24 = value;
		Il2CppCodeGenWriteBarrier(&___elfAnimator_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
