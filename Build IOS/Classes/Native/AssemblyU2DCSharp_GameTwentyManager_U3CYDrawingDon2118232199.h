﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// GameTwentyManager
struct GameTwentyManager_t1246478874;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTwentyManager/<YDrawingDone>c__Iterator0
struct  U3CYDrawingDoneU3Ec__Iterator0_t2118232199  : public Il2CppObject
{
public:
	// System.Single GameTwentyManager/<YDrawingDone>c__Iterator0::<alphaValue>__0
	float ___U3CalphaValueU3E__0_0;
	// UnityEngine.Color GameTwentyManager/<YDrawingDone>c__Iterator0::<newColor>__1
	Color_t2020392075  ___U3CnewColorU3E__1_1;
	// GameTwentyManager GameTwentyManager/<YDrawingDone>c__Iterator0::$this
	GameTwentyManager_t1246478874 * ___U24this_2;
	// System.Object GameTwentyManager/<YDrawingDone>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean GameTwentyManager/<YDrawingDone>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 GameTwentyManager/<YDrawingDone>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CalphaValueU3E__0_0() { return static_cast<int32_t>(offsetof(U3CYDrawingDoneU3Ec__Iterator0_t2118232199, ___U3CalphaValueU3E__0_0)); }
	inline float get_U3CalphaValueU3E__0_0() const { return ___U3CalphaValueU3E__0_0; }
	inline float* get_address_of_U3CalphaValueU3E__0_0() { return &___U3CalphaValueU3E__0_0; }
	inline void set_U3CalphaValueU3E__0_0(float value)
	{
		___U3CalphaValueU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CnewColorU3E__1_1() { return static_cast<int32_t>(offsetof(U3CYDrawingDoneU3Ec__Iterator0_t2118232199, ___U3CnewColorU3E__1_1)); }
	inline Color_t2020392075  get_U3CnewColorU3E__1_1() const { return ___U3CnewColorU3E__1_1; }
	inline Color_t2020392075 * get_address_of_U3CnewColorU3E__1_1() { return &___U3CnewColorU3E__1_1; }
	inline void set_U3CnewColorU3E__1_1(Color_t2020392075  value)
	{
		___U3CnewColorU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CYDrawingDoneU3Ec__Iterator0_t2118232199, ___U24this_2)); }
	inline GameTwentyManager_t1246478874 * get_U24this_2() const { return ___U24this_2; }
	inline GameTwentyManager_t1246478874 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(GameTwentyManager_t1246478874 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CYDrawingDoneU3Ec__Iterator0_t2118232199, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CYDrawingDoneU3Ec__Iterator0_t2118232199, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CYDrawingDoneU3Ec__Iterator0_t2118232199, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
