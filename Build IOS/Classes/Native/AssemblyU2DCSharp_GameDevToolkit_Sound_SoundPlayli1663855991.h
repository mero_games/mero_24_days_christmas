﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameDevToolkit.Sound.SoundPlaylist/PlaylistDelegate
struct PlaylistDelegate_t2451478837;
// GameDevToolkit.Sound.SoundManagerEventProccessor
struct SoundManagerEventProccessor_t3825530019;
// GameDevToolkit.Sound.AudibleSound
struct AudibleSound_t2302054751;
// GameDevToolkit.Sound.SoundClip[]
struct SoundClipU5BU5D_t2876072966;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Sound.SoundPlaylist
struct  SoundPlaylist_t1663855991  : public Il2CppObject
{
public:
	// GameDevToolkit.Sound.SoundPlaylist/PlaylistDelegate GameDevToolkit.Sound.SoundPlaylist::PlaylistComplete
	PlaylistDelegate_t2451478837 * ___PlaylistComplete_0;
	// GameDevToolkit.Sound.SoundPlaylist/PlaylistDelegate GameDevToolkit.Sound.SoundPlaylist::PlaylistJump
	PlaylistDelegate_t2451478837 * ___PlaylistJump_1;
	// System.Boolean GameDevToolkit.Sound.SoundPlaylist::_isPlaying
	bool ____isPlaying_2;
	// GameDevToolkit.Sound.SoundManagerEventProccessor GameDevToolkit.Sound.SoundPlaylist::proccessor
	SoundManagerEventProccessor_t3825530019 * ___proccessor_3;
	// GameDevToolkit.Sound.AudibleSound GameDevToolkit.Sound.SoundPlaylist::_currentSound
	AudibleSound_t2302054751 * ____currentSound_4;
	// System.Int32 GameDevToolkit.Sound.SoundPlaylist::_currentSoundIndex
	int32_t ____currentSoundIndex_5;
	// GameDevToolkit.Sound.SoundClip[] GameDevToolkit.Sound.SoundPlaylist::sounds
	SoundClipU5BU5D_t2876072966* ___sounds_6;

public:
	inline static int32_t get_offset_of_PlaylistComplete_0() { return static_cast<int32_t>(offsetof(SoundPlaylist_t1663855991, ___PlaylistComplete_0)); }
	inline PlaylistDelegate_t2451478837 * get_PlaylistComplete_0() const { return ___PlaylistComplete_0; }
	inline PlaylistDelegate_t2451478837 ** get_address_of_PlaylistComplete_0() { return &___PlaylistComplete_0; }
	inline void set_PlaylistComplete_0(PlaylistDelegate_t2451478837 * value)
	{
		___PlaylistComplete_0 = value;
		Il2CppCodeGenWriteBarrier(&___PlaylistComplete_0, value);
	}

	inline static int32_t get_offset_of_PlaylistJump_1() { return static_cast<int32_t>(offsetof(SoundPlaylist_t1663855991, ___PlaylistJump_1)); }
	inline PlaylistDelegate_t2451478837 * get_PlaylistJump_1() const { return ___PlaylistJump_1; }
	inline PlaylistDelegate_t2451478837 ** get_address_of_PlaylistJump_1() { return &___PlaylistJump_1; }
	inline void set_PlaylistJump_1(PlaylistDelegate_t2451478837 * value)
	{
		___PlaylistJump_1 = value;
		Il2CppCodeGenWriteBarrier(&___PlaylistJump_1, value);
	}

	inline static int32_t get_offset_of__isPlaying_2() { return static_cast<int32_t>(offsetof(SoundPlaylist_t1663855991, ____isPlaying_2)); }
	inline bool get__isPlaying_2() const { return ____isPlaying_2; }
	inline bool* get_address_of__isPlaying_2() { return &____isPlaying_2; }
	inline void set__isPlaying_2(bool value)
	{
		____isPlaying_2 = value;
	}

	inline static int32_t get_offset_of_proccessor_3() { return static_cast<int32_t>(offsetof(SoundPlaylist_t1663855991, ___proccessor_3)); }
	inline SoundManagerEventProccessor_t3825530019 * get_proccessor_3() const { return ___proccessor_3; }
	inline SoundManagerEventProccessor_t3825530019 ** get_address_of_proccessor_3() { return &___proccessor_3; }
	inline void set_proccessor_3(SoundManagerEventProccessor_t3825530019 * value)
	{
		___proccessor_3 = value;
		Il2CppCodeGenWriteBarrier(&___proccessor_3, value);
	}

	inline static int32_t get_offset_of__currentSound_4() { return static_cast<int32_t>(offsetof(SoundPlaylist_t1663855991, ____currentSound_4)); }
	inline AudibleSound_t2302054751 * get__currentSound_4() const { return ____currentSound_4; }
	inline AudibleSound_t2302054751 ** get_address_of__currentSound_4() { return &____currentSound_4; }
	inline void set__currentSound_4(AudibleSound_t2302054751 * value)
	{
		____currentSound_4 = value;
		Il2CppCodeGenWriteBarrier(&____currentSound_4, value);
	}

	inline static int32_t get_offset_of__currentSoundIndex_5() { return static_cast<int32_t>(offsetof(SoundPlaylist_t1663855991, ____currentSoundIndex_5)); }
	inline int32_t get__currentSoundIndex_5() const { return ____currentSoundIndex_5; }
	inline int32_t* get_address_of__currentSoundIndex_5() { return &____currentSoundIndex_5; }
	inline void set__currentSoundIndex_5(int32_t value)
	{
		____currentSoundIndex_5 = value;
	}

	inline static int32_t get_offset_of_sounds_6() { return static_cast<int32_t>(offsetof(SoundPlaylist_t1663855991, ___sounds_6)); }
	inline SoundClipU5BU5D_t2876072966* get_sounds_6() const { return ___sounds_6; }
	inline SoundClipU5BU5D_t2876072966** get_address_of_sounds_6() { return &___sounds_6; }
	inline void set_sounds_6(SoundClipU5BU5D_t2876072966* value)
	{
		___sounds_6 = value;
		Il2CppCodeGenWriteBarrier(&___sounds_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
