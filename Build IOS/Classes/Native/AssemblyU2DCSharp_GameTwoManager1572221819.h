﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen571736909.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// TransformGrid[]
struct TransformGridU5BU5D_t3186924753;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// UnityEngine.Collider2D
struct Collider2D_t646061738;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTwoManager
struct  GameTwoManager_t1572221819  : public SingletonBase_1_t571736909
{
public:
	// System.Boolean GameTwoManager::isPlaying
	bool ___isPlaying_4;
	// UnityEngine.GameObject GameTwoManager::snow
	GameObject_t1756533147 * ___snow_5;
	// UnityEngine.GameObject[] GameTwoManager::elements
	GameObjectU5BU5D_t3057952154* ___elements_6;
	// UnityEngine.Transform[] GameTwoManager::startPositions
	TransformU5BU5D_t3764228911* ___startPositions_7;
	// TransformGrid[] GameTwoManager::rows
	TransformGridU5BU5D_t3186924753* ___rows_8;
	// UnityEngine.ParticleSystem GameTwoManager::digEffect
	ParticleSystem_t3394631041 * ___digEffect_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameTwoManager::spawnedObjects
	List_1_t1125654279 * ___spawnedObjects_10;
	// System.Boolean[] GameTwoManager::elementsUsed
	BooleanU5BU5D_t3568034315* ___elementsUsed_11;
	// UnityEngine.Collider2D GameTwoManager::lastClickedCollider
	Collider2D_t646061738 * ___lastClickedCollider_12;
	// System.Int32 GameTwoManager::itemCounter
	int32_t ___itemCounter_13;

public:
	inline static int32_t get_offset_of_isPlaying_4() { return static_cast<int32_t>(offsetof(GameTwoManager_t1572221819, ___isPlaying_4)); }
	inline bool get_isPlaying_4() const { return ___isPlaying_4; }
	inline bool* get_address_of_isPlaying_4() { return &___isPlaying_4; }
	inline void set_isPlaying_4(bool value)
	{
		___isPlaying_4 = value;
	}

	inline static int32_t get_offset_of_snow_5() { return static_cast<int32_t>(offsetof(GameTwoManager_t1572221819, ___snow_5)); }
	inline GameObject_t1756533147 * get_snow_5() const { return ___snow_5; }
	inline GameObject_t1756533147 ** get_address_of_snow_5() { return &___snow_5; }
	inline void set_snow_5(GameObject_t1756533147 * value)
	{
		___snow_5 = value;
		Il2CppCodeGenWriteBarrier(&___snow_5, value);
	}

	inline static int32_t get_offset_of_elements_6() { return static_cast<int32_t>(offsetof(GameTwoManager_t1572221819, ___elements_6)); }
	inline GameObjectU5BU5D_t3057952154* get_elements_6() const { return ___elements_6; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_elements_6() { return &___elements_6; }
	inline void set_elements_6(GameObjectU5BU5D_t3057952154* value)
	{
		___elements_6 = value;
		Il2CppCodeGenWriteBarrier(&___elements_6, value);
	}

	inline static int32_t get_offset_of_startPositions_7() { return static_cast<int32_t>(offsetof(GameTwoManager_t1572221819, ___startPositions_7)); }
	inline TransformU5BU5D_t3764228911* get_startPositions_7() const { return ___startPositions_7; }
	inline TransformU5BU5D_t3764228911** get_address_of_startPositions_7() { return &___startPositions_7; }
	inline void set_startPositions_7(TransformU5BU5D_t3764228911* value)
	{
		___startPositions_7 = value;
		Il2CppCodeGenWriteBarrier(&___startPositions_7, value);
	}

	inline static int32_t get_offset_of_rows_8() { return static_cast<int32_t>(offsetof(GameTwoManager_t1572221819, ___rows_8)); }
	inline TransformGridU5BU5D_t3186924753* get_rows_8() const { return ___rows_8; }
	inline TransformGridU5BU5D_t3186924753** get_address_of_rows_8() { return &___rows_8; }
	inline void set_rows_8(TransformGridU5BU5D_t3186924753* value)
	{
		___rows_8 = value;
		Il2CppCodeGenWriteBarrier(&___rows_8, value);
	}

	inline static int32_t get_offset_of_digEffect_9() { return static_cast<int32_t>(offsetof(GameTwoManager_t1572221819, ___digEffect_9)); }
	inline ParticleSystem_t3394631041 * get_digEffect_9() const { return ___digEffect_9; }
	inline ParticleSystem_t3394631041 ** get_address_of_digEffect_9() { return &___digEffect_9; }
	inline void set_digEffect_9(ParticleSystem_t3394631041 * value)
	{
		___digEffect_9 = value;
		Il2CppCodeGenWriteBarrier(&___digEffect_9, value);
	}

	inline static int32_t get_offset_of_spawnedObjects_10() { return static_cast<int32_t>(offsetof(GameTwoManager_t1572221819, ___spawnedObjects_10)); }
	inline List_1_t1125654279 * get_spawnedObjects_10() const { return ___spawnedObjects_10; }
	inline List_1_t1125654279 ** get_address_of_spawnedObjects_10() { return &___spawnedObjects_10; }
	inline void set_spawnedObjects_10(List_1_t1125654279 * value)
	{
		___spawnedObjects_10 = value;
		Il2CppCodeGenWriteBarrier(&___spawnedObjects_10, value);
	}

	inline static int32_t get_offset_of_elementsUsed_11() { return static_cast<int32_t>(offsetof(GameTwoManager_t1572221819, ___elementsUsed_11)); }
	inline BooleanU5BU5D_t3568034315* get_elementsUsed_11() const { return ___elementsUsed_11; }
	inline BooleanU5BU5D_t3568034315** get_address_of_elementsUsed_11() { return &___elementsUsed_11; }
	inline void set_elementsUsed_11(BooleanU5BU5D_t3568034315* value)
	{
		___elementsUsed_11 = value;
		Il2CppCodeGenWriteBarrier(&___elementsUsed_11, value);
	}

	inline static int32_t get_offset_of_lastClickedCollider_12() { return static_cast<int32_t>(offsetof(GameTwoManager_t1572221819, ___lastClickedCollider_12)); }
	inline Collider2D_t646061738 * get_lastClickedCollider_12() const { return ___lastClickedCollider_12; }
	inline Collider2D_t646061738 ** get_address_of_lastClickedCollider_12() { return &___lastClickedCollider_12; }
	inline void set_lastClickedCollider_12(Collider2D_t646061738 * value)
	{
		___lastClickedCollider_12 = value;
		Il2CppCodeGenWriteBarrier(&___lastClickedCollider_12, value);
	}

	inline static int32_t get_offset_of_itemCounter_13() { return static_cast<int32_t>(offsetof(GameTwoManager_t1572221819, ___itemCounter_13)); }
	inline int32_t get_itemCounter_13() const { return ___itemCounter_13; }
	inline int32_t* get_address_of_itemCounter_13() { return &___itemCounter_13; }
	inline void set_itemCounter_13(int32_t value)
	{
		___itemCounter_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
