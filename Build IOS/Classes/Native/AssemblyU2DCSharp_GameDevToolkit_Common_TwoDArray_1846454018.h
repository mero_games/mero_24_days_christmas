﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object[]
struct ObjectU5BU5D_t3614634134;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Common.TwoDArray`1<System.Object>
struct  TwoDArray_1_t846454018  : public Il2CppObject
{
public:
	// T[] GameDevToolkit.Common.TwoDArray`1::elements
	ObjectU5BU5D_t3614634134* ___elements_0;

public:
	inline static int32_t get_offset_of_elements_0() { return static_cast<int32_t>(offsetof(TwoDArray_1_t846454018, ___elements_0)); }
	inline ObjectU5BU5D_t3614634134* get_elements_0() const { return ___elements_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_elements_0() { return &___elements_0; }
	inline void set_elements_0(ObjectU5BU5D_t3614634134* value)
	{
		___elements_0 = value;
		Il2CppCodeGenWriteBarrier(&___elements_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
