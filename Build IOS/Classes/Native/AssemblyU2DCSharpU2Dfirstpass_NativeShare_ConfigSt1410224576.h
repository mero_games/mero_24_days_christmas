﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare/ConfigStruct
struct  ConfigStruct_t1410224576 
{
public:
	// System.String NativeShare/ConfigStruct::title
	String_t* ___title_0;
	// System.String NativeShare/ConfigStruct::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(ConfigStruct_t1410224576, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier(&___title_0, value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(ConfigStruct_t1410224576, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier(&___message_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NativeShare/ConfigStruct
struct ConfigStruct_t1410224576_marshaled_pinvoke
{
	char* ___title_0;
	char* ___message_1;
};
// Native definition for COM marshalling of NativeShare/ConfigStruct
struct ConfigStruct_t1410224576_marshaled_com
{
	Il2CppChar* ___title_0;
	Il2CppChar* ___message_1;
};
