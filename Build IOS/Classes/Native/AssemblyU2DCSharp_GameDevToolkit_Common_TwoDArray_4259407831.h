﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameDevToolkit.Visual.ObjectSequence[]
struct ObjectSequenceU5BU5D_t268795597;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Common.TwoDArray`1<GameDevToolkit.Visual.ObjectSequence>
struct  TwoDArray_1_t4259407831  : public Il2CppObject
{
public:
	// T[] GameDevToolkit.Common.TwoDArray`1::elements
	ObjectSequenceU5BU5D_t268795597* ___elements_0;

public:
	inline static int32_t get_offset_of_elements_0() { return static_cast<int32_t>(offsetof(TwoDArray_1_t4259407831, ___elements_0)); }
	inline ObjectSequenceU5BU5D_t268795597* get_elements_0() const { return ___elements_0; }
	inline ObjectSequenceU5BU5D_t268795597** get_address_of_elements_0() { return &___elements_0; }
	inline void set_elements_0(ObjectSequenceU5BU5D_t268795597* value)
	{
		___elements_0 = value;
		Il2CppCodeGenWriteBarrier(&___elements_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
