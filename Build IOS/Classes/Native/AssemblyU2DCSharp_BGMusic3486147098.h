﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t2203355011;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BGMusic
struct  BGMusic_t3486147098  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip[] BGMusic::backgroundSongs
	AudioClipU5BU5D_t2203355011* ___backgroundSongs_2;
	// System.Single BGMusic::fadeTime
	float ___fadeTime_3;
	// System.Int32 BGMusic::songsCounter
	int32_t ___songsCounter_4;
	// UnityEngine.AudioSource BGMusic::audioSource
	AudioSource_t1135106623 * ___audioSource_5;

public:
	inline static int32_t get_offset_of_backgroundSongs_2() { return static_cast<int32_t>(offsetof(BGMusic_t3486147098, ___backgroundSongs_2)); }
	inline AudioClipU5BU5D_t2203355011* get_backgroundSongs_2() const { return ___backgroundSongs_2; }
	inline AudioClipU5BU5D_t2203355011** get_address_of_backgroundSongs_2() { return &___backgroundSongs_2; }
	inline void set_backgroundSongs_2(AudioClipU5BU5D_t2203355011* value)
	{
		___backgroundSongs_2 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSongs_2, value);
	}

	inline static int32_t get_offset_of_fadeTime_3() { return static_cast<int32_t>(offsetof(BGMusic_t3486147098, ___fadeTime_3)); }
	inline float get_fadeTime_3() const { return ___fadeTime_3; }
	inline float* get_address_of_fadeTime_3() { return &___fadeTime_3; }
	inline void set_fadeTime_3(float value)
	{
		___fadeTime_3 = value;
	}

	inline static int32_t get_offset_of_songsCounter_4() { return static_cast<int32_t>(offsetof(BGMusic_t3486147098, ___songsCounter_4)); }
	inline int32_t get_songsCounter_4() const { return ___songsCounter_4; }
	inline int32_t* get_address_of_songsCounter_4() { return &___songsCounter_4; }
	inline void set_songsCounter_4(int32_t value)
	{
		___songsCounter_4 = value;
	}

	inline static int32_t get_offset_of_audioSource_5() { return static_cast<int32_t>(offsetof(BGMusic_t3486147098, ___audioSource_5)); }
	inline AudioSource_t1135106623 * get_audioSource_5() const { return ___audioSource_5; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_5() { return &___audioSource_5; }
	inline void set_audioSource_5(AudioSource_t1135106623 * value)
	{
		___audioSource_5 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
