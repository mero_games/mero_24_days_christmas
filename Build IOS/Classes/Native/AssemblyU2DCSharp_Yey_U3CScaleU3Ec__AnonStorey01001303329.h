﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Yey/<Scale>c__AnonStorey0
struct  U3CScaleU3Ec__AnonStorey0_t1001303329  : public Il2CppObject
{
public:
	// UnityEngine.Transform Yey/<Scale>c__AnonStorey0::t
	Transform_t3275118058 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CScaleU3Ec__AnonStorey0_t1001303329, ___t_0)); }
	inline Transform_t3275118058 * get_t_0() const { return ___t_0; }
	inline Transform_t3275118058 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Transform_t3275118058 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier(&___t_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
