﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t2203355011;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EndScreen
struct  EndScreen_t1192627425  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] EndScreen::spwanPoints
	GameObjectU5BU5D_t3057952154* ___spwanPoints_2;
	// UnityEngine.GameObject EndScreen::star
	GameObject_t1756533147 * ___star_3;
	// UnityEngine.GameObject[] EndScreen::menuBtns
	GameObjectU5BU5D_t3057952154* ___menuBtns_4;
	// UnityEngine.GameObject EndScreen::starDestroyEffect
	GameObject_t1756533147 * ___starDestroyEffect_5;
	// UnityEngine.Sprite[] EndScreen::candies
	SpriteU5BU5D_t3359083662* ___candies_6;
	// UnityEngine.GameObject EndScreen::tempStar
	GameObject_t1756533147 * ___tempStar_7;
	// UnityEngine.AudioClip[] EndScreen::sound
	AudioClipU5BU5D_t2203355011* ___sound_8;
	// System.Collections.Generic.List`1<System.Int32> EndScreen::occupied
	List_1_t1440998580 * ___occupied_9;

public:
	inline static int32_t get_offset_of_spwanPoints_2() { return static_cast<int32_t>(offsetof(EndScreen_t1192627425, ___spwanPoints_2)); }
	inline GameObjectU5BU5D_t3057952154* get_spwanPoints_2() const { return ___spwanPoints_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_spwanPoints_2() { return &___spwanPoints_2; }
	inline void set_spwanPoints_2(GameObjectU5BU5D_t3057952154* value)
	{
		___spwanPoints_2 = value;
		Il2CppCodeGenWriteBarrier(&___spwanPoints_2, value);
	}

	inline static int32_t get_offset_of_star_3() { return static_cast<int32_t>(offsetof(EndScreen_t1192627425, ___star_3)); }
	inline GameObject_t1756533147 * get_star_3() const { return ___star_3; }
	inline GameObject_t1756533147 ** get_address_of_star_3() { return &___star_3; }
	inline void set_star_3(GameObject_t1756533147 * value)
	{
		___star_3 = value;
		Il2CppCodeGenWriteBarrier(&___star_3, value);
	}

	inline static int32_t get_offset_of_menuBtns_4() { return static_cast<int32_t>(offsetof(EndScreen_t1192627425, ___menuBtns_4)); }
	inline GameObjectU5BU5D_t3057952154* get_menuBtns_4() const { return ___menuBtns_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_menuBtns_4() { return &___menuBtns_4; }
	inline void set_menuBtns_4(GameObjectU5BU5D_t3057952154* value)
	{
		___menuBtns_4 = value;
		Il2CppCodeGenWriteBarrier(&___menuBtns_4, value);
	}

	inline static int32_t get_offset_of_starDestroyEffect_5() { return static_cast<int32_t>(offsetof(EndScreen_t1192627425, ___starDestroyEffect_5)); }
	inline GameObject_t1756533147 * get_starDestroyEffect_5() const { return ___starDestroyEffect_5; }
	inline GameObject_t1756533147 ** get_address_of_starDestroyEffect_5() { return &___starDestroyEffect_5; }
	inline void set_starDestroyEffect_5(GameObject_t1756533147 * value)
	{
		___starDestroyEffect_5 = value;
		Il2CppCodeGenWriteBarrier(&___starDestroyEffect_5, value);
	}

	inline static int32_t get_offset_of_candies_6() { return static_cast<int32_t>(offsetof(EndScreen_t1192627425, ___candies_6)); }
	inline SpriteU5BU5D_t3359083662* get_candies_6() const { return ___candies_6; }
	inline SpriteU5BU5D_t3359083662** get_address_of_candies_6() { return &___candies_6; }
	inline void set_candies_6(SpriteU5BU5D_t3359083662* value)
	{
		___candies_6 = value;
		Il2CppCodeGenWriteBarrier(&___candies_6, value);
	}

	inline static int32_t get_offset_of_tempStar_7() { return static_cast<int32_t>(offsetof(EndScreen_t1192627425, ___tempStar_7)); }
	inline GameObject_t1756533147 * get_tempStar_7() const { return ___tempStar_7; }
	inline GameObject_t1756533147 ** get_address_of_tempStar_7() { return &___tempStar_7; }
	inline void set_tempStar_7(GameObject_t1756533147 * value)
	{
		___tempStar_7 = value;
		Il2CppCodeGenWriteBarrier(&___tempStar_7, value);
	}

	inline static int32_t get_offset_of_sound_8() { return static_cast<int32_t>(offsetof(EndScreen_t1192627425, ___sound_8)); }
	inline AudioClipU5BU5D_t2203355011* get_sound_8() const { return ___sound_8; }
	inline AudioClipU5BU5D_t2203355011** get_address_of_sound_8() { return &___sound_8; }
	inline void set_sound_8(AudioClipU5BU5D_t2203355011* value)
	{
		___sound_8 = value;
		Il2CppCodeGenWriteBarrier(&___sound_8, value);
	}

	inline static int32_t get_offset_of_occupied_9() { return static_cast<int32_t>(offsetof(EndScreen_t1192627425, ___occupied_9)); }
	inline List_1_t1440998580 * get_occupied_9() const { return ___occupied_9; }
	inline List_1_t1440998580 ** get_address_of_occupied_9() { return &___occupied_9; }
	inline void set_occupied_9(List_1_t1440998580 * value)
	{
		___occupied_9 = value;
		Il2CppCodeGenWriteBarrier(&___occupied_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
