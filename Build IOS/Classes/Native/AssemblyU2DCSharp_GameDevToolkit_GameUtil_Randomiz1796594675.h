﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameDevToolkit.GameUtil.IRandomizerRule`1<System.Int32>
struct IRandomizerRule_1_t4244293120;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.GameUtil.Randomizer
struct  Randomizer_t1796594675  : public Il2CppObject
{
public:
	// GameDevToolkit.GameUtil.IRandomizerRule`1<System.Int32> GameDevToolkit.GameUtil.Randomizer::randomRule
	Il2CppObject* ___randomRule_0;
	// System.Int32 GameDevToolkit.GameUtil.Randomizer::min
	int32_t ___min_1;
	// System.Int32 GameDevToolkit.GameUtil.Randomizer::max
	int32_t ___max_2;

public:
	inline static int32_t get_offset_of_randomRule_0() { return static_cast<int32_t>(offsetof(Randomizer_t1796594675, ___randomRule_0)); }
	inline Il2CppObject* get_randomRule_0() const { return ___randomRule_0; }
	inline Il2CppObject** get_address_of_randomRule_0() { return &___randomRule_0; }
	inline void set_randomRule_0(Il2CppObject* value)
	{
		___randomRule_0 = value;
		Il2CppCodeGenWriteBarrier(&___randomRule_0, value);
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(Randomizer_t1796594675, ___min_1)); }
	inline int32_t get_min_1() const { return ___min_1; }
	inline int32_t* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(int32_t value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(Randomizer_t1796594675, ___max_2)); }
	inline int32_t get_max_2() const { return ___max_2; }
	inline int32_t* get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(int32_t value)
	{
		___max_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
