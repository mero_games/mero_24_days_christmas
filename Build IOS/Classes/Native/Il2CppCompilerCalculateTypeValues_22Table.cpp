﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSON3971185768.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_ArbitraryC3825778126.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_ArbitraryC3280263404.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_ArbitraryEv766134336.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_ColliderEve795033329.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_ColliderEv2027680195.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_KeyboardEv2538007188.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_KeyboardEve882008899.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_KeyboardEv2394732920.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_KeyboardEve572823003.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_MouseEventP442073356.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_MouseEvent3454497406.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_MouseEvent2370363659.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_ParticleSy2045555809.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_ParticleSys654927383.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_ParticleSy2356180715.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_ParticleSys623652476.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_GridConst555398878.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_GridPoin3774252050.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_GridRep2017325921.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_LookAtMo3531508735.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_LookAtMo2628874358.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_Randomiz1796594675.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_Randomiz4265547936.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_Randomiz1463926147.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_TwoDGame1262067236.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_AudibleSoun2302054751.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_AudibleSoun3383377214.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_AudibleSoun1083626444.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_SoundClip3743947423.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_SoundLayer2188462586.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_SoundLayer_2775777142.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_SoundManager13585936.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_SoundManager711187121.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_SoundManager711187122.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_SoundManage3825530019.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_SoundPlayli1663855991.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Sound_SoundPlayli2451478837.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_ActualFont3346628750.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_FollowerCa3769774071.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_Geom3460865626.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_LifeHUD785887741.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_ObjectSequ1807435812.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_ObjectSeque135521474.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_ScalingBar3437833994.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_ScalingBar3059270671.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_SequenceMo3638882036.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_TimeCalcul1446961881.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Visual_TwoDSequen1727799968.h"
#include "AssemblyU2DCSharp_GetSongData4249642665.h"
#include "AssemblyU2DCSharp_InputManager1610719423.h"
#include "AssemblyU2DCSharp_Loading2871513552.h"
#include "AssemblyU2DCSharp_LoadingSanta2184952393.h"
#include "AssemblyU2DCSharp_NavigationManager4055369543.h"
#include "AssemblyU2DCSharp_NavigationManager_U3CYInstantiat2959782327.h"
#include "AssemblyU2DCSharp_ObjectPool2689037807.h"
#include "AssemblyU2DCSharp_ObjectPool_StartupPoolMode3405548370.h"
#include "AssemblyU2DCSharp_ObjectPool_StartupPool2587102281.h"
#include "AssemblyU2DCSharp_ObjectPool_OnRecycleDelegate3801249229.h"
#include "AssemblyU2DCSharp_ObjectPoolExtensions3542169457.h"
#include "AssemblyU2DCSharp_ParentalControl2040855302.h"
#include "AssemblyU2DCSharp_ParentalControl_Type3248676223.h"
#include "AssemblyU2DCSharp_UIButtonOpenParental1943914589.h"
#include "AssemblyU2DCSharp_UIButtonOpenParental_Type1384202272.h"
#include "AssemblyU2DCSharp_Slide3421086917.h"
#include "AssemblyU2DCSharp_Slide_U3CYFadeInU3Ec__Iterator02538061961.h"
#include "AssemblyU2DCSharp_SlideShow1942539524.h"
#include "AssemblyU2DCSharp_SlideShow_U3CYStartSlideShowU3Ec_227050163.h"
#include "AssemblyU2DCSharp_UIButtonShowSlideshow175868935.h"
#include "AssemblyU2DCSharp_SoundClip2598932235.h"
#include "AssemblyU2DCSharp_SoundManager654432262.h"
#include "AssemblyU2DCSharp_UIButtonTutorial123001136.h"
#include "AssemblyU2DCSharp_UIButtonTutorial_U3CYCloseTutori1054761711.h"
#include "AssemblyU2DCSharp_UIButtonTutorial_U3CYOpenTutorial230414610.h"
#include "AssemblyU2DCSharp_UIButton3377238306.h"
#include "AssemblyU2DCSharp_UIButtonBack1890699263.h"
#include "AssemblyU2DCSharp_UIButtonLoadGame1239272532.h"
#include "AssemblyU2DCSharp_UIButtonLoadGame_U3CYLoadSceneU33230658609.h"
#include "AssemblyU2DCSharp_Utils4194145797.h"
#include "AssemblyU2DCSharp_CFX_AutoStopLoopedEffect3214280257.h"
#include "AssemblyU2DCSharp_CFX_Demo1465385775.h"
#include "AssemblyU2DCSharp_CFX_Demo_U3CRandomSpawnsCoroutin3999667400.h"
#include "AssemblyU2DCSharp_CFX_Demo_New3744422722.h"
#include "AssemblyU2DCSharp_CFX_Demo_New_U3CCheckForDeletedP2558571495.h"
#include "AssemblyU2DCSharp_CFX_Demo_RandomDir1076153330.h"
#include "AssemblyU2DCSharp_CFX_Demo_RandomDirectionTranslate747977598.h"
#include "AssemblyU2DCSharp_CFX_Demo_RotateCamera2665602760.h"
#include "AssemblyU2DCSharp_CFX_Demo_Translate3218432890.h"
#include "AssemblyU2DCSharp_CFX_AutoDestructShuriken1500114366.h"
#include "AssemblyU2DCSharp_CFX_AutoDestructShuriken_U3CChec3107901010.h"
#include "AssemblyU2DCSharp_CFX_AutoRotate1831446564.h"
#include "AssemblyU2DCSharp_CFX_AutodestructWhenNoChildren3972162727.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (JSON_t3971185768), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (ArbitraryCleaner_t3825778126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[1] = 
{
	ArbitraryCleaner_t3825778126::get_offset_of_Done_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (U3CdestroyMeU3Ec__Iterator0_t3280263404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[4] = 
{
	U3CdestroyMeU3Ec__Iterator0_t3280263404::get_offset_of_U24this_0(),
	U3CdestroyMeU3Ec__Iterator0_t3280263404::get_offset_of_U24current_1(),
	U3CdestroyMeU3Ec__Iterator0_t3280263404::get_offset_of_U24disposing_2(),
	U3CdestroyMeU3Ec__Iterator0_t3280263404::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (ArbitraryEvent_t766134336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[3] = 
{
	ArbitraryEvent_t766134336::get_offset_of_EventGameObject_2(),
	ArbitraryEvent_t766134336::get_offset_of_EventString_3(),
	ArbitraryEvent_t766134336::get_offset_of_EventTarget_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (ColliderEventSystem_t795033329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[4] = 
{
	ColliderEventSystem_t795033329::get_offset_of_TriggerEntered_2(),
	ColliderEventSystem_t795033329::get_offset_of_TriggerExited_3(),
	ColliderEventSystem_t795033329::get_offset_of_ColliderEntered_4(),
	ColliderEventSystem_t795033329::get_offset_of_ColliderExited_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (ColliderDelegate_t2027680195), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (KeyboardEventSystem_t2538007188), -1, sizeof(KeyboardEventSystem_t2538007188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2210[6] = 
{
	KeyboardEventSystem_t2538007188::get_offset_of__isPressed_2(),
	KeyboardEventSystem_t2538007188::get_offset_of_map_3(),
	KeyboardEventSystem_t2538007188::get_offset_of_keysToCheck_4(),
	KeyboardEventSystem_t2538007188_StaticFields::get_offset_of_instance_5(),
	KeyboardEventSystem_t2538007188::get_offset_of_KeyBoardEvent_6(),
	KeyboardEventSystem_t2538007188_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (KeyboardDelegate_t882008899), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (U3CisPressedU3Ec__AnonStorey0_t2394732920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[1] = 
{
	U3CisPressedU3Ec__AnonStorey0_t2394732920::get_offset_of_keyCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (KeyboardEventType_t572823003)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2213[4] = 
{
	KeyboardEventType_t572823003::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (MouseEventProccessor_t442073356), -1, sizeof(MouseEventProccessor_t442073356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2214[4] = 
{
	MouseEventProccessor_t442073356_StaticFields::get_offset_of_Instance_2(),
	MouseEventProccessor_t442073356::get_offset_of_captureMouseMouveEvents_3(),
	MouseEventProccessor_t442073356::get_offset_of__captureEvents_4(),
	MouseEventProccessor_t442073356::get_offset_of_lastHit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (MouseEventSystem_t3454497406), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (MouseEventType_t2370363659)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2216[9] = 
{
	MouseEventType_t2370363659::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (ParticleSystemCleaner_t2045555809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (U3CdestroyMeU3Ec__Iterator0_t654927383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[4] = 
{
	U3CdestroyMeU3Ec__Iterator0_t654927383::get_offset_of_U24this_0(),
	U3CdestroyMeU3Ec__Iterator0_t654927383::get_offset_of_U24current_1(),
	U3CdestroyMeU3Ec__Iterator0_t654927383::get_offset_of_U24disposing_2(),
	U3CdestroyMeU3Ec__Iterator0_t654927383::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (ParticleSystemEvent_t2356180715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[5] = 
{
	ParticleSystemEvent_t2356180715::get_offset_of_ParticleSystemDone_2(),
	ParticleSystemEvent_t2356180715::get_offset_of_maxLifeTime_3(),
	ParticleSystemEvent_t2356180715::get_offset_of_startTime_4(),
	ParticleSystemEvent_t2356180715::get_offset_of_dispatchedDone_5(),
	ParticleSystemEvent_t2356180715::get_offset_of_pSystem_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (ParticlySystemDoneDelegate_t623652476), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (GridConstrucor_t555398878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[5] = 
{
	GridConstrucor_t555398878::get_offset_of_rows_2(),
	GridConstrucor_t555398878::get_offset_of_cols_3(),
	GridConstrucor_t555398878::get_offset_of_CellPrefab_4(),
	GridConstrucor_t555398878::get_offset_of_cellWidth_5(),
	GridConstrucor_t555398878::get_offset_of_cellHeigth_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (GridPoint_t3774252050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[2] = 
{
	GridPoint_t3774252050::get_offset_of_x_0(),
	GridPoint_t3774252050::get_offset_of_y_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (GridRep_t2017325921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (LookAtMouse_t3531508735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[4] = 
{
	LookAtMouse_t3531508735::get_offset_of_cam_2(),
	LookAtMouse_t3531508735::get_offset_of_my_3(),
	LookAtMouse_t3531508735::get_offset_of__isSleeping_4(),
	LookAtMouse_t3531508735::get_offset_of_sleepRoutine_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (U3CdoSleepU3Ec__Iterator0_t2628874358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[5] = 
{
	U3CdoSleepU3Ec__Iterator0_t2628874358::get_offset_of_seconds_0(),
	U3CdoSleepU3Ec__Iterator0_t2628874358::get_offset_of_U24this_1(),
	U3CdoSleepU3Ec__Iterator0_t2628874358::get_offset_of_U24current_2(),
	U3CdoSleepU3Ec__Iterator0_t2628874358::get_offset_of_U24disposing_3(),
	U3CdoSleepU3Ec__Iterator0_t2628874358::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (Randomizer_t1796594675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[3] = 
{
	Randomizer_t1796594675::get_offset_of_randomRule_0(),
	Randomizer_t1796594675::get_offset_of_min_1(),
	Randomizer_t1796594675::get_offset_of_max_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (DefaultRandomRule_t4265547936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[4] = 
{
	DefaultRandomRule_t4265547936::get_offset_of_asked_0(),
	DefaultRandomRule_t4265547936::get_offset_of_nrLeft_1(),
	DefaultRandomRule_t4265547936::get_offset_of_initialNrLeft_2(),
	DefaultRandomRule_t4265547936::get_offset_of_lastBlock_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (SemiRandomRule_t1463926147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[3] = 
{
	SemiRandomRule_t1463926147::get_offset_of_asked_0(),
	SemiRandomRule_t1463926147::get_offset_of_min_1(),
	SemiRandomRule_t1463926147::get_offset_of_lastBlock_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (TwoDGameObjectArray_t1262067236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (AudibleSound_t2302054751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[7] = 
{
	AudibleSound_t2302054751::get_offset_of_SoundComplete_0(),
	AudibleSound_t2302054751::get_offset_of__sound_1(),
	AudibleSound_t2302054751::get_offset_of__updateVolumeFromLayer_2(),
	AudibleSound_t2302054751::get_offset_of_dispatcher_3(),
	AudibleSound_t2302054751::get_offset_of_source_4(),
	AudibleSound_t2302054751::get_offset_of_targetGameObject_5(),
	AudibleSound_t2302054751::get_offset_of_proccessor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (AudibleSoundDelegate_t3383377214), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (U3CdispatchCompleteEventU3Ec__Iterator0_t1083626444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[4] = 
{
	U3CdispatchCompleteEventU3Ec__Iterator0_t1083626444::get_offset_of_U24this_0(),
	U3CdispatchCompleteEventU3Ec__Iterator0_t1083626444::get_offset_of_U24current_1(),
	U3CdispatchCompleteEventU3Ec__Iterator0_t1083626444::get_offset_of_U24disposing_2(),
	U3CdispatchCompleteEventU3Ec__Iterator0_t1083626444::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (SoundClip_t3743947423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[3] = 
{
	SoundClip_t3743947423::get_offset_of__parentLayer_0(),
	SoundClip_t3743947423::get_offset_of__clip_1(),
	SoundClip_t3743947423::get_offset_of__relativeVolume_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (SoundLayer_t2188462586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[4] = 
{
	SoundLayer_t2188462586::get_offset_of_VolumeChangedEvent_0(),
	SoundLayer_t2188462586::get_offset_of__name_1(),
	SoundLayer_t2188462586::get_offset_of__volume_2(),
	SoundLayer_t2188462586::get_offset_of_sounds_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (VolumeDelegate_t2775777142), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (SoundManager_t13585936), -1, sizeof(SoundManager_t13585936_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2239[8] = 
{
	0,
	0,
	0,
	SoundManager_t13585936::get_offset_of_layers_3(),
	SoundManager_t13585936::get_offset_of_defaultLayer_4(),
	SoundManager_t13585936::get_offset_of_currentlyPlayingSounds_5(),
	SoundManager_t13585936::get_offset_of_defaultGameObject_6(),
	SoundManager_t13585936_StaticFields::get_offset_of_instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (U3CPlayWithDelayU3Ec__Iterator0_t711187121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[7] = 
{
	U3CPlayWithDelayU3Ec__Iterator0_t711187121::get_offset_of_delayInSeconds_0(),
	U3CPlayWithDelayU3Ec__Iterator0_t711187121::get_offset_of_sound_1(),
	U3CPlayWithDelayU3Ec__Iterator0_t711187121::get_offset_of_playOn_2(),
	U3CPlayWithDelayU3Ec__Iterator0_t711187121::get_offset_of_U24this_3(),
	U3CPlayWithDelayU3Ec__Iterator0_t711187121::get_offset_of_U24current_4(),
	U3CPlayWithDelayU3Ec__Iterator0_t711187121::get_offset_of_U24disposing_5(),
	U3CPlayWithDelayU3Ec__Iterator0_t711187121::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (U3CPlayWithDelayU3Ec__Iterator1_t711187122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[6] = 
{
	U3CPlayWithDelayU3Ec__Iterator1_t711187122::get_offset_of_delayInSeconds_0(),
	U3CPlayWithDelayU3Ec__Iterator1_t711187122::get_offset_of_sound_1(),
	U3CPlayWithDelayU3Ec__Iterator1_t711187122::get_offset_of_U24this_2(),
	U3CPlayWithDelayU3Ec__Iterator1_t711187122::get_offset_of_U24current_3(),
	U3CPlayWithDelayU3Ec__Iterator1_t711187122::get_offset_of_U24disposing_4(),
	U3CPlayWithDelayU3Ec__Iterator1_t711187122::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (SoundManagerEventProccessor_t3825530019), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (SoundPlaylist_t1663855991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[7] = 
{
	SoundPlaylist_t1663855991::get_offset_of_PlaylistComplete_0(),
	SoundPlaylist_t1663855991::get_offset_of_PlaylistJump_1(),
	SoundPlaylist_t1663855991::get_offset_of__isPlaying_2(),
	SoundPlaylist_t1663855991::get_offset_of_proccessor_3(),
	SoundPlaylist_t1663855991::get_offset_of__currentSound_4(),
	SoundPlaylist_t1663855991::get_offset_of__currentSoundIndex_5(),
	SoundPlaylist_t1663855991::get_offset_of_sounds_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (PlaylistDelegate_t2451478837), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (ActualFontSize_t3346628750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[1] = 
{
	ActualFontSize_t3346628750::get_offset_of_FontSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (FollowerCam_t3769774071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[11] = 
{
	FollowerCam_t3769774071::get_offset_of_target_2(),
	FollowerCam_t3769774071::get_offset_of_followOnY_3(),
	FollowerCam_t3769774071::get_offset_of_followOnX_4(),
	FollowerCam_t3769774071::get_offset_of_constrainedOnX_5(),
	FollowerCam_t3769774071::get_offset_of_constrainedOnY_6(),
	FollowerCam_t3769774071::get_offset_of_minY_7(),
	FollowerCam_t3769774071::get_offset_of_maxY_8(),
	FollowerCam_t3769774071::get_offset_of_minX_9(),
	FollowerCam_t3769774071::get_offset_of_maxX_10(),
	FollowerCam_t3769774071::get_offset_of_offset_11(),
	FollowerCam_t3769774071::get_offset_of_originalPosition_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (Geom_t3460865626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (LifeHUD_t785887741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[2] = 
{
	LifeHUD_t785887741::get_offset_of_hearts_2(),
	LifeHUD_t785887741::get_offset_of_currentLifes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (ObjectSequence_t1807435812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[5] = 
{
	ObjectSequence_t1807435812::get_offset_of_CurrentChildChanged_2(),
	ObjectSequence_t1807435812::get_offset_of_children_3(),
	ObjectSequence_t1807435812::get_offset_of__currentChild_4(),
	ObjectSequence_t1807435812::get_offset_of__actualCurrentChild_5(),
	ObjectSequence_t1807435812::get_offset_of_startRandom_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (CurrentChildDelegate_t135521474), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (ScalingBar_t3437833994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[4] = 
{
	ScalingBar_t3437833994::get_offset_of__precentage_2(),
	ScalingBar_t3437833994::get_offset_of_scaleType_3(),
	ScalingBar_t3437833994::get_offset_of_originalScale_4(),
	ScalingBar_t3437833994::get_offset_of_oldPrecentage_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (ScaleOn_t3059270671)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2252[3] = 
{
	ScaleOn_t3059270671::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (SequenceMouseEvent_t3638882036), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (TimeCalculator_t1446961881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[2] = 
{
	TimeCalculator_t1446961881::get_offset_of__distance_0(),
	TimeCalculator_t1446961881::get_offset_of__time_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (TwoDSequenceArray_t1727799968), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (GetSongData_t4249642665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[8] = 
{
	0,
	GetSongData_t4249642665::get_offset_of_rmsValue_3(),
	GetSongData_t4249642665::get_offset_of_dbValue_4(),
	GetSongData_t4249642665::get_offset_of_pitchValue_5(),
	GetSongData_t4249642665::get_offset_of_source_6(),
	GetSongData_t4249642665::get_offset_of_samples_7(),
	GetSongData_t4249642665::get_offset_of_spectrum_8(),
	GetSongData_t4249642665::get_offset_of_sampleRate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (InputManager_t1610719423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (Loading_t2871513552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (LoadingSanta_t2184952393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (NavigationManager_t4055369543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[3] = 
{
	NavigationManager_t4055369543::get_offset_of_mainInterfaceBuildIndex_4(),
	NavigationManager_t4055369543::get_offset_of_yearNumberStart_5(),
	NavigationManager_t4055369543::get_offset_of_endScreen_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (U3CYInstantiateEndScreenU3Ec__Iterator0_t2959782327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[5] = 
{
	U3CYInstantiateEndScreenU3Ec__Iterator0_t2959782327::get_offset_of_trans_0(),
	U3CYInstantiateEndScreenU3Ec__Iterator0_t2959782327::get_offset_of_U24this_1(),
	U3CYInstantiateEndScreenU3Ec__Iterator0_t2959782327::get_offset_of_U24current_2(),
	U3CYInstantiateEndScreenU3Ec__Iterator0_t2959782327::get_offset_of_U24disposing_3(),
	U3CYInstantiateEndScreenU3Ec__Iterator0_t2959782327::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (ObjectPool_t2689037807), -1, sizeof(ObjectPool_t2689037807_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2262[8] = 
{
	ObjectPool_t2689037807_StaticFields::get_offset_of_OnRecyle_2(),
	ObjectPool_t2689037807_StaticFields::get_offset_of__instance_3(),
	ObjectPool_t2689037807_StaticFields::get_offset_of_tempList_4(),
	ObjectPool_t2689037807::get_offset_of_pooledObjects_5(),
	ObjectPool_t2689037807::get_offset_of_spawnedObjects_6(),
	ObjectPool_t2689037807::get_offset_of_startupPoolMode_7(),
	ObjectPool_t2689037807::get_offset_of_startupPools_8(),
	ObjectPool_t2689037807::get_offset_of_startupPoolsCreated_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (StartupPoolMode_t3405548370)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2263[4] = 
{
	StartupPoolMode_t3405548370::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (StartupPool_t2587102281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[2] = 
{
	StartupPool_t2587102281::get_offset_of_size_0(),
	StartupPool_t2587102281::get_offset_of_prefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (OnRecycleDelegate_t3801249229), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (ObjectPoolExtensions_t3542169457), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (ParentalControl_t2040855302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[12] = 
{
	ParentalControl_t2040855302::get_offset_of_type_2(),
	ParentalControl_t2040855302::get_offset_of_buttonLeft_3(),
	ParentalControl_t2040855302::get_offset_of_buttonMiddle_4(),
	ParentalControl_t2040855302::get_offset_of_buttonRight_5(),
	ParentalControl_t2040855302::get_offset_of_xButton_6(),
	ParentalControl_t2040855302::get_offset_of_equationText_7(),
	ParentalControl_t2040855302::get_offset_of_texts_8(),
	ParentalControl_t2040855302::get_offset_of_number1_9(),
	ParentalControl_t2040855302::get_offset_of_number2_10(),
	ParentalControl_t2040855302::get_offset_of_correctAnswer_11(),
	ParentalControl_t2040855302::get_offset_of_wrongAnswer1_12(),
	ParentalControl_t2040855302::get_offset_of_wrongAnswer2_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (Type_t3248676223)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2268[3] = 
{
	Type_t3248676223::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (UIButtonOpenParental_t1943914589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[2] = 
{
	UIButtonOpenParental_t1943914589::get_offset_of_parental_4(),
	UIButtonOpenParental_t1943914589::get_offset_of_buttonType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (Type_t1384202272)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2270[3] = 
{
	Type_t1384202272::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (Slide_t3421086917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[1] = 
{
	Slide_t3421086917::get_offset_of_mySpriteRenderer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (U3CYFadeInU3Ec__Iterator0_t2538061961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[5] = 
{
	U3CYFadeInU3Ec__Iterator0_t2538061961::get_offset_of_U3CtimerU3E__0_0(),
	U3CYFadeInU3Ec__Iterator0_t2538061961::get_offset_of_U24this_1(),
	U3CYFadeInU3Ec__Iterator0_t2538061961::get_offset_of_U24current_2(),
	U3CYFadeInU3Ec__Iterator0_t2538061961::get_offset_of_U24disposing_3(),
	U3CYFadeInU3Ec__Iterator0_t2538061961::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (SlideShow_t1942539524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[4] = 
{
	SlideShow_t1942539524::get_offset_of_backgrounds_2(),
	SlideShow_t1942539524::get_offset_of_slide_3(),
	SlideShow_t1942539524::get_offset_of_bgCounter_4(),
	SlideShow_t1942539524::get_offset_of_sortOrder_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (U3CYStartSlideShowU3Ec__Iterator0_t227050163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[5] = 
{
	U3CYStartSlideShowU3Ec__Iterator0_t227050163::get_offset_of_U3CtimerU3E__0_0(),
	U3CYStartSlideShowU3Ec__Iterator0_t227050163::get_offset_of_U24this_1(),
	U3CYStartSlideShowU3Ec__Iterator0_t227050163::get_offset_of_U24current_2(),
	U3CYStartSlideShowU3Ec__Iterator0_t227050163::get_offset_of_U24disposing_3(),
	U3CYStartSlideShowU3Ec__Iterator0_t227050163::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (UIButtonShowSlideshow_t175868935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[2] = 
{
	UIButtonShowSlideshow_t175868935::get_offset_of_slideShow_4(),
	UIButtonShowSlideshow_t175868935::get_offset_of_slidesHolder_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (SoundClip_t2598932235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[2] = 
{
	SoundClip_t2598932235::get_offset_of_sound_0(),
	SoundClip_t2598932235::get_offset_of_volume_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (SoundManager_t654432262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[65] = 
{
	SoundManager_t654432262::get_offset_of_tempSound_4(),
	SoundManager_t654432262::get_offset_of_buttonAppear_5(),
	SoundManager_t654432262::get_offset_of_candyAppear_6(),
	SoundManager_t654432262::get_offset_of_candyDestroy_7(),
	SoundManager_t654432262::get_offset_of_correctChoice_8(),
	SoundManager_t654432262::get_offset_of_incorrectChoice_9(),
	SoundManager_t654432262::get_offset_of_centerLetterChange_10(),
	SoundManager_t654432262::get_offset_of_digSound_11(),
	SoundManager_t654432262::get_offset_of_itemFound_12(),
	SoundManager_t654432262::get_offset_of_hatRise_13(),
	SoundManager_t654432262::get_offset_of_ringThrow_14(),
	SoundManager_t654432262::get_offset_of_correctThrow_15(),
	SoundManager_t654432262::get_offset_of_drawingLoop_16(),
	SoundManager_t654432262::get_offset_of_correctDraw_17(),
	SoundManager_t654432262::get_offset_of_drawDelete_18(),
	SoundManager_t654432262::get_offset_of_cleaningSoundLoop_19(),
	SoundManager_t654432262::get_offset_of_dirtCleaned_20(),
	SoundManager_t654432262::get_offset_of_goodCollect_21(),
	SoundManager_t654432262::get_offset_of_badCollect_22(),
	SoundManager_t654432262::get_offset_of_snowmanRise_23(),
	SoundManager_t654432262::get_offset_of_goodSnowballThrow_24(),
	SoundManager_t654432262::get_offset_of_badSnowballThrow_25(),
	SoundManager_t654432262::get_offset_of_snowballThrow_26(),
	SoundManager_t654432262::get_offset_of_snowballSplash_27(),
	SoundManager_t654432262::get_offset_of_levelStartShake_28(),
	SoundManager_t654432262::get_offset_of_correctPlacement_29(),
	SoundManager_t654432262::get_offset_of_incorrectPlacement_30(),
	SoundManager_t654432262::get_offset_of_levelFinished_31(),
	SoundManager_t654432262::get_offset_of_featureChange_32(),
	SoundManager_t654432262::get_offset_of_bubbleTap_33(),
	SoundManager_t654432262::get_offset_of_lineDrawingLoop_34(),
	SoundManager_t654432262::get_offset_of_correctLine_35(),
	SoundManager_t654432262::get_offset_of_incorrectLine_36(),
	SoundManager_t654432262::get_offset_of_waterToSnowChange_37(),
	SoundManager_t654432262::get_offset_of_clockTicking_38(),
	SoundManager_t654432262::get_offset_of_clockTickingFast_39(),
	SoundManager_t654432262::get_offset_of_correctFound_40(),
	SoundManager_t654432262::get_offset_of_incorrectTap_41(),
	SoundManager_t654432262::get_offset_of_snowmanAppearingSound_42(),
	SoundManager_t654432262::get_offset_of_correctSnowman_43(),
	SoundManager_t654432262::get_offset_of_flipSound_44(),
	SoundManager_t654432262::get_offset_of_badMatch_45(),
	SoundManager_t654432262::get_offset_of_goodMatch_46(),
	SoundManager_t654432262::get_offset_of_gridLevelEnd_47(),
	SoundManager_t654432262::get_offset_of_bulbTapSound_48(),
	SoundManager_t654432262::get_offset_of_lineWin_49(),
	SoundManager_t654432262::get_offset_of_roundWin_50(),
	SoundManager_t654432262::get_offset_of_moveSound_51(),
	SoundManager_t654432262::get_offset_of_fallSound_52(),
	SoundManager_t654432262::get_offset_of_rotateSound_53(),
	SoundManager_t654432262::get_offset_of_lineSound_54(),
	SoundManager_t654432262::get_offset_of_foundSound_55(),
	SoundManager_t654432262::get_offset_of_sounds_56(),
	SoundManager_t654432262::get_offset_of_badSound_57(),
	SoundManager_t654432262::get_offset_of_correctSound_58(),
	SoundManager_t654432262::get_offset_of_noteSounds_59(),
	SoundManager_t654432262::get_offset_of_endHappyElf_60(),
	SoundManager_t654432262::get_offset_of_correctDrawn_61(),
	SoundManager_t654432262::get_offset_of_incorrectDrawn_62(),
	SoundManager_t654432262::get_offset_of_wrappingSoundLoop_63(),
	SoundManager_t654432262::get_offset_of_correctResult_64(),
	SoundManager_t654432262::get_offset_of_incorrectResult_65(),
	SoundManager_t654432262::get_offset_of_tapSound_66(),
	SoundManager_t654432262::get_offset_of_giftSpawnSound_67(),
	SoundManager_t654432262::get_offset_of_hitSound_68(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (UIButtonTutorial_t123001136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[7] = 
{
	UIButtonTutorial_t123001136::get_offset_of_tutorialTexts_4(),
	UIButtonTutorial_t123001136::get_offset_of_helpText_5(),
	UIButtonTutorial_t123001136::get_offset_of_canClick_6(),
	UIButtonTutorial_t123001136::get_offset_of_minX_7(),
	UIButtonTutorial_t123001136::get_offset_of_maxX_8(),
	UIButtonTutorial_t123001136::get_offset_of_minXtetris_9(),
	UIButtonTutorial_t123001136::get_offset_of_maxXtetris_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (U3CYCloseTutorialU3Ec__Iterator0_t1054761711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[4] = 
{
	U3CYCloseTutorialU3Ec__Iterator0_t1054761711::get_offset_of_U24this_0(),
	U3CYCloseTutorialU3Ec__Iterator0_t1054761711::get_offset_of_U24current_1(),
	U3CYCloseTutorialU3Ec__Iterator0_t1054761711::get_offset_of_U24disposing_2(),
	U3CYCloseTutorialU3Ec__Iterator0_t1054761711::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (U3CYOpenTutorialU3Ec__Iterator1_t230414610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[4] = 
{
	U3CYOpenTutorialU3Ec__Iterator1_t230414610::get_offset_of_U24this_0(),
	U3CYOpenTutorialU3Ec__Iterator1_t230414610::get_offset_of_U24current_1(),
	U3CYOpenTutorialU3Ec__Iterator1_t230414610::get_offset_of_U24disposing_2(),
	U3CYOpenTutorialU3Ec__Iterator1_t230414610::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (UIButton_t3377238306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[2] = 
{
	UIButton_t3377238306::get_offset_of_OnClickDown_2(),
	UIButton_t3377238306::get_offset_of_OnClickUp_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (UIButtonBack_t1890699263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (UIButtonLoadGame_t1239272532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[4] = 
{
	UIButtonLoadGame_t1239272532::get_offset_of_sceneToLoad_4(),
	UIButtonLoadGame_t1239272532::get_offset_of_isOpen_5(),
	UIButtonLoadGame_t1239272532::get_offset_of_notDone_6(),
	UIButtonLoadGame_t1239272532::get_offset_of_bg_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (U3CYLoadSceneU3Ec__Iterator0_t3230658609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[4] = 
{
	U3CYLoadSceneU3Ec__Iterator0_t3230658609::get_offset_of_U24this_0(),
	U3CYLoadSceneU3Ec__Iterator0_t3230658609::get_offset_of_U24current_1(),
	U3CYLoadSceneU3Ec__Iterator0_t3230658609::get_offset_of_U24disposing_2(),
	U3CYLoadSceneU3Ec__Iterator0_t3230658609::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (Utils_t4194145797), -1, sizeof(Utils_t4194145797_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2286[1] = 
{
	Utils_t4194145797_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (CFX_AutoStopLoopedEffect_t3214280257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[2] = 
{
	CFX_AutoStopLoopedEffect_t3214280257::get_offset_of_effectDuration_2(),
	CFX_AutoStopLoopedEffect_t3214280257::get_offset_of_d_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (CFX_Demo_t1465385775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[12] = 
{
	CFX_Demo_t1465385775::get_offset_of_orderedSpawns_2(),
	CFX_Demo_t1465385775::get_offset_of_step_3(),
	CFX_Demo_t1465385775::get_offset_of_range_4(),
	CFX_Demo_t1465385775::get_offset_of_order_5(),
	CFX_Demo_t1465385775::get_offset_of_groundMat_6(),
	CFX_Demo_t1465385775::get_offset_of_waterMat_7(),
	CFX_Demo_t1465385775::get_offset_of_ParticleExamples_8(),
	CFX_Demo_t1465385775::get_offset_of_ParticlesYOffsetD_9(),
	CFX_Demo_t1465385775::get_offset_of_exampleIndex_10(),
	CFX_Demo_t1465385775::get_offset_of_randomSpawnsDelay_11(),
	CFX_Demo_t1465385775::get_offset_of_randomSpawns_12(),
	CFX_Demo_t1465385775::get_offset_of_slowMo_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[5] = 
{
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400::get_offset_of_U3CparticlesU3E__0_0(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400::get_offset_of_U24this_1(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400::get_offset_of_U24current_2(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400::get_offset_of_U24disposing_3(),
	U3CRandomSpawnsCoroutineU3Ec__Iterator0_t3999667400::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (CFX_Demo_New_t3744422722), -1, sizeof(CFX_Demo_New_t3744422722_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2290[17] = 
{
	CFX_Demo_New_t3744422722::get_offset_of_groundRenderer_2(),
	CFX_Demo_New_t3744422722::get_offset_of_groundCollider_3(),
	CFX_Demo_New_t3744422722::get_offset_of_slowMoBtn_4(),
	CFX_Demo_New_t3744422722::get_offset_of_slowMoLabel_5(),
	CFX_Demo_New_t3744422722::get_offset_of_camRotBtn_6(),
	CFX_Demo_New_t3744422722::get_offset_of_camRotLabel_7(),
	CFX_Demo_New_t3744422722::get_offset_of_groundBtn_8(),
	CFX_Demo_New_t3744422722::get_offset_of_groundLabel_9(),
	CFX_Demo_New_t3744422722::get_offset_of_EffectLabel_10(),
	CFX_Demo_New_t3744422722::get_offset_of_EffectIndexLabel_11(),
	CFX_Demo_New_t3744422722::get_offset_of_ParticleExamples_12(),
	CFX_Demo_New_t3744422722::get_offset_of_exampleIndex_13(),
	CFX_Demo_New_t3744422722::get_offset_of_slowMo_14(),
	CFX_Demo_New_t3744422722::get_offset_of_defaultCamPosition_15(),
	CFX_Demo_New_t3744422722::get_offset_of_defaultCamRotation_16(),
	CFX_Demo_New_t3744422722::get_offset_of_onScreenParticles_17(),
	CFX_Demo_New_t3744422722_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (U3CCheckForDeletedParticlesU3Ec__Iterator0_t2558571495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[4] = 
{
	U3CCheckForDeletedParticlesU3Ec__Iterator0_t2558571495::get_offset_of_U24this_0(),
	U3CCheckForDeletedParticlesU3Ec__Iterator0_t2558571495::get_offset_of_U24current_1(),
	U3CCheckForDeletedParticlesU3Ec__Iterator0_t2558571495::get_offset_of_U24disposing_2(),
	U3CCheckForDeletedParticlesU3Ec__Iterator0_t2558571495::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (CFX_Demo_RandomDir_t1076153330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[2] = 
{
	CFX_Demo_RandomDir_t1076153330::get_offset_of_min_2(),
	CFX_Demo_RandomDir_t1076153330::get_offset_of_max_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (CFX_Demo_RandomDirectionTranslate_t747977598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[5] = 
{
	CFX_Demo_RandomDirectionTranslate_t747977598::get_offset_of_speed_2(),
	CFX_Demo_RandomDirectionTranslate_t747977598::get_offset_of_baseDir_3(),
	CFX_Demo_RandomDirectionTranslate_t747977598::get_offset_of_axis_4(),
	CFX_Demo_RandomDirectionTranslate_t747977598::get_offset_of_gravity_5(),
	CFX_Demo_RandomDirectionTranslate_t747977598::get_offset_of_dir_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (CFX_Demo_RotateCamera_t2665602760), -1, sizeof(CFX_Demo_RotateCamera_t2665602760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2294[3] = 
{
	CFX_Demo_RotateCamera_t2665602760_StaticFields::get_offset_of_rotating_2(),
	CFX_Demo_RotateCamera_t2665602760::get_offset_of_speed_3(),
	CFX_Demo_RotateCamera_t2665602760::get_offset_of_rotationCenter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (CFX_Demo_Translate_t3218432890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[5] = 
{
	CFX_Demo_Translate_t3218432890::get_offset_of_speed_2(),
	CFX_Demo_Translate_t3218432890::get_offset_of_rotation_3(),
	CFX_Demo_Translate_t3218432890::get_offset_of_axis_4(),
	CFX_Demo_Translate_t3218432890::get_offset_of_gravity_5(),
	CFX_Demo_Translate_t3218432890::get_offset_of_dir_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (CFX_AutoDestructShuriken_t1500114366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[1] = 
{
	CFX_AutoDestructShuriken_t1500114366::get_offset_of_OnlyDeactivate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (U3CCheckIfAliveU3Ec__Iterator0_t3107901010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[5] = 
{
	U3CCheckIfAliveU3Ec__Iterator0_t3107901010::get_offset_of_U3CpsU3E__0_0(),
	U3CCheckIfAliveU3Ec__Iterator0_t3107901010::get_offset_of_U24this_1(),
	U3CCheckIfAliveU3Ec__Iterator0_t3107901010::get_offset_of_U24current_2(),
	U3CCheckIfAliveU3Ec__Iterator0_t3107901010::get_offset_of_U24disposing_3(),
	U3CCheckIfAliveU3Ec__Iterator0_t3107901010::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (CFX_AutoRotate_t1831446564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[2] = 
{
	CFX_AutoRotate_t1831446564::get_offset_of_rotation_2(),
	CFX_AutoRotate_t1831446564::get_offset_of_space_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (CFX_AutodestructWhenNoChildren_t3972162727), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
