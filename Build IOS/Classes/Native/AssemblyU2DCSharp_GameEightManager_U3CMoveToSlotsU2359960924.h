﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// GameEightManager
struct GameEightManager_t903757396;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEightManager/<MoveToSlots>c__Iterator3
struct  U3CMoveToSlotsU3Ec__Iterator3_t2359960924  : public Il2CppObject
{
public:
	// UnityEngine.Transform[] GameEightManager/<MoveToSlots>c__Iterator3::pieces
	TransformU5BU5D_t3764228911* ___pieces_0;
	// UnityEngine.Transform[] GameEightManager/<MoveToSlots>c__Iterator3::$locvar0
	TransformU5BU5D_t3764228911* ___U24locvar0_1;
	// System.Int32 GameEightManager/<MoveToSlots>c__Iterator3::$locvar1
	int32_t ___U24locvar1_2;
	// GameEightManager GameEightManager/<MoveToSlots>c__Iterator3::$this
	GameEightManager_t903757396 * ___U24this_3;
	// System.Object GameEightManager/<MoveToSlots>c__Iterator3::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean GameEightManager/<MoveToSlots>c__Iterator3::$disposing
	bool ___U24disposing_5;
	// System.Int32 GameEightManager/<MoveToSlots>c__Iterator3::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_pieces_0() { return static_cast<int32_t>(offsetof(U3CMoveToSlotsU3Ec__Iterator3_t2359960924, ___pieces_0)); }
	inline TransformU5BU5D_t3764228911* get_pieces_0() const { return ___pieces_0; }
	inline TransformU5BU5D_t3764228911** get_address_of_pieces_0() { return &___pieces_0; }
	inline void set_pieces_0(TransformU5BU5D_t3764228911* value)
	{
		___pieces_0 = value;
		Il2CppCodeGenWriteBarrier(&___pieces_0, value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CMoveToSlotsU3Ec__Iterator3_t2359960924, ___U24locvar0_1)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_1, value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CMoveToSlotsU3Ec__Iterator3_t2359960924, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CMoveToSlotsU3Ec__Iterator3_t2359960924, ___U24this_3)); }
	inline GameEightManager_t903757396 * get_U24this_3() const { return ___U24this_3; }
	inline GameEightManager_t903757396 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(GameEightManager_t903757396 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CMoveToSlotsU3Ec__Iterator3_t2359960924, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CMoveToSlotsU3Ec__Iterator3_t2359960924, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CMoveToSlotsU3Ec__Iterator3_t2359960924, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
