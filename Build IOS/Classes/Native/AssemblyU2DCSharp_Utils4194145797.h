﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Random
struct Random_t1044426839;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils
struct  Utils_t4194145797  : public Il2CppObject
{
public:

public:
};

struct Utils_t4194145797_StaticFields
{
public:
	// System.Random Utils::rng
	Random_t1044426839 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(Utils_t4194145797_StaticFields, ___rng_0)); }
	inline Random_t1044426839 * get_rng_0() const { return ___rng_0; }
	inline Random_t1044426839 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(Random_t1044426839 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier(&___rng_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
