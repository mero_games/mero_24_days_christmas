﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Events.KeyboardEventSystem/<isPressed>c__AnonStorey0
struct  U3CisPressedU3Ec__AnonStorey0_t2394732920  : public Il2CppObject
{
public:
	// UnityEngine.KeyCode GameDevToolkit.Events.KeyboardEventSystem/<isPressed>c__AnonStorey0::keyCode
	int32_t ___keyCode_0;

public:
	inline static int32_t get_offset_of_keyCode_0() { return static_cast<int32_t>(offsetof(U3CisPressedU3Ec__AnonStorey0_t2394732920, ___keyCode_0)); }
	inline int32_t get_keyCode_0() const { return ___keyCode_0; }
	inline int32_t* get_address_of_keyCode_0() { return &___keyCode_0; }
	inline void set_keyCode_0(int32_t value)
	{
		___keyCode_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
