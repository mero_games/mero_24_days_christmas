﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Comparison`1<UnityEngine.GameObject>
struct Comparison_1_t3018271998;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_New
struct  CFX_Demo_New_t3744422722  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Renderer CFX_Demo_New::groundRenderer
	Renderer_t257310565 * ___groundRenderer_2;
	// UnityEngine.Collider CFX_Demo_New::groundCollider
	Collider_t3497673348 * ___groundCollider_3;
	// UnityEngine.UI.Image CFX_Demo_New::slowMoBtn
	Image_t2042527209 * ___slowMoBtn_4;
	// UnityEngine.UI.Text CFX_Demo_New::slowMoLabel
	Text_t356221433 * ___slowMoLabel_5;
	// UnityEngine.UI.Image CFX_Demo_New::camRotBtn
	Image_t2042527209 * ___camRotBtn_6;
	// UnityEngine.UI.Text CFX_Demo_New::camRotLabel
	Text_t356221433 * ___camRotLabel_7;
	// UnityEngine.UI.Image CFX_Demo_New::groundBtn
	Image_t2042527209 * ___groundBtn_8;
	// UnityEngine.UI.Text CFX_Demo_New::groundLabel
	Text_t356221433 * ___groundLabel_9;
	// UnityEngine.UI.Text CFX_Demo_New::EffectLabel
	Text_t356221433 * ___EffectLabel_10;
	// UnityEngine.UI.Text CFX_Demo_New::EffectIndexLabel
	Text_t356221433 * ___EffectIndexLabel_11;
	// UnityEngine.GameObject[] CFX_Demo_New::ParticleExamples
	GameObjectU5BU5D_t3057952154* ___ParticleExamples_12;
	// System.Int32 CFX_Demo_New::exampleIndex
	int32_t ___exampleIndex_13;
	// System.Boolean CFX_Demo_New::slowMo
	bool ___slowMo_14;
	// UnityEngine.Vector3 CFX_Demo_New::defaultCamPosition
	Vector3_t2243707580  ___defaultCamPosition_15;
	// UnityEngine.Quaternion CFX_Demo_New::defaultCamRotation
	Quaternion_t4030073918  ___defaultCamRotation_16;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CFX_Demo_New::onScreenParticles
	List_1_t1125654279 * ___onScreenParticles_17;

public:
	inline static int32_t get_offset_of_groundRenderer_2() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___groundRenderer_2)); }
	inline Renderer_t257310565 * get_groundRenderer_2() const { return ___groundRenderer_2; }
	inline Renderer_t257310565 ** get_address_of_groundRenderer_2() { return &___groundRenderer_2; }
	inline void set_groundRenderer_2(Renderer_t257310565 * value)
	{
		___groundRenderer_2 = value;
		Il2CppCodeGenWriteBarrier(&___groundRenderer_2, value);
	}

	inline static int32_t get_offset_of_groundCollider_3() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___groundCollider_3)); }
	inline Collider_t3497673348 * get_groundCollider_3() const { return ___groundCollider_3; }
	inline Collider_t3497673348 ** get_address_of_groundCollider_3() { return &___groundCollider_3; }
	inline void set_groundCollider_3(Collider_t3497673348 * value)
	{
		___groundCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&___groundCollider_3, value);
	}

	inline static int32_t get_offset_of_slowMoBtn_4() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___slowMoBtn_4)); }
	inline Image_t2042527209 * get_slowMoBtn_4() const { return ___slowMoBtn_4; }
	inline Image_t2042527209 ** get_address_of_slowMoBtn_4() { return &___slowMoBtn_4; }
	inline void set_slowMoBtn_4(Image_t2042527209 * value)
	{
		___slowMoBtn_4 = value;
		Il2CppCodeGenWriteBarrier(&___slowMoBtn_4, value);
	}

	inline static int32_t get_offset_of_slowMoLabel_5() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___slowMoLabel_5)); }
	inline Text_t356221433 * get_slowMoLabel_5() const { return ___slowMoLabel_5; }
	inline Text_t356221433 ** get_address_of_slowMoLabel_5() { return &___slowMoLabel_5; }
	inline void set_slowMoLabel_5(Text_t356221433 * value)
	{
		___slowMoLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___slowMoLabel_5, value);
	}

	inline static int32_t get_offset_of_camRotBtn_6() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___camRotBtn_6)); }
	inline Image_t2042527209 * get_camRotBtn_6() const { return ___camRotBtn_6; }
	inline Image_t2042527209 ** get_address_of_camRotBtn_6() { return &___camRotBtn_6; }
	inline void set_camRotBtn_6(Image_t2042527209 * value)
	{
		___camRotBtn_6 = value;
		Il2CppCodeGenWriteBarrier(&___camRotBtn_6, value);
	}

	inline static int32_t get_offset_of_camRotLabel_7() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___camRotLabel_7)); }
	inline Text_t356221433 * get_camRotLabel_7() const { return ___camRotLabel_7; }
	inline Text_t356221433 ** get_address_of_camRotLabel_7() { return &___camRotLabel_7; }
	inline void set_camRotLabel_7(Text_t356221433 * value)
	{
		___camRotLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___camRotLabel_7, value);
	}

	inline static int32_t get_offset_of_groundBtn_8() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___groundBtn_8)); }
	inline Image_t2042527209 * get_groundBtn_8() const { return ___groundBtn_8; }
	inline Image_t2042527209 ** get_address_of_groundBtn_8() { return &___groundBtn_8; }
	inline void set_groundBtn_8(Image_t2042527209 * value)
	{
		___groundBtn_8 = value;
		Il2CppCodeGenWriteBarrier(&___groundBtn_8, value);
	}

	inline static int32_t get_offset_of_groundLabel_9() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___groundLabel_9)); }
	inline Text_t356221433 * get_groundLabel_9() const { return ___groundLabel_9; }
	inline Text_t356221433 ** get_address_of_groundLabel_9() { return &___groundLabel_9; }
	inline void set_groundLabel_9(Text_t356221433 * value)
	{
		___groundLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___groundLabel_9, value);
	}

	inline static int32_t get_offset_of_EffectLabel_10() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___EffectLabel_10)); }
	inline Text_t356221433 * get_EffectLabel_10() const { return ___EffectLabel_10; }
	inline Text_t356221433 ** get_address_of_EffectLabel_10() { return &___EffectLabel_10; }
	inline void set_EffectLabel_10(Text_t356221433 * value)
	{
		___EffectLabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___EffectLabel_10, value);
	}

	inline static int32_t get_offset_of_EffectIndexLabel_11() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___EffectIndexLabel_11)); }
	inline Text_t356221433 * get_EffectIndexLabel_11() const { return ___EffectIndexLabel_11; }
	inline Text_t356221433 ** get_address_of_EffectIndexLabel_11() { return &___EffectIndexLabel_11; }
	inline void set_EffectIndexLabel_11(Text_t356221433 * value)
	{
		___EffectIndexLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___EffectIndexLabel_11, value);
	}

	inline static int32_t get_offset_of_ParticleExamples_12() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___ParticleExamples_12)); }
	inline GameObjectU5BU5D_t3057952154* get_ParticleExamples_12() const { return ___ParticleExamples_12; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ParticleExamples_12() { return &___ParticleExamples_12; }
	inline void set_ParticleExamples_12(GameObjectU5BU5D_t3057952154* value)
	{
		___ParticleExamples_12 = value;
		Il2CppCodeGenWriteBarrier(&___ParticleExamples_12, value);
	}

	inline static int32_t get_offset_of_exampleIndex_13() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___exampleIndex_13)); }
	inline int32_t get_exampleIndex_13() const { return ___exampleIndex_13; }
	inline int32_t* get_address_of_exampleIndex_13() { return &___exampleIndex_13; }
	inline void set_exampleIndex_13(int32_t value)
	{
		___exampleIndex_13 = value;
	}

	inline static int32_t get_offset_of_slowMo_14() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___slowMo_14)); }
	inline bool get_slowMo_14() const { return ___slowMo_14; }
	inline bool* get_address_of_slowMo_14() { return &___slowMo_14; }
	inline void set_slowMo_14(bool value)
	{
		___slowMo_14 = value;
	}

	inline static int32_t get_offset_of_defaultCamPosition_15() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___defaultCamPosition_15)); }
	inline Vector3_t2243707580  get_defaultCamPosition_15() const { return ___defaultCamPosition_15; }
	inline Vector3_t2243707580 * get_address_of_defaultCamPosition_15() { return &___defaultCamPosition_15; }
	inline void set_defaultCamPosition_15(Vector3_t2243707580  value)
	{
		___defaultCamPosition_15 = value;
	}

	inline static int32_t get_offset_of_defaultCamRotation_16() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___defaultCamRotation_16)); }
	inline Quaternion_t4030073918  get_defaultCamRotation_16() const { return ___defaultCamRotation_16; }
	inline Quaternion_t4030073918 * get_address_of_defaultCamRotation_16() { return &___defaultCamRotation_16; }
	inline void set_defaultCamRotation_16(Quaternion_t4030073918  value)
	{
		___defaultCamRotation_16 = value;
	}

	inline static int32_t get_offset_of_onScreenParticles_17() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722, ___onScreenParticles_17)); }
	inline List_1_t1125654279 * get_onScreenParticles_17() const { return ___onScreenParticles_17; }
	inline List_1_t1125654279 ** get_address_of_onScreenParticles_17() { return &___onScreenParticles_17; }
	inline void set_onScreenParticles_17(List_1_t1125654279 * value)
	{
		___onScreenParticles_17 = value;
		Il2CppCodeGenWriteBarrier(&___onScreenParticles_17, value);
	}
};

struct CFX_Demo_New_t3744422722_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.GameObject> CFX_Demo_New::<>f__am$cache0
	Comparison_1_t3018271998 * ___U3CU3Ef__amU24cache0_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_18() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t3744422722_StaticFields, ___U3CU3Ef__amU24cache0_18)); }
	inline Comparison_1_t3018271998 * get_U3CU3Ef__amU24cache0_18() const { return ___U3CU3Ef__amU24cache0_18; }
	inline Comparison_1_t3018271998 ** get_address_of_U3CU3Ef__amU24cache0_18() { return &___U3CU3Ef__amU24cache0_18; }
	inline void set_U3CU3Ef__amU24cache0_18(Comparison_1_t3018271998 * value)
	{
		___U3CU3Ef__amU24cache0_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
