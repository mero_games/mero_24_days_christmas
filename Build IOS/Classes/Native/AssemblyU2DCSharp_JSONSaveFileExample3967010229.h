﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Data.JSONSaveFile
struct JSONSaveFile_t938573021;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONSaveFileExample
struct  JSONSaveFileExample_t3967010229  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Data.JSONSaveFile JSONSaveFileExample::saveFile
	JSONSaveFile_t938573021 * ___saveFile_2;

public:
	inline static int32_t get_offset_of_saveFile_2() { return static_cast<int32_t>(offsetof(JSONSaveFileExample_t3967010229, ___saveFile_2)); }
	inline JSONSaveFile_t938573021 * get_saveFile_2() const { return ___saveFile_2; }
	inline JSONSaveFile_t938573021 ** get_address_of_saveFile_2() { return &___saveFile_2; }
	inline void set_saveFile_2(JSONSaveFile_t938573021 * value)
	{
		___saveFile_2 = value;
		Il2CppCodeGenWriteBarrier(&___saveFile_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
