﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HatEnabler
struct  HatEnabler_t1910487184  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject HatEnabler::sleepingState
	GameObject_t1756533147 * ___sleepingState_2;
	// UnityEngine.GameObject HatEnabler::standingState
	GameObject_t1756533147 * ___standingState_3;
	// System.Boolean HatEnabler::isStanding
	bool ___isStanding_4;

public:
	inline static int32_t get_offset_of_sleepingState_2() { return static_cast<int32_t>(offsetof(HatEnabler_t1910487184, ___sleepingState_2)); }
	inline GameObject_t1756533147 * get_sleepingState_2() const { return ___sleepingState_2; }
	inline GameObject_t1756533147 ** get_address_of_sleepingState_2() { return &___sleepingState_2; }
	inline void set_sleepingState_2(GameObject_t1756533147 * value)
	{
		___sleepingState_2 = value;
		Il2CppCodeGenWriteBarrier(&___sleepingState_2, value);
	}

	inline static int32_t get_offset_of_standingState_3() { return static_cast<int32_t>(offsetof(HatEnabler_t1910487184, ___standingState_3)); }
	inline GameObject_t1756533147 * get_standingState_3() const { return ___standingState_3; }
	inline GameObject_t1756533147 ** get_address_of_standingState_3() { return &___standingState_3; }
	inline void set_standingState_3(GameObject_t1756533147 * value)
	{
		___standingState_3 = value;
		Il2CppCodeGenWriteBarrier(&___standingState_3, value);
	}

	inline static int32_t get_offset_of_isStanding_4() { return static_cast<int32_t>(offsetof(HatEnabler_t1910487184, ___isStanding_4)); }
	inline bool get_isStanding_4() const { return ___isStanding_4; }
	inline bool* get_address_of_isStanding_4() { return &___isStanding_4; }
	inline void set_isStanding_4(bool value)
	{
		___isStanding_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
