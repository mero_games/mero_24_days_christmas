﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Collider2D
struct Collider2D_t646061738;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightBulb
struct  LightBulb_t3998556073  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean LightBulb::canGlow
	bool ___canGlow_2;
	// System.Boolean LightBulb::canClick
	bool ___canClick_3;
	// UnityEngine.SpriteRenderer LightBulb::_sprite
	SpriteRenderer_t1209076198 * ____sprite_4;
	// UnityEngine.SpriteRenderer LightBulb::_spriteGlow
	SpriteRenderer_t1209076198 * ____spriteGlow_5;
	// UnityEngine.Collider2D LightBulb::_col
	Collider2D_t646061738 * ____col_6;
	// System.Int32 LightBulb::<type>k__BackingField
	int32_t ___U3CtypeU3Ek__BackingField_7;
	// System.Int32 LightBulb::<row>k__BackingField
	int32_t ___U3CrowU3Ek__BackingField_8;
	// System.Int32 LightBulb::<num>k__BackingField
	int32_t ___U3CnumU3Ek__BackingField_9;
	// System.Int32 LightBulb::<col>k__BackingField
	int32_t ___U3CcolU3Ek__BackingField_10;
	// System.Boolean LightBulb::<lightUp>k__BackingField
	bool ___U3ClightUpU3Ek__BackingField_11;
	// UnityEngine.Color LightBulb::<baseColor>k__BackingField
	Color_t2020392075  ___U3CbaseColorU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_canGlow_2() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ___canGlow_2)); }
	inline bool get_canGlow_2() const { return ___canGlow_2; }
	inline bool* get_address_of_canGlow_2() { return &___canGlow_2; }
	inline void set_canGlow_2(bool value)
	{
		___canGlow_2 = value;
	}

	inline static int32_t get_offset_of_canClick_3() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ___canClick_3)); }
	inline bool get_canClick_3() const { return ___canClick_3; }
	inline bool* get_address_of_canClick_3() { return &___canClick_3; }
	inline void set_canClick_3(bool value)
	{
		___canClick_3 = value;
	}

	inline static int32_t get_offset_of__sprite_4() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ____sprite_4)); }
	inline SpriteRenderer_t1209076198 * get__sprite_4() const { return ____sprite_4; }
	inline SpriteRenderer_t1209076198 ** get_address_of__sprite_4() { return &____sprite_4; }
	inline void set__sprite_4(SpriteRenderer_t1209076198 * value)
	{
		____sprite_4 = value;
		Il2CppCodeGenWriteBarrier(&____sprite_4, value);
	}

	inline static int32_t get_offset_of__spriteGlow_5() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ____spriteGlow_5)); }
	inline SpriteRenderer_t1209076198 * get__spriteGlow_5() const { return ____spriteGlow_5; }
	inline SpriteRenderer_t1209076198 ** get_address_of__spriteGlow_5() { return &____spriteGlow_5; }
	inline void set__spriteGlow_5(SpriteRenderer_t1209076198 * value)
	{
		____spriteGlow_5 = value;
		Il2CppCodeGenWriteBarrier(&____spriteGlow_5, value);
	}

	inline static int32_t get_offset_of__col_6() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ____col_6)); }
	inline Collider2D_t646061738 * get__col_6() const { return ____col_6; }
	inline Collider2D_t646061738 ** get_address_of__col_6() { return &____col_6; }
	inline void set__col_6(Collider2D_t646061738 * value)
	{
		____col_6 = value;
		Il2CppCodeGenWriteBarrier(&____col_6, value);
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ___U3CtypeU3Ek__BackingField_7)); }
	inline int32_t get_U3CtypeU3Ek__BackingField_7() const { return ___U3CtypeU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CtypeU3Ek__BackingField_7() { return &___U3CtypeU3Ek__BackingField_7; }
	inline void set_U3CtypeU3Ek__BackingField_7(int32_t value)
	{
		___U3CtypeU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CrowU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ___U3CrowU3Ek__BackingField_8)); }
	inline int32_t get_U3CrowU3Ek__BackingField_8() const { return ___U3CrowU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CrowU3Ek__BackingField_8() { return &___U3CrowU3Ek__BackingField_8; }
	inline void set_U3CrowU3Ek__BackingField_8(int32_t value)
	{
		___U3CrowU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CnumU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ___U3CnumU3Ek__BackingField_9)); }
	inline int32_t get_U3CnumU3Ek__BackingField_9() const { return ___U3CnumU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CnumU3Ek__BackingField_9() { return &___U3CnumU3Ek__BackingField_9; }
	inline void set_U3CnumU3Ek__BackingField_9(int32_t value)
	{
		___U3CnumU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CcolU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ___U3CcolU3Ek__BackingField_10)); }
	inline int32_t get_U3CcolU3Ek__BackingField_10() const { return ___U3CcolU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CcolU3Ek__BackingField_10() { return &___U3CcolU3Ek__BackingField_10; }
	inline void set_U3CcolU3Ek__BackingField_10(int32_t value)
	{
		___U3CcolU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3ClightUpU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ___U3ClightUpU3Ek__BackingField_11)); }
	inline bool get_U3ClightUpU3Ek__BackingField_11() const { return ___U3ClightUpU3Ek__BackingField_11; }
	inline bool* get_address_of_U3ClightUpU3Ek__BackingField_11() { return &___U3ClightUpU3Ek__BackingField_11; }
	inline void set_U3ClightUpU3Ek__BackingField_11(bool value)
	{
		___U3ClightUpU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CbaseColorU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(LightBulb_t3998556073, ___U3CbaseColorU3Ek__BackingField_12)); }
	inline Color_t2020392075  get_U3CbaseColorU3Ek__BackingField_12() const { return ___U3CbaseColorU3Ek__BackingField_12; }
	inline Color_t2020392075 * get_address_of_U3CbaseColorU3Ek__BackingField_12() { return &___U3CbaseColorU3Ek__BackingField_12; }
	inline void set_U3CbaseColorU3Ek__BackingField_12(Color_t2020392075  value)
	{
		___U3CbaseColorU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
