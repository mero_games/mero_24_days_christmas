﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEighteenManager/<GetBulb>c__AnonStorey1
struct  U3CGetBulbU3Ec__AnonStorey1_t2219634306  : public Il2CppObject
{
public:
	// UnityEngine.RaycastHit2D GameEighteenManager/<GetBulb>c__AnonStorey1::hit
	RaycastHit2D_t4063908774  ___hit_0;

public:
	inline static int32_t get_offset_of_hit_0() { return static_cast<int32_t>(offsetof(U3CGetBulbU3Ec__AnonStorey1_t2219634306, ___hit_0)); }
	inline RaycastHit2D_t4063908774  get_hit_0() const { return ___hit_0; }
	inline RaycastHit2D_t4063908774 * get_address_of_hit_0() { return &___hit_0; }
	inline void set_hit_0(RaycastHit2D_t4063908774  value)
	{
		___hit_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
