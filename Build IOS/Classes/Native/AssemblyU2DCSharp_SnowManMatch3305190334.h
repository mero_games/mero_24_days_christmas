﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SnowManMatch
struct  SnowManMatch_t3305190334  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean SnowManMatch::<IsCorrect>k__BackingField
	bool ___U3CIsCorrectU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIsCorrectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SnowManMatch_t3305190334, ___U3CIsCorrectU3Ek__BackingField_2)); }
	inline bool get_U3CIsCorrectU3Ek__BackingField_2() const { return ___U3CIsCorrectU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsCorrectU3Ek__BackingField_2() { return &___U3CIsCorrectU3Ek__BackingField_2; }
	inline void set_U3CIsCorrectU3Ek__BackingField_2(bool value)
	{
		___U3CIsCorrectU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
