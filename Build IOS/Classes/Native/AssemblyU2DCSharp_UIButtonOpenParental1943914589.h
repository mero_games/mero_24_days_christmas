﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_UIButton3377238306.h"
#include "AssemblyU2DCSharp_UIButtonOpenParental_Type1384202272.h"

// ParentalControl
struct ParentalControl_t2040855302;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonOpenParental
struct  UIButtonOpenParental_t1943914589  : public UIButton_t3377238306
{
public:
	// ParentalControl UIButtonOpenParental::parental
	ParentalControl_t2040855302 * ___parental_4;
	// UIButtonOpenParental/Type UIButtonOpenParental::buttonType
	int32_t ___buttonType_5;

public:
	inline static int32_t get_offset_of_parental_4() { return static_cast<int32_t>(offsetof(UIButtonOpenParental_t1943914589, ___parental_4)); }
	inline ParentalControl_t2040855302 * get_parental_4() const { return ___parental_4; }
	inline ParentalControl_t2040855302 ** get_address_of_parental_4() { return &___parental_4; }
	inline void set_parental_4(ParentalControl_t2040855302 * value)
	{
		___parental_4 = value;
		Il2CppCodeGenWriteBarrier(&___parental_4, value);
	}

	inline static int32_t get_offset_of_buttonType_5() { return static_cast<int32_t>(offsetof(UIButtonOpenParental_t1943914589, ___buttonType_5)); }
	inline int32_t get_buttonType_5() const { return ___buttonType_5; }
	inline int32_t* get_address_of_buttonType_5() { return &___buttonType_5; }
	inline void set_buttonType_5(int32_t value)
	{
		___buttonType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
