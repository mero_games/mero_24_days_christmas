﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<ObstacleBase>
struct List_1_t4029556144;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObstacleManager
struct  ObstacleManager_t3466292636  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ObstacleManager::positionX
	float ___positionX_2;
	// System.Single ObstacleManager::spawnTime
	float ___spawnTime_3;
	// System.Collections.Generic.List`1<ObstacleBase> ObstacleManager::obstacles
	List_1_t4029556144 * ___obstacles_4;
	// System.Int32 ObstacleManager::currentIndex
	int32_t ___currentIndex_5;
	// System.Single ObstacleManager::lastTimeSpawned
	float ___lastTimeSpawned_6;

public:
	inline static int32_t get_offset_of_positionX_2() { return static_cast<int32_t>(offsetof(ObstacleManager_t3466292636, ___positionX_2)); }
	inline float get_positionX_2() const { return ___positionX_2; }
	inline float* get_address_of_positionX_2() { return &___positionX_2; }
	inline void set_positionX_2(float value)
	{
		___positionX_2 = value;
	}

	inline static int32_t get_offset_of_spawnTime_3() { return static_cast<int32_t>(offsetof(ObstacleManager_t3466292636, ___spawnTime_3)); }
	inline float get_spawnTime_3() const { return ___spawnTime_3; }
	inline float* get_address_of_spawnTime_3() { return &___spawnTime_3; }
	inline void set_spawnTime_3(float value)
	{
		___spawnTime_3 = value;
	}

	inline static int32_t get_offset_of_obstacles_4() { return static_cast<int32_t>(offsetof(ObstacleManager_t3466292636, ___obstacles_4)); }
	inline List_1_t4029556144 * get_obstacles_4() const { return ___obstacles_4; }
	inline List_1_t4029556144 ** get_address_of_obstacles_4() { return &___obstacles_4; }
	inline void set_obstacles_4(List_1_t4029556144 * value)
	{
		___obstacles_4 = value;
		Il2CppCodeGenWriteBarrier(&___obstacles_4, value);
	}

	inline static int32_t get_offset_of_currentIndex_5() { return static_cast<int32_t>(offsetof(ObstacleManager_t3466292636, ___currentIndex_5)); }
	inline int32_t get_currentIndex_5() const { return ___currentIndex_5; }
	inline int32_t* get_address_of_currentIndex_5() { return &___currentIndex_5; }
	inline void set_currentIndex_5(int32_t value)
	{
		___currentIndex_5 = value;
	}

	inline static int32_t get_offset_of_lastTimeSpawned_6() { return static_cast<int32_t>(offsetof(ObstacleManager_t3466292636, ___lastTimeSpawned_6)); }
	inline float get_lastTimeSpawned_6() const { return ___lastTimeSpawned_6; }
	inline float* get_address_of_lastTimeSpawned_6() { return &___lastTimeSpawned_6; }
	inline void set_lastTimeSpawned_6(float value)
	{
		___lastTimeSpawned_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
