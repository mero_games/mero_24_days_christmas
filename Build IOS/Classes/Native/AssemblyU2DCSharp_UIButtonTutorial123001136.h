﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_UIButton3377238306.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonTutorial
struct  UIButtonTutorial_t123001136  : public UIButton_t3377238306
{
public:
	// System.String[] UIButtonTutorial::tutorialTexts
	StringU5BU5D_t1642385972* ___tutorialTexts_4;
	// UnityEngine.UI.Text UIButtonTutorial::helpText
	Text_t356221433 * ___helpText_5;
	// System.Boolean UIButtonTutorial::canClick
	bool ___canClick_6;
	// System.Single UIButtonTutorial::minX
	float ___minX_7;
	// System.Single UIButtonTutorial::maxX
	float ___maxX_8;
	// System.Single UIButtonTutorial::minXtetris
	float ___minXtetris_9;
	// System.Single UIButtonTutorial::maxXtetris
	float ___maxXtetris_10;

public:
	inline static int32_t get_offset_of_tutorialTexts_4() { return static_cast<int32_t>(offsetof(UIButtonTutorial_t123001136, ___tutorialTexts_4)); }
	inline StringU5BU5D_t1642385972* get_tutorialTexts_4() const { return ___tutorialTexts_4; }
	inline StringU5BU5D_t1642385972** get_address_of_tutorialTexts_4() { return &___tutorialTexts_4; }
	inline void set_tutorialTexts_4(StringU5BU5D_t1642385972* value)
	{
		___tutorialTexts_4 = value;
		Il2CppCodeGenWriteBarrier(&___tutorialTexts_4, value);
	}

	inline static int32_t get_offset_of_helpText_5() { return static_cast<int32_t>(offsetof(UIButtonTutorial_t123001136, ___helpText_5)); }
	inline Text_t356221433 * get_helpText_5() const { return ___helpText_5; }
	inline Text_t356221433 ** get_address_of_helpText_5() { return &___helpText_5; }
	inline void set_helpText_5(Text_t356221433 * value)
	{
		___helpText_5 = value;
		Il2CppCodeGenWriteBarrier(&___helpText_5, value);
	}

	inline static int32_t get_offset_of_canClick_6() { return static_cast<int32_t>(offsetof(UIButtonTutorial_t123001136, ___canClick_6)); }
	inline bool get_canClick_6() const { return ___canClick_6; }
	inline bool* get_address_of_canClick_6() { return &___canClick_6; }
	inline void set_canClick_6(bool value)
	{
		___canClick_6 = value;
	}

	inline static int32_t get_offset_of_minX_7() { return static_cast<int32_t>(offsetof(UIButtonTutorial_t123001136, ___minX_7)); }
	inline float get_minX_7() const { return ___minX_7; }
	inline float* get_address_of_minX_7() { return &___minX_7; }
	inline void set_minX_7(float value)
	{
		___minX_7 = value;
	}

	inline static int32_t get_offset_of_maxX_8() { return static_cast<int32_t>(offsetof(UIButtonTutorial_t123001136, ___maxX_8)); }
	inline float get_maxX_8() const { return ___maxX_8; }
	inline float* get_address_of_maxX_8() { return &___maxX_8; }
	inline void set_maxX_8(float value)
	{
		___maxX_8 = value;
	}

	inline static int32_t get_offset_of_minXtetris_9() { return static_cast<int32_t>(offsetof(UIButtonTutorial_t123001136, ___minXtetris_9)); }
	inline float get_minXtetris_9() const { return ___minXtetris_9; }
	inline float* get_address_of_minXtetris_9() { return &___minXtetris_9; }
	inline void set_minXtetris_9(float value)
	{
		___minXtetris_9 = value;
	}

	inline static int32_t get_offset_of_maxXtetris_10() { return static_cast<int32_t>(offsetof(UIButtonTutorial_t123001136, ___maxXtetris_10)); }
	inline float get_maxXtetris_10() const { return ___maxXtetris_10; }
	inline float* get_address_of_maxXtetris_10() { return &___maxXtetris_10; }
	inline void set_maxXtetris_10(float value)
	{
		___maxXtetris_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
