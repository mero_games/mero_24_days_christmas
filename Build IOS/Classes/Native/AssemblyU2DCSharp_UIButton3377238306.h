﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButton
struct  UIButton_t3377238306  : public MonoBehaviour_t1158329972
{
public:
	// System.Action UIButton::OnClickDown
	Action_t3226471752 * ___OnClickDown_2;
	// System.Action UIButton::OnClickUp
	Action_t3226471752 * ___OnClickUp_3;

public:
	inline static int32_t get_offset_of_OnClickDown_2() { return static_cast<int32_t>(offsetof(UIButton_t3377238306, ___OnClickDown_2)); }
	inline Action_t3226471752 * get_OnClickDown_2() const { return ___OnClickDown_2; }
	inline Action_t3226471752 ** get_address_of_OnClickDown_2() { return &___OnClickDown_2; }
	inline void set_OnClickDown_2(Action_t3226471752 * value)
	{
		___OnClickDown_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnClickDown_2, value);
	}

	inline static int32_t get_offset_of_OnClickUp_3() { return static_cast<int32_t>(offsetof(UIButton_t3377238306, ___OnClickUp_3)); }
	inline Action_t3226471752 * get_OnClickUp_3() const { return ___OnClickUp_3; }
	inline Action_t3226471752 ** get_address_of_OnClickUp_3() { return &___OnClickUp_3; }
	inline void set_OnClickUp_3(Action_t3226471752 * value)
	{
		___OnClickUp_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnClickUp_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
