﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Note
struct  Note_t686167636  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Note::speed
	float ___speed_2;
	// System.Int32 Note::noteIndex
	int32_t ___noteIndex_3;
	// UnityEngine.Vector2 Note::startPos
	Vector2_t2243707579  ___startPos_4;
	// System.Single Note::amplitudeY
	float ___amplitudeY_5;
	// System.Single Note::omegaY
	float ___omegaY_6;
	// System.Single Note::index
	float ___index_7;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Note_t686167636, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_noteIndex_3() { return static_cast<int32_t>(offsetof(Note_t686167636, ___noteIndex_3)); }
	inline int32_t get_noteIndex_3() const { return ___noteIndex_3; }
	inline int32_t* get_address_of_noteIndex_3() { return &___noteIndex_3; }
	inline void set_noteIndex_3(int32_t value)
	{
		___noteIndex_3 = value;
	}

	inline static int32_t get_offset_of_startPos_4() { return static_cast<int32_t>(offsetof(Note_t686167636, ___startPos_4)); }
	inline Vector2_t2243707579  get_startPos_4() const { return ___startPos_4; }
	inline Vector2_t2243707579 * get_address_of_startPos_4() { return &___startPos_4; }
	inline void set_startPos_4(Vector2_t2243707579  value)
	{
		___startPos_4 = value;
	}

	inline static int32_t get_offset_of_amplitudeY_5() { return static_cast<int32_t>(offsetof(Note_t686167636, ___amplitudeY_5)); }
	inline float get_amplitudeY_5() const { return ___amplitudeY_5; }
	inline float* get_address_of_amplitudeY_5() { return &___amplitudeY_5; }
	inline void set_amplitudeY_5(float value)
	{
		___amplitudeY_5 = value;
	}

	inline static int32_t get_offset_of_omegaY_6() { return static_cast<int32_t>(offsetof(Note_t686167636, ___omegaY_6)); }
	inline float get_omegaY_6() const { return ___omegaY_6; }
	inline float* get_address_of_omegaY_6() { return &___omegaY_6; }
	inline void set_omegaY_6(float value)
	{
		___omegaY_6 = value;
	}

	inline static int32_t get_offset_of_index_7() { return static_cast<int32_t>(offsetof(Note_t686167636, ___index_7)); }
	inline float get_index_7() const { return ___index_7; }
	inline float* get_address_of_index_7() { return &___index_7; }
	inline void set_index_7(float value)
	{
		___index_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
