﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen3578120407.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// Santa
struct Santa_t3804168179;
// GiftManager
struct GiftManager_t2451335245;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Game24Manager
struct  Game24Manager_t283638021  : public SingletonBase_1_t3578120407
{
public:
	// Santa Game24Manager::santa
	Santa_t3804168179 * ___santa_4;
	// GiftManager Game24Manager::giftManager
	GiftManager_t2451335245 * ___giftManager_5;
	// UnityEngine.Vector2 Game24Manager::backgroundSpeed
	Vector2_t2243707579  ___backgroundSpeed_6;
	// UnityEngine.Vector2 Game24Manager::obstacleSpeed
	Vector2_t2243707579  ___obstacleSpeed_7;
	// System.Boolean Game24Manager::isPlaying
	bool ___isPlaying_8;

public:
	inline static int32_t get_offset_of_santa_4() { return static_cast<int32_t>(offsetof(Game24Manager_t283638021, ___santa_4)); }
	inline Santa_t3804168179 * get_santa_4() const { return ___santa_4; }
	inline Santa_t3804168179 ** get_address_of_santa_4() { return &___santa_4; }
	inline void set_santa_4(Santa_t3804168179 * value)
	{
		___santa_4 = value;
		Il2CppCodeGenWriteBarrier(&___santa_4, value);
	}

	inline static int32_t get_offset_of_giftManager_5() { return static_cast<int32_t>(offsetof(Game24Manager_t283638021, ___giftManager_5)); }
	inline GiftManager_t2451335245 * get_giftManager_5() const { return ___giftManager_5; }
	inline GiftManager_t2451335245 ** get_address_of_giftManager_5() { return &___giftManager_5; }
	inline void set_giftManager_5(GiftManager_t2451335245 * value)
	{
		___giftManager_5 = value;
		Il2CppCodeGenWriteBarrier(&___giftManager_5, value);
	}

	inline static int32_t get_offset_of_backgroundSpeed_6() { return static_cast<int32_t>(offsetof(Game24Manager_t283638021, ___backgroundSpeed_6)); }
	inline Vector2_t2243707579  get_backgroundSpeed_6() const { return ___backgroundSpeed_6; }
	inline Vector2_t2243707579 * get_address_of_backgroundSpeed_6() { return &___backgroundSpeed_6; }
	inline void set_backgroundSpeed_6(Vector2_t2243707579  value)
	{
		___backgroundSpeed_6 = value;
	}

	inline static int32_t get_offset_of_obstacleSpeed_7() { return static_cast<int32_t>(offsetof(Game24Manager_t283638021, ___obstacleSpeed_7)); }
	inline Vector2_t2243707579  get_obstacleSpeed_7() const { return ___obstacleSpeed_7; }
	inline Vector2_t2243707579 * get_address_of_obstacleSpeed_7() { return &___obstacleSpeed_7; }
	inline void set_obstacleSpeed_7(Vector2_t2243707579  value)
	{
		___obstacleSpeed_7 = value;
	}

	inline static int32_t get_offset_of_isPlaying_8() { return static_cast<int32_t>(offsetof(Game24Manager_t283638021, ___isPlaying_8)); }
	inline bool get_isPlaying_8() const { return ___isPlaying_8; }
	inline bool* get_address_of_isPlaying_8() { return &___isPlaying_8; }
	inline void set_isPlaying_8(bool value)
	{
		___isPlaying_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
