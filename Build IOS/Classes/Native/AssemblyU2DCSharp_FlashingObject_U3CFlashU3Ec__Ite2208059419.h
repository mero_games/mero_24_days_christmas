﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Func`1<System.Boolean>
struct Func_1_t1485000104;
// FlashingObject
struct FlashingObject_t2247968655;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlashingObject/<Flash>c__Iterator0
struct  U3CFlashU3Ec__Iterator0_t2208059419  : public Il2CppObject
{
public:
	// System.Single FlashingObject/<Flash>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_0;
	// System.Int32 FlashingObject/<Flash>c__Iterator0::<index>__0
	int32_t ___U3CindexU3E__0_1;
	// System.Single FlashingObject/<Flash>c__Iterator0::time
	float ___time_2;
	// System.Single FlashingObject/<Flash>c__Iterator0::intervalTime
	float ___intervalTime_3;
	// System.Func`1<System.Boolean> FlashingObject/<Flash>c__Iterator0::callbackFunction
	Func_1_t1485000104 * ___callbackFunction_4;
	// FlashingObject FlashingObject/<Flash>c__Iterator0::$this
	FlashingObject_t2247968655 * ___U24this_5;
	// System.Object FlashingObject/<Flash>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean FlashingObject/<Flash>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 FlashingObject/<Flash>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2208059419, ___U3CelapsedTimeU3E__0_0)); }
	inline float get_U3CelapsedTimeU3E__0_0() const { return ___U3CelapsedTimeU3E__0_0; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_0() { return &___U3CelapsedTimeU3E__0_0; }
	inline void set_U3CelapsedTimeU3E__0_0(float value)
	{
		___U3CelapsedTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CindexU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2208059419, ___U3CindexU3E__0_1)); }
	inline int32_t get_U3CindexU3E__0_1() const { return ___U3CindexU3E__0_1; }
	inline int32_t* get_address_of_U3CindexU3E__0_1() { return &___U3CindexU3E__0_1; }
	inline void set_U3CindexU3E__0_1(int32_t value)
	{
		___U3CindexU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2208059419, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_intervalTime_3() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2208059419, ___intervalTime_3)); }
	inline float get_intervalTime_3() const { return ___intervalTime_3; }
	inline float* get_address_of_intervalTime_3() { return &___intervalTime_3; }
	inline void set_intervalTime_3(float value)
	{
		___intervalTime_3 = value;
	}

	inline static int32_t get_offset_of_callbackFunction_4() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2208059419, ___callbackFunction_4)); }
	inline Func_1_t1485000104 * get_callbackFunction_4() const { return ___callbackFunction_4; }
	inline Func_1_t1485000104 ** get_address_of_callbackFunction_4() { return &___callbackFunction_4; }
	inline void set_callbackFunction_4(Func_1_t1485000104 * value)
	{
		___callbackFunction_4 = value;
		Il2CppCodeGenWriteBarrier(&___callbackFunction_4, value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2208059419, ___U24this_5)); }
	inline FlashingObject_t2247968655 * get_U24this_5() const { return ___U24this_5; }
	inline FlashingObject_t2247968655 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(FlashingObject_t2247968655 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2208059419, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2208059419, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CFlashU3Ec__Iterator0_t2208059419, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
