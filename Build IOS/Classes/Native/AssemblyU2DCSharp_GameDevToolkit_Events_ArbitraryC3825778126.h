﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>
struct UnityAction_1_t3123118898;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Events.ArbitraryCleaner
struct  ArbitraryCleaner_t3825778126  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Events.UnityAction`1<UnityEngine.GameObject> GameDevToolkit.Events.ArbitraryCleaner::Done
	UnityAction_1_t3123118898 * ___Done_2;

public:
	inline static int32_t get_offset_of_Done_2() { return static_cast<int32_t>(offsetof(ArbitraryCleaner_t3825778126, ___Done_2)); }
	inline UnityAction_1_t3123118898 * get_Done_2() const { return ___Done_2; }
	inline UnityAction_1_t3123118898 ** get_address_of_Done_2() { return &___Done_2; }
	inline void set_Done_2(UnityAction_1_t3123118898 * value)
	{
		___Done_2 = value;
		Il2CppCodeGenWriteBarrier(&___Done_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
