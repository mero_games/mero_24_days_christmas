﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare/SocialSharingStruct
struct  SocialSharingStruct_t1091601589 
{
public:
	// System.String NativeShare/SocialSharingStruct::text
	String_t* ___text_0;
	// System.String NativeShare/SocialSharingStruct::subject
	String_t* ___subject_1;
	// System.String NativeShare/SocialSharingStruct::filePaths
	String_t* ___filePaths_2;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(SocialSharingStruct_t1091601589, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier(&___text_0, value);
	}

	inline static int32_t get_offset_of_subject_1() { return static_cast<int32_t>(offsetof(SocialSharingStruct_t1091601589, ___subject_1)); }
	inline String_t* get_subject_1() const { return ___subject_1; }
	inline String_t** get_address_of_subject_1() { return &___subject_1; }
	inline void set_subject_1(String_t* value)
	{
		___subject_1 = value;
		Il2CppCodeGenWriteBarrier(&___subject_1, value);
	}

	inline static int32_t get_offset_of_filePaths_2() { return static_cast<int32_t>(offsetof(SocialSharingStruct_t1091601589, ___filePaths_2)); }
	inline String_t* get_filePaths_2() const { return ___filePaths_2; }
	inline String_t** get_address_of_filePaths_2() { return &___filePaths_2; }
	inline void set_filePaths_2(String_t* value)
	{
		___filePaths_2 = value;
		Il2CppCodeGenWriteBarrier(&___filePaths_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NativeShare/SocialSharingStruct
struct SocialSharingStruct_t1091601589_marshaled_pinvoke
{
	char* ___text_0;
	char* ___subject_1;
	char* ___filePaths_2;
};
// Native definition for COM marshalling of NativeShare/SocialSharingStruct
struct SocialSharingStruct_t1091601589_marshaled_com
{
	Il2CppChar* ___text_0;
	Il2CppChar* ___subject_1;
	Il2CppChar* ___filePaths_2;
};
