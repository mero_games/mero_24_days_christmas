﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// MemoryGamePiece[]
struct MemoryGamePieceU5BU5D_t1723749366;
// MemoryGamePiece
struct MemoryGamePiece_t2964149615;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// MemoryGame/WinDelegate
struct WinDelegate_t3170222997;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MemoryGame
struct  MemoryGame_t4018179393  : public MonoBehaviour_t1158329972
{
public:
	// MemoryGamePiece[] MemoryGame::pieces
	MemoryGamePieceU5BU5D_t1723749366* ___pieces_2;
	// MemoryGamePiece MemoryGame::clicked1
	MemoryGamePiece_t2964149615 * ___clicked1_3;
	// MemoryGamePiece MemoryGame::clicked2
	MemoryGamePiece_t2964149615 * ___clicked2_4;
	// System.Boolean MemoryGame::canClick
	bool ___canClick_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MemoryGame::cantClickAnyMore
	List_1_t1125654279 * ___cantClickAnyMore_6;
	// System.Int32 MemoryGame::matchCount
	int32_t ___matchCount_7;
	// System.Int32 MemoryGame::LevelNumber
	int32_t ___LevelNumber_8;
	// MemoryGame/WinDelegate MemoryGame::DoneMemory
	WinDelegate_t3170222997 * ___DoneMemory_9;

public:
	inline static int32_t get_offset_of_pieces_2() { return static_cast<int32_t>(offsetof(MemoryGame_t4018179393, ___pieces_2)); }
	inline MemoryGamePieceU5BU5D_t1723749366* get_pieces_2() const { return ___pieces_2; }
	inline MemoryGamePieceU5BU5D_t1723749366** get_address_of_pieces_2() { return &___pieces_2; }
	inline void set_pieces_2(MemoryGamePieceU5BU5D_t1723749366* value)
	{
		___pieces_2 = value;
		Il2CppCodeGenWriteBarrier(&___pieces_2, value);
	}

	inline static int32_t get_offset_of_clicked1_3() { return static_cast<int32_t>(offsetof(MemoryGame_t4018179393, ___clicked1_3)); }
	inline MemoryGamePiece_t2964149615 * get_clicked1_3() const { return ___clicked1_3; }
	inline MemoryGamePiece_t2964149615 ** get_address_of_clicked1_3() { return &___clicked1_3; }
	inline void set_clicked1_3(MemoryGamePiece_t2964149615 * value)
	{
		___clicked1_3 = value;
		Il2CppCodeGenWriteBarrier(&___clicked1_3, value);
	}

	inline static int32_t get_offset_of_clicked2_4() { return static_cast<int32_t>(offsetof(MemoryGame_t4018179393, ___clicked2_4)); }
	inline MemoryGamePiece_t2964149615 * get_clicked2_4() const { return ___clicked2_4; }
	inline MemoryGamePiece_t2964149615 ** get_address_of_clicked2_4() { return &___clicked2_4; }
	inline void set_clicked2_4(MemoryGamePiece_t2964149615 * value)
	{
		___clicked2_4 = value;
		Il2CppCodeGenWriteBarrier(&___clicked2_4, value);
	}

	inline static int32_t get_offset_of_canClick_5() { return static_cast<int32_t>(offsetof(MemoryGame_t4018179393, ___canClick_5)); }
	inline bool get_canClick_5() const { return ___canClick_5; }
	inline bool* get_address_of_canClick_5() { return &___canClick_5; }
	inline void set_canClick_5(bool value)
	{
		___canClick_5 = value;
	}

	inline static int32_t get_offset_of_cantClickAnyMore_6() { return static_cast<int32_t>(offsetof(MemoryGame_t4018179393, ___cantClickAnyMore_6)); }
	inline List_1_t1125654279 * get_cantClickAnyMore_6() const { return ___cantClickAnyMore_6; }
	inline List_1_t1125654279 ** get_address_of_cantClickAnyMore_6() { return &___cantClickAnyMore_6; }
	inline void set_cantClickAnyMore_6(List_1_t1125654279 * value)
	{
		___cantClickAnyMore_6 = value;
		Il2CppCodeGenWriteBarrier(&___cantClickAnyMore_6, value);
	}

	inline static int32_t get_offset_of_matchCount_7() { return static_cast<int32_t>(offsetof(MemoryGame_t4018179393, ___matchCount_7)); }
	inline int32_t get_matchCount_7() const { return ___matchCount_7; }
	inline int32_t* get_address_of_matchCount_7() { return &___matchCount_7; }
	inline void set_matchCount_7(int32_t value)
	{
		___matchCount_7 = value;
	}

	inline static int32_t get_offset_of_LevelNumber_8() { return static_cast<int32_t>(offsetof(MemoryGame_t4018179393, ___LevelNumber_8)); }
	inline int32_t get_LevelNumber_8() const { return ___LevelNumber_8; }
	inline int32_t* get_address_of_LevelNumber_8() { return &___LevelNumber_8; }
	inline void set_LevelNumber_8(int32_t value)
	{
		___LevelNumber_8 = value;
	}

	inline static int32_t get_offset_of_DoneMemory_9() { return static_cast<int32_t>(offsetof(MemoryGame_t4018179393, ___DoneMemory_9)); }
	inline WinDelegate_t3170222997 * get_DoneMemory_9() const { return ___DoneMemory_9; }
	inline WinDelegate_t3170222997 ** get_address_of_DoneMemory_9() { return &___DoneMemory_9; }
	inline void set_DoneMemory_9(WinDelegate_t3170222997 * value)
	{
		___DoneMemory_9 = value;
		Il2CppCodeGenWriteBarrier(&___DoneMemory_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
