﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t13116344;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// HatEnabler
struct HatEnabler_t1910487184;
// UnityEngine.PlatformEffector2D[]
struct PlatformEffector2DU5BU5D_t815519938;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RingTossController
struct  RingTossController_t956255555  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean RingTossController::canToss
	bool ___canToss_2;
	// System.Single RingTossController::force
	float ___force_3;
	// UnityEngine.SpriteRenderer RingTossController::topRing
	SpriteRenderer_t1209076198 * ___topRing_4;
	// UnityEngine.CircleCollider2D RingTossController::col1
	CircleCollider2D_t13116344 * ___col1_5;
	// UnityEngine.CircleCollider2D RingTossController::col2
	CircleCollider2D_t13116344 * ___col2_6;
	// UnityEngine.Rigidbody2D RingTossController::rb2d
	Rigidbody2D_t502193897 * ___rb2d_7;
	// UnityEngine.Vector3 RingTossController::startPos
	Vector3_t2243707580  ___startPos_8;
	// UnityEngine.Vector3 RingTossController::endPos
	Vector3_t2243707580  ___endPos_9;
	// HatEnabler RingTossController::hat
	HatEnabler_t1910487184 * ___hat_10;
	// System.Boolean RingTossController::hasCollided
	bool ___hasCollided_11;
	// System.Boolean RingTossController::toSleep
	bool ___toSleep_12;
	// UnityEngine.PlatformEffector2D[] RingTossController::effectors
	PlatformEffector2DU5BU5D_t815519938* ___effectors_13;

public:
	inline static int32_t get_offset_of_canToss_2() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___canToss_2)); }
	inline bool get_canToss_2() const { return ___canToss_2; }
	inline bool* get_address_of_canToss_2() { return &___canToss_2; }
	inline void set_canToss_2(bool value)
	{
		___canToss_2 = value;
	}

	inline static int32_t get_offset_of_force_3() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___force_3)); }
	inline float get_force_3() const { return ___force_3; }
	inline float* get_address_of_force_3() { return &___force_3; }
	inline void set_force_3(float value)
	{
		___force_3 = value;
	}

	inline static int32_t get_offset_of_topRing_4() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___topRing_4)); }
	inline SpriteRenderer_t1209076198 * get_topRing_4() const { return ___topRing_4; }
	inline SpriteRenderer_t1209076198 ** get_address_of_topRing_4() { return &___topRing_4; }
	inline void set_topRing_4(SpriteRenderer_t1209076198 * value)
	{
		___topRing_4 = value;
		Il2CppCodeGenWriteBarrier(&___topRing_4, value);
	}

	inline static int32_t get_offset_of_col1_5() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___col1_5)); }
	inline CircleCollider2D_t13116344 * get_col1_5() const { return ___col1_5; }
	inline CircleCollider2D_t13116344 ** get_address_of_col1_5() { return &___col1_5; }
	inline void set_col1_5(CircleCollider2D_t13116344 * value)
	{
		___col1_5 = value;
		Il2CppCodeGenWriteBarrier(&___col1_5, value);
	}

	inline static int32_t get_offset_of_col2_6() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___col2_6)); }
	inline CircleCollider2D_t13116344 * get_col2_6() const { return ___col2_6; }
	inline CircleCollider2D_t13116344 ** get_address_of_col2_6() { return &___col2_6; }
	inline void set_col2_6(CircleCollider2D_t13116344 * value)
	{
		___col2_6 = value;
		Il2CppCodeGenWriteBarrier(&___col2_6, value);
	}

	inline static int32_t get_offset_of_rb2d_7() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___rb2d_7)); }
	inline Rigidbody2D_t502193897 * get_rb2d_7() const { return ___rb2d_7; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb2d_7() { return &___rb2d_7; }
	inline void set_rb2d_7(Rigidbody2D_t502193897 * value)
	{
		___rb2d_7 = value;
		Il2CppCodeGenWriteBarrier(&___rb2d_7, value);
	}

	inline static int32_t get_offset_of_startPos_8() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___startPos_8)); }
	inline Vector3_t2243707580  get_startPos_8() const { return ___startPos_8; }
	inline Vector3_t2243707580 * get_address_of_startPos_8() { return &___startPos_8; }
	inline void set_startPos_8(Vector3_t2243707580  value)
	{
		___startPos_8 = value;
	}

	inline static int32_t get_offset_of_endPos_9() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___endPos_9)); }
	inline Vector3_t2243707580  get_endPos_9() const { return ___endPos_9; }
	inline Vector3_t2243707580 * get_address_of_endPos_9() { return &___endPos_9; }
	inline void set_endPos_9(Vector3_t2243707580  value)
	{
		___endPos_9 = value;
	}

	inline static int32_t get_offset_of_hat_10() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___hat_10)); }
	inline HatEnabler_t1910487184 * get_hat_10() const { return ___hat_10; }
	inline HatEnabler_t1910487184 ** get_address_of_hat_10() { return &___hat_10; }
	inline void set_hat_10(HatEnabler_t1910487184 * value)
	{
		___hat_10 = value;
		Il2CppCodeGenWriteBarrier(&___hat_10, value);
	}

	inline static int32_t get_offset_of_hasCollided_11() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___hasCollided_11)); }
	inline bool get_hasCollided_11() const { return ___hasCollided_11; }
	inline bool* get_address_of_hasCollided_11() { return &___hasCollided_11; }
	inline void set_hasCollided_11(bool value)
	{
		___hasCollided_11 = value;
	}

	inline static int32_t get_offset_of_toSleep_12() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___toSleep_12)); }
	inline bool get_toSleep_12() const { return ___toSleep_12; }
	inline bool* get_address_of_toSleep_12() { return &___toSleep_12; }
	inline void set_toSleep_12(bool value)
	{
		___toSleep_12 = value;
	}

	inline static int32_t get_offset_of_effectors_13() { return static_cast<int32_t>(offsetof(RingTossController_t956255555, ___effectors_13)); }
	inline PlatformEffector2DU5BU5D_t815519938* get_effectors_13() const { return ___effectors_13; }
	inline PlatformEffector2DU5BU5D_t815519938** get_address_of_effectors_13() { return &___effectors_13; }
	inline void set_effectors_13(PlatformEffector2DU5BU5D_t815519938* value)
	{
		___effectors_13 = value;
		Il2CppCodeGenWriteBarrier(&___effectors_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
