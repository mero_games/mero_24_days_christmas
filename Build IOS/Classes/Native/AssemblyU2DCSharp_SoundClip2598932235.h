﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.AudioClip
struct AudioClip_t1932558630;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundClip
struct  SoundClip_t2598932235  : public Il2CppObject
{
public:
	// UnityEngine.AudioClip SoundClip::sound
	AudioClip_t1932558630 * ___sound_0;
	// System.Single SoundClip::volume
	float ___volume_1;

public:
	inline static int32_t get_offset_of_sound_0() { return static_cast<int32_t>(offsetof(SoundClip_t2598932235, ___sound_0)); }
	inline AudioClip_t1932558630 * get_sound_0() const { return ___sound_0; }
	inline AudioClip_t1932558630 ** get_address_of_sound_0() { return &___sound_0; }
	inline void set_sound_0(AudioClip_t1932558630 * value)
	{
		___sound_0 = value;
		Il2CppCodeGenWriteBarrier(&___sound_0, value);
	}

	inline static int32_t get_offset_of_volume_1() { return static_cast<int32_t>(offsetof(SoundClip_t2598932235, ___volume_1)); }
	inline float get_volume_1() const { return ___volume_1; }
	inline float* get_address_of_volume_1() { return &___volume_1; }
	inline void set_volume_1(float value)
	{
		___volume_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
