﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<UnityEngine.GameObject>
struct TweenCallbackDelegate_1_t2074729623;
// GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<System.String>
struct TweenCallbackDelegate_1_t2347416709;
// GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<GameDevToolkit.Events.ArbitraryEvent>
struct TweenCallbackDelegate_1_t1084330812;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Events.ArbitraryEvent
struct  ArbitraryEvent_t766134336  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<UnityEngine.GameObject> GameDevToolkit.Events.ArbitraryEvent::EventGameObject
	TweenCallbackDelegate_1_t2074729623 * ___EventGameObject_2;
	// GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<System.String> GameDevToolkit.Events.ArbitraryEvent::EventString
	TweenCallbackDelegate_1_t2347416709 * ___EventString_3;
	// GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<GameDevToolkit.Events.ArbitraryEvent> GameDevToolkit.Events.ArbitraryEvent::EventTarget
	TweenCallbackDelegate_1_t1084330812 * ___EventTarget_4;

public:
	inline static int32_t get_offset_of_EventGameObject_2() { return static_cast<int32_t>(offsetof(ArbitraryEvent_t766134336, ___EventGameObject_2)); }
	inline TweenCallbackDelegate_1_t2074729623 * get_EventGameObject_2() const { return ___EventGameObject_2; }
	inline TweenCallbackDelegate_1_t2074729623 ** get_address_of_EventGameObject_2() { return &___EventGameObject_2; }
	inline void set_EventGameObject_2(TweenCallbackDelegate_1_t2074729623 * value)
	{
		___EventGameObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___EventGameObject_2, value);
	}

	inline static int32_t get_offset_of_EventString_3() { return static_cast<int32_t>(offsetof(ArbitraryEvent_t766134336, ___EventString_3)); }
	inline TweenCallbackDelegate_1_t2347416709 * get_EventString_3() const { return ___EventString_3; }
	inline TweenCallbackDelegate_1_t2347416709 ** get_address_of_EventString_3() { return &___EventString_3; }
	inline void set_EventString_3(TweenCallbackDelegate_1_t2347416709 * value)
	{
		___EventString_3 = value;
		Il2CppCodeGenWriteBarrier(&___EventString_3, value);
	}

	inline static int32_t get_offset_of_EventTarget_4() { return static_cast<int32_t>(offsetof(ArbitraryEvent_t766134336, ___EventTarget_4)); }
	inline TweenCallbackDelegate_1_t1084330812 * get_EventTarget_4() const { return ___EventTarget_4; }
	inline TweenCallbackDelegate_1_t1084330812 ** get_address_of_EventTarget_4() { return &___EventTarget_4; }
	inline void set_EventTarget_4(TweenCallbackDelegate_1_t1084330812 * value)
	{
		___EventTarget_4 = value;
		Il2CppCodeGenWriteBarrier(&___EventTarget_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
