﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_DDManagerBase_1_gen3108376481.h"
#include "mscorlib_System_Void1841601450.h"
#include "AssemblyU2DCSharp_SingletonBase_1_gen1688964385.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2014832765.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "DOTween_DG_Tweening_Color2232726623.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1565154527.h"
#include "mscorlib_System_Double4078015681.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3853983590.h"
#include "mscorlib_System_Int322071877448.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2691184179.h"
#include "mscorlib_System_Int64909078037.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen176588141.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074.h"
#include "mscorlib_System_Single2076509932.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3931788163.h"
#include "mscorlib_System_UInt322149682021.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen396335760.h"
#include "mscorlib_System_UInt642909196914.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1517212764.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1168894472.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813722.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813723.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1890955609.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1441277371.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3730106434.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2567307023.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen52710985.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3807911007.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen272458604.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1393335608.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1045017316.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936566.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936567.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3925803634.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "mscorlib_System_Type1303803226.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "DOTween_DG_Tweening_Core_ABSSequentiable2284140720.h"
#include "DOTween_DG_Tweening_TweenType169444141.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751.h"
#include "mscorlib_System_String2029220233.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2833266637.h"
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions2213017305.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode2539919096.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice2468589887.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen456598886.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3659029185.h"
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions2508431845.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3160108754.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2067571757.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3199111798.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2106574801.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1705766462.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g613229465.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2082658550.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g990121553.h"
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions2885323933.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2279406887.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1186869890.h"
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions1421548266.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1648829745.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g556292748.h"
#include "DOTween_DG_Tweening_Plugins_Options_UintOptions2267095136.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2937657178.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1845120181.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2998039394.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1905502397.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1672798003.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g580261006.h"
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptio466049668.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3065187631.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1972650634.h"
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions3393635162.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3250868854.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2158331857.h"
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions293385261.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1622518059.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g529981062.h"
#include "DOTween_DG_Tweening_Plugins_Options_PathOptions2659884781.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1635203449.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g542666452.h"
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOp2672570171.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1108663466.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_ge16126469.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3261425374.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2168888377.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen3418705418.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen4036277265.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Common_TwoDArray_3326477674.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Common_TwoDArray_1846454018.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "mscorlib_System_Exception1927440687.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_ArbitraryE3007645771.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_GenericMous702691374.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_MouseEvent2370363659.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_GenericMou2681505391.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Events_MouseEventP442073356.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_GridRepG2492059455.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_GridRepG3548015612.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_Randomiz1796594675.h"
#include "AssemblyU2DCSharp_GameDevToolkit_GameUtil_GridPoin3774252050.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3143373182.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "mscorlib_System_Action_1_gen1946475162.h"
#include "AssemblyU2DCSharp_GameTwentyoneManager_GiftObject2144675780.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen3256280720.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Action_1_gen1873676830.h"
#include "mscorlib_System_Action_1_gen2491248677.h"
#include "mscorlib_System_Action_1_gen4190924221.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Action_1_gen1299997296.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Action_1_gen3707550731.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "mscorlib_System_Action_1_gen1822191457.h"
#include "mscorlib_System_Action_1_gen676316900.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Action_1_gen4117953054.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Action_1_gen2085194534.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "mscorlib_System_Action_1_gen2858436182.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Action_1_gen3423077256.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Action_1_gen1006058200.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Action_1_gen2045506961.h"
#include "mscorlib_System_Action_1_gen2045506962.h"
#include "mscorlib_System_Action_1_gen2045506963.h"
#include "System_Core_System_Action_2_gen2525452034.h"
#include "System_Core_System_Action_2_gen2572051853.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"

// DDManagerBase`1<System.Object>
struct DDManagerBase_1_t3108376481;
// UnityEngine.Object
struct Object_t1021602117;
// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
struct DOGetter_1_t2014832765;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// DG.Tweening.Core.DOGetter`1<System.Double>
struct DOGetter_1_t1565154527;
// DG.Tweening.Core.DOGetter`1<System.Int32>
struct DOGetter_1_t3853983590;
// DG.Tweening.Core.DOGetter`1<System.Int64>
struct DOGetter_1_t2691184179;
// DG.Tweening.Core.DOGetter`1<System.Object>
struct DOGetter_1_t176588141;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t3858616074;
// DG.Tweening.Core.DOGetter`1<System.UInt32>
struct DOGetter_1_t3931788163;
// DG.Tweening.Core.DOGetter`1<System.UInt64>
struct DOGetter_1_t396335760;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_t3802498217;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
struct DOGetter_1_t1517212764;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
struct DOGetter_1_t1168894472;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t4025813721;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t4025813722;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
struct DOGetter_1_t4025813723;
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
struct DOSetter_1_t1890955609;
// DG.Tweening.Core.DOSetter`1<System.Double>
struct DOSetter_1_t1441277371;
// DG.Tweening.Core.DOSetter`1<System.Int32>
struct DOSetter_1_t3730106434;
// DG.Tweening.Core.DOSetter`1<System.Int64>
struct DOSetter_1_t2567307023;
// DG.Tweening.Core.DOSetter`1<System.Object>
struct DOSetter_1_t52710985;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t3734738918;
// DG.Tweening.Core.DOSetter`1<System.UInt32>
struct DOSetter_1_t3807911007;
// DG.Tweening.Core.DOSetter`1<System.UInt64>
struct DOSetter_1_t272458604;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_t3678621061;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
struct DOSetter_1_t1393335608;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
struct DOSetter_1_t1045017316;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t3901936565;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t3901936566;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
struct DOSetter_1_t3901936567;
// DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t3925803634;
// DG.Tweening.Tweener
struct Tweener_t760404022;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// DG.Tweening.Tween
struct Tween_t278478013;
// DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t456598886;
// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t3160108754;
// DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t3199111798;
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1705766462;
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t2082658550;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t2279406887;
// DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct TweenerCore_3_t1648829745;
// DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t2937657178;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t2998039394;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct TweenerCore_3_t1672798003;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct TweenerCore_3_t3065187631;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t3250868854;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t1622518059;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t1635203449;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t1108663466;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t3261425374;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t2833266637;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t3659029185;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2067571757;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2106574801;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t613229465;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct ABSTweenPlugin_3_t990121553;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct ABSTweenPlugin_3_t1186869890;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct ABSTweenPlugin_3_t556292748;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t1845120181;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t1905502397;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct ABSTweenPlugin_3_t580261006;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct ABSTweenPlugin_3_t1972650634;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t2158331857;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_t529981062;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct ABSTweenPlugin_3_t542666452;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t16126469;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t2168888377;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3418705418;
// DG.Tweening.TweenCallback`1<System.Object>
struct TweenCallback_1_t4036277265;
// GameDevToolkit.Common.TwoDArray`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3326477674;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// GameDevToolkit.Common.TwoDArray`1<System.Object>
struct TwoDArray_1_t846454018;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t3527622107;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<System.Object>
struct TweenCallbackDelegate_1_t3007645771;
// GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<System.Object>
struct MouseDelegate_t702691374;
// GameDevToolkit.Events.GenericMouseEvent`1<System.Object>
struct GenericMouseEvent_1_t2681505391;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.Delegate
struct Delegate_t3022476291;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// GameDevToolkit.Events.MouseEventProccessor
struct MouseEventProccessor_t442073356;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// GameDevToolkit.GameUtil.GridRepGenereic`2/<GetEnumerator>c__Iterator0<System.Object,System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t2492059455;
// GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>
struct GridRepGenereic_2_t3548015612;
// UnityEngine.Transform
struct Transform_t3275118058;
// GameDevToolkit.GameUtil.Randomizer
struct Randomizer_t1796594675;
// GameDevToolkit.GameUtil.GridPoint
struct GridPoint_t3774252050;
// GameDevToolkit.GameUtil.GridPoint[]
struct GridPointU5BU5D_t1678401383;
// System.Collections.Generic.List`1<GameDevToolkit.GameUtil.GridPoint>
struct List_1_t3143373182;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Exception
struct Exception_t1927440687;
// SingletonBase`1<System.Object>
struct SingletonBase_1_t1688964385;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Action`1<GameTwentyoneManager/GiftObject>
struct Action_1_t1946475162;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<System.Char>
struct Action_1_t3256280720;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<System.Reflection.CustomAttributeNamedArgument>
struct Action_1_t4190924221;
// System.Action`1<System.Reflection.CustomAttributeTypedArgument>
struct Action_1_t1299997296;
// System.Action`1<UnityEngine.AnimatorClipInfo>
struct Action_1_t3707550731;
// System.Action`1<UnityEngine.Color>
struct Action_1_t1822191457;
// System.Action`1<UnityEngine.Color32>
struct Action_1_t676316900;
// System.Action`1<UnityEngine.EventSystems.RaycastResult>
struct Action_1_t4117953054;
// System.Action`1<UnityEngine.KeyCode>
struct Action_1_t2085194534;
// System.Action`1<UnityEngine.UICharInfo>
struct Action_1_t2858436182;
// System.Action`1<UnityEngine.UILineInfo>
struct Action_1_t3423077256;
// System.Action`1<UnityEngine.UIVertex>
struct Action_1_t1006058200;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2045506961;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t2045506962;
// System.Action`1<UnityEngine.Vector4>
struct Action_1_t2045506963;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2525452034;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t2445488949;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t4145164493;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t1254237568;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2471096271;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t279959794;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t4170771815;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1864648666;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t DDManagerBase_1_Awake_m1282781541_MetadataUsageId;
extern Il2CppClass* Color2_t232726623_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3915298133_MetadataUsageId;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1311537432_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3860168801_MetadataUsageId;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m736671164_MetadataUsageId;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3656965161_MetadataUsageId;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2436040648_MetadataUsageId;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2966874951_MetadataUsageId;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2663926534_MetadataUsageId;
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1885597315_MetadataUsageId;
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3580342655_MetadataUsageId;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2720752834_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m681046145_MetadataUsageId;
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2980016964_MetadataUsageId;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1803465319_MetadataUsageId;
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3909355140_MetadataUsageId;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3682676813_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1180323095_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m912926810_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3426149681_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m2533780907_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3330381752_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2587998509_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1918761711_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2582633306_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m1754088405_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1198377687_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m188691034_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m749792945_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m3971101479_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2269915508_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m4039624089_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1849842838_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2067864171_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3135202618_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m4074697550_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m949598801_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3033691518_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m418004407_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m1802430330_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2278209809_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m2978185163_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3105054398_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2248740889_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m2776308754_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3657890433_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2434462778_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m231619286_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2524932243_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m4231943806_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m3640631583_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m863616202_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3268195009_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1012438200_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2654203693_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m1795781652_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1432615606_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2062234395_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3384733786_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m4171196319_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3688747850_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3434606145_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1407812639_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3143172042_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m140499649_MetadataUsageId;
extern const uint32_t TweenCallback_1_BeginInvoke_m711035500_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2437084550_MetadataUsageId;
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029318;
extern const uint32_t TwoDArray_1_get_Item_m506725763_MetadataUsageId;
extern Il2CppClass* MouseEventType_t2370363659_il2cpp_TypeInfo_var;
extern const uint32_t MouseDelegate_BeginInvoke_m2803613529_MetadataUsageId;
extern const Il2CppType* GameObject_t1756533147_0_0_0_var;
extern Il2CppClass* MouseEventProccessor_t442073356_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCollider2D_t646061738_m4291101372_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2125706819;
extern Il2CppCodeGenString* _stringLiteral3540188832;
extern const uint32_t GenericMouseEvent_1_Awake_m819933243_MetadataUsageId;
extern const uint32_t GenericMouseEvent_1_dispatchMouse_m1359806630_MetadataUsageId;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m705677362_MetadataUsageId;
extern const MethodInfo* Randomizer_CreateRandomizer_TisSemiRandomRule_t1463926147_m2503539335_MethodInfo_var;
extern const uint32_t GridRepGenereic_2_Awake_m921158666_MetadataUsageId;
extern Il2CppClass* GridPoint_t3774252050_il2cpp_TypeInfo_var;
extern const uint32_t GridRepGenereic_2_getRandomPoint_m3276798414_MetadataUsageId;
extern Il2CppClass* List_1_t3143373182_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3541242976_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2362510564_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4211291750_MethodInfo_var;
extern const uint32_t GridRepGenereic_2_getNeighbors_m2938468996_MetadataUsageId;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t GridRepGenereic_2_Exists_m3379732834_MetadataUsageId;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3486192891;
extern const uint32_t GridRepGenereic_2_hasAbove_m3548441245_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_hasBelow_m3087394133_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_hasToLeft_m2021908934_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_hasToRight_m1771079277_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getPositionAbove_m1340081224_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getPositionBelow_m3753575844_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getPositionToLeft_m1630567973_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getPositionToRight_m4017048604_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getPositionAbove_m2782846948_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getPositionBelow_m3728707952_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getPositionToLeft_m3820077597_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getPositionToRight_m4219751296_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getCell_m3595911306_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getCells_m4142893693_MetadataUsageId;
extern const uint32_t GridRepGenereic_2_getPositionOfCell_m4074657698_MetadataUsageId;
extern const uint32_t SingletonBase_1_get_Instance_m2217304019_MetadataUsageId;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t SingletonBase_1_Awake_m746335005_MetadataUsageId;
extern Il2CppClass* GiftObject_t2144675780_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m292367995_MetadataUsageId;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m226849422_MetadataUsageId;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1592224130_MetadataUsageId;
extern const uint32_t Action_1_BeginInvoke_m1720726178_MetadataUsageId;
extern Il2CppClass* CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2361014002_MetadataUsageId;
extern Il2CppClass* CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1386903007_MetadataUsageId;
extern Il2CppClass* AnimatorClipInfo_t3905751349_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2796970985_MetadataUsageId;
extern const uint32_t Action_1_BeginInvoke_m1002727263_MetadataUsageId;
extern Il2CppClass* Color32_t874517518_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m670751528_MetadataUsageId;
extern Il2CppClass* RaycastResult_t21186376_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m910318220_MetadataUsageId;
extern Il2CppClass* KeyCode_t2283395152_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2052621508_MetadataUsageId;
extern Il2CppClass* UICharInfo_t3056636800_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3589829392_MetadataUsageId;
extern Il2CppClass* UILineInfo_t3621277874_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1314092322_MetadataUsageId;
extern Il2CppClass* UIVertex_t1204258818_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3450966034_MetadataUsageId;
extern const uint32_t Action_1_BeginInvoke_m493747221_MetadataUsageId;
extern const uint32_t Action_1_BeginInvoke_m402802490_MetadataUsageId;
extern const uint32_t Action_1_BeginInvoke_m1860947955_MetadataUsageId;
extern const uint32_t Action_2_BeginInvoke_m3907381723_MetadataUsageId;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GameDevToolkit.GameUtil.GridPoint[]
struct GridPointU5BU5D_t1678401383  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GridPoint_t3774252050 * m_Items[1];

public:
	inline GridPoint_t3774252050 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GridPoint_t3774252050 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GridPoint_t3774252050 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GridPoint_t3774252050 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GridPoint_t3774252050 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GridPoint_t3774252050 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeNamedArgument_t94157543  m_Items[1];

public:
	inline CustomAttributeNamedArgument_t94157543  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t94157543 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeNamedArgument_t94157543  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeNamedArgument_t94157543  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t94157543 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeNamedArgument_t94157543  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1075686591  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CustomAttributeTypedArgument_t1498197914  m_Items[1];

public:
	inline CustomAttributeTypedArgument_t1498197914  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t1498197914 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeTypedArgument_t1498197914  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeTypedArgument_t1498197914  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t1498197914 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeTypedArgument_t1498197914  value)
	{
		m_Items[index] = value;
	}
};


// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C"  Color2_t232726623  DOGetter_1_Invoke_m1748194493_gshared (DOGetter_1_t2014832765 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
extern "C"  double DOGetter_1_Invoke_m4107339380_gshared (DOGetter_1_t1565154527 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
extern "C"  int32_t DOGetter_1_Invoke_m2109831897_gshared (DOGetter_1_t3853983590 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C"  int64_t DOGetter_1_Invoke_m111587642_gshared (DOGetter_1_t2691184179 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * DOGetter_1_Invoke_m882816534_gshared (DOGetter_1_t176588141 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
extern "C"  float DOGetter_1_Invoke_m3647275797_gshared (DOGetter_1_t3858616074 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
extern "C"  uint32_t DOGetter_1_Invoke_m370264108_gshared (DOGetter_1_t3931788163 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
extern "C"  uint64_t DOGetter_1_Invoke_m2202291235_gshared (DOGetter_1_t396335760 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C"  Color_t2020392075  DOGetter_1_Invoke_m222071538_gshared (DOGetter_1_t3802498217 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
extern "C"  Quaternion_t4030073918  DOGetter_1_Invoke_m1844179403_gshared (DOGetter_1_t1517212764 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C"  Rect_t3681755626  DOGetter_1_Invoke_m205270839_gshared (DOGetter_1_t1168894472 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C"  Vector2_t2243707579  DOGetter_1_Invoke_m3450181142_gshared (DOGetter_1_t4025813721 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
extern "C"  Vector3_t2243707580  DOGetter_1_Invoke_m3486946741_gshared (DOGetter_1_t4025813722 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
extern "C"  Vector4_t2243707581  DOGetter_1_Invoke_m3680105936_gshared (DOGetter_1_t4025813723 * __this, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3163525136_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1726747861_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m748616666_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2393688041_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1999305269_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m184768384_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m236045679_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4121538054_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3892954541_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m903072570_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1386366628_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2956451219_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4169715988_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m212800661_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3615052821_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, const MethodInfo* method);
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m872757678_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, const MethodInfo* method);
// System.Void GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<System.Object>::Invoke(T)
extern "C"  void TweenCallbackDelegate_1_Invoke_m686840820_gshared (TweenCallbackDelegate_1_t3007645771 * __this, Il2CppObject * ___argumeny0, const MethodInfo* method);
// System.Void GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<System.Object>::Invoke(T,GameDevToolkit.Events.MouseEventType)
extern "C"  void MouseDelegate_Invoke_m2233526212_gshared (MouseDelegate_t702691374 * __this, Il2CppObject * ___target0, int32_t ___type1, const MethodInfo* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// GameDevToolkit.GameUtil.Randomizer GameDevToolkit.GameUtil.Randomizer::CreateRandomizer<System.Object>(System.Int32,System.Int32)
extern "C"  Randomizer_t1796594675 * Randomizer_CreateRandomizer_TisIl2CppObject_m1955574599_gshared (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t3614634134* List_1_ToArray_m546658539_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Action`1<GameTwentyoneManager/GiftObject>::Invoke(T)
extern "C"  void Action_1_Invoke_m3862957638_gshared (Action_1_t1946475162 * __this, GiftObject_t2144675780  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Char>::Invoke(T)
extern "C"  void Action_1_Invoke_m3702739261_gshared (Action_1_t3256280720 * __this, Il2CppChar ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Int32>::Invoke(T)
extern "C"  void Action_1_Invoke_m2055228803_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m4180501989_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  void Action_1_Invoke_m3377187479_gshared (Action_1_t4190924221 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  void Action_1_Invoke_m3721769778_gshared (Action_1_t1299997296 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.AnimatorClipInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m648331504_gshared (Action_1_t3707550731 * __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.Color>::Invoke(T)
extern "C"  void Action_1_Invoke_m3422995740_gshared (Action_1_t1822191457 * __this, Color_t2020392075  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.Color32>::Invoke(T)
extern "C"  void Action_1_Invoke_m3852022121_gshared (Action_1_t676316900 * __this, Color32_t874517518  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  void Action_1_Invoke_m2664461947_gshared (Action_1_t4117953054 * __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.KeyCode>::Invoke(T)
extern "C"  void Action_1_Invoke_m4078720527_gshared (Action_1_t2085194534 * __this, int32_t ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m433096323_gshared (Action_1_t2858436182 * __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m1440002041_gshared (Action_1_t3423077256 * __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  void Action_1_Invoke_m2226099137_gshared (Action_1_t1006058200 * __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void Action_1_Invoke_m3472061792_gshared (Action_1_t2045506961 * __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void Action_1_Invoke_m212965499_gshared (Action_1_t2045506962 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void Action_1_Invoke_m3127338686_gshared (Action_1_t2045506963 * __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m352317182_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1501152969_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);

// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2330762974 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
#define DOGetter_1_Invoke_m1748194493(__this, method) ((  Color2_t232726623  (*) (DOGetter_1_t2014832765 *, const MethodInfo*))DOGetter_1_Invoke_m1748194493_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
#define DOGetter_1_Invoke_m4107339380(__this, method) ((  double (*) (DOGetter_1_t1565154527 *, const MethodInfo*))DOGetter_1_Invoke_m4107339380_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
#define DOGetter_1_Invoke_m2109831897(__this, method) ((  int32_t (*) (DOGetter_1_t3853983590 *, const MethodInfo*))DOGetter_1_Invoke_m2109831897_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
#define DOGetter_1_Invoke_m111587642(__this, method) ((  int64_t (*) (DOGetter_1_t2691184179 *, const MethodInfo*))DOGetter_1_Invoke_m111587642_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
#define DOGetter_1_Invoke_m882816534(__this, method) ((  Il2CppObject * (*) (DOGetter_1_t176588141 *, const MethodInfo*))DOGetter_1_Invoke_m882816534_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
#define DOGetter_1_Invoke_m3647275797(__this, method) ((  float (*) (DOGetter_1_t3858616074 *, const MethodInfo*))DOGetter_1_Invoke_m3647275797_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
#define DOGetter_1_Invoke_m370264108(__this, method) ((  uint32_t (*) (DOGetter_1_t3931788163 *, const MethodInfo*))DOGetter_1_Invoke_m370264108_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
#define DOGetter_1_Invoke_m2202291235(__this, method) ((  uint64_t (*) (DOGetter_1_t396335760 *, const MethodInfo*))DOGetter_1_Invoke_m2202291235_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
#define DOGetter_1_Invoke_m222071538(__this, method) ((  Color_t2020392075  (*) (DOGetter_1_t3802498217 *, const MethodInfo*))DOGetter_1_Invoke_m222071538_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
#define DOGetter_1_Invoke_m1844179403(__this, method) ((  Quaternion_t4030073918  (*) (DOGetter_1_t1517212764 *, const MethodInfo*))DOGetter_1_Invoke_m1844179403_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
#define DOGetter_1_Invoke_m205270839(__this, method) ((  Rect_t3681755626  (*) (DOGetter_1_t1168894472 *, const MethodInfo*))DOGetter_1_Invoke_m205270839_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
#define DOGetter_1_Invoke_m3450181142(__this, method) ((  Vector2_t2243707579  (*) (DOGetter_1_t4025813721 *, const MethodInfo*))DOGetter_1_Invoke_m3450181142_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
#define DOGetter_1_Invoke_m3486946741(__this, method) ((  Vector3_t2243707580  (*) (DOGetter_1_t4025813722 *, const MethodInfo*))DOGetter_1_Invoke_m3486946741_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
#define DOGetter_1_Invoke_m3680105936(__this, method) ((  Vector4_t2243707581  (*) (DOGetter_1_t4025813723 *, const MethodInfo*))DOGetter_1_Invoke_m3680105936_gshared)(__this, method)
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
#define DOSetter_1_Invoke_m3163525136(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1890955609 *, Color2_t232726623 , const MethodInfo*))DOSetter_1_Invoke_m3163525136_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
#define DOSetter_1_Invoke_m1726747861(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1441277371 *, double, const MethodInfo*))DOSetter_1_Invoke_m1726747861_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
#define DOSetter_1_Invoke_m748616666(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3730106434 *, int32_t, const MethodInfo*))DOSetter_1_Invoke_m748616666_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
#define DOSetter_1_Invoke_m2393688041(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t2567307023 *, int64_t, const MethodInfo*))DOSetter_1_Invoke_m2393688041_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
#define DOSetter_1_Invoke_m1999305269(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t52710985 *, Il2CppObject *, const MethodInfo*))DOSetter_1_Invoke_m1999305269_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
#define DOSetter_1_Invoke_m184768384(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3734738918 *, float, const MethodInfo*))DOSetter_1_Invoke_m184768384_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
#define DOSetter_1_Invoke_m236045679(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3807911007 *, uint32_t, const MethodInfo*))DOSetter_1_Invoke_m236045679_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
#define DOSetter_1_Invoke_m4121538054(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t272458604 *, uint64_t, const MethodInfo*))DOSetter_1_Invoke_m4121538054_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
#define DOSetter_1_Invoke_m3892954541(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3678621061 *, Color_t2020392075 , const MethodInfo*))DOSetter_1_Invoke_m3892954541_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
#define DOSetter_1_Invoke_m903072570(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1393335608 *, Quaternion_t4030073918 , const MethodInfo*))DOSetter_1_Invoke_m903072570_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
#define DOSetter_1_Invoke_m1386366628(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1045017316 *, Rect_t3681755626 , const MethodInfo*))DOSetter_1_Invoke_m1386366628_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
#define DOSetter_1_Invoke_m2956451219(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3901936565 *, Vector2_t2243707579 , const MethodInfo*))DOSetter_1_Invoke_m2956451219_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
#define DOSetter_1_Invoke_m4169715988(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3901936566 *, Vector3_t2243707580 , const MethodInfo*))DOSetter_1_Invoke_m4169715988_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
#define DOSetter_1_Invoke_m212800661(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3901936567 *, Vector4_t2243707581 , const MethodInfo*))DOSetter_1_Invoke_m212800661_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Tweener::.ctor()
extern "C"  void Tweener__ctor_m102043033 (Tweener_t760404022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
extern "C"  void Debugger_LogWarning_m1294243619 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m191970594 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m3881798623 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Tween::Reset()
extern "C"  void Tween_Reset_m2158413979 (Tween_t278478013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.ColorOptions::Reset()
extern "C"  void ColorOptions_Reset_m2624799597 (ColorOptions_t2213017305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.NoOptions::Reset()
extern "C"  void NoOptions_Reset_m532649001 (NoOptions_t2508431845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.StringOptions::Reset()
extern "C"  void StringOptions_Reset_m530675817 (StringOptions_t2885323933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.FloatOptions::Reset()
extern "C"  void FloatOptions_Reset_m1378398748 (FloatOptions_t1421548266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.UintOptions::Reset()
extern "C"  void UintOptions_Reset_m3831863362 (UintOptions_t2267095136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.QuaternionOptions::Reset()
extern "C"  void QuaternionOptions_Reset_m4116534850 (QuaternionOptions_t466049668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.RectOptions::Reset()
extern "C"  void RectOptions_Reset_m297368492 (RectOptions_t3393635162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.VectorOptions::Reset()
extern "C"  void VectorOptions_Reset_m245768289 (VectorOptions_t293385261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.PathOptions::Reset()
extern "C"  void PathOptions_Reset_m4016313393 (PathOptions_t2659884781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.Vector3ArrayOptions::Reset()
extern "C"  void Vector3ArrayOptions_Reset_m277565119 (Vector3ArrayOptions_t2672570171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
#define TweenCallback_1_Invoke_m3615052821(__this, ___value0, method) ((  void (*) (TweenCallback_1_t3418705418 *, int32_t, const MethodInfo*))TweenCallback_1_Invoke_m3615052821_gshared)(__this, ___value0, method)
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
#define TweenCallback_1_Invoke_m872757678(__this, ___value0, method) ((  void (*) (TweenCallback_1_t4036277265 *, Il2CppObject *, const MethodInfo*))TweenCallback_1_Invoke_m872757678_gshared)(__this, ___value0, method)
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C"  void IndexOutOfRangeException__ctor_m1847153122 (IndexOutOfRangeException_t3527622107 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<System.Object>::Invoke(T)
#define TweenCallbackDelegate_1_Invoke_m686840820(__this, ___argumeny0, method) ((  void (*) (TweenCallbackDelegate_1_t3007645771 *, Il2CppObject *, const MethodInfo*))TweenCallbackDelegate_1_Invoke_m686840820_gshared)(__this, ___argumeny0, method)
// System.Void GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<System.Object>::Invoke(T,GameDevToolkit.Events.MouseEventType)
#define MouseDelegate_Invoke_m2233526212(__this, ___target0, ___type1, method) ((  void (*) (MouseDelegate_t702691374 *, Il2CppObject *, int32_t, const MethodInfo*))MouseDelegate_Invoke_m2233526212_gshared)(__this, ___target0, ___type1, method)
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Combine_m3791207084 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Remove_m2626518725 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameDevToolkit.Events.MouseEventProccessor::createInstance()
extern "C"  void MouseEventProccessor_createInstance_m3819814644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider2D>()
#define GameObject_GetComponent_TisCollider2D_t646061738_m4291101372(__this, method) ((  Collider2D_t646061738 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m2503577968 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameDevToolkit.Events.MouseEventProccessor::get_captureEvents()
extern "C"  bool MouseEventProccessor_get_captureEvents_m2887403793 (MouseEventProccessor_t442073356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m4079055610 (Behaviour_t955675639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2216684562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m881385315 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3275118058 * Transform_GetChild_m3838588184 (Transform_t3275118058 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type,System.Object[])
extern "C"  Il2CppObject * Activator_CreateInstance_m1465989661 (Il2CppObject * __this /* static, unused */, Type_t * p0, ObjectU5BU5D_t3614634134* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameDevToolkit.GameUtil.Randomizer GameDevToolkit.GameUtil.Randomizer::CreateRandomizer<GameDevToolkit.GameUtil.RandomizerRules.SemiRandomRule>(System.Int32,System.Int32)
#define Randomizer_CreateRandomizer_TisSemiRandomRule_t1463926147_m2503539335(__this /* static, unused */, ___min0, ___max1, method) ((  Randomizer_t1796594675 * (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Randomizer_CreateRandomizer_TisIl2CppObject_m1955574599_gshared)(__this /* static, unused */, ___min0, ___max1, method)
// System.Int32 GameDevToolkit.GameUtil.Randomizer::getRandom(System.Boolean)
extern "C"  int32_t Randomizer_getRandom_m577675497 (Randomizer_t1796594675 * __this, bool ___doNotUseRules0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameDevToolkit.GameUtil.GridPoint::.ctor(System.Int32,System.Int32)
extern "C"  void GridPoint__ctor_m3345851570 (GridPoint_t3774252050 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameDevToolkit.GameUtil.GridPoint::op_Equality(GameDevToolkit.GameUtil.GridPoint,GameDevToolkit.GameUtil.GridPoint)
extern "C"  bool GridPoint_op_Equality_m3028292086 (Il2CppObject * __this /* static, unused */, GridPoint_t3774252050 * ___a0, GridPoint_t3774252050 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<GameDevToolkit.GameUtil.GridPoint>::.ctor()
#define List_1__ctor_m3541242976(__this, method) ((  void (*) (List_1_t3143373182 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GameDevToolkit.GameUtil.GridPoint>::Add(!0)
#define List_1_Add_m2362510564(__this, p0, method) ((  void (*) (List_1_t3143373182 *, GridPoint_t3774252050 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// !0[] System.Collections.Generic.List`1<GameDevToolkit.GameUtil.GridPoint>::ToArray()
#define List_1_ToArray_m4211291750(__this, method) ((  GridPointU5BU5D_t1678401383* (*) (List_1_t3143373182 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m485833136 (Exception_t1927440687 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m475173995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Convert::ChangeType(System.Object,System.Type)
extern "C"  Il2CppObject * Convert_ChangeType_m1630780412 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Type_t * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<GameTwentyoneManager/GiftObject>::Invoke(T)
#define Action_1_Invoke_m3862957638(__this, ___obj0, method) ((  void (*) (Action_1_t1946475162 *, GiftObject_t2144675780 , const MethodInfo*))Action_1_Invoke_m3862957638_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Boolean>::Invoke(T)
#define Action_1_Invoke_m3662000152(__this, ___obj0, method) ((  void (*) (Action_1_t3627374100 *, bool, const MethodInfo*))Action_1_Invoke_m3662000152_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Char>::Invoke(T)
#define Action_1_Invoke_m3702739261(__this, ___obj0, method) ((  void (*) (Action_1_t3256280720 *, Il2CppChar, const MethodInfo*))Action_1_Invoke_m3702739261_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Int32>::Invoke(T)
#define Action_1_Invoke_m2055228803(__this, ___obj0, method) ((  void (*) (Action_1_t1873676830 *, int32_t, const MethodInfo*))Action_1_Invoke_m2055228803_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Object>::Invoke(T)
#define Action_1_Invoke_m4180501989(__this, ___obj0, method) ((  void (*) (Action_1_t2491248677 *, Il2CppObject *, const MethodInfo*))Action_1_Invoke_m4180501989_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
#define Action_1_Invoke_m3377187479(__this, ___obj0, method) ((  void (*) (Action_1_t4190924221 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))Action_1_Invoke_m3377187479_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
#define Action_1_Invoke_m3721769778(__this, ___obj0, method) ((  void (*) (Action_1_t1299997296 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))Action_1_Invoke_m3721769778_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.AnimatorClipInfo>::Invoke(T)
#define Action_1_Invoke_m648331504(__this, ___obj0, method) ((  void (*) (Action_1_t3707550731 *, AnimatorClipInfo_t3905751349 , const MethodInfo*))Action_1_Invoke_m648331504_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.Color>::Invoke(T)
#define Action_1_Invoke_m3422995740(__this, ___obj0, method) ((  void (*) (Action_1_t1822191457 *, Color_t2020392075 , const MethodInfo*))Action_1_Invoke_m3422995740_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.Color32>::Invoke(T)
#define Action_1_Invoke_m3852022121(__this, ___obj0, method) ((  void (*) (Action_1_t676316900 *, Color32_t874517518 , const MethodInfo*))Action_1_Invoke_m3852022121_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
#define Action_1_Invoke_m2664461947(__this, ___obj0, method) ((  void (*) (Action_1_t4117953054 *, RaycastResult_t21186376 , const MethodInfo*))Action_1_Invoke_m2664461947_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.KeyCode>::Invoke(T)
#define Action_1_Invoke_m4078720527(__this, ___obj0, method) ((  void (*) (Action_1_t2085194534 *, int32_t, const MethodInfo*))Action_1_Invoke_m4078720527_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.UICharInfo>::Invoke(T)
#define Action_1_Invoke_m433096323(__this, ___obj0, method) ((  void (*) (Action_1_t2858436182 *, UICharInfo_t3056636800 , const MethodInfo*))Action_1_Invoke_m433096323_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.UILineInfo>::Invoke(T)
#define Action_1_Invoke_m1440002041(__this, ___obj0, method) ((  void (*) (Action_1_t3423077256 *, UILineInfo_t3621277874 , const MethodInfo*))Action_1_Invoke_m1440002041_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.UIVertex>::Invoke(T)
#define Action_1_Invoke_m2226099137(__this, ___obj0, method) ((  void (*) (Action_1_t1006058200 *, UIVertex_t1204258818 , const MethodInfo*))Action_1_Invoke_m2226099137_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.Vector2>::Invoke(T)
#define Action_1_Invoke_m3472061792(__this, ___obj0, method) ((  void (*) (Action_1_t2045506961 *, Vector2_t2243707579 , const MethodInfo*))Action_1_Invoke_m3472061792_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.Vector3>::Invoke(T)
#define Action_1_Invoke_m212965499(__this, ___obj0, method) ((  void (*) (Action_1_t2045506962 *, Vector3_t2243707580 , const MethodInfo*))Action_1_Invoke_m212965499_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.Vector4>::Invoke(T)
#define Action_1_Invoke_m3127338686(__this, ___obj0, method) ((  void (*) (Action_1_t2045506963 *, Vector4_t2243707581 , const MethodInfo*))Action_1_Invoke_m3127338686_gshared)(__this, ___obj0, method)
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
#define Action_2_Invoke_m352317182(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2525452034 *, bool, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m352317182_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
#define Action_2_Invoke_m1501152969(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2572051853 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m1501152969_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m1595007065 (ArgumentOutOfRangeException_t279959794 * __this, String_t* ___paramName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::CopyTo(System.Array,System.Int32)
extern "C"  void Array_CopyTo_m4061033315 (Il2CppArray * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor(System.String)
extern "C"  void NotSupportedException__ctor_m836173213 (NotSupportedException_t1793819818 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DDManagerBase`1<System.Object>::.ctor()
extern "C"  void DDManagerBase_1__ctor_m3690082962_gshared (DDManagerBase_1_t3108376481 * __this, const MethodInfo* method)
{
	{
		NullCheck((SingletonBase_1_t1688964385 *)__this);
		((  void (*) (SingletonBase_1_t1688964385 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((SingletonBase_1_t1688964385 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void DDManagerBase`1<System.Object>::Awake()
extern "C"  void DDManagerBase_1_Awake_m1282781541_gshared (DDManagerBase_1_t3108376481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DDManagerBase_1_Awake_m1282781541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((SingletonBase_1_t1688964385 *)__this);
		((  void (*) (SingletonBase_1_t1688964385 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((SingletonBase_1_t1688964385 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, (Object_t1021602117 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2103134696_gshared (DOGetter_1_t2014832765 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C"  Color2_t232726623  DOGetter_1_Invoke_m1748194493_gshared (DOGetter_1_t2014832765 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1748194493((DOGetter_1_t2014832765 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Color2_t232726623  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color2_t232726623  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3134053505_gshared (DOGetter_1_t2014832765 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C"  Color2_t232726623  DOGetter_1_EndInvoke_m3467158547_gshared (DOGetter_1_t2014832765 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color2_t232726623 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m188978433_gshared (DOGetter_1_t1565154527 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
extern "C"  double DOGetter_1_Invoke_m4107339380_gshared (DOGetter_1_t1565154527 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m4107339380((DOGetter_1_t1565154527 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef double (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Double>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2666210620_gshared (DOGetter_1_t1565154527 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double DOGetter_1_EndInvoke_m4233629864_gshared (DOGetter_1_t1565154527 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2793538546_gshared (DOGetter_1_t3853983590 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
extern "C"  int32_t DOGetter_1_Invoke_m2109831897_gshared (DOGetter_1_t3853983590 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2109831897((DOGetter_1_t3853983590 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1887709557_gshared (DOGetter_1_t3853983590 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t DOGetter_1_EndInvoke_m912235863_gshared (DOGetter_1_t3853983590 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1068054853_gshared (DOGetter_1_t2691184179 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C"  int64_t DOGetter_1_Invoke_m111587642_gshared (DOGetter_1_t2691184179 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m111587642((DOGetter_1_t2691184179 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2803366776_gshared (DOGetter_1_t2691184179 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t DOGetter_1_EndInvoke_m1345976682_gshared (DOGetter_1_t2691184179 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2292700265_gshared (DOGetter_1_t176588141 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * DOGetter_1_Invoke_m882816534_gshared (DOGetter_1_t176588141 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m882816534((DOGetter_1_t176588141 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3769142830_gshared (DOGetter_1_t176588141 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * DOGetter_1_EndInvoke_m4273483522_gshared (DOGetter_1_t176588141 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3288120768_gshared (DOGetter_1_t3858616074 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
extern "C"  float DOGetter_1_Invoke_m3647275797_gshared (DOGetter_1_t3858616074 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3647275797((DOGetter_1_t3858616074 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Single>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1690324125_gshared (DOGetter_1_t3858616074 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float DOGetter_1_EndInvoke_m2074726563_gshared (DOGetter_1_t3858616074 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1362595967_gshared (DOGetter_1_t3931788163 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
extern "C"  uint32_t DOGetter_1_Invoke_m370264108_gshared (DOGetter_1_t3931788163 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m370264108((DOGetter_1_t3931788163 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3143608020_gshared (DOGetter_1_t3931788163 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t DOGetter_1_EndInvoke_m859660492_gshared (DOGetter_1_t3931788163 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3368190962_gshared (DOGetter_1_t396335760 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
extern "C"  uint64_t DOGetter_1_Invoke_m2202291235_gshared (DOGetter_1_t396335760 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2202291235((DOGetter_1_t396335760 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint64_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1007973739_gshared (DOGetter_1_t396335760 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  uint64_t DOGetter_1_EndInvoke_m3446459513_gshared (DOGetter_1_t396335760 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m4239784889_gshared (DOGetter_1_t3802498217 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C"  Color_t2020392075  DOGetter_1_Invoke_m222071538_gshared (DOGetter_1_t3802498217 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m222071538((DOGetter_1_t3802498217 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Color_t2020392075  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color_t2020392075  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3894549130_gshared (DOGetter_1_t3802498217 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  Color_t2020392075  DOGetter_1_EndInvoke_m2918527242_gshared (DOGetter_1_t3802498217 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color_t2020392075 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3923641982_gshared (DOGetter_1_t1517212764 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
extern "C"  Quaternion_t4030073918  DOGetter_1_Invoke_m1844179403_gshared (DOGetter_1_t1517212764 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1844179403((DOGetter_1_t1517212764 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Quaternion_t4030073918  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Quaternion_t4030073918  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2685409055_gshared (DOGetter_1_t1517212764 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  Quaternion_t4030073918  DOGetter_1_EndInvoke_m4117334265_gshared (DOGetter_1_t1517212764 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Quaternion_t4030073918 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2167974952_gshared (DOGetter_1_t1168894472 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C"  Rect_t3681755626  DOGetter_1_Invoke_m205270839_gshared (DOGetter_1_t1168894472 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m205270839((DOGetter_1_t1168894472 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Rect_t3681755626  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Rect_t3681755626  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2542487891_gshared (DOGetter_1_t1168894472 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  Rect_t3681755626  DOGetter_1_EndInvoke_m47398349_gshared (DOGetter_1_t1168894472 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Rect_t3681755626 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2878130562_gshared (DOGetter_1_t4025813721 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C"  Vector2_t2243707579  DOGetter_1_Invoke_m3450181142_gshared (DOGetter_1_t4025813721 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3450181142((DOGetter_1_t4025813721 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector2_t2243707579  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector2_t2243707579  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2030350446_gshared (DOGetter_1_t4025813721 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  Vector2_t2243707579  DOGetter_1_EndInvoke_m1102230086_gshared (DOGetter_1_t4025813721 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector2_t2243707579 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1323234132_gshared (DOGetter_1_t4025813722 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
extern "C"  Vector3_t2243707580  DOGetter_1_Invoke_m3486946741_gshared (DOGetter_1_t4025813722 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3486946741((DOGetter_1_t4025813722 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2677033165_gshared (DOGetter_1_t4025813722 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3_t2243707580  DOGetter_1_EndInvoke_m3095315399_gshared (DOGetter_1_t4025813722 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector3_t2243707580 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m664413081_gshared (DOGetter_1_t4025813723 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
extern "C"  Vector4_t2243707581  DOGetter_1_Invoke_m3680105936_gshared (DOGetter_1_t4025813723 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3680105936((DOGetter_1_t4025813723 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector4_t2243707581  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector4_t2243707581  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1764844456_gshared (DOGetter_1_t4025813723 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  Vector4_t2243707581  DOGetter_1_EndInvoke_m2636742600_gshared (DOGetter_1_t4025813723 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector4_t2243707581 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1733732172_gshared (DOSetter_1_t1890955609 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3163525136_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3163525136((DOSetter_1_t1890955609 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3915298133_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3915298133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color2_t232726623_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2796185966_gshared (DOSetter_1_t1890955609 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3181129925_gshared (DOSetter_1_t1441277371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1726747861_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1726747861((DOSetter_1_t1441277371 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, double ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, double ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1311537432_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1311537432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Double_t4078015681_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2118077943_gshared (DOSetter_1_t1441277371 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2404517806_gshared (DOSetter_1_t3730106434 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m748616666_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m748616666((DOSetter_1_t3730106434 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3860168801_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3860168801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m3034368096_gshared (DOSetter_1_t3730106434 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1460312617_gshared (DOSetter_1_t2567307023 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2393688041_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2393688041((DOSetter_1_t2567307023 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m736671164_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m736671164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m977414887_gshared (DOSetter_1_t2567307023 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m4187492813_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1999305269_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1999305269((DOSetter_1_t52710985 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m4262435930_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___pNewValue0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m936279787_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2613085532_gshared (DOSetter_1_t3734738918 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m184768384_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m184768384((DOSetter_1_t3734738918 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3656965161_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3656965161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1222853090_gshared (DOSetter_1_t3734738918 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2057781435_gshared (DOSetter_1_t3807911007 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m236045679_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m236045679((DOSetter_1_t3807911007 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2436040648_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2436040648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2552133209_gshared (DOSetter_1_t3807911007 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1650381910_gshared (DOSetter_1_t272458604 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4121538054_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m4121538054((DOSetter_1_t272458604 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2966874951_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2966874951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m480288392_gshared (DOSetter_1_t272458604 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3548377173_gshared (DOSetter_1_t3678621061 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3892954541_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3892954541((DOSetter_1_t3678621061 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2663926534_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2663926534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2243481199_gshared (DOSetter_1_t3678621061 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2116426170_gshared (DOSetter_1_t1393335608 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m903072570_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m903072570((DOSetter_1_t1393335608 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1885597315_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1885597315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Quaternion_t4030073918_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2987470368_gshared (DOSetter_1_t1393335608 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2570159628_gshared (DOSetter_1_t1045017316 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1386366628_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1386366628((DOSetter_1_t1045017316 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3580342655_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3580342655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Rect_t3681755626_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1624612738_gshared (DOSetter_1_t1045017316 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3532836198_gshared (DOSetter_1_t3901936565 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2956451219_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2956451219((DOSetter_1_t3901936565 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2720752834_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2720752834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m3251197393_gshared (DOSetter_1_t3901936565 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m668528496_gshared (DOSetter_1_t3901936566 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4169715988_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m4169715988((DOSetter_1_t3901936566 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m681046145_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m681046145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m949315410_gshared (DOSetter_1_t3901936566 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m9707445_gshared (DOSetter_1_t3901936567 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m212800661_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m212800661((DOSetter_1_t3901936567 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2980016964_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2980016964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1324394315_gshared (DOSetter_1_t3901936567 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1803465319_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1803465319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m157243687_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3909355140_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3909355140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, Color2_t232726623 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (Color2_t232726623 )((*(Color2_t232726623 *)((Color2_t232726623 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1733021291_gshared (TweenerCore_3_t3925803634 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2833266637 * L_0 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3925803634 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2833266637 *)L_0, (TweenerCore_3_t3925803634 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m731417792_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2833266637 * L_0 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2833266637 * L_1 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3925803634 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2833266637 *)L_1, (TweenerCore_3_t3925803634 *)__this);
	}

IL_001a:
	{
		ColorOptions_t2213017305 * L_2 = (ColorOptions_t2213017305 *)__this->get_address_of_plugOptions_56();
		ColorOptions_Reset_m2624799597((ColorOptions_t2213017305 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t2014832765 *)NULL);
		__this->set_setter_58((DOSetter_1_t1890955609 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2044147238_gshared (TweenerCore_3_t3925803634 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m4133666392_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3682676813_gshared (TweenerCore_3_t3925803634 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3682676813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2833266637 * L_5 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_6 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2014832765 * L_8 = (DOGetter_1_t2014832765 *)__this->get_getter_57();
		DOSetter_1_t1890955609 * L_9 = (DOSetter_1_t1890955609 *)__this->get_setter_58();
		float L_10 = V_0;
		Color2_t232726623  L_11 = (Color2_t232726623 )__this->get_startValue_53();
		Color2_t232726623  L_12 = (Color2_t232726623 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_5);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t2014832765 *, DOSetter_1_t1890955609 *, float, Color2_t232726623 , Color2_t232726623 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2833266637 *)L_5, (ColorOptions_t2213017305 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t2014832765 *)L_8, (DOSetter_1_t1890955609 *)L_9, (float)L_10, (Color2_t232726623 )L_11, (Color2_t232726623 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2833266637 * L_16 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_17 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2014832765 * L_19 = (DOGetter_1_t2014832765 *)__this->get_getter_57();
		DOSetter_1_t1890955609 * L_20 = (DOSetter_1_t1890955609 *)__this->get_setter_58();
		float L_21 = V_0;
		Color2_t232726623  L_22 = (Color2_t232726623 )__this->get_startValue_53();
		Color2_t232726623  L_23 = (Color2_t232726623 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_16);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t2014832765 *, DOSetter_1_t1890955609 *, float, Color2_t232726623 , Color2_t232726623 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2833266637 *)L_16, (ColorOptions_t2213017305 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t2014832765 *)L_19, (DOSetter_1_t1890955609 *)L_20, (float)L_21, (Color2_t232726623 )L_22, (Color2_t232726623 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1180323095_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1180323095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m647570427_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m912926810_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m912926810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, double, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (double)((*(double*)((double*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m3460574231_gshared (TweenerCore_3_t456598886 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3659029185 * L_0 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t456598886 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3659029185 *)L_0, (TweenerCore_3_t456598886 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3599961708_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3659029185 * L_0 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3659029185 * L_1 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t456598886 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3659029185 *)L_1, (TweenerCore_3_t456598886 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t1565154527 *)NULL);
		__this->set_setter_58((DOSetter_1_t1441277371 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3059104922_gshared (TweenerCore_3_t456598886 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m514828332_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3426149681_gshared (TweenerCore_3_t456598886 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3426149681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3659029185 * L_5 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1565154527 * L_8 = (DOGetter_1_t1565154527 *)__this->get_getter_57();
		DOSetter_1_t1441277371 * L_9 = (DOSetter_1_t1441277371 *)__this->get_setter_58();
		float L_10 = V_0;
		double L_11 = (double)__this->get_startValue_53();
		double L_12 = (double)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1565154527 *, DOSetter_1_t1441277371 *, float, double, double, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3659029185 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1565154527 *)L_8, (DOSetter_1_t1441277371 *)L_9, (float)L_10, (double)L_11, (double)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3659029185 * L_16 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1565154527 * L_19 = (DOGetter_1_t1565154527 *)__this->get_getter_57();
		DOSetter_1_t1441277371 * L_20 = (DOSetter_1_t1441277371 *)__this->get_setter_58();
		float L_21 = V_0;
		double L_22 = (double)__this->get_startValue_53();
		double L_23 = (double)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1565154527 *, DOSetter_1_t1441277371 *, float, double, double, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3659029185 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1565154527 *)L_19, (DOSetter_1_t1441277371 *)L_20, (float)L_21, (double)L_22, (double)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m2533780907_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2533780907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3691364999_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3330381752_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3330381752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, int32_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m230978731_gshared (TweenerCore_3_t3160108754 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2067571757 * L_0 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3160108754 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2067571757 *)L_0, (TweenerCore_3_t3160108754 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m387790262_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2067571757 * L_0 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2067571757 * L_1 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3160108754 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2067571757 *)L_1, (TweenerCore_3_t3160108754 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t3853983590 *)NULL);
		__this->set_setter_58((DOSetter_1_t3730106434 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3455091692_gshared (TweenerCore_3_t3160108754 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3282992694_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2587998509_gshared (TweenerCore_3_t3160108754 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2587998509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2067571757 * L_5 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3853983590 * L_8 = (DOGetter_1_t3853983590 *)__this->get_getter_57();
		DOSetter_1_t3730106434 * L_9 = (DOSetter_1_t3730106434 *)__this->get_setter_58();
		float L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get_startValue_53();
		int32_t L_12 = (int32_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t3853983590 *, DOSetter_1_t3730106434 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2067571757 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3853983590 *)L_8, (DOSetter_1_t3730106434 *)L_9, (float)L_10, (int32_t)L_11, (int32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2067571757 * L_16 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3853983590 * L_19 = (DOGetter_1_t3853983590 *)__this->get_getter_57();
		DOSetter_1_t3730106434 * L_20 = (DOSetter_1_t3730106434 *)__this->get_setter_58();
		float L_21 = V_0;
		int32_t L_22 = (int32_t)__this->get_startValue_53();
		int32_t L_23 = (int32_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t3853983590 *, DOSetter_1_t3730106434 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2067571757 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3853983590 *)L_19, (DOSetter_1_t3730106434 *)L_20, (float)L_21, (int32_t)L_22, (int32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1918761711_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1918761711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3842480115_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2582633306_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2582633306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, int64_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m4270264951_gshared (TweenerCore_3_t3199111798 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2106574801 * L_0 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3199111798 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2106574801 *)L_0, (TweenerCore_3_t3199111798 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1606553284_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2106574801 * L_0 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2106574801 * L_1 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3199111798 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2106574801 *)L_1, (TweenerCore_3_t3199111798 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t2691184179 *)NULL);
		__this->set_setter_58((DOSetter_1_t2567307023 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m690694406_gshared (TweenerCore_3_t3199111798 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1097327556_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m1754088405_gshared (TweenerCore_3_t3199111798 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1754088405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2106574801 * L_5 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2691184179 * L_8 = (DOGetter_1_t2691184179 *)__this->get_getter_57();
		DOSetter_1_t2567307023 * L_9 = (DOSetter_1_t2567307023 *)__this->get_setter_58();
		float L_10 = V_0;
		int64_t L_11 = (int64_t)__this->get_startValue_53();
		int64_t L_12 = (int64_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t2691184179 *, DOSetter_1_t2567307023 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2106574801 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t2691184179 *)L_8, (DOSetter_1_t2567307023 *)L_9, (float)L_10, (int64_t)L_11, (int64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2106574801 * L_16 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2691184179 * L_19 = (DOGetter_1_t2691184179 *)__this->get_getter_57();
		DOSetter_1_t2567307023 * L_20 = (DOSetter_1_t2567307023 *)__this->get_setter_58();
		float L_21 = V_0;
		int64_t L_22 = (int64_t)__this->get_startValue_53();
		int64_t L_23 = (int64_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t2691184179 *, DOSetter_1_t2567307023 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2106574801 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t2691184179 *)L_19, (DOSetter_1_t2567307023 *)L_20, (float)L_21, (int64_t)L_22, (int64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1198377687_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1198377687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m1306328571_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m188691034_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m188691034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2965628695_gshared (TweenerCore_3_t1705766462 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t613229465 * L_0 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1705766462 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t613229465 *)L_0, (TweenerCore_3_t1705766462 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2741938284_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t613229465 * L_0 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t613229465 * L_1 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1705766462 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t613229465 *)L_1, (TweenerCore_3_t1705766462 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t176588141 *)NULL);
		__this->set_setter_58((DOSetter_1_t52710985 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2609812250_gshared (TweenerCore_3_t1705766462 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3258896428_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m749792945_gshared (TweenerCore_3_t1705766462 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m749792945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t613229465 * L_5 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_8 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_9 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t613229465 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t176588141 *)L_8, (DOSetter_1_t52710985 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t613229465 * L_16 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_19 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_20 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t613229465 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t176588141 *)L_19, (DOSetter_1_t52710985 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m3971101479_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3971101479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2107526987_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2269915508_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2269915508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m528864199_gshared (TweenerCore_3_t2082658550 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t990121553 * L_0 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2082658550 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t990121553 *)L_0, (TweenerCore_3_t2082658550 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m492887674_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t990121553 * L_0 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t990121553 * L_1 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2082658550 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t990121553 *)L_1, (TweenerCore_3_t2082658550 *)__this);
	}

IL_001a:
	{
		StringOptions_t2885323933 * L_2 = (StringOptions_t2885323933 *)__this->get_address_of_plugOptions_56();
		StringOptions_Reset_m530675817((StringOptions_t2885323933 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t176588141 *)NULL);
		__this->set_setter_58((DOSetter_1_t52710985 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3197074312_gshared (TweenerCore_3_t2082658550 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2775364066_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m4039624089_gshared (TweenerCore_3_t2082658550 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m4039624089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t990121553 * L_5 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		StringOptions_t2885323933  L_6 = (StringOptions_t2885323933 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_8 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_9 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_5);
		VirtActionInvoker11< StringOptions_t2885323933 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t990121553 *)L_5, (StringOptions_t2885323933 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t176588141 *)L_8, (DOSetter_1_t52710985 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t990121553 * L_16 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		StringOptions_t2885323933  L_17 = (StringOptions_t2885323933 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_19 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_20 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_16);
		VirtActionInvoker11< StringOptions_t2885323933 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t990121553 *)L_16, (StringOptions_t2885323933 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t176588141 *)L_19, (DOSetter_1_t52710985 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1849842838_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1849842838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m407697134_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2067864171_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2067864171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)((*(float*)((float*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m3054976738_gshared (TweenerCore_3_t2279406887 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1186869890 * L_0 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2279406887 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1186869890 *)L_0, (TweenerCore_3_t2279406887 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3808154951_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1186869890 * L_0 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1186869890 * L_1 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2279406887 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1186869890 *)L_1, (TweenerCore_3_t2279406887 *)__this);
	}

IL_001a:
	{
		FloatOptions_t1421548266 * L_2 = (FloatOptions_t1421548266 *)__this->get_address_of_plugOptions_56();
		FloatOptions_Reset_m1378398748((FloatOptions_t1421548266 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t3858616074 *)NULL);
		__this->set_setter_58((DOSetter_1_t3734738918 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2385689477_gshared (TweenerCore_3_t2279406887 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3559394027_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3135202618_gshared (TweenerCore_3_t2279406887 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3135202618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1186869890 * L_5 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		FloatOptions_t1421548266  L_6 = (FloatOptions_t1421548266 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3858616074 * L_8 = (DOGetter_1_t3858616074 *)__this->get_getter_57();
		DOSetter_1_t3734738918 * L_9 = (DOSetter_1_t3734738918 *)__this->get_setter_58();
		float L_10 = V_0;
		float L_11 = (float)__this->get_startValue_53();
		float L_12 = (float)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_5);
		VirtActionInvoker11< FloatOptions_t1421548266 , Tween_t278478013 *, bool, DOGetter_1_t3858616074 *, DOSetter_1_t3734738918 *, float, float, float, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1186869890 *)L_5, (FloatOptions_t1421548266 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3858616074 *)L_8, (DOSetter_1_t3734738918 *)L_9, (float)L_10, (float)L_11, (float)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1186869890 * L_16 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		FloatOptions_t1421548266  L_17 = (FloatOptions_t1421548266 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3858616074 * L_19 = (DOGetter_1_t3858616074 *)__this->get_getter_57();
		DOSetter_1_t3734738918 * L_20 = (DOSetter_1_t3734738918 *)__this->get_setter_58();
		float L_21 = V_0;
		float L_22 = (float)__this->get_startValue_53();
		float L_23 = (float)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_16);
		VirtActionInvoker11< FloatOptions_t1421548266 , Tween_t278478013 *, bool, DOGetter_1_t3858616074 *, DOSetter_1_t3734738918 *, float, float, float, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1186869890 *)L_16, (FloatOptions_t1421548266 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3858616074 *)L_19, (DOSetter_1_t3734738918 *)L_20, (float)L_21, (float)L_22, (float)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m4074697550_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m4074697550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2418838464_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m949598801_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m949598801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, uint32_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1000169988_gshared (TweenerCore_3_t1648829745 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t556292748 * L_0 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1648829745 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t556292748 *)L_0, (TweenerCore_3_t1648829745 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2146775889_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t556292748 * L_0 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t556292748 * L_1 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1648829745 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t556292748 *)L_1, (TweenerCore_3_t1648829745 *)__this);
	}

IL_001a:
	{
		UintOptions_t2267095136 * L_2 = (UintOptions_t2267095136 *)__this->get_address_of_plugOptions_56();
		UintOptions_Reset_m3831863362((UintOptions_t2267095136 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t3931788163 *)NULL);
		__this->set_setter_58((DOSetter_1_t3807911007 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3527224199_gshared (TweenerCore_3_t1648829745 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3369460217_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3033691518_gshared (TweenerCore_3_t1648829745 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3033691518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t556292748 * L_5 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		UintOptions_t2267095136  L_6 = (UintOptions_t2267095136 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3931788163 * L_8 = (DOGetter_1_t3931788163 *)__this->get_getter_57();
		DOSetter_1_t3807911007 * L_9 = (DOSetter_1_t3807911007 *)__this->get_setter_58();
		float L_10 = V_0;
		uint32_t L_11 = (uint32_t)__this->get_startValue_53();
		uint32_t L_12 = (uint32_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_5);
		VirtActionInvoker11< UintOptions_t2267095136 , Tween_t278478013 *, bool, DOGetter_1_t3931788163 *, DOSetter_1_t3807911007 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t556292748 *)L_5, (UintOptions_t2267095136 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3931788163 *)L_8, (DOSetter_1_t3807911007 *)L_9, (float)L_10, (uint32_t)L_11, (uint32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t556292748 * L_16 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		UintOptions_t2267095136  L_17 = (UintOptions_t2267095136 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3931788163 * L_19 = (DOGetter_1_t3931788163 *)__this->get_getter_57();
		DOSetter_1_t3807911007 * L_20 = (DOSetter_1_t3807911007 *)__this->get_setter_58();
		float L_21 = V_0;
		uint32_t L_22 = (uint32_t)__this->get_startValue_53();
		uint32_t L_23 = (uint32_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_16);
		VirtActionInvoker11< UintOptions_t2267095136 , Tween_t278478013 *, bool, DOGetter_1_t3931788163 *, DOSetter_1_t3807911007 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t556292748 *)L_16, (UintOptions_t2267095136 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3931788163 *)L_19, (DOSetter_1_t3807911007 *)L_20, (float)L_21, (uint32_t)L_22, (uint32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m418004407_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m418004407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2663196443_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m1802430330_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m1802430330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, uint64_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m175513655_gshared (TweenerCore_3_t2937657178 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1845120181 * L_0 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2937657178 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1845120181 *)L_0, (TweenerCore_3_t2937657178 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2800505548_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1845120181 * L_0 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1845120181 * L_1 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2937657178 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1845120181 *)L_1, (TweenerCore_3_t2937657178 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t396335760 *)NULL);
		__this->set_setter_58((DOSetter_1_t272458604 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1164357562_gshared (TweenerCore_3_t2937657178 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m501231052_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2278209809_gshared (TweenerCore_3_t2937657178 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2278209809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1845120181 * L_5 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t396335760 * L_8 = (DOGetter_1_t396335760 *)__this->get_getter_57();
		DOSetter_1_t272458604 * L_9 = (DOSetter_1_t272458604 *)__this->get_setter_58();
		float L_10 = V_0;
		uint64_t L_11 = (uint64_t)__this->get_startValue_53();
		uint64_t L_12 = (uint64_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t396335760 *, DOSetter_1_t272458604 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1845120181 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t396335760 *)L_8, (DOSetter_1_t272458604 *)L_9, (float)L_10, (uint64_t)L_11, (uint64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1845120181 * L_16 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t396335760 * L_19 = (DOGetter_1_t396335760 *)__this->get_getter_57();
		DOSetter_1_t272458604 * L_20 = (DOSetter_1_t272458604 *)__this->get_setter_58();
		float L_21 = V_0;
		uint64_t L_22 = (uint64_t)__this->get_startValue_53();
		uint64_t L_23 = (uint64_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t396335760 *, DOSetter_1_t272458604 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1845120181 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t396335760 *)L_19, (DOSetter_1_t272458604 *)L_20, (float)L_21, (uint64_t)L_22, (uint64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m2978185163_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2978185163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m437196795_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3105054398_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3105054398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Color_t2020392075 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1414404375_gshared (TweenerCore_3_t2998039394 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1905502397 * L_0 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2998039394 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1905502397 *)L_0, (TweenerCore_3_t2998039394 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3158058770_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1905502397 * L_0 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1905502397 * L_1 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2998039394 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1905502397 *)L_1, (TweenerCore_3_t2998039394 *)__this);
	}

IL_001a:
	{
		ColorOptions_t2213017305 * L_2 = (ColorOptions_t2213017305 *)__this->get_address_of_plugOptions_56();
		ColorOptions_Reset_m2624799597((ColorOptions_t2213017305 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t3802498217 *)NULL);
		__this->set_setter_58((DOSetter_1_t3678621061 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1284389728_gshared (TweenerCore_3_t2998039394 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1966872506_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2248740889_gshared (TweenerCore_3_t2998039394 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2248740889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1905502397 * L_5 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_6 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3802498217 * L_8 = (DOGetter_1_t3802498217 *)__this->get_getter_57();
		DOSetter_1_t3678621061 * L_9 = (DOSetter_1_t3678621061 *)__this->get_setter_58();
		float L_10 = V_0;
		Color_t2020392075  L_11 = (Color_t2020392075 )__this->get_startValue_53();
		Color_t2020392075  L_12 = (Color_t2020392075 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_5);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t3802498217 *, DOSetter_1_t3678621061 *, float, Color_t2020392075 , Color_t2020392075 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1905502397 *)L_5, (ColorOptions_t2213017305 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3802498217 *)L_8, (DOSetter_1_t3678621061 *)L_9, (float)L_10, (Color_t2020392075 )L_11, (Color_t2020392075 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1905502397 * L_16 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_17 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3802498217 * L_19 = (DOGetter_1_t3802498217 *)__this->get_getter_57();
		DOSetter_1_t3678621061 * L_20 = (DOSetter_1_t3678621061 *)__this->get_setter_58();
		float L_21 = V_0;
		Color_t2020392075  L_22 = (Color_t2020392075 )__this->get_startValue_53();
		Color_t2020392075  L_23 = (Color_t2020392075 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_16);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t3802498217 *, DOSetter_1_t3678621061 *, float, Color_t2020392075 , Color_t2020392075 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1905502397 *)L_16, (ColorOptions_t2213017305 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3802498217 *)L_19, (DOSetter_1_t3678621061 *)L_20, (float)L_21, (Color_t2020392075 )L_22, (Color_t2020392075 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m2776308754_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2776308754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m398039294_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3657890433_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3657890433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, Vector3_t2243707580 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2275858234_gshared (TweenerCore_3_t1672798003 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t580261006 * L_0 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1672798003 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t580261006 *)L_0, (TweenerCore_3_t1672798003 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1088438225_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t580261006 * L_0 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t580261006 * L_1 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1672798003 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t580261006 *)L_1, (TweenerCore_3_t1672798003 *)__this);
	}

IL_001a:
	{
		QuaternionOptions_t466049668 * L_2 = (QuaternionOptions_t466049668 *)__this->get_address_of_plugOptions_56();
		QuaternionOptions_Reset_m4116534850((QuaternionOptions_t466049668 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t1517212764 *)NULL);
		__this->set_setter_58((DOSetter_1_t1393335608 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1578326919_gshared (TweenerCore_3_t1672798003 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3129492921_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2434462778_gshared (TweenerCore_3_t1672798003 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2434462778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t580261006 * L_5 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		QuaternionOptions_t466049668  L_6 = (QuaternionOptions_t466049668 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_8 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_9 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector3_t2243707580  L_11 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_12 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_5);
		VirtActionInvoker11< QuaternionOptions_t466049668 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t580261006 *)L_5, (QuaternionOptions_t466049668 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1517212764 *)L_8, (DOSetter_1_t1393335608 *)L_9, (float)L_10, (Vector3_t2243707580 )L_11, (Vector3_t2243707580 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t580261006 * L_16 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		QuaternionOptions_t466049668  L_17 = (QuaternionOptions_t466049668 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_19 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_20 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector3_t2243707580  L_22 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_23 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_16);
		VirtActionInvoker11< QuaternionOptions_t466049668 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t580261006 *)L_16, (QuaternionOptions_t466049668 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1517212764 *)L_19, (DOSetter_1_t1393335608 *)L_20, (float)L_21, (Vector3_t2243707580 )L_22, (Vector3_t2243707580 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m231619286_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m231619286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3831761276_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2524932243_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2524932243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, Rect_t3681755626 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1170036736_gshared (TweenerCore_3_t3065187631 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1972650634 * L_0 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3065187631 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1972650634 *)L_0, (TweenerCore_3_t3065187631 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m48799595_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1972650634 * L_0 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1972650634 * L_1 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3065187631 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1972650634 *)L_1, (TweenerCore_3_t3065187631 *)__this);
	}

IL_001a:
	{
		RectOptions_t3393635162 * L_2 = (RectOptions_t3393635162 *)__this->get_address_of_plugOptions_56();
		RectOptions_Reset_m297368492((RectOptions_t3393635162 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t1168894472 *)NULL);
		__this->set_setter_58((DOSetter_1_t1045017316 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3599180177_gshared (TweenerCore_3_t3065187631 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2071201127_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m4231943806_gshared (TweenerCore_3_t3065187631 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m4231943806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1972650634 * L_5 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		RectOptions_t3393635162  L_6 = (RectOptions_t3393635162 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1168894472 * L_8 = (DOGetter_1_t1168894472 *)__this->get_getter_57();
		DOSetter_1_t1045017316 * L_9 = (DOSetter_1_t1045017316 *)__this->get_setter_58();
		float L_10 = V_0;
		Rect_t3681755626  L_11 = (Rect_t3681755626 )__this->get_startValue_53();
		Rect_t3681755626  L_12 = (Rect_t3681755626 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_5);
		VirtActionInvoker11< RectOptions_t3393635162 , Tween_t278478013 *, bool, DOGetter_1_t1168894472 *, DOSetter_1_t1045017316 *, float, Rect_t3681755626 , Rect_t3681755626 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1972650634 *)L_5, (RectOptions_t3393635162 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1168894472 *)L_8, (DOSetter_1_t1045017316 *)L_9, (float)L_10, (Rect_t3681755626 )L_11, (Rect_t3681755626 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1972650634 * L_16 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		RectOptions_t3393635162  L_17 = (RectOptions_t3393635162 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1168894472 * L_19 = (DOGetter_1_t1168894472 *)__this->get_getter_57();
		DOSetter_1_t1045017316 * L_20 = (DOSetter_1_t1045017316 *)__this->get_setter_58();
		float L_21 = V_0;
		Rect_t3681755626  L_22 = (Rect_t3681755626 )__this->get_startValue_53();
		Rect_t3681755626  L_23 = (Rect_t3681755626 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_16);
		VirtActionInvoker11< RectOptions_t3393635162 , Tween_t278478013 *, bool, DOGetter_1_t1168894472 *, DOSetter_1_t1045017316 *, float, Rect_t3681755626 , Rect_t3681755626 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1972650634 *)L_16, (RectOptions_t3393635162 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1168894472 *)L_19, (DOSetter_1_t1045017316 *)L_20, (float)L_21, (Rect_t3681755626 )L_22, (Rect_t3681755626 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m3640631583_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3640631583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m4014959587_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m863616202_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m863616202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, Vector2_t2243707579 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2801490071_gshared (TweenerCore_3_t3250868854 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2158331857 * L_0 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3250868854 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2158331857 *)L_0, (TweenerCore_3_t3250868854 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3315574948_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2158331857 * L_0 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2158331857 * L_1 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3250868854 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2158331857 *)L_1, (TweenerCore_3_t3250868854 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261 * L_2 = (VectorOptions_t293385261 *)__this->get_address_of_plugOptions_56();
		VectorOptions_Reset_m245768289((VectorOptions_t293385261 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t4025813721 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936565 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1266215114_gshared (TweenerCore_3_t3250868854 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2050368132_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3268195009_gshared (TweenerCore_3_t3250868854 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3268195009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2158331857 * L_5 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813721 * L_8 = (DOGetter_1_t4025813721 *)__this->get_getter_57();
		DOSetter_1_t3901936565 * L_9 = (DOSetter_1_t3901936565 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector2_t2243707579  L_11 = (Vector2_t2243707579 )__this->get_startValue_53();
		Vector2_t2243707579  L_12 = (Vector2_t2243707579 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813721 *, DOSetter_1_t3901936565 *, float, Vector2_t2243707579 , Vector2_t2243707579 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2158331857 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813721 *)L_8, (DOSetter_1_t3901936565 *)L_9, (float)L_10, (Vector2_t2243707579 )L_11, (Vector2_t2243707579 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2158331857 * L_16 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813721 * L_19 = (DOGetter_1_t4025813721 *)__this->get_getter_57();
		DOSetter_1_t3901936565 * L_20 = (DOSetter_1_t3901936565 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector2_t2243707579  L_22 = (Vector2_t2243707579 )__this->get_startValue_53();
		Vector2_t2243707579  L_23 = (Vector2_t2243707579 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813721 *, DOSetter_1_t3901936565 *, float, Vector2_t2243707579 , Vector2_t2243707579 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2158331857 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813721 *)L_19, (DOSetter_1_t3901936565 *)L_20, (float)L_21, (Vector2_t2243707579 )L_22, (Vector2_t2243707579 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1012438200_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1012438200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2479530614_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2654203693_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2654203693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2363190842_gshared (TweenerCore_3_t1622518059 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t529981062 * L_0 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1622518059 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t529981062 *)L_0, (TweenerCore_3_t1622518059 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2389620197_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t529981062 * L_0 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t529981062 * L_1 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1622518059 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t529981062 *)L_1, (TweenerCore_3_t1622518059 *)__this);
	}

IL_001a:
	{
		PathOptions_t2659884781 * L_2 = (PathOptions_t2659884781 *)__this->get_address_of_plugOptions_56();
		PathOptions_Reset_m4016313393((PathOptions_t2659884781 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2529672523_gshared (TweenerCore_3_t1622518059 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2279545925_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m1795781652_gshared (TweenerCore_3_t1622518059 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1795781652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t529981062 * L_5 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		PathOptions_t2659884781  L_6 = (PathOptions_t2659884781 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_5);
		VirtActionInvoker11< PathOptions_t2659884781 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t529981062 *)L_5, (PathOptions_t2659884781 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t529981062 * L_16 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		PathOptions_t2659884781  L_17 = (PathOptions_t2659884781 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_16);
		VirtActionInvoker11< PathOptions_t2659884781 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t529981062 *)L_16, (PathOptions_t2659884781 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1432615606_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1432615606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m4171376220_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2062234395_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2062234395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2915691848_gshared (TweenerCore_3_t1635203449 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t542666452 * L_0 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1635203449 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t542666452 *)L_0, (TweenerCore_3_t1635203449 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3865103419_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t542666452 * L_0 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t542666452 * L_1 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1635203449 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t542666452 *)L_1, (TweenerCore_3_t1635203449 *)__this);
	}

IL_001a:
	{
		Vector3ArrayOptions_t2672570171 * L_2 = (Vector3ArrayOptions_t2672570171 *)__this->get_address_of_plugOptions_56();
		Vector3ArrayOptions_Reset_m277565119((Vector3ArrayOptions_t2672570171 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1806502181_gshared (TweenerCore_3_t1635203449 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2282744407_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3384733786_gshared (TweenerCore_3_t1635203449 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3384733786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t542666452 * L_5 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		Vector3ArrayOptions_t2672570171  L_6 = (Vector3ArrayOptions_t2672570171 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_5);
		VirtActionInvoker11< Vector3ArrayOptions_t2672570171 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t542666452 *)L_5, (Vector3ArrayOptions_t2672570171 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t542666452 * L_16 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		Vector3ArrayOptions_t2672570171  L_17 = (Vector3ArrayOptions_t2672570171 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_16);
		VirtActionInvoker11< Vector3ArrayOptions_t2672570171 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t542666452 *)L_16, (Vector3ArrayOptions_t2672570171 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m4171196319_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m4171196319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2834813539_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3688747850_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3688747850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, Vector3_t2243707580 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1933156119_gshared (TweenerCore_3_t1108663466 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t16126469 * L_0 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1108663466 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t16126469 *)L_0, (TweenerCore_3_t1108663466 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3846139684_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t16126469 * L_0 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t16126469 * L_1 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1108663466 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t16126469 *)L_1, (TweenerCore_3_t1108663466 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261 * L_2 = (VectorOptions_t293385261 *)__this->get_address_of_plugOptions_56();
		VectorOptions_Reset_m245768289((VectorOptions_t293385261 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m283403338_gshared (TweenerCore_3_t1108663466 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2716651012_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3434606145_gshared (TweenerCore_3_t1108663466 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3434606145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t16126469 * L_5 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector3_t2243707580  L_11 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_12 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t16126469 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Vector3_t2243707580 )L_11, (Vector3_t2243707580 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t16126469 * L_16 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector3_t2243707580  L_22 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_23 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t16126469 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Vector3_t2243707580 )L_22, (Vector3_t2243707580 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1407812639_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1407812639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2737049315_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3143172042_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3143172042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, Vector4_t2243707581 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1394097047_gshared (TweenerCore_3_t3261425374 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2168888377 * L_0 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3261425374 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2168888377 *)L_0, (TweenerCore_3_t3261425374 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1082756004_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2168888377 * L_0 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2168888377 * L_1 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3261425374 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2168888377 *)L_1, (TweenerCore_3_t3261425374 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261 * L_2 = (VectorOptions_t293385261 *)__this->get_address_of_plugOptions_56();
		VectorOptions_Reset_m245768289((VectorOptions_t293385261 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t4025813723 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936567 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m465544650_gshared (TweenerCore_3_t3261425374 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3303950980_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m140499649_gshared (TweenerCore_3_t3261425374 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m140499649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2168888377 * L_5 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813723 * L_8 = (DOGetter_1_t4025813723 *)__this->get_getter_57();
		DOSetter_1_t3901936567 * L_9 = (DOSetter_1_t3901936567 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector4_t2243707581  L_11 = (Vector4_t2243707581 )__this->get_startValue_53();
		Vector4_t2243707581  L_12 = (Vector4_t2243707581 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813723 *, DOSetter_1_t3901936567 *, float, Vector4_t2243707581 , Vector4_t2243707581 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2168888377 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813723 *)L_8, (DOSetter_1_t3901936567 *)L_9, (float)L_10, (Vector4_t2243707581 )L_11, (Vector4_t2243707581 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2168888377 * L_16 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813723 * L_19 = (DOGetter_1_t4025813723 *)__this->get_getter_57();
		DOSetter_1_t3901936567 * L_20 = (DOSetter_1_t3901936567 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector4_t2243707581  L_22 = (Vector4_t2243707581 )__this->get_startValue_53();
		Vector4_t2243707581  L_23 = (Vector4_t2243707581 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813723 *, DOSetter_1_t3901936567 *, float, Vector4_t2243707581 , Vector4_t2243707581 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2168888377 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813723 *)L_19, (DOSetter_1_t3901936567 *)L_20, (float)L_21, (Vector4_t2243707581 )L_22, (Vector4_t2243707581 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3622767690_gshared (ABSTweenPlugin_3_t2833266637 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1578194524_gshared (ABSTweenPlugin_3_t3659029185 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m36425734_gshared (ABSTweenPlugin_3_t2067571757 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m223063124_gshared (ABSTweenPlugin_3_t2106574801 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2153224412_gshared (ABSTweenPlugin_3_t613229465 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m912798746_gshared (ABSTweenPlugin_3_t990121553 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m4275081527_gshared (ABSTweenPlugin_3_t1186869890 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m4159300161_gshared (ABSTweenPlugin_3_t556292748 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1254825468_gshared (ABSTweenPlugin_3_t1845120181 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1367930952_gshared (ABSTweenPlugin_3_t1905502397 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m862077949_gshared (ABSTweenPlugin_3_t580261006 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1871528759_gshared (ABSTweenPlugin_3_t1972650634 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1914255460_gshared (ABSTweenPlugin_3_t2158331857 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2241027021_gshared (ABSTweenPlugin_3_t529981062 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3497636823_gshared (ABSTweenPlugin_3_t542666452 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3289379812_gshared (ABSTweenPlugin_3_t16126469 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3385119076_gshared (ABSTweenPlugin_3_t2168888377 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3015172213_gshared (TweenCallback_1_t3418705418 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3615052821_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m3615052821((TweenCallback_1_t3418705418 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m711035500_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenCallback_1_BeginInvoke_m711035500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m1922991223_gshared (TweenCallback_1_t3418705418 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3888348002_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m872757678_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m872757678((TweenCallback_1_t4036277265 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m138693653_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___value0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m3980063300_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GameDevToolkit.Common.TwoDArray`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m992873623_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3326477674 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameDevToolkit.Common.TwoDArray`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2476177349_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3326477674 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_005d;
			}
		}
	}
	{
		goto IL_0088;
	}

IL_0021:
	{
		__this->set_U3CjU3E__1_0(0);
		goto IL_006b;
	}

IL_002d:
	{
		TwoDArray_1_t846454018 * L_2 = (TwoDArray_1_t846454018 *)__this->get_U24this_1();
		int32_t L_3 = (int32_t)__this->get_U3CjU3E__1_0();
		NullCheck((TwoDArray_1_t846454018 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (TwoDArray_1_t846454018 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((TwoDArray_1_t846454018 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24current_2(L_4);
		bool L_5 = (bool)__this->get_U24disposing_3();
		if (L_5)
		{
			goto IL_0058;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0058:
	{
		goto IL_008a;
	}

IL_005d:
	{
		int32_t L_6 = (int32_t)__this->get_U3CjU3E__1_0();
		__this->set_U3CjU3E__1_0(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_006b:
	{
		int32_t L_7 = (int32_t)__this->get_U3CjU3E__1_0();
		TwoDArray_1_t846454018 * L_8 = (TwoDArray_1_t846454018 *)__this->get_U24this_1();
		NullCheck((TwoDArray_1_t846454018 *)L_8);
		int32_t L_9 = ((  int32_t (*) (TwoDArray_1_t846454018 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((TwoDArray_1_t846454018 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_0088:
	{
		return (bool)0;
	}

IL_008a:
	{
		return (bool)1;
	}
}
// System.Object GameDevToolkit.Common.TwoDArray`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m236299417_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3326477674 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object GameDevToolkit.Common.TwoDArray`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1769689169_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3326477674 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Void GameDevToolkit.Common.TwoDArray`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m71748544_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3326477674 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void GameDevToolkit.Common.TwoDArray`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2437084550_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3326477674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2437084550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GameDevToolkit.Common.TwoDArray`1<System.Object>::.ctor(System.Int32)
extern "C"  void TwoDArray_1__ctor_m3387351152_gshared (TwoDArray_1_t846454018 * __this, int32_t ___size0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___size0;
		__this->set_elements_0(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)L_0)));
		return;
	}
}
// System.Int32 GameDevToolkit.Common.TwoDArray`1<System.Object>::get_Length()
extern "C"  int32_t TwoDArray_1_get_Length_m549103542_gshared (TwoDArray_1_t846454018 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// T GameDevToolkit.Common.TwoDArray`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * TwoDArray_1_get_Item_m506725763_gshared (TwoDArray_1_t846454018 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDArray_1_get_Item_m506725763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	IndexOutOfRangeException_t3527622107 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = (Il2CppObject *)L_3;
		goto IL_0046;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0012;
		throw e;
	}

CATCH_0012:
	{ // begin catch(System.IndexOutOfRangeException)
		V_1 = (IndexOutOfRangeException_t3527622107 *)((IndexOutOfRangeException_t3527622107 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		IndexOutOfRangeException_t3527622107 * L_5 = V_1;
		NullCheck((Exception_t1927440687 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, (Exception_t1927440687 *)L_5);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)L_4;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral372029318);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral372029318);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		int32_t L_9 = ___index0;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029317);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_12, /*hidden argument*/NULL);
		IndexOutOfRangeException_t3527622107 * L_14 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_14, (String_t*)L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	} // end catch (depth: 1)

IL_0046:
	{
		Il2CppObject * L_15 = V_0;
		return L_15;
	}
}
// System.Void GameDevToolkit.Common.TwoDArray`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void TwoDArray_1_set_Item_m3261703054_gshared (TwoDArray_1_t846454018 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_elements_0();
		int32_t L_1 = ___index0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Il2CppObject *)L_2);
		return;
	}
}
// System.Collections.IEnumerator GameDevToolkit.Common.TwoDArray`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject * TwoDArray_1_GetEnumerator_m112666485_gshared (TwoDArray_1_t846454018 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t3326477674 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t3326477674 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3326477674 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3326477674 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3326477674 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t3326477674 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t3326477674 * L_2 = V_0;
		return L_2;
	}
}
// System.Void GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallbackDelegate_1__ctor_m1335553936_gshared (TweenCallbackDelegate_1_t3007645771 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<System.Object>::Invoke(T)
extern "C"  void TweenCallbackDelegate_1_Invoke_m686840820_gshared (TweenCallbackDelegate_1_t3007645771 * __this, Il2CppObject * ___argumeny0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallbackDelegate_1_Invoke_m686840820((TweenCallbackDelegate_1_t3007645771 *)__this->get_prev_9(),___argumeny0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___argumeny0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___argumeny0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___argumeny0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___argumeny0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___argumeny0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallbackDelegate_1_BeginInvoke_m2145768221_gshared (TweenCallbackDelegate_1_t3007645771 * __this, Il2CppObject * ___argumeny0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___argumeny0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void GameDevToolkit.Events.ArbitraryEvent/TweenCallbackDelegate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallbackDelegate_1_EndInvoke_m2517613686_gshared (TweenCallbackDelegate_1_t3007645771 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void MouseDelegate__ctor_m3715145187_gshared (MouseDelegate_t702691374 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<System.Object>::Invoke(T,GameDevToolkit.Events.MouseEventType)
extern "C"  void MouseDelegate_Invoke_m2233526212_gshared (MouseDelegate_t702691374 * __this, Il2CppObject * ___target0, int32_t ___type1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		MouseDelegate_Invoke_m2233526212((MouseDelegate_t702691374 *)__this->get_prev_9(),___target0, ___type1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___target0, int32_t ___type1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___target0, ___type1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___target0, int32_t ___type1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___target0, ___type1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___type1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___target0, ___type1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<System.Object>::BeginInvoke(T,GameDevToolkit.Events.MouseEventType,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MouseDelegate_BeginInvoke_m2803613529_gshared (MouseDelegate_t702691374 * __this, Il2CppObject * ___target0, int32_t ___type1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseDelegate_BeginInvoke_m2803613529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___target0;
	__d_args[1] = Box(MouseEventType_t2370363659_il2cpp_TypeInfo_var, &___type1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void MouseDelegate_EndInvoke_m1535332657_gshared (MouseDelegate_t702691374 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::.ctor()
extern "C"  void GenericMouseEvent_1__ctor_m3231803110_gshared (GenericMouseEvent_1_t2681505391 * __this, const MethodInfo* method)
{
	{
		__this->set__canProces_5((bool)1);
		NullCheck((MonoBehaviour_t1158329972 *)__this);
		MonoBehaviour__ctor_m2464341955((MonoBehaviour_t1158329972 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::add_MouseEvent(GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<T>)
extern "C"  void GenericMouseEvent_1_add_MouseEvent_m524070991_gshared (GenericMouseEvent_1_t2681505391 * __this, MouseDelegate_t702691374 * ___value0, const MethodInfo* method)
{
	MouseDelegate_t702691374 * V_0 = NULL;
	MouseDelegate_t702691374 * V_1 = NULL;
	{
		MouseDelegate_t702691374 * L_0 = (MouseDelegate_t702691374 *)__this->get_MouseEvent_3();
		V_0 = (MouseDelegate_t702691374 *)L_0;
	}

IL_0007:
	{
		MouseDelegate_t702691374 * L_1 = V_0;
		V_1 = (MouseDelegate_t702691374 *)L_1;
		MouseDelegate_t702691374 ** L_2 = (MouseDelegate_t702691374 **)__this->get_address_of_MouseEvent_3();
		MouseDelegate_t702691374 * L_3 = V_1;
		MouseDelegate_t702691374 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		MouseDelegate_t702691374 * L_6 = V_0;
		MouseDelegate_t702691374 * L_7 = InterlockedCompareExchangeImpl<MouseDelegate_t702691374 *>((MouseDelegate_t702691374 **)L_2, (MouseDelegate_t702691374 *)((MouseDelegate_t702691374 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), (MouseDelegate_t702691374 *)L_6);
		V_0 = (MouseDelegate_t702691374 *)L_7;
		MouseDelegate_t702691374 * L_8 = V_0;
		MouseDelegate_t702691374 * L_9 = V_1;
		if ((!(((Il2CppObject*)(MouseDelegate_t702691374 *)L_8) == ((Il2CppObject*)(MouseDelegate_t702691374 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::remove_MouseEvent(GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<T>)
extern "C"  void GenericMouseEvent_1_remove_MouseEvent_m1363838978_gshared (GenericMouseEvent_1_t2681505391 * __this, MouseDelegate_t702691374 * ___value0, const MethodInfo* method)
{
	MouseDelegate_t702691374 * V_0 = NULL;
	MouseDelegate_t702691374 * V_1 = NULL;
	{
		MouseDelegate_t702691374 * L_0 = (MouseDelegate_t702691374 *)__this->get_MouseEvent_3();
		V_0 = (MouseDelegate_t702691374 *)L_0;
	}

IL_0007:
	{
		MouseDelegate_t702691374 * L_1 = V_0;
		V_1 = (MouseDelegate_t702691374 *)L_1;
		MouseDelegate_t702691374 ** L_2 = (MouseDelegate_t702691374 **)__this->get_address_of_MouseEvent_3();
		MouseDelegate_t702691374 * L_3 = V_1;
		MouseDelegate_t702691374 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		MouseDelegate_t702691374 * L_6 = V_0;
		MouseDelegate_t702691374 * L_7 = InterlockedCompareExchangeImpl<MouseDelegate_t702691374 *>((MouseDelegate_t702691374 **)L_2, (MouseDelegate_t702691374 *)((MouseDelegate_t702691374 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), (MouseDelegate_t702691374 *)L_6);
		V_0 = (MouseDelegate_t702691374 *)L_7;
		MouseDelegate_t702691374 * L_8 = V_0;
		MouseDelegate_t702691374 * L_9 = V_1;
		if ((!(((Il2CppObject*)(MouseDelegate_t702691374 *)L_8) == ((Il2CppObject*)(MouseDelegate_t702691374 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Boolean GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::get_entered()
extern "C"  bool GenericMouseEvent_1_get_entered_m618322264_gshared (GenericMouseEvent_1_t2681505391 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get__entered_4();
		return L_0;
	}
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::set_entered(System.Boolean)
extern "C"  void GenericMouseEvent_1_set_entered_m4221807769_gshared (GenericMouseEvent_1_t2681505391 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set__entered_4(L_0);
		return;
	}
}
// System.Boolean GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::get_canProces()
extern "C"  bool GenericMouseEvent_1_get_canProces_m713581555_gshared (GenericMouseEvent_1_t2681505391 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get__canProces_5();
		return L_0;
	}
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::set_canProces(System.Boolean)
extern "C"  void GenericMouseEvent_1_set_canProces_m1151700034_gshared (GenericMouseEvent_1_t2681505391 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set__canProces_5(L_0);
		return;
	}
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::Awake()
extern "C"  void GenericMouseEvent_1_Awake_m819933243_gshared (GenericMouseEvent_1_t2681505391 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericMouseEvent_1_Awake_m819933243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MouseEventProccessor_t442073356 * L_0 = ((MouseEventProccessor_t442073356_StaticFields*)MouseEventProccessor_t442073356_il2cpp_TypeInfo_var->static_fields)->get_Instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		MouseEventProccessor_createInstance_m3819814644(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0015:
	{
		NullCheck((Component_t3819376471 *)__this);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_2);
		Collider2D_t646061738 * L_3 = GameObject_GetComponent_TisCollider2D_t646061738_m4291101372((GameObject_t1756533147 *)L_2, /*hidden argument*/GameObject_GetComponent_TisCollider2D_t646061738_m4291101372_MethodInfo_var);
		__this->set_spriteCollider_7(L_3);
		Collider2D_t646061738 * L_4 = (Collider2D_t646061738 *)__this->get_spriteCollider_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0056;
		}
	}
	{
		NullCheck((Component_t3819376471 *)__this);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		NullCheck((Object_t1021602117 *)L_6);
		String_t* L_7 = Object_get_name_m2079638459((Object_t1021602117 *)L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)_stringLiteral2125706819, (String_t*)L_7, (String_t*)_stringLiteral3540188832, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		Type_t * L_10 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GameObject_t1756533147_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_9) == ((Il2CppObject*)(Type_t *)L_10))))
		{
			goto IL_008f;
		}
	}
	{
		NullCheck((Component_t3819376471 *)__this);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		__this->set_actualTarget_8(((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_11, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		goto IL_00a0;
	}

IL_008f:
	{
		NullCheck((Component_t3819376471 *)__this);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_12);
		Il2CppObject * L_13 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((GameObject_t1756533147 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_actualTarget_8(L_13);
	}

IL_00a0:
	{
		return;
	}
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::dispatchMouse(GameDevToolkit.Events.MouseEventType)
extern "C"  void GenericMouseEvent_1_dispatchMouse_m1359806630_gshared (GenericMouseEvent_1_t2681505391 * __this, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericMouseEvent_1_dispatchMouse_m1359806630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MouseEventProccessor_t442073356 * L_0 = ((MouseEventProccessor_t442073356_StaticFields*)MouseEventProccessor_t442073356_il2cpp_TypeInfo_var->static_fields)->get_Instance_2();
		NullCheck((MouseEventProccessor_t442073356 *)L_0);
		bool L_1 = MouseEventProccessor_get_captureEvents_m2887403793((MouseEventProccessor_t442073356 *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((Behaviour_t955675639 *)__this);
		bool L_2 = Behaviour_get_enabled_m4079055610((Behaviour_t955675639 *)__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001b;
		}
	}

IL_001a:
	{
		return;
	}

IL_001b:
	{
		int32_t L_3 = ___type0;
		if (L_3)
		{
			goto IL_0077;
		}
	}
	{
		bool L_4 = (bool)__this->get_hasDoubleClick_6();
		if (!L_4)
		{
			goto IL_0077;
		}
	}
	{
		bool L_5 = (bool)__this->get_clickedOnce_10();
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		__this->set_clickedOnce_10((bool)1);
		float L_6 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastClick_9(L_6);
		goto IL_0077;
	}

IL_004e:
	{
		float L_7 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = (float)__this->get_lastClick_9();
		if ((!(((float)((float)((float)L_7-(float)L_8))) <= ((float)(0.5f)))))
		{
			goto IL_006c;
		}
	}
	{
		___type0 = (int32_t)1;
		goto IL_0077;
	}

IL_006c:
	{
		float L_9 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastClick_9(L_9);
	}

IL_0077:
	{
		MouseDelegate_t702691374 * L_10 = (MouseDelegate_t702691374 *)__this->get_MouseEvent_3();
		if (!L_10)
		{
			goto IL_00a5;
		}
	}
	{
		NullCheck((Component_t3819376471 *)__this);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_11, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00a5;
		}
	}
	{
		MouseDelegate_t702691374 * L_13 = (MouseDelegate_t702691374 *)__this->get_MouseEvent_3();
		Il2CppObject * L_14 = (Il2CppObject *)__this->get_actualTarget_8();
		int32_t L_15 = ___type0;
		NullCheck((MouseDelegate_t702691374 *)L_13);
		((  void (*) (MouseDelegate_t702691374 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((MouseDelegate_t702691374 *)L_13, (Il2CppObject *)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_00a5:
	{
		return;
	}
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::OnDestroy()
extern "C"  void GenericMouseEvent_1_OnDestroy_m1604701661_gshared (GenericMouseEvent_1_t2681505391 * __this, const MethodInfo* method)
{
	{
		__this->set__canProces_5((bool)0);
		return;
	}
}
// System.Void GameDevToolkit.Events.GenericMouseEvent`1<System.Object>::RemoveAllListeners()
extern "C"  void GenericMouseEvent_1_RemoveAllListeners_m1071691264_gshared (GenericMouseEvent_1_t2681505391 * __this, const MethodInfo* method)
{
	{
		__this->set_MouseEvent_3((MouseDelegate_t702691374 *)NULL);
		return;
	}
}
// System.Void GameDevToolkit.GameUtil.GridRepGenereic`2/<GetEnumerator>c__Iterator0<System.Object,System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m3035340939_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2492059455 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2/<GetEnumerator>c__Iterator0<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m758649697_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2492059455 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0081;
			}
		}
	}
	{
		goto IL_00ea;
	}

IL_0021:
	{
		__this->set_U3CiU3E__1_0(0);
		goto IL_00cb;
	}

IL_002d:
	{
		__this->set_U3CjU3E__2_1(0);
		goto IL_008f;
	}

IL_0039:
	{
		GridRepGenereic_2_t3548015612 * L_2 = (GridRepGenereic_2_t3548015612 *)__this->get_U24this_2();
		NullCheck(L_2);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_2->get_grid_2();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__1_0();
		NullCheck(L_3);
		int32_t L_5 = (int32_t)__this->get_U3CjU3E__2_1();
		NullCheck((TwoDArray_1_t846454018 *)(*((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))));
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (TwoDArray_1_t846454018 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((TwoDArray_1_t846454018 *)(*((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))), (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_U24current_3(L_6);
		bool L_7 = (bool)__this->get_U24disposing_4();
		if (L_7)
		{
			goto IL_007c;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_007c:
	{
		goto IL_00ec;
	}

IL_0081:
	{
		int32_t L_8 = (int32_t)__this->get_U3CjU3E__2_1();
		__this->set_U3CjU3E__2_1(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_008f:
	{
		int32_t L_9 = (int32_t)__this->get_U3CjU3E__2_1();
		GridRepGenereic_2_t3548015612 * L_10 = (GridRepGenereic_2_t3548015612 *)__this->get_U24this_2();
		NullCheck(L_10);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10->get_grid_2();
		int32_t L_12 = (int32_t)__this->get_U3CiU3E__1_0();
		NullCheck(L_11);
		NullCheck((TwoDArray_1_t846454018 *)(*((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))));
		int32_t L_13 = ((  int32_t (*) (TwoDArray_1_t846454018 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((TwoDArray_1_t846454018 *)(*((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		if ((((int32_t)L_9) < ((int32_t)L_13)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_14 = (int32_t)__this->get_U3CiU3E__1_0();
		__this->set_U3CiU3E__1_0(((int32_t)((int32_t)L_14+(int32_t)1)));
	}

IL_00cb:
	{
		int32_t L_15 = (int32_t)__this->get_U3CiU3E__1_0();
		GridRepGenereic_2_t3548015612 * L_16 = (GridRepGenereic_2_t3548015612 *)__this->get_U24this_2();
		NullCheck(L_16);
		ObjectU5BU5D_t3614634134* L_17 = (ObjectU5BU5D_t3614634134*)L_16->get_grid_2();
		NullCheck(L_17);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_5((-1));
	}

IL_00ea:
	{
		return (bool)0;
	}

IL_00ec:
	{
		return (bool)1;
	}
}
// System.Object GameDevToolkit.GameUtil.GridRepGenereic`2/<GetEnumerator>c__Iterator0<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m276439901_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2492059455 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Object GameDevToolkit.GameUtil.GridRepGenereic`2/<GetEnumerator>c__Iterator0<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3460824725_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2492059455 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Void GameDevToolkit.GameUtil.GridRepGenereic`2/<GetEnumerator>c__Iterator0<System.Object,System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2481371324_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2492059455 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void GameDevToolkit.GameUtil.GridRepGenereic`2/<GetEnumerator>c__Iterator0<System.Object,System.Object>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m705677362_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2492059455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m705677362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::.ctor()
extern "C"  void GridRepGenereic_2__ctor_m1555546541_gshared (GridRepGenereic_2_t3548015612 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t1158329972 *)__this);
		MonoBehaviour__ctor_m2464341955((MonoBehaviour_t1158329972 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::Awake()
extern "C"  void GridRepGenereic_2_Awake_m921158666_gshared (GridRepGenereic_2_t3548015612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_Awake_m921158666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = (bool)0;
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		NullCheck(L_1);
		if (!(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_001c:
	{
		bool L_2 = V_0;
		if (L_2)
		{
			goto IL_0140;
		}
	}
	{
		NullCheck((Component_t3819376471 *)__this);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_3);
		int32_t L_4 = Transform_get_childCount_m881385315((Transform_t3275118058 *)L_3, /*hidden argument*/NULL);
		V_1 = (int32_t)L_4;
		NullCheck((Component_t3819376471 *)__this);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_5);
		Transform_t3275118058 * L_6 = Transform_GetChild_m3838588184((Transform_t3275118058 *)L_5, (int32_t)0, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_6);
		int32_t L_7 = Transform_get_childCount_m881385315((Transform_t3275118058 *)L_6, /*hidden argument*/NULL);
		V_2 = (int32_t)L_7;
		int32_t L_8 = V_1;
		__this->set_grid_2(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)L_8)));
		V_3 = (int32_t)0;
		goto IL_0139;
	}

IL_0053:
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		int32_t L_10 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		int32_t L_13 = V_2;
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_15);
		Il2CppObject * L_16 = Activator_CreateInstance_m1465989661(NULL /*static, unused*/, (Type_t *)L_11, (ObjectU5BU5D_t3614634134*)L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))), IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
		V_4 = (int32_t)0;
		goto IL_012d;
	}

IL_008f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 3)), /*hidden argument*/NULL);
		Type_t * L_18 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GameObject_t1756533147_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18))))
		{
			goto IL_00ef;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_19 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t L_21 = V_4;
		NullCheck((Component_t3819376471 *)__this);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		NullCheck((Transform_t3275118058 *)L_22);
		Transform_t3275118058 * L_24 = Transform_GetChild_m3838588184((Transform_t3275118058 *)L_22, (int32_t)L_23, /*hidden argument*/NULL);
		int32_t L_25 = V_4;
		NullCheck((Transform_t3275118058 *)L_24);
		Transform_t3275118058 * L_26 = Transform_GetChild_m3838588184((Transform_t3275118058 *)L_24, (int32_t)L_25, /*hidden argument*/NULL);
		NullCheck((Component_t3819376471 *)L_26);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_26, /*hidden argument*/NULL);
		NullCheck((TwoDArray_1_t846454018 *)(*((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20)))));
		((  void (*) (TwoDArray_1_t846454018 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((TwoDArray_1_t846454018 *)(*((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20)))), (int32_t)L_21, (Il2CppObject *)((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_27, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0127;
	}

IL_00ef:
	{
		ObjectU5BU5D_t3614634134* L_28 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		int32_t L_29 = V_3;
		NullCheck(L_28);
		int32_t L_30 = V_4;
		NullCheck((Component_t3819376471 *)__this);
		Transform_t3275118058 * L_31 = Component_get_transform_m2697483695((Component_t3819376471 *)__this, /*hidden argument*/NULL);
		int32_t L_32 = V_3;
		NullCheck((Transform_t3275118058 *)L_31);
		Transform_t3275118058 * L_33 = Transform_GetChild_m3838588184((Transform_t3275118058 *)L_31, (int32_t)L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_4;
		NullCheck((Transform_t3275118058 *)L_33);
		Transform_t3275118058 * L_35 = Transform_GetChild_m3838588184((Transform_t3275118058 *)L_33, (int32_t)L_34, /*hidden argument*/NULL);
		NullCheck((Component_t3819376471 *)L_35);
		GameObject_t1756533147 * L_36 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_35, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_36);
		Il2CppObject * L_37 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((GameObject_t1756533147 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((TwoDArray_1_t846454018 *)(*((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29)))));
		((  void (*) (TwoDArray_1_t846454018 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((TwoDArray_1_t846454018 *)(*((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29)))), (int32_t)L_30, (Il2CppObject *)L_37, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0127:
	{
		int32_t L_38 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_012d:
	{
		int32_t L_39 = V_4;
		int32_t L_40 = V_2;
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_008f;
		}
	}
	{
		int32_t L_41 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_0139:
	{
		int32_t L_42 = V_3;
		int32_t L_43 = V_1;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0053;
		}
	}

IL_0140:
	{
		ObjectU5BU5D_t3614634134* L_44 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		NullCheck(L_44);
		Randomizer_t1796594675 * L_45 = Randomizer_CreateRandomizer_TisSemiRandomRule_t1463926147_m2503539335(NULL /*static, unused*/, (int32_t)1, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_44)->max_length)))), /*hidden argument*/Randomizer_CreateRandomizer_TisSemiRandomRule_t1463926147_m2503539335_MethodInfo_var);
		__this->set_rowRandom_3(L_45);
		ObjectU5BU5D_t3614634134* L_46 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		NullCheck(L_46);
		NullCheck((TwoDArray_1_t846454018 *)(*((L_46)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))));
		int32_t L_47 = ((  int32_t (*) (TwoDArray_1_t846454018 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((TwoDArray_1_t846454018 *)(*((L_46)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		Randomizer_t1796594675 * L_48 = Randomizer_CreateRandomizer_TisSemiRandomRule_t1463926147_m2503539335(NULL /*static, unused*/, (int32_t)1, (int32_t)L_47, /*hidden argument*/Randomizer_CreateRandomizer_TisSemiRandomRule_t1463926147_m2503539335_MethodInfo_var);
		__this->set_cellRandom_4(L_48);
		return;
	}
}
// GameDevToolkit.GameUtil.GridPoint GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getRandomPoint()
extern "C"  GridPoint_t3774252050 * GridRepGenereic_2_getRandomPoint_m3276798414_gshared (GridRepGenereic_2_t3548015612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getRandomPoint_m3276798414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Randomizer_t1796594675 * L_0 = (Randomizer_t1796594675 *)__this->get_rowRandom_3();
		NullCheck((Randomizer_t1796594675 *)L_0);
		int32_t L_1 = Randomizer_getRandom_m577675497((Randomizer_t1796594675 *)L_0, (bool)0, /*hidden argument*/NULL);
		Randomizer_t1796594675 * L_2 = (Randomizer_t1796594675 *)__this->get_cellRandom_4();
		NullCheck((Randomizer_t1796594675 *)L_2);
		int32_t L_3 = Randomizer_getRandom_m577675497((Randomizer_t1796594675 *)L_2, (bool)0, /*hidden argument*/NULL);
		GridPoint_t3774252050 * L_4 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_4, (int32_t)((int32_t)((int32_t)L_1-(int32_t)1)), (int32_t)((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::isNeighbor(GameDevToolkit.GameUtil.GridPoint,GameDevToolkit.GameUtil.GridPoint)
extern "C"  bool GridRepGenereic_2_isNeighbor_m1205732909_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___a0, GridPoint_t3774252050 * ___b1, const MethodInfo* method)
{
	GridPointU5BU5D_t1678401383* V_0 = NULL;
	GridPoint_t3774252050 * V_1 = NULL;
	GridPointU5BU5D_t1678401383* V_2 = NULL;
	int32_t V_3 = 0;
	{
		GridPoint_t3774252050 * L_0 = ___a0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPointU5BU5D_t1678401383* L_1 = ((  GridPointU5BU5D_t1678401383* (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_0, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (GridPointU5BU5D_t1678401383*)L_1;
		GridPointU5BU5D_t1678401383* L_2 = V_0;
		V_2 = (GridPointU5BU5D_t1678401383*)L_2;
		V_3 = (int32_t)0;
		goto IL_0028;
	}

IL_0012:
	{
		GridPointU5BU5D_t1678401383* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		GridPoint_t3774252050 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = (GridPoint_t3774252050 *)L_6;
		GridPoint_t3774252050 * L_7 = V_1;
		GridPoint_t3774252050 * L_8 = ___b1;
		bool L_9 = GridPoint_op_Equality_m3028292086(NULL /*static, unused*/, (GridPoint_t3774252050 *)L_7, (GridPoint_t3774252050 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0024;
		}
	}
	{
		return (bool)1;
	}

IL_0024:
	{
		int32_t L_10 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_11 = V_3;
		GridPointU5BU5D_t1678401383* L_12 = V_2;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0012;
		}
	}
	{
		return (bool)0;
	}
}
// GameDevToolkit.GameUtil.GridPoint[] GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getNeighbors(GameDevToolkit.GameUtil.GridPoint,System.Boolean)
extern "C"  GridPointU5BU5D_t1678401383* GridRepGenereic_2_getNeighbors_m2938468996_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___position0, bool ___includeDiagonal1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getNeighbors_m2938468996_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3143373182 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	List_1_t3143373182 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		bool L_0 = ___includeDiagonal1;
		if (L_0)
		{
			goto IL_008d;
		}
	}
	{
		List_1_t3143373182 * L_1 = (List_1_t3143373182 *)il2cpp_codegen_object_new(List_1_t3143373182_il2cpp_TypeInfo_var);
		List_1__ctor_m3541242976(L_1, /*hidden argument*/List_1__ctor_m3541242976_MethodInfo_var);
		V_0 = (List_1_t3143373182 *)L_1;
		GridPoint_t3774252050 * L_2 = ___position0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_x_0();
		V_1 = (int32_t)L_3;
		GridPoint_t3774252050 * L_4 = ___position0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_y_1();
		V_2 = (int32_t)L_5;
		GridPoint_t3774252050 * L_6 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_7 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		List_1_t3143373182 * L_8 = V_0;
		int32_t L_9 = V_1;
		int32_t L_10 = V_2;
		GridPoint_t3774252050 * L_11 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_11, (int32_t)((int32_t)((int32_t)L_9-(int32_t)1)), (int32_t)L_10, /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_8);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_8, (GridPoint_t3774252050 *)L_11, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
	}

IL_0035:
	{
		GridPoint_t3774252050 * L_12 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_13 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if (!L_13)
		{
			goto IL_0050;
		}
	}
	{
		List_1_t3143373182 * L_14 = V_0;
		int32_t L_15 = V_1;
		int32_t L_16 = V_2;
		GridPoint_t3774252050 * L_17 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_17, (int32_t)((int32_t)((int32_t)L_15+(int32_t)1)), (int32_t)L_16, /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_14);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_14, (GridPoint_t3774252050 *)L_17, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
	}

IL_0050:
	{
		GridPoint_t3774252050 * L_18 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_19 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_19)
		{
			goto IL_006b;
		}
	}
	{
		List_1_t3143373182 * L_20 = V_0;
		int32_t L_21 = V_1;
		int32_t L_22 = V_2;
		GridPoint_t3774252050 * L_23 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_23, (int32_t)L_21, (int32_t)((int32_t)((int32_t)L_22-(int32_t)1)), /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_20);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_20, (GridPoint_t3774252050 *)L_23, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
	}

IL_006b:
	{
		GridPoint_t3774252050 * L_24 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_25 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		if (!L_25)
		{
			goto IL_0086;
		}
	}
	{
		List_1_t3143373182 * L_26 = V_0;
		int32_t L_27 = V_1;
		int32_t L_28 = V_2;
		GridPoint_t3774252050 * L_29 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_29, (int32_t)L_27, (int32_t)((int32_t)((int32_t)L_28+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_26);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_26, (GridPoint_t3774252050 *)L_29, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
	}

IL_0086:
	{
		List_1_t3143373182 * L_30 = V_0;
		NullCheck((List_1_t3143373182 *)L_30);
		GridPointU5BU5D_t1678401383* L_31 = List_1_ToArray_m4211291750((List_1_t3143373182 *)L_30, /*hidden argument*/List_1_ToArray_m4211291750_MethodInfo_var);
		return L_31;
	}

IL_008d:
	{
		List_1_t3143373182 * L_32 = (List_1_t3143373182 *)il2cpp_codegen_object_new(List_1_t3143373182_il2cpp_TypeInfo_var);
		List_1__ctor_m3541242976(L_32, /*hidden argument*/List_1__ctor_m3541242976_MethodInfo_var);
		V_3 = (List_1_t3143373182 *)L_32;
		GridPoint_t3774252050 * L_33 = ___position0;
		NullCheck(L_33);
		int32_t L_34 = (int32_t)L_33->get_x_0();
		V_4 = (int32_t)L_34;
		GridPoint_t3774252050 * L_35 = ___position0;
		NullCheck(L_35);
		int32_t L_36 = (int32_t)L_35->get_y_1();
		V_5 = (int32_t)L_36;
		GridPoint_t3774252050 * L_37 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_38 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_37, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		if (!L_38)
		{
			goto IL_00fe;
		}
	}
	{
		List_1_t3143373182 * L_39 = V_3;
		int32_t L_40 = V_4;
		int32_t L_41 = V_5;
		GridPoint_t3774252050 * L_42 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_42, (int32_t)((int32_t)((int32_t)L_40-(int32_t)1)), (int32_t)L_41, /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_39);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_39, (GridPoint_t3774252050 *)L_42, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
		GridPoint_t3774252050 * L_43 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_44 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_43, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_44)
		{
			goto IL_00df;
		}
	}
	{
		List_1_t3143373182 * L_45 = V_3;
		int32_t L_46 = V_4;
		int32_t L_47 = V_5;
		GridPoint_t3774252050 * L_48 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_48, (int32_t)((int32_t)((int32_t)L_46-(int32_t)1)), (int32_t)((int32_t)((int32_t)L_47-(int32_t)1)), /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_45);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_45, (GridPoint_t3774252050 *)L_48, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
	}

IL_00df:
	{
		GridPoint_t3774252050 * L_49 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_50 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_49, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		if (!L_50)
		{
			goto IL_00fe;
		}
	}
	{
		List_1_t3143373182 * L_51 = V_3;
		int32_t L_52 = V_4;
		int32_t L_53 = V_5;
		GridPoint_t3774252050 * L_54 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_54, (int32_t)((int32_t)((int32_t)L_52-(int32_t)1)), (int32_t)((int32_t)((int32_t)L_53+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_51);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_51, (GridPoint_t3774252050 *)L_54, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
	}

IL_00fe:
	{
		GridPoint_t3774252050 * L_55 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_56 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_55, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if (!L_56)
		{
			goto IL_0159;
		}
	}
	{
		List_1_t3143373182 * L_57 = V_3;
		int32_t L_58 = V_4;
		int32_t L_59 = V_5;
		GridPoint_t3774252050 * L_60 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_60, (int32_t)((int32_t)((int32_t)L_58+(int32_t)1)), (int32_t)L_59, /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_57);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_57, (GridPoint_t3774252050 *)L_60, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
		GridPoint_t3774252050 * L_61 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_62 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_61, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_62)
		{
			goto IL_013a;
		}
	}
	{
		List_1_t3143373182 * L_63 = V_3;
		int32_t L_64 = V_4;
		int32_t L_65 = V_5;
		GridPoint_t3774252050 * L_66 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_66, (int32_t)((int32_t)((int32_t)L_64+(int32_t)1)), (int32_t)((int32_t)((int32_t)L_65-(int32_t)1)), /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_63);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_63, (GridPoint_t3774252050 *)L_66, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
	}

IL_013a:
	{
		GridPoint_t3774252050 * L_67 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_68 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_67, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		if (!L_68)
		{
			goto IL_0159;
		}
	}
	{
		List_1_t3143373182 * L_69 = V_3;
		int32_t L_70 = V_4;
		int32_t L_71 = V_5;
		GridPoint_t3774252050 * L_72 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_72, (int32_t)((int32_t)((int32_t)L_70+(int32_t)1)), (int32_t)((int32_t)((int32_t)L_71+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_69);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_69, (GridPoint_t3774252050 *)L_72, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
	}

IL_0159:
	{
		GridPoint_t3774252050 * L_73 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_74 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_73, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_74)
		{
			goto IL_0176;
		}
	}
	{
		List_1_t3143373182 * L_75 = V_3;
		int32_t L_76 = V_4;
		int32_t L_77 = V_5;
		GridPoint_t3774252050 * L_78 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_78, (int32_t)L_76, (int32_t)((int32_t)((int32_t)L_77-(int32_t)1)), /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_75);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_75, (GridPoint_t3774252050 *)L_78, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
	}

IL_0176:
	{
		GridPoint_t3774252050 * L_79 = ___position0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_80 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_79, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		if (!L_80)
		{
			goto IL_0193;
		}
	}
	{
		List_1_t3143373182 * L_81 = V_3;
		int32_t L_82 = V_4;
		int32_t L_83 = V_5;
		GridPoint_t3774252050 * L_84 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_84, (int32_t)L_82, (int32_t)((int32_t)((int32_t)L_83+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck((List_1_t3143373182 *)L_81);
		List_1_Add_m2362510564((List_1_t3143373182 *)L_81, (GridPoint_t3774252050 *)L_84, /*hidden argument*/List_1_Add_m2362510564_MethodInfo_var);
	}

IL_0193:
	{
		List_1_t3143373182 * L_85 = V_3;
		NullCheck((List_1_t3143373182 *)L_85);
		GridPointU5BU5D_t1678401383* L_86 = List_1_ToArray_m4211291750((List_1_t3143373182 *)L_85, /*hidden argument*/List_1_ToArray_m4211291750_MethodInfo_var);
		return L_86;
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::Exists(GameDevToolkit.GameUtil.GridPoint)
extern "C"  bool GridRepGenereic_2_Exists_m2628998418_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_x_0();
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		GridPoint_t3774252050 * L_2 = ___poz0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_y_1();
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		GridPoint_t3774252050 * L_4 = ___poz0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_x_0();
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		int32_t L_6 = ((  int32_t (*) (GridRepGenereic_2_t3548015612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		if ((((int32_t)L_5) > ((int32_t)((int32_t)((int32_t)L_6-(int32_t)1)))))
		{
			goto IL_0040;
		}
	}
	{
		GridPoint_t3774252050 * L_7 = ___poz0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get_y_1();
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		int32_t L_9 = ((  int32_t (*) (GridRepGenereic_2_t3548015612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		G_B5_0 = ((((int32_t)((((int32_t)L_8) > ((int32_t)((int32_t)((int32_t)L_9-(int32_t)1))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0041;
	}

IL_0040:
	{
		G_B5_0 = 0;
	}

IL_0041:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::Exists(T1)
extern "C"  bool GridRepGenereic_2_Exists_m3379732834_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_Exists_m3379732834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		V_1 = (Il2CppObject *)L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_000c:
		{
			Il2CppObject * L_1 = V_1;
			NullCheck((Il2CppObject *)L_1);
			Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
			V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4)));
			Il2CppObject * L_3 = V_0;
			Il2CppObject * L_4 = ___cell0;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_3, (Object_t1021602117 *)L_4, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_0035;
			}
		}

IL_002e:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x5B, FINALLY_0045);
		}

IL_0035:
		{
			Il2CppObject * L_6 = V_1;
			NullCheck((Il2CppObject *)L_6);
			bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_6);
			if (L_7)
			{
				goto IL_000c;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x59, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_8 = V_1;
			Il2CppObject * L_9 = (Il2CppObject *)((Il2CppObject *)IsInst(L_8, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_3 = (Il2CppObject *)L_9;
			if (!L_9)
			{
				goto IL_0058;
			}
		}

IL_0052:
		{
			Il2CppObject * L_10 = V_3;
			NullCheck((Il2CppObject *)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
		}

IL_0058:
		{
			IL2CPP_END_FINALLY(69)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0059:
	{
		return (bool)0;
	}

IL_005b:
	{
		bool L_11 = V_2;
		return L_11;
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::hasAbove(T1)
extern "C"  bool GridRepGenereic_2_hasAbove_m3548441245_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_hasAbove_m3548441245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_1 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		Exception_t1927440687 * L_2 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_2, (String_t*)_stringLiteral3486192891, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		Il2CppObject * L_3 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_4 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_5 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_5;
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::hasBelow(T1)
extern "C"  bool GridRepGenereic_2_hasBelow_m3087394133_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_hasBelow_m3087394133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_1 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		Exception_t1927440687 * L_2 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_2, (String_t*)_stringLiteral3486192891, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		Il2CppObject * L_3 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_4 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_5 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_5;
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::hasToLeft(T1)
extern "C"  bool GridRepGenereic_2_hasToLeft_m2021908934_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_hasToLeft_m2021908934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_1 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		Exception_t1927440687 * L_2 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_2, (String_t*)_stringLiteral3486192891, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		Il2CppObject * L_3 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_4 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_5 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_5;
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::hasToRight(T1)
extern "C"  bool GridRepGenereic_2_hasToRight_m1771079277_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_hasToRight_m1771079277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_1 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		Exception_t1927440687 * L_2 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_2, (String_t*)_stringLiteral3486192891, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		Il2CppObject * L_3 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_4 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		bool L_5 = ((  bool (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_5;
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::hasAbove(GameDevToolkit.GameUtil.GridPoint)
extern "C"  bool GridRepGenereic_2_hasAbove_m2925845237_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_x_0();
		return (bool)((((int32_t)L_1) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::hasBelow(GameDevToolkit.GameUtil.GridPoint)
extern "C"  bool GridRepGenereic_2_hasBelow_m1438600261_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_x_0();
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		int32_t L_2 = ((  int32_t (*) (GridRepGenereic_2_t3548015612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return (bool)((((int32_t)L_1) < ((int32_t)((int32_t)((int32_t)L_2-(int32_t)1))))? 1 : 0);
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::hasToLeft(GameDevToolkit.GameUtil.GridPoint)
extern "C"  bool GridRepGenereic_2_hasToLeft_m3281514_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_y_1();
		return (bool)((((int32_t)L_1) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::hasToRight(GameDevToolkit.GameUtil.GridPoint)
extern "C"  bool GridRepGenereic_2_hasToRight_m1869723861_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_y_1();
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		int32_t L_2 = ((  int32_t (*) (GridRepGenereic_2_t3548015612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return (bool)((((int32_t)L_1) < ((int32_t)((int32_t)((int32_t)L_2-(int32_t)1))))? 1 : 0);
	}
}
// GameDevToolkit.GameUtil.GridPoint GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getPositionAbove(T1)
extern "C"  GridPoint_t3774252050 * GridRepGenereic_2_getPositionAbove_m1340081224_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getPositionAbove_m1340081224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GridPoint_t3774252050 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (GridPoint_t3774252050 *)L_1;
		GridPoint_t3774252050 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_x_0();
		GridPoint_t3774252050 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_y_1();
		GridPoint_t3774252050 * L_6 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_6, (int32_t)((int32_t)((int32_t)L_3-(int32_t)1)), (int32_t)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// GameDevToolkit.GameUtil.GridPoint GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getPositionBelow(T1)
extern "C"  GridPoint_t3774252050 * GridRepGenereic_2_getPositionBelow_m3753575844_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getPositionBelow_m3753575844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GridPoint_t3774252050 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (GridPoint_t3774252050 *)L_1;
		GridPoint_t3774252050 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_x_0();
		GridPoint_t3774252050 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_y_1();
		GridPoint_t3774252050 * L_6 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_6, (int32_t)((int32_t)((int32_t)L_3+(int32_t)1)), (int32_t)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// GameDevToolkit.GameUtil.GridPoint GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getPositionToLeft(T1)
extern "C"  GridPoint_t3774252050 * GridRepGenereic_2_getPositionToLeft_m1630567973_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getPositionToLeft_m1630567973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GridPoint_t3774252050 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (GridPoint_t3774252050 *)L_1;
		GridPoint_t3774252050 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_x_0();
		GridPoint_t3774252050 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_y_1();
		GridPoint_t3774252050 * L_6 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_6, (int32_t)L_3, (int32_t)((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		return L_6;
	}
}
// GameDevToolkit.GameUtil.GridPoint GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getPositionToRight(T1)
extern "C"  GridPoint_t3774252050 * GridRepGenereic_2_getPositionToRight_m4017048604_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getPositionToRight_m4017048604_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GridPoint_t3774252050 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (GridPoint_t3774252050 *)L_1;
		GridPoint_t3774252050 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_x_0();
		GridPoint_t3774252050 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_y_1();
		GridPoint_t3774252050 * L_6 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_6, (int32_t)L_3, (int32_t)((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/NULL);
		return L_6;
	}
}
// GameDevToolkit.GameUtil.GridPoint GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getPositionAbove(GameDevToolkit.GameUtil.GridPoint)
extern "C"  GridPoint_t3774252050 * GridRepGenereic_2_getPositionAbove_m2782846948_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getPositionAbove_m2782846948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_x_0();
		GridPoint_t3774252050 * L_2 = ___poz0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_y_1();
		GridPoint_t3774252050 * L_4 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_4, (int32_t)((int32_t)((int32_t)L_1-(int32_t)1)), (int32_t)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// GameDevToolkit.GameUtil.GridPoint GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getPositionBelow(GameDevToolkit.GameUtil.GridPoint)
extern "C"  GridPoint_t3774252050 * GridRepGenereic_2_getPositionBelow_m3728707952_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getPositionBelow_m3728707952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_x_0();
		GridPoint_t3774252050 * L_2 = ___poz0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_y_1();
		GridPoint_t3774252050 * L_4 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_4, (int32_t)((int32_t)((int32_t)L_1+(int32_t)1)), (int32_t)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// GameDevToolkit.GameUtil.GridPoint GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getPositionToLeft(GameDevToolkit.GameUtil.GridPoint)
extern "C"  GridPoint_t3774252050 * GridRepGenereic_2_getPositionToLeft_m3820077597_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getPositionToLeft_m3820077597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_x_0();
		GridPoint_t3774252050 * L_2 = ___poz0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_y_1();
		GridPoint_t3774252050 * L_4 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_4, (int32_t)L_1, (int32_t)((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/NULL);
		return L_4;
	}
}
// GameDevToolkit.GameUtil.GridPoint GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getPositionToRight(GameDevToolkit.GameUtil.GridPoint)
extern "C"  GridPoint_t3774252050 * GridRepGenereic_2_getPositionToRight_m4219751296_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getPositionToRight_m4219751296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_x_0();
		GridPoint_t3774252050 * L_2 = ___poz0;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_y_1();
		GridPoint_t3774252050 * L_4 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_4, (int32_t)L_1, (int32_t)((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/NULL);
		return L_4;
	}
}
// T1 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getCell(GameDevToolkit.GameUtil.GridPoint)
extern "C"  Il2CppObject * GridRepGenereic_2_getCell_m3595911306_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getCell_m3595911306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_x_0();
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		V_0 = (Il2CppObject *)L_2;
		GridPoint_t3774252050 * L_3 = ___poz0;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)L_3->get_y_1();
		NullCheck((TwoDArray_1_t846454018 *)(*(&V_0)));
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (TwoDArray_1_t846454018 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((TwoDArray_1_t846454018 *)(*(&V_0)), (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_1 = (Il2CppObject *)L_5;
		goto IL_0033;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0026;
		throw e;
	}

CATCH_0026:
	{ // begin catch(System.IndexOutOfRangeException)
		V_1 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4)));
		goto IL_0033;
	} // end catch (depth: 1)

IL_0033:
	{
		Il2CppObject * L_6 = V_1;
		return L_6;
	}
}
// T1[] GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getCells(System.Collections.Generic.List`1<GameDevToolkit.GameUtil.GridPoint>)
extern "C"  ObjectU5BU5D_t3614634134* GridRepGenereic_2_getCells_m4142893693_gshared (GridRepGenereic_2_t3548015612 * __this, List_1_t3143373182 * ___positions0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getCells_m4142893693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3143373182 * L_0 = ___positions0;
		NullCheck((List_1_t3143373182 *)L_0);
		GridPointU5BU5D_t1678401383* L_1 = List_1_ToArray_m4211291750((List_1_t3143373182 *)L_0, /*hidden argument*/List_1_ToArray_m4211291750_MethodInfo_var);
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		ObjectU5BU5D_t3614634134* L_2 = ((  ObjectU5BU5D_t3614634134* (*) (GridRepGenereic_2_t3548015612 *, GridPointU5BU5D_t1678401383*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPointU5BU5D_t1678401383*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		return L_2;
	}
}
// T1[] GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getCells(GameDevToolkit.GameUtil.GridPoint[])
extern "C"  ObjectU5BU5D_t3614634134* GridRepGenereic_2_getCells_m39536153_gshared (GridRepGenereic_2_t3548015612 * __this, GridPointU5BU5D_t1678401383* ___positions0, const MethodInfo* method)
{
	List_1_t2058570427 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21));
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		V_0 = (List_1_t2058570427 *)L_0;
		V_1 = (int32_t)0;
		goto IL_0020;
	}

IL_000d:
	{
		List_1_t2058570427 * L_1 = V_0;
		GridPointU5BU5D_t1678401383* L_2 = ___positions0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GridPoint_t3774252050 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		NullCheck((List_1_t2058570427 *)L_1);
		((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t2058570427 *)L_1, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_8 = V_1;
		GridPointU5BU5D_t1678401383* L_9 = ___positions0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		List_1_t2058570427 * L_10 = V_0;
		NullCheck((List_1_t2058570427 *)L_10);
		ObjectU5BU5D_t3614634134* L_11 = ((  ObjectU5BU5D_t3614634134* (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t2058570427 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		return L_11;
	}
}
// T1 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getAbove(GameDevToolkit.GameUtil.GridPoint)
extern "C"  Il2CppObject * GridRepGenereic_2_getAbove_m2487861599_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_2;
	}
}
// T1 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getBelow(GameDevToolkit.GameUtil.GridPoint)
extern "C"  Il2CppObject * GridRepGenereic_2_getBelow_m2841267987_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_2;
	}
}
// T1 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getToLeft(GameDevToolkit.GameUtil.GridPoint)
extern "C"  Il2CppObject * GridRepGenereic_2_getToLeft_m2931021330_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_2;
	}
}
// T1 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getToRight(GameDevToolkit.GameUtil.GridPoint)
extern "C"  Il2CppObject * GridRepGenereic_2_getToRight_m2674791883_gshared (GridRepGenereic_2_t3548015612 * __this, GridPoint_t3774252050 * ___poz0, const MethodInfo* method)
{
	{
		GridPoint_t3774252050 * L_0 = ___poz0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_2;
	}
}
// T1 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getAbove(T1)
extern "C"  Il2CppObject * GridRepGenereic_2_getAbove_m908035171_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_2;
	}
}
// T1 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getBelow(T1)
extern "C"  Il2CppObject * GridRepGenereic_2_getBelow_m2262330151_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_2;
	}
}
// T1 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getToLeft(T1)
extern "C"  Il2CppObject * GridRepGenereic_2_getToLeft_m3615451610_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_2;
	}
}
// T1 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getToRight(T1)
extern "C"  Il2CppObject * GridRepGenereic_2_getToRight_m282286983_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___cell0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___cell0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		GridPoint_t3774252050 * L_1 = ((  GridPoint_t3774252050 * (*) (GridRepGenereic_2_t3548015612 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (GridRepGenereic_2_t3548015612 *, GridPoint_t3774252050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, (GridPoint_t3774252050 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_2;
	}
}
// GameDevToolkit.GameUtil.GridPoint GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::getPositionOfCell(T1)
extern "C"  GridPoint_t3774252050 * GridRepGenereic_2_getPositionOfCell_m4074657698_gshared (GridRepGenereic_2_t3548015612 * __this, Il2CppObject * ___child0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridRepGenereic_2_getPositionOfCell_m4074657698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_0059;
	}

IL_0007:
	{
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_000e:
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = V_1;
		NullCheck((TwoDArray_1_t846454018 *)(*((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))));
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (TwoDArray_1_t846454018 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((TwoDArray_1_t846454018 *)(*((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))), (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		Il2CppObject * L_4 = ___child0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_3, (Object_t1021602117 *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		GridPoint_t3774252050 * L_8 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_8, (int32_t)L_6, (int32_t)L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0045:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		int32_t L_11 = ((  int32_t (*) (GridRepGenereic_2_t3548015612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0059:
	{
		int32_t L_13 = V_0;
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		int32_t L_14 = ((  int32_t (*) (GridRepGenereic_2_t3548015612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		GridPoint_t3774252050 * L_15 = (GridPoint_t3774252050 *)il2cpp_codegen_object_new(GridPoint_t3774252050_il2cpp_TypeInfo_var);
		GridPoint__ctor_m3345851570(L_15, (int32_t)(-1), (int32_t)(-1), /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Int32 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::get_rows()
extern "C"  int32_t GridRepGenereic_2_get_rows_m2956594107_gshared (GridRepGenereic_2_t3548015612 * __this, const MethodInfo* method)
{
	{
		NullCheck((GridRepGenereic_2_t3548015612 *)__this);
		int32_t L_0 = ((  int32_t (*) (GridRepGenereic_2_t3548015612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)((GridRepGenereic_2_t3548015612 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		return L_0;
	}
}
// System.Int32 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::get_cols()
extern "C"  int32_t GridRepGenereic_2_get_cols_m2109429161_gshared (GridRepGenereic_2_t3548015612 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		NullCheck(L_0);
		NullCheck((TwoDArray_1_t846454018 *)(*((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))));
		int32_t L_1 = ((  int32_t (*) (TwoDArray_1_t846454018 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((TwoDArray_1_t846454018 *)(*((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_1;
	}
}
// System.Int32 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::get_Length()
extern "C"  int32_t GridRepGenereic_2_get_Length_m631943666_gshared (GridRepGenereic_2_t3548015612 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// T2 GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * GridRepGenereic_2_get_Item_m125297765_gshared (GridRepGenereic_2_t3548015612 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::set_Item(System.Int32,T2)
extern "C"  void GridRepGenereic_2_set_Item_m1252587698_gshared (GridRepGenereic_2_t3548015612 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_grid_2();
		int32_t L_1 = ___index0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Il2CppObject *)L_2);
		return;
	}
}
// System.Collections.IEnumerator GameDevToolkit.GameUtil.GridRepGenereic`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject * GridRepGenereic_2_GetEnumerator_m2764845457_gshared (GridRepGenereic_2_t3548015612 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2492059455 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2492059455 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2492059455 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2492059455 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2492059455 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2492059455 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2492059455 * L_2 = V_0;
		return L_2;
	}
}
// System.Void SingletonBase`1<System.Object>::.ctor()
extern "C"  void SingletonBase_1__ctor_m1377611544_gshared (SingletonBase_1_t1688964385 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t1158329972 *)__this);
		MonoBehaviour__ctor_m2464341955((MonoBehaviour_t1158329972 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Camera SingletonBase`1<System.Object>::get_MainCamera()
extern "C"  Camera_t189460977 * SingletonBase_1_get_MainCamera_m1659347230_gshared (SingletonBase_1_t1688964385 * __this, const MethodInfo* method)
{
	{
		Camera_t189460977 * L_0 = (Camera_t189460977 *)__this->get_U3CMainCameraU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void SingletonBase`1<System.Object>::set_MainCamera(UnityEngine.Camera)
extern "C"  void SingletonBase_1_set_MainCamera_m896354473_gshared (SingletonBase_1_t1688964385 * __this, Camera_t189460977 * ___value0, const MethodInfo* method)
{
	{
		Camera_t189460977 * L_0 = ___value0;
		__this->set_U3CMainCameraU3Ek__BackingField_3(L_0);
		return;
	}
}
// T SingletonBase`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * SingletonBase_1_get_Instance_m2217304019_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SingletonBase_1_get_Instance_m2217304019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ((SingletonBase_1_t1688964385_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((SingletonBase_1_t1688964385_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_instance_2(L_2);
	}

IL_001f:
	{
		Il2CppObject * L_3 = ((SingletonBase_1_t1688964385_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_instance_2();
		return L_3;
	}
}
// System.Void SingletonBase`1<System.Object>::Awake()
extern "C"  void SingletonBase_1_Awake_m746335005_gshared (SingletonBase_1_t1688964385 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SingletonBase_1_Awake_m746335005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, (Object_t1021602117 *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((SingletonBase_1_t1688964385 *)__this);
		((  void (*) (SingletonBase_1_t1688964385 *, Camera_t189460977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((SingletonBase_1_t1688964385 *)__this, (Camera_t189460977 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_001a:
	{
		Il2CppObject * L_3 = ((SingletonBase_1_t1688964385_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 4)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)__this, (Type_t *)L_5, /*hidden argument*/NULL);
		((SingletonBase_1_t1688964385_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->set_instance_2(((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
	}

IL_004e:
	{
		return;
	}
}
// System.Void SingletonBase`1<System.Object>::OnDestroy()
extern "C"  void SingletonBase_1_OnDestroy_m3734678359_gshared (SingletonBase_1_t1688964385 * __this, const MethodInfo* method)
{
	{
		((SingletonBase_1_t1688964385_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->set_instance_2(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void System.Action`1<GameTwentyoneManager/GiftObject>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1775910342_gshared (Action_1_t1946475162 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GameTwentyoneManager/GiftObject>::Invoke(T)
extern "C"  void Action_1_Invoke_m3862957638_gshared (Action_1_t1946475162 * __this, GiftObject_t2144675780  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3862957638((Action_1_t1946475162 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, GiftObject_t2144675780  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, GiftObject_t2144675780  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GameTwentyoneManager/GiftObject>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m292367995_gshared (Action_1_t1946475162 * __this, GiftObject_t2144675780  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m292367995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(GiftObject_t2144675780_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GameTwentyoneManager/GiftObject>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4292541024_gshared (Action_1_t1946475162 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3072925129_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3662000152((Action_1_t3627374100 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m226849422_gshared (Action_1_t3627374100 * __this, bool ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m226849422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2990292511_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Char>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m32077197_gshared (Action_1_t3256280720 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Char>::Invoke(T)
extern "C"  void Action_1_Invoke_m3702739261_gshared (Action_1_t3256280720 * __this, Il2CppChar ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3702739261((Action_1_t3256280720 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppChar ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppChar ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1592224130_gshared (Action_1_t3256280720 * __this, Il2CppChar ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1592224130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Char_t3454481338_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Char>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3307217879_gshared (Action_1_t3256280720 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m514039655_gshared (Action_1_t1873676830 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Int32>::Invoke(T)
extern "C"  void Action_1_Invoke_m2055228803_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2055228803((Action_1_t1873676830 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1720726178_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1720726178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3128406917_gshared (Action_1_t1873676830 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m584977596_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m4180501989_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m4180501989((Action_1_t2491248677 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1305519803_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2057605070_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3609901643_gshared (Action_1_t4190924221 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  void Action_1_Invoke_m3377187479_gshared (Action_1_t4190924221 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3377187479((Action_1_t4190924221 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2361014002_gshared (Action_1_t4190924221 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2361014002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3555714869_gshared (Action_1_t4190924221 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m592790842_gshared (Action_1_t1299997296 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  void Action_1_Invoke_m3721769778_gshared (Action_1_t1299997296 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3721769778((Action_1_t1299997296 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1386903007_gshared (Action_1_t1299997296 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1386903007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m350311208_gshared (Action_1_t1299997296 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.AnimatorClipInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m928429764_gshared (Action_1_t3707550731 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.AnimatorClipInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m648331504_gshared (Action_1_t3707550731 * __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m648331504((Action_1_t3707550731 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.AnimatorClipInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2796970985_gshared (Action_1_t3707550731 * __this, AnimatorClipInfo_t3905751349  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2796970985_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AnimatorClipInfo_t3905751349_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.AnimatorClipInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m445936302_gshared (Action_1_t3707550731 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m74371284_gshared (Action_1_t1822191457 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Color>::Invoke(T)
extern "C"  void Action_1_Invoke_m3422995740_gshared (Action_1_t1822191457 * __this, Color_t2020392075  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3422995740((Action_1_t1822191457 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2020392075  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1002727263_gshared (Action_1_t1822191457 * __this, Color_t2020392075  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1002727263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m5961818_gshared (Action_1_t1822191457 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m984921673_gshared (Action_1_t676316900 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Color32>::Invoke(T)
extern "C"  void Action_1_Invoke_m3852022121_gshared (Action_1_t676316900 * __this, Color32_t874517518  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3852022121((Action_1_t676316900 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Color32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m670751528_gshared (Action_1_t676316900 * __this, Color32_t874517518  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m670751528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_t874517518_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3798228067_gshared (Action_1_t676316900 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3415321671_gshared (Action_1_t4117953054 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  void Action_1_Invoke_m2664461947_gshared (Action_1_t4117953054 * __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2664461947((Action_1_t4117953054 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m910318220_gshared (Action_1_t4117953054 * __this, RaycastResult_t21186376  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m910318220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastResult_t21186376_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m857894033_gshared (Action_1_t4117953054 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.KeyCode>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2397509699_gshared (Action_1_t2085194534 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.KeyCode>::Invoke(T)
extern "C"  void Action_1_Invoke_m4078720527_gshared (Action_1_t2085194534 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m4078720527((Action_1_t2085194534 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.KeyCode>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2052621508_gshared (Action_1_t2085194534 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2052621508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyCode_t2283395152_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.KeyCode>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m843330993_gshared (Action_1_t2085194534 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3865220215_gshared (Action_1_t2858436182 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m433096323_gshared (Action_1_t2858436182 * __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m433096323((Action_1_t2858436182 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3589829392_gshared (Action_1_t2858436182 * __this, UICharInfo_t3056636800  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3589829392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UICharInfo_t3056636800_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1787496237_gshared (Action_1_t2858436182 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m933377513_gshared (Action_1_t3423077256 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m1440002041_gshared (Action_1_t3423077256 * __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1440002041((Action_1_t3423077256 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1314092322_gshared (Action_1_t3423077256 * __this, UILineInfo_t3621277874  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1314092322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UILineInfo_t3621277874_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2124810843_gshared (Action_1_t3423077256 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m722292825_gshared (Action_1_t1006058200 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  void Action_1_Invoke_m2226099137_gshared (Action_1_t1006058200 * __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2226099137((Action_1_t1006058200 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3450966034_gshared (Action_1_t1006058200 * __this, UIVertex_t1204258818  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3450966034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIVertex_t1204258818_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1826856235_gshared (Action_1_t1006058200 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2178801012_gshared (Action_1_t2045506961 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void Action_1_Invoke_m3472061792_gshared (Action_1_t2045506961 * __this, Vector2_t2243707579  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3472061792((Action_1_t2045506961 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m493747221_gshared (Action_1_t2045506961 * __this, Vector2_t2243707579  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m493747221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1586893730_gshared (Action_1_t2045506961 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1033856207_gshared (Action_1_t2045506962 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void Action_1_Invoke_m212965499_gshared (Action_1_t2045506962 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m212965499((Action_1_t2045506962 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m402802490_gshared (Action_1_t2045506962 * __this, Vector3_t2243707580  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m402802490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4206793277_gshared (Action_1_t2045506962 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m858813886_gshared (Action_1_t2045506963 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void Action_1_Invoke_m3127338686_gshared (Action_1_t2045506963 * __this, Vector4_t2243707581  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3127338686((Action_1_t2045506963 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1860947955_gshared (Action_1_t2045506963 * __this, Vector4_t2243707581  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1860947955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m11252568_gshared (Action_1_t2045506963 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m946854823_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m352317182_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m352317182((Action_2_t2525452034 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3907381723_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3907381723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2798191693_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3362391082_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1501152969_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1501152969((Action_2_t2572051853 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1914861552_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3956733788_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1015489335_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1791706206_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2580780957_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2489948797_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0055;
			}
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t2471096271 * L_2 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t2471096271 * L_9 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1859988746_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1942816078_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m285299945_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m480171694_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t94157543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m949306872_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0055;
			}
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t4170771815 * L_2 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t4170771815 * L_9 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_10 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2403602883_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m409316647_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m988222504_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2332089385_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t1498197914  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m692741405_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0055;
			}
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1279844890 * L_2 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1279844890 * L_9 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_10 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2201090542_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m2430810679_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2780765696_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t2471096271 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t2471096271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t2471096271 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m176001975_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m314687476_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m962317777_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2717922212_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3970067462_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2539474626_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1266627404_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m816115094_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1078352793_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1537228832_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m1136669199_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m1875216835_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2701218731_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2289309720_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m691892240_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3039869667_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t4170771815 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t4170771815 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t4170771815 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t94157543  ArrayReadOnlyList_1_get_Item_m2694472846_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3536854615_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2661355086_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2189922207_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m961024239_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1565299387_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1269788217_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m4003949395_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m634288642_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1220844927_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2938723476_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m2325516426_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m4104441984_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2160816107_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
