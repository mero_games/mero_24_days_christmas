﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen4198239782.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// BackgroundsHolder[]
struct BackgroundsHolderU5BU5D_t1836318050;
// StartingSlot[]
struct StartingSlotU5BU5D_t820505645;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEightManager
struct  GameEightManager_t903757396  : public SingletonBase_1_t4198239782
{
public:
	// System.Int32 GameEightManager::counter2by2
	int32_t ___counter2by2_4;
	// System.Int32 GameEightManager::counter2by3
	int32_t ___counter2by3_5;
	// System.Int32 GameEightManager::counter3by3
	int32_t ___counter3by3_6;
	// UnityEngine.GameObject GameEightManager::grid2by2
	GameObject_t1756533147 * ___grid2by2_7;
	// UnityEngine.GameObject GameEightManager::grid2by3
	GameObject_t1756533147 * ___grid2by3_8;
	// UnityEngine.GameObject GameEightManager::grid3by3
	GameObject_t1756533147 * ___grid3by3_9;
	// UnityEngine.UI.Image GameEightManager::background
	Image_t2042527209 * ___background_10;
	// UnityEngine.Transform[] GameEightManager::puzzlePieces3by3
	TransformU5BU5D_t3764228911* ___puzzlePieces3by3_11;
	// UnityEngine.Transform[] GameEightManager::puzzlePieces2by3
	TransformU5BU5D_t3764228911* ___puzzlePieces2by3_12;
	// UnityEngine.Transform[] GameEightManager::puzzlePieces2by2
	TransformU5BU5D_t3764228911* ___puzzlePieces2by2_13;
	// BackgroundsHolder[] GameEightManager::backgrounds2by2
	BackgroundsHolderU5BU5D_t1836318050* ___backgrounds2by2_14;
	// BackgroundsHolder[] GameEightManager::backgrounds2by3
	BackgroundsHolderU5BU5D_t1836318050* ___backgrounds2by3_15;
	// BackgroundsHolder[] GameEightManager::backgrounds3by3
	BackgroundsHolderU5BU5D_t1836318050* ___backgrounds3by3_16;
	// StartingSlot[] GameEightManager::startingSlots
	StartingSlotU5BU5D_t820505645* ___startingSlots_17;

public:
	inline static int32_t get_offset_of_counter2by2_4() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___counter2by2_4)); }
	inline int32_t get_counter2by2_4() const { return ___counter2by2_4; }
	inline int32_t* get_address_of_counter2by2_4() { return &___counter2by2_4; }
	inline void set_counter2by2_4(int32_t value)
	{
		___counter2by2_4 = value;
	}

	inline static int32_t get_offset_of_counter2by3_5() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___counter2by3_5)); }
	inline int32_t get_counter2by3_5() const { return ___counter2by3_5; }
	inline int32_t* get_address_of_counter2by3_5() { return &___counter2by3_5; }
	inline void set_counter2by3_5(int32_t value)
	{
		___counter2by3_5 = value;
	}

	inline static int32_t get_offset_of_counter3by3_6() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___counter3by3_6)); }
	inline int32_t get_counter3by3_6() const { return ___counter3by3_6; }
	inline int32_t* get_address_of_counter3by3_6() { return &___counter3by3_6; }
	inline void set_counter3by3_6(int32_t value)
	{
		___counter3by3_6 = value;
	}

	inline static int32_t get_offset_of_grid2by2_7() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___grid2by2_7)); }
	inline GameObject_t1756533147 * get_grid2by2_7() const { return ___grid2by2_7; }
	inline GameObject_t1756533147 ** get_address_of_grid2by2_7() { return &___grid2by2_7; }
	inline void set_grid2by2_7(GameObject_t1756533147 * value)
	{
		___grid2by2_7 = value;
		Il2CppCodeGenWriteBarrier(&___grid2by2_7, value);
	}

	inline static int32_t get_offset_of_grid2by3_8() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___grid2by3_8)); }
	inline GameObject_t1756533147 * get_grid2by3_8() const { return ___grid2by3_8; }
	inline GameObject_t1756533147 ** get_address_of_grid2by3_8() { return &___grid2by3_8; }
	inline void set_grid2by3_8(GameObject_t1756533147 * value)
	{
		___grid2by3_8 = value;
		Il2CppCodeGenWriteBarrier(&___grid2by3_8, value);
	}

	inline static int32_t get_offset_of_grid3by3_9() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___grid3by3_9)); }
	inline GameObject_t1756533147 * get_grid3by3_9() const { return ___grid3by3_9; }
	inline GameObject_t1756533147 ** get_address_of_grid3by3_9() { return &___grid3by3_9; }
	inline void set_grid3by3_9(GameObject_t1756533147 * value)
	{
		___grid3by3_9 = value;
		Il2CppCodeGenWriteBarrier(&___grid3by3_9, value);
	}

	inline static int32_t get_offset_of_background_10() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___background_10)); }
	inline Image_t2042527209 * get_background_10() const { return ___background_10; }
	inline Image_t2042527209 ** get_address_of_background_10() { return &___background_10; }
	inline void set_background_10(Image_t2042527209 * value)
	{
		___background_10 = value;
		Il2CppCodeGenWriteBarrier(&___background_10, value);
	}

	inline static int32_t get_offset_of_puzzlePieces3by3_11() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___puzzlePieces3by3_11)); }
	inline TransformU5BU5D_t3764228911* get_puzzlePieces3by3_11() const { return ___puzzlePieces3by3_11; }
	inline TransformU5BU5D_t3764228911** get_address_of_puzzlePieces3by3_11() { return &___puzzlePieces3by3_11; }
	inline void set_puzzlePieces3by3_11(TransformU5BU5D_t3764228911* value)
	{
		___puzzlePieces3by3_11 = value;
		Il2CppCodeGenWriteBarrier(&___puzzlePieces3by3_11, value);
	}

	inline static int32_t get_offset_of_puzzlePieces2by3_12() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___puzzlePieces2by3_12)); }
	inline TransformU5BU5D_t3764228911* get_puzzlePieces2by3_12() const { return ___puzzlePieces2by3_12; }
	inline TransformU5BU5D_t3764228911** get_address_of_puzzlePieces2by3_12() { return &___puzzlePieces2by3_12; }
	inline void set_puzzlePieces2by3_12(TransformU5BU5D_t3764228911* value)
	{
		___puzzlePieces2by3_12 = value;
		Il2CppCodeGenWriteBarrier(&___puzzlePieces2by3_12, value);
	}

	inline static int32_t get_offset_of_puzzlePieces2by2_13() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___puzzlePieces2by2_13)); }
	inline TransformU5BU5D_t3764228911* get_puzzlePieces2by2_13() const { return ___puzzlePieces2by2_13; }
	inline TransformU5BU5D_t3764228911** get_address_of_puzzlePieces2by2_13() { return &___puzzlePieces2by2_13; }
	inline void set_puzzlePieces2by2_13(TransformU5BU5D_t3764228911* value)
	{
		___puzzlePieces2by2_13 = value;
		Il2CppCodeGenWriteBarrier(&___puzzlePieces2by2_13, value);
	}

	inline static int32_t get_offset_of_backgrounds2by2_14() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___backgrounds2by2_14)); }
	inline BackgroundsHolderU5BU5D_t1836318050* get_backgrounds2by2_14() const { return ___backgrounds2by2_14; }
	inline BackgroundsHolderU5BU5D_t1836318050** get_address_of_backgrounds2by2_14() { return &___backgrounds2by2_14; }
	inline void set_backgrounds2by2_14(BackgroundsHolderU5BU5D_t1836318050* value)
	{
		___backgrounds2by2_14 = value;
		Il2CppCodeGenWriteBarrier(&___backgrounds2by2_14, value);
	}

	inline static int32_t get_offset_of_backgrounds2by3_15() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___backgrounds2by3_15)); }
	inline BackgroundsHolderU5BU5D_t1836318050* get_backgrounds2by3_15() const { return ___backgrounds2by3_15; }
	inline BackgroundsHolderU5BU5D_t1836318050** get_address_of_backgrounds2by3_15() { return &___backgrounds2by3_15; }
	inline void set_backgrounds2by3_15(BackgroundsHolderU5BU5D_t1836318050* value)
	{
		___backgrounds2by3_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgrounds2by3_15, value);
	}

	inline static int32_t get_offset_of_backgrounds3by3_16() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___backgrounds3by3_16)); }
	inline BackgroundsHolderU5BU5D_t1836318050* get_backgrounds3by3_16() const { return ___backgrounds3by3_16; }
	inline BackgroundsHolderU5BU5D_t1836318050** get_address_of_backgrounds3by3_16() { return &___backgrounds3by3_16; }
	inline void set_backgrounds3by3_16(BackgroundsHolderU5BU5D_t1836318050* value)
	{
		___backgrounds3by3_16 = value;
		Il2CppCodeGenWriteBarrier(&___backgrounds3by3_16, value);
	}

	inline static int32_t get_offset_of_startingSlots_17() { return static_cast<int32_t>(offsetof(GameEightManager_t903757396, ___startingSlots_17)); }
	inline StartingSlotU5BU5D_t820505645* get_startingSlots_17() const { return ___startingSlots_17; }
	inline StartingSlotU5BU5D_t820505645** get_address_of_startingSlots_17() { return &___startingSlots_17; }
	inline void set_startingSlots_17(StartingSlotU5BU5D_t820505645* value)
	{
		___startingSlots_17 = value;
		Il2CppCodeGenWriteBarrier(&___startingSlots_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
