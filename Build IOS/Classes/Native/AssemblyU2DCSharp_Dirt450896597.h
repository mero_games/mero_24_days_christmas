﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dirt
struct  Dirt_t450896597  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Dirt::cleanTime
	float ___cleanTime_2;
	// System.Single Dirt::<TotalCleanTime>k__BackingField
	float ___U3CTotalCleanTimeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_cleanTime_2() { return static_cast<int32_t>(offsetof(Dirt_t450896597, ___cleanTime_2)); }
	inline float get_cleanTime_2() const { return ___cleanTime_2; }
	inline float* get_address_of_cleanTime_2() { return &___cleanTime_2; }
	inline void set_cleanTime_2(float value)
	{
		___cleanTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CTotalCleanTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Dirt_t450896597, ___U3CTotalCleanTimeU3Ek__BackingField_3)); }
	inline float get_U3CTotalCleanTimeU3Ek__BackingField_3() const { return ___U3CTotalCleanTimeU3Ek__BackingField_3; }
	inline float* get_address_of_U3CTotalCleanTimeU3Ek__BackingField_3() { return &___U3CTotalCleanTimeU3Ek__BackingField_3; }
	inline void set_U3CTotalCleanTimeU3Ek__BackingField_3(float value)
	{
		___U3CTotalCleanTimeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
