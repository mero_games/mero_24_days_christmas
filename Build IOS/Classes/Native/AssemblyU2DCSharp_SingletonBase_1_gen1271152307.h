﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameSixManager
struct GameSixManager_t2271637217;
// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonBase`1<GameSixManager>
struct  SingletonBase_1_t1271152307  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera SingletonBase`1::<MainCamera>k__BackingField
	Camera_t189460977 * ___U3CMainCameraU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CMainCameraU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SingletonBase_1_t1271152307, ___U3CMainCameraU3Ek__BackingField_3)); }
	inline Camera_t189460977 * get_U3CMainCameraU3Ek__BackingField_3() const { return ___U3CMainCameraU3Ek__BackingField_3; }
	inline Camera_t189460977 ** get_address_of_U3CMainCameraU3Ek__BackingField_3() { return &___U3CMainCameraU3Ek__BackingField_3; }
	inline void set_U3CMainCameraU3Ek__BackingField_3(Camera_t189460977 * value)
	{
		___U3CMainCameraU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMainCameraU3Ek__BackingField_3, value);
	}
};

struct SingletonBase_1_t1271152307_StaticFields
{
public:
	// T SingletonBase`1::instance
	GameSixManager_t2271637217 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SingletonBase_1_t1271152307_StaticFields, ___instance_2)); }
	inline GameSixManager_t2271637217 * get_instance_2() const { return ___instance_2; }
	inline GameSixManager_t2271637217 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GameSixManager_t2271637217 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
