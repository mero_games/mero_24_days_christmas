﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen2515179875.h"

// System.Collections.Generic.List`1<DifferenceCollection>
struct List_1_t797390543;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Action`1<DifferenceCollection>
struct Action_1_t1230068793;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Game17Manager
struct  Game17Manager_t3515664785  : public SingletonBase_1_t2515179875
{
public:
	// System.Collections.Generic.List`1<DifferenceCollection> Game17Manager::differenceCollections
	List_1_t797390543 * ___differenceCollections_4;
	// System.Int32 Game17Manager::remaining
	int32_t ___remaining_5;
	// System.Boolean Game17Manager::started
	bool ___started_6;
	// UnityEngine.ParticleSystem Game17Manager::particlePrefab
	ParticleSystem_t3394631041 * ___particlePrefab_7;
	// UnityEngine.UI.Text Game17Manager::remainingText
	Text_t356221433 * ___remainingText_8;

public:
	inline static int32_t get_offset_of_differenceCollections_4() { return static_cast<int32_t>(offsetof(Game17Manager_t3515664785, ___differenceCollections_4)); }
	inline List_1_t797390543 * get_differenceCollections_4() const { return ___differenceCollections_4; }
	inline List_1_t797390543 ** get_address_of_differenceCollections_4() { return &___differenceCollections_4; }
	inline void set_differenceCollections_4(List_1_t797390543 * value)
	{
		___differenceCollections_4 = value;
		Il2CppCodeGenWriteBarrier(&___differenceCollections_4, value);
	}

	inline static int32_t get_offset_of_remaining_5() { return static_cast<int32_t>(offsetof(Game17Manager_t3515664785, ___remaining_5)); }
	inline int32_t get_remaining_5() const { return ___remaining_5; }
	inline int32_t* get_address_of_remaining_5() { return &___remaining_5; }
	inline void set_remaining_5(int32_t value)
	{
		___remaining_5 = value;
	}

	inline static int32_t get_offset_of_started_6() { return static_cast<int32_t>(offsetof(Game17Manager_t3515664785, ___started_6)); }
	inline bool get_started_6() const { return ___started_6; }
	inline bool* get_address_of_started_6() { return &___started_6; }
	inline void set_started_6(bool value)
	{
		___started_6 = value;
	}

	inline static int32_t get_offset_of_particlePrefab_7() { return static_cast<int32_t>(offsetof(Game17Manager_t3515664785, ___particlePrefab_7)); }
	inline ParticleSystem_t3394631041 * get_particlePrefab_7() const { return ___particlePrefab_7; }
	inline ParticleSystem_t3394631041 ** get_address_of_particlePrefab_7() { return &___particlePrefab_7; }
	inline void set_particlePrefab_7(ParticleSystem_t3394631041 * value)
	{
		___particlePrefab_7 = value;
		Il2CppCodeGenWriteBarrier(&___particlePrefab_7, value);
	}

	inline static int32_t get_offset_of_remainingText_8() { return static_cast<int32_t>(offsetof(Game17Manager_t3515664785, ___remainingText_8)); }
	inline Text_t356221433 * get_remainingText_8() const { return ___remainingText_8; }
	inline Text_t356221433 ** get_address_of_remainingText_8() { return &___remainingText_8; }
	inline void set_remainingText_8(Text_t356221433 * value)
	{
		___remainingText_8 = value;
		Il2CppCodeGenWriteBarrier(&___remainingText_8, value);
	}
};

struct Game17Manager_t3515664785_StaticFields
{
public:
	// System.Action`1<DifferenceCollection> Game17Manager::<>f__am$cache0
	Action_1_t1230068793 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(Game17Manager_t3515664785_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Action_1_t1230068793 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Action_1_t1230068793 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Action_1_t1230068793 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
