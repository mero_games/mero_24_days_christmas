﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameDevToolkit.Sound.SoundLayer
struct SoundLayer_t2188462586;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Sound.SoundClip
struct  SoundClip_t3743947423  : public Il2CppObject
{
public:
	// GameDevToolkit.Sound.SoundLayer GameDevToolkit.Sound.SoundClip::_parentLayer
	SoundLayer_t2188462586 * ____parentLayer_0;
	// UnityEngine.AudioClip GameDevToolkit.Sound.SoundClip::_clip
	AudioClip_t1932558630 * ____clip_1;
	// System.Single GameDevToolkit.Sound.SoundClip::_relativeVolume
	float ____relativeVolume_2;

public:
	inline static int32_t get_offset_of__parentLayer_0() { return static_cast<int32_t>(offsetof(SoundClip_t3743947423, ____parentLayer_0)); }
	inline SoundLayer_t2188462586 * get__parentLayer_0() const { return ____parentLayer_0; }
	inline SoundLayer_t2188462586 ** get_address_of__parentLayer_0() { return &____parentLayer_0; }
	inline void set__parentLayer_0(SoundLayer_t2188462586 * value)
	{
		____parentLayer_0 = value;
		Il2CppCodeGenWriteBarrier(&____parentLayer_0, value);
	}

	inline static int32_t get_offset_of__clip_1() { return static_cast<int32_t>(offsetof(SoundClip_t3743947423, ____clip_1)); }
	inline AudioClip_t1932558630 * get__clip_1() const { return ____clip_1; }
	inline AudioClip_t1932558630 ** get_address_of__clip_1() { return &____clip_1; }
	inline void set__clip_1(AudioClip_t1932558630 * value)
	{
		____clip_1 = value;
		Il2CppCodeGenWriteBarrier(&____clip_1, value);
	}

	inline static int32_t get_offset_of__relativeVolume_2() { return static_cast<int32_t>(offsetof(SoundClip_t3743947423, ____relativeVolume_2)); }
	inline float get__relativeVolume_2() const { return ____relativeVolume_2; }
	inline float* get_address_of__relativeVolume_2() { return &____relativeVolume_2; }
	inline void set__relativeVolume_2(float value)
	{
		____relativeVolume_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
