﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Events.MouseEventProccessor
struct MouseEventProccessor_t442073356;
// GameDevToolkit.Events.IMouseEventSystem
struct IMouseEventSystem_t2547859815;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Events.MouseEventProccessor
struct  MouseEventProccessor_t442073356  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GameDevToolkit.Events.MouseEventProccessor::captureMouseMouveEvents
	bool ___captureMouseMouveEvents_3;
	// System.Boolean GameDevToolkit.Events.MouseEventProccessor::_captureEvents
	bool ____captureEvents_4;
	// GameDevToolkit.Events.IMouseEventSystem GameDevToolkit.Events.MouseEventProccessor::lastHit
	Il2CppObject * ___lastHit_5;

public:
	inline static int32_t get_offset_of_captureMouseMouveEvents_3() { return static_cast<int32_t>(offsetof(MouseEventProccessor_t442073356, ___captureMouseMouveEvents_3)); }
	inline bool get_captureMouseMouveEvents_3() const { return ___captureMouseMouveEvents_3; }
	inline bool* get_address_of_captureMouseMouveEvents_3() { return &___captureMouseMouveEvents_3; }
	inline void set_captureMouseMouveEvents_3(bool value)
	{
		___captureMouseMouveEvents_3 = value;
	}

	inline static int32_t get_offset_of__captureEvents_4() { return static_cast<int32_t>(offsetof(MouseEventProccessor_t442073356, ____captureEvents_4)); }
	inline bool get__captureEvents_4() const { return ____captureEvents_4; }
	inline bool* get_address_of__captureEvents_4() { return &____captureEvents_4; }
	inline void set__captureEvents_4(bool value)
	{
		____captureEvents_4 = value;
	}

	inline static int32_t get_offset_of_lastHit_5() { return static_cast<int32_t>(offsetof(MouseEventProccessor_t442073356, ___lastHit_5)); }
	inline Il2CppObject * get_lastHit_5() const { return ___lastHit_5; }
	inline Il2CppObject ** get_address_of_lastHit_5() { return &___lastHit_5; }
	inline void set_lastHit_5(Il2CppObject * value)
	{
		___lastHit_5 = value;
		Il2CppCodeGenWriteBarrier(&___lastHit_5, value);
	}
};

struct MouseEventProccessor_t442073356_StaticFields
{
public:
	// GameDevToolkit.Events.MouseEventProccessor GameDevToolkit.Events.MouseEventProccessor::Instance
	MouseEventProccessor_t442073356 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(MouseEventProccessor_t442073356_StaticFields, ___Instance_2)); }
	inline MouseEventProccessor_t442073356 * get_Instance_2() const { return ___Instance_2; }
	inline MouseEventProccessor_t442073356 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(MouseEventProccessor_t442073356 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
