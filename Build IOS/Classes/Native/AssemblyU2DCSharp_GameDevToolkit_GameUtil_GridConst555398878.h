﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.GameUtil.GridConstrucor
struct  GridConstrucor_t555398878  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 GameDevToolkit.GameUtil.GridConstrucor::rows
	int32_t ___rows_2;
	// System.Int32 GameDevToolkit.GameUtil.GridConstrucor::cols
	int32_t ___cols_3;
	// UnityEngine.GameObject GameDevToolkit.GameUtil.GridConstrucor::CellPrefab
	GameObject_t1756533147 * ___CellPrefab_4;
	// System.Single GameDevToolkit.GameUtil.GridConstrucor::cellWidth
	float ___cellWidth_5;
	// System.Single GameDevToolkit.GameUtil.GridConstrucor::cellHeigth
	float ___cellHeigth_6;

public:
	inline static int32_t get_offset_of_rows_2() { return static_cast<int32_t>(offsetof(GridConstrucor_t555398878, ___rows_2)); }
	inline int32_t get_rows_2() const { return ___rows_2; }
	inline int32_t* get_address_of_rows_2() { return &___rows_2; }
	inline void set_rows_2(int32_t value)
	{
		___rows_2 = value;
	}

	inline static int32_t get_offset_of_cols_3() { return static_cast<int32_t>(offsetof(GridConstrucor_t555398878, ___cols_3)); }
	inline int32_t get_cols_3() const { return ___cols_3; }
	inline int32_t* get_address_of_cols_3() { return &___cols_3; }
	inline void set_cols_3(int32_t value)
	{
		___cols_3 = value;
	}

	inline static int32_t get_offset_of_CellPrefab_4() { return static_cast<int32_t>(offsetof(GridConstrucor_t555398878, ___CellPrefab_4)); }
	inline GameObject_t1756533147 * get_CellPrefab_4() const { return ___CellPrefab_4; }
	inline GameObject_t1756533147 ** get_address_of_CellPrefab_4() { return &___CellPrefab_4; }
	inline void set_CellPrefab_4(GameObject_t1756533147 * value)
	{
		___CellPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___CellPrefab_4, value);
	}

	inline static int32_t get_offset_of_cellWidth_5() { return static_cast<int32_t>(offsetof(GridConstrucor_t555398878, ___cellWidth_5)); }
	inline float get_cellWidth_5() const { return ___cellWidth_5; }
	inline float* get_address_of_cellWidth_5() { return &___cellWidth_5; }
	inline void set_cellWidth_5(float value)
	{
		___cellWidth_5 = value;
	}

	inline static int32_t get_offset_of_cellHeigth_6() { return static_cast<int32_t>(offsetof(GridConstrucor_t555398878, ___cellHeigth_6)); }
	inline float get_cellHeigth_6() const { return ___cellHeigth_6; }
	inline float* get_address_of_cellHeigth_6() { return &___cellHeigth_6; }
	inline void set_cellHeigth_6(float value)
	{
		___cellHeigth_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
