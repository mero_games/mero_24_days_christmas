﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameThirteenManager3231626088.h"
#include "AssemblyU2DCSharp_GameThirteenManager_U3CYGenerate2284528656.h"
#include "AssemblyU2DCSharp_GameThirteenManager_U3CYResetSnow678839100.h"
#include "AssemblyU2DCSharp_SnowManMatch3305190334.h"
#include "AssemblyU2DCSharp_LevelSelector1424646157.h"
#include "AssemblyU2DCSharp_LevelSelector_U3CChangeTheLevelU3752393928.h"
#include "AssemblyU2DCSharp_MemoryGame4018179393.h"
#include "AssemblyU2DCSharp_MemoryGame_WinDelegate3170222997.h"
#include "AssemblyU2DCSharp_MemoryGame_U3CflipToFrontU3Ec__I3083261804.h"
#include "AssemblyU2DCSharp_MemoryGame_U3CflipToBackU3Ec__It2557887515.h"
#include "AssemblyU2DCSharp_MemoryGamePiece2964149615.h"
#include "AssemblyU2DCSharp_GameFifteenManager1527188406.h"
#include "AssemblyU2DCSharp_GameFifteenManager_LightColors1169232587.h"
#include "AssemblyU2DCSharp_GameFifteenManager_LightComplete2236120434.h"
#include "AssemblyU2DCSharp_GameFifteenManager_LightPuzzle891176437.h"
#include "AssemblyU2DCSharp_GameFifteenManager_U3CYChangeLig2915884486.h"
#include "AssemblyU2DCSharp_LightBulb3998556073.h"
#include "AssemblyU2DCSharp_GameSixteenManager358793919.h"
#include "AssemblyU2DCSharp_Grid4161269204.h"
#include "AssemblyU2DCSharp_NextSpawner3258520277.h"
#include "AssemblyU2DCSharp_Piece3723615338.h"
#include "AssemblyU2DCSharp_ScoreManager3573108141.h"
#include "AssemblyU2DCSharp_Spawner534830648.h"
#include "AssemblyU2DCSharp_TetrisUtils4156835306.h"
#include "AssemblyU2DCSharp_Difference980400781.h"
#include "AssemblyU2DCSharp_Difference_StartType1229205632.h"
#include "AssemblyU2DCSharp_Difference_OnClickType2462927197.h"
#include "AssemblyU2DCSharp_DifferenceCollection1428269411.h"
#include "AssemblyU2DCSharp_Game17Manager3515664785.h"
#include "AssemblyU2DCSharp_Twin1055953070.h"
#include "AssemblyU2DCSharp_GameEighteenManager3662215866.h"
#include "AssemblyU2DCSharp_GameEighteenManager_GameRounds1482069856.h"
#include "AssemblyU2DCSharp_GameEighteenManager_U3CGetBulbU32219634306.h"
#include "AssemblyU2DCSharp_GameEighteenManager_U3CYShowGame1547970616.h"
#include "AssemblyU2DCSharp_Song1848967459.h"
#include "AssemblyU2DCSharp_GameNineteenManager942308577.h"
#include "AssemblyU2DCSharp_GameNineteenManager_U3CYNoteSpaw2681467500.h"
#include "AssemblyU2DCSharp_Note686167636.h"
#include "AssemblyU2DCSharp_PianoButton3912953253.h"
#include "AssemblyU2DCSharp_GameTwoManager1572221819.h"
#include "AssemblyU2DCSharp_GameTwoManager_U3CYSetupItemsU3Ec568853987.h"
#include "AssemblyU2DCSharp_ItemClickProcessor208213687.h"
#include "AssemblyU2DCSharp_TransformGrid2312775536.h"
#include "AssemblyU2DCSharp_DotPencil1876295642.h"
#include "AssemblyU2DCSharp_GameTwentyManager1246478874.h"
#include "AssemblyU2DCSharp_GameTwentyManager_U3CYDrawingDon2118232199.h"
#include "AssemblyU2DCSharp_GameTwentyoneManager426707788.h"
#include "AssemblyU2DCSharp_GameTwentyoneManager_GiftObject2144675780.h"
#include "AssemblyU2DCSharp_GiftDrawer3411291385.h"
#include "AssemblyU2DCSharp_GiftDrawer_U3CYFadeOutU3Ec__Iter3483403400.h"
#include "AssemblyU2DCSharp_GiftDrawer_U3CYFadeInU3Ec__Iterat129333632.h"
#include "AssemblyU2DCSharp_Testscript905621679.h"
#include "AssemblyU2DCSharp_DraggableOrnament3124044711.h"
#include "AssemblyU2DCSharp_DraggableOrnament_U3CYFollowMous4055350718.h"
#include "AssemblyU2DCSharp_DraggableOrnament_U3CYLightUpU3E4139173986.h"
#include "AssemblyU2DCSharp_GameTwentytwoManager3731809278.h"
#include "AssemblyU2DCSharp_ClickObject2003887325.h"
#include "AssemblyU2DCSharp_ClickObject_ClickType4062750492.h"
#include "AssemblyU2DCSharp_GameTwentythreeManager2915129944.h"
#include "AssemblyU2DCSharp_GameTwentythreeManager_U3CYSwipe3099782161.h"
#include "AssemblyU2DCSharp_GameTwentythreeManager_U3Cdelaye2531885572.h"
#include "AssemblyU2DCSharp_GiftSpawner1625374518.h"
#include "AssemblyU2DCSharp_RepeatingBackground2924144931.h"
#include "AssemblyU2DCSharp_Game24Manager283638021.h"
#include "AssemblyU2DCSharp_Gift450896550.h"
#include "AssemblyU2DCSharp_GiftManager2451335245.h"
#include "AssemblyU2DCSharp_GiftManager_U3CThrownU3Ec__Iterat974249205.h"
#include "AssemblyU2DCSharp_GiftSpawnPoint604323357.h"
#include "AssemblyU2DCSharp_Yey2293551375.h"
#include "AssemblyU2DCSharp_Yey_U3CScaleU3Ec__AnonStorey01001303329.h"
#include "AssemblyU2DCSharp_ObjectScroller389948937.h"
#include "AssemblyU2DCSharp_ObjectScroller_U3CYStopScrollU3Ec632694609.h"
#include "AssemblyU2DCSharp_Bird1163357383.h"
#include "AssemblyU2DCSharp_Building2809767176.h"
#include "AssemblyU2DCSharp_ObstacleBase365467716.h"
#include "AssemblyU2DCSharp_ObstacleManager3466292636.h"
#include "AssemblyU2DCSharp_ObstacleManager_U3CRecycleSpawne1754565804.h"
#include "AssemblyU2DCSharp_ObstacleManager_U3CRecycleSpawned840817611.h"
#include "AssemblyU2DCSharp_Tree2595185816.h"
#include "AssemblyU2DCSharp_Santa3804168179.h"
#include "AssemblyU2DCSharp_Santa_U3CRecylceParticleU3Ec__It3066034944.h"
#include "AssemblyU2DCSharp_Timer2917042437.h"
#include "AssemblyU2DCSharp_GameThreeManager1469036085.h"
#include "AssemblyU2DCSharp_GameThreeManager_U3CYHatControll2147178273.h"
#include "AssemblyU2DCSharp_HatEnabler1910487184.h"
#include "AssemblyU2DCSharp_RingTossController956255555.h"
#include "AssemblyU2DCSharp_RingTossController_U3CYSleepHatU3412436362.h"
#include "AssemblyU2DCSharp_GameFourManager2429316785.h"
#include "AssemblyU2DCSharp_GameFourManager_U3CYStartNextDra4224780971.h"
#include "AssemblyU2DCSharp_Pencil4115486279.h"
#include "AssemblyU2DCSharp_Cleaner1604682602.h"
#include "AssemblyU2DCSharp_Cleaner_U3CYFollowMouseU3Ec__Ite1570186929.h"
#include "AssemblyU2DCSharp_Dirt450896597.h"
#include "AssemblyU2DCSharp_GameFiveManager3955517211.h"
#include "AssemblyU2DCSharp_GameFiveManager_U3CYCleanDirtU3E2454611604.h"
#include "AssemblyU2DCSharp_Candy221644187.h"
#include "AssemblyU2DCSharp_Candy_Type2899008702.h"
#include "AssemblyU2DCSharp_GameSixManager2271637217.h"
#include "AssemblyU2DCSharp_GameSixManager_U3CYFollowMouseU31347612948.h"
#include "AssemblyU2DCSharp_GameSixManager_U3CYCandySpawnerU2324817448.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (GameThirteenManager_t3231626088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[11] = 
{
	GameThirteenManager_t3231626088::get_offset_of_numberOfSetsToDo_4(),
	GameThirteenManager_t3231626088::get_offset_of_setsDone_5(),
	GameThirteenManager_t3231626088::get_offset_of_snowManSize_6(),
	GameThirteenManager_t3231626088::get_offset_of_exampleSnowMan_7(),
	GameThirteenManager_t3231626088::get_offset_of_snowMen_8(),
	GameThirteenManager_t3231626088::get_offset_of_snowMenParts_9(),
	GameThirteenManager_t3231626088::get_offset_of_canClick_10(),
	GameThirteenManager_t3231626088::get_offset_of_originalHead_11(),
	GameThirteenManager_t3231626088::get_offset_of_originalBody_12(),
	GameThirteenManager_t3231626088::get_offset_of_spriteOneCount_13(),
	GameThirteenManager_t3231626088::get_offset_of_spriteTwoCount_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (U3CYGenerateNewSetU3Ec__Iterator0_t2284528656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[6] = 
{
	U3CYGenerateNewSetU3Ec__Iterator0_t2284528656::get_offset_of_U3CcorrectSnowmanU3E__0_0(),
	U3CYGenerateNewSetU3Ec__Iterator0_t2284528656::get_offset_of_U3CiU3E__1_1(),
	U3CYGenerateNewSetU3Ec__Iterator0_t2284528656::get_offset_of_U24this_2(),
	U3CYGenerateNewSetU3Ec__Iterator0_t2284528656::get_offset_of_U24current_3(),
	U3CYGenerateNewSetU3Ec__Iterator0_t2284528656::get_offset_of_U24disposing_4(),
	U3CYGenerateNewSetU3Ec__Iterator0_t2284528656::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (U3CYResetSnowMenU3Ec__Iterator1_t678839100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[5] = 
{
	U3CYResetSnowMenU3Ec__Iterator1_t678839100::get_offset_of_U3CcorrectSnowmanU3E__0_0(),
	U3CYResetSnowMenU3Ec__Iterator1_t678839100::get_offset_of_U24this_1(),
	U3CYResetSnowMenU3Ec__Iterator1_t678839100::get_offset_of_U24current_2(),
	U3CYResetSnowMenU3Ec__Iterator1_t678839100::get_offset_of_U24disposing_3(),
	U3CYResetSnowMenU3Ec__Iterator1_t678839100::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (SnowManMatch_t3305190334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[1] = 
{
	SnowManMatch_t3305190334::get_offset_of_U3CIsCorrectU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (LevelSelector_t1424646157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[2] = 
{
	LevelSelector_t1424646157::get_offset_of_levels_10(),
	LevelSelector_t1424646157::get_offset_of_bgs_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (U3CChangeTheLevelU3Ec__Iterator0_t3752393928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[4] = 
{
	U3CChangeTheLevelU3Ec__Iterator0_t3752393928::get_offset_of_U24this_0(),
	U3CChangeTheLevelU3Ec__Iterator0_t3752393928::get_offset_of_U24current_1(),
	U3CChangeTheLevelU3Ec__Iterator0_t3752393928::get_offset_of_U24disposing_2(),
	U3CChangeTheLevelU3Ec__Iterator0_t3752393928::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (MemoryGame_t4018179393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[8] = 
{
	MemoryGame_t4018179393::get_offset_of_pieces_2(),
	MemoryGame_t4018179393::get_offset_of_clicked1_3(),
	MemoryGame_t4018179393::get_offset_of_clicked2_4(),
	MemoryGame_t4018179393::get_offset_of_canClick_5(),
	MemoryGame_t4018179393::get_offset_of_cantClickAnyMore_6(),
	MemoryGame_t4018179393::get_offset_of_matchCount_7(),
	MemoryGame_t4018179393::get_offset_of_LevelNumber_8(),
	MemoryGame_t4018179393::get_offset_of_DoneMemory_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (WinDelegate_t3170222997), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CflipToFrontU3Ec__Iterator0_t3083261804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[5] = 
{
	U3CflipToFrontU3Ec__Iterator0_t3083261804::get_offset_of_clicked_0(),
	U3CflipToFrontU3Ec__Iterator0_t3083261804::get_offset_of_U24this_1(),
	U3CflipToFrontU3Ec__Iterator0_t3083261804::get_offset_of_U24current_2(),
	U3CflipToFrontU3Ec__Iterator0_t3083261804::get_offset_of_U24disposing_3(),
	U3CflipToFrontU3Ec__Iterator0_t3083261804::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (U3CflipToBackU3Ec__Iterator1_t2557887515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[6] = 
{
	U3CflipToBackU3Ec__Iterator1_t2557887515::get_offset_of_clicked_0(),
	U3CflipToBackU3Ec__Iterator1_t2557887515::get_offset_of_enableCanClickWhenDone_1(),
	U3CflipToBackU3Ec__Iterator1_t2557887515::get_offset_of_U24this_2(),
	U3CflipToBackU3Ec__Iterator1_t2557887515::get_offset_of_U24current_3(),
	U3CflipToBackU3Ec__Iterator1_t2557887515::get_offset_of_U24disposing_4(),
	U3CflipToBackU3Ec__Iterator1_t2557887515::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (MemoryGamePiece_t2964149615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[2] = 
{
	MemoryGamePiece_t2964149615::get_offset_of_back_2(),
	MemoryGamePiece_t2964149615::get_offset_of_front_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (GameFifteenManager_t1527188406), -1, sizeof(GameFifteenManager_t1527188406_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2011[19] = 
{
	GameFifteenManager_t1527188406::get_offset_of_rounds_4(),
	GameFifteenManager_t1527188406::get_offset_of_lights_5(),
	GameFifteenManager_t1527188406::get_offset_of_offLight_6(),
	GameFifteenManager_t1527188406::get_offset_of_roundDelay_7(),
	GameFifteenManager_t1527188406::get_offset_of_waitTime_8(),
	GameFifteenManager_t1527188406::get_offset_of_newWaitTime_9(),
	GameFifteenManager_t1527188406::get_offset_of_speed_10(),
	GameFifteenManager_t1527188406::get_offset_of_newspeed_11(),
	GameFifteenManager_t1527188406::get_offset_of_lightsParent_12(),
	GameFifteenManager_t1527188406::get_offset_of_mask_13(),
	GameFifteenManager_t1527188406::get_offset_of_puzzle_14(),
	GameFifteenManager_t1527188406::get_offset_of_completed_15(),
	GameFifteenManager_t1527188406::get_offset_of_lines_16(),
	GameFifteenManager_t1527188406::get_offset_of_canClick_17(),
	GameFifteenManager_t1527188406::get_offset_of_list_18(),
	GameFifteenManager_t1527188406::get_offset_of_curBulb_19(),
	GameFifteenManager_t1527188406::get_offset_of_curRound_20(),
	GameFifteenManager_t1527188406::get_offset_of_canGlow_21(),
	GameFifteenManager_t1527188406_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (LightColors_t1169232587)+ sizeof (Il2CppObject), sizeof(LightColors_t1169232587 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2012[2] = 
{
	LightColors_t1169232587::get_offset_of_newcolor_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LightColors_t1169232587::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (LightCompleted_t2236120434)+ sizeof (Il2CppObject), sizeof(LightCompleted_t2236120434_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2013[2] = 
{
	LightCompleted_t2236120434::get_offset_of_completed_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LightCompleted_t2236120434::get_offset_of_list_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (LightPuzzle_t891176437)+ sizeof (Il2CppObject), sizeof(LightPuzzle_t891176437_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2014[2] = 
{
	LightPuzzle_t891176437::get_offset_of_showLights_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LightPuzzle_t891176437::get_offset_of_scheme_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (U3CYChangeLightsU3Ec__Iterator0_t2915884486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[8] = 
{
	U3CYChangeLightsU3Ec__Iterator0_t2915884486::get_offset_of_U3CcountU3E__0_0(),
	U3CYChangeLightsU3Ec__Iterator0_t2915884486::get_offset_of_U3CcurSpeedU3E__0_1(),
	U3CYChangeLightsU3Ec__Iterator0_t2915884486::get_offset_of_U3CcurWaitU3E__0_2(),
	U3CYChangeLightsU3Ec__Iterator0_t2915884486::get_offset_of_row_3(),
	U3CYChangeLightsU3Ec__Iterator0_t2915884486::get_offset_of_U24this_4(),
	U3CYChangeLightsU3Ec__Iterator0_t2915884486::get_offset_of_U24current_5(),
	U3CYChangeLightsU3Ec__Iterator0_t2915884486::get_offset_of_U24disposing_6(),
	U3CYChangeLightsU3Ec__Iterator0_t2915884486::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (LightBulb_t3998556073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[11] = 
{
	LightBulb_t3998556073::get_offset_of_canGlow_2(),
	LightBulb_t3998556073::get_offset_of_canClick_3(),
	LightBulb_t3998556073::get_offset_of__sprite_4(),
	LightBulb_t3998556073::get_offset_of__spriteGlow_5(),
	LightBulb_t3998556073::get_offset_of__col_6(),
	LightBulb_t3998556073::get_offset_of_U3CtypeU3Ek__BackingField_7(),
	LightBulb_t3998556073::get_offset_of_U3CrowU3Ek__BackingField_8(),
	LightBulb_t3998556073::get_offset_of_U3CnumU3Ek__BackingField_9(),
	LightBulb_t3998556073::get_offset_of_U3CcolU3Ek__BackingField_10(),
	LightBulb_t3998556073::get_offset_of_U3ClightUpU3Ek__BackingField_11(),
	LightBulb_t3998556073::get_offset_of_U3CbaseColorU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (GameSixteenManager_t358793919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[5] = 
{
	GameSixteenManager_t358793919::get_offset_of_endScreenContainer_4(),
	GameSixteenManager_t358793919::get_offset_of_gridPrefab_5(),
	GameSixteenManager_t358793919::get_offset_of_gridWidth_6(),
	GameSixteenManager_t358793919::get_offset_of_gridHeight_7(),
	GameSixteenManager_t358793919::get_offset_of_gridSquareSize_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (Grid_t4161269204), -1, sizeof(Grid_t4161269204_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2018[3] = 
{
	Grid_t4161269204_StaticFields::get_offset_of_w_2(),
	Grid_t4161269204_StaticFields::get_offset_of_h_3(),
	Grid_t4161269204_StaticFields::get_offset_of_grid_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (NextSpawner_t3258520277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[3] = 
{
	NextSpawner_t3258520277::get_offset_of_spawner_2(),
	NextSpawner_t3258520277::get_offset_of_currentPieceObject_3(),
	NextSpawner_t3258520277::get_offset_of_currentPieceId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (Piece_t3723615338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[7] = 
{
	Piece_t3723615338::get_offset_of_lastFall_2(),
	Piece_t3723615338::get_offset_of_lastKeyDown_3(),
	Piece_t3723615338::get_offset_of_timeKeyPressed_4(),
	Piece_t3723615338::get_offset_of_fp_5(),
	Piece_t3723615338::get_offset_of_lp_6(),
	Piece_t3723615338::get_offset_of_dragDistance_7(),
	Piece_t3723615338::get_offset_of_touchPosWorld_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (ScoreManager_t3573108141), -1, sizeof(ScoreManager_t3573108141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2021[2] = 
{
	ScoreManager_t3573108141_StaticFields::get_offset_of_score_2(),
	ScoreManager_t3573108141::get_offset_of_scoreText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (Spawner_t534830648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[2] = 
{
	Spawner_t534830648::get_offset_of_pieces_2(),
	Spawner_t534830648::get_offset_of_nextId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (TetrisUtils_t4156835306), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (Difference_t980400781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[6] = 
{
	Difference_t980400781::get_offset_of_startType_2(),
	Difference_t980400781::get_offset_of_onClickType_3(),
	Difference_t980400781::get_offset_of_rotateBy_4(),
	Difference_t980400781::get_offset_of_selected_5(),
	Difference_t980400781::get_offset_of_found_6(),
	Difference_t980400781::get_offset_of_isVisible_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (StartType_t1229205632)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2025[4] = 
{
	StartType_t1229205632::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (OnClickType_t2462927197)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2026[4] = 
{
	OnClickType_t2462927197::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (DifferenceCollection_t1428269411), -1, sizeof(DifferenceCollection_t1428269411_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2027[2] = 
{
	DifferenceCollection_t1428269411::get_offset_of_differences_2(),
	DifferenceCollection_t1428269411_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (Game17Manager_t3515664785), -1, sizeof(Game17Manager_t3515664785_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2028[6] = 
{
	Game17Manager_t3515664785::get_offset_of_differenceCollections_4(),
	Game17Manager_t3515664785::get_offset_of_remaining_5(),
	Game17Manager_t3515664785::get_offset_of_started_6(),
	Game17Manager_t3515664785::get_offset_of_particlePrefab_7(),
	Game17Manager_t3515664785::get_offset_of_remainingText_8(),
	Game17Manager_t3515664785_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (Twin_t1055953070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[1] = 
{
	Twin_t1055953070::get_offset_of_twin_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (GameEighteenManager_t3662215866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[10] = 
{
	GameEighteenManager_t3662215866::get_offset_of_colors_4(),
	GameEighteenManager_t3662215866::get_offset_of_bulbs_5(),
	GameEighteenManager_t3662215866::get_offset_of_rounds_6(),
	GameEighteenManager_t3662215866::get_offset_of_swapColors_7(),
	GameEighteenManager_t3662215866::get_offset_of_mask_8(),
	GameEighteenManager_t3662215866::get_offset_of_waitTime_9(),
	GameEighteenManager_t3662215866::get_offset_of_curRound_10(),
	GameEighteenManager_t3662215866::get_offset_of_canClick_11(),
	GameEighteenManager_t3662215866::get_offset_of_clicks_12(),
	GameEighteenManager_t3662215866::get_offset_of_oldClicks_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (GameRounds_t1482069856)+ sizeof (Il2CppObject), sizeof(GameRounds_t1482069856 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2031[3] = 
{
	GameRounds_t1482069856::get_offset_of_colors_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GameRounds_t1482069856::get_offset_of_length_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GameRounds_t1482069856::get_offset_of_speed_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (U3CGetBulbU3Ec__AnonStorey1_t2219634306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[1] = 
{
	U3CGetBulbU3Ec__AnonStorey1_t2219634306::get_offset_of_hit_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (U3CYShowGameU3Ec__Iterator0_t1547970616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[12] = 
{
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_ind_0(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_U3CroundU3E__0_1(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_random_2(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_U3CkU3E__1_3(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_U3CiU3E__2_4(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_U3CrandU3E__0_5(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_U3CiU3E__3_6(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_U3CiU3E__4_7(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_U24this_8(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_U24current_9(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_U24disposing_10(),
	U3CYShowGameU3Ec__Iterator0_t1547970616::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (Song_t1848967459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[1] = 
{
	Song_t1848967459::get_offset_of_songSequence_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (GameNineteenManager_t942308577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[13] = 
{
	GameNineteenManager_t942308577::get_offset_of_noteInterval_4(),
	GameNineteenManager_t942308577::get_offset_of_currentNote_5(),
	GameNineteenManager_t942308577::get_offset_of_feedbackIntervalCounter_6(),
	GameNineteenManager_t942308577::get_offset_of_feedbackInterval_7(),
	GameNineteenManager_t942308577::get_offset_of_toHappy_8(),
	GameNineteenManager_t942308577::get_offset_of_canSetMood_9(),
	GameNineteenManager_t942308577::get_offset_of_note_10(),
	GameNineteenManager_t942308577::get_offset_of_correctParticle_11(),
	GameNineteenManager_t942308577::get_offset_of_startPosition_12(),
	GameNineteenManager_t942308577::get_offset_of_noteColors_13(),
	GameNineteenManager_t942308577::get_offset_of_noteToPress_14(),
	GameNineteenManager_t942308577::get_offset_of_songs_15(),
	GameNineteenManager_t942308577::get_offset_of_currentSong_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (U3CYNoteSpawnerU3Ec__Iterator0_t2681467500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[6] = 
{
	U3CYNoteSpawnerU3Ec__Iterator0_t2681467500::get_offset_of_U3CtimerU3E__0_0(),
	U3CYNoteSpawnerU3Ec__Iterator0_t2681467500::get_offset_of_U3CiU3E__0_1(),
	U3CYNoteSpawnerU3Ec__Iterator0_t2681467500::get_offset_of_U24this_2(),
	U3CYNoteSpawnerU3Ec__Iterator0_t2681467500::get_offset_of_U24current_3(),
	U3CYNoteSpawnerU3Ec__Iterator0_t2681467500::get_offset_of_U24disposing_4(),
	U3CYNoteSpawnerU3Ec__Iterator0_t2681467500::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (Note_t686167636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[6] = 
{
	Note_t686167636::get_offset_of_speed_2(),
	Note_t686167636::get_offset_of_noteIndex_3(),
	Note_t686167636::get_offset_of_startPos_4(),
	Note_t686167636::get_offset_of_amplitudeY_5(),
	Note_t686167636::get_offset_of_omegaY_6(),
	Note_t686167636::get_offset_of_index_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (PianoButton_t3912953253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[1] = 
{
	PianoButton_t3912953253::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (GameTwoManager_t1572221819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[10] = 
{
	GameTwoManager_t1572221819::get_offset_of_isPlaying_4(),
	GameTwoManager_t1572221819::get_offset_of_snow_5(),
	GameTwoManager_t1572221819::get_offset_of_elements_6(),
	GameTwoManager_t1572221819::get_offset_of_startPositions_7(),
	GameTwoManager_t1572221819::get_offset_of_rows_8(),
	GameTwoManager_t1572221819::get_offset_of_digEffect_9(),
	GameTwoManager_t1572221819::get_offset_of_spawnedObjects_10(),
	GameTwoManager_t1572221819::get_offset_of_elementsUsed_11(),
	GameTwoManager_t1572221819::get_offset_of_lastClickedCollider_12(),
	GameTwoManager_t1572221819::get_offset_of_itemCounter_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (U3CYSetupItemsU3Ec__Iterator0_t568853987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[5] = 
{
	U3CYSetupItemsU3Ec__Iterator0_t568853987::get_offset_of_U3CtempU3E__0_0(),
	U3CYSetupItemsU3Ec__Iterator0_t568853987::get_offset_of_U24this_1(),
	U3CYSetupItemsU3Ec__Iterator0_t568853987::get_offset_of_U24current_2(),
	U3CYSetupItemsU3Ec__Iterator0_t568853987::get_offset_of_U24disposing_3(),
	U3CYSetupItemsU3Ec__Iterator0_t568853987::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (ItemClickProcessor_t208213687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[3] = 
{
	ItemClickProcessor_t208213687::get_offset_of_beenOpened_2(),
	ItemClickProcessor_t208213687::get_offset_of_clickCounter_3(),
	ItemClickProcessor_t208213687::get_offset_of_clickThreshold_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (TransformGrid_t2312775536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[1] = 
{
	TransformGrid_t2312775536::get_offset_of_columns_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (DotPencil_t1876295642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[1] = 
{
	DotPencil_t1876295642::get_offset_of_toMove_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (GameTwentyManager_t1246478874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[9] = 
{
	GameTwentyManager_t1246478874::get_offset_of_canDraw_4(),
	GameTwentyManager_t1246478874::get_offset_of_pencil_5(),
	GameTwentyManager_t1246478874::get_offset_of_wrongCollidersHit_6(),
	GameTwentyManager_t1246478874::get_offset_of_correctCollidersLeft_7(),
	GameTwentyManager_t1246478874::get_offset_of_shapes_8(),
	GameTwentyManager_t1246478874::get_offset_of_drawingsHolder_9(),
	GameTwentyManager_t1246478874::get_offset_of_correctColliders_10(),
	GameTwentyManager_t1246478874::get_offset_of_shapeIndex_11(),
	GameTwentyManager_t1246478874::get_offset_of_iPencil_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (U3CYDrawingDoneU3Ec__Iterator0_t2118232199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[6] = 
{
	U3CYDrawingDoneU3Ec__Iterator0_t2118232199::get_offset_of_U3CalphaValueU3E__0_0(),
	U3CYDrawingDoneU3Ec__Iterator0_t2118232199::get_offset_of_U3CnewColorU3E__1_1(),
	U3CYDrawingDoneU3Ec__Iterator0_t2118232199::get_offset_of_U24this_2(),
	U3CYDrawingDoneU3Ec__Iterator0_t2118232199::get_offset_of_U24current_3(),
	U3CYDrawingDoneU3Ec__Iterator0_t2118232199::get_offset_of_U24disposing_4(),
	U3CYDrawingDoneU3Ec__Iterator0_t2118232199::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (GameTwentyoneManager_t426707788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[10] = 
{
	GameTwentyoneManager_t426707788::get_offset_of_imageComplete_4(),
	GameTwentyoneManager_t426707788::get_offset_of_rounds_5(),
	GameTwentyoneManager_t426707788::get_offset_of_fadeSpeed_6(),
	GameTwentyoneManager_t426707788::get_offset_of_scale_7(),
	GameTwentyoneManager_t426707788::get_offset_of_checkTime_8(),
	GameTwentyoneManager_t426707788::get_offset_of_gifts_9(),
	GameTwentyoneManager_t426707788::get_offset_of_giftDrawer_10(),
	GameTwentyoneManager_t426707788::get_offset_of__gifts_11(),
	GameTwentyoneManager_t426707788::get_offset_of_curRound_12(),
	GameTwentyoneManager_t426707788::get_offset_of_drawLoopSound_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (GiftObject_t2144675780)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[2] = 
{
	GiftObject_t2144675780::get_offset_of_gift_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GiftObject_t2144675780::get_offset_of_wrapper_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (GiftDrawer_t3411291385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[24] = 
{
	GiftDrawer_t3411291385::get_offset_of_wrapSprite_2(),
	GiftDrawer_t3411291385::get_offset_of_giftSprite_3(),
	GiftDrawer_t3411291385::get_offset_of_img_4(),
	GiftDrawer_t3411291385::get_offset_of_img2_5(),
	GiftDrawer_t3411291385::get_offset_of_clear_6(),
	GiftDrawer_t3411291385::get_offset_of_clearMaterial_7(),
	GiftDrawer_t3411291385::get_offset_of_time_8(),
	GiftDrawer_t3411291385::get_offset_of_startPos_9(),
	GiftDrawer_t3411291385::get_offset_of_endPos_10(),
	GiftDrawer_t3411291385::get_offset_of_U3CcanDrawU3Ek__BackingField_11(),
	GiftDrawer_t3411291385::get_offset_of_texwidth_12(),
	GiftDrawer_t3411291385::get_offset_of_texheight_13(),
	GiftDrawer_t3411291385::get_offset_of_size_14(),
	GiftDrawer_t3411291385::get_offset_of_width_15(),
	GiftDrawer_t3411291385::get_offset_of_height_16(),
	GiftDrawer_t3411291385::get_offset_of_minbordx_17(),
	GiftDrawer_t3411291385::get_offset_of_minbordy_18(),
	GiftDrawer_t3411291385::get_offset_of_maxbordx_19(),
	GiftDrawer_t3411291385::get_offset_of_maxbordy_20(),
	GiftDrawer_t3411291385::get_offset_of_texture_21(),
	GiftDrawer_t3411291385::get_offset_of_oldPos_22(),
	GiftDrawer_t3411291385::get_offset_of_pos_23(),
	GiftDrawer_t3411291385::get_offset_of_count_24(),
	GiftDrawer_t3411291385::get_offset_of_testTex_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (U3CYFadeOutU3Ec__Iterator0_t3483403400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[7] = 
{
	U3CYFadeOutU3Ec__Iterator0_t3483403400::get_offset_of_U3CstartPosU3E__0_0(),
	U3CYFadeOutU3Ec__Iterator0_t3483403400::get_offset_of_U3CendPosU3E__0_1(),
	U3CYFadeOutU3Ec__Iterator0_t3483403400::get_offset_of_U3CcountU3E__0_2(),
	U3CYFadeOutU3Ec__Iterator0_t3483403400::get_offset_of_U24this_3(),
	U3CYFadeOutU3Ec__Iterator0_t3483403400::get_offset_of_U24current_4(),
	U3CYFadeOutU3Ec__Iterator0_t3483403400::get_offset_of_U24disposing_5(),
	U3CYFadeOutU3Ec__Iterator0_t3483403400::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (U3CYFadeInU3Ec__Iterator1_t129333632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[7] = 
{
	U3CYFadeInU3Ec__Iterator1_t129333632::get_offset_of_U3CstartPosU3E__0_0(),
	U3CYFadeInU3Ec__Iterator1_t129333632::get_offset_of_U3CendPosU3E__0_1(),
	U3CYFadeInU3Ec__Iterator1_t129333632::get_offset_of_U3CcountU3E__0_2(),
	U3CYFadeInU3Ec__Iterator1_t129333632::get_offset_of_U24this_3(),
	U3CYFadeInU3Ec__Iterator1_t129333632::get_offset_of_U24current_4(),
	U3CYFadeInU3Ec__Iterator1_t129333632::get_offset_of_U24disposing_5(),
	U3CYFadeInU3Ec__Iterator1_t129333632::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (Testscript_t905621679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[21] = 
{
	Testscript_t905621679::get_offset_of_waitTime_2(),
	Testscript_t905621679::get_offset_of_wrapSprite_3(),
	Testscript_t905621679::get_offset_of_giftSprite_4(),
	Testscript_t905621679::get_offset_of_material_5(),
	Testscript_t905621679::get_offset_of_texwidth_6(),
	Testscript_t905621679::get_offset_of_texheight_7(),
	Testscript_t905621679::get_offset_of_size_8(),
	Testscript_t905621679::get_offset_of_width_9(),
	Testscript_t905621679::get_offset_of_height_10(),
	Testscript_t905621679::get_offset_of_minbordx_11(),
	Testscript_t905621679::get_offset_of_minbordy_12(),
	Testscript_t905621679::get_offset_of_maxbordx_13(),
	Testscript_t905621679::get_offset_of_maxbordy_14(),
	Testscript_t905621679::get_offset_of_texture_15(),
	Testscript_t905621679::get_offset_of_img_16(),
	Testscript_t905621679::get_offset_of_oldPos_17(),
	Testscript_t905621679::get_offset_of_pos_18(),
	Testscript_t905621679::get_offset_of_count_19(),
	Testscript_t905621679::get_offset_of_testTex_20(),
	Testscript_t905621679::get_offset_of_clear_21(),
	Testscript_t905621679::get_offset_of_clearMaterial_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (DraggableOrnament_t3124044711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[5] = 
{
	DraggableOrnament_t3124044711::get_offset_of_beenUsed_2(),
	DraggableOrnament_t3124044711::get_offset_of_number_3(),
	DraggableOrnament_t3124044711::get_offset_of_overlapCollider_4(),
	DraggableOrnament_t3124044711::get_offset_of_canClick_5(),
	DraggableOrnament_t3124044711::get_offset_of_light_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (U3CYFollowMouseU3Ec__Iterator0_t4055350718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[5] = 
{
	U3CYFollowMouseU3Ec__Iterator0_t4055350718::get_offset_of_U3CnewPosU3E__1_0(),
	U3CYFollowMouseU3Ec__Iterator0_t4055350718::get_offset_of_U24this_1(),
	U3CYFollowMouseU3Ec__Iterator0_t4055350718::get_offset_of_U24current_2(),
	U3CYFollowMouseU3Ec__Iterator0_t4055350718::get_offset_of_U24disposing_3(),
	U3CYFollowMouseU3Ec__Iterator0_t4055350718::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (U3CYLightUpU3Ec__Iterator1_t4139173986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2054[6] = 
{
	U3CYLightUpU3Ec__Iterator1_t4139173986::get_offset_of_U3CtimerU3E__0_0(),
	U3CYLightUpU3Ec__Iterator1_t4139173986::get_offset_of_U3CnewColorU3E__1_1(),
	U3CYLightUpU3Ec__Iterator1_t4139173986::get_offset_of_U24this_2(),
	U3CYLightUpU3Ec__Iterator1_t4139173986::get_offset_of_U24current_3(),
	U3CYLightUpU3Ec__Iterator1_t4139173986::get_offset_of_U24disposing_4(),
	U3CYLightUpU3Ec__Iterator1_t4139173986::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (GameTwentytwoManager_t3731809278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[11] = 
{
	GameTwentytwoManager_t3731809278::get_offset_of_numberOneText_4(),
	GameTwentytwoManager_t3731809278::get_offset_of_numberTwoText_5(),
	GameTwentytwoManager_t3731809278::get_offset_of_operatorText_6(),
	GameTwentytwoManager_t3731809278::get_offset_of_ornaments_7(),
	GameTwentytwoManager_t3731809278::get_offset_of_junkOrnaments_8(),
	GameTwentytwoManager_t3731809278::get_offset_of_ornamentsHolder_9(),
	GameTwentytwoManager_t3731809278::get_offset_of_targetSockets_10(),
	GameTwentytwoManager_t3731809278::get_offset_of_star_11(),
	GameTwentytwoManager_t3731809278::get_offset_of_result_12(),
	GameTwentytwoManager_t3731809278::get_offset_of_numberOne_13(),
	GameTwentytwoManager_t3731809278::get_offset_of_numberTwo_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (ClickObject_t2003887325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2056[4] = 
{
	ClickObject_t2003887325::get_offset_of_type_2(),
	ClickObject_t2003887325::get_offset_of_num_3(),
	ClickObject_t2003887325::get_offset_of__order_4(),
	ClickObject_t2003887325::get_offset_of__sprite_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (ClickType_t4062750492)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2057[5] = 
{
	ClickType_t4062750492::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (GameTwentythreeManager_t2915129944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[25] = 
{
	GameTwentythreeManager_t2915129944::get_offset_of_backgrounds_2(),
	GameTwentythreeManager_t2915129944::get_offset_of_tree_3(),
	GameTwentythreeManager_t2915129944::get_offset_of_text_4(),
	GameTwentythreeManager_t2915129944::get_offset_of_mask_5(),
	GameTwentythreeManager_t2915129944::get_offset_of_treeMask_6(),
	GameTwentythreeManager_t2915129944::get_offset_of_swipeSpeed_7(),
	GameTwentythreeManager_t2915129944::get_offset_of_nextButton_8(),
	GameTwentythreeManager_t2915129944::get_offset_of_shareButton_9(),
	GameTwentythreeManager_t2915129944::get_offset_of_exitButton_10(),
	GameTwentythreeManager_t2915129944::get_offset_of_gameLogo_11(),
	GameTwentythreeManager_t2915129944::get_offset_of_treeBG_12(),
	GameTwentythreeManager_t2915129944::get_offset_of_tutorial_13(),
	GameTwentythreeManager_t2915129944::get_offset_of_fp_14(),
	GameTwentythreeManager_t2915129944::get_offset_of_lp_15(),
	GameTwentythreeManager_t2915129944::get_offset_of_dragDistance_16(),
	GameTwentythreeManager_t2915129944::get_offset_of_canSwipe_17(),
	GameTwentythreeManager_t2915129944::get_offset_of_canDrag_18(),
	GameTwentythreeManager_t2915129944::get_offset_of_canClick_19(),
	GameTwentythreeManager_t2915129944::get_offset_of_bg_20(),
	GameTwentythreeManager_t2915129944::get_offset_of_ornaments_21(),
	GameTwentythreeManager_t2915129944::get_offset_of_dragObj_22(),
	GameTwentythreeManager_t2915129944::get_offset_of_lastClick_23(),
	GameTwentythreeManager_t2915129944::get_offset_of_spawners_24(),
	GameTwentythreeManager_t2915129944::get_offset_of_curImage_25(),
	GameTwentythreeManager_t2915129944::get_offset_of_curStage_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (U3CYSwipeU3Ec__Iterator0_t3099782161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[9] = 
{
	U3CYSwipeU3Ec__Iterator0_t3099782161::get_offset_of_U3CnextImageU3E__0_0(),
	U3CYSwipeU3Ec__Iterator0_t3099782161::get_offset_of_U3CstartPosU3E__0_1(),
	U3CYSwipeU3Ec__Iterator0_t3099782161::get_offset_of_U3CendPosU3E__0_2(),
	U3CYSwipeU3Ec__Iterator0_t3099782161::get_offset_of_left_3(),
	U3CYSwipeU3Ec__Iterator0_t3099782161::get_offset_of_U3CcountU3E__0_4(),
	U3CYSwipeU3Ec__Iterator0_t3099782161::get_offset_of_U24this_5(),
	U3CYSwipeU3Ec__Iterator0_t3099782161::get_offset_of_U24current_6(),
	U3CYSwipeU3Ec__Iterator0_t3099782161::get_offset_of_U24disposing_7(),
	U3CYSwipeU3Ec__Iterator0_t3099782161::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (U3CdelayedShareU3Ec__Iterator1_t2531885572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[6] = 
{
	U3CdelayedShareU3Ec__Iterator1_t2531885572::get_offset_of_screenShotPath_0(),
	U3CdelayedShareU3Ec__Iterator1_t2531885572::get_offset_of_text_1(),
	U3CdelayedShareU3Ec__Iterator1_t2531885572::get_offset_of_U24this_2(),
	U3CdelayedShareU3Ec__Iterator1_t2531885572::get_offset_of_U24current_3(),
	U3CdelayedShareU3Ec__Iterator1_t2531885572::get_offset_of_U24disposing_4(),
	U3CdelayedShareU3Ec__Iterator1_t2531885572::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (GiftSpawner_t1625374518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[4] = 
{
	GiftSpawner_t1625374518::get_offset_of_prefab_2(),
	GiftSpawner_t1625374518::get_offset_of_spawnNum_3(),
	GiftSpawner_t1625374518::get_offset_of_curspawn_4(),
	GiftSpawner_t1625374518::get_offset_of_temp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (RepeatingBackground_t2924144931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[2] = 
{
	RepeatingBackground_t2924144931::get_offset_of_boxCollider_2(),
	RepeatingBackground_t2924144931::get_offset_of_backgroundLenght_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (Game24Manager_t283638021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[5] = 
{
	Game24Manager_t283638021::get_offset_of_santa_4(),
	Game24Manager_t283638021::get_offset_of_giftManager_5(),
	Game24Manager_t283638021::get_offset_of_backgroundSpeed_6(),
	Game24Manager_t283638021::get_offset_of_obstacleSpeed_7(),
	Game24Manager_t283638021::get_offset_of_isPlaying_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (Gift_t450896550), -1, sizeof(Gift_t450896550_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2064[4] = 
{
	Gift_t450896550::get_offset_of_force_2(),
	Gift_t450896550::get_offset_of_yeyPosition_3(),
	Gift_t450896550::get_offset_of_rb_4(),
	Gift_t450896550_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (GiftManager_t2451335245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[5] = 
{
	GiftManager_t2451335245::get_offset_of_spawnPostion_2(),
	GiftManager_t2451335245::get_offset_of_yeyPrefab_3(),
	GiftManager_t2451335245::get_offset_of_gifts_4(),
	GiftManager_t2451335245::get_offset_of_canGiveGift_5(),
	GiftManager_t2451335245::get_offset_of_currentIndex_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (U3CThrownU3Ec__Iterator0_t974249205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[8] = 
{
	U3CThrownU3Ec__Iterator0_t974249205::get_offset_of_gift_0(),
	U3CThrownU3Ec__Iterator0_t974249205::get_offset_of_U3CtoSpawnU3E__0_1(),
	U3CThrownU3Ec__Iterator0_t974249205::get_offset_of_position_2(),
	U3CThrownU3Ec__Iterator0_t974249205::get_offset_of_U3CyeyU3E__0_3(),
	U3CThrownU3Ec__Iterator0_t974249205::get_offset_of_U24this_4(),
	U3CThrownU3Ec__Iterator0_t974249205::get_offset_of_U24current_5(),
	U3CThrownU3Ec__Iterator0_t974249205::get_offset_of_U24disposing_6(),
	U3CThrownU3Ec__Iterator0_t974249205::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (GiftSpawnPoint_t604323357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (Yey_t2293551375), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (U3CScaleU3Ec__AnonStorey0_t1001303329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[1] = 
{
	U3CScaleU3Ec__AnonStorey0_t1001303329::get_offset_of_t_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (ObjectScroller_t389948937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[3] = 
{
	ObjectScroller_t389948937::get_offset_of_rb_2(),
	ObjectScroller_t389948937::get_offset_of_desiredVelocity_3(),
	ObjectScroller_t389948937::get_offset_of_isScrolling_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (U3CYStopScrollU3Ec__Iterator0_t632694609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[5] = 
{
	U3CYStopScrollU3Ec__Iterator0_t632694609::get_offset_of_U3CvelocityXU3E__1_0(),
	U3CYStopScrollU3Ec__Iterator0_t632694609::get_offset_of_U24this_1(),
	U3CYStopScrollU3Ec__Iterator0_t632694609::get_offset_of_U24current_2(),
	U3CYStopScrollU3Ec__Iterator0_t632694609::get_offset_of_U24disposing_3(),
	U3CYStopScrollU3Ec__Iterator0_t632694609::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (Bird_t1163357383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (Building_t2809767176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (ObstacleBase_t365467716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2074[2] = 
{
	ObstacleBase_t365467716::get_offset_of_minY_2(),
	ObstacleBase_t365467716::get_offset_of_maxY_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (ObstacleManager_t3466292636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[5] = 
{
	ObstacleManager_t3466292636::get_offset_of_positionX_2(),
	ObstacleManager_t3466292636::get_offset_of_spawnTime_3(),
	ObstacleManager_t3466292636::get_offset_of_obstacles_4(),
	ObstacleManager_t3466292636::get_offset_of_currentIndex_5(),
	ObstacleManager_t3466292636::get_offset_of_lastTimeSpawned_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[5] = 
{
	U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804::get_offset_of_obj_0(),
	U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804::get_offset_of_U24current_1(),
	U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804::get_offset_of_U24disposing_2(),
	U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804::get_offset_of_U24PC_3(),
	U3CRecycleSpawnedObstacleU3Ec__Iterator0_t1754565804::get_offset_of_U24locvar0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (U3CRecycleSpawnedObstacleU3Ec__AnonStorey1_t840817611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[1] = 
{
	U3CRecycleSpawnedObstacleU3Ec__AnonStorey1_t840817611::get_offset_of_obj_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (Tree_t2595185816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (Santa_t3804168179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[9] = 
{
	Santa_t3804168179::get_offset_of_force_2(),
	Santa_t3804168179::get_offset_of_ps_3(),
	Santa_t3804168179::get_offset_of_particlePosition_4(),
	Santa_t3804168179::get_offset_of_rb_5(),
	Santa_t3804168179::get_offset_of_flash_6(),
	Santa_t3804168179::get_offset_of_wasHit_7(),
	Santa_t3804168179::get_offset_of_isAtTop_8(),
	Santa_t3804168179::get_offset_of_cameraRect_9(),
	Santa_t3804168179::get_offset_of_santaBounds_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (U3CRecylceParticleU3Ec__Iterator0_t3066034944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2080[5] = 
{
	U3CRecylceParticleU3Ec__Iterator0_t3066034944::get_offset_of_obj_0(),
	U3CRecylceParticleU3Ec__Iterator0_t3066034944::get_offset_of_U24this_1(),
	U3CRecylceParticleU3Ec__Iterator0_t3066034944::get_offset_of_U24current_2(),
	U3CRecylceParticleU3Ec__Iterator0_t3066034944::get_offset_of_U24disposing_3(),
	U3CRecylceParticleU3Ec__Iterator0_t3066034944::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (Timer_t2917042437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[3] = 
{
	Timer_t2917042437::get_offset_of_minutesToPlay_2(),
	Timer_t2917042437::get_offset_of_timeToPlay_3(),
	Timer_t2917042437::get_offset_of_timer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (GameThreeManager_t1469036085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[12] = 
{
	GameThreeManager_t1469036085::get_offset_of_isPlaying_4(),
	GameThreeManager_t1469036085::get_offset_of_ringsToToss_5(),
	GameThreeManager_t1469036085::get_offset_of_landedRingsCounter_6(),
	GameThreeManager_t1469036085::get_offset_of_minSpawnRate_7(),
	GameThreeManager_t1469036085::get_offset_of_maxSpawnRate_8(),
	GameThreeManager_t1469036085::get_offset_of_ring_9(),
	GameThreeManager_t1469036085::get_offset_of_ringSpawnPos_10(),
	GameThreeManager_t1469036085::get_offset_of_ringScale_11(),
	GameThreeManager_t1469036085::get_offset_of_hats_12(),
	GameThreeManager_t1469036085::get_offset_of_numberOfStandingHats_13(),
	GameThreeManager_t1469036085::get_offset_of_standUpRate_14(),
	GameThreeManager_t1469036085::get_offset_of_ringScript_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (U3CYHatControllerU3Ec__Iterator0_t2147178273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2083[5] = 
{
	U3CYHatControllerU3Ec__Iterator0_t2147178273::get_offset_of_U3CtimerU3E__0_0(),
	U3CYHatControllerU3Ec__Iterator0_t2147178273::get_offset_of_U24this_1(),
	U3CYHatControllerU3Ec__Iterator0_t2147178273::get_offset_of_U24current_2(),
	U3CYHatControllerU3Ec__Iterator0_t2147178273::get_offset_of_U24disposing_3(),
	U3CYHatControllerU3Ec__Iterator0_t2147178273::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (HatEnabler_t1910487184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[3] = 
{
	HatEnabler_t1910487184::get_offset_of_sleepingState_2(),
	HatEnabler_t1910487184::get_offset_of_standingState_3(),
	HatEnabler_t1910487184::get_offset_of_isStanding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (RingTossController_t956255555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2085[12] = 
{
	RingTossController_t956255555::get_offset_of_canToss_2(),
	RingTossController_t956255555::get_offset_of_force_3(),
	RingTossController_t956255555::get_offset_of_topRing_4(),
	RingTossController_t956255555::get_offset_of_col1_5(),
	RingTossController_t956255555::get_offset_of_col2_6(),
	RingTossController_t956255555::get_offset_of_rb2d_7(),
	RingTossController_t956255555::get_offset_of_startPos_8(),
	RingTossController_t956255555::get_offset_of_endPos_9(),
	RingTossController_t956255555::get_offset_of_hat_10(),
	RingTossController_t956255555::get_offset_of_hasCollided_11(),
	RingTossController_t956255555::get_offset_of_toSleep_12(),
	RingTossController_t956255555::get_offset_of_effectors_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (U3CYSleepHatU3Ec__Iterator0_t412436362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[5] = 
{
	U3CYSleepHatU3Ec__Iterator0_t412436362::get_offset_of_collision_0(),
	U3CYSleepHatU3Ec__Iterator0_t412436362::get_offset_of_U24this_1(),
	U3CYSleepHatU3Ec__Iterator0_t412436362::get_offset_of_U24current_2(),
	U3CYSleepHatU3Ec__Iterator0_t412436362::get_offset_of_U24disposing_3(),
	U3CYSleepHatU3Ec__Iterator0_t412436362::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (GameFourManager_t2429316785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[20] = 
{
	GameFourManager_t2429316785::get_offset_of_pencil_4(),
	GameFourManager_t2429316785::get_offset_of_drawingBoardsHolder_5(),
	GameFourManager_t2429316785::get_offset_of_drawingsHolder_6(),
	GameFourManager_t2429316785::get_offset_of_correctColliders_7(),
	GameFourManager_t2429316785::get_offset_of_finishedBody_8(),
	GameFourManager_t2429316785::get_offset_of_finishedHead_9(),
	GameFourManager_t2429316785::get_offset_of_exampleBody_10(),
	GameFourManager_t2429316785::get_offset_of_exampleHead_11(),
	GameFourManager_t2429316785::get_offset_of_numberOfDrawingsToMake_12(),
	GameFourManager_t2429316785::get_offset_of_numberOfDrawingsDone_13(),
	GameFourManager_t2429316785::get_offset_of_correctCollidersLeft_14(),
	GameFourManager_t2429316785::get_offset_of_wrongCollidersHit_15(),
	GameFourManager_t2429316785::get_offset_of_canDraw_16(),
	GameFourManager_t2429316785::get_offset_of_shapesDrawn_17(),
	GameFourManager_t2429316785::get_offset_of_bodyIndex_18(),
	GameFourManager_t2429316785::get_offset_of_headIndex_19(),
	GameFourManager_t2429316785::get_offset_of_shapeToDraw_20(),
	GameFourManager_t2429316785::get_offset_of_currentBoard_21(),
	GameFourManager_t2429316785::get_offset_of_iPencil_22(),
	GameFourManager_t2429316785::get_offset_of_finishedShape_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (U3CYStartNextDrawingU3Ec__Iterator0_t4224780971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[4] = 
{
	U3CYStartNextDrawingU3Ec__Iterator0_t4224780971::get_offset_of_U24this_0(),
	U3CYStartNextDrawingU3Ec__Iterator0_t4224780971::get_offset_of_U24current_1(),
	U3CYStartNextDrawingU3Ec__Iterator0_t4224780971::get_offset_of_U24disposing_2(),
	U3CYStartNextDrawingU3Ec__Iterator0_t4224780971::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (Pencil_t4115486279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[2] = 
{
	Pencil_t4115486279::get_offset_of_drawLoopSound_2(),
	Pencil_t4115486279::get_offset_of_toMove_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (Cleaner_t1604682602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[3] = 
{
	Cleaner_t1604682602::get_offset_of_cleanSound_2(),
	Cleaner_t1604682602::get_offset_of_currentCollider_3(),
	Cleaner_t1604682602::get_offset_of_smokePuff_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (U3CYFollowMouseU3Ec__Iterator0_t1570186929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[5] = 
{
	U3CYFollowMouseU3Ec__Iterator0_t1570186929::get_offset_of_U3CtimerU3E__0_0(),
	U3CYFollowMouseU3Ec__Iterator0_t1570186929::get_offset_of_U24this_1(),
	U3CYFollowMouseU3Ec__Iterator0_t1570186929::get_offset_of_U24current_2(),
	U3CYFollowMouseU3Ec__Iterator0_t1570186929::get_offset_of_U24disposing_3(),
	U3CYFollowMouseU3Ec__Iterator0_t1570186929::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (Dirt_t450896597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[2] = 
{
	Dirt_t450896597::get_offset_of_cleanTime_2(),
	Dirt_t450896597::get_offset_of_U3CTotalCleanTimeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (GameFiveManager_t3955517211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[10] = 
{
	GameFiveManager_t3955517211::get_offset_of_isPlaying_4(),
	GameFiveManager_t3955517211::get_offset_of_bootColorsMale_5(),
	GameFiveManager_t3955517211::get_offset_of_bootColorsFemale_6(),
	GameFiveManager_t3955517211::get_offset_of_femaleBoot_7(),
	GameFiveManager_t3955517211::get_offset_of_maleBoot_8(),
	GameFiveManager_t3955517211::get_offset_of_numberOfDirtFlecks_9(),
	GameFiveManager_t3955517211::get_offset_of_dirtFlecksCleaned_10(),
	GameFiveManager_t3955517211::get_offset_of_cleaner_11(),
	GameFiveManager_t3955517211::get_offset_of_iCleaner_12(),
	GameFiveManager_t3955517211::get_offset_of_lastMousePos_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (U3CYCleanDirtU3Ec__Iterator0_t2454611604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[5] = 
{
	U3CYCleanDirtU3Ec__Iterator0_t2454611604::get_offset_of_dirt_0(),
	U3CYCleanDirtU3Ec__Iterator0_t2454611604::get_offset_of_U24this_1(),
	U3CYCleanDirtU3Ec__Iterator0_t2454611604::get_offset_of_U24current_2(),
	U3CYCleanDirtU3Ec__Iterator0_t2454611604::get_offset_of_U24disposing_3(),
	U3CYCleanDirtU3Ec__Iterator0_t2454611604::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (Candy_t221644187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[1] = 
{
	Candy_t221644187::get_offset_of_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (Type_t2899008702)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2096[3] = 
{
	Type_t2899008702::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (GameSixManager_t2271637217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2097[12] = 
{
	GameSixManager_t2271637217::get_offset_of_isPlaying_4(),
	GameSixManager_t2271637217::get_offset_of_candiesToCollect_5(),
	GameSixManager_t2271637217::get_offset_of_candiesCollected_6(),
	GameSixManager_t2271637217::get_offset_of_chanceToSpawnCoal_7(),
	GameSixManager_t2271637217::get_offset_of_xSpawnLimit_8(),
	GameSixManager_t2271637217::get_offset_of_candySpawnRate_9(),
	GameSixManager_t2271637217::get_offset_of_candySpawnHeight_10(),
	GameSixManager_t2271637217::get_offset_of_candySprites_11(),
	GameSixManager_t2271637217::get_offset_of_boot_12(),
	GameSixManager_t2271637217::get_offset_of_candy_13(),
	GameSixManager_t2271637217::get_offset_of_coal_14(),
	GameSixManager_t2271637217::get_offset_of_bootCoroutine_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (U3CYFollowMouseU3Ec__Iterator0_t1347612948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[4] = 
{
	U3CYFollowMouseU3Ec__Iterator0_t1347612948::get_offset_of_U24this_0(),
	U3CYFollowMouseU3Ec__Iterator0_t1347612948::get_offset_of_U24current_1(),
	U3CYFollowMouseU3Ec__Iterator0_t1347612948::get_offset_of_U24disposing_2(),
	U3CYFollowMouseU3Ec__Iterator0_t1347612948::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (U3CYCandySpawnerU3Ec__Iterator1_t2324817448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[5] = 
{
	U3CYCandySpawnerU3Ec__Iterator1_t2324817448::get_offset_of_U3CtimerU3E__0_0(),
	U3CYCandySpawnerU3Ec__Iterator1_t2324817448::get_offset_of_U24this_1(),
	U3CYCandySpawnerU3Ec__Iterator1_t2324817448::get_offset_of_U24current_2(),
	U3CYCandySpawnerU3Ec__Iterator1_t2324817448::get_offset_of_U24disposing_3(),
	U3CYCandySpawnerU3Ec__Iterator1_t2324817448::get_offset_of_U24PC_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
