﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen4236790963.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// Note
struct Note_t686167636;
// Song[]
struct SongU5BU5D_t3258851058;
// Song
struct Song_t1848967459;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameNineteenManager
struct  GameNineteenManager_t942308577  : public SingletonBase_1_t4236790963
{
public:
	// System.Single GameNineteenManager::noteInterval
	float ___noteInterval_4;
	// System.Int32 GameNineteenManager::currentNote
	int32_t ___currentNote_5;
	// System.Int32 GameNineteenManager::feedbackIntervalCounter
	int32_t ___feedbackIntervalCounter_6;
	// System.Int32 GameNineteenManager::feedbackInterval
	int32_t ___feedbackInterval_7;
	// System.Boolean GameNineteenManager::toHappy
	bool ___toHappy_8;
	// System.Boolean GameNineteenManager::canSetMood
	bool ___canSetMood_9;
	// UnityEngine.GameObject GameNineteenManager::note
	GameObject_t1756533147 * ___note_10;
	// UnityEngine.GameObject GameNineteenManager::correctParticle
	GameObject_t1756533147 * ___correctParticle_11;
	// UnityEngine.Transform GameNineteenManager::startPosition
	Transform_t3275118058 * ___startPosition_12;
	// UnityEngine.Color[] GameNineteenManager::noteColors
	ColorU5BU5D_t672350442* ___noteColors_13;
	// Note GameNineteenManager::noteToPress
	Note_t686167636 * ___noteToPress_14;
	// Song[] GameNineteenManager::songs
	SongU5BU5D_t3258851058* ___songs_15;
	// Song GameNineteenManager::currentSong
	Song_t1848967459 * ___currentSong_16;

public:
	inline static int32_t get_offset_of_noteInterval_4() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___noteInterval_4)); }
	inline float get_noteInterval_4() const { return ___noteInterval_4; }
	inline float* get_address_of_noteInterval_4() { return &___noteInterval_4; }
	inline void set_noteInterval_4(float value)
	{
		___noteInterval_4 = value;
	}

	inline static int32_t get_offset_of_currentNote_5() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___currentNote_5)); }
	inline int32_t get_currentNote_5() const { return ___currentNote_5; }
	inline int32_t* get_address_of_currentNote_5() { return &___currentNote_5; }
	inline void set_currentNote_5(int32_t value)
	{
		___currentNote_5 = value;
	}

	inline static int32_t get_offset_of_feedbackIntervalCounter_6() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___feedbackIntervalCounter_6)); }
	inline int32_t get_feedbackIntervalCounter_6() const { return ___feedbackIntervalCounter_6; }
	inline int32_t* get_address_of_feedbackIntervalCounter_6() { return &___feedbackIntervalCounter_6; }
	inline void set_feedbackIntervalCounter_6(int32_t value)
	{
		___feedbackIntervalCounter_6 = value;
	}

	inline static int32_t get_offset_of_feedbackInterval_7() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___feedbackInterval_7)); }
	inline int32_t get_feedbackInterval_7() const { return ___feedbackInterval_7; }
	inline int32_t* get_address_of_feedbackInterval_7() { return &___feedbackInterval_7; }
	inline void set_feedbackInterval_7(int32_t value)
	{
		___feedbackInterval_7 = value;
	}

	inline static int32_t get_offset_of_toHappy_8() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___toHappy_8)); }
	inline bool get_toHappy_8() const { return ___toHappy_8; }
	inline bool* get_address_of_toHappy_8() { return &___toHappy_8; }
	inline void set_toHappy_8(bool value)
	{
		___toHappy_8 = value;
	}

	inline static int32_t get_offset_of_canSetMood_9() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___canSetMood_9)); }
	inline bool get_canSetMood_9() const { return ___canSetMood_9; }
	inline bool* get_address_of_canSetMood_9() { return &___canSetMood_9; }
	inline void set_canSetMood_9(bool value)
	{
		___canSetMood_9 = value;
	}

	inline static int32_t get_offset_of_note_10() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___note_10)); }
	inline GameObject_t1756533147 * get_note_10() const { return ___note_10; }
	inline GameObject_t1756533147 ** get_address_of_note_10() { return &___note_10; }
	inline void set_note_10(GameObject_t1756533147 * value)
	{
		___note_10 = value;
		Il2CppCodeGenWriteBarrier(&___note_10, value);
	}

	inline static int32_t get_offset_of_correctParticle_11() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___correctParticle_11)); }
	inline GameObject_t1756533147 * get_correctParticle_11() const { return ___correctParticle_11; }
	inline GameObject_t1756533147 ** get_address_of_correctParticle_11() { return &___correctParticle_11; }
	inline void set_correctParticle_11(GameObject_t1756533147 * value)
	{
		___correctParticle_11 = value;
		Il2CppCodeGenWriteBarrier(&___correctParticle_11, value);
	}

	inline static int32_t get_offset_of_startPosition_12() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___startPosition_12)); }
	inline Transform_t3275118058 * get_startPosition_12() const { return ___startPosition_12; }
	inline Transform_t3275118058 ** get_address_of_startPosition_12() { return &___startPosition_12; }
	inline void set_startPosition_12(Transform_t3275118058 * value)
	{
		___startPosition_12 = value;
		Il2CppCodeGenWriteBarrier(&___startPosition_12, value);
	}

	inline static int32_t get_offset_of_noteColors_13() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___noteColors_13)); }
	inline ColorU5BU5D_t672350442* get_noteColors_13() const { return ___noteColors_13; }
	inline ColorU5BU5D_t672350442** get_address_of_noteColors_13() { return &___noteColors_13; }
	inline void set_noteColors_13(ColorU5BU5D_t672350442* value)
	{
		___noteColors_13 = value;
		Il2CppCodeGenWriteBarrier(&___noteColors_13, value);
	}

	inline static int32_t get_offset_of_noteToPress_14() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___noteToPress_14)); }
	inline Note_t686167636 * get_noteToPress_14() const { return ___noteToPress_14; }
	inline Note_t686167636 ** get_address_of_noteToPress_14() { return &___noteToPress_14; }
	inline void set_noteToPress_14(Note_t686167636 * value)
	{
		___noteToPress_14 = value;
		Il2CppCodeGenWriteBarrier(&___noteToPress_14, value);
	}

	inline static int32_t get_offset_of_songs_15() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___songs_15)); }
	inline SongU5BU5D_t3258851058* get_songs_15() const { return ___songs_15; }
	inline SongU5BU5D_t3258851058** get_address_of_songs_15() { return &___songs_15; }
	inline void set_songs_15(SongU5BU5D_t3258851058* value)
	{
		___songs_15 = value;
		Il2CppCodeGenWriteBarrier(&___songs_15, value);
	}

	inline static int32_t get_offset_of_currentSong_16() { return static_cast<int32_t>(offsetof(GameNineteenManager_t942308577, ___currentSong_16)); }
	inline Song_t1848967459 * get_currentSong_16() const { return ___currentSong_16; }
	inline Song_t1848967459 ** get_address_of_currentSong_16() { return &___currentSong_16; }
	inline void set_currentSong_16(Song_t1848967459 * value)
	{
		___currentSong_16 = value;
		Il2CppCodeGenWriteBarrier(&___currentSong_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
