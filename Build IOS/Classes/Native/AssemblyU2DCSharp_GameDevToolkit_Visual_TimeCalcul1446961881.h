﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Visual.TimeCalculator
struct  TimeCalculator_t1446961881  : public Il2CppObject
{
public:
	// System.Single GameDevToolkit.Visual.TimeCalculator::_distance
	float ____distance_0;
	// System.Single GameDevToolkit.Visual.TimeCalculator::_time
	float ____time_1;

public:
	inline static int32_t get_offset_of__distance_0() { return static_cast<int32_t>(offsetof(TimeCalculator_t1446961881, ____distance_0)); }
	inline float get__distance_0() const { return ____distance_0; }
	inline float* get_address_of__distance_0() { return &____distance_0; }
	inline void set__distance_0(float value)
	{
		____distance_0 = value;
	}

	inline static int32_t get_offset_of__time_1() { return static_cast<int32_t>(offsetof(TimeCalculator_t1446961881, ____time_1)); }
	inline float get__time_1() const { return ____time_1; }
	inline float* get_address_of__time_1() { return &____time_1; }
	inline void set__time_1(float value)
	{
		____time_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
