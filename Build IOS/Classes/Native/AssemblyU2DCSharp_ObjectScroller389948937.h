﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectScroller
struct  ObjectScroller_t389948937  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rigidbody2D ObjectScroller::rb
	Rigidbody2D_t502193897 * ___rb_2;
	// UnityEngine.Vector2 ObjectScroller::desiredVelocity
	Vector2_t2243707579  ___desiredVelocity_3;
	// System.Boolean ObjectScroller::isScrolling
	bool ___isScrolling_4;

public:
	inline static int32_t get_offset_of_rb_2() { return static_cast<int32_t>(offsetof(ObjectScroller_t389948937, ___rb_2)); }
	inline Rigidbody2D_t502193897 * get_rb_2() const { return ___rb_2; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb_2() { return &___rb_2; }
	inline void set_rb_2(Rigidbody2D_t502193897 * value)
	{
		___rb_2 = value;
		Il2CppCodeGenWriteBarrier(&___rb_2, value);
	}

	inline static int32_t get_offset_of_desiredVelocity_3() { return static_cast<int32_t>(offsetof(ObjectScroller_t389948937, ___desiredVelocity_3)); }
	inline Vector2_t2243707579  get_desiredVelocity_3() const { return ___desiredVelocity_3; }
	inline Vector2_t2243707579 * get_address_of_desiredVelocity_3() { return &___desiredVelocity_3; }
	inline void set_desiredVelocity_3(Vector2_t2243707579  value)
	{
		___desiredVelocity_3 = value;
	}

	inline static int32_t get_offset_of_isScrolling_4() { return static_cast<int32_t>(offsetof(ObjectScroller_t389948937, ___isScrolling_4)); }
	inline bool get_isScrolling_4() const { return ___isScrolling_4; }
	inline bool* get_address_of_isScrolling_4() { return &___isScrolling_4; }
	inline void set_isScrolling_4(bool value)
	{
		___isScrolling_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
