﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// Slide
struct Slide_t3421086917;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SlideShow
struct  SlideShow_t1942539524  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite[] SlideShow::backgrounds
	SpriteU5BU5D_t3359083662* ___backgrounds_2;
	// Slide SlideShow::slide
	Slide_t3421086917 * ___slide_3;
	// System.Int32 SlideShow::bgCounter
	int32_t ___bgCounter_4;
	// System.Int32 SlideShow::sortOrder
	int32_t ___sortOrder_5;

public:
	inline static int32_t get_offset_of_backgrounds_2() { return static_cast<int32_t>(offsetof(SlideShow_t1942539524, ___backgrounds_2)); }
	inline SpriteU5BU5D_t3359083662* get_backgrounds_2() const { return ___backgrounds_2; }
	inline SpriteU5BU5D_t3359083662** get_address_of_backgrounds_2() { return &___backgrounds_2; }
	inline void set_backgrounds_2(SpriteU5BU5D_t3359083662* value)
	{
		___backgrounds_2 = value;
		Il2CppCodeGenWriteBarrier(&___backgrounds_2, value);
	}

	inline static int32_t get_offset_of_slide_3() { return static_cast<int32_t>(offsetof(SlideShow_t1942539524, ___slide_3)); }
	inline Slide_t3421086917 * get_slide_3() const { return ___slide_3; }
	inline Slide_t3421086917 ** get_address_of_slide_3() { return &___slide_3; }
	inline void set_slide_3(Slide_t3421086917 * value)
	{
		___slide_3 = value;
		Il2CppCodeGenWriteBarrier(&___slide_3, value);
	}

	inline static int32_t get_offset_of_bgCounter_4() { return static_cast<int32_t>(offsetof(SlideShow_t1942539524, ___bgCounter_4)); }
	inline int32_t get_bgCounter_4() const { return ___bgCounter_4; }
	inline int32_t* get_address_of_bgCounter_4() { return &___bgCounter_4; }
	inline void set_bgCounter_4(int32_t value)
	{
		___bgCounter_4 = value;
	}

	inline static int32_t get_offset_of_sortOrder_5() { return static_cast<int32_t>(offsetof(SlideShow_t1942539524, ___sortOrder_5)); }
	inline int32_t get_sortOrder_5() const { return ___sortOrder_5; }
	inline int32_t* get_address_of_sortOrder_5() { return &___sortOrder_5; }
	inline void set_sortOrder_5(int32_t value)
	{
		___sortOrder_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
