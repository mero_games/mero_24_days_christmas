﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,System.Boolean>
struct Dictionary_2_t3143676921;
// System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,UnityEngine.KeyCode>
struct Dictionary_2_t1601497355;
// System.Collections.Generic.List`1<UnityEngine.KeyCode>
struct List_1_t1652516284;
// GameDevToolkit.Events.KeyboardEventSystem
struct KeyboardEventSystem_t2538007188;
// GameDevToolkit.Events.KeyboardEventSystem/KeyboardDelegate
struct KeyboardDelegate_t882008899;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,UnityEngine.KeyCode>
struct Func_2_t1866569452;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Events.KeyboardEventSystem
struct  KeyboardEventSystem_t2538007188  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,System.Boolean> GameDevToolkit.Events.KeyboardEventSystem::_isPressed
	Dictionary_2_t3143676921 * ____isPressed_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.KeyCode,UnityEngine.KeyCode> GameDevToolkit.Events.KeyboardEventSystem::map
	Dictionary_2_t1601497355 * ___map_3;
	// System.Collections.Generic.List`1<UnityEngine.KeyCode> GameDevToolkit.Events.KeyboardEventSystem::keysToCheck
	List_1_t1652516284 * ___keysToCheck_4;
	// GameDevToolkit.Events.KeyboardEventSystem/KeyboardDelegate GameDevToolkit.Events.KeyboardEventSystem::KeyBoardEvent
	KeyboardDelegate_t882008899 * ___KeyBoardEvent_6;

public:
	inline static int32_t get_offset_of__isPressed_2() { return static_cast<int32_t>(offsetof(KeyboardEventSystem_t2538007188, ____isPressed_2)); }
	inline Dictionary_2_t3143676921 * get__isPressed_2() const { return ____isPressed_2; }
	inline Dictionary_2_t3143676921 ** get_address_of__isPressed_2() { return &____isPressed_2; }
	inline void set__isPressed_2(Dictionary_2_t3143676921 * value)
	{
		____isPressed_2 = value;
		Il2CppCodeGenWriteBarrier(&____isPressed_2, value);
	}

	inline static int32_t get_offset_of_map_3() { return static_cast<int32_t>(offsetof(KeyboardEventSystem_t2538007188, ___map_3)); }
	inline Dictionary_2_t1601497355 * get_map_3() const { return ___map_3; }
	inline Dictionary_2_t1601497355 ** get_address_of_map_3() { return &___map_3; }
	inline void set_map_3(Dictionary_2_t1601497355 * value)
	{
		___map_3 = value;
		Il2CppCodeGenWriteBarrier(&___map_3, value);
	}

	inline static int32_t get_offset_of_keysToCheck_4() { return static_cast<int32_t>(offsetof(KeyboardEventSystem_t2538007188, ___keysToCheck_4)); }
	inline List_1_t1652516284 * get_keysToCheck_4() const { return ___keysToCheck_4; }
	inline List_1_t1652516284 ** get_address_of_keysToCheck_4() { return &___keysToCheck_4; }
	inline void set_keysToCheck_4(List_1_t1652516284 * value)
	{
		___keysToCheck_4 = value;
		Il2CppCodeGenWriteBarrier(&___keysToCheck_4, value);
	}

	inline static int32_t get_offset_of_KeyBoardEvent_6() { return static_cast<int32_t>(offsetof(KeyboardEventSystem_t2538007188, ___KeyBoardEvent_6)); }
	inline KeyboardDelegate_t882008899 * get_KeyBoardEvent_6() const { return ___KeyBoardEvent_6; }
	inline KeyboardDelegate_t882008899 ** get_address_of_KeyBoardEvent_6() { return &___KeyBoardEvent_6; }
	inline void set_KeyBoardEvent_6(KeyboardDelegate_t882008899 * value)
	{
		___KeyBoardEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___KeyBoardEvent_6, value);
	}
};

struct KeyboardEventSystem_t2538007188_StaticFields
{
public:
	// GameDevToolkit.Events.KeyboardEventSystem GameDevToolkit.Events.KeyboardEventSystem::instance
	KeyboardEventSystem_t2538007188 * ___instance_5;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.KeyCode,UnityEngine.KeyCode>,UnityEngine.KeyCode> GameDevToolkit.Events.KeyboardEventSystem::<>f__am$cache0
	Func_2_t1866569452 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(KeyboardEventSystem_t2538007188_StaticFields, ___instance_5)); }
	inline KeyboardEventSystem_t2538007188 * get_instance_5() const { return ___instance_5; }
	inline KeyboardEventSystem_t2538007188 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(KeyboardEventSystem_t2538007188 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(KeyboardEventSystem_t2538007188_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t1866569452 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t1866569452 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t1866569452 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
