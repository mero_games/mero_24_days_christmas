﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<System.Object>
struct MouseDelegate_t702691374;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Events.GenericMouseEvent`1<System.Object>
struct  GenericMouseEvent_1_t2681505391  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Events.GenericMouseEvent`1/MouseDelegate<T> GameDevToolkit.Events.GenericMouseEvent`1::MouseEvent
	MouseDelegate_t702691374 * ___MouseEvent_3;
	// System.Boolean GameDevToolkit.Events.GenericMouseEvent`1::_entered
	bool ____entered_4;
	// System.Boolean GameDevToolkit.Events.GenericMouseEvent`1::_canProces
	bool ____canProces_5;
	// System.Boolean GameDevToolkit.Events.GenericMouseEvent`1::hasDoubleClick
	bool ___hasDoubleClick_6;
	// UnityEngine.Collider2D GameDevToolkit.Events.GenericMouseEvent`1::spriteCollider
	Collider2D_t646061738 * ___spriteCollider_7;
	// T GameDevToolkit.Events.GenericMouseEvent`1::actualTarget
	Il2CppObject * ___actualTarget_8;
	// System.Single GameDevToolkit.Events.GenericMouseEvent`1::lastClick
	float ___lastClick_9;
	// System.Boolean GameDevToolkit.Events.GenericMouseEvent`1::clickedOnce
	bool ___clickedOnce_10;

public:
	inline static int32_t get_offset_of_MouseEvent_3() { return static_cast<int32_t>(offsetof(GenericMouseEvent_1_t2681505391, ___MouseEvent_3)); }
	inline MouseDelegate_t702691374 * get_MouseEvent_3() const { return ___MouseEvent_3; }
	inline MouseDelegate_t702691374 ** get_address_of_MouseEvent_3() { return &___MouseEvent_3; }
	inline void set_MouseEvent_3(MouseDelegate_t702691374 * value)
	{
		___MouseEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___MouseEvent_3, value);
	}

	inline static int32_t get_offset_of__entered_4() { return static_cast<int32_t>(offsetof(GenericMouseEvent_1_t2681505391, ____entered_4)); }
	inline bool get__entered_4() const { return ____entered_4; }
	inline bool* get_address_of__entered_4() { return &____entered_4; }
	inline void set__entered_4(bool value)
	{
		____entered_4 = value;
	}

	inline static int32_t get_offset_of__canProces_5() { return static_cast<int32_t>(offsetof(GenericMouseEvent_1_t2681505391, ____canProces_5)); }
	inline bool get__canProces_5() const { return ____canProces_5; }
	inline bool* get_address_of__canProces_5() { return &____canProces_5; }
	inline void set__canProces_5(bool value)
	{
		____canProces_5 = value;
	}

	inline static int32_t get_offset_of_hasDoubleClick_6() { return static_cast<int32_t>(offsetof(GenericMouseEvent_1_t2681505391, ___hasDoubleClick_6)); }
	inline bool get_hasDoubleClick_6() const { return ___hasDoubleClick_6; }
	inline bool* get_address_of_hasDoubleClick_6() { return &___hasDoubleClick_6; }
	inline void set_hasDoubleClick_6(bool value)
	{
		___hasDoubleClick_6 = value;
	}

	inline static int32_t get_offset_of_spriteCollider_7() { return static_cast<int32_t>(offsetof(GenericMouseEvent_1_t2681505391, ___spriteCollider_7)); }
	inline Collider2D_t646061738 * get_spriteCollider_7() const { return ___spriteCollider_7; }
	inline Collider2D_t646061738 ** get_address_of_spriteCollider_7() { return &___spriteCollider_7; }
	inline void set_spriteCollider_7(Collider2D_t646061738 * value)
	{
		___spriteCollider_7 = value;
		Il2CppCodeGenWriteBarrier(&___spriteCollider_7, value);
	}

	inline static int32_t get_offset_of_actualTarget_8() { return static_cast<int32_t>(offsetof(GenericMouseEvent_1_t2681505391, ___actualTarget_8)); }
	inline Il2CppObject * get_actualTarget_8() const { return ___actualTarget_8; }
	inline Il2CppObject ** get_address_of_actualTarget_8() { return &___actualTarget_8; }
	inline void set_actualTarget_8(Il2CppObject * value)
	{
		___actualTarget_8 = value;
		Il2CppCodeGenWriteBarrier(&___actualTarget_8, value);
	}

	inline static int32_t get_offset_of_lastClick_9() { return static_cast<int32_t>(offsetof(GenericMouseEvent_1_t2681505391, ___lastClick_9)); }
	inline float get_lastClick_9() const { return ___lastClick_9; }
	inline float* get_address_of_lastClick_9() { return &___lastClick_9; }
	inline void set_lastClick_9(float value)
	{
		___lastClick_9 = value;
	}

	inline static int32_t get_offset_of_clickedOnce_10() { return static_cast<int32_t>(offsetof(GenericMouseEvent_1_t2681505391, ___clickedOnce_10)); }
	inline bool get_clickedOnce_10() const { return ___clickedOnce_10; }
	inline bool* get_address_of_clickedOnce_10() { return &___clickedOnce_10; }
	inline void set_clickedOnce_10(bool value)
	{
		___clickedOnce_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
