﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Gift
struct Gift_t450896550;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// GiftManager
struct GiftManager_t2451335245;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GiftManager/<Thrown>c__Iterator0
struct  U3CThrownU3Ec__Iterator0_t974249205  : public Il2CppObject
{
public:
	// Gift GiftManager/<Thrown>c__Iterator0::gift
	Gift_t450896550 * ___gift_0;
	// UnityEngine.GameObject GiftManager/<Thrown>c__Iterator0::<toSpawn>__0
	GameObject_t1756533147 * ___U3CtoSpawnU3E__0_1;
	// UnityEngine.Transform GiftManager/<Thrown>c__Iterator0::position
	Transform_t3275118058 * ___position_2;
	// UnityEngine.GameObject GiftManager/<Thrown>c__Iterator0::<yey>__0
	GameObject_t1756533147 * ___U3CyeyU3E__0_3;
	// GiftManager GiftManager/<Thrown>c__Iterator0::$this
	GiftManager_t2451335245 * ___U24this_4;
	// System.Object GiftManager/<Thrown>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean GiftManager/<Thrown>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 GiftManager/<Thrown>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_gift_0() { return static_cast<int32_t>(offsetof(U3CThrownU3Ec__Iterator0_t974249205, ___gift_0)); }
	inline Gift_t450896550 * get_gift_0() const { return ___gift_0; }
	inline Gift_t450896550 ** get_address_of_gift_0() { return &___gift_0; }
	inline void set_gift_0(Gift_t450896550 * value)
	{
		___gift_0 = value;
		Il2CppCodeGenWriteBarrier(&___gift_0, value);
	}

	inline static int32_t get_offset_of_U3CtoSpawnU3E__0_1() { return static_cast<int32_t>(offsetof(U3CThrownU3Ec__Iterator0_t974249205, ___U3CtoSpawnU3E__0_1)); }
	inline GameObject_t1756533147 * get_U3CtoSpawnU3E__0_1() const { return ___U3CtoSpawnU3E__0_1; }
	inline GameObject_t1756533147 ** get_address_of_U3CtoSpawnU3E__0_1() { return &___U3CtoSpawnU3E__0_1; }
	inline void set_U3CtoSpawnU3E__0_1(GameObject_t1756533147 * value)
	{
		___U3CtoSpawnU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtoSpawnU3E__0_1, value);
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(U3CThrownU3Ec__Iterator0_t974249205, ___position_2)); }
	inline Transform_t3275118058 * get_position_2() const { return ___position_2; }
	inline Transform_t3275118058 ** get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(Transform_t3275118058 * value)
	{
		___position_2 = value;
		Il2CppCodeGenWriteBarrier(&___position_2, value);
	}

	inline static int32_t get_offset_of_U3CyeyU3E__0_3() { return static_cast<int32_t>(offsetof(U3CThrownU3Ec__Iterator0_t974249205, ___U3CyeyU3E__0_3)); }
	inline GameObject_t1756533147 * get_U3CyeyU3E__0_3() const { return ___U3CyeyU3E__0_3; }
	inline GameObject_t1756533147 ** get_address_of_U3CyeyU3E__0_3() { return &___U3CyeyU3E__0_3; }
	inline void set_U3CyeyU3E__0_3(GameObject_t1756533147 * value)
	{
		___U3CyeyU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CyeyU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CThrownU3Ec__Iterator0_t974249205, ___U24this_4)); }
	inline GiftManager_t2451335245 * get_U24this_4() const { return ___U24this_4; }
	inline GiftManager_t2451335245 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(GiftManager_t2451335245 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CThrownU3Ec__Iterator0_t974249205, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CThrownU3Ec__Iterator0_t974249205, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CThrownU3Ec__Iterator0_t974249205, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
