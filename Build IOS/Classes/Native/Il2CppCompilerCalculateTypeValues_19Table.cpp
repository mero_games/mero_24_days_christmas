﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "DOTween43_U3CModuleU3E3783534214.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43301896125.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334720.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334751.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334817.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334918.h"
#include "DOTween46_U3CModuleU3E3783534214.h"
#include "DOTween46_DG_Tweening_DOTweenUtils461550156519.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46705180652.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939845.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939942.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939977.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480080.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130305.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124924.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117214.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799891.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130274.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124893.h"
#include "DOTweenPro_U3CModuleU3E3783534214.h"
#include "DOTweenPro_DG_Tweening_DOTweenVisualManager2945673405.h"
#include "DOTweenPro_DG_Tweening_HandlesDrawMode3273484032.h"
#include "DOTweenPro_DG_Tweening_HandlesType3201532857.h"
#include "DOTweenPro_DG_Tweening_DOTweenInspectorMode2739551672.h"
#include "DOTweenPro_DG_Tweening_DOTweenPath1397145371.h"
#include "DOTweenPro_DG_Tweening_Core_ABSAnimationComponent2205594551.h"
#include "DOTweenPro_DG_Tweening_Core_DOTweenAnimationType119935370.h"
#include "DOTweenPro_DG_Tweening_Core_OnDisableBehaviour125315118.h"
#include "DOTweenPro_DG_Tweening_Core_OnEnableBehaviour285142911.h"
#include "DOTweenPro_DG_Tweening_Core_TargetType2706200073.h"
#include "DOTweenPro_DG_Tweening_Core_VisualManagerPreset4087939440.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_NativeShare1150945090.h"
#include "AssemblyU2DCSharpU2Dfirstpass_NativeShare_ConfigSt1410224576.h"
#include "AssemblyU2DCSharpU2Dfirstpass_NativeShare_SocialSh1091601589.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MoveSample4188468541.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RotateSample3367094513.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SampleInfo4001993458.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween488923914.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EasingFunctio3676968174.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_ApplyTween747394300.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EaseType818674011.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_LoopType1490651981.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_NamedValueCol2874784184.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_Defaults4204852305.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_CRSpline4177960625.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CTweenDelay1889752800.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CTweenResta3903815285.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CStartU3Ec_1700299370.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_GameOneManager2648179911.h"
#include "AssemblyU2DCSharp_GameOneManager_U3CYSnowflakeSpawn362439958.h"
#include "AssemblyU2DCSharp_MoveSnowflake721823583.h"
#include "AssemblyU2DCSharp_MoveSnowflake_U3CYTimerU3Ec__Ite3081063149.h"
#include "AssemblyU2DCSharp_GameTenManager1577134516.h"
#include "AssemblyU2DCSharp_GameTenManager_U3CPopulateSlotsU2419992485.h"
#include "AssemblyU2DCSharp_GiftSlot3101111170.h"
#include "AssemblyU2DCSharp_PencilLine2072928649.h"
#include "AssemblyU2DCSharp_GameElevenManager2036900502.h"
#include "AssemblyU2DCSharp_GameElevenManager_U3CYWaterSnowSp119758335.h"
#include "AssemblyU2DCSharp_WaterSnow2237366536.h"
#include "AssemblyU2DCSharp_WaterSnow_U3CYSnowFallU3Ec__Iter3674066925.h"
#include "AssemblyU2DCSharp_GameTwelveManager2786305482.h"
#include "AssemblyU2DCSharp_GameTwelveManager_U3CHandleGameO3440186884.h"
#include "AssemblyU2DCSharp_GameTwelveManager_U3CYSetupItemU2478430660.h"
#include "AssemblyU2DCSharp_GameTwelveManager_U3CYGameCountDow31630424.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1907[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1908[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1911[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1914[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1919[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1927[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (ShortcutExtensions43_t301896125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (U3CU3Ec__DisplayClass2_0_t426334720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[1] = 
{
	U3CU3Ec__DisplayClass2_0_t426334720::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (U3CU3Ec__DisplayClass3_0_t426334751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[1] = 
{
	U3CU3Ec__DisplayClass3_0_t426334751::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (U3CU3Ec__DisplayClass5_0_t426334817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[1] = 
{
	U3CU3Ec__DisplayClass5_0_t426334817::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (U3CU3Ec__DisplayClass8_0_t426334918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[1] = 
{
	U3CU3Ec__DisplayClass8_0_t426334918::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (DOTweenUtils46_t1550156519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (ShortcutExtensions46_t705180652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (U3CU3Ec__DisplayClass0_0_t3371939845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[1] = 
{
	U3CU3Ec__DisplayClass0_0_t3371939845::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (U3CU3Ec__DisplayClass3_0_t3371939942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[1] = 
{
	U3CU3Ec__DisplayClass3_0_t3371939942::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (U3CU3Ec__DisplayClass4_0_t3371939977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[1] = 
{
	U3CU3Ec__DisplayClass4_0_t3371939977::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (U3CU3Ec__DisplayClass16_0_t3017480080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[1] = 
{
	U3CU3Ec__DisplayClass16_0_t3017480080::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (U3CU3Ec__DisplayClass22_0_t3582130305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[1] = 
{
	U3CU3Ec__DisplayClass22_0_t3582130305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (U3CU3Ec__DisplayClass23_0_t591124924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[1] = 
{
	U3CU3Ec__DisplayClass23_0_t591124924::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (U3CU3Ec__DisplayClass25_0_t4039117214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[1] = 
{
	U3CU3Ec__DisplayClass25_0_t4039117214::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (U3CU3Ec__DisplayClass31_0_t308799891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[1] = 
{
	U3CU3Ec__DisplayClass31_0_t308799891::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (U3CU3Ec__DisplayClass32_0_t3582130274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[1] = 
{
	U3CU3Ec__DisplayClass32_0_t3582130274::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (U3CU3Ec__DisplayClass33_0_t591124893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[1] = 
{
	U3CU3Ec__DisplayClass33_0_t591124893::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (DOTweenVisualManager_t2945673405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1949[4] = 
{
	DOTweenVisualManager_t2945673405::get_offset_of_preset_2(),
	DOTweenVisualManager_t2945673405::get_offset_of_onEnableBehaviour_3(),
	DOTweenVisualManager_t2945673405::get_offset_of_onDisableBehaviour_4(),
	DOTweenVisualManager_t2945673405::get_offset_of__requiresRestartFromSpawnPoint_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (HandlesDrawMode_t3273484032)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1950[3] = 
{
	HandlesDrawMode_t3273484032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (HandlesType_t3201532857)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1951[3] = 
{
	HandlesType_t3201532857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (DOTweenInspectorMode_t2739551672)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1952[5] = 
{
	DOTweenInspectorMode_t2739551672::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (DOTweenPath_t1397145371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[38] = 
{
	DOTweenPath_t1397145371::get_offset_of_delay_19(),
	DOTweenPath_t1397145371::get_offset_of_duration_20(),
	DOTweenPath_t1397145371::get_offset_of_easeType_21(),
	DOTweenPath_t1397145371::get_offset_of_easeCurve_22(),
	DOTweenPath_t1397145371::get_offset_of_loops_23(),
	DOTweenPath_t1397145371::get_offset_of_id_24(),
	DOTweenPath_t1397145371::get_offset_of_loopType_25(),
	DOTweenPath_t1397145371::get_offset_of_orientType_26(),
	DOTweenPath_t1397145371::get_offset_of_lookAtTransform_27(),
	DOTweenPath_t1397145371::get_offset_of_lookAtPosition_28(),
	DOTweenPath_t1397145371::get_offset_of_lookAhead_29(),
	DOTweenPath_t1397145371::get_offset_of_autoPlay_30(),
	DOTweenPath_t1397145371::get_offset_of_autoKill_31(),
	DOTweenPath_t1397145371::get_offset_of_relative_32(),
	DOTweenPath_t1397145371::get_offset_of_isLocal_33(),
	DOTweenPath_t1397145371::get_offset_of_isClosedPath_34(),
	DOTweenPath_t1397145371::get_offset_of_pathResolution_35(),
	DOTweenPath_t1397145371::get_offset_of_pathMode_36(),
	DOTweenPath_t1397145371::get_offset_of_lockRotation_37(),
	DOTweenPath_t1397145371::get_offset_of_assignForwardAndUp_38(),
	DOTweenPath_t1397145371::get_offset_of_forwardDirection_39(),
	DOTweenPath_t1397145371::get_offset_of_upDirection_40(),
	DOTweenPath_t1397145371::get_offset_of_tweenRigidbody_41(),
	DOTweenPath_t1397145371::get_offset_of_wps_42(),
	DOTweenPath_t1397145371::get_offset_of_fullWps_43(),
	DOTweenPath_t1397145371::get_offset_of_path_44(),
	DOTweenPath_t1397145371::get_offset_of_inspectorMode_45(),
	DOTweenPath_t1397145371::get_offset_of_pathType_46(),
	DOTweenPath_t1397145371::get_offset_of_handlesType_47(),
	DOTweenPath_t1397145371::get_offset_of_livePreview_48(),
	DOTweenPath_t1397145371::get_offset_of_handlesDrawMode_49(),
	DOTweenPath_t1397145371::get_offset_of_perspectiveHandleSize_50(),
	DOTweenPath_t1397145371::get_offset_of_showIndexes_51(),
	DOTweenPath_t1397145371::get_offset_of_showWpLength_52(),
	DOTweenPath_t1397145371::get_offset_of_pathColor_53(),
	DOTweenPath_t1397145371::get_offset_of_lastSrcPosition_54(),
	DOTweenPath_t1397145371::get_offset_of_wpsDropdown_55(),
	DOTweenPath_t1397145371::get_offset_of_dropToFloorOffset_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (ABSAnimationComponent_t2205594551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[17] = 
{
	ABSAnimationComponent_t2205594551::get_offset_of_updateType_2(),
	ABSAnimationComponent_t2205594551::get_offset_of_isSpeedBased_3(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnStart_4(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnPlay_5(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnUpdate_6(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnStepComplete_7(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnComplete_8(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnTweenCreated_9(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnRewind_10(),
	ABSAnimationComponent_t2205594551::get_offset_of_onStart_11(),
	ABSAnimationComponent_t2205594551::get_offset_of_onPlay_12(),
	ABSAnimationComponent_t2205594551::get_offset_of_onUpdate_13(),
	ABSAnimationComponent_t2205594551::get_offset_of_onStepComplete_14(),
	ABSAnimationComponent_t2205594551::get_offset_of_onComplete_15(),
	ABSAnimationComponent_t2205594551::get_offset_of_onTweenCreated_16(),
	ABSAnimationComponent_t2205594551::get_offset_of_onRewind_17(),
	ABSAnimationComponent_t2205594551::get_offset_of_tween_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (DOTweenAnimationType_t119935370)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1955[23] = 
{
	DOTweenAnimationType_t119935370::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (OnDisableBehaviour_t125315118)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1956[7] = 
{
	OnDisableBehaviour_t125315118::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (OnEnableBehaviour_t285142911)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1957[5] = 
{
	OnEnableBehaviour_t285142911::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (TargetType_t2706200073)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1958[17] = 
{
	TargetType_t2706200073::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (VisualManagerPreset_t4087939440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1959[3] = 
{
	VisualManagerPreset_t4087939440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1962[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (NativeShare_t1150945090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (ConfigStruct_t1410224576)+ sizeof (Il2CppObject), sizeof(ConfigStruct_t1410224576_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1967[2] = 
{
	ConfigStruct_t1410224576::get_offset_of_title_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConfigStruct_t1410224576::get_offset_of_message_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (SocialSharingStruct_t1091601589)+ sizeof (Il2CppObject), sizeof(SocialSharingStruct_t1091601589_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1968[3] = 
{
	SocialSharingStruct_t1091601589::get_offset_of_text_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SocialSharingStruct_t1091601589::get_offset_of_subject_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SocialSharingStruct_t1091601589::get_offset_of_filePaths_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (MoveSample_t4188468541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (RotateSample_t3367094513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (SampleInfo_t4001993458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (iTween_t488923914), -1, sizeof(iTween_t488923914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1972[39] = 
{
	iTween_t488923914_StaticFields::get_offset_of_tweens_2(),
	iTween_t488923914_StaticFields::get_offset_of_cameraFade_3(),
	iTween_t488923914::get_offset_of_id_4(),
	iTween_t488923914::get_offset_of_type_5(),
	iTween_t488923914::get_offset_of_method_6(),
	iTween_t488923914::get_offset_of_easeType_7(),
	iTween_t488923914::get_offset_of_time_8(),
	iTween_t488923914::get_offset_of_delay_9(),
	iTween_t488923914::get_offset_of_loopType_10(),
	iTween_t488923914::get_offset_of_isRunning_11(),
	iTween_t488923914::get_offset_of_isPaused_12(),
	iTween_t488923914::get_offset_of__name_13(),
	iTween_t488923914::get_offset_of_runningTime_14(),
	iTween_t488923914::get_offset_of_percentage_15(),
	iTween_t488923914::get_offset_of_delayStarted_16(),
	iTween_t488923914::get_offset_of_kinematic_17(),
	iTween_t488923914::get_offset_of_isLocal_18(),
	iTween_t488923914::get_offset_of_loop_19(),
	iTween_t488923914::get_offset_of_reverse_20(),
	iTween_t488923914::get_offset_of_wasPaused_21(),
	iTween_t488923914::get_offset_of_physics_22(),
	iTween_t488923914::get_offset_of_tweenArguments_23(),
	iTween_t488923914::get_offset_of_space_24(),
	iTween_t488923914::get_offset_of_ease_25(),
	iTween_t488923914::get_offset_of_apply_26(),
	iTween_t488923914::get_offset_of_audioSource_27(),
	iTween_t488923914::get_offset_of_vector3s_28(),
	iTween_t488923914::get_offset_of_vector2s_29(),
	iTween_t488923914::get_offset_of_colors_30(),
	iTween_t488923914::get_offset_of_floats_31(),
	iTween_t488923914::get_offset_of_rects_32(),
	iTween_t488923914::get_offset_of_path_33(),
	iTween_t488923914::get_offset_of_preUpdate_34(),
	iTween_t488923914::get_offset_of_postUpdate_35(),
	iTween_t488923914::get_offset_of_namedcolorvalue_36(),
	iTween_t488923914::get_offset_of_lastRealTime_37(),
	iTween_t488923914::get_offset_of_useRealTime_38(),
	iTween_t488923914::get_offset_of_thisTransform_39(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (EasingFunction_t3676968174), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (ApplyTween_t747394300), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (EaseType_t818674011)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1975[34] = 
{
	EaseType_t818674011::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (LoopType_t1490651981)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1976[4] = 
{
	LoopType_t1490651981::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (NamedValueColor_t2874784184)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1977[5] = 
{
	NamedValueColor_t2874784184::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (Defaults_t4204852305), -1, sizeof(Defaults_t4204852305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1978[16] = 
{
	Defaults_t4204852305_StaticFields::get_offset_of_time_0(),
	Defaults_t4204852305_StaticFields::get_offset_of_delay_1(),
	Defaults_t4204852305_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t4204852305_StaticFields::get_offset_of_loopType_3(),
	Defaults_t4204852305_StaticFields::get_offset_of_easeType_4(),
	Defaults_t4204852305_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t4204852305_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t4204852305_StaticFields::get_offset_of_space_7(),
	Defaults_t4204852305_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t4204852305_StaticFields::get_offset_of_color_9(),
	Defaults_t4204852305_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t4204852305_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t4204852305_StaticFields::get_offset_of_cameraFadeDepth_12(),
	Defaults_t4204852305_StaticFields::get_offset_of_lookAhead_13(),
	Defaults_t4204852305_StaticFields::get_offset_of_useRealTime_14(),
	Defaults_t4204852305_StaticFields::get_offset_of_up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (CRSpline_t4177960625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[1] = 
{
	CRSpline_t4177960625::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t1889752800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[4] = 
{
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24this_0(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24disposing_2(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t3903815285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[4] = 
{
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24this_0(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24disposing_2(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (U3CStartU3Ec__Iterator2_t1700299370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[4] = 
{
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (GameOneManager_t2648179911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1984[18] = 
{
	GameOneManager_t2648179911::get_offset_of_isPlaying_4(),
	GameOneManager_t2648179911::get_offset_of_centerScaleFactor_5(),
	GameOneManager_t2648179911::get_offset_of_movingScaleFactor_6(),
	GameOneManager_t2648179911::get_offset_of_fallingFlakeLifetime_7(),
	GameOneManager_t2648179911::get_offset_of_snowflakeSpawnRate_8(),
	GameOneManager_t2648179911::get_offset_of_chanceOfCorrectSpawn_9(),
	GameOneManager_t2648179911::get_offset_of_correctChoicesToMake_10(),
	GameOneManager_t2648179911::get_offset_of_nrOfLettersToDo_11(),
	GameOneManager_t2648179911::get_offset_of_centerPieceHolder_12(),
	GameOneManager_t2648179911::get_offset_of_leftSpawnPoint_13(),
	GameOneManager_t2648179911::get_offset_of_rightSpawnPoint_14(),
	GameOneManager_t2648179911::get_offset_of_currentCenterLetter_15(),
	GameOneManager_t2648179911::get_offset_of_snowFlakes_16(),
	GameOneManager_t2648179911::get_offset_of_letters_17(),
	GameOneManager_t2648179911::get_offset_of_snowPopParticle_18(),
	GameOneManager_t2648179911::get_offset_of_correctChoiceCounter_19(),
	GameOneManager_t2648179911::get_offset_of_lettersDone_20(),
	GameOneManager_t2648179911::get_offset_of_usedLetters_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1985[7] = 
{
	U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958::get_offset_of_startTime_0(),
	U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958::get_offset_of_U3CtimerU3E__0_1(),
	U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958::get_offset_of_holder_2(),
	U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958::get_offset_of_U24this_3(),
	U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958::get_offset_of_U24current_4(),
	U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958::get_offset_of_U24disposing_5(),
	U3CYSnowflakeSpawnerU3Ec__Iterator0_t362439958::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (MoveSnowflake_t721823583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[4] = 
{
	MoveSnowflake_t721823583::get_offset_of_col_2(),
	MoveSnowflake_t721823583::get_offset_of_thisLetter_3(),
	MoveSnowflake_t721823583::get_offset_of_originalPos_4(),
	MoveSnowflake_t721823583::get_offset_of_xScaler_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (U3CYTimerU3Ec__Iterator0_t3081063149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[5] = 
{
	U3CYTimerU3Ec__Iterator0_t3081063149::get_offset_of_U3CtimerU3E__0_0(),
	U3CYTimerU3Ec__Iterator0_t3081063149::get_offset_of_U24this_1(),
	U3CYTimerU3Ec__Iterator0_t3081063149::get_offset_of_U24current_2(),
	U3CYTimerU3Ec__Iterator0_t3081063149::get_offset_of_U24disposing_3(),
	U3CYTimerU3Ec__Iterator0_t3081063149::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (GameTenManager_t1577134516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[11] = 
{
	GameTenManager_t1577134516::get_offset_of_correctCounter_4(),
	GameTenManager_t1577134516::get_offset_of_drawings_5(),
	GameTenManager_t1577134516::get_offset_of_startCollider_6(),
	GameTenManager_t1577134516::get_offset_of_drawColors_7(),
	GameTenManager_t1577134516::get_offset_of_unwrappedGifts_8(),
	GameTenManager_t1577134516::get_offset_of_wrappedGifts_9(),
	GameTenManager_t1577134516::get_offset_of_topSlots_10(),
	GameTenManager_t1577134516::get_offset_of_bottomSlots_11(),
	GameTenManager_t1577134516::get_offset_of_pencil_12(),
	GameTenManager_t1577134516::get_offset_of_iPencil_13(),
	GameTenManager_t1577134516::get_offset_of_beenUsed_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (U3CPopulateSlotsU3Ec__Iterator0_t2419992485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[6] = 
{
	U3CPopulateSlotsU3Ec__Iterator0_t2419992485::get_offset_of_U3CtempUsedU3E__0_0(),
	U3CPopulateSlotsU3Ec__Iterator0_t2419992485::get_offset_of_U3CtempU3E__0_1(),
	U3CPopulateSlotsU3Ec__Iterator0_t2419992485::get_offset_of_U24this_2(),
	U3CPopulateSlotsU3Ec__Iterator0_t2419992485::get_offset_of_U24current_3(),
	U3CPopulateSlotsU3Ec__Iterator0_t2419992485::get_offset_of_U24disposing_4(),
	U3CPopulateSlotsU3Ec__Iterator0_t2419992485::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (GiftSlot_t3101111170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[1] = 
{
	GiftSlot_t3101111170::get_offset_of_slotIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (PencilLine_t2072928649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[2] = 
{
	PencilLine_t2072928649::get_offset_of_toMove_2(),
	PencilLine_t2072928649::get_offset_of_drawSound_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (GameElevenManager_t2036900502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1992[10] = 
{
	GameElevenManager_t2036900502::get_offset_of_isPlaying_4(),
	GameElevenManager_t2036900502::get_offset_of_riseAmount_5(),
	GameElevenManager_t2036900502::get_offset_of_riseSpeed_6(),
	GameElevenManager_t2036900502::get_offset_of_spawnRate_7(),
	GameElevenManager_t2036900502::get_offset_of_xThreshold_8(),
	GameElevenManager_t2036900502::get_offset_of_flakesToHit_9(),
	GameElevenManager_t2036900502::get_offset_of_snowPile_10(),
	GameElevenManager_t2036900502::get_offset_of_waterSnow_11(),
	GameElevenManager_t2036900502::get_offset_of_snowflakeSprites_12(),
	GameElevenManager_t2036900502::get_offset_of_snowHitCount_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (U3CYWaterSnowSpawnerU3Ec__Iterator0_t119758335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[5] = 
{
	U3CYWaterSnowSpawnerU3Ec__Iterator0_t119758335::get_offset_of_U3CtimerU3E__0_0(),
	U3CYWaterSnowSpawnerU3Ec__Iterator0_t119758335::get_offset_of_U24this_1(),
	U3CYWaterSnowSpawnerU3Ec__Iterator0_t119758335::get_offset_of_U24current_2(),
	U3CYWaterSnowSpawnerU3Ec__Iterator0_t119758335::get_offset_of_U24disposing_3(),
	U3CYWaterSnowSpawnerU3Ec__Iterator0_t119758335::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (WaterSnow_t2237366536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[5] = 
{
	WaterSnow_t2237366536::get_offset_of_snowFlakeSpeed_2(),
	WaterSnow_t2237366536::get_offset_of_isSnow_3(),
	WaterSnow_t2237366536::get_offset_of_waterDrop_4(),
	WaterSnow_t2237366536::get_offset_of_snowFlake_5(),
	WaterSnow_t2237366536::get_offset_of_rb2d_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (U3CYSnowFallU3Ec__Iterator0_t3674066925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[6] = 
{
	U3CYSnowFallU3Ec__Iterator0_t3674066925::get_offset_of_U3CvalU3E__0_0(),
	U3CYSnowFallU3Ec__Iterator0_t3674066925::get_offset_of_U3CtimerU3E__0_1(),
	U3CYSnowFallU3Ec__Iterator0_t3674066925::get_offset_of_U24this_2(),
	U3CYSnowFallU3Ec__Iterator0_t3674066925::get_offset_of_U24current_3(),
	U3CYSnowFallU3Ec__Iterator0_t3674066925::get_offset_of_U24disposing_4(),
	U3CYSnowFallU3Ec__Iterator0_t3674066925::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (GameTwelveManager_t2786305482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[11] = 
{
	GameTwelveManager_t2786305482::get_offset_of_tickingSound_4(),
	GameTwelveManager_t2786305482::get_offset_of_tickingSoundFast_5(),
	GameTwelveManager_t2786305482::get_offset_of_itemsToFind_6(),
	GameTwelveManager_t2786305482::get_offset_of_itemsFound_7(),
	GameTwelveManager_t2786305482::get_offset_of_textTimer_8(),
	GameTwelveManager_t2786305482::get_offset_of_secondsToPlay_9(),
	GameTwelveManager_t2786305482::get_offset_of_exampleSize_10(),
	GameTwelveManager_t2786305482::get_offset_of_itemSprites_11(),
	GameTwelveManager_t2786305482::get_offset_of_spriteUsed_12(),
	GameTwelveManager_t2786305482::get_offset_of_exampleItem_13(),
	GameTwelveManager_t2786305482::get_offset_of_canClick_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (U3CHandleGameOverU3Ec__Iterator0_t3440186884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[4] = 
{
	U3CHandleGameOverU3Ec__Iterator0_t3440186884::get_offset_of_U24this_0(),
	U3CHandleGameOverU3Ec__Iterator0_t3440186884::get_offset_of_U24current_1(),
	U3CHandleGameOverU3Ec__Iterator0_t3440186884::get_offset_of_U24disposing_2(),
	U3CHandleGameOverU3Ec__Iterator0_t3440186884::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (U3CYSetupItemU3Ec__Iterator1_t2478430660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[5] = 
{
	U3CYSetupItemU3Ec__Iterator1_t2478430660::get_offset_of_U3CtempU3E__0_0(),
	U3CYSetupItemU3Ec__Iterator1_t2478430660::get_offset_of_U24this_1(),
	U3CYSetupItemU3Ec__Iterator1_t2478430660::get_offset_of_U24current_2(),
	U3CYSetupItemU3Ec__Iterator1_t2478430660::get_offset_of_U24disposing_3(),
	U3CYSetupItemU3Ec__Iterator1_t2478430660::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (U3CYGameCountDownU3Ec__Iterator2_t31630424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[7] = 
{
	U3CYGameCountDownU3Ec__Iterator2_t31630424::get_offset_of_U3CtimerU3E__0_0(),
	U3CYGameCountDownU3Ec__Iterator2_t31630424::get_offset_of_U3CfastStartedU3E__0_1(),
	U3CYGameCountDownU3Ec__Iterator2_t31630424::get_offset_of_U3CintSecondU3E__1_2(),
	U3CYGameCountDownU3Ec__Iterator2_t31630424::get_offset_of_U24this_3(),
	U3CYGameCountDownU3Ec__Iterator2_t31630424::get_offset_of_U24current_4(),
	U3CYGameCountDownU3Ec__Iterator2_t31630424::get_offset_of_U24disposing_5(),
	U3CYGameCountDownU3Ec__Iterator2_t31630424::get_offset_of_U24PC_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
