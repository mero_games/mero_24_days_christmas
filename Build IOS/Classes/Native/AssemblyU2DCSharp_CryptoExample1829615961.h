﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CryptoExample
struct  CryptoExample_t1829615961  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField CryptoExample::keyField
	InputField_t1631627530 * ___keyField_2;
	// UnityEngine.UI.InputField CryptoExample::sourceTextField
	InputField_t1631627530 * ___sourceTextField_3;
	// UnityEngine.UI.InputField CryptoExample::encryptedTextField
	InputField_t1631627530 * ___encryptedTextField_4;
	// UnityEngine.UI.Button CryptoExample::encryptBtn
	Button_t2872111280 * ___encryptBtn_5;
	// UnityEngine.UI.Button CryptoExample::decryptBtn
	Button_t2872111280 * ___decryptBtn_6;

public:
	inline static int32_t get_offset_of_keyField_2() { return static_cast<int32_t>(offsetof(CryptoExample_t1829615961, ___keyField_2)); }
	inline InputField_t1631627530 * get_keyField_2() const { return ___keyField_2; }
	inline InputField_t1631627530 ** get_address_of_keyField_2() { return &___keyField_2; }
	inline void set_keyField_2(InputField_t1631627530 * value)
	{
		___keyField_2 = value;
		Il2CppCodeGenWriteBarrier(&___keyField_2, value);
	}

	inline static int32_t get_offset_of_sourceTextField_3() { return static_cast<int32_t>(offsetof(CryptoExample_t1829615961, ___sourceTextField_3)); }
	inline InputField_t1631627530 * get_sourceTextField_3() const { return ___sourceTextField_3; }
	inline InputField_t1631627530 ** get_address_of_sourceTextField_3() { return &___sourceTextField_3; }
	inline void set_sourceTextField_3(InputField_t1631627530 * value)
	{
		___sourceTextField_3 = value;
		Il2CppCodeGenWriteBarrier(&___sourceTextField_3, value);
	}

	inline static int32_t get_offset_of_encryptedTextField_4() { return static_cast<int32_t>(offsetof(CryptoExample_t1829615961, ___encryptedTextField_4)); }
	inline InputField_t1631627530 * get_encryptedTextField_4() const { return ___encryptedTextField_4; }
	inline InputField_t1631627530 ** get_address_of_encryptedTextField_4() { return &___encryptedTextField_4; }
	inline void set_encryptedTextField_4(InputField_t1631627530 * value)
	{
		___encryptedTextField_4 = value;
		Il2CppCodeGenWriteBarrier(&___encryptedTextField_4, value);
	}

	inline static int32_t get_offset_of_encryptBtn_5() { return static_cast<int32_t>(offsetof(CryptoExample_t1829615961, ___encryptBtn_5)); }
	inline Button_t2872111280 * get_encryptBtn_5() const { return ___encryptBtn_5; }
	inline Button_t2872111280 ** get_address_of_encryptBtn_5() { return &___encryptBtn_5; }
	inline void set_encryptBtn_5(Button_t2872111280 * value)
	{
		___encryptBtn_5 = value;
		Il2CppCodeGenWriteBarrier(&___encryptBtn_5, value);
	}

	inline static int32_t get_offset_of_decryptBtn_6() { return static_cast<int32_t>(offsetof(CryptoExample_t1829615961, ___decryptBtn_6)); }
	inline Button_t2872111280 * get_decryptBtn_6() const { return ___decryptBtn_6; }
	inline Button_t2872111280 ** get_address_of_decryptBtn_6() { return &___decryptBtn_6; }
	inline void set_decryptBtn_6(Button_t2872111280 * value)
	{
		___decryptBtn_6 = value;
		Il2CppCodeGenWriteBarrier(&___decryptBtn_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
