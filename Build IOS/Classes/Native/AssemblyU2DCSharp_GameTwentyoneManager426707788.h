﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen3721190174.h"

// System.Collections.Generic.List`1<GameTwentyoneManager/GiftObject>
struct List_1_t1513796912;
// GiftDrawer
struct GiftDrawer_t3411291385;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTwentyoneManager
struct  GameTwentyoneManager_t426707788  : public SingletonBase_1_t3721190174
{
public:
	// System.Single GameTwentyoneManager::imageComplete
	float ___imageComplete_4;
	// System.Int32 GameTwentyoneManager::rounds
	int32_t ___rounds_5;
	// System.Single GameTwentyoneManager::fadeSpeed
	float ___fadeSpeed_6;
	// System.Single GameTwentyoneManager::scale
	float ___scale_7;
	// System.Single GameTwentyoneManager::checkTime
	float ___checkTime_8;
	// System.Collections.Generic.List`1<GameTwentyoneManager/GiftObject> GameTwentyoneManager::gifts
	List_1_t1513796912 * ___gifts_9;
	// GiftDrawer GameTwentyoneManager::giftDrawer
	GiftDrawer_t3411291385 * ___giftDrawer_10;
	// System.Collections.Generic.List`1<GameTwentyoneManager/GiftObject> GameTwentyoneManager::_gifts
	List_1_t1513796912 * ____gifts_11;
	// System.Int32 GameTwentyoneManager::curRound
	int32_t ___curRound_12;
	// UnityEngine.AudioSource GameTwentyoneManager::drawLoopSound
	AudioSource_t1135106623 * ___drawLoopSound_13;

public:
	inline static int32_t get_offset_of_imageComplete_4() { return static_cast<int32_t>(offsetof(GameTwentyoneManager_t426707788, ___imageComplete_4)); }
	inline float get_imageComplete_4() const { return ___imageComplete_4; }
	inline float* get_address_of_imageComplete_4() { return &___imageComplete_4; }
	inline void set_imageComplete_4(float value)
	{
		___imageComplete_4 = value;
	}

	inline static int32_t get_offset_of_rounds_5() { return static_cast<int32_t>(offsetof(GameTwentyoneManager_t426707788, ___rounds_5)); }
	inline int32_t get_rounds_5() const { return ___rounds_5; }
	inline int32_t* get_address_of_rounds_5() { return &___rounds_5; }
	inline void set_rounds_5(int32_t value)
	{
		___rounds_5 = value;
	}

	inline static int32_t get_offset_of_fadeSpeed_6() { return static_cast<int32_t>(offsetof(GameTwentyoneManager_t426707788, ___fadeSpeed_6)); }
	inline float get_fadeSpeed_6() const { return ___fadeSpeed_6; }
	inline float* get_address_of_fadeSpeed_6() { return &___fadeSpeed_6; }
	inline void set_fadeSpeed_6(float value)
	{
		___fadeSpeed_6 = value;
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(GameTwentyoneManager_t426707788, ___scale_7)); }
	inline float get_scale_7() const { return ___scale_7; }
	inline float* get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(float value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_checkTime_8() { return static_cast<int32_t>(offsetof(GameTwentyoneManager_t426707788, ___checkTime_8)); }
	inline float get_checkTime_8() const { return ___checkTime_8; }
	inline float* get_address_of_checkTime_8() { return &___checkTime_8; }
	inline void set_checkTime_8(float value)
	{
		___checkTime_8 = value;
	}

	inline static int32_t get_offset_of_gifts_9() { return static_cast<int32_t>(offsetof(GameTwentyoneManager_t426707788, ___gifts_9)); }
	inline List_1_t1513796912 * get_gifts_9() const { return ___gifts_9; }
	inline List_1_t1513796912 ** get_address_of_gifts_9() { return &___gifts_9; }
	inline void set_gifts_9(List_1_t1513796912 * value)
	{
		___gifts_9 = value;
		Il2CppCodeGenWriteBarrier(&___gifts_9, value);
	}

	inline static int32_t get_offset_of_giftDrawer_10() { return static_cast<int32_t>(offsetof(GameTwentyoneManager_t426707788, ___giftDrawer_10)); }
	inline GiftDrawer_t3411291385 * get_giftDrawer_10() const { return ___giftDrawer_10; }
	inline GiftDrawer_t3411291385 ** get_address_of_giftDrawer_10() { return &___giftDrawer_10; }
	inline void set_giftDrawer_10(GiftDrawer_t3411291385 * value)
	{
		___giftDrawer_10 = value;
		Il2CppCodeGenWriteBarrier(&___giftDrawer_10, value);
	}

	inline static int32_t get_offset_of__gifts_11() { return static_cast<int32_t>(offsetof(GameTwentyoneManager_t426707788, ____gifts_11)); }
	inline List_1_t1513796912 * get__gifts_11() const { return ____gifts_11; }
	inline List_1_t1513796912 ** get_address_of__gifts_11() { return &____gifts_11; }
	inline void set__gifts_11(List_1_t1513796912 * value)
	{
		____gifts_11 = value;
		Il2CppCodeGenWriteBarrier(&____gifts_11, value);
	}

	inline static int32_t get_offset_of_curRound_12() { return static_cast<int32_t>(offsetof(GameTwentyoneManager_t426707788, ___curRound_12)); }
	inline int32_t get_curRound_12() const { return ___curRound_12; }
	inline int32_t* get_address_of_curRound_12() { return &___curRound_12; }
	inline void set_curRound_12(int32_t value)
	{
		___curRound_12 = value;
	}

	inline static int32_t get_offset_of_drawLoopSound_13() { return static_cast<int32_t>(offsetof(GameTwentyoneManager_t426707788, ___drawLoopSound_13)); }
	inline AudioSource_t1135106623 * get_drawLoopSound_13() const { return ___drawLoopSound_13; }
	inline AudioSource_t1135106623 ** get_address_of_drawLoopSound_13() { return &___drawLoopSound_13; }
	inline void set_drawLoopSound_13(AudioSource_t1135106623 * value)
	{
		___drawLoopSound_13 = value;
		Il2CppCodeGenWriteBarrier(&___drawLoopSound_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
