﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// DestroyParticle/<YDestroy>c__Iterator0
struct U3CYDestroyU3Ec__Iterator0_t1854615267;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyParticle/<YDestroy>c__Iterator0/<YDestroy>c__AnonStorey1
struct  U3CYDestroyU3Ec__AnonStorey1_t674293687  : public Il2CppObject
{
public:
	// System.Boolean DestroyParticle/<YDestroy>c__Iterator0/<YDestroy>c__AnonStorey1::alive
	bool ___alive_0;
	// DestroyParticle/<YDestroy>c__Iterator0 DestroyParticle/<YDestroy>c__Iterator0/<YDestroy>c__AnonStorey1::<>f__ref$0
	U3CYDestroyU3Ec__Iterator0_t1854615267 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_alive_0() { return static_cast<int32_t>(offsetof(U3CYDestroyU3Ec__AnonStorey1_t674293687, ___alive_0)); }
	inline bool get_alive_0() const { return ___alive_0; }
	inline bool* get_address_of_alive_0() { return &___alive_0; }
	inline void set_alive_0(bool value)
	{
		___alive_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CYDestroyU3Ec__AnonStorey1_t674293687, ___U3CU3Ef__refU240_1)); }
	inline U3CYDestroyU3Ec__Iterator0_t1854615267 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CYDestroyU3Ec__Iterator0_t1854615267 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CYDestroyU3Ec__Iterator0_t1854615267 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU240_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
