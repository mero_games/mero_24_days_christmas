﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen468551175.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// HatEnabler[]
struct HatEnablerU5BU5D_t4191028017;
// RingTossController
struct RingTossController_t956255555;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameThreeManager
struct  GameThreeManager_t1469036085  : public SingletonBase_1_t468551175
{
public:
	// System.Boolean GameThreeManager::isPlaying
	bool ___isPlaying_4;
	// System.Int32 GameThreeManager::ringsToToss
	int32_t ___ringsToToss_5;
	// System.Int32 GameThreeManager::landedRingsCounter
	int32_t ___landedRingsCounter_6;
	// System.Single GameThreeManager::minSpawnRate
	float ___minSpawnRate_7;
	// System.Single GameThreeManager::maxSpawnRate
	float ___maxSpawnRate_8;
	// UnityEngine.GameObject GameThreeManager::ring
	GameObject_t1756533147 * ___ring_9;
	// UnityEngine.Vector3 GameThreeManager::ringSpawnPos
	Vector3_t2243707580  ___ringSpawnPos_10;
	// System.Single GameThreeManager::ringScale
	float ___ringScale_11;
	// HatEnabler[] GameThreeManager::hats
	HatEnablerU5BU5D_t4191028017* ___hats_12;
	// System.Int32 GameThreeManager::numberOfStandingHats
	int32_t ___numberOfStandingHats_13;
	// System.Single GameThreeManager::standUpRate
	float ___standUpRate_14;
	// RingTossController GameThreeManager::ringScript
	RingTossController_t956255555 * ___ringScript_15;

public:
	inline static int32_t get_offset_of_isPlaying_4() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___isPlaying_4)); }
	inline bool get_isPlaying_4() const { return ___isPlaying_4; }
	inline bool* get_address_of_isPlaying_4() { return &___isPlaying_4; }
	inline void set_isPlaying_4(bool value)
	{
		___isPlaying_4 = value;
	}

	inline static int32_t get_offset_of_ringsToToss_5() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___ringsToToss_5)); }
	inline int32_t get_ringsToToss_5() const { return ___ringsToToss_5; }
	inline int32_t* get_address_of_ringsToToss_5() { return &___ringsToToss_5; }
	inline void set_ringsToToss_5(int32_t value)
	{
		___ringsToToss_5 = value;
	}

	inline static int32_t get_offset_of_landedRingsCounter_6() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___landedRingsCounter_6)); }
	inline int32_t get_landedRingsCounter_6() const { return ___landedRingsCounter_6; }
	inline int32_t* get_address_of_landedRingsCounter_6() { return &___landedRingsCounter_6; }
	inline void set_landedRingsCounter_6(int32_t value)
	{
		___landedRingsCounter_6 = value;
	}

	inline static int32_t get_offset_of_minSpawnRate_7() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___minSpawnRate_7)); }
	inline float get_minSpawnRate_7() const { return ___minSpawnRate_7; }
	inline float* get_address_of_minSpawnRate_7() { return &___minSpawnRate_7; }
	inline void set_minSpawnRate_7(float value)
	{
		___minSpawnRate_7 = value;
	}

	inline static int32_t get_offset_of_maxSpawnRate_8() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___maxSpawnRate_8)); }
	inline float get_maxSpawnRate_8() const { return ___maxSpawnRate_8; }
	inline float* get_address_of_maxSpawnRate_8() { return &___maxSpawnRate_8; }
	inline void set_maxSpawnRate_8(float value)
	{
		___maxSpawnRate_8 = value;
	}

	inline static int32_t get_offset_of_ring_9() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___ring_9)); }
	inline GameObject_t1756533147 * get_ring_9() const { return ___ring_9; }
	inline GameObject_t1756533147 ** get_address_of_ring_9() { return &___ring_9; }
	inline void set_ring_9(GameObject_t1756533147 * value)
	{
		___ring_9 = value;
		Il2CppCodeGenWriteBarrier(&___ring_9, value);
	}

	inline static int32_t get_offset_of_ringSpawnPos_10() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___ringSpawnPos_10)); }
	inline Vector3_t2243707580  get_ringSpawnPos_10() const { return ___ringSpawnPos_10; }
	inline Vector3_t2243707580 * get_address_of_ringSpawnPos_10() { return &___ringSpawnPos_10; }
	inline void set_ringSpawnPos_10(Vector3_t2243707580  value)
	{
		___ringSpawnPos_10 = value;
	}

	inline static int32_t get_offset_of_ringScale_11() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___ringScale_11)); }
	inline float get_ringScale_11() const { return ___ringScale_11; }
	inline float* get_address_of_ringScale_11() { return &___ringScale_11; }
	inline void set_ringScale_11(float value)
	{
		___ringScale_11 = value;
	}

	inline static int32_t get_offset_of_hats_12() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___hats_12)); }
	inline HatEnablerU5BU5D_t4191028017* get_hats_12() const { return ___hats_12; }
	inline HatEnablerU5BU5D_t4191028017** get_address_of_hats_12() { return &___hats_12; }
	inline void set_hats_12(HatEnablerU5BU5D_t4191028017* value)
	{
		___hats_12 = value;
		Il2CppCodeGenWriteBarrier(&___hats_12, value);
	}

	inline static int32_t get_offset_of_numberOfStandingHats_13() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___numberOfStandingHats_13)); }
	inline int32_t get_numberOfStandingHats_13() const { return ___numberOfStandingHats_13; }
	inline int32_t* get_address_of_numberOfStandingHats_13() { return &___numberOfStandingHats_13; }
	inline void set_numberOfStandingHats_13(int32_t value)
	{
		___numberOfStandingHats_13 = value;
	}

	inline static int32_t get_offset_of_standUpRate_14() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___standUpRate_14)); }
	inline float get_standUpRate_14() const { return ___standUpRate_14; }
	inline float* get_address_of_standUpRate_14() { return &___standUpRate_14; }
	inline void set_standUpRate_14(float value)
	{
		___standUpRate_14 = value;
	}

	inline static int32_t get_offset_of_ringScript_15() { return static_cast<int32_t>(offsetof(GameThreeManager_t1469036085, ___ringScript_15)); }
	inline RingTossController_t956255555 * get_ringScript_15() const { return ___ringScript_15; }
	inline RingTossController_t956255555 ** get_address_of_ringScript_15() { return &___ringScript_15; }
	inline void set_ringScript_15(RingTossController_t956255555 * value)
	{
		___ringScript_15 = value;
		Il2CppCodeGenWriteBarrier(&___ringScript_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
