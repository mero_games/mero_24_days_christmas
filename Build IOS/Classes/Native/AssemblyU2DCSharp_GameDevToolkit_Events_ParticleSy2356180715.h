﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Events.ParticleSystemEvent/ParticlySystemDoneDelegate
struct ParticlySystemDoneDelegate_t623652476;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Events.ParticleSystemEvent
struct  ParticleSystemEvent_t2356180715  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Events.ParticleSystemEvent/ParticlySystemDoneDelegate GameDevToolkit.Events.ParticleSystemEvent::ParticleSystemDone
	ParticlySystemDoneDelegate_t623652476 * ___ParticleSystemDone_2;
	// System.Single GameDevToolkit.Events.ParticleSystemEvent::maxLifeTime
	float ___maxLifeTime_3;
	// System.Single GameDevToolkit.Events.ParticleSystemEvent::startTime
	float ___startTime_4;
	// System.Boolean GameDevToolkit.Events.ParticleSystemEvent::dispatchedDone
	bool ___dispatchedDone_5;
	// UnityEngine.ParticleSystem GameDevToolkit.Events.ParticleSystemEvent::pSystem
	ParticleSystem_t3394631041 * ___pSystem_6;

public:
	inline static int32_t get_offset_of_ParticleSystemDone_2() { return static_cast<int32_t>(offsetof(ParticleSystemEvent_t2356180715, ___ParticleSystemDone_2)); }
	inline ParticlySystemDoneDelegate_t623652476 * get_ParticleSystemDone_2() const { return ___ParticleSystemDone_2; }
	inline ParticlySystemDoneDelegate_t623652476 ** get_address_of_ParticleSystemDone_2() { return &___ParticleSystemDone_2; }
	inline void set_ParticleSystemDone_2(ParticlySystemDoneDelegate_t623652476 * value)
	{
		___ParticleSystemDone_2 = value;
		Il2CppCodeGenWriteBarrier(&___ParticleSystemDone_2, value);
	}

	inline static int32_t get_offset_of_maxLifeTime_3() { return static_cast<int32_t>(offsetof(ParticleSystemEvent_t2356180715, ___maxLifeTime_3)); }
	inline float get_maxLifeTime_3() const { return ___maxLifeTime_3; }
	inline float* get_address_of_maxLifeTime_3() { return &___maxLifeTime_3; }
	inline void set_maxLifeTime_3(float value)
	{
		___maxLifeTime_3 = value;
	}

	inline static int32_t get_offset_of_startTime_4() { return static_cast<int32_t>(offsetof(ParticleSystemEvent_t2356180715, ___startTime_4)); }
	inline float get_startTime_4() const { return ___startTime_4; }
	inline float* get_address_of_startTime_4() { return &___startTime_4; }
	inline void set_startTime_4(float value)
	{
		___startTime_4 = value;
	}

	inline static int32_t get_offset_of_dispatchedDone_5() { return static_cast<int32_t>(offsetof(ParticleSystemEvent_t2356180715, ___dispatchedDone_5)); }
	inline bool get_dispatchedDone_5() const { return ___dispatchedDone_5; }
	inline bool* get_address_of_dispatchedDone_5() { return &___dispatchedDone_5; }
	inline void set_dispatchedDone_5(bool value)
	{
		___dispatchedDone_5 = value;
	}

	inline static int32_t get_offset_of_pSystem_6() { return static_cast<int32_t>(offsetof(ParticleSystemEvent_t2356180715, ___pSystem_6)); }
	inline ParticleSystem_t3394631041 * get_pSystem_6() const { return ___pSystem_6; }
	inline ParticleSystem_t3394631041 ** get_address_of_pSystem_6() { return &___pSystem_6; }
	inline void set_pSystem_6(ParticleSystem_t3394631041 * value)
	{
		___pSystem_6 = value;
		Il2CppCodeGenWriteBarrier(&___pSystem_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
