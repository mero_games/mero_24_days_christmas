﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Texture
struct Texture_t2243626319;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Testscript
struct  Testscript_t905621679  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Testscript::waitTime
	float ___waitTime_2;
	// UnityEngine.Sprite Testscript::wrapSprite
	Sprite_t309593783 * ___wrapSprite_3;
	// UnityEngine.Sprite Testscript::giftSprite
	Sprite_t309593783 * ___giftSprite_4;
	// UnityEngine.Material Testscript::material
	Material_t193706927 * ___material_5;
	// System.Int32 Testscript::texwidth
	int32_t ___texwidth_6;
	// System.Int32 Testscript::texheight
	int32_t ___texheight_7;
	// UnityEngine.Vector2 Testscript::size
	Vector2_t2243707579  ___size_8;
	// System.Single Testscript::width
	float ___width_9;
	// System.Single Testscript::height
	float ___height_10;
	// System.Single Testscript::minbordx
	float ___minbordx_11;
	// System.Single Testscript::minbordy
	float ___minbordy_12;
	// System.Single Testscript::maxbordx
	float ___maxbordx_13;
	// System.Single Testscript::maxbordy
	float ___maxbordy_14;
	// UnityEngine.RenderTexture Testscript::texture
	RenderTexture_t2666733923 * ___texture_15;
	// UnityEngine.UI.RawImage Testscript::img
	RawImage_t2749640213 * ___img_16;
	// UnityEngine.Vector2 Testscript::oldPos
	Vector2_t2243707579  ___oldPos_17;
	// UnityEngine.Vector2 Testscript::pos
	Vector2_t2243707579  ___pos_18;
	// System.Single Testscript::count
	float ___count_19;
	// UnityEngine.Texture2D Testscript::testTex
	Texture2D_t3542995729 * ___testTex_20;
	// UnityEngine.Texture Testscript::clear
	Texture_t2243626319 * ___clear_21;
	// UnityEngine.Material Testscript::clearMaterial
	Material_t193706927 * ___clearMaterial_22;

public:
	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_wrapSprite_3() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___wrapSprite_3)); }
	inline Sprite_t309593783 * get_wrapSprite_3() const { return ___wrapSprite_3; }
	inline Sprite_t309593783 ** get_address_of_wrapSprite_3() { return &___wrapSprite_3; }
	inline void set_wrapSprite_3(Sprite_t309593783 * value)
	{
		___wrapSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___wrapSprite_3, value);
	}

	inline static int32_t get_offset_of_giftSprite_4() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___giftSprite_4)); }
	inline Sprite_t309593783 * get_giftSprite_4() const { return ___giftSprite_4; }
	inline Sprite_t309593783 ** get_address_of_giftSprite_4() { return &___giftSprite_4; }
	inline void set_giftSprite_4(Sprite_t309593783 * value)
	{
		___giftSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___giftSprite_4, value);
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___material_5)); }
	inline Material_t193706927 * get_material_5() const { return ___material_5; }
	inline Material_t193706927 ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(Material_t193706927 * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier(&___material_5, value);
	}

	inline static int32_t get_offset_of_texwidth_6() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___texwidth_6)); }
	inline int32_t get_texwidth_6() const { return ___texwidth_6; }
	inline int32_t* get_address_of_texwidth_6() { return &___texwidth_6; }
	inline void set_texwidth_6(int32_t value)
	{
		___texwidth_6 = value;
	}

	inline static int32_t get_offset_of_texheight_7() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___texheight_7)); }
	inline int32_t get_texheight_7() const { return ___texheight_7; }
	inline int32_t* get_address_of_texheight_7() { return &___texheight_7; }
	inline void set_texheight_7(int32_t value)
	{
		___texheight_7 = value;
	}

	inline static int32_t get_offset_of_size_8() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___size_8)); }
	inline Vector2_t2243707579  get_size_8() const { return ___size_8; }
	inline Vector2_t2243707579 * get_address_of_size_8() { return &___size_8; }
	inline void set_size_8(Vector2_t2243707579  value)
	{
		___size_8 = value;
	}

	inline static int32_t get_offset_of_width_9() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___width_9)); }
	inline float get_width_9() const { return ___width_9; }
	inline float* get_address_of_width_9() { return &___width_9; }
	inline void set_width_9(float value)
	{
		___width_9 = value;
	}

	inline static int32_t get_offset_of_height_10() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___height_10)); }
	inline float get_height_10() const { return ___height_10; }
	inline float* get_address_of_height_10() { return &___height_10; }
	inline void set_height_10(float value)
	{
		___height_10 = value;
	}

	inline static int32_t get_offset_of_minbordx_11() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___minbordx_11)); }
	inline float get_minbordx_11() const { return ___minbordx_11; }
	inline float* get_address_of_minbordx_11() { return &___minbordx_11; }
	inline void set_minbordx_11(float value)
	{
		___minbordx_11 = value;
	}

	inline static int32_t get_offset_of_minbordy_12() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___minbordy_12)); }
	inline float get_minbordy_12() const { return ___minbordy_12; }
	inline float* get_address_of_minbordy_12() { return &___minbordy_12; }
	inline void set_minbordy_12(float value)
	{
		___minbordy_12 = value;
	}

	inline static int32_t get_offset_of_maxbordx_13() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___maxbordx_13)); }
	inline float get_maxbordx_13() const { return ___maxbordx_13; }
	inline float* get_address_of_maxbordx_13() { return &___maxbordx_13; }
	inline void set_maxbordx_13(float value)
	{
		___maxbordx_13 = value;
	}

	inline static int32_t get_offset_of_maxbordy_14() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___maxbordy_14)); }
	inline float get_maxbordy_14() const { return ___maxbordy_14; }
	inline float* get_address_of_maxbordy_14() { return &___maxbordy_14; }
	inline void set_maxbordy_14(float value)
	{
		___maxbordy_14 = value;
	}

	inline static int32_t get_offset_of_texture_15() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___texture_15)); }
	inline RenderTexture_t2666733923 * get_texture_15() const { return ___texture_15; }
	inline RenderTexture_t2666733923 ** get_address_of_texture_15() { return &___texture_15; }
	inline void set_texture_15(RenderTexture_t2666733923 * value)
	{
		___texture_15 = value;
		Il2CppCodeGenWriteBarrier(&___texture_15, value);
	}

	inline static int32_t get_offset_of_img_16() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___img_16)); }
	inline RawImage_t2749640213 * get_img_16() const { return ___img_16; }
	inline RawImage_t2749640213 ** get_address_of_img_16() { return &___img_16; }
	inline void set_img_16(RawImage_t2749640213 * value)
	{
		___img_16 = value;
		Il2CppCodeGenWriteBarrier(&___img_16, value);
	}

	inline static int32_t get_offset_of_oldPos_17() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___oldPos_17)); }
	inline Vector2_t2243707579  get_oldPos_17() const { return ___oldPos_17; }
	inline Vector2_t2243707579 * get_address_of_oldPos_17() { return &___oldPos_17; }
	inline void set_oldPos_17(Vector2_t2243707579  value)
	{
		___oldPos_17 = value;
	}

	inline static int32_t get_offset_of_pos_18() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___pos_18)); }
	inline Vector2_t2243707579  get_pos_18() const { return ___pos_18; }
	inline Vector2_t2243707579 * get_address_of_pos_18() { return &___pos_18; }
	inline void set_pos_18(Vector2_t2243707579  value)
	{
		___pos_18 = value;
	}

	inline static int32_t get_offset_of_count_19() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___count_19)); }
	inline float get_count_19() const { return ___count_19; }
	inline float* get_address_of_count_19() { return &___count_19; }
	inline void set_count_19(float value)
	{
		___count_19 = value;
	}

	inline static int32_t get_offset_of_testTex_20() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___testTex_20)); }
	inline Texture2D_t3542995729 * get_testTex_20() const { return ___testTex_20; }
	inline Texture2D_t3542995729 ** get_address_of_testTex_20() { return &___testTex_20; }
	inline void set_testTex_20(Texture2D_t3542995729 * value)
	{
		___testTex_20 = value;
		Il2CppCodeGenWriteBarrier(&___testTex_20, value);
	}

	inline static int32_t get_offset_of_clear_21() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___clear_21)); }
	inline Texture_t2243626319 * get_clear_21() const { return ___clear_21; }
	inline Texture_t2243626319 ** get_address_of_clear_21() { return &___clear_21; }
	inline void set_clear_21(Texture_t2243626319 * value)
	{
		___clear_21 = value;
		Il2CppCodeGenWriteBarrier(&___clear_21, value);
	}

	inline static int32_t get_offset_of_clearMaterial_22() { return static_cast<int32_t>(offsetof(Testscript_t905621679, ___clearMaterial_22)); }
	inline Material_t193706927 * get_clearMaterial_22() const { return ___clearMaterial_22; }
	inline Material_t193706927 ** get_address_of_clearMaterial_22() { return &___clearMaterial_22; }
	inline void set_clearMaterial_22(Material_t193706927 * value)
	{
		___clearMaterial_22 = value;
		Il2CppCodeGenWriteBarrier(&___clearMaterial_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
