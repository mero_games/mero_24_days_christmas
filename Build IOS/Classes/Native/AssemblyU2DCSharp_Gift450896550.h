﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// System.Action`1<Gift>
struct Action_1_t252695932;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gift
struct  Gift_t450896550  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Gift::force
	float ___force_2;
	// UnityEngine.Transform Gift::yeyPosition
	Transform_t3275118058 * ___yeyPosition_3;
	// UnityEngine.Rigidbody2D Gift::rb
	Rigidbody2D_t502193897 * ___rb_4;

public:
	inline static int32_t get_offset_of_force_2() { return static_cast<int32_t>(offsetof(Gift_t450896550, ___force_2)); }
	inline float get_force_2() const { return ___force_2; }
	inline float* get_address_of_force_2() { return &___force_2; }
	inline void set_force_2(float value)
	{
		___force_2 = value;
	}

	inline static int32_t get_offset_of_yeyPosition_3() { return static_cast<int32_t>(offsetof(Gift_t450896550, ___yeyPosition_3)); }
	inline Transform_t3275118058 * get_yeyPosition_3() const { return ___yeyPosition_3; }
	inline Transform_t3275118058 ** get_address_of_yeyPosition_3() { return &___yeyPosition_3; }
	inline void set_yeyPosition_3(Transform_t3275118058 * value)
	{
		___yeyPosition_3 = value;
		Il2CppCodeGenWriteBarrier(&___yeyPosition_3, value);
	}

	inline static int32_t get_offset_of_rb_4() { return static_cast<int32_t>(offsetof(Gift_t450896550, ___rb_4)); }
	inline Rigidbody2D_t502193897 * get_rb_4() const { return ___rb_4; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb_4() { return &___rb_4; }
	inline void set_rb_4(Rigidbody2D_t502193897 * value)
	{
		___rb_4 = value;
		Il2CppCodeGenWriteBarrier(&___rb_4, value);
	}
};

struct Gift_t450896550_StaticFields
{
public:
	// System.Action`1<Gift> Gift::<>f__am$cache0
	Action_1_t252695932 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(Gift_t450896550_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t252695932 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t252695932 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t252695932 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
