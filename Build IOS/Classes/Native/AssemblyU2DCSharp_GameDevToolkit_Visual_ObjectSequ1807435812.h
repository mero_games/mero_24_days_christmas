﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Visual.ObjectSequence/CurrentChildDelegate
struct CurrentChildDelegate_t135521474;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Visual.ObjectSequence
struct  ObjectSequence_t1807435812  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Visual.ObjectSequence/CurrentChildDelegate GameDevToolkit.Visual.ObjectSequence::CurrentChildChanged
	CurrentChildDelegate_t135521474 * ___CurrentChildChanged_2;
	// UnityEngine.GameObject[] GameDevToolkit.Visual.ObjectSequence::children
	GameObjectU5BU5D_t3057952154* ___children_3;
	// System.UInt32 GameDevToolkit.Visual.ObjectSequence::_currentChild
	uint32_t ____currentChild_4;
	// System.UInt32 GameDevToolkit.Visual.ObjectSequence::_actualCurrentChild
	uint32_t ____actualCurrentChild_5;
	// System.Boolean GameDevToolkit.Visual.ObjectSequence::startRandom
	bool ___startRandom_6;

public:
	inline static int32_t get_offset_of_CurrentChildChanged_2() { return static_cast<int32_t>(offsetof(ObjectSequence_t1807435812, ___CurrentChildChanged_2)); }
	inline CurrentChildDelegate_t135521474 * get_CurrentChildChanged_2() const { return ___CurrentChildChanged_2; }
	inline CurrentChildDelegate_t135521474 ** get_address_of_CurrentChildChanged_2() { return &___CurrentChildChanged_2; }
	inline void set_CurrentChildChanged_2(CurrentChildDelegate_t135521474 * value)
	{
		___CurrentChildChanged_2 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentChildChanged_2, value);
	}

	inline static int32_t get_offset_of_children_3() { return static_cast<int32_t>(offsetof(ObjectSequence_t1807435812, ___children_3)); }
	inline GameObjectU5BU5D_t3057952154* get_children_3() const { return ___children_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_children_3() { return &___children_3; }
	inline void set_children_3(GameObjectU5BU5D_t3057952154* value)
	{
		___children_3 = value;
		Il2CppCodeGenWriteBarrier(&___children_3, value);
	}

	inline static int32_t get_offset_of__currentChild_4() { return static_cast<int32_t>(offsetof(ObjectSequence_t1807435812, ____currentChild_4)); }
	inline uint32_t get__currentChild_4() const { return ____currentChild_4; }
	inline uint32_t* get_address_of__currentChild_4() { return &____currentChild_4; }
	inline void set__currentChild_4(uint32_t value)
	{
		____currentChild_4 = value;
	}

	inline static int32_t get_offset_of__actualCurrentChild_5() { return static_cast<int32_t>(offsetof(ObjectSequence_t1807435812, ____actualCurrentChild_5)); }
	inline uint32_t get__actualCurrentChild_5() const { return ____actualCurrentChild_5; }
	inline uint32_t* get_address_of__actualCurrentChild_5() { return &____actualCurrentChild_5; }
	inline void set__actualCurrentChild_5(uint32_t value)
	{
		____actualCurrentChild_5 = value;
	}

	inline static int32_t get_offset_of_startRandom_6() { return static_cast<int32_t>(offsetof(ObjectSequence_t1807435812, ___startRandom_6)); }
	inline bool get_startRandom_6() const { return ___startRandom_6; }
	inline bool* get_address_of_startRandom_6() { return &___startRandom_6; }
	inline void set_startRandom_6(bool value)
	{
		___startRandom_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
