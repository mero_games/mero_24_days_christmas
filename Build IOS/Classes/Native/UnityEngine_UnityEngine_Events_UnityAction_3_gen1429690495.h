﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate3201952435.h"
#include "mscorlib_System_Void1841601450.h"

// GameDevToolkit.Characters.RocketChar
struct RocketChar_t1772935162;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`3<GameDevToolkit.Characters.RocketChar,UnityEngine.Collider2D,UnityEngine.Collision2D>
struct  UnityAction_3_t1429690495  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
