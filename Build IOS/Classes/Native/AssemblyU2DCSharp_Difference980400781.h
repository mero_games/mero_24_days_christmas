﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Difference_StartType1229205632.h"
#include "AssemblyU2DCSharp_Difference_OnClickType2462927197.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Difference
struct  Difference_t980400781  : public MonoBehaviour_t1158329972
{
public:
	// Difference/StartType Difference::startType
	int32_t ___startType_2;
	// Difference/OnClickType Difference::onClickType
	int32_t ___onClickType_3;
	// System.Single Difference::rotateBy
	float ___rotateBy_4;
	// System.Boolean Difference::selected
	bool ___selected_5;
	// System.Boolean Difference::found
	bool ___found_6;
	// System.Boolean Difference::isVisible
	bool ___isVisible_7;

public:
	inline static int32_t get_offset_of_startType_2() { return static_cast<int32_t>(offsetof(Difference_t980400781, ___startType_2)); }
	inline int32_t get_startType_2() const { return ___startType_2; }
	inline int32_t* get_address_of_startType_2() { return &___startType_2; }
	inline void set_startType_2(int32_t value)
	{
		___startType_2 = value;
	}

	inline static int32_t get_offset_of_onClickType_3() { return static_cast<int32_t>(offsetof(Difference_t980400781, ___onClickType_3)); }
	inline int32_t get_onClickType_3() const { return ___onClickType_3; }
	inline int32_t* get_address_of_onClickType_3() { return &___onClickType_3; }
	inline void set_onClickType_3(int32_t value)
	{
		___onClickType_3 = value;
	}

	inline static int32_t get_offset_of_rotateBy_4() { return static_cast<int32_t>(offsetof(Difference_t980400781, ___rotateBy_4)); }
	inline float get_rotateBy_4() const { return ___rotateBy_4; }
	inline float* get_address_of_rotateBy_4() { return &___rotateBy_4; }
	inline void set_rotateBy_4(float value)
	{
		___rotateBy_4 = value;
	}

	inline static int32_t get_offset_of_selected_5() { return static_cast<int32_t>(offsetof(Difference_t980400781, ___selected_5)); }
	inline bool get_selected_5() const { return ___selected_5; }
	inline bool* get_address_of_selected_5() { return &___selected_5; }
	inline void set_selected_5(bool value)
	{
		___selected_5 = value;
	}

	inline static int32_t get_offset_of_found_6() { return static_cast<int32_t>(offsetof(Difference_t980400781, ___found_6)); }
	inline bool get_found_6() const { return ___found_6; }
	inline bool* get_address_of_found_6() { return &___found_6; }
	inline void set_found_6(bool value)
	{
		___found_6 = value;
	}

	inline static int32_t get_offset_of_isVisible_7() { return static_cast<int32_t>(offsetof(Difference_t980400781, ___isVisible_7)); }
	inline bool get_isVisible_7() const { return ___isVisible_7; }
	inline bool* get_address_of_isVisible_7() { return &___isVisible_7; }
	inline void set_isVisible_7(bool value)
	{
		___isVisible_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
