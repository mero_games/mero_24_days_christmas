﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_GameEighteenManager_GameRounds1482069856.h"

// GameEighteenManager
struct GameEighteenManager_t3662215866;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEighteenManager/<YShowGame>c__Iterator0
struct  U3CYShowGameU3Ec__Iterator0_t1547970616  : public Il2CppObject
{
public:
	// System.Int32 GameEighteenManager/<YShowGame>c__Iterator0::ind
	int32_t ___ind_0;
	// GameEighteenManager/GameRounds GameEighteenManager/<YShowGame>c__Iterator0::<round>__0
	GameRounds_t1482069856  ___U3CroundU3E__0_1;
	// System.Boolean GameEighteenManager/<YShowGame>c__Iterator0::random
	bool ___random_2;
	// System.Int32 GameEighteenManager/<YShowGame>c__Iterator0::<k>__1
	int32_t ___U3CkU3E__1_3;
	// System.Int32 GameEighteenManager/<YShowGame>c__Iterator0::<i>__2
	int32_t ___U3CiU3E__2_4;
	// System.Int32 GameEighteenManager/<YShowGame>c__Iterator0::<rand>__0
	int32_t ___U3CrandU3E__0_5;
	// System.Int32 GameEighteenManager/<YShowGame>c__Iterator0::<i>__3
	int32_t ___U3CiU3E__3_6;
	// System.Int32 GameEighteenManager/<YShowGame>c__Iterator0::<i>__4
	int32_t ___U3CiU3E__4_7;
	// GameEighteenManager GameEighteenManager/<YShowGame>c__Iterator0::$this
	GameEighteenManager_t3662215866 * ___U24this_8;
	// System.Object GameEighteenManager/<YShowGame>c__Iterator0::$current
	Il2CppObject * ___U24current_9;
	// System.Boolean GameEighteenManager/<YShowGame>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 GameEighteenManager/<YShowGame>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_ind_0() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___ind_0)); }
	inline int32_t get_ind_0() const { return ___ind_0; }
	inline int32_t* get_address_of_ind_0() { return &___ind_0; }
	inline void set_ind_0(int32_t value)
	{
		___ind_0 = value;
	}

	inline static int32_t get_offset_of_U3CroundU3E__0_1() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___U3CroundU3E__0_1)); }
	inline GameRounds_t1482069856  get_U3CroundU3E__0_1() const { return ___U3CroundU3E__0_1; }
	inline GameRounds_t1482069856 * get_address_of_U3CroundU3E__0_1() { return &___U3CroundU3E__0_1; }
	inline void set_U3CroundU3E__0_1(GameRounds_t1482069856  value)
	{
		___U3CroundU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_random_2() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___random_2)); }
	inline bool get_random_2() const { return ___random_2; }
	inline bool* get_address_of_random_2() { return &___random_2; }
	inline void set_random_2(bool value)
	{
		___random_2 = value;
	}

	inline static int32_t get_offset_of_U3CkU3E__1_3() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___U3CkU3E__1_3)); }
	inline int32_t get_U3CkU3E__1_3() const { return ___U3CkU3E__1_3; }
	inline int32_t* get_address_of_U3CkU3E__1_3() { return &___U3CkU3E__1_3; }
	inline void set_U3CkU3E__1_3(int32_t value)
	{
		___U3CkU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__2_4() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___U3CiU3E__2_4)); }
	inline int32_t get_U3CiU3E__2_4() const { return ___U3CiU3E__2_4; }
	inline int32_t* get_address_of_U3CiU3E__2_4() { return &___U3CiU3E__2_4; }
	inline void set_U3CiU3E__2_4(int32_t value)
	{
		___U3CiU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CrandU3E__0_5() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___U3CrandU3E__0_5)); }
	inline int32_t get_U3CrandU3E__0_5() const { return ___U3CrandU3E__0_5; }
	inline int32_t* get_address_of_U3CrandU3E__0_5() { return &___U3CrandU3E__0_5; }
	inline void set_U3CrandU3E__0_5(int32_t value)
	{
		___U3CrandU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__3_6() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___U3CiU3E__3_6)); }
	inline int32_t get_U3CiU3E__3_6() const { return ___U3CiU3E__3_6; }
	inline int32_t* get_address_of_U3CiU3E__3_6() { return &___U3CiU3E__3_6; }
	inline void set_U3CiU3E__3_6(int32_t value)
	{
		___U3CiU3E__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__4_7() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___U3CiU3E__4_7)); }
	inline int32_t get_U3CiU3E__4_7() const { return ___U3CiU3E__4_7; }
	inline int32_t* get_address_of_U3CiU3E__4_7() { return &___U3CiU3E__4_7; }
	inline void set_U3CiU3E__4_7(int32_t value)
	{
		___U3CiU3E__4_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___U24this_8)); }
	inline GameEighteenManager_t3662215866 * get_U24this_8() const { return ___U24this_8; }
	inline GameEighteenManager_t3662215866 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(GameEighteenManager_t3662215866 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_8, value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CYShowGameU3Ec__Iterator0_t1547970616, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
