﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen2661730956.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"

// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// GameEighteenManager/GameRounds[]
struct GameRoundsU5BU5D_t2979938081;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEighteenManager
struct  GameEighteenManager_t3662215866  : public SingletonBase_1_t2661730956
{
public:
	// UnityEngine.Color[] GameEighteenManager::colors
	ColorU5BU5D_t672350442* ___colors_4;
	// System.Collections.Generic.List`1<UnityEngine.Transform> GameEighteenManager::bulbs
	List_1_t2644239190 * ___bulbs_5;
	// GameEighteenManager/GameRounds[] GameEighteenManager::rounds
	GameRoundsU5BU5D_t2979938081* ___rounds_6;
	// System.Boolean GameEighteenManager::swapColors
	bool ___swapColors_7;
	// UnityEngine.LayerMask GameEighteenManager::mask
	LayerMask_t3188175821  ___mask_8;
	// System.Single GameEighteenManager::waitTime
	float ___waitTime_9;
	// System.Int32 GameEighteenManager::curRound
	int32_t ___curRound_10;
	// System.Boolean GameEighteenManager::canClick
	bool ___canClick_11;
	// System.Collections.Generic.List`1<System.Int32> GameEighteenManager::clicks
	List_1_t1440998580 * ___clicks_12;
	// System.Collections.Generic.List`1<System.Int32> GameEighteenManager::oldClicks
	List_1_t1440998580 * ___oldClicks_13;

public:
	inline static int32_t get_offset_of_colors_4() { return static_cast<int32_t>(offsetof(GameEighteenManager_t3662215866, ___colors_4)); }
	inline ColorU5BU5D_t672350442* get_colors_4() const { return ___colors_4; }
	inline ColorU5BU5D_t672350442** get_address_of_colors_4() { return &___colors_4; }
	inline void set_colors_4(ColorU5BU5D_t672350442* value)
	{
		___colors_4 = value;
		Il2CppCodeGenWriteBarrier(&___colors_4, value);
	}

	inline static int32_t get_offset_of_bulbs_5() { return static_cast<int32_t>(offsetof(GameEighteenManager_t3662215866, ___bulbs_5)); }
	inline List_1_t2644239190 * get_bulbs_5() const { return ___bulbs_5; }
	inline List_1_t2644239190 ** get_address_of_bulbs_5() { return &___bulbs_5; }
	inline void set_bulbs_5(List_1_t2644239190 * value)
	{
		___bulbs_5 = value;
		Il2CppCodeGenWriteBarrier(&___bulbs_5, value);
	}

	inline static int32_t get_offset_of_rounds_6() { return static_cast<int32_t>(offsetof(GameEighteenManager_t3662215866, ___rounds_6)); }
	inline GameRoundsU5BU5D_t2979938081* get_rounds_6() const { return ___rounds_6; }
	inline GameRoundsU5BU5D_t2979938081** get_address_of_rounds_6() { return &___rounds_6; }
	inline void set_rounds_6(GameRoundsU5BU5D_t2979938081* value)
	{
		___rounds_6 = value;
		Il2CppCodeGenWriteBarrier(&___rounds_6, value);
	}

	inline static int32_t get_offset_of_swapColors_7() { return static_cast<int32_t>(offsetof(GameEighteenManager_t3662215866, ___swapColors_7)); }
	inline bool get_swapColors_7() const { return ___swapColors_7; }
	inline bool* get_address_of_swapColors_7() { return &___swapColors_7; }
	inline void set_swapColors_7(bool value)
	{
		___swapColors_7 = value;
	}

	inline static int32_t get_offset_of_mask_8() { return static_cast<int32_t>(offsetof(GameEighteenManager_t3662215866, ___mask_8)); }
	inline LayerMask_t3188175821  get_mask_8() const { return ___mask_8; }
	inline LayerMask_t3188175821 * get_address_of_mask_8() { return &___mask_8; }
	inline void set_mask_8(LayerMask_t3188175821  value)
	{
		___mask_8 = value;
	}

	inline static int32_t get_offset_of_waitTime_9() { return static_cast<int32_t>(offsetof(GameEighteenManager_t3662215866, ___waitTime_9)); }
	inline float get_waitTime_9() const { return ___waitTime_9; }
	inline float* get_address_of_waitTime_9() { return &___waitTime_9; }
	inline void set_waitTime_9(float value)
	{
		___waitTime_9 = value;
	}

	inline static int32_t get_offset_of_curRound_10() { return static_cast<int32_t>(offsetof(GameEighteenManager_t3662215866, ___curRound_10)); }
	inline int32_t get_curRound_10() const { return ___curRound_10; }
	inline int32_t* get_address_of_curRound_10() { return &___curRound_10; }
	inline void set_curRound_10(int32_t value)
	{
		___curRound_10 = value;
	}

	inline static int32_t get_offset_of_canClick_11() { return static_cast<int32_t>(offsetof(GameEighteenManager_t3662215866, ___canClick_11)); }
	inline bool get_canClick_11() const { return ___canClick_11; }
	inline bool* get_address_of_canClick_11() { return &___canClick_11; }
	inline void set_canClick_11(bool value)
	{
		___canClick_11 = value;
	}

	inline static int32_t get_offset_of_clicks_12() { return static_cast<int32_t>(offsetof(GameEighteenManager_t3662215866, ___clicks_12)); }
	inline List_1_t1440998580 * get_clicks_12() const { return ___clicks_12; }
	inline List_1_t1440998580 ** get_address_of_clicks_12() { return &___clicks_12; }
	inline void set_clicks_12(List_1_t1440998580 * value)
	{
		___clicks_12 = value;
		Il2CppCodeGenWriteBarrier(&___clicks_12, value);
	}

	inline static int32_t get_offset_of_oldClicks_13() { return static_cast<int32_t>(offsetof(GameEighteenManager_t3662215866, ___oldClicks_13)); }
	inline List_1_t1440998580 * get_oldClicks_13() const { return ___oldClicks_13; }
	inline List_1_t1440998580 ** get_address_of_oldClicks_13() { return &___oldClicks_13; }
	inline void set_oldClicks_13(List_1_t1440998580 * value)
	{
		___oldClicks_13 = value;
		Il2CppCodeGenWriteBarrier(&___oldClicks_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
