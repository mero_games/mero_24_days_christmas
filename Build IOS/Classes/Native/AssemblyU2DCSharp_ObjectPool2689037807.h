﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_ObjectPool_StartupPoolMode3405548370.h"

// ObjectPool/OnRecycleDelegate
struct OnRecycleDelegate_t3801249229;
// ObjectPool
struct ObjectPool_t2689037807;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t161032427;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,UnityEngine.GameObject>
struct Dictionary_2_t791911295;
// ObjectPool/StartupPool[]
struct StartupPoolU5BU5D_t3583423604;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectPool
struct  ObjectPool_t2689037807  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.Collections.Generic.List`1<UnityEngine.GameObject>> ObjectPool::pooledObjects
	Dictionary_2_t161032427 * ___pooledObjects_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,UnityEngine.GameObject> ObjectPool::spawnedObjects
	Dictionary_2_t791911295 * ___spawnedObjects_6;
	// ObjectPool/StartupPoolMode ObjectPool::startupPoolMode
	int32_t ___startupPoolMode_7;
	// ObjectPool/StartupPool[] ObjectPool::startupPools
	StartupPoolU5BU5D_t3583423604* ___startupPools_8;
	// System.Boolean ObjectPool::startupPoolsCreated
	bool ___startupPoolsCreated_9;

public:
	inline static int32_t get_offset_of_pooledObjects_5() { return static_cast<int32_t>(offsetof(ObjectPool_t2689037807, ___pooledObjects_5)); }
	inline Dictionary_2_t161032427 * get_pooledObjects_5() const { return ___pooledObjects_5; }
	inline Dictionary_2_t161032427 ** get_address_of_pooledObjects_5() { return &___pooledObjects_5; }
	inline void set_pooledObjects_5(Dictionary_2_t161032427 * value)
	{
		___pooledObjects_5 = value;
		Il2CppCodeGenWriteBarrier(&___pooledObjects_5, value);
	}

	inline static int32_t get_offset_of_spawnedObjects_6() { return static_cast<int32_t>(offsetof(ObjectPool_t2689037807, ___spawnedObjects_6)); }
	inline Dictionary_2_t791911295 * get_spawnedObjects_6() const { return ___spawnedObjects_6; }
	inline Dictionary_2_t791911295 ** get_address_of_spawnedObjects_6() { return &___spawnedObjects_6; }
	inline void set_spawnedObjects_6(Dictionary_2_t791911295 * value)
	{
		___spawnedObjects_6 = value;
		Il2CppCodeGenWriteBarrier(&___spawnedObjects_6, value);
	}

	inline static int32_t get_offset_of_startupPoolMode_7() { return static_cast<int32_t>(offsetof(ObjectPool_t2689037807, ___startupPoolMode_7)); }
	inline int32_t get_startupPoolMode_7() const { return ___startupPoolMode_7; }
	inline int32_t* get_address_of_startupPoolMode_7() { return &___startupPoolMode_7; }
	inline void set_startupPoolMode_7(int32_t value)
	{
		___startupPoolMode_7 = value;
	}

	inline static int32_t get_offset_of_startupPools_8() { return static_cast<int32_t>(offsetof(ObjectPool_t2689037807, ___startupPools_8)); }
	inline StartupPoolU5BU5D_t3583423604* get_startupPools_8() const { return ___startupPools_8; }
	inline StartupPoolU5BU5D_t3583423604** get_address_of_startupPools_8() { return &___startupPools_8; }
	inline void set_startupPools_8(StartupPoolU5BU5D_t3583423604* value)
	{
		___startupPools_8 = value;
		Il2CppCodeGenWriteBarrier(&___startupPools_8, value);
	}

	inline static int32_t get_offset_of_startupPoolsCreated_9() { return static_cast<int32_t>(offsetof(ObjectPool_t2689037807, ___startupPoolsCreated_9)); }
	inline bool get_startupPoolsCreated_9() const { return ___startupPoolsCreated_9; }
	inline bool* get_address_of_startupPoolsCreated_9() { return &___startupPoolsCreated_9; }
	inline void set_startupPoolsCreated_9(bool value)
	{
		___startupPoolsCreated_9 = value;
	}
};

struct ObjectPool_t2689037807_StaticFields
{
public:
	// ObjectPool/OnRecycleDelegate ObjectPool::OnRecyle
	OnRecycleDelegate_t3801249229 * ___OnRecyle_2;
	// ObjectPool ObjectPool::_instance
	ObjectPool_t2689037807 * ____instance_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ObjectPool::tempList
	List_1_t1125654279 * ___tempList_4;

public:
	inline static int32_t get_offset_of_OnRecyle_2() { return static_cast<int32_t>(offsetof(ObjectPool_t2689037807_StaticFields, ___OnRecyle_2)); }
	inline OnRecycleDelegate_t3801249229 * get_OnRecyle_2() const { return ___OnRecyle_2; }
	inline OnRecycleDelegate_t3801249229 ** get_address_of_OnRecyle_2() { return &___OnRecyle_2; }
	inline void set_OnRecyle_2(OnRecycleDelegate_t3801249229 * value)
	{
		___OnRecyle_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnRecyle_2, value);
	}

	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(ObjectPool_t2689037807_StaticFields, ____instance_3)); }
	inline ObjectPool_t2689037807 * get__instance_3() const { return ____instance_3; }
	inline ObjectPool_t2689037807 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(ObjectPool_t2689037807 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}

	inline static int32_t get_offset_of_tempList_4() { return static_cast<int32_t>(offsetof(ObjectPool_t2689037807_StaticFields, ___tempList_4)); }
	inline List_1_t1125654279 * get_tempList_4() const { return ___tempList_4; }
	inline List_1_t1125654279 ** get_address_of_tempList_4() { return &___tempList_4; }
	inline void set_tempList_4(List_1_t1125654279 * value)
	{
		___tempList_4 = value;
		Il2CppCodeGenWriteBarrier(&___tempList_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
