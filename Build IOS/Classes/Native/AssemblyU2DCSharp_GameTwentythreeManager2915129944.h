﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// ClickObject
struct ClickObject_t2003887325;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t1098056643;
// System.Collections.Generic.List`1<ClickObject>
struct List_1_t1373008457;
// GiftSpawner[]
struct GiftSpawnerU5BU5D_t225263411;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTwentythreeManager
struct  GameTwentythreeManager_t2915129944  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform GameTwentythreeManager::backgrounds
	Transform_t3275118058 * ___backgrounds_2;
	// UnityEngine.Transform GameTwentythreeManager::tree
	Transform_t3275118058 * ___tree_3;
	// UnityEngine.Transform GameTwentythreeManager::text
	Transform_t3275118058 * ___text_4;
	// UnityEngine.LayerMask GameTwentythreeManager::mask
	LayerMask_t3188175821  ___mask_5;
	// UnityEngine.LayerMask GameTwentythreeManager::treeMask
	LayerMask_t3188175821  ___treeMask_6;
	// System.Single GameTwentythreeManager::swipeSpeed
	float ___swipeSpeed_7;
	// ClickObject GameTwentythreeManager::nextButton
	ClickObject_t2003887325 * ___nextButton_8;
	// ClickObject GameTwentythreeManager::shareButton
	ClickObject_t2003887325 * ___shareButton_9;
	// ClickObject GameTwentythreeManager::exitButton
	ClickObject_t2003887325 * ___exitButton_10;
	// UnityEngine.Transform GameTwentythreeManager::gameLogo
	Transform_t3275118058 * ___gameLogo_11;
	// UnityEngine.Transform GameTwentythreeManager::treeBG
	Transform_t3275118058 * ___treeBG_12;
	// UnityEngine.Transform GameTwentythreeManager::tutorial
	Transform_t3275118058 * ___tutorial_13;
	// UnityEngine.Vector3 GameTwentythreeManager::fp
	Vector3_t2243707580  ___fp_14;
	// UnityEngine.Vector3 GameTwentythreeManager::lp
	Vector3_t2243707580  ___lp_15;
	// System.Single GameTwentythreeManager::dragDistance
	float ___dragDistance_16;
	// System.Boolean GameTwentythreeManager::canSwipe
	bool ___canSwipe_17;
	// System.Boolean GameTwentythreeManager::canDrag
	bool ___canDrag_18;
	// System.Boolean GameTwentythreeManager::canClick
	bool ___canClick_19;
	// UnityEngine.SpriteRenderer[] GameTwentythreeManager::bg
	SpriteRendererU5BU5D_t1098056643* ___bg_20;
	// System.Collections.Generic.List`1<ClickObject> GameTwentythreeManager::ornaments
	List_1_t1373008457 * ___ornaments_21;
	// ClickObject GameTwentythreeManager::dragObj
	ClickObject_t2003887325 * ___dragObj_22;
	// ClickObject GameTwentythreeManager::lastClick
	ClickObject_t2003887325 * ___lastClick_23;
	// GiftSpawner[] GameTwentythreeManager::spawners
	GiftSpawnerU5BU5D_t225263411* ___spawners_24;
	// System.Int32 GameTwentythreeManager::curImage
	int32_t ___curImage_25;
	// System.Int32 GameTwentythreeManager::curStage
	int32_t ___curStage_26;

public:
	inline static int32_t get_offset_of_backgrounds_2() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___backgrounds_2)); }
	inline Transform_t3275118058 * get_backgrounds_2() const { return ___backgrounds_2; }
	inline Transform_t3275118058 ** get_address_of_backgrounds_2() { return &___backgrounds_2; }
	inline void set_backgrounds_2(Transform_t3275118058 * value)
	{
		___backgrounds_2 = value;
		Il2CppCodeGenWriteBarrier(&___backgrounds_2, value);
	}

	inline static int32_t get_offset_of_tree_3() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___tree_3)); }
	inline Transform_t3275118058 * get_tree_3() const { return ___tree_3; }
	inline Transform_t3275118058 ** get_address_of_tree_3() { return &___tree_3; }
	inline void set_tree_3(Transform_t3275118058 * value)
	{
		___tree_3 = value;
		Il2CppCodeGenWriteBarrier(&___tree_3, value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___text_4)); }
	inline Transform_t3275118058 * get_text_4() const { return ___text_4; }
	inline Transform_t3275118058 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Transform_t3275118058 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier(&___text_4, value);
	}

	inline static int32_t get_offset_of_mask_5() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___mask_5)); }
	inline LayerMask_t3188175821  get_mask_5() const { return ___mask_5; }
	inline LayerMask_t3188175821 * get_address_of_mask_5() { return &___mask_5; }
	inline void set_mask_5(LayerMask_t3188175821  value)
	{
		___mask_5 = value;
	}

	inline static int32_t get_offset_of_treeMask_6() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___treeMask_6)); }
	inline LayerMask_t3188175821  get_treeMask_6() const { return ___treeMask_6; }
	inline LayerMask_t3188175821 * get_address_of_treeMask_6() { return &___treeMask_6; }
	inline void set_treeMask_6(LayerMask_t3188175821  value)
	{
		___treeMask_6 = value;
	}

	inline static int32_t get_offset_of_swipeSpeed_7() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___swipeSpeed_7)); }
	inline float get_swipeSpeed_7() const { return ___swipeSpeed_7; }
	inline float* get_address_of_swipeSpeed_7() { return &___swipeSpeed_7; }
	inline void set_swipeSpeed_7(float value)
	{
		___swipeSpeed_7 = value;
	}

	inline static int32_t get_offset_of_nextButton_8() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___nextButton_8)); }
	inline ClickObject_t2003887325 * get_nextButton_8() const { return ___nextButton_8; }
	inline ClickObject_t2003887325 ** get_address_of_nextButton_8() { return &___nextButton_8; }
	inline void set_nextButton_8(ClickObject_t2003887325 * value)
	{
		___nextButton_8 = value;
		Il2CppCodeGenWriteBarrier(&___nextButton_8, value);
	}

	inline static int32_t get_offset_of_shareButton_9() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___shareButton_9)); }
	inline ClickObject_t2003887325 * get_shareButton_9() const { return ___shareButton_9; }
	inline ClickObject_t2003887325 ** get_address_of_shareButton_9() { return &___shareButton_9; }
	inline void set_shareButton_9(ClickObject_t2003887325 * value)
	{
		___shareButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___shareButton_9, value);
	}

	inline static int32_t get_offset_of_exitButton_10() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___exitButton_10)); }
	inline ClickObject_t2003887325 * get_exitButton_10() const { return ___exitButton_10; }
	inline ClickObject_t2003887325 ** get_address_of_exitButton_10() { return &___exitButton_10; }
	inline void set_exitButton_10(ClickObject_t2003887325 * value)
	{
		___exitButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___exitButton_10, value);
	}

	inline static int32_t get_offset_of_gameLogo_11() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___gameLogo_11)); }
	inline Transform_t3275118058 * get_gameLogo_11() const { return ___gameLogo_11; }
	inline Transform_t3275118058 ** get_address_of_gameLogo_11() { return &___gameLogo_11; }
	inline void set_gameLogo_11(Transform_t3275118058 * value)
	{
		___gameLogo_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameLogo_11, value);
	}

	inline static int32_t get_offset_of_treeBG_12() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___treeBG_12)); }
	inline Transform_t3275118058 * get_treeBG_12() const { return ___treeBG_12; }
	inline Transform_t3275118058 ** get_address_of_treeBG_12() { return &___treeBG_12; }
	inline void set_treeBG_12(Transform_t3275118058 * value)
	{
		___treeBG_12 = value;
		Il2CppCodeGenWriteBarrier(&___treeBG_12, value);
	}

	inline static int32_t get_offset_of_tutorial_13() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___tutorial_13)); }
	inline Transform_t3275118058 * get_tutorial_13() const { return ___tutorial_13; }
	inline Transform_t3275118058 ** get_address_of_tutorial_13() { return &___tutorial_13; }
	inline void set_tutorial_13(Transform_t3275118058 * value)
	{
		___tutorial_13 = value;
		Il2CppCodeGenWriteBarrier(&___tutorial_13, value);
	}

	inline static int32_t get_offset_of_fp_14() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___fp_14)); }
	inline Vector3_t2243707580  get_fp_14() const { return ___fp_14; }
	inline Vector3_t2243707580 * get_address_of_fp_14() { return &___fp_14; }
	inline void set_fp_14(Vector3_t2243707580  value)
	{
		___fp_14 = value;
	}

	inline static int32_t get_offset_of_lp_15() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___lp_15)); }
	inline Vector3_t2243707580  get_lp_15() const { return ___lp_15; }
	inline Vector3_t2243707580 * get_address_of_lp_15() { return &___lp_15; }
	inline void set_lp_15(Vector3_t2243707580  value)
	{
		___lp_15 = value;
	}

	inline static int32_t get_offset_of_dragDistance_16() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___dragDistance_16)); }
	inline float get_dragDistance_16() const { return ___dragDistance_16; }
	inline float* get_address_of_dragDistance_16() { return &___dragDistance_16; }
	inline void set_dragDistance_16(float value)
	{
		___dragDistance_16 = value;
	}

	inline static int32_t get_offset_of_canSwipe_17() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___canSwipe_17)); }
	inline bool get_canSwipe_17() const { return ___canSwipe_17; }
	inline bool* get_address_of_canSwipe_17() { return &___canSwipe_17; }
	inline void set_canSwipe_17(bool value)
	{
		___canSwipe_17 = value;
	}

	inline static int32_t get_offset_of_canDrag_18() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___canDrag_18)); }
	inline bool get_canDrag_18() const { return ___canDrag_18; }
	inline bool* get_address_of_canDrag_18() { return &___canDrag_18; }
	inline void set_canDrag_18(bool value)
	{
		___canDrag_18 = value;
	}

	inline static int32_t get_offset_of_canClick_19() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___canClick_19)); }
	inline bool get_canClick_19() const { return ___canClick_19; }
	inline bool* get_address_of_canClick_19() { return &___canClick_19; }
	inline void set_canClick_19(bool value)
	{
		___canClick_19 = value;
	}

	inline static int32_t get_offset_of_bg_20() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___bg_20)); }
	inline SpriteRendererU5BU5D_t1098056643* get_bg_20() const { return ___bg_20; }
	inline SpriteRendererU5BU5D_t1098056643** get_address_of_bg_20() { return &___bg_20; }
	inline void set_bg_20(SpriteRendererU5BU5D_t1098056643* value)
	{
		___bg_20 = value;
		Il2CppCodeGenWriteBarrier(&___bg_20, value);
	}

	inline static int32_t get_offset_of_ornaments_21() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___ornaments_21)); }
	inline List_1_t1373008457 * get_ornaments_21() const { return ___ornaments_21; }
	inline List_1_t1373008457 ** get_address_of_ornaments_21() { return &___ornaments_21; }
	inline void set_ornaments_21(List_1_t1373008457 * value)
	{
		___ornaments_21 = value;
		Il2CppCodeGenWriteBarrier(&___ornaments_21, value);
	}

	inline static int32_t get_offset_of_dragObj_22() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___dragObj_22)); }
	inline ClickObject_t2003887325 * get_dragObj_22() const { return ___dragObj_22; }
	inline ClickObject_t2003887325 ** get_address_of_dragObj_22() { return &___dragObj_22; }
	inline void set_dragObj_22(ClickObject_t2003887325 * value)
	{
		___dragObj_22 = value;
		Il2CppCodeGenWriteBarrier(&___dragObj_22, value);
	}

	inline static int32_t get_offset_of_lastClick_23() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___lastClick_23)); }
	inline ClickObject_t2003887325 * get_lastClick_23() const { return ___lastClick_23; }
	inline ClickObject_t2003887325 ** get_address_of_lastClick_23() { return &___lastClick_23; }
	inline void set_lastClick_23(ClickObject_t2003887325 * value)
	{
		___lastClick_23 = value;
		Il2CppCodeGenWriteBarrier(&___lastClick_23, value);
	}

	inline static int32_t get_offset_of_spawners_24() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___spawners_24)); }
	inline GiftSpawnerU5BU5D_t225263411* get_spawners_24() const { return ___spawners_24; }
	inline GiftSpawnerU5BU5D_t225263411** get_address_of_spawners_24() { return &___spawners_24; }
	inline void set_spawners_24(GiftSpawnerU5BU5D_t225263411* value)
	{
		___spawners_24 = value;
		Il2CppCodeGenWriteBarrier(&___spawners_24, value);
	}

	inline static int32_t get_offset_of_curImage_25() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___curImage_25)); }
	inline int32_t get_curImage_25() const { return ___curImage_25; }
	inline int32_t* get_address_of_curImage_25() { return &___curImage_25; }
	inline void set_curImage_25(int32_t value)
	{
		___curImage_25 = value;
	}

	inline static int32_t get_offset_of_curStage_26() { return static_cast<int32_t>(offsetof(GameTwentythreeManager_t2915129944, ___curStage_26)); }
	inline int32_t get_curStage_26() const { return ___curStage_26; }
	inline int32_t* get_address_of_curStage_26() { return &___curStage_26; }
	inline void set_curStage_26(int32_t value)
	{
		___curStage_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
