﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Collider2D
struct Collider2D_t646061738;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AvatarCreator
struct  AvatarCreator_t3048088315  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Collider2D AvatarCreator::faceCollider
	Collider2D_t646061738 * ___faceCollider_2;
	// UnityEngine.Collider2D AvatarCreator::eyeCollider
	Collider2D_t646061738 * ___eyeCollider_3;
	// UnityEngine.Collider2D AvatarCreator::hairCollider
	Collider2D_t646061738 * ___hairCollider_4;

public:
	inline static int32_t get_offset_of_faceCollider_2() { return static_cast<int32_t>(offsetof(AvatarCreator_t3048088315, ___faceCollider_2)); }
	inline Collider2D_t646061738 * get_faceCollider_2() const { return ___faceCollider_2; }
	inline Collider2D_t646061738 ** get_address_of_faceCollider_2() { return &___faceCollider_2; }
	inline void set_faceCollider_2(Collider2D_t646061738 * value)
	{
		___faceCollider_2 = value;
		Il2CppCodeGenWriteBarrier(&___faceCollider_2, value);
	}

	inline static int32_t get_offset_of_eyeCollider_3() { return static_cast<int32_t>(offsetof(AvatarCreator_t3048088315, ___eyeCollider_3)); }
	inline Collider2D_t646061738 * get_eyeCollider_3() const { return ___eyeCollider_3; }
	inline Collider2D_t646061738 ** get_address_of_eyeCollider_3() { return &___eyeCollider_3; }
	inline void set_eyeCollider_3(Collider2D_t646061738 * value)
	{
		___eyeCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&___eyeCollider_3, value);
	}

	inline static int32_t get_offset_of_hairCollider_4() { return static_cast<int32_t>(offsetof(AvatarCreator_t3048088315, ___hairCollider_4)); }
	inline Collider2D_t646061738 * get_hairCollider_4() const { return ___hairCollider_4; }
	inline Collider2D_t646061738 ** get_address_of_hairCollider_4() { return &___hairCollider_4; }
	inline void set_hairCollider_4(Collider2D_t646061738 * value)
	{
		___hairCollider_4 = value;
		Il2CppCodeGenWriteBarrier(&___hairCollider_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
