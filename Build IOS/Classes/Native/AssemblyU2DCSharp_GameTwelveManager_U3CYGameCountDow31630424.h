﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameTwelveManager
struct GameTwelveManager_t2786305482;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTwelveManager/<YGameCountDown>c__Iterator2
struct  U3CYGameCountDownU3Ec__Iterator2_t31630424  : public Il2CppObject
{
public:
	// System.Single GameTwelveManager/<YGameCountDown>c__Iterator2::<timer>__0
	float ___U3CtimerU3E__0_0;
	// System.Boolean GameTwelveManager/<YGameCountDown>c__Iterator2::<fastStarted>__0
	bool ___U3CfastStartedU3E__0_1;
	// System.Int32 GameTwelveManager/<YGameCountDown>c__Iterator2::<intSecond>__1
	int32_t ___U3CintSecondU3E__1_2;
	// GameTwelveManager GameTwelveManager/<YGameCountDown>c__Iterator2::$this
	GameTwelveManager_t2786305482 * ___U24this_3;
	// System.Object GameTwelveManager/<YGameCountDown>c__Iterator2::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean GameTwelveManager/<YGameCountDown>c__Iterator2::$disposing
	bool ___U24disposing_5;
	// System.Int32 GameTwelveManager/<YGameCountDown>c__Iterator2::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtimerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CYGameCountDownU3Ec__Iterator2_t31630424, ___U3CtimerU3E__0_0)); }
	inline float get_U3CtimerU3E__0_0() const { return ___U3CtimerU3E__0_0; }
	inline float* get_address_of_U3CtimerU3E__0_0() { return &___U3CtimerU3E__0_0; }
	inline void set_U3CtimerU3E__0_0(float value)
	{
		___U3CtimerU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CfastStartedU3E__0_1() { return static_cast<int32_t>(offsetof(U3CYGameCountDownU3Ec__Iterator2_t31630424, ___U3CfastStartedU3E__0_1)); }
	inline bool get_U3CfastStartedU3E__0_1() const { return ___U3CfastStartedU3E__0_1; }
	inline bool* get_address_of_U3CfastStartedU3E__0_1() { return &___U3CfastStartedU3E__0_1; }
	inline void set_U3CfastStartedU3E__0_1(bool value)
	{
		___U3CfastStartedU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CintSecondU3E__1_2() { return static_cast<int32_t>(offsetof(U3CYGameCountDownU3Ec__Iterator2_t31630424, ___U3CintSecondU3E__1_2)); }
	inline int32_t get_U3CintSecondU3E__1_2() const { return ___U3CintSecondU3E__1_2; }
	inline int32_t* get_address_of_U3CintSecondU3E__1_2() { return &___U3CintSecondU3E__1_2; }
	inline void set_U3CintSecondU3E__1_2(int32_t value)
	{
		___U3CintSecondU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CYGameCountDownU3Ec__Iterator2_t31630424, ___U24this_3)); }
	inline GameTwelveManager_t2786305482 * get_U24this_3() const { return ___U24this_3; }
	inline GameTwelveManager_t2786305482 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(GameTwelveManager_t2786305482 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CYGameCountDownU3Ec__Iterator2_t31630424, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CYGameCountDownU3Ec__Iterator2_t31630424, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CYGameCountDownU3Ec__Iterator2_t31630424, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
