﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_UIButton3377238306.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIButtonLoadGame
struct  UIButtonLoadGame_t1239272532  : public UIButton_t3377238306
{
public:
	// System.Int32 UIButtonLoadGame::sceneToLoad
	int32_t ___sceneToLoad_4;
	// System.Boolean UIButtonLoadGame::isOpen
	bool ___isOpen_5;
	// System.Boolean UIButtonLoadGame::notDone
	bool ___notDone_6;
	// UnityEngine.GameObject UIButtonLoadGame::bg
	GameObject_t1756533147 * ___bg_7;

public:
	inline static int32_t get_offset_of_sceneToLoad_4() { return static_cast<int32_t>(offsetof(UIButtonLoadGame_t1239272532, ___sceneToLoad_4)); }
	inline int32_t get_sceneToLoad_4() const { return ___sceneToLoad_4; }
	inline int32_t* get_address_of_sceneToLoad_4() { return &___sceneToLoad_4; }
	inline void set_sceneToLoad_4(int32_t value)
	{
		___sceneToLoad_4 = value;
	}

	inline static int32_t get_offset_of_isOpen_5() { return static_cast<int32_t>(offsetof(UIButtonLoadGame_t1239272532, ___isOpen_5)); }
	inline bool get_isOpen_5() const { return ___isOpen_5; }
	inline bool* get_address_of_isOpen_5() { return &___isOpen_5; }
	inline void set_isOpen_5(bool value)
	{
		___isOpen_5 = value;
	}

	inline static int32_t get_offset_of_notDone_6() { return static_cast<int32_t>(offsetof(UIButtonLoadGame_t1239272532, ___notDone_6)); }
	inline bool get_notDone_6() const { return ___notDone_6; }
	inline bool* get_address_of_notDone_6() { return &___notDone_6; }
	inline void set_notDone_6(bool value)
	{
		___notDone_6 = value;
	}

	inline static int32_t get_offset_of_bg_7() { return static_cast<int32_t>(offsetof(UIButtonLoadGame_t1239272532, ___bg_7)); }
	inline GameObject_t1756533147 * get_bg_7() const { return ___bg_7; }
	inline GameObject_t1756533147 ** get_address_of_bg_7() { return &___bg_7; }
	inline void set_bg_7(GameObject_t1756533147 * value)
	{
		___bg_7 = value;
		Il2CppCodeGenWriteBarrier(&___bg_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
