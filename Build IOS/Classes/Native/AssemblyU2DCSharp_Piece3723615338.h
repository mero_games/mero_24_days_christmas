﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Piece
struct  Piece_t3723615338  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Piece::lastFall
	float ___lastFall_2;
	// System.Single Piece::lastKeyDown
	float ___lastKeyDown_3;
	// System.Single Piece::timeKeyPressed
	float ___timeKeyPressed_4;
	// UnityEngine.Vector3 Piece::fp
	Vector3_t2243707580  ___fp_5;
	// UnityEngine.Vector3 Piece::lp
	Vector3_t2243707580  ___lp_6;
	// System.Single Piece::dragDistance
	float ___dragDistance_7;
	// UnityEngine.Vector3 Piece::touchPosWorld
	Vector3_t2243707580  ___touchPosWorld_8;

public:
	inline static int32_t get_offset_of_lastFall_2() { return static_cast<int32_t>(offsetof(Piece_t3723615338, ___lastFall_2)); }
	inline float get_lastFall_2() const { return ___lastFall_2; }
	inline float* get_address_of_lastFall_2() { return &___lastFall_2; }
	inline void set_lastFall_2(float value)
	{
		___lastFall_2 = value;
	}

	inline static int32_t get_offset_of_lastKeyDown_3() { return static_cast<int32_t>(offsetof(Piece_t3723615338, ___lastKeyDown_3)); }
	inline float get_lastKeyDown_3() const { return ___lastKeyDown_3; }
	inline float* get_address_of_lastKeyDown_3() { return &___lastKeyDown_3; }
	inline void set_lastKeyDown_3(float value)
	{
		___lastKeyDown_3 = value;
	}

	inline static int32_t get_offset_of_timeKeyPressed_4() { return static_cast<int32_t>(offsetof(Piece_t3723615338, ___timeKeyPressed_4)); }
	inline float get_timeKeyPressed_4() const { return ___timeKeyPressed_4; }
	inline float* get_address_of_timeKeyPressed_4() { return &___timeKeyPressed_4; }
	inline void set_timeKeyPressed_4(float value)
	{
		___timeKeyPressed_4 = value;
	}

	inline static int32_t get_offset_of_fp_5() { return static_cast<int32_t>(offsetof(Piece_t3723615338, ___fp_5)); }
	inline Vector3_t2243707580  get_fp_5() const { return ___fp_5; }
	inline Vector3_t2243707580 * get_address_of_fp_5() { return &___fp_5; }
	inline void set_fp_5(Vector3_t2243707580  value)
	{
		___fp_5 = value;
	}

	inline static int32_t get_offset_of_lp_6() { return static_cast<int32_t>(offsetof(Piece_t3723615338, ___lp_6)); }
	inline Vector3_t2243707580  get_lp_6() const { return ___lp_6; }
	inline Vector3_t2243707580 * get_address_of_lp_6() { return &___lp_6; }
	inline void set_lp_6(Vector3_t2243707580  value)
	{
		___lp_6 = value;
	}

	inline static int32_t get_offset_of_dragDistance_7() { return static_cast<int32_t>(offsetof(Piece_t3723615338, ___dragDistance_7)); }
	inline float get_dragDistance_7() const { return ___dragDistance_7; }
	inline float* get_address_of_dragDistance_7() { return &___dragDistance_7; }
	inline void set_dragDistance_7(float value)
	{
		___dragDistance_7 = value;
	}

	inline static int32_t get_offset_of_touchPosWorld_8() { return static_cast<int32_t>(offsetof(Piece_t3723615338, ___touchPosWorld_8)); }
	inline Vector3_t2243707580  get_touchPosWorld_8() const { return ___touchPosWorld_8; }
	inline Vector3_t2243707580 * get_address_of_touchPosWorld_8() { return &___touchPosWorld_8; }
	inline void set_touchPosWorld_8(Vector3_t2243707580  value)
	{
		___touchPosWorld_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
