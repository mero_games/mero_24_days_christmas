﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"





extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t2071877448_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0 = { 1, GenInst_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Char_t3454481338_0_0_0;
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0 = { 1, GenInst_Char_t3454481338_0_0_0_Types };
extern const Il2CppType Int64_t909078037_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0 = { 1, GenInst_Int64_t909078037_0_0_0_Types };
extern const Il2CppType UInt32_t2149682021_0_0_0;
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0 = { 1, GenInst_UInt32_t2149682021_0_0_0_Types };
extern const Il2CppType UInt64_t2909196914_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0 = { 1, GenInst_UInt64_t2909196914_0_0_0_Types };
extern const Il2CppType Byte_t3683104436_0_0_0;
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0 = { 1, GenInst_Byte_t3683104436_0_0_0_Types };
extern const Il2CppType SByte_t454417549_0_0_0;
static const Il2CppType* GenInst_SByte_t454417549_0_0_0_Types[] = { &SByte_t454417549_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0 = { 1, GenInst_SByte_t454417549_0_0_0_Types };
extern const Il2CppType Int16_t4041245914_0_0_0;
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Types[] = { &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0 = { 1, GenInst_Int16_t4041245914_0_0_0_Types };
extern const Il2CppType UInt16_t986882611_0_0_0;
static const Il2CppType* GenInst_UInt16_t986882611_0_0_0_Types[] = { &UInt16_t986882611_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0 = { 1, GenInst_UInt16_t986882611_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IConvertible_t908092482_0_0_0;
static const Il2CppType* GenInst_IConvertible_t908092482_0_0_0_Types[] = { &IConvertible_t908092482_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t908092482_0_0_0 = { 1, GenInst_IConvertible_t908092482_0_0_0_Types };
extern const Il2CppType IComparable_t1857082765_0_0_0;
static const Il2CppType* GenInst_IComparable_t1857082765_0_0_0_Types[] = { &IComparable_t1857082765_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1857082765_0_0_0 = { 1, GenInst_IComparable_t1857082765_0_0_0_Types };
extern const Il2CppType IEnumerable_t2911409499_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t2911409499_0_0_0_Types[] = { &IEnumerable_t2911409499_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t2911409499_0_0_0 = { 1, GenInst_IEnumerable_t2911409499_0_0_0_Types };
extern const Il2CppType ICloneable_t3853279282_0_0_0;
static const Il2CppType* GenInst_ICloneable_t3853279282_0_0_0_Types[] = { &ICloneable_t3853279282_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t3853279282_0_0_0 = { 1, GenInst_ICloneable_t3853279282_0_0_0_Types };
extern const Il2CppType IComparable_1_t3861059456_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3861059456_0_0_0_Types[] = { &IComparable_1_t3861059456_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3861059456_0_0_0 = { 1, GenInst_IComparable_1_t3861059456_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4233202402_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4233202402_0_0_0_Types[] = { &IEquatable_1_t4233202402_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4233202402_0_0_0 = { 1, GenInst_IEquatable_1_t4233202402_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t3412036974_0_0_0;
static const Il2CppType* GenInst_IReflect_t3412036974_0_0_0_Types[] = { &IReflect_t3412036974_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3412036974_0_0_0 = { 1, GenInst_IReflect_t3412036974_0_0_0_Types };
extern const Il2CppType _Type_t102776839_0_0_0;
static const Il2CppType* GenInst__Type_t102776839_0_0_0_Types[] = { &_Type_t102776839_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t102776839_0_0_0 = { 1, GenInst__Type_t102776839_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t502202687_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types };
extern const Il2CppType _MemberInfo_t332722161_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t332722161_0_0_0_Types[] = { &_MemberInfo_t332722161_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t332722161_0_0_0 = { 1, GenInst__MemberInfo_t332722161_0_0_0_Types };
extern const Il2CppType Double_t4078015681_0_0_0;
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Types[] = { &Double_t4078015681_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0 = { 1, GenInst_Double_t4078015681_0_0_0_Types };
extern const Il2CppType Single_t2076509932_0_0_0;
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0 = { 1, GenInst_Single_t2076509932_0_0_0_Types };
extern const Il2CppType Decimal_t724701077_0_0_0;
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Types[] = { &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0 = { 1, GenInst_Decimal_t724701077_0_0_0_Types };
extern const Il2CppType Boolean_t3825574718_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0 = { 1, GenInst_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Delegate_t3022476291_0_0_0;
static const Il2CppType* GenInst_Delegate_t3022476291_0_0_0_Types[] = { &Delegate_t3022476291_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3022476291_0_0_0 = { 1, GenInst_Delegate_t3022476291_0_0_0_Types };
extern const Il2CppType ISerializable_t1245643778_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1245643778_0_0_0_Types[] = { &ISerializable_t1245643778_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1245643778_0_0_0 = { 1, GenInst_ISerializable_t1245643778_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0 = { 1, GenInst_ParameterInfo_t2249040075_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t470209990_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t470209990_0_0_0_Types[] = { &_ParameterInfo_t470209990_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t470209990_0_0_0 = { 1, GenInst__ParameterInfo_t470209990_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1820634920_0_0_0_Types[] = { &ParameterModifier_t1820634920_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1820634920_0_0_0 = { 1, GenInst_ParameterModifier_t1820634920_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2511231167_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2511231167_0_0_0_Types[] = { &_FieldInfo_t2511231167_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2511231167_0_0_0 = { 1, GenInst__FieldInfo_t2511231167_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3642518830_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3642518830_0_0_0_Types[] = { &_MethodInfo_t3642518830_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3642518830_0_0_0 = { 1, GenInst__MethodInfo_t3642518830_0_0_0_Types };
extern const Il2CppType MethodBase_t904190842_0_0_0;
static const Il2CppType* GenInst_MethodBase_t904190842_0_0_0_Types[] = { &MethodBase_t904190842_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t904190842_0_0_0 = { 1, GenInst_MethodBase_t904190842_0_0_0_Types };
extern const Il2CppType _MethodBase_t1935530873_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1935530873_0_0_0_Types[] = { &_MethodBase_t1935530873_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1935530873_0_0_0 = { 1, GenInst__MethodBase_t1935530873_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t2851816542_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t2851816542_0_0_0_Types[] = { &ConstructorInfo_t2851816542_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0 = { 1, GenInst_ConstructorInfo_t2851816542_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3269099341_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3269099341_0_0_0_Types[] = { &_ConstructorInfo_t3269099341_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3269099341_0_0_0 = { 1, GenInst__ConstructorInfo_t3269099341_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t2011406615_0_0_0;
static const Il2CppType* GenInst_TableRange_t2011406615_0_0_0_Types[] = { &TableRange_t2011406615_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t2011406615_0_0_0 = { 1, GenInst_TableRange_t2011406615_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1449609243_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1449609243_0_0_0_Types[] = { &TailoringInfo_t1449609243_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1449609243_0_0_0 = { 1, GenInst_TailoringInfo_t1449609243_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3716250094_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716250094_0_0_0_Types };
extern const Il2CppType Link_t2723257478_0_0_0;
static const Il2CppType* GenInst_Link_t2723257478_0_0_0_Types[] = { &Link_t2723257478_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2723257478_0_0_0 = { 1, GenInst_Link_t2723257478_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0 = { 1, GenInst_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1744001932_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t1744001932_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1744001932_0_0_0_Types[] = { &KeyValuePair_2_t1744001932_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744001932_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744001932_0_0_0_Types };
extern const Il2CppType Contraction_t1673853792_0_0_0;
static const Il2CppType* GenInst_Contraction_t1673853792_0_0_0_Types[] = { &Contraction_t1673853792_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1673853792_0_0_0 = { 1, GenInst_Contraction_t1673853792_0_0_0_Types };
extern const Il2CppType Level2Map_t3322505726_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3322505726_0_0_0_Types[] = { &Level2Map_t3322505726_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3322505726_0_0_0 = { 1, GenInst_Level2Map_t3322505726_0_0_0_Types };
extern const Il2CppType BigInteger_t925946152_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946152_0_0_0_Types[] = { &BigInteger_t925946152_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946152_0_0_0 = { 1, GenInst_BigInteger_t925946152_0_0_0_Types };
extern const Il2CppType KeySizes_t3144736271_0_0_0;
static const Il2CppType* GenInst_KeySizes_t3144736271_0_0_0_Types[] = { &KeySizes_t3144736271_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t3144736271_0_0_0 = { 1, GenInst_KeySizes_t3144736271_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38854645_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0 = { 1, GenInst_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
extern const Il2CppType Slot_t2022531261_0_0_0;
static const Il2CppType* GenInst_Slot_t2022531261_0_0_0_Types[] = { &Slot_t2022531261_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2022531261_0_0_0 = { 1, GenInst_Slot_t2022531261_0_0_0_Types };
extern const Il2CppType Slot_t2267560602_0_0_0;
static const Il2CppType* GenInst_Slot_t2267560602_0_0_0_Types[] = { &Slot_t2267560602_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2267560602_0_0_0 = { 1, GenInst_Slot_t2267560602_0_0_0_Types };
extern const Il2CppType StackFrame_t2050294881_0_0_0;
static const Il2CppType* GenInst_StackFrame_t2050294881_0_0_0_Types[] = { &StackFrame_t2050294881_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t2050294881_0_0_0 = { 1, GenInst_StackFrame_t2050294881_0_0_0_Types };
extern const Il2CppType Calendar_t585061108_0_0_0;
static const Il2CppType* GenInst_Calendar_t585061108_0_0_0_Types[] = { &Calendar_t585061108_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t585061108_0_0_0 = { 1, GenInst_Calendar_t585061108_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t4156028127_0_0_0_Types[] = { &ModuleBuilder_t4156028127_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t4156028127_0_0_0 = { 1, GenInst_ModuleBuilder_t4156028127_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1075102050_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1075102050_0_0_0_Types[] = { &_ModuleBuilder_t1075102050_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1075102050_0_0_0 = { 1, GenInst__ModuleBuilder_t1075102050_0_0_0_Types };
extern const Il2CppType Module_t4282841206_0_0_0;
static const Il2CppType* GenInst_Module_t4282841206_0_0_0_Types[] = { &Module_t4282841206_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t4282841206_0_0_0 = { 1, GenInst_Module_t4282841206_0_0_0_Types };
extern const Il2CppType _Module_t2144668161_0_0_0;
static const Il2CppType* GenInst__Module_t2144668161_0_0_0_Types[] = { &_Module_t2144668161_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2144668161_0_0_0 = { 1, GenInst__Module_t2144668161_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3344728474_0_0_0_Types[] = { &ParameterBuilder_t3344728474_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3344728474_0_0_0 = { 1, GenInst_ParameterBuilder_t3344728474_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2251638747_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2251638747_0_0_0_Types[] = { &_ParameterBuilder_t2251638747_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2251638747_0_0_0 = { 1, GenInst__ParameterBuilder_t2251638747_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1664964607_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1664964607_0_0_0_Types[] = { &TypeU5BU5D_t1664964607_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1664964607_0_0_0 = { 1, GenInst_TypeU5BU5D_t1664964607_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t91669223_0_0_0;
static const Il2CppType* GenInst_ICollection_t91669223_0_0_0_Types[] = { &ICollection_t91669223_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t91669223_0_0_0 = { 1, GenInst_ICollection_t91669223_0_0_0_Types };
extern const Il2CppType IList_t3321498491_0_0_0;
static const Il2CppType* GenInst_IList_t3321498491_0_0_0_Types[] = { &IList_t3321498491_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0 = { 1, GenInst_IList_t3321498491_0_0_0_Types };
extern const Il2CppType IList_1_t1844743827_0_0_0;
static const Il2CppType* GenInst_IList_1_t1844743827_0_0_0_Types[] = { &IList_1_t1844743827_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1844743827_0_0_0 = { 1, GenInst_IList_1_t1844743827_0_0_0_Types };
extern const Il2CppType ICollection_1_t2255878531_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2255878531_0_0_0_Types[] = { &ICollection_1_t2255878531_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2255878531_0_0_0 = { 1, GenInst_ICollection_1_t2255878531_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1595930271_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1595930271_0_0_0_Types[] = { &IEnumerable_1_t1595930271_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1595930271_0_0_0 = { 1, GenInst_IEnumerable_1_t1595930271_0_0_0_Types };
extern const Il2CppType IList_1_t3952977575_0_0_0;
static const Il2CppType* GenInst_IList_1_t3952977575_0_0_0_Types[] = { &IList_1_t3952977575_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3952977575_0_0_0 = { 1, GenInst_IList_1_t3952977575_0_0_0_Types };
extern const Il2CppType ICollection_1_t69144983_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t69144983_0_0_0_Types[] = { &ICollection_1_t69144983_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t69144983_0_0_0 = { 1, GenInst_ICollection_1_t69144983_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3704164019_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3704164019_0_0_0_Types[] = { &IEnumerable_1_t3704164019_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3704164019_0_0_0 = { 1, GenInst_IEnumerable_1_t3704164019_0_0_0_Types };
extern const Il2CppType IList_1_t643717440_0_0_0;
static const Il2CppType* GenInst_IList_1_t643717440_0_0_0_Types[] = { &IList_1_t643717440_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t643717440_0_0_0 = { 1, GenInst_IList_1_t643717440_0_0_0_Types };
extern const Il2CppType ICollection_1_t1054852144_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1054852144_0_0_0_Types[] = { &ICollection_1_t1054852144_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1054852144_0_0_0 = { 1, GenInst_ICollection_1_t1054852144_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t394903884_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t394903884_0_0_0_Types[] = { &IEnumerable_1_t394903884_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t394903884_0_0_0 = { 1, GenInst_IEnumerable_1_t394903884_0_0_0_Types };
extern const Il2CppType IList_1_t289070565_0_0_0;
static const Il2CppType* GenInst_IList_1_t289070565_0_0_0_Types[] = { &IList_1_t289070565_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t289070565_0_0_0 = { 1, GenInst_IList_1_t289070565_0_0_0_Types };
extern const Il2CppType ICollection_1_t700205269_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t700205269_0_0_0_Types[] = { &ICollection_1_t700205269_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t700205269_0_0_0 = { 1, GenInst_ICollection_1_t700205269_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t40257009_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t40257009_0_0_0_Types[] = { &IEnumerable_1_t40257009_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t40257009_0_0_0 = { 1, GenInst_IEnumerable_1_t40257009_0_0_0_Types };
extern const Il2CppType IList_1_t1043143288_0_0_0;
static const Il2CppType* GenInst_IList_1_t1043143288_0_0_0_Types[] = { &IList_1_t1043143288_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1043143288_0_0_0 = { 1, GenInst_IList_1_t1043143288_0_0_0_Types };
extern const Il2CppType ICollection_1_t1454277992_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1454277992_0_0_0_Types[] = { &ICollection_1_t1454277992_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1454277992_0_0_0 = { 1, GenInst_ICollection_1_t1454277992_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t794329732_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t794329732_0_0_0_Types[] = { &IEnumerable_1_t794329732_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t794329732_0_0_0 = { 1, GenInst_IEnumerable_1_t794329732_0_0_0_Types };
extern const Il2CppType IList_1_t873662762_0_0_0;
static const Il2CppType* GenInst_IList_1_t873662762_0_0_0_Types[] = { &IList_1_t873662762_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t873662762_0_0_0 = { 1, GenInst_IList_1_t873662762_0_0_0_Types };
extern const Il2CppType ICollection_1_t1284797466_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1284797466_0_0_0_Types[] = { &ICollection_1_t1284797466_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1284797466_0_0_0 = { 1, GenInst_ICollection_1_t1284797466_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t624849206_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t624849206_0_0_0_Types[] = { &IEnumerable_1_t624849206_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t624849206_0_0_0 = { 1, GenInst_IEnumerable_1_t624849206_0_0_0_Types };
extern const Il2CppType IList_1_t3230389896_0_0_0;
static const Il2CppType* GenInst_IList_1_t3230389896_0_0_0_Types[] = { &IList_1_t3230389896_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3230389896_0_0_0 = { 1, GenInst_IList_1_t3230389896_0_0_0_Types };
extern const Il2CppType ICollection_1_t3641524600_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3641524600_0_0_0_Types[] = { &ICollection_1_t3641524600_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3641524600_0_0_0 = { 1, GenInst_ICollection_1_t3641524600_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2981576340_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2981576340_0_0_0_Types[] = { &IEnumerable_1_t2981576340_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2981576340_0_0_0 = { 1, GenInst_IEnumerable_1_t2981576340_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t149559338_0_0_0_Types[] = { &ILTokenInfo_t149559338_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t149559338_0_0_0 = { 1, GenInst_ILTokenInfo_t149559338_0_0_0_Types };
extern const Il2CppType LabelData_t3712112744_0_0_0;
static const Il2CppType* GenInst_LabelData_t3712112744_0_0_0_Types[] = { &LabelData_t3712112744_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3712112744_0_0_0 = { 1, GenInst_LabelData_t3712112744_0_0_0_Types };
extern const Il2CppType LabelFixup_t4090909514_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t4090909514_0_0_0_Types[] = { &LabelFixup_t4090909514_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t4090909514_0_0_0 = { 1, GenInst_LabelFixup_t4090909514_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1370236603_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1370236603_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types };
extern const Il2CppType TypeBuilder_t3308873219_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t3308873219_0_0_0_Types[] = { &TypeBuilder_t3308873219_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3308873219_0_0_0 = { 1, GenInst_TypeBuilder_t3308873219_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2783404358_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2783404358_0_0_0_Types[] = { &_TypeBuilder_t2783404358_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2783404358_0_0_0 = { 1, GenInst__TypeBuilder_t2783404358_0_0_0_Types };
extern const Il2CppType MethodBuilder_t644187984_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t644187984_0_0_0_Types[] = { &MethodBuilder_t644187984_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t644187984_0_0_0 = { 1, GenInst_MethodBuilder_t644187984_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3932949077_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3932949077_0_0_0_Types[] = { &_MethodBuilder_t3932949077_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3932949077_0_0_0 = { 1, GenInst__MethodBuilder_t3932949077_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t700974433_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t700974433_0_0_0_Types[] = { &ConstructorBuilder_t700974433_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t700974433_0_0_0 = { 1, GenInst_ConstructorBuilder_t700974433_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1236878896_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1236878896_0_0_0_Types[] = { &_ConstructorBuilder_t1236878896_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1236878896_0_0_0 = { 1, GenInst__ConstructorBuilder_t1236878896_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2784804005_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2784804005_0_0_0_Types[] = { &FieldBuilder_t2784804005_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2784804005_0_0_0 = { 1, GenInst_FieldBuilder_t2784804005_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1895266044_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1895266044_0_0_0_Types[] = { &_FieldBuilder_t1895266044_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1895266044_0_0_0 = { 1, GenInst__FieldBuilder_t1895266044_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1567586598_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1567586598_0_0_0_Types[] = { &_PropertyInfo_t1567586598_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1567586598_0_0_0 = { 1, GenInst__PropertyInfo_t1567586598_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t3093286891_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t3093286891_0_0_0_Types[] = { &CustomAttributeData_t3093286891_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3093286891_0_0_0 = { 1, GenInst_CustomAttributeData_t3093286891_0_0_0_Types };
extern const Il2CppType ResourceInfo_t3933049236_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t3933049236_0_0_0_Types[] = { &ResourceInfo_t3933049236_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3933049236_0_0_0 = { 1, GenInst_ResourceInfo_t3933049236_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t333236149_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t333236149_0_0_0_Types[] = { &ResourceCacheItem_t333236149_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t333236149_0_0_0 = { 1, GenInst_ResourceCacheItem_t333236149_0_0_0_Types };
extern const Il2CppType IContextProperty_t287246399_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t287246399_0_0_0_Types[] = { &IContextProperty_t287246399_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t287246399_0_0_0 = { 1, GenInst_IContextProperty_t287246399_0_0_0_Types };
extern const Il2CppType Header_t2756440555_0_0_0;
static const Il2CppType* GenInst_Header_t2756440555_0_0_0_Types[] = { &Header_t2756440555_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t2756440555_0_0_0 = { 1, GenInst_Header_t2756440555_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2759960940_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2759960940_0_0_0_Types[] = { &ITrackingHandler_t2759960940_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2759960940_0_0_0 = { 1, GenInst_ITrackingHandler_t2759960940_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2439121372_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2439121372_0_0_0_Types[] = { &IContextAttribute_t2439121372_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2439121372_0_0_0 = { 1, GenInst_IContextAttribute_t2439121372_0_0_0_Types };
extern const Il2CppType DateTime_t693205669_0_0_0;
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Types[] = { &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0 = { 1, GenInst_DateTime_t693205669_0_0_0_Types };
extern const Il2CppType TimeSpan_t3430258949_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t3430258949_0_0_0_Types[] = { &TimeSpan_t3430258949_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0 = { 1, GenInst_TimeSpan_t3430258949_0_0_0_Types };
extern const Il2CppType TypeTag_t141209596_0_0_0;
static const Il2CppType* GenInst_TypeTag_t141209596_0_0_0_Types[] = { &TypeTag_t141209596_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t141209596_0_0_0 = { 1, GenInst_TypeTag_t141209596_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType StrongName_t2988747270_0_0_0;
static const Il2CppType* GenInst_StrongName_t2988747270_0_0_0_Types[] = { &StrongName_t2988747270_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2988747270_0_0_0 = { 1, GenInst_StrongName_t2988747270_0_0_0_Types };
extern const Il2CppType IBuiltInEvidence_t1114073477_0_0_0;
static const Il2CppType* GenInst_IBuiltInEvidence_t1114073477_0_0_0_Types[] = { &IBuiltInEvidence_t1114073477_0_0_0 };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t1114073477_0_0_0 = { 1, GenInst_IBuiltInEvidence_t1114073477_0_0_0_Types };
extern const Il2CppType IIdentityPermissionFactory_t2988326850_0_0_0;
static const Il2CppType* GenInst_IIdentityPermissionFactory_t2988326850_0_0_0_Types[] = { &IIdentityPermissionFactory_t2988326850_0_0_0 };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t2988326850_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t2988326850_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1362988906_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1362988906_0_0_0_Types[] = { &DateTimeOffset_t1362988906_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1362988906_0_0_0 = { 1, GenInst_DateTimeOffset_t1362988906_0_0_0_Types };
extern const Il2CppType Guid_t_0_0_0;
static const Il2CppType* GenInst_Guid_t_0_0_0_Types[] = { &Guid_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
extern const Il2CppType Version_t1755874712_0_0_0;
static const Il2CppType* GenInst_Version_t1755874712_0_0_0_Types[] = { &Version_t1755874712_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1755874712_0_0_0 = { 1, GenInst_Version_t1755874712_0_0_0_Types };
extern const Il2CppType BigInteger_t925946153_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946153_0_0_0_Types[] = { &BigInteger_t925946153_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946153_0_0_0 = { 1, GenInst_BigInteger_t925946153_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t3397334013_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0 = { 1, GenInst_ByteU5BU5D_t3397334013_0_0_0_Types };
extern const Il2CppType IList_1_t4224045037_0_0_0;
static const Il2CppType* GenInst_IList_1_t4224045037_0_0_0_Types[] = { &IList_1_t4224045037_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t4224045037_0_0_0 = { 1, GenInst_IList_1_t4224045037_0_0_0_Types };
extern const Il2CppType ICollection_1_t340212445_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t340212445_0_0_0_Types[] = { &ICollection_1_t340212445_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t340212445_0_0_0 = { 1, GenInst_ICollection_1_t340212445_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3975231481_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3975231481_0_0_0_Types[] = { &IEnumerable_1_t3975231481_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3975231481_0_0_0 = { 1, GenInst_IEnumerable_1_t3975231481_0_0_0_Types };
extern const Il2CppType X509Certificate_t283079845_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t283079845_0_0_0_Types[] = { &X509Certificate_t283079845_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t283079845_0_0_0 = { 1, GenInst_X509Certificate_t283079845_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t327125377_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t327125377_0_0_0_Types[] = { &IDeserializationCallback_t327125377_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t327125377_0_0_0 = { 1, GenInst_IDeserializationCallback_t327125377_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t4001384466_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t4001384466_0_0_0_Types[] = { &ClientCertificateType_t4001384466_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4001384466_0_0_0 = { 1, GenInst_ClientCertificateType_t4001384466_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1174980068_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3497699202_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t3497699202_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Types[] = { &KeyValuePair_2_t3497699202_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497699202_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t4278378721_0_0_0_Types[] = { &X509ChainStatus_t4278378721_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t4278378721_0_0_0 = { 1, GenInst_X509ChainStatus_t4278378721_0_0_0_Types };
extern const Il2CppType Capture_t4157900610_0_0_0;
static const Il2CppType* GenInst_Capture_t4157900610_0_0_0_Types[] = { &Capture_t4157900610_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t4157900610_0_0_0 = { 1, GenInst_Capture_t4157900610_0_0_0_Types };
extern const Il2CppType Group_t3761430853_0_0_0;
static const Il2CppType* GenInst_Group_t3761430853_0_0_0_Types[] = { &Group_t3761430853_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3761430853_0_0_0 = { 1, GenInst_Group_t3761430853_0_0_0_Types };
extern const Il2CppType Mark_t2724874473_0_0_0;
static const Il2CppType* GenInst_Mark_t2724874473_0_0_0_Types[] = { &Mark_t2724874473_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t2724874473_0_0_0 = { 1, GenInst_Mark_t2724874473_0_0_0_Types };
extern const Il2CppType UriScheme_t1876590943_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1876590943_0_0_0_Types[] = { &UriScheme_t1876590943_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1876590943_0_0_0 = { 1, GenInst_UriScheme_t1876590943_0_0_0_Types };
extern const Il2CppType Link_t865133271_0_0_0;
static const Il2CppType* GenInst_Link_t865133271_0_0_0_Types[] = { &Link_t865133271_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t865133271_0_0_0 = { 1, GenInst_Link_t865133271_0_0_0_Types };
extern const Il2CppType Object_t1021602117_0_0_0;
static const Il2CppType* GenInst_Object_t1021602117_0_0_0_Types[] = { &Object_t1021602117_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0 = { 1, GenInst_Object_t1021602117_0_0_0_Types };
extern const Il2CppType Camera_t189460977_0_0_0;
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Types[] = { &Camera_t189460977_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0 = { 1, GenInst_Camera_t189460977_0_0_0_Types };
extern const Il2CppType Behaviour_t955675639_0_0_0;
static const Il2CppType* GenInst_Behaviour_t955675639_0_0_0_Types[] = { &Behaviour_t955675639_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t955675639_0_0_0 = { 1, GenInst_Behaviour_t955675639_0_0_0_Types };
extern const Il2CppType Component_t3819376471_0_0_0;
static const Il2CppType* GenInst_Component_t3819376471_0_0_0_Types[] = { &Component_t3819376471_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0 = { 1, GenInst_Component_t3819376471_0_0_0_Types };
extern const Il2CppType Display_t3666191348_0_0_0;
static const Il2CppType* GenInst_Display_t3666191348_0_0_0_Types[] = { &Display_t3666191348_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t3666191348_0_0_0 = { 1, GenInst_Display_t3666191348_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType AchievementDescription_t3110978151_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t3110978151_0_0_0_Types[] = { &AchievementDescription_t3110978151_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3110978151_0_0_0 = { 1, GenInst_AchievementDescription_t3110978151_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3498529102_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3498529102_0_0_0_Types[] = { &IAchievementDescription_t3498529102_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3498529102_0_0_0 = { 1, GenInst_IAchievementDescription_t3498529102_0_0_0_Types };
extern const Il2CppType UserProfile_t3365630962_0_0_0;
static const Il2CppType* GenInst_UserProfile_t3365630962_0_0_0_Types[] = { &UserProfile_t3365630962_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t3365630962_0_0_0 = { 1, GenInst_UserProfile_t3365630962_0_0_0_Types };
extern const Il2CppType IUserProfile_t4108565527_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t4108565527_0_0_0_Types[] = { &IUserProfile_t4108565527_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t4108565527_0_0_0 = { 1, GenInst_IUserProfile_t4108565527_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t453887929_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t453887929_0_0_0_Types[] = { &GcLeaderboard_t453887929_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t453887929_0_0_0 = { 1, GenInst_GcLeaderboard_t453887929_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t4083280315_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t4083280315_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t2709554645_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types[] = { &IAchievementU5BU5D_t2709554645_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2709554645_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types };
extern const Il2CppType IAchievement_t1752291260_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1752291260_0_0_0_Types[] = { &IAchievement_t1752291260_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1752291260_0_0_0 = { 1, GenInst_IAchievement_t1752291260_0_0_0_Types };
extern const Il2CppType GcAchievementData_t1754866149_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t1754866149_0_0_0_Types[] = { &GcAchievementData_t1754866149_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1754866149_0_0_0 = { 1, GenInst_GcAchievementData_t1754866149_0_0_0_Types };
extern const Il2CppType Achievement_t1333316625_0_0_0;
static const Il2CppType* GenInst_Achievement_t1333316625_0_0_0_Types[] = { &Achievement_t1333316625_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t1333316625_0_0_0 = { 1, GenInst_Achievement_t1333316625_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t3237304636_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types[] = { &IScoreU5BU5D_t3237304636_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3237304636_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types };
extern const Il2CppType IScore_t513966369_0_0_0;
static const Il2CppType* GenInst_IScore_t513966369_0_0_0_Types[] = { &IScore_t513966369_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t513966369_0_0_0 = { 1, GenInst_IScore_t513966369_0_0_0_Types };
extern const Il2CppType GcScoreData_t3676783238_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t3676783238_0_0_0_Types[] = { &GcScoreData_t3676783238_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t3676783238_0_0_0 = { 1, GenInst_GcScoreData_t3676783238_0_0_0_Types };
extern const Il2CppType Score_t2307748940_0_0_0;
static const Il2CppType* GenInst_Score_t2307748940_0_0_0_Types[] = { &Score_t2307748940_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t2307748940_0_0_0 = { 1, GenInst_Score_t2307748940_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t3461248430_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types[] = { &IUserProfileU5BU5D_t3461248430_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3461248430_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types };
extern const Il2CppType Material_t193706927_0_0_0;
static const Il2CppType* GenInst_Material_t193706927_0_0_0_Types[] = { &Material_t193706927_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t193706927_0_0_0 = { 1, GenInst_Material_t193706927_0_0_0_Types };
extern const Il2CppType Touch_t407273883_0_0_0;
static const Il2CppType* GenInst_Touch_t407273883_0_0_0_Types[] = { &Touch_t407273883_0_0_0 };
extern const Il2CppGenericInst GenInst_Touch_t407273883_0_0_0 = { 1, GenInst_Touch_t407273883_0_0_0_Types };
extern const Il2CppType Keyframe_t1449471340_0_0_0;
static const Il2CppType* GenInst_Keyframe_t1449471340_0_0_0_Types[] = { &Keyframe_t1449471340_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t1449471340_0_0_0 = { 1, GenInst_Keyframe_t1449471340_0_0_0_Types };
extern const Il2CppType Vector3_t2243707580_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0 = { 1, GenInst_Vector3_t2243707580_0_0_0_Types };
extern const Il2CppType Vector4_t2243707581_0_0_0;
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0 = { 1, GenInst_Vector4_t2243707581_0_0_0_Types };
extern const Il2CppType Vector2_t2243707579_0_0_0;
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0 = { 1, GenInst_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType Color32_t874517518_0_0_0;
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0 = { 1, GenInst_Color32_t874517518_0_0_0_Types };
extern const Il2CppType Color_t2020392075_0_0_0;
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Types[] = { &Color_t2020392075_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0 = { 1, GenInst_Color_t2020392075_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1701344717_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &KeyValuePair_2_t1701344717_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Types[] = { &KeyValuePair_2_t1701344717_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0 = { 1, GenInst_KeyValuePair_2_t1701344717_0_0_0_Types };
extern const Il2CppType Scene_t1684909666_0_0_0;
extern const Il2CppType LoadSceneMode_t2981886439_0_0_0;
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &LoadSceneMode_t2981886439_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0 = { 1, GenInst_Scene_t1684909666_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types };
extern const Il2CppType ContactPoint_t1376425630_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t1376425630_0_0_0_Types[] = { &ContactPoint_t1376425630_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t1376425630_0_0_0 = { 1, GenInst_ContactPoint_t1376425630_0_0_0_Types };
extern const Il2CppType RaycastHit_t87180320_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t87180320_0_0_0_Types[] = { &RaycastHit_t87180320_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0 = { 1, GenInst_RaycastHit_t87180320_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t502193897_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t502193897_0_0_0_Types[] = { &Rigidbody2D_t502193897_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t502193897_0_0_0 = { 1, GenInst_Rigidbody2D_t502193897_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t4063908774_0_0_0_Types[] = { &RaycastHit2D_t4063908774_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0 = { 1, GenInst_RaycastHit2D_t4063908774_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t3659330976_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t3659330976_0_0_0_Types[] = { &ContactPoint2D_t3659330976_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3659330976_0_0_0 = { 1, GenInst_ContactPoint2D_t3659330976_0_0_0_Types };
extern const Il2CppType AnimatorClipInfo_t3905751349_0_0_0;
static const Il2CppType* GenInst_AnimatorClipInfo_t3905751349_0_0_0_Types[] = { &AnimatorClipInfo_t3905751349_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3905751349_0_0_0 = { 1, GenInst_AnimatorClipInfo_t3905751349_0_0_0_Types };
extern const Il2CppType AnimatorControllerParameter_t1381019216_0_0_0;
static const Il2CppType* GenInst_AnimatorControllerParameter_t1381019216_0_0_0_Types[] = { &AnimatorControllerParameter_t1381019216_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorControllerParameter_t1381019216_0_0_0 = { 1, GenInst_AnimatorControllerParameter_t1381019216_0_0_0_Types };
extern const Il2CppType UIVertex_t1204258818_0_0_0;
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0 = { 1, GenInst_UIVertex_t1204258818_0_0_0_Types };
extern const Il2CppType UICharInfo_t3056636800_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0 = { 1, GenInst_UICharInfo_t3056636800_0_0_0_Types };
extern const Il2CppType UILineInfo_t3621277874_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0 = { 1, GenInst_UILineInfo_t3621277874_0_0_0_Types };
extern const Il2CppType Font_t4239498691_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_Types[] = { &Font_t4239498691_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0 = { 1, GenInst_Font_t4239498691_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t4183744904_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t4183744904_0_0_0_Types[] = { &GUILayoutOption_t4183744904_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t4183744904_0_0_0 = { 1, GenInst_GUILayoutOption_t4183744904_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t3828586629_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t3828586629_0_0_0_Types[] = { &GUILayoutEntry_t3828586629_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3828586629_0_0_0 = { 1, GenInst_GUILayoutEntry_t3828586629_0_0_0_Types };
extern const Il2CppType LayoutCache_t3120781045_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3749587448_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4180919198_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0, &KeyValuePair_2_t4180919198_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4180919198_0_0_0_Types[] = { &KeyValuePair_2_t4180919198_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180919198_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180919198_0_0_0_Types };
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t1799908754_0_0_0_Types[] = { &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t1799908754_0_0_0 = { 1, GenInst_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1472033238_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &KeyValuePair_2_t1472033238_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1472033238_0_0_0_Types[] = { &KeyValuePair_2_t1472033238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472033238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472033238_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
extern const Il2CppType Event_t3028476042_0_0_0;
extern const Il2CppType TextEditOp_t3138797698_0_0_0;
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t488203048_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0 = { 1, GenInst_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0 = { 1, GenInst_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_Types[] = { &Event_t3028476042_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0 = { 1, GenInst_Event_t3028476042_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3799506081_0_0_0;
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t3799506081_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0, &KeyValuePair_2_t3799506081_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t3799506081_0_0_0 = { 3, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t3799506081_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3799506081_0_0_0_Types[] = { &KeyValuePair_2_t3799506081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3799506081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3799506081_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2361573779_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2361573779_0_0_0_Types[] = { &KeyValuePair_2_t2361573779_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361573779_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361573779_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t2656950_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types[] = { &DisallowMultipleComponent_t2656950_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2656950_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types };
extern const Il2CppType Attribute_t542643598_0_0_0;
static const Il2CppType* GenInst_Attribute_t542643598_0_0_0_Types[] = { &Attribute_t542643598_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t542643598_0_0_0 = { 1, GenInst_Attribute_t542643598_0_0_0_Types };
extern const Il2CppType _Attribute_t1557664299_0_0_0;
static const Il2CppType* GenInst__Attribute_t1557664299_0_0_0_Types[] = { &_Attribute_t1557664299_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1557664299_0_0_0 = { 1, GenInst__Attribute_t1557664299_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3043633143_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types[] = { &ExecuteInEditMode_t3043633143_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3043633143_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types };
extern const Il2CppType RequireComponent_t864575032_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t864575032_0_0_0_Types[] = { &RequireComponent_t864575032_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t864575032_0_0_0 = { 1, GenInst_RequireComponent_t864575032_0_0_0_Types };
extern const Il2CppType HitInfo_t1761367055_0_0_0;
static const Il2CppType* GenInst_HitInfo_t1761367055_0_0_0_Types[] = { &HitInfo_t1761367055_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t1761367055_0_0_0 = { 1, GenInst_HitInfo_t1761367055_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType PersistentCall_t3793436469_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t3793436469_0_0_0_Types[] = { &PersistentCall_t3793436469_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t3793436469_0_0_0 = { 1, GenInst_PersistentCall_t3793436469_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t2229564840_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t2229564840_0_0_0_Types[] = { &BaseInvokableCall_t2229564840_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2229564840_0_0_0 = { 1, GenInst_BaseInvokableCall_t2229564840_0_0_0_Types };
extern const Il2CppType MessageTypeSubscribers_t2291506050_0_0_0;
static const Il2CppType* GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Types[] = { &MessageTypeSubscribers_t2291506050_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2291506050_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Types };
static const Il2CppType* GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &MessageTypeSubscribers_t2291506050_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType MessageEventArgs_t301283622_0_0_0;
static const Il2CppType* GenInst_MessageEventArgs_t301283622_0_0_0_Types[] = { &MessageEventArgs_t301283622_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t301283622_0_0_0 = { 1, GenInst_MessageEventArgs_t301283622_0_0_0_Types };
extern const Il2CppType TweenCallback_t3697142134_0_0_0;
static const Il2CppType* GenInst_TweenCallback_t3697142134_0_0_0_Types[] = { &TweenCallback_t3697142134_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenCallback_t3697142134_0_0_0 = { 1, GenInst_TweenCallback_t3697142134_0_0_0_Types };
extern const Il2CppType LogBehaviour_t3505725029_0_0_0;
static const Il2CppType* GenInst_LogBehaviour_t3505725029_0_0_0_Types[] = { &LogBehaviour_t3505725029_0_0_0 };
extern const Il2CppGenericInst GenInst_LogBehaviour_t3505725029_0_0_0 = { 1, GenInst_LogBehaviour_t3505725029_0_0_0_Types };
extern const Il2CppType FloatOptions_t1421548266_0_0_0;
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_FloatOptions_t1421548266_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0, &FloatOptions_t1421548266_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_FloatOptions_t1421548266_0_0_0 = { 3, GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_FloatOptions_t1421548266_0_0_0_Types };
extern const Il2CppType Quaternion_t4030073918_0_0_0;
extern const Il2CppType QuaternionOptions_t466049668_0_0_0;
static const Il2CppType* GenInst_Quaternion_t4030073918_0_0_0_Vector3_t2243707580_0_0_0_QuaternionOptions_t466049668_0_0_0_Types[] = { &Quaternion_t4030073918_0_0_0, &Vector3_t2243707580_0_0_0, &QuaternionOptions_t466049668_0_0_0 };
extern const Il2CppGenericInst GenInst_Quaternion_t4030073918_0_0_0_Vector3_t2243707580_0_0_0_QuaternionOptions_t466049668_0_0_0 = { 3, GenInst_Quaternion_t4030073918_0_0_0_Vector3_t2243707580_0_0_0_QuaternionOptions_t466049668_0_0_0_Types };
static const Il2CppType* GenInst_Quaternion_t4030073918_0_0_0_Types[] = { &Quaternion_t4030073918_0_0_0 };
extern const Il2CppGenericInst GenInst_Quaternion_t4030073918_0_0_0 = { 1, GenInst_Quaternion_t4030073918_0_0_0_Types };
extern const Il2CppType Vector3U5BU5D_t1172311765_0_0_0;
extern const Il2CppType Vector3ArrayOptions_t2672570171_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3U5BU5D_t1172311765_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3U5BU5D_t1172311765_0_0_0, &Vector3ArrayOptions_t2672570171_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3U5BU5D_t1172311765_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Vector3U5BU5D_t1172311765_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Il2CppObject_0_0_0, &Vector3ArrayOptions_t2672570171_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0_Types };
extern const Il2CppType Tween_t278478013_0_0_0;
static const Il2CppType* GenInst_Tween_t278478013_0_0_0_Types[] = { &Tween_t278478013_0_0_0 };
extern const Il2CppGenericInst GenInst_Tween_t278478013_0_0_0 = { 1, GenInst_Tween_t278478013_0_0_0_Types };
extern const Il2CppType ABSSequentiable_t2284140720_0_0_0;
static const Il2CppType* GenInst_ABSSequentiable_t2284140720_0_0_0_Types[] = { &ABSSequentiable_t2284140720_0_0_0 };
extern const Il2CppGenericInst GenInst_ABSSequentiable_t2284140720_0_0_0 = { 1, GenInst_ABSSequentiable_t2284140720_0_0_0_Types };
extern const Il2CppType StringOptions_t2885323933_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_StringOptions_t2885323933_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &StringOptions_t2885323933_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_StringOptions_t2885323933_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_StringOptions_t2885323933_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_StringOptions_t2885323933_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &StringOptions_t2885323933_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_StringOptions_t2885323933_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_StringOptions_t2885323933_0_0_0_Types };
extern const Il2CppType VectorOptions_t293385261_0_0_0;
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_VectorOptions_t293385261_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Vector2_t2243707579_0_0_0, &VectorOptions_t293385261_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_VectorOptions_t293385261_0_0_0 = { 3, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_VectorOptions_t293385261_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_VectorOptions_t293385261_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0, &VectorOptions_t293385261_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_VectorOptions_t293385261_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_VectorOptions_t293385261_0_0_0_Types };
extern const Il2CppType ColorOptions_t2213017305_0_0_0;
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_ColorOptions_t2213017305_0_0_0_Types[] = { &Color_t2020392075_0_0_0, &Color_t2020392075_0_0_0, &ColorOptions_t2213017305_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_ColorOptions_t2213017305_0_0_0 = { 3, GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_ColorOptions_t2213017305_0_0_0_Types };
extern const Il2CppType Rect_t3681755626_0_0_0;
extern const Il2CppType RectOptions_t3393635162_0_0_0;
static const Il2CppType* GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0_RectOptions_t3393635162_0_0_0_Types[] = { &Rect_t3681755626_0_0_0, &Rect_t3681755626_0_0_0, &RectOptions_t3393635162_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0_RectOptions_t3393635162_0_0_0 = { 3, GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0_RectOptions_t3393635162_0_0_0_Types };
static const Il2CppType* GenInst_Rect_t3681755626_0_0_0_Types[] = { &Rect_t3681755626_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t3681755626_0_0_0 = { 1, GenInst_Rect_t3681755626_0_0_0_Types };
extern const Il2CppType Path_t2828565993_0_0_0;
extern const Il2CppType PathOptions_t2659884781_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Path_t2828565993_0_0_0, &PathOptions_t2659884781_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Il2CppObject_0_0_0, &PathOptions_t2659884781_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0_Types };
extern const Il2CppType Color2_t232726623_0_0_0;
static const Il2CppType* GenInst_Color2_t232726623_0_0_0_Color2_t232726623_0_0_0_ColorOptions_t2213017305_0_0_0_Types[] = { &Color2_t232726623_0_0_0, &Color2_t232726623_0_0_0, &ColorOptions_t2213017305_0_0_0 };
extern const Il2CppGenericInst GenInst_Color2_t232726623_0_0_0_Color2_t232726623_0_0_0_ColorOptions_t2213017305_0_0_0 = { 3, GenInst_Color2_t232726623_0_0_0_Color2_t232726623_0_0_0_ColorOptions_t2213017305_0_0_0_Types };
static const Il2CppType* GenInst_Color2_t232726623_0_0_0_Types[] = { &Color2_t232726623_0_0_0 };
extern const Il2CppGenericInst GenInst_Color2_t232726623_0_0_0 = { 1, GenInst_Color2_t232726623_0_0_0_Types };
extern const Il2CppType NoOptions_t2508431845_0_0_0;
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &Double_t4078015681_0_0_0, &Double_t4078015681_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0_NoOptions_t2508431845_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Int64_t909078037_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_NoOptions_t2508431845_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &UInt64_t2909196914_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_NoOptions_t2508431845_0_0_0_Types };
extern const Il2CppType ITweenPlugin_t2991430675_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_Types[] = { &Type_t_0_0_0, &ITweenPlugin_t2991430675_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0 = { 2, GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ITweenPlugin_t2991430675_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2686133794_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_KeyValuePair_2_t2686133794_0_0_0_Types[] = { &Type_t_0_0_0, &ITweenPlugin_t2991430675_0_0_0, &KeyValuePair_2_t2686133794_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_KeyValuePair_2_t2686133794_0_0_0 = { 3, GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_KeyValuePair_2_t2686133794_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2686133794_0_0_0_Types[] = { &KeyValuePair_2_t2686133794_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2686133794_0_0_0 = { 1, GenInst_KeyValuePair_2_t2686133794_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_NoOptions_t2508431845_0_0_0_Types };
extern const Il2CppType RectOffset_t3387826427_0_0_0;
static const Il2CppType* GenInst_RectOffset_t3387826427_0_0_0_RectOffset_t3387826427_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &RectOffset_t3387826427_0_0_0, &RectOffset_t3387826427_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0_RectOffset_t3387826427_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_RectOffset_t3387826427_0_0_0_RectOffset_t3387826427_0_0_0_NoOptions_t2508431845_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_NoOptions_t2508431845_0_0_0_Types };
static const Il2CppType* GenInst_RectOffset_t3387826427_0_0_0_Types[] = { &RectOffset_t3387826427_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0 = { 1, GenInst_RectOffset_t3387826427_0_0_0_Types };
extern const Il2CppType UintOptions_t2267095136_0_0_0;
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_UInt32_t2149682021_0_0_0_UintOptions_t2267095136_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0, &UInt32_t2149682021_0_0_0, &UintOptions_t2267095136_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0_UInt32_t2149682021_0_0_0_UintOptions_t2267095136_0_0_0 = { 3, GenInst_UInt32_t2149682021_0_0_0_UInt32_t2149682021_0_0_0_UintOptions_t2267095136_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_VectorOptions_t293385261_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0, &Vector4_t2243707581_0_0_0, &VectorOptions_t293385261_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_VectorOptions_t293385261_0_0_0 = { 3, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_VectorOptions_t293385261_0_0_0_Types };
extern const Il2CppType ControlPoint_t168081159_0_0_0;
static const Il2CppType* GenInst_ControlPoint_t168081159_0_0_0_Types[] = { &ControlPoint_t168081159_0_0_0 };
extern const Il2CppGenericInst GenInst_ControlPoint_t168081159_0_0_0 = { 1, GenInst_ControlPoint_t168081159_0_0_0_Types };
extern const Il2CppType BaseInputModule_t1295781545_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t1295781545_0_0_0_Types[] = { &BaseInputModule_t1295781545_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t1295781545_0_0_0 = { 1, GenInst_BaseInputModule_t1295781545_0_0_0_Types };
extern const Il2CppType RaycastResult_t21186376_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0 = { 1, GenInst_RaycastResult_t21186376_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t3182198310_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t3182198310_0_0_0_Types[] = { &IDeselectHandler_t3182198310_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t3182198310_0_0_0 = { 1, GenInst_IDeselectHandler_t3182198310_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2741188318_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2741188318_0_0_0_Types[] = { &IEventSystemHandler_t2741188318_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2741188318_0_0_0 = { 1, GenInst_IEventSystemHandler_t2741188318_0_0_0_Types };
extern const Il2CppType List_1_t2110309450_0_0_0;
static const Il2CppType* GenInst_List_1_t2110309450_0_0_0_Types[] = { &List_1_t2110309450_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2110309450_0_0_0 = { 1, GenInst_List_1_t2110309450_0_0_0_Types };
extern const Il2CppType List_1_t2058570427_0_0_0;
static const Il2CppType* GenInst_List_1_t2058570427_0_0_0_Types[] = { &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0 = { 1, GenInst_List_1_t2058570427_0_0_0_Types };
extern const Il2CppType List_1_t3188497603_0_0_0;
static const Il2CppType* GenInst_List_1_t3188497603_0_0_0_Types[] = { &List_1_t3188497603_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3188497603_0_0_0 = { 1, GenInst_List_1_t3188497603_0_0_0_Types };
extern const Il2CppType ISelectHandler_t2812555161_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t2812555161_0_0_0_Types[] = { &ISelectHandler_t2812555161_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2812555161_0_0_0 = { 1, GenInst_ISelectHandler_t2812555161_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t2336171397_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t2336171397_0_0_0_Types[] = { &BaseRaycaster_t2336171397_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2336171397_0_0_0 = { 1, GenInst_BaseRaycaster_t2336171397_0_0_0_Types };
extern const Il2CppType Entry_t3365010046_0_0_0;
static const Il2CppType* GenInst_Entry_t3365010046_0_0_0_Types[] = { &Entry_t3365010046_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3365010046_0_0_0 = { 1, GenInst_Entry_t3365010046_0_0_0_Types };
extern const Il2CppType BaseEventData_t2681005625_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t2681005625_0_0_0_Types[] = { &BaseEventData_t2681005625_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t2681005625_0_0_0 = { 1, GenInst_BaseEventData_t2681005625_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t193164956_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t193164956_0_0_0_Types[] = { &IPointerEnterHandler_t193164956_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t193164956_0_0_0 = { 1, GenInst_IPointerEnterHandler_t193164956_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t461019860_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t461019860_0_0_0_Types[] = { &IPointerExitHandler_t461019860_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t461019860_0_0_0 = { 1, GenInst_IPointerExitHandler_t461019860_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t3929046918_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t3929046918_0_0_0_Types[] = { &IPointerDownHandler_t3929046918_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t3929046918_0_0_0 = { 1, GenInst_IPointerDownHandler_t3929046918_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t1847764461_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t1847764461_0_0_0_Types[] = { &IPointerUpHandler_t1847764461_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t1847764461_0_0_0 = { 1, GenInst_IPointerUpHandler_t1847764461_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t96169666_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t96169666_0_0_0_Types[] = { &IPointerClickHandler_t96169666_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t96169666_0_0_0 = { 1, GenInst_IPointerClickHandler_t96169666_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t3350809087_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types[] = { &IInitializePotentialDragHandler_t3350809087_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t3135127860_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t3135127860_0_0_0_Types[] = { &IBeginDragHandler_t3135127860_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3135127860_0_0_0 = { 1, GenInst_IBeginDragHandler_t3135127860_0_0_0_Types };
extern const Il2CppType IDragHandler_t2583993319_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t2583993319_0_0_0_Types[] = { &IDragHandler_t2583993319_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t2583993319_0_0_0 = { 1, GenInst_IDragHandler_t2583993319_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t1349123600_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t1349123600_0_0_0_Types[] = { &IEndDragHandler_t1349123600_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1349123600_0_0_0 = { 1, GenInst_IEndDragHandler_t1349123600_0_0_0_Types };
extern const Il2CppType IDropHandler_t2390101210_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t2390101210_0_0_0_Types[] = { &IDropHandler_t2390101210_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t2390101210_0_0_0 = { 1, GenInst_IDropHandler_t2390101210_0_0_0_Types };
extern const Il2CppType IScrollHandler_t3834677510_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t3834677510_0_0_0_Types[] = { &IScrollHandler_t3834677510_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3834677510_0_0_0 = { 1, GenInst_IScrollHandler_t3834677510_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t3778909353_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types[] = { &IUpdateSelectedHandler_t3778909353_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3778909353_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types };
extern const Il2CppType IMoveHandler_t2611925506_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t2611925506_0_0_0_Types[] = { &IMoveHandler_t2611925506_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2611925506_0_0_0 = { 1, GenInst_IMoveHandler_t2611925506_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t525803901_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t525803901_0_0_0_Types[] = { &ISubmitHandler_t525803901_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t525803901_0_0_0 = { 1, GenInst_ISubmitHandler_t525803901_0_0_0_Types };
extern const Il2CppType ICancelHandler_t1980319651_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t1980319651_0_0_0_Types[] = { &ICancelHandler_t1980319651_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1980319651_0_0_0 = { 1, GenInst_ICancelHandler_t1980319651_0_0_0_Types };
extern const Il2CppType Transform_t3275118058_0_0_0;
static const Il2CppType* GenInst_Transform_t3275118058_0_0_0_Types[] = { &Transform_t3275118058_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0 = { 1, GenInst_Transform_t3275118058_0_0_0_Types };
extern const Il2CppType GameObject_t1756533147_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0 = { 1, GenInst_GameObject_t1756533147_0_0_0_Types };
extern const Il2CppType BaseInput_t621514313_0_0_0;
static const Il2CppType* GenInst_BaseInput_t621514313_0_0_0_Types[] = { &BaseInput_t621514313_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInput_t621514313_0_0_0 = { 1, GenInst_BaseInput_t621514313_0_0_0_Types };
extern const Il2CppType UIBehaviour_t3960014691_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t3960014691_0_0_0_Types[] = { &UIBehaviour_t3960014691_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3960014691_0_0_0 = { 1, GenInst_UIBehaviour_t3960014691_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t1158329972_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t1158329972_0_0_0_Types[] = { &MonoBehaviour_t1158329972_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1158329972_0_0_0 = { 1, GenInst_MonoBehaviour_t1158329972_0_0_0_Types };
extern const Il2CppType PointerEventData_t1599784723_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2659922876_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &KeyValuePair_2_t2659922876_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2659922876_0_0_0_Types[] = { &KeyValuePair_2_t2659922876_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2659922876_0_0_0 = { 1, GenInst_KeyValuePair_2_t2659922876_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t1599784723_0_0_0_Types[] = { &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t1599784723_0_0_0 = { 1, GenInst_PointerEventData_t1599784723_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
extern const Il2CppType ButtonState_t2688375492_0_0_0;
static const Il2CppType* GenInst_ButtonState_t2688375492_0_0_0_Types[] = { &ButtonState_t2688375492_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t2688375492_0_0_0 = { 1, GenInst_ButtonState_t2688375492_0_0_0_Types };
extern const Il2CppType ICanvasElement_t986520779_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0 = { 1, GenInst_ICanvasElement_t986520779_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ColorBlock_t2652774230_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t2652774230_0_0_0_Types[] = { &ColorBlock_t2652774230_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t2652774230_0_0_0 = { 1, GenInst_ColorBlock_t2652774230_0_0_0_Types };
extern const Il2CppType OptionData_t2420267500_0_0_0;
static const Il2CppType* GenInst_OptionData_t2420267500_0_0_0_Types[] = { &OptionData_t2420267500_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t2420267500_0_0_0 = { 1, GenInst_OptionData_t2420267500_0_0_0_Types };
extern const Il2CppType DropdownItem_t4139978805_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t4139978805_0_0_0_Types[] = { &DropdownItem_t4139978805_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t4139978805_0_0_0 = { 1, GenInst_DropdownItem_t4139978805_0_0_0_Types };
extern const Il2CppType FloatTween_t2986189219_0_0_0;
static const Il2CppType* GenInst_FloatTween_t2986189219_0_0_0_Types[] = { &FloatTween_t2986189219_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t2986189219_0_0_0 = { 1, GenInst_FloatTween_t2986189219_0_0_0_Types };
extern const Il2CppType Sprite_t309593783_0_0_0;
static const Il2CppType* GenInst_Sprite_t309593783_0_0_0_Types[] = { &Sprite_t309593783_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t309593783_0_0_0 = { 1, GenInst_Sprite_t309593783_0_0_0_Types };
extern const Il2CppType Canvas_t209405766_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_Types[] = { &Canvas_t209405766_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0 = { 1, GenInst_Canvas_t209405766_0_0_0_Types };
extern const Il2CppType List_1_t3873494194_0_0_0;
static const Il2CppType* GenInst_List_1_t3873494194_0_0_0_Types[] = { &List_1_t3873494194_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3873494194_0_0_0 = { 1, GenInst_List_1_t3873494194_0_0_0_Types };
extern const Il2CppType HashSet_1_t2984649583_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0 = { 2, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types };
extern const Il2CppType Text_t356221433_0_0_0;
static const Il2CppType* GenInst_Text_t356221433_0_0_0_Types[] = { &Text_t356221433_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t356221433_0_0_0 = { 1, GenInst_Text_t356221433_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t850112849_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0, &KeyValuePair_2_t850112849_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t850112849_0_0_0_Types[] = { &KeyValuePair_2_t850112849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t850112849_0_0_0 = { 1, GenInst_KeyValuePair_2_t850112849_0_0_0_Types };
extern const Il2CppType ColorTween_t3438117476_0_0_0;
static const Il2CppType* GenInst_ColorTween_t3438117476_0_0_0_Types[] = { &ColorTween_t3438117476_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t3438117476_0_0_0 = { 1, GenInst_ColorTween_t3438117476_0_0_0_Types };
extern const Il2CppType Graphic_t2426225576_0_0_0;
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0 = { 1, GenInst_Graphic_t2426225576_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t286373651_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0 = { 2, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2391682566_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0, &KeyValuePair_2_t2391682566_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2391682566_0_0_0_Types[] = { &KeyValuePair_2_t2391682566_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2391682566_0_0_0 = { 1, GenInst_KeyValuePair_2_t2391682566_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3010968081_0_0_0;
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3010968081_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3010968081_0_0_0_Types[] = { &KeyValuePair_2_t3010968081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3010968081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3010968081_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1912381698_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t1912381698_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1912381698_0_0_0_Types[] = { &KeyValuePair_2_t1912381698_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912381698_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912381698_0_0_0_Types };
extern const Il2CppType Type_t3352948571_0_0_0;
static const Il2CppType* GenInst_Type_t3352948571_0_0_0_Types[] = { &Type_t3352948571_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t3352948571_0_0_0 = { 1, GenInst_Type_t3352948571_0_0_0_Types };
extern const Il2CppType FillMethod_t1640962579_0_0_0;
static const Il2CppType* GenInst_FillMethod_t1640962579_0_0_0_Types[] = { &FillMethod_t1640962579_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t1640962579_0_0_0 = { 1, GenInst_FillMethod_t1640962579_0_0_0_Types };
extern const Il2CppType ContentType_t1028629049_0_0_0;
static const Il2CppType* GenInst_ContentType_t1028629049_0_0_0_Types[] = { &ContentType_t1028629049_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t1028629049_0_0_0 = { 1, GenInst_ContentType_t1028629049_0_0_0_Types };
extern const Il2CppType LineType_t2931319356_0_0_0;
static const Il2CppType* GenInst_LineType_t2931319356_0_0_0_Types[] = { &LineType_t2931319356_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t2931319356_0_0_0 = { 1, GenInst_LineType_t2931319356_0_0_0_Types };
extern const Il2CppType InputType_t1274231802_0_0_0;
static const Il2CppType* GenInst_InputType_t1274231802_0_0_0_Types[] = { &InputType_t1274231802_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1274231802_0_0_0 = { 1, GenInst_InputType_t1274231802_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t875112366_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types[] = { &TouchScreenKeyboardType_t875112366_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t875112366_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types };
extern const Il2CppType CharacterValidation_t3437478890_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t3437478890_0_0_0_Types[] = { &CharacterValidation_t3437478890_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t3437478890_0_0_0 = { 1, GenInst_CharacterValidation_t3437478890_0_0_0_Types };
extern const Il2CppType Mask_t2977958238_0_0_0;
static const Il2CppType* GenInst_Mask_t2977958238_0_0_0_Types[] = { &Mask_t2977958238_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t2977958238_0_0_0 = { 1, GenInst_Mask_t2977958238_0_0_0_Types };
extern const Il2CppType List_1_t2347079370_0_0_0;
static const Il2CppType* GenInst_List_1_t2347079370_0_0_0_Types[] = { &List_1_t2347079370_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2347079370_0_0_0 = { 1, GenInst_List_1_t2347079370_0_0_0_Types };
extern const Il2CppType RectMask2D_t1156185964_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t1156185964_0_0_0_Types[] = { &RectMask2D_t1156185964_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t1156185964_0_0_0 = { 1, GenInst_RectMask2D_t1156185964_0_0_0_Types };
extern const Il2CppType List_1_t525307096_0_0_0;
static const Il2CppType* GenInst_List_1_t525307096_0_0_0_Types[] = { &List_1_t525307096_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t525307096_0_0_0 = { 1, GenInst_List_1_t525307096_0_0_0_Types };
extern const Il2CppType Navigation_t1571958496_0_0_0;
static const Il2CppType* GenInst_Navigation_t1571958496_0_0_0_Types[] = { &Navigation_t1571958496_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t1571958496_0_0_0 = { 1, GenInst_Navigation_t1571958496_0_0_0_Types };
extern const Il2CppType IClippable_t1941276057_0_0_0;
static const Il2CppType* GenInst_IClippable_t1941276057_0_0_0_Types[] = { &IClippable_t1941276057_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t1941276057_0_0_0 = { 1, GenInst_IClippable_t1941276057_0_0_0_Types };
extern const Il2CppType Direction_t3696775921_0_0_0;
static const Il2CppType* GenInst_Direction_t3696775921_0_0_0_Types[] = { &Direction_t3696775921_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t3696775921_0_0_0 = { 1, GenInst_Direction_t3696775921_0_0_0_Types };
extern const Il2CppType Selectable_t1490392188_0_0_0;
static const Il2CppType* GenInst_Selectable_t1490392188_0_0_0_Types[] = { &Selectable_t1490392188_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t1490392188_0_0_0 = { 1, GenInst_Selectable_t1490392188_0_0_0_Types };
extern const Il2CppType Transition_t605142169_0_0_0;
static const Il2CppType* GenInst_Transition_t605142169_0_0_0_Types[] = { &Transition_t605142169_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t605142169_0_0_0 = { 1, GenInst_Transition_t605142169_0_0_0_Types };
extern const Il2CppType SpriteState_t1353336012_0_0_0;
static const Il2CppType* GenInst_SpriteState_t1353336012_0_0_0_Types[] = { &SpriteState_t1353336012_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t1353336012_0_0_0 = { 1, GenInst_SpriteState_t1353336012_0_0_0_Types };
extern const Il2CppType CanvasGroup_t3296560743_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t3296560743_0_0_0_Types[] = { &CanvasGroup_t3296560743_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3296560743_0_0_0 = { 1, GenInst_CanvasGroup_t3296560743_0_0_0_Types };
extern const Il2CppType Direction_t1525323322_0_0_0;
static const Il2CppType* GenInst_Direction_t1525323322_0_0_0_Types[] = { &Direction_t1525323322_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t1525323322_0_0_0 = { 1, GenInst_Direction_t1525323322_0_0_0_Types };
extern const Il2CppType MatEntry_t3157325053_0_0_0;
static const Il2CppType* GenInst_MatEntry_t3157325053_0_0_0_Types[] = { &MatEntry_t3157325053_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t3157325053_0_0_0 = { 1, GenInst_MatEntry_t3157325053_0_0_0_Types };
extern const Il2CppType Toggle_t3976754468_0_0_0;
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0 = { 1, GenInst_Toggle_t3976754468_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IClipper_t900477982_0_0_0;
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Types[] = { &IClipper_t900477982_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0 = { 1, GenInst_IClipper_t900477982_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t379984643_0_0_0;
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t379984643_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t379984643_0_0_0_Types[] = { &KeyValuePair_2_t379984643_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t379984643_0_0_0 = { 1, GenInst_KeyValuePair_2_t379984643_0_0_0_Types };
extern const Il2CppType AspectMode_t1166448724_0_0_0;
static const Il2CppType* GenInst_AspectMode_t1166448724_0_0_0_Types[] = { &AspectMode_t1166448724_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t1166448724_0_0_0 = { 1, GenInst_AspectMode_t1166448724_0_0_0_Types };
extern const Il2CppType FitMode_t4030874534_0_0_0;
static const Il2CppType* GenInst_FitMode_t4030874534_0_0_0_Types[] = { &FitMode_t4030874534_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t4030874534_0_0_0 = { 1, GenInst_FitMode_t4030874534_0_0_0_Types };
extern const Il2CppType RectTransform_t3349966182_0_0_0;
static const Il2CppType* GenInst_RectTransform_t3349966182_0_0_0_Types[] = { &RectTransform_t3349966182_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t3349966182_0_0_0 = { 1, GenInst_RectTransform_t3349966182_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t2155218138_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t2155218138_0_0_0_Types[] = { &LayoutRebuilder_t2155218138_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2155218138_0_0_0 = { 1, GenInst_LayoutRebuilder_t2155218138_0_0_0_Types };
extern const Il2CppType ILayoutElement_t1975293769_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType List_1_t1612828712_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828712_0_0_0_Types[] = { &List_1_t1612828712_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828712_0_0_0 = { 1, GenInst_List_1_t1612828712_0_0_0_Types };
extern const Il2CppType List_1_t243638650_0_0_0;
static const Il2CppType* GenInst_List_1_t243638650_0_0_0_Types[] = { &List_1_t243638650_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t243638650_0_0_0 = { 1, GenInst_List_1_t243638650_0_0_0_Types };
extern const Il2CppType List_1_t1612828711_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828711_0_0_0_Types[] = { &List_1_t1612828711_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828711_0_0_0 = { 1, GenInst_List_1_t1612828711_0_0_0_Types };
extern const Il2CppType List_1_t1612828713_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828713_0_0_0_Types[] = { &List_1_t1612828713_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828713_0_0_0 = { 1, GenInst_List_1_t1612828713_0_0_0_Types };
extern const Il2CppType List_1_t1440998580_0_0_0;
static const Il2CppType* GenInst_List_1_t1440998580_0_0_0_Types[] = { &List_1_t1440998580_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1440998580_0_0_0 = { 1, GenInst_List_1_t1440998580_0_0_0_Types };
extern const Il2CppType List_1_t573379950_0_0_0;
static const Il2CppType* GenInst_List_1_t573379950_0_0_0_Types[] = { &List_1_t573379950_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t573379950_0_0_0 = { 1, GenInst_List_1_t573379950_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2361573779_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0_Types };
extern const Il2CppType FieldWithTarget_t2256174789_0_0_0;
static const Il2CppType* GenInst_FieldWithTarget_t2256174789_0_0_0_Types[] = { &FieldWithTarget_t2256174789_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldWithTarget_t2256174789_0_0_0 = { 1, GenInst_FieldWithTarget_t2256174789_0_0_0_Types };
extern const Il2CppType Hashtable_t909839986_0_0_0;
static const Il2CppType* GenInst_Hashtable_t909839986_0_0_0_Types[] = { &Hashtable_t909839986_0_0_0 };
extern const Il2CppGenericInst GenInst_Hashtable_t909839986_0_0_0 = { 1, GenInst_Hashtable_t909839986_0_0_0_Types };
extern const Il2CppType GameOneManager_t2648179911_0_0_0;
static const Il2CppType* GenInst_GameOneManager_t2648179911_0_0_0_Types[] = { &GameOneManager_t2648179911_0_0_0 };
extern const Il2CppGenericInst GenInst_GameOneManager_t2648179911_0_0_0 = { 1, GenInst_GameOneManager_t2648179911_0_0_0_Types };
extern const Il2CppType SoundManager_t654432262_0_0_0;
static const Il2CppType* GenInst_SoundManager_t654432262_0_0_0_Types[] = { &SoundManager_t654432262_0_0_0 };
extern const Il2CppGenericInst GenInst_SoundManager_t654432262_0_0_0 = { 1, GenInst_SoundManager_t654432262_0_0_0_Types };
extern const Il2CppType NavigationManager_t4055369543_0_0_0;
static const Il2CppType* GenInst_NavigationManager_t4055369543_0_0_0_Types[] = { &NavigationManager_t4055369543_0_0_0 };
extern const Il2CppGenericInst GenInst_NavigationManager_t4055369543_0_0_0 = { 1, GenInst_NavigationManager_t4055369543_0_0_0_Types };
extern const Il2CppType GameTenManager_t1577134516_0_0_0;
static const Il2CppType* GenInst_GameTenManager_t1577134516_0_0_0_Types[] = { &GameTenManager_t1577134516_0_0_0 };
extern const Il2CppGenericInst GenInst_GameTenManager_t1577134516_0_0_0 = { 1, GenInst_GameTenManager_t1577134516_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t1209076198_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t1209076198_0_0_0_Types[] = { &SpriteRenderer_t1209076198_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t1209076198_0_0_0 = { 1, GenInst_SpriteRenderer_t1209076198_0_0_0_Types };
extern const Il2CppType Renderer_t257310565_0_0_0;
static const Il2CppType* GenInst_Renderer_t257310565_0_0_0_Types[] = { &Renderer_t257310565_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t257310565_0_0_0 = { 1, GenInst_Renderer_t257310565_0_0_0_Types };
extern const Il2CppType GameElevenManager_t2036900502_0_0_0;
static const Il2CppType* GenInst_GameElevenManager_t2036900502_0_0_0_Types[] = { &GameElevenManager_t2036900502_0_0_0 };
extern const Il2CppGenericInst GenInst_GameElevenManager_t2036900502_0_0_0 = { 1, GenInst_GameElevenManager_t2036900502_0_0_0_Types };
extern const Il2CppType GameTwelveManager_t2786305482_0_0_0;
static const Il2CppType* GenInst_GameTwelveManager_t2786305482_0_0_0_Types[] = { &GameTwelveManager_t2786305482_0_0_0 };
extern const Il2CppGenericInst GenInst_GameTwelveManager_t2786305482_0_0_0 = { 1, GenInst_GameTwelveManager_t2786305482_0_0_0_Types };
extern const Il2CppType GameThirteenManager_t3231626088_0_0_0;
static const Il2CppType* GenInst_GameThirteenManager_t3231626088_0_0_0_Types[] = { &GameThirteenManager_t3231626088_0_0_0 };
extern const Il2CppGenericInst GenInst_GameThirteenManager_t3231626088_0_0_0 = { 1, GenInst_GameThirteenManager_t3231626088_0_0_0_Types };
extern const Il2CppType SnowManMatch_t3305190334_0_0_0;
static const Il2CppType* GenInst_SnowManMatch_t3305190334_0_0_0_Types[] = { &SnowManMatch_t3305190334_0_0_0 };
extern const Il2CppGenericInst GenInst_SnowManMatch_t3305190334_0_0_0 = { 1, GenInst_SnowManMatch_t3305190334_0_0_0_Types };
extern const Il2CppType MemoryGame_t4018179393_0_0_0;
static const Il2CppType* GenInst_MemoryGame_t4018179393_0_0_0_Types[] = { &MemoryGame_t4018179393_0_0_0 };
extern const Il2CppGenericInst GenInst_MemoryGame_t4018179393_0_0_0 = { 1, GenInst_MemoryGame_t4018179393_0_0_0_Types };
extern const Il2CppType MemoryGamePiece_t2964149615_0_0_0;
static const Il2CppType* GenInst_MemoryGamePiece_t2964149615_0_0_0_Types[] = { &MemoryGamePiece_t2964149615_0_0_0 };
extern const Il2CppGenericInst GenInst_MemoryGamePiece_t2964149615_0_0_0 = { 1, GenInst_MemoryGamePiece_t2964149615_0_0_0_Types };
extern const Il2CppType GameFifteenManager_t1527188406_0_0_0;
static const Il2CppType* GenInst_GameFifteenManager_t1527188406_0_0_0_Types[] = { &GameFifteenManager_t1527188406_0_0_0 };
extern const Il2CppGenericInst GenInst_GameFifteenManager_t1527188406_0_0_0 = { 1, GenInst_GameFifteenManager_t1527188406_0_0_0_Types };
extern const Il2CppType LightColors_t1169232587_0_0_0;
static const Il2CppType* GenInst_LightColors_t1169232587_0_0_0_Types[] = { &LightColors_t1169232587_0_0_0 };
extern const Il2CppGenericInst GenInst_LightColors_t1169232587_0_0_0 = { 1, GenInst_LightColors_t1169232587_0_0_0_Types };
extern const Il2CppType LightPuzzle_t891176437_0_0_0;
static const Il2CppType* GenInst_LightPuzzle_t891176437_0_0_0_Types[] = { &LightPuzzle_t891176437_0_0_0 };
extern const Il2CppGenericInst GenInst_LightPuzzle_t891176437_0_0_0 = { 1, GenInst_LightPuzzle_t891176437_0_0_0_Types };
extern const Il2CppType LightCompleted_t2236120434_0_0_0;
static const Il2CppType* GenInst_LightCompleted_t2236120434_0_0_0_Types[] = { &LightCompleted_t2236120434_0_0_0 };
extern const Il2CppGenericInst GenInst_LightCompleted_t2236120434_0_0_0 = { 1, GenInst_LightCompleted_t2236120434_0_0_0_Types };
extern const Il2CppType LightBulb_t3998556073_0_0_0;
static const Il2CppType* GenInst_LightBulb_t3998556073_0_0_0_Types[] = { &LightBulb_t3998556073_0_0_0 };
extern const Il2CppGenericInst GenInst_LightBulb_t3998556073_0_0_0 = { 1, GenInst_LightBulb_t3998556073_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType GameSixteenManager_t358793919_0_0_0;
static const Il2CppType* GenInst_GameSixteenManager_t358793919_0_0_0_Types[] = { &GameSixteenManager_t358793919_0_0_0 };
extern const Il2CppGenericInst GenInst_GameSixteenManager_t358793919_0_0_0 = { 1, GenInst_GameSixteenManager_t358793919_0_0_0_Types };
extern const Il2CppType Game17Manager_t3515664785_0_0_0;
static const Il2CppType* GenInst_Game17Manager_t3515664785_0_0_0_Types[] = { &Game17Manager_t3515664785_0_0_0 };
extern const Il2CppGenericInst GenInst_Game17Manager_t3515664785_0_0_0 = { 1, GenInst_Game17Manager_t3515664785_0_0_0_Types };
extern const Il2CppType Difference_t980400781_0_0_0;
static const Il2CppType* GenInst_Difference_t980400781_0_0_0_Types[] = { &Difference_t980400781_0_0_0 };
extern const Il2CppGenericInst GenInst_Difference_t980400781_0_0_0 = { 1, GenInst_Difference_t980400781_0_0_0_Types };
extern const Il2CppType DifferenceCollection_t1428269411_0_0_0;
static const Il2CppType* GenInst_DifferenceCollection_t1428269411_0_0_0_Types[] = { &DifferenceCollection_t1428269411_0_0_0 };
extern const Il2CppGenericInst GenInst_DifferenceCollection_t1428269411_0_0_0 = { 1, GenInst_DifferenceCollection_t1428269411_0_0_0_Types };
extern const Il2CppType GameEighteenManager_t3662215866_0_0_0;
static const Il2CppType* GenInst_GameEighteenManager_t3662215866_0_0_0_Types[] = { &GameEighteenManager_t3662215866_0_0_0 };
extern const Il2CppGenericInst GenInst_GameEighteenManager_t3662215866_0_0_0 = { 1, GenInst_GameEighteenManager_t3662215866_0_0_0_Types };
extern const Il2CppType GameRounds_t1482069856_0_0_0;
static const Il2CppType* GenInst_GameRounds_t1482069856_0_0_0_Types[] = { &GameRounds_t1482069856_0_0_0 };
extern const Il2CppGenericInst GenInst_GameRounds_t1482069856_0_0_0 = { 1, GenInst_GameRounds_t1482069856_0_0_0_Types };
extern const Il2CppType SoundClip_t2598932235_0_0_0;
static const Il2CppType* GenInst_SoundClip_t2598932235_0_0_0_Types[] = { &SoundClip_t2598932235_0_0_0 };
extern const Il2CppGenericInst GenInst_SoundClip_t2598932235_0_0_0 = { 1, GenInst_SoundClip_t2598932235_0_0_0_Types };
extern const Il2CppType GameNineteenManager_t942308577_0_0_0;
static const Il2CppType* GenInst_GameNineteenManager_t942308577_0_0_0_Types[] = { &GameNineteenManager_t942308577_0_0_0 };
extern const Il2CppGenericInst GenInst_GameNineteenManager_t942308577_0_0_0 = { 1, GenInst_GameNineteenManager_t942308577_0_0_0_Types };
extern const Il2CppType Song_t1848967459_0_0_0;
static const Il2CppType* GenInst_Song_t1848967459_0_0_0_Types[] = { &Song_t1848967459_0_0_0 };
extern const Il2CppGenericInst GenInst_Song_t1848967459_0_0_0 = { 1, GenInst_Song_t1848967459_0_0_0_Types };
extern const Il2CppType GameTwoManager_t1572221819_0_0_0;
static const Il2CppType* GenInst_GameTwoManager_t1572221819_0_0_0_Types[] = { &GameTwoManager_t1572221819_0_0_0 };
extern const Il2CppGenericInst GenInst_GameTwoManager_t1572221819_0_0_0 = { 1, GenInst_GameTwoManager_t1572221819_0_0_0_Types };
extern const Il2CppType TransformGrid_t2312775536_0_0_0;
static const Il2CppType* GenInst_TransformGrid_t2312775536_0_0_0_Types[] = { &TransformGrid_t2312775536_0_0_0 };
extern const Il2CppGenericInst GenInst_TransformGrid_t2312775536_0_0_0 = { 1, GenInst_TransformGrid_t2312775536_0_0_0_Types };
extern const Il2CppType GameTwentyManager_t1246478874_0_0_0;
static const Il2CppType* GenInst_GameTwentyManager_t1246478874_0_0_0_Types[] = { &GameTwentyManager_t1246478874_0_0_0 };
extern const Il2CppGenericInst GenInst_GameTwentyManager_t1246478874_0_0_0 = { 1, GenInst_GameTwentyManager_t1246478874_0_0_0_Types };
extern const Il2CppType GameTwentyoneManager_t426707788_0_0_0;
static const Il2CppType* GenInst_GameTwentyoneManager_t426707788_0_0_0_Types[] = { &GameTwentyoneManager_t426707788_0_0_0 };
extern const Il2CppGenericInst GenInst_GameTwentyoneManager_t426707788_0_0_0 = { 1, GenInst_GameTwentyoneManager_t426707788_0_0_0_Types };
extern const Il2CppType GiftObject_t2144675780_0_0_0;
static const Il2CppType* GenInst_GiftObject_t2144675780_0_0_0_Types[] = { &GiftObject_t2144675780_0_0_0 };
extern const Il2CppGenericInst GenInst_GiftObject_t2144675780_0_0_0 = { 1, GenInst_GiftObject_t2144675780_0_0_0_Types };
extern const Il2CppType GameTwentytwoManager_t3731809278_0_0_0;
static const Il2CppType* GenInst_GameTwentytwoManager_t3731809278_0_0_0_Types[] = { &GameTwentytwoManager_t3731809278_0_0_0 };
extern const Il2CppGenericInst GenInst_GameTwentytwoManager_t3731809278_0_0_0 = { 1, GenInst_GameTwentytwoManager_t3731809278_0_0_0_Types };
extern const Il2CppType DraggableOrnament_t3124044711_0_0_0;
static const Il2CppType* GenInst_DraggableOrnament_t3124044711_0_0_0_Types[] = { &DraggableOrnament_t3124044711_0_0_0 };
extern const Il2CppGenericInst GenInst_DraggableOrnament_t3124044711_0_0_0 = { 1, GenInst_DraggableOrnament_t3124044711_0_0_0_Types };
extern const Il2CppType ClickObject_t2003887325_0_0_0;
static const Il2CppType* GenInst_ClickObject_t2003887325_0_0_0_Types[] = { &ClickObject_t2003887325_0_0_0 };
extern const Il2CppGenericInst GenInst_ClickObject_t2003887325_0_0_0 = { 1, GenInst_ClickObject_t2003887325_0_0_0_Types };
extern const Il2CppType GiftSpawner_t1625374518_0_0_0;
static const Il2CppType* GenInst_GiftSpawner_t1625374518_0_0_0_Types[] = { &GiftSpawner_t1625374518_0_0_0 };
extern const Il2CppGenericInst GenInst_GiftSpawner_t1625374518_0_0_0 = { 1, GenInst_GiftSpawner_t1625374518_0_0_0_Types };
extern const Il2CppType Game24Manager_t283638021_0_0_0;
static const Il2CppType* GenInst_Game24Manager_t283638021_0_0_0_Types[] = { &Game24Manager_t283638021_0_0_0 };
extern const Il2CppGenericInst GenInst_Game24Manager_t283638021_0_0_0 = { 1, GenInst_Game24Manager_t283638021_0_0_0_Types };
extern const Il2CppType Gift_t450896550_0_0_0;
static const Il2CppType* GenInst_Gift_t450896550_0_0_0_Types[] = { &Gift_t450896550_0_0_0 };
extern const Il2CppGenericInst GenInst_Gift_t450896550_0_0_0 = { 1, GenInst_Gift_t450896550_0_0_0_Types };
extern const Il2CppType ObstacleBase_t365467716_0_0_0;
static const Il2CppType* GenInst_ObstacleBase_t365467716_0_0_0_Types[] = { &ObstacleBase_t365467716_0_0_0 };
extern const Il2CppGenericInst GenInst_ObstacleBase_t365467716_0_0_0 = { 1, GenInst_ObstacleBase_t365467716_0_0_0_Types };
extern const Il2CppType GameThreeManager_t1469036085_0_0_0;
static const Il2CppType* GenInst_GameThreeManager_t1469036085_0_0_0_Types[] = { &GameThreeManager_t1469036085_0_0_0 };
extern const Il2CppGenericInst GenInst_GameThreeManager_t1469036085_0_0_0 = { 1, GenInst_GameThreeManager_t1469036085_0_0_0_Types };
extern const Il2CppType HatEnabler_t1910487184_0_0_0;
static const Il2CppType* GenInst_HatEnabler_t1910487184_0_0_0_Types[] = { &HatEnabler_t1910487184_0_0_0 };
extern const Il2CppGenericInst GenInst_HatEnabler_t1910487184_0_0_0 = { 1, GenInst_HatEnabler_t1910487184_0_0_0_Types };
extern const Il2CppType PlatformEffector2D_t3299381139_0_0_0;
static const Il2CppType* GenInst_PlatformEffector2D_t3299381139_0_0_0_Types[] = { &PlatformEffector2D_t3299381139_0_0_0 };
extern const Il2CppGenericInst GenInst_PlatformEffector2D_t3299381139_0_0_0 = { 1, GenInst_PlatformEffector2D_t3299381139_0_0_0_Types };
extern const Il2CppType Effector2D_t2551982566_0_0_0;
static const Il2CppType* GenInst_Effector2D_t2551982566_0_0_0_Types[] = { &Effector2D_t2551982566_0_0_0 };
extern const Il2CppGenericInst GenInst_Effector2D_t2551982566_0_0_0 = { 1, GenInst_Effector2D_t2551982566_0_0_0_Types };
extern const Il2CppType GameFourManager_t2429316785_0_0_0;
static const Il2CppType* GenInst_GameFourManager_t2429316785_0_0_0_Types[] = { &GameFourManager_t2429316785_0_0_0 };
extern const Il2CppGenericInst GenInst_GameFourManager_t2429316785_0_0_0 = { 1, GenInst_GameFourManager_t2429316785_0_0_0_Types };
extern const Il2CppType GameFiveManager_t3955517211_0_0_0;
static const Il2CppType* GenInst_GameFiveManager_t3955517211_0_0_0_Types[] = { &GameFiveManager_t3955517211_0_0_0 };
extern const Il2CppGenericInst GenInst_GameFiveManager_t3955517211_0_0_0 = { 1, GenInst_GameFiveManager_t3955517211_0_0_0_Types };
extern const Il2CppType GameSixManager_t2271637217_0_0_0;
static const Il2CppType* GenInst_GameSixManager_t2271637217_0_0_0_Types[] = { &GameSixManager_t2271637217_0_0_0 };
extern const Il2CppGenericInst GenInst_GameSixManager_t2271637217_0_0_0 = { 1, GenInst_GameSixManager_t2271637217_0_0_0_Types };
extern const Il2CppType GameSevenManager_t3596770590_0_0_0;
static const Il2CppType* GenInst_GameSevenManager_t3596770590_0_0_0_Types[] = { &GameSevenManager_t3596770590_0_0_0 };
extern const Il2CppGenericInst GenInst_GameSevenManager_t3596770590_0_0_0 = { 1, GenInst_GameSevenManager_t3596770590_0_0_0_Types };
extern const Il2CppType SnowMan_t902289717_0_0_0;
static const Il2CppType* GenInst_SnowMan_t902289717_0_0_0_Types[] = { &SnowMan_t902289717_0_0_0 };
extern const Il2CppGenericInst GenInst_SnowMan_t902289717_0_0_0 = { 1, GenInst_SnowMan_t902289717_0_0_0_Types };
extern const Il2CppType GameEightManager_t903757396_0_0_0;
static const Il2CppType* GenInst_GameEightManager_t903757396_0_0_0_Types[] = { &GameEightManager_t903757396_0_0_0 };
extern const Il2CppGenericInst GenInst_GameEightManager_t903757396_0_0_0 = { 1, GenInst_GameEightManager_t903757396_0_0_0_Types };
extern const Il2CppType BackgroundsHolder_t4153806707_0_0_0;
static const Il2CppType* GenInst_BackgroundsHolder_t4153806707_0_0_0_Types[] = { &BackgroundsHolder_t4153806707_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundsHolder_t4153806707_0_0_0 = { 1, GenInst_BackgroundsHolder_t4153806707_0_0_0_Types };
extern const Il2CppType StartingSlot_t3631029828_0_0_0;
static const Il2CppType* GenInst_StartingSlot_t3631029828_0_0_0_Types[] = { &StartingSlot_t3631029828_0_0_0 };
extern const Il2CppGenericInst GenInst_StartingSlot_t3631029828_0_0_0 = { 1, GenInst_StartingSlot_t3631029828_0_0_0_Types };
extern const Il2CppType GameNineManager_t3632946731_0_0_0;
static const Il2CppType* GenInst_GameNineManager_t3632946731_0_0_0_Types[] = { &GameNineManager_t3632946731_0_0_0 };
extern const Il2CppGenericInst GenInst_GameNineManager_t3632946731_0_0_0 = { 1, GenInst_GameNineManager_t3632946731_0_0_0_Types };
extern const Il2CppType AudioClip_t1932558630_0_0_0;
static const Il2CppType* GenInst_AudioClip_t1932558630_0_0_0_Types[] = { &AudioClip_t1932558630_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioClip_t1932558630_0_0_0 = { 1, GenInst_AudioClip_t1932558630_0_0_0_Types };
extern const Il2CppType DOTweenAnimation_t858634588_0_0_0;
static const Il2CppType* GenInst_DOTweenAnimation_t858634588_0_0_0_Types[] = { &DOTweenAnimation_t858634588_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTweenAnimation_t858634588_0_0_0 = { 1, GenInst_DOTweenAnimation_t858634588_0_0_0_Types };
extern const Il2CppType ABSAnimationComponent_t2205594551_0_0_0;
static const Il2CppType* GenInst_ABSAnimationComponent_t2205594551_0_0_0_Types[] = { &ABSAnimationComponent_t2205594551_0_0_0 };
extern const Il2CppGenericInst GenInst_ABSAnimationComponent_t2205594551_0_0_0 = { 1, GenInst_ABSAnimationComponent_t2205594551_0_0_0_Types };
extern const Il2CppType RocketChar_t1772935162_0_0_0;
extern const Il2CppType Collider2D_t646061738_0_0_0;
static const Il2CppType* GenInst_RocketChar_t1772935162_0_0_0_Collider2D_t646061738_0_0_0_Types[] = { &RocketChar_t1772935162_0_0_0, &Collider2D_t646061738_0_0_0 };
extern const Il2CppGenericInst GenInst_RocketChar_t1772935162_0_0_0_Collider2D_t646061738_0_0_0 = { 2, GenInst_RocketChar_t1772935162_0_0_0_Collider2D_t646061738_0_0_0_Types };
extern const Il2CppType Collision2D_t1539500754_0_0_0;
static const Il2CppType* GenInst_RocketChar_t1772935162_0_0_0_Collider2D_t646061738_0_0_0_Collision2D_t1539500754_0_0_0_Types[] = { &RocketChar_t1772935162_0_0_0, &Collider2D_t646061738_0_0_0, &Collision2D_t1539500754_0_0_0 };
extern const Il2CppGenericInst GenInst_RocketChar_t1772935162_0_0_0_Collider2D_t646061738_0_0_0_Collision2D_t1539500754_0_0_0 = { 3, GenInst_RocketChar_t1772935162_0_0_0_Collider2D_t646061738_0_0_0_Collision2D_t1539500754_0_0_0_Types };
extern const Il2CppType MandatoryDummyImplmplementation_t1329551270_0_0_0;
static const Il2CppType* GenInst_MandatoryDummyImplmplementation_t1329551270_0_0_0_Types[] = { &MandatoryDummyImplmplementation_t1329551270_0_0_0 };
extern const Il2CppGenericInst GenInst_MandatoryDummyImplmplementation_t1329551270_0_0_0 = { 1, GenInst_MandatoryDummyImplmplementation_t1329551270_0_0_0_Types };
extern const Il2CppType TwoDArray_1_t4208505166_0_0_0;
static const Il2CppType* GenInst_TwoDArray_1_t4208505166_0_0_0_Types[] = { &TwoDArray_1_t4208505166_0_0_0 };
extern const Il2CppGenericInst GenInst_TwoDArray_1_t4208505166_0_0_0 = { 1, GenInst_TwoDArray_1_t4208505166_0_0_0_Types };
extern const Il2CppType ArbitraryComponent_t2637609733_0_0_0;
static const Il2CppType* GenInst_ArbitraryComponent_t2637609733_0_0_0_Types[] = { &ArbitraryComponent_t2637609733_0_0_0 };
extern const Il2CppGenericInst GenInst_ArbitraryComponent_t2637609733_0_0_0 = { 1, GenInst_ArbitraryComponent_t2637609733_0_0_0_Types };
extern const Il2CppType KeyCode_t2283395152_0_0_0;
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0 = { 1, GenInst_KeyCode_t2283395152_0_0_0_Types };
extern const Il2CppType GridPoint_t3774252050_0_0_0;
static const Il2CppType* GenInst_GridPoint_t3774252050_0_0_0_Types[] = { &GridPoint_t3774252050_0_0_0 };
extern const Il2CppGenericInst GenInst_GridPoint_t3774252050_0_0_0 = { 1, GenInst_GridPoint_t3774252050_0_0_0_Types };
extern const Il2CppType TwoDGameObjectArray_t1262067236_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_TwoDGameObjectArray_t1262067236_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &TwoDGameObjectArray_t1262067236_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_TwoDGameObjectArray_t1262067236_0_0_0 = { 2, GenInst_GameObject_t1756533147_0_0_0_TwoDGameObjectArray_t1262067236_0_0_0_Types };
extern const Il2CppType JSONNode_t1250409636_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_Types[] = { &String_t_0_0_0, &JSONNode_t1250409636_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0 = { 2, GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &JSONNode_t1250409636_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t922534120_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_KeyValuePair_2_t922534120_0_0_0_Types[] = { &String_t_0_0_0, &JSONNode_t1250409636_0_0_0, &KeyValuePair_2_t922534120_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_KeyValuePair_2_t922534120_0_0_0 = { 3, GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_KeyValuePair_2_t922534120_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t922534120_0_0_0_Types[] = { &KeyValuePair_2_t922534120_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t922534120_0_0_0 = { 1, GenInst_KeyValuePair_2_t922534120_0_0_0_Types };
extern const Il2CppType DelayedWriteReference_t933455898_0_0_0;
static const Il2CppType* GenInst_DelayedWriteReference_t933455898_0_0_0_Types[] = { &DelayedWriteReference_t933455898_0_0_0 };
extern const Il2CppGenericInst GenInst_DelayedWriteReference_t933455898_0_0_0 = { 1, GenInst_DelayedWriteReference_t933455898_0_0_0_Types };
static const Il2CppType* GenInst_JSONNode_t1250409636_0_0_0_Types[] = { &JSONNode_t1250409636_0_0_0 };
extern const Il2CppGenericInst GenInst_JSONNode_t1250409636_0_0_0 = { 1, GenInst_JSONNode_t1250409636_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t922534120_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t922534120_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t922534120_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t922534120_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &JSONNode_t1250409636_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ArbitraryEvent_t766134336_0_0_0;
static const Il2CppType* GenInst_ArbitraryEvent_t766134336_0_0_0_Types[] = { &ArbitraryEvent_t766134336_0_0_0 };
extern const Il2CppGenericInst GenInst_ArbitraryEvent_t766134336_0_0_0 = { 1, GenInst_ArbitraryEvent_t766134336_0_0_0_Types };
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t901022143_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t901022143_0_0_0_Types[] = { &KeyValuePair_2_t901022143_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t901022143_0_0_0 = { 1, GenInst_KeyValuePair_2_t901022143_0_0_0_Types };
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_KeyCode_t2283395152_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0, &Boolean_t3825574718_0_0_0, &KeyCode_t2283395152_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_KeyCode_t2283395152_0_0_0 = { 3, GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_KeyCode_t2283395152_0_0_0_Types };
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0, &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t901022143_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t901022143_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t901022143_0_0_0 = { 3, GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t901022143_0_0_0_Types };
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0, &KeyCode_t2283395152_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0 = { 2, GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3653809873_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3653809873_0_0_0_Types[] = { &KeyValuePair_2_t3653809873_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653809873_0_0_0 = { 1, GenInst_KeyValuePair_2_t3653809873_0_0_0_Types };
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0, &KeyCode_t2283395152_0_0_0, &KeyCode_t2283395152_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0 = { 3, GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_Types };
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0, &KeyCode_t2283395152_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_KeyValuePair_2_t3653809873_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0, &KeyCode_t2283395152_0_0_0, &KeyValuePair_2_t3653809873_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_KeyValuePair_2_t3653809873_0_0_0 = { 3, GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_KeyValuePair_2_t3653809873_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3653809873_0_0_0_KeyCode_t2283395152_0_0_0_Types[] = { &KeyValuePair_2_t3653809873_0_0_0, &KeyCode_t2283395152_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653809873_0_0_0_KeyCode_t2283395152_0_0_0 = { 2, GenInst_KeyValuePair_2_t3653809873_0_0_0_KeyCode_t2283395152_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3653809873_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &KeyValuePair_2_t3653809873_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653809873_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t3653809873_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IMouseEventSystem_t2547859815_0_0_0;
static const Il2CppType* GenInst_IMouseEventSystem_t2547859815_0_0_0_Types[] = { &IMouseEventSystem_t2547859815_0_0_0 };
extern const Il2CppGenericInst GenInst_IMouseEventSystem_t2547859815_0_0_0 = { 1, GenInst_IMouseEventSystem_t2547859815_0_0_0_Types };
extern const Il2CppType AudibleSound_t2302054751_0_0_0;
static const Il2CppType* GenInst_AudibleSound_t2302054751_0_0_0_Types[] = { &AudibleSound_t2302054751_0_0_0 };
extern const Il2CppGenericInst GenInst_AudibleSound_t2302054751_0_0_0 = { 1, GenInst_AudibleSound_t2302054751_0_0_0_Types };
extern const Il2CppType SoundClip_t3743947423_0_0_0;
static const Il2CppType* GenInst_SoundClip_t3743947423_0_0_0_Types[] = { &SoundClip_t3743947423_0_0_0 };
extern const Il2CppGenericInst GenInst_SoundClip_t3743947423_0_0_0 = { 1, GenInst_SoundClip_t3743947423_0_0_0_Types };
extern const Il2CppType SoundLayer_t2188462586_0_0_0;
static const Il2CppType* GenInst_SoundLayer_t2188462586_0_0_0_Types[] = { &SoundLayer_t2188462586_0_0_0 };
extern const Il2CppGenericInst GenInst_SoundLayer_t2188462586_0_0_0 = { 1, GenInst_SoundLayer_t2188462586_0_0_0_Types };
extern const Il2CppType ScaleOn_t3059270671_0_0_0;
static const Il2CppType* GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_Types[] = { &ScaleOn_t3059270671_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1258057682_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1258057682_0_0_0_Types[] = { &KeyValuePair_2_t1258057682_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1258057682_0_0_0 = { 1, GenInst_KeyValuePair_2_t1258057682_0_0_0_Types };
static const Il2CppType* GenInst_ScaleOn_t3059270671_0_0_0_Types[] = { &ScaleOn_t3059270671_0_0_0 };
extern const Il2CppGenericInst GenInst_ScaleOn_t3059270671_0_0_0 = { 1, GenInst_ScaleOn_t3059270671_0_0_0_Types };
static const Il2CppType* GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_ScaleOn_t3059270671_0_0_0_Types[] = { &ScaleOn_t3059270671_0_0_0, &Single_t2076509932_0_0_0, &ScaleOn_t3059270671_0_0_0 };
extern const Il2CppGenericInst GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_ScaleOn_t3059270671_0_0_0 = { 3, GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_ScaleOn_t3059270671_0_0_0_Types };
static const Il2CppType* GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &ScaleOn_t3059270671_0_0_0, &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 3, GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ScaleOn_t3059270671_0_0_0, &Single_t2076509932_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t1258057682_0_0_0_Types[] = { &ScaleOn_t3059270671_0_0_0, &Single_t2076509932_0_0_0, &KeyValuePair_2_t1258057682_0_0_0 };
extern const Il2CppGenericInst GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t1258057682_0_0_0 = { 3, GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t1258057682_0_0_0_Types };
extern const Il2CppType ObjectSequence_t1807435812_0_0_0;
static const Il2CppType* GenInst_ObjectSequence_t1807435812_0_0_0_Types[] = { &ObjectSequence_t1807435812_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectSequence_t1807435812_0_0_0 = { 1, GenInst_ObjectSequence_t1807435812_0_0_0_Types };
extern const Il2CppType InputManager_t1610719423_0_0_0;
static const Il2CppType* GenInst_InputManager_t1610719423_0_0_0_Types[] = { &InputManager_t1610719423_0_0_0 };
extern const Il2CppGenericInst GenInst_InputManager_t1610719423_0_0_0 = { 1, GenInst_InputManager_t1610719423_0_0_0_Types };
extern const Il2CppType List_1_t1125654279_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &List_1_t1125654279_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0 = { 2, GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &List_1_t1125654279_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2213344945_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_KeyValuePair_2_t2213344945_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &List_1_t1125654279_0_0_0, &KeyValuePair_2_t2213344945_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_KeyValuePair_2_t2213344945_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_KeyValuePair_2_t2213344945_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2213344945_0_0_0_Types[] = { &KeyValuePair_2_t2213344945_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2213344945_0_0_0 = { 1, GenInst_KeyValuePair_2_t2213344945_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2844223813_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2844223813_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0, &KeyValuePair_2_t2844223813_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2844223813_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2844223813_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2844223813_0_0_0_Types[] = { &KeyValuePair_2_t2844223813_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2844223813_0_0_0 = { 1, GenInst_KeyValuePair_2_t2844223813_0_0_0_Types };
extern const Il2CppType StartupPool_t2587102281_0_0_0;
static const Il2CppType* GenInst_StartupPool_t2587102281_0_0_0_Types[] = { &StartupPool_t2587102281_0_0_0 };
extern const Il2CppGenericInst GenInst_StartupPool_t2587102281_0_0_0 = { 1, GenInst_StartupPool_t2587102281_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1125654279_0_0_0_Types[] = { &List_1_t1125654279_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1125654279_0_0_0 = { 1, GenInst_List_1_t1125654279_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_List_1_t1125654279_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &List_1_t1125654279_0_0_0, &List_1_t1125654279_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_List_1_t1125654279_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_List_1_t1125654279_0_0_0_Types };
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0 = { 1, GenInst_ILayoutElement_t1975293769_0_0_0_Types };
extern const Il2CppType MaskableGraphic_t540192618_0_0_0;
static const Il2CppType* GenInst_MaskableGraphic_t540192618_0_0_0_Types[] = { &MaskableGraphic_t540192618_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t540192618_0_0_0 = { 1, GenInst_MaskableGraphic_t540192618_0_0_0_Types };
extern const Il2CppType IMaskable_t1431842707_0_0_0;
static const Il2CppType* GenInst_IMaskable_t1431842707_0_0_0_Types[] = { &IMaskable_t1431842707_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaskable_t1431842707_0_0_0 = { 1, GenInst_IMaskable_t1431842707_0_0_0_Types };
extern const Il2CppType IMaterialModifier_t3028564983_0_0_0;
static const Il2CppType* GenInst_IMaterialModifier_t3028564983_0_0_0_Types[] = { &IMaterialModifier_t3028564983_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t3028564983_0_0_0 = { 1, GenInst_IMaterialModifier_t3028564983_0_0_0_Types };
extern const Il2CppType AudioSource_t1135106623_0_0_0;
static const Il2CppType* GenInst_AudioSource_t1135106623_0_0_0_Types[] = { &AudioSource_t1135106623_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t1135106623_0_0_0 = { 1, GenInst_AudioSource_t1135106623_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t2076509932_0_0_0_Types[] = { &String_t_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_String_t_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3720882578_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3720882578_0_0_0_Types[] = { &KeyValuePair_2_t3720882578_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3720882578_0_0_0 = { 1, GenInst_KeyValuePair_2_t3720882578_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t3720882578_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0, &KeyValuePair_2_t3720882578_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t3720882578_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t3720882578_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Single_t2076509932_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1748634416_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t1748634416_0_0_0_Types[] = { &String_t_0_0_0, &Single_t2076509932_0_0_0, &KeyValuePair_2_t1748634416_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t1748634416_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t1748634416_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1748634416_0_0_0_Types[] = { &KeyValuePair_2_t1748634416_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1748634416_0_0_0 = { 1, GenInst_KeyValuePair_2_t1748634416_0_0_0_Types };
extern const Il2CppType ParticleSystem_t3394631041_0_0_0;
static const Il2CppType* GenInst_ParticleSystem_t3394631041_0_0_0_Types[] = { &ParticleSystem_t3394631041_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleSystem_t3394631041_0_0_0 = { 1, GenInst_ParticleSystem_t3394631041_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &List_1_t1125654279_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &List_1_t1125654279_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2185792432_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0_KeyValuePair_2_t2185792432_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &List_1_t1125654279_0_0_0, &KeyValuePair_2_t2185792432_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0_KeyValuePair_2_t2185792432_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0_KeyValuePair_2_t2185792432_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2185792432_0_0_0_Types[] = { &KeyValuePair_2_t2185792432_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2185792432_0_0_0 = { 1, GenInst_KeyValuePair_2_t2185792432_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3132015601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0 = { 1, GenInst_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
extern const Il2CppType CFX_AutoDestructShuriken_t1500114366_0_0_0;
static const Il2CppType* GenInst_CFX_AutoDestructShuriken_t1500114366_0_0_0_Types[] = { &CFX_AutoDestructShuriken_t1500114366_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX_AutoDestructShuriken_t1500114366_0_0_0 = { 1, GenInst_CFX_AutoDestructShuriken_t1500114366_0_0_0_Types };
extern const Il2CppType CFX_LightIntensityFade_t4221734619_0_0_0;
static const Il2CppType* GenInst_CFX_LightIntensityFade_t4221734619_0_0_0_Types[] = { &CFX_LightIntensityFade_t4221734619_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX_LightIntensityFade_t4221734619_0_0_0 = { 1, GenInst_CFX_LightIntensityFade_t4221734619_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4048664256_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types[] = { &IEnumerable_1_t4048664256_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1730553742_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types[] = { &Array_Sort_m1730553742_gp_0_0_0_0, &Array_Sort_m1730553742_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3106198730_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3106198730_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types[] = { &Array_Sort_m3106198730_gp_0_0_0_0, &Array_Sort_m3106198730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2090966156_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0, &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0, &Array_Sort_m1985772939_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2736815140_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types[] = { &Array_Sort_m2736815140_gp_0_0_0_0, &Array_Sort_m2736815140_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2468799988_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2468799988_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types[] = { &Array_Sort_m2468799988_gp_0_0_0_0, &Array_Sort_m2468799988_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2587948790_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0, &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0, &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m52621935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types[] = { &Array_Sort_m52621935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m52621935_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3546416104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types[] = { &Array_Sort_m3546416104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3546416104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0, &Array_qsort_m533480027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m940423571_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m940423571_gp_0_0_0_0_Types[] = { &Array_compare_m940423571_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m940423571_gp_0_0_0_0 = { 1, GenInst_Array_compare_m940423571_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m565008110_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types[] = { &Array_qsort_m565008110_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m565008110_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m1201602141_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types[] = { &Array_Resize_m1201602141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m1201602141_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m2783802133_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m2783802133_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m3775633118_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types[] = { &Array_ForEach_m3775633118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m3775633118_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1734974082_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1734974082_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1734974082_gp_0_0_0_0, &Array_ConvertAll_m1734974082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m934773128_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m934773128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m3202023711_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m3202023711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m352384762_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m352384762_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1593955424_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1593955424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1546138173_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1546138173_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1082322798_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1082322798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m525402987_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m525402987_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3577113407_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3577113407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1033585031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1033585031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3052238307_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3052238307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1306290405_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1306290405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2825795862_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2825795862_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2841140625_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2841140625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3304283431_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3304283431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3860096562_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3860096562_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m2100440379_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m2100440379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m982349212_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types[] = { &Array_FindAll_m982349212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m982349212_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1825464757_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types[] = { &Array_Exists_m1825464757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1825464757_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1258056624_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1258056624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m2529971459_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types[] = { &Array_Find_m2529971459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m2529971459_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3929249453_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types[] = { &Array_FindLast_m3929249453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3929249453_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3582267753_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3582267753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3737699284_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3737699284_gp_0_0_0_0_Types[] = { &IList_1_t3737699284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3737699284_gp_0_0_0_0 = { 1, GenInst_IList_1_t3737699284_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1552160836_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types[] = { &ICollection_1_t1552160836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1552160836_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1398937014_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types[] = { &Nullable_1_t1398937014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1398937014_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1036860714_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types[] = { &Comparer_1_t1036860714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1036860714_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3074655092_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types[] = { &DefaultComparer_t3074655092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3074655092_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1787398723_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1787398723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3180694294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0 = { 1, GenInst_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3895203923_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3895203923_gp_0_0_0_0, &ShimEnumerator_t3895203923_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2089681430_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types[] = { &Enumerator_t2089681430_gp_0_0_0_0, &Enumerator_t2089681430_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3434615342_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3434615342_0_0_0_Types[] = { &KeyValuePair_2_t3434615342_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434615342_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434615342_0_0_0_Types };
extern const Il2CppType KeyCollection_t1229212677_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t83320710_gp_0_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0, &Enumerator_t83320710_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0 = { 2, GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0 = { 1, GenInst_Enumerator_t83320710_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t2262344653_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3111723616_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_0_0_0_0, &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 2, GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2066709010_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2066709010_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1766400012_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types[] = { &DefaultComparer_t1766400012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1766400012_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2202941003_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4174120762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4174120762_0_0_0_Types[] = { &KeyValuePair_2_t4174120762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4174120762_0_0_0 = { 1, GenInst_KeyValuePair_2_t4174120762_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t3502329323_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0, &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1988958766_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1988958766_gp_0_0_0_0, &KeyValuePair_2_t1988958766_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t1169184319_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1169184319_gp_0_0_0_0_Types[] = { &List_1_t1169184319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0 = { 1, GenInst_List_1_t1169184319_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1292967705_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types[] = { &Enumerator_t1292967705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1292967705_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t686054069_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t686054069_gp_0_0_0_0_Types[] = { &Collection_1_t686054069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t686054069_gp_0_0_0_0 = { 1, GenInst_Collection_1_t686054069_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3540981679_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t4016656541_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types[] = { &Stack_1_t4016656541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4016656541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t546412149_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t546412149_gp_0_0_0_0_Types[] = { &Enumerator_t546412149_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t546412149_gp_0_0_0_0 = { 1, GenInst_Enumerator_t546412149_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t2624254809_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types[] = { &HashSet_1_t2624254809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2624254809_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2109956843_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types[] = { &Enumerator_t2109956843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2109956843_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3424417428_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types[] = { &PrimeHelper_t3424417428_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3424417428_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m665396702_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types[] = { &Enumerable_Any_m665396702_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m665396702_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ElementAt_m258442918_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0_Types[] = { &Enumerable_ElementAt_m258442918_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0 = { 1, GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ElementAt_m714932326_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0_Types[] = { &Enumerable_ElementAt_m714932326_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0 = { 1, GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m1693250038_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m1693250038_gp_0_0_0_0_Types[] = { &Enumerable_First_m1693250038_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1693250038_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m1693250038_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0, &Enumerable_OrderBy_m920500904_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0, &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0, &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m2343256994_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m2343256994_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m261161385_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m261161385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m2409552823_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IOrderedEnumerable_1_t641749975_gp_0_0_0_0;
static const Il2CppType* GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_t641749975_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types };
extern const Il2CppType OrderedEnumerable_1_t753306046_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_t753306046_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_1_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0, &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 2, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
extern const Il2CppType QuickSort_1_t1290221672_gp_0_0_0_0;
static const Il2CppType* GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types[] = { &QuickSort_1_t1290221672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuickSort_1_t1290221672_gp_0_0_0_0 = { 1, GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types };
extern const Il2CppType U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types[] = { &U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 = { 1, GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types };
extern const Il2CppType SortContext_1_t4088581714_gp_0_0_0_0;
static const Il2CppType* GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types[] = { &SortContext_1_t4088581714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortContext_1_t4088581714_gp_0_0_0_0 = { 1, GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_0_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_1_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0, &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 2, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m3417738402_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m825036157_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m825036157_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m3873375864_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m1600202230_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3990064736_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3990064736_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2051523689_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2051523689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m2621570726_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m2621570726_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types[] = { &Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SafeLength_m3101579087_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types[] = { &Mesh_SafeLength_m3101579087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m3999848894_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m4171325764_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types[] = { &Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types };
extern const Il2CppType Object_Instantiate_m2530741872_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types[] = { &Object_Instantiate_m2530741872_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m894835059_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m894835059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t476640868_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types[] = { &InvokableCall_1_t476640868_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t476640868_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types };
extern const Il2CppType UnityAction_1_t2490859068_0_0_0;
static const Il2CppType* GenInst_UnityAction_1_t2490859068_0_0_0_Types[] = { &UnityAction_1_t2490859068_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_1_t2490859068_0_0_0 = { 1, GenInst_UnityAction_1_t2490859068_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2042724809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0, &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3608808750_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0, &InvokableCall_3_t3608808750_gp_1_0_0_0, &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t879925395_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0, &InvokableCall_4_t879925395_gp_1_0_0_0, &InvokableCall_4_t879925395_gp_2_0_0_0, &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t224769006_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t224769006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t4075366602_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types[] = { &UnityEvent_1_t4075366602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t4075366599_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types[] = { &UnityEvent_2_t4075366599_gp_0_0_0_0, &UnityEvent_2_t4075366599_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t4075366600_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types[] = { &UnityEvent_3_t4075366600_gp_0_0_0_0, &UnityEvent_3_t4075366600_gp_1_0_0_0, &UnityEvent_3_t4075366600_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t4075366597_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types[] = { &UnityEvent_4_t4075366597_gp_0_0_0_0, &UnityEvent_4_t4075366597_gp_1_0_0_0, &UnityEvent_4_t4075366597_gp_2_0_0_0, &UnityEvent_4_t4075366597_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types };
extern const Il2CppType DOTween_To_m3442317795_gp_0_0_0_0;
extern const Il2CppType DOTween_To_m3442317795_gp_1_0_0_0;
extern const Il2CppType DOTween_To_m3442317795_gp_2_0_0_0;
static const Il2CppType* GenInst_DOTween_To_m3442317795_gp_0_0_0_0_DOTween_To_m3442317795_gp_1_0_0_0_DOTween_To_m3442317795_gp_2_0_0_0_Types[] = { &DOTween_To_m3442317795_gp_0_0_0_0, &DOTween_To_m3442317795_gp_1_0_0_0, &DOTween_To_m3442317795_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTween_To_m3442317795_gp_0_0_0_0_DOTween_To_m3442317795_gp_1_0_0_0_DOTween_To_m3442317795_gp_2_0_0_0 = { 3, GenInst_DOTween_To_m3442317795_gp_0_0_0_0_DOTween_To_m3442317795_gp_1_0_0_0_DOTween_To_m3442317795_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_DOTween_To_m3442317795_gp_0_0_0_0_Types[] = { &DOTween_To_m3442317795_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTween_To_m3442317795_gp_0_0_0_0 = { 1, GenInst_DOTween_To_m3442317795_gp_0_0_0_0_Types };
extern const Il2CppType DOTween_ApplyTo_m4058900771_gp_0_0_0_0;
static const Il2CppType* GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_Types[] = { &DOTween_ApplyTo_m4058900771_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0 = { 1, GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_Types };
extern const Il2CppType DOTween_ApplyTo_m4058900771_gp_1_0_0_0;
extern const Il2CppType DOTween_ApplyTo_m4058900771_gp_2_0_0_0;
static const Il2CppType* GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_DOTween_ApplyTo_m4058900771_gp_1_0_0_0_DOTween_ApplyTo_m4058900771_gp_2_0_0_0_Types[] = { &DOTween_ApplyTo_m4058900771_gp_0_0_0_0, &DOTween_ApplyTo_m4058900771_gp_1_0_0_0, &DOTween_ApplyTo_m4058900771_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_DOTween_ApplyTo_m4058900771_gp_1_0_0_0_DOTween_ApplyTo_m4058900771_gp_2_0_0_0 = { 3, GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_DOTween_ApplyTo_m4058900771_gp_1_0_0_0_DOTween_ApplyTo_m4058900771_gp_2_0_0_0_Types };
extern const Il2CppType Tween_OnTweenCallback_m4208167374_gp_0_0_0_0;
static const Il2CppType* GenInst_Tween_OnTweenCallback_m4208167374_gp_0_0_0_0_Types[] = { &Tween_OnTweenCallback_m4208167374_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Tween_OnTweenCallback_m4208167374_gp_0_0_0_0 = { 1, GenInst_Tween_OnTweenCallback_m4208167374_gp_0_0_0_0_Types };
extern const Il2CppType Tweener_Setup_m1281370100_gp_0_0_0_0;
extern const Il2CppType Tweener_Setup_m1281370100_gp_1_0_0_0;
extern const Il2CppType Tweener_Setup_m1281370100_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Tweener_Setup_m1281370100_gp_1_0_0_0_Tweener_Setup_m1281370100_gp_2_0_0_0_Types[] = { &Tweener_Setup_m1281370100_gp_0_0_0_0, &Tweener_Setup_m1281370100_gp_1_0_0_0, &Tweener_Setup_m1281370100_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Tweener_Setup_m1281370100_gp_1_0_0_0_Tweener_Setup_m1281370100_gp_2_0_0_0 = { 3, GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Tweener_Setup_m1281370100_gp_1_0_0_0_Tweener_Setup_m1281370100_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Types[] = { &Tweener_Setup_m1281370100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0 = { 1, GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Types };
extern const Il2CppType Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0;
extern const Il2CppType Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0;
extern const Il2CppType Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0_Types[] = { &Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0, &Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0, &Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0 = { 3, GenInst_Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0_Types };
extern const Il2CppType Tweener_DoStartup_m3383447813_gp_0_0_0_0;
extern const Il2CppType Tweener_DoStartup_m3383447813_gp_1_0_0_0;
extern const Il2CppType Tweener_DoStartup_m3383447813_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Tweener_DoStartup_m3383447813_gp_1_0_0_0_Tweener_DoStartup_m3383447813_gp_2_0_0_0_Types[] = { &Tweener_DoStartup_m3383447813_gp_0_0_0_0, &Tweener_DoStartup_m3383447813_gp_1_0_0_0, &Tweener_DoStartup_m3383447813_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Tweener_DoStartup_m3383447813_gp_1_0_0_0_Tweener_DoStartup_m3383447813_gp_2_0_0_0 = { 3, GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Tweener_DoStartup_m3383447813_gp_1_0_0_0_Tweener_DoStartup_m3383447813_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Types[] = { &Tweener_DoStartup_m3383447813_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0 = { 1, GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Types };
extern const Il2CppType Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0;
extern const Il2CppType Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0;
extern const Il2CppType Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0_Types[] = { &Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0, &Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0, &Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0 = { 3, GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Types[] = { &Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0 = { 1, GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Types };
extern const Il2CppType Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0;
extern const Il2CppType Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0;
extern const Il2CppType Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0_Types[] = { &Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0, &Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0, &Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0 = { 3, GenInst_Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0_Types };
extern const Il2CppType Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0;
extern const Il2CppType Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0;
extern const Il2CppType Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0_Types[] = { &Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0, &Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0, &Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0 = { 3, GenInst_Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0_Types };
extern const Il2CppType ABSTweenPlugin_3_t670396699_gp_0_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t670396699_gp_1_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t670396699_gp_2_0_0_0;
static const Il2CppType* GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_ABSTweenPlugin_3_t670396699_gp_1_0_0_0_ABSTweenPlugin_3_t670396699_gp_2_0_0_0_Types[] = { &ABSTweenPlugin_3_t670396699_gp_0_0_0_0, &ABSTweenPlugin_3_t670396699_gp_1_0_0_0, &ABSTweenPlugin_3_t670396699_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_ABSTweenPlugin_3_t670396699_gp_1_0_0_0_ABSTweenPlugin_3_t670396699_gp_2_0_0_0 = { 3, GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_ABSTweenPlugin_3_t670396699_gp_1_0_0_0_ABSTweenPlugin_3_t670396699_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_Types[] = { &ABSTweenPlugin_3_t670396699_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0 = { 1, GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_Types };
extern const Il2CppType PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0;
extern const Il2CppType PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0;
extern const Il2CppType PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0;
static const Il2CppType* GenInst_PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0_Types[] = { &PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0, &PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0, &PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0 = { 3, GenInst_PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0_Types };
extern const Il2CppType PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0;
extern const Il2CppType PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0;
extern const Il2CppType PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0;
static const Il2CppType* GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0_Types[] = { &PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0, &PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0, &PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0 = { 3, GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0_Types };
extern const Il2CppType PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0;
static const Il2CppType* GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0_Types[] = { &PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0 = { 1, GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0_Types };
extern const Il2CppType Extensions_NoFrom_m2168138502_gp_0_0_0_0;
extern const Il2CppType Extensions_NoFrom_m2168138502_gp_1_0_0_0;
extern const Il2CppType Extensions_NoFrom_m2168138502_gp_2_0_0_0;
static const Il2CppType* GenInst_Extensions_NoFrom_m2168138502_gp_0_0_0_0_Extensions_NoFrom_m2168138502_gp_1_0_0_0_Extensions_NoFrom_m2168138502_gp_2_0_0_0_Types[] = { &Extensions_NoFrom_m2168138502_gp_0_0_0_0, &Extensions_NoFrom_m2168138502_gp_1_0_0_0, &Extensions_NoFrom_m2168138502_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_NoFrom_m2168138502_gp_0_0_0_0_Extensions_NoFrom_m2168138502_gp_1_0_0_0_Extensions_NoFrom_m2168138502_gp_2_0_0_0 = { 3, GenInst_Extensions_NoFrom_m2168138502_gp_0_0_0_0_Extensions_NoFrom_m2168138502_gp_1_0_0_0_Extensions_NoFrom_m2168138502_gp_2_0_0_0_Types };
extern const Il2CppType TweenManager_GetTweener_m346233533_gp_0_0_0_0;
extern const Il2CppType TweenManager_GetTweener_m346233533_gp_1_0_0_0;
extern const Il2CppType TweenManager_GetTweener_m346233533_gp_2_0_0_0;
static const Il2CppType* GenInst_TweenManager_GetTweener_m346233533_gp_0_0_0_0_TweenManager_GetTweener_m346233533_gp_1_0_0_0_TweenManager_GetTweener_m346233533_gp_2_0_0_0_Types[] = { &TweenManager_GetTweener_m346233533_gp_0_0_0_0, &TweenManager_GetTweener_m346233533_gp_1_0_0_0, &TweenManager_GetTweener_m346233533_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenManager_GetTweener_m346233533_gp_0_0_0_0_TweenManager_GetTweener_m346233533_gp_1_0_0_0_TweenManager_GetTweener_m346233533_gp_2_0_0_0 = { 3, GenInst_TweenManager_GetTweener_m346233533_gp_0_0_0_0_TweenManager_GetTweener_m346233533_gp_1_0_0_0_TweenManager_GetTweener_m346233533_gp_2_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3922192912_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_Types[] = { &TweenerCore_3_t3922192912_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0 = { 1, GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3922192912_gp_1_0_0_0;
extern const Il2CppType TweenerCore_3_t3922192912_gp_2_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_TweenerCore_3_t3922192912_gp_1_0_0_0_TweenerCore_3_t3922192912_gp_2_0_0_0_Types[] = { &TweenerCore_3_t3922192912_gp_0_0_0_0, &TweenerCore_3_t3922192912_gp_1_0_0_0, &TweenerCore_3_t3922192912_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_TweenerCore_3_t3922192912_gp_1_0_0_0_TweenerCore_3_t3922192912_gp_2_0_0_0 = { 3, GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_TweenerCore_3_t3922192912_gp_1_0_0_0_TweenerCore_3_t3922192912_gp_2_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m1961163955_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2584777480_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2584777480_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t573160278_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType ListPool_1_t1984115411_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types[] = { &ListPool_1_t1984115411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t1984115411_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2000868992_0_0_0;
static const Il2CppType* GenInst_List_1_t2000868992_0_0_0_Types[] = { &List_1_t2000868992_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2000868992_0_0_0 = { 1, GenInst_List_1_t2000868992_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t4265859154_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types[] = { &ObjectPool_1_t4265859154_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types };
extern const Il2CppType DDManagerBase_1_t2960862101_gp_0_0_0_0;
static const Il2CppType* GenInst_DDManagerBase_1_t2960862101_gp_0_0_0_0_Types[] = { &DDManagerBase_1_t2960862101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DDManagerBase_1_t2960862101_gp_0_0_0_0 = { 1, GenInst_DDManagerBase_1_t2960862101_gp_0_0_0_0_Types };
extern const Il2CppType ArrayUtils_randomizeList_m2699713364_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayUtils_randomizeList_m2699713364_gp_0_0_0_0_Types[] = { &ArrayUtils_randomizeList_m2699713364_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayUtils_randomizeList_m2699713364_gp_0_0_0_0 = { 1, GenInst_ArrayUtils_randomizeList_m2699713364_gp_0_0_0_0_Types };
extern const Il2CppType ArrayUtils_randomizeListSameOrder_m3179219044_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayUtils_randomizeListSameOrder_m3179219044_gp_0_0_0_0_Types[] = { &ArrayUtils_randomizeListSameOrder_m3179219044_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayUtils_randomizeListSameOrder_m3179219044_gp_0_0_0_0 = { 1, GenInst_ArrayUtils_randomizeListSameOrder_m3179219044_gp_0_0_0_0_Types };
extern const Il2CppType ArrayUtils_printArray_m1474984333_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayUtils_printArray_m1474984333_gp_0_0_0_0_Types[] = { &ArrayUtils_printArray_m1474984333_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayUtils_printArray_m1474984333_gp_0_0_0_0 = { 1, GenInst_ArrayUtils_printArray_m1474984333_gp_0_0_0_0_Types };
extern const Il2CppType ArrayUtils_printList_m3464464492_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayUtils_printList_m3464464492_gp_0_0_0_0_Types[] = { &ArrayUtils_printList_m3464464492_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayUtils_printList_m3464464492_gp_0_0_0_0 = { 1, GenInst_ArrayUtils_printList_m3464464492_gp_0_0_0_0_Types };
extern const Il2CppType ArrayUtils_compareArrays_m4204017686_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayUtils_compareArrays_m4204017686_gp_0_0_0_0_Types[] = { &ArrayUtils_compareArrays_m4204017686_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayUtils_compareArrays_m4204017686_gp_0_0_0_0 = { 1, GenInst_ArrayUtils_compareArrays_m4204017686_gp_0_0_0_0_Types };
extern const Il2CppType ArrayUtils_compareOrderedArrays_m2308127077_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayUtils_compareOrderedArrays_m2308127077_gp_0_0_0_0_Types[] = { &ArrayUtils_compareOrderedArrays_m2308127077_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayUtils_compareOrderedArrays_m2308127077_gp_0_0_0_0 = { 1, GenInst_ArrayUtils_compareOrderedArrays_m2308127077_gp_0_0_0_0_Types };
extern const Il2CppType ArrayUtils_getCommonElements_m1688440392_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayUtils_getCommonElements_m1688440392_gp_0_0_0_0_Types[] = { &ArrayUtils_getCommonElements_m1688440392_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayUtils_getCommonElements_m1688440392_gp_0_0_0_0 = { 1, GenInst_ArrayUtils_getCommonElements_m1688440392_gp_0_0_0_0_Types };
extern const Il2CppType ArrayUtils_ArrayToString_m1945898527_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayUtils_ArrayToString_m1945898527_gp_0_0_0_0_Types[] = { &ArrayUtils_ArrayToString_m1945898527_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayUtils_ArrayToString_m1945898527_gp_0_0_0_0 = { 1, GenInst_ArrayUtils_ArrayToString_m1945898527_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtension_GetOrAddComponent_m1609223567_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtension_GetOrAddComponent_m1609223567_gp_0_0_0_0_Types[] = { &GameObjectExtension_GetOrAddComponent_m1609223567_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtension_GetOrAddComponent_m1609223567_gp_0_0_0_0 = { 1, GenInst_GameObjectExtension_GetOrAddComponent_m1609223567_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t4277882758_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4277882758_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t4277882758_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4277882758_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4277882758_gp_0_0_0_0_Types };
extern const Il2CppType TwoDArray_1_t2102069470_gp_0_0_0_0;
static const Il2CppType* GenInst_TwoDArray_1_t2102069470_gp_0_0_0_0_Types[] = { &TwoDArray_1_t2102069470_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TwoDArray_1_t2102069470_gp_0_0_0_0 = { 1, GenInst_TwoDArray_1_t2102069470_gp_0_0_0_0_Types };
extern const Il2CppType GenericMouseEvent_1_t523005203_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericMouseEvent_1_t523005203_gp_0_0_0_0_Types[] = { &GenericMouseEvent_1_t523005203_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericMouseEvent_1_t523005203_gp_0_0_0_0 = { 1, GenInst_GenericMouseEvent_1_t523005203_gp_0_0_0_0_Types };
extern const Il2CppType MouseDelegate_t3478847027_0_0_0;
static const Il2CppType* GenInst_MouseDelegate_t3478847027_0_0_0_Types[] = { &MouseDelegate_t3478847027_0_0_0 };
extern const Il2CppGenericInst GenInst_MouseDelegate_t3478847027_0_0_0 = { 1, GenInst_MouseDelegate_t3478847027_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_1_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0, &U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_1_0_0_0 = { 2, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0_Types };
extern const Il2CppType GridRepGenereic_2_t959362555_gp_0_0_0_0;
static const Il2CppType* GenInst_GridRepGenereic_2_t959362555_gp_0_0_0_0_Types[] = { &GridRepGenereic_2_t959362555_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GridRepGenereic_2_t959362555_gp_0_0_0_0 = { 1, GenInst_GridRepGenereic_2_t959362555_gp_0_0_0_0_Types };
extern const Il2CppType GridRepGenereic_2_t959362555_gp_1_0_0_0;
static const Il2CppType* GenInst_GridRepGenereic_2_t959362555_gp_0_0_0_0_GridRepGenereic_2_t959362555_gp_1_0_0_0_Types[] = { &GridRepGenereic_2_t959362555_gp_0_0_0_0, &GridRepGenereic_2_t959362555_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GridRepGenereic_2_t959362555_gp_0_0_0_0_GridRepGenereic_2_t959362555_gp_1_0_0_0 = { 2, GenInst_GridRepGenereic_2_t959362555_gp_0_0_0_0_GridRepGenereic_2_t959362555_gp_1_0_0_0_Types };
extern const Il2CppType ObjectPool_Spawn_m3379636899_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_Spawn_m3379636899_gp_0_0_0_0_Types[] = { &ObjectPool_Spawn_m3379636899_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_Spawn_m3379636899_gp_0_0_0_0 = { 1, GenInst_ObjectPool_Spawn_m3379636899_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPool_Spawn_m3029418448_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_Spawn_m3029418448_gp_0_0_0_0_Types[] = { &ObjectPool_Spawn_m3029418448_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_Spawn_m3029418448_gp_0_0_0_0 = { 1, GenInst_ObjectPool_Spawn_m3029418448_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPool_Spawn_m941763866_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_Spawn_m941763866_gp_0_0_0_0_Types[] = { &ObjectPool_Spawn_m941763866_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_Spawn_m941763866_gp_0_0_0_0 = { 1, GenInst_ObjectPool_Spawn_m941763866_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPool_Spawn_m2099581555_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_Spawn_m2099581555_gp_0_0_0_0_Types[] = { &ObjectPool_Spawn_m2099581555_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_Spawn_m2099581555_gp_0_0_0_0 = { 1, GenInst_ObjectPool_Spawn_m2099581555_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPool_Spawn_m3044749625_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_Spawn_m3044749625_gp_0_0_0_0_Types[] = { &ObjectPool_Spawn_m3044749625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_Spawn_m3044749625_gp_0_0_0_0 = { 1, GenInst_ObjectPool_Spawn_m3044749625_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPool_Spawn_m2976207054_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_Spawn_m2976207054_gp_0_0_0_0_Types[] = { &ObjectPool_Spawn_m2976207054_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_Spawn_m2976207054_gp_0_0_0_0 = { 1, GenInst_ObjectPool_Spawn_m2976207054_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPool_GetPooled_m3585561301_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_GetPooled_m3585561301_gp_0_0_0_0_Types[] = { &ObjectPool_GetPooled_m3585561301_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_GetPooled_m3585561301_gp_0_0_0_0 = { 1, GenInst_ObjectPool_GetPooled_m3585561301_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPool_GetSpawned_m4048446810_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_GetSpawned_m4048446810_gp_0_0_0_0_Types[] = { &ObjectPool_GetSpawned_m4048446810_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_GetSpawned_m4048446810_gp_0_0_0_0 = { 1, GenInst_ObjectPool_GetSpawned_m4048446810_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_CreatePool_m2099972984_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_CreatePool_m2099972984_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_CreatePool_m2099972984_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_CreatePool_m2099972984_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_CreatePool_m2099972984_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_CreatePool_m152801013_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_CreatePool_m152801013_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_CreatePool_m152801013_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_CreatePool_m152801013_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_CreatePool_m152801013_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_Spawn_m3658873873_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_Spawn_m3658873873_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_Spawn_m3658873873_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_Spawn_m3658873873_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_Spawn_m3658873873_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_Spawn_m359633822_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_Spawn_m359633822_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_Spawn_m359633822_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_Spawn_m359633822_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_Spawn_m359633822_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_Spawn_m881325688_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_Spawn_m881325688_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_Spawn_m881325688_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_Spawn_m881325688_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_Spawn_m881325688_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_Spawn_m1216740145_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_Spawn_m1216740145_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_Spawn_m1216740145_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_Spawn_m1216740145_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_Spawn_m1216740145_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_Spawn_m2877761083_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_Spawn_m2877761083_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_Spawn_m2877761083_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_Spawn_m2877761083_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_Spawn_m2877761083_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_Spawn_m2085758064_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_Spawn_m2085758064_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_Spawn_m2085758064_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_Spawn_m2085758064_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_Spawn_m2085758064_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_Recycle_m1959930187_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_Recycle_m1959930187_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_Recycle_m1959930187_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_Recycle_m1959930187_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_Recycle_m1959930187_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_RecycleAll_m2567932538_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_RecycleAll_m2567932538_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_RecycleAll_m2567932538_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_RecycleAll_m2567932538_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_RecycleAll_m2567932538_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_CountPooled_m1364441074_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_CountPooled_m1364441074_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_CountPooled_m1364441074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_CountPooled_m1364441074_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_CountPooled_m1364441074_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_CountSpawned_m1385273839_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_CountSpawned_m1385273839_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_CountSpawned_m1385273839_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_CountSpawned_m1385273839_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_CountSpawned_m1385273839_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_GetSpawned_m2878770644_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_GetSpawned_m2878770644_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_GetSpawned_m2878770644_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_GetSpawned_m2878770644_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_GetSpawned_m2878770644_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_GetSpawned_m1321398291_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_GetSpawned_m1321398291_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_GetSpawned_m1321398291_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_GetSpawned_m1321398291_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_GetSpawned_m1321398291_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_GetSpawned_m410148605_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_GetSpawned_m410148605_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_GetSpawned_m410148605_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_GetSpawned_m410148605_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_GetSpawned_m410148605_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_GetPooled_m3776185483_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_GetPooled_m3776185483_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_GetPooled_m3776185483_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_GetPooled_m3776185483_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_GetPooled_m3776185483_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_GetPooled_m24459516_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_GetPooled_m24459516_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_GetPooled_m24459516_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_GetPooled_m24459516_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_GetPooled_m24459516_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPoolExtensions_GetPooled_m4125519546_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPoolExtensions_GetPooled_m4125519546_gp_0_0_0_0_Types[] = { &ObjectPoolExtensions_GetPooled_m4125519546_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPoolExtensions_GetPooled_m4125519546_gp_0_0_0_0 = { 1, GenInst_ObjectPoolExtensions_GetPooled_m4125519546_gp_0_0_0_0_Types };
extern const Il2CppType SingletonBase_1_t2432853269_gp_0_0_0_0;
static const Il2CppType* GenInst_SingletonBase_1_t2432853269_gp_0_0_0_0_Types[] = { &SingletonBase_1_t2432853269_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SingletonBase_1_t2432853269_gp_0_0_0_0 = { 1, GenInst_SingletonBase_1_t2432853269_gp_0_0_0_0_Types };
extern const Il2CppType Utils_Shuffle_m726398728_gp_0_0_0_0;
static const Il2CppType* GenInst_Utils_Shuffle_m726398728_gp_0_0_0_0_Types[] = { &Utils_Shuffle_m726398728_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Utils_Shuffle_m726398728_gp_0_0_0_0 = { 1, GenInst_Utils_Shuffle_m726398728_gp_0_0_0_0_Types };
extern const Il2CppType DefaultExecutionOrder_t2717914595_0_0_0;
static const Il2CppType* GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types[] = { &DefaultExecutionOrder_t2717914595_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t2717914595_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types };
extern const Il2CppType PlayerConnection_t3517219175_0_0_0;
static const Il2CppType* GenInst_PlayerConnection_t3517219175_0_0_0_Types[] = { &PlayerConnection_t3517219175_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerConnection_t3517219175_0_0_0 = { 1, GenInst_PlayerConnection_t3517219175_0_0_0_Types };
extern const Il2CppType GUILayer_t3254902478_0_0_0;
static const Il2CppType* GenInst_GUILayer_t3254902478_0_0_0_Types[] = { &GUILayer_t3254902478_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t3254902478_0_0_0 = { 1, GenInst_GUILayer_t3254902478_0_0_0_Types };
extern const Il2CppType DOTweenComponent_t696744215_0_0_0;
static const Il2CppType* GenInst_DOTweenComponent_t696744215_0_0_0_Types[] = { &DOTweenComponent_t696744215_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTweenComponent_t696744215_0_0_0 = { 1, GenInst_DOTweenComponent_t696744215_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3793077019_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3793077019_0_0_0_Types[] = { &TweenerCore_3_t3793077019_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3793077019_0_0_0 = { 1, GenInst_TweenerCore_3_t3793077019_0_0_0_Types };
extern const Il2CppType PathPlugin_t4171842066_0_0_0;
static const Il2CppType* GenInst_PathPlugin_t4171842066_0_0_0_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0_Types[] = { &PathPlugin_t4171842066_0_0_0, &Vector3_t2243707580_0_0_0, &Path_t2828565993_0_0_0, &PathOptions_t2659884781_0_0_0 };
extern const Il2CppGenericInst GenInst_PathPlugin_t4171842066_0_0_0_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0 = { 4, GenInst_PathPlugin_t4171842066_0_0_0_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t2279406887_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t2279406887_0_0_0_Types[] = { &TweenerCore_3_t2279406887_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t2279406887_0_0_0 = { 1, GenInst_TweenerCore_3_t2279406887_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t2998039394_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t2998039394_0_0_0_Types[] = { &TweenerCore_3_t2998039394_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t2998039394_0_0_0 = { 1, GenInst_TweenerCore_3_t2998039394_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3065187631_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3065187631_0_0_0_Types[] = { &TweenerCore_3_t3065187631_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3065187631_0_0_0 = { 1, GenInst_TweenerCore_3_t3065187631_0_0_0_Types };
extern const Il2CppType Tweener_t760404022_0_0_0;
static const Il2CppType* GenInst_Tweener_t760404022_0_0_0_Types[] = { &Tweener_t760404022_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_t760404022_0_0_0 = { 1, GenInst_Tweener_t760404022_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t1672798003_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t1672798003_0_0_0_Types[] = { &TweenerCore_3_t1672798003_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t1672798003_0_0_0 = { 1, GenInst_TweenerCore_3_t1672798003_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3035488489_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3035488489_0_0_0_Types[] = { &TweenerCore_3_t3035488489_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3035488489_0_0_0 = { 1, GenInst_TweenerCore_3_t3035488489_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t1108663466_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t1108663466_0_0_0_Types[] = { &TweenerCore_3_t1108663466_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t1108663466_0_0_0 = { 1, GenInst_TweenerCore_3_t1108663466_0_0_0_Types };
extern const Il2CppType EventSystem_t3466835263_0_0_0;
static const Il2CppType* GenInst_EventSystem_t3466835263_0_0_0_Types[] = { &EventSystem_t3466835263_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t3466835263_0_0_0 = { 1, GenInst_EventSystem_t3466835263_0_0_0_Types };
extern const Il2CppType AxisEventData_t1524870173_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t1524870173_0_0_0_Types[] = { &AxisEventData_t1524870173_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t1524870173_0_0_0 = { 1, GenInst_AxisEventData_t1524870173_0_0_0_Types };
extern const Il2CppType Image_t2042527209_0_0_0;
static const Il2CppType* GenInst_Image_t2042527209_0_0_0_Types[] = { &Image_t2042527209_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2042527209_0_0_0 = { 1, GenInst_Image_t2042527209_0_0_0_Types };
extern const Il2CppType Button_t2872111280_0_0_0;
static const Il2CppType* GenInst_Button_t2872111280_0_0_0_Types[] = { &Button_t2872111280_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t2872111280_0_0_0 = { 1, GenInst_Button_t2872111280_0_0_0_Types };
extern const Il2CppType RawImage_t2749640213_0_0_0;
static const Il2CppType* GenInst_RawImage_t2749640213_0_0_0_Types[] = { &RawImage_t2749640213_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t2749640213_0_0_0 = { 1, GenInst_RawImage_t2749640213_0_0_0_Types };
extern const Il2CppType Slider_t297367283_0_0_0;
static const Il2CppType* GenInst_Slider_t297367283_0_0_0_Types[] = { &Slider_t297367283_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t297367283_0_0_0 = { 1, GenInst_Slider_t297367283_0_0_0_Types };
extern const Il2CppType Scrollbar_t3248359358_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t3248359358_0_0_0_Types[] = { &Scrollbar_t3248359358_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t3248359358_0_0_0 = { 1, GenInst_Scrollbar_t3248359358_0_0_0_Types };
extern const Il2CppType InputField_t1631627530_0_0_0;
static const Il2CppType* GenInst_InputField_t1631627530_0_0_0_Types[] = { &InputField_t1631627530_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t1631627530_0_0_0 = { 1, GenInst_InputField_t1631627530_0_0_0_Types };
extern const Il2CppType ScrollRect_t1199013257_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t1199013257_0_0_0_Types[] = { &ScrollRect_t1199013257_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t1199013257_0_0_0 = { 1, GenInst_ScrollRect_t1199013257_0_0_0_Types };
extern const Il2CppType Dropdown_t1985816271_0_0_0;
static const Il2CppType* GenInst_Dropdown_t1985816271_0_0_0_Types[] = { &Dropdown_t1985816271_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t1985816271_0_0_0 = { 1, GenInst_Dropdown_t1985816271_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t410733016_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t410733016_0_0_0_Types[] = { &GraphicRaycaster_t410733016_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t410733016_0_0_0 = { 1, GenInst_GraphicRaycaster_t410733016_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t261436805_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t261436805_0_0_0_Types[] = { &CanvasRenderer_t261436805_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t261436805_0_0_0 = { 1, GenInst_CanvasRenderer_t261436805_0_0_0_Types };
extern const Il2CppType Corner_t1077473318_0_0_0;
static const Il2CppType* GenInst_Corner_t1077473318_0_0_0_Types[] = { &Corner_t1077473318_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t1077473318_0_0_0 = { 1, GenInst_Corner_t1077473318_0_0_0_Types };
extern const Il2CppType Axis_t1431825778_0_0_0;
static const Il2CppType* GenInst_Axis_t1431825778_0_0_0_Types[] = { &Axis_t1431825778_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t1431825778_0_0_0 = { 1, GenInst_Axis_t1431825778_0_0_0_Types };
extern const Il2CppType Constraint_t3558160636_0_0_0;
static const Il2CppType* GenInst_Constraint_t3558160636_0_0_0_Types[] = { &Constraint_t3558160636_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t3558160636_0_0_0 = { 1, GenInst_Constraint_t3558160636_0_0_0_Types };
extern const Il2CppType SubmitEvent_t907918422_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t907918422_0_0_0_Types[] = { &SubmitEvent_t907918422_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t907918422_0_0_0 = { 1, GenInst_SubmitEvent_t907918422_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t2863344003_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t2863344003_0_0_0_Types[] = { &OnChangeEvent_t2863344003_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2863344003_0_0_0 = { 1, GenInst_OnChangeEvent_t2863344003_0_0_0_Types };
extern const Il2CppType OnValidateInput_t1946318473_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t1946318473_0_0_0_Types[] = { &OnValidateInput_t1946318473_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1946318473_0_0_0 = { 1, GenInst_OnValidateInput_t1946318473_0_0_0_Types };
extern const Il2CppType LayoutElement_t2808691390_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t2808691390_0_0_0_Types[] = { &LayoutElement_t2808691390_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t2808691390_0_0_0 = { 1, GenInst_LayoutElement_t2808691390_0_0_0_Types };
extern const Il2CppType TextAnchor_t112990806_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t112990806_0_0_0_Types[] = { &TextAnchor_t112990806_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t112990806_0_0_0 = { 1, GenInst_TextAnchor_t112990806_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t3244928895_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t3244928895_0_0_0_Types[] = { &AnimationTriggers_t3244928895_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t3244928895_0_0_0 = { 1, GenInst_AnimationTriggers_t3244928895_0_0_0_Types };
extern const Il2CppType Animator_t69676727_0_0_0;
static const Il2CppType* GenInst_Animator_t69676727_0_0_0_Types[] = { &Animator_t69676727_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t69676727_0_0_0 = { 1, GenInst_Animator_t69676727_0_0_0_Types };
extern const Il2CppType Rigidbody_t4233889191_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t4233889191_0_0_0_Types[] = { &Rigidbody_t4233889191_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t4233889191_0_0_0 = { 1, GenInst_Rigidbody_t4233889191_0_0_0_Types };
extern const Il2CppType GUITexture_t1909122990_0_0_0;
static const Il2CppType* GenInst_GUITexture_t1909122990_0_0_0_Types[] = { &GUITexture_t1909122990_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t1909122990_0_0_0 = { 1, GenInst_GUITexture_t1909122990_0_0_0_Types };
extern const Il2CppType GUIText_t2411476300_0_0_0;
static const Il2CppType* GenInst_GUIText_t2411476300_0_0_0_Types[] = { &GUIText_t2411476300_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIText_t2411476300_0_0_0 = { 1, GenInst_GUIText_t2411476300_0_0_0_Types };
extern const Il2CppType Light_t494725636_0_0_0;
static const Il2CppType* GenInst_Light_t494725636_0_0_0_Types[] = { &Light_t494725636_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t494725636_0_0_0 = { 1, GenInst_Light_t494725636_0_0_0_Types };
extern const Il2CppType iTween_t488923914_0_0_0;
static const Il2CppType* GenInst_iTween_t488923914_0_0_0_Types[] = { &iTween_t488923914_0_0_0 };
extern const Il2CppGenericInst GenInst_iTween_t488923914_0_0_0 = { 1, GenInst_iTween_t488923914_0_0_0_Types };
extern const Il2CppType CFX_Demo_Translate_t3218432890_0_0_0;
static const Il2CppType* GenInst_CFX_Demo_Translate_t3218432890_0_0_0_Types[] = { &CFX_Demo_Translate_t3218432890_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX_Demo_Translate_t3218432890_0_0_0 = { 1, GenInst_CFX_Demo_Translate_t3218432890_0_0_0_Types };
extern const Il2CppType Collider_t3497673348_0_0_0;
static const Il2CppType* GenInst_Collider_t3497673348_0_0_0_Types[] = { &Collider_t3497673348_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t3497673348_0_0_0 = { 1, GenInst_Collider_t3497673348_0_0_0_Types };
extern const Il2CppType CFX_AutoStopLoopedEffect_t3214280257_0_0_0;
static const Il2CppType* GenInst_CFX_AutoStopLoopedEffect_t3214280257_0_0_0_Types[] = { &CFX_AutoStopLoopedEffect_t3214280257_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX_AutoStopLoopedEffect_t3214280257_0_0_0 = { 1, GenInst_CFX_AutoStopLoopedEffect_t3214280257_0_0_0_Types };
extern const Il2CppType SortingGroup_t3832012067_0_0_0;
static const Il2CppType* GenInst_SortingGroup_t3832012067_0_0_0_Types[] = { &SortingGroup_t3832012067_0_0_0 };
extern const Il2CppGenericInst GenInst_SortingGroup_t3832012067_0_0_0 = { 1, GenInst_SortingGroup_t3832012067_0_0_0_Types };
extern const Il2CppType ColliderEventSystem_t795033329_0_0_0;
static const Il2CppType* GenInst_ColliderEventSystem_t795033329_0_0_0_Types[] = { &ColliderEventSystem_t795033329_0_0_0 };
extern const Il2CppGenericInst GenInst_ColliderEventSystem_t795033329_0_0_0 = { 1, GenInst_ColliderEventSystem_t795033329_0_0_0_Types };
extern const Il2CppType TrailRenderer_t2490637367_0_0_0;
static const Il2CppType* GenInst_TrailRenderer_t2490637367_0_0_0_Types[] = { &TrailRenderer_t2490637367_0_0_0 };
extern const Il2CppGenericInst GenInst_TrailRenderer_t2490637367_0_0_0 = { 1, GenInst_TrailRenderer_t2490637367_0_0_0_Types };
static const Il2CppType* GenInst_Collider2D_t646061738_0_0_0_Types[] = { &Collider2D_t646061738_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider2D_t646061738_0_0_0 = { 1, GenInst_Collider2D_t646061738_0_0_0_Types };
extern const Il2CppType MouseEventSystem_t3454497406_0_0_0;
static const Il2CppType* GenInst_MouseEventSystem_t3454497406_0_0_0_Types[] = { &MouseEventSystem_t3454497406_0_0_0 };
extern const Il2CppGenericInst GenInst_MouseEventSystem_t3454497406_0_0_0 = { 1, GenInst_MouseEventSystem_t3454497406_0_0_0_Types };
extern const Il2CppType FlappyChar_t3600499100_0_0_0;
static const Il2CppType* GenInst_FlappyChar_t3600499100_0_0_0_Types[] = { &FlappyChar_t3600499100_0_0_0 };
extern const Il2CppGenericInst GenInst_FlappyChar_t3600499100_0_0_0 = { 1, GenInst_FlappyChar_t3600499100_0_0_0_Types };
extern const Il2CppType RedundantFileWriter_t3761749544_0_0_0;
static const Il2CppType* GenInst_RedundantFileWriter_t3761749544_0_0_0_Types[] = { &RedundantFileWriter_t3761749544_0_0_0 };
extern const Il2CppGenericInst GenInst_RedundantFileWriter_t3761749544_0_0_0 = { 1, GenInst_RedundantFileWriter_t3761749544_0_0_0_Types };
extern const Il2CppType KeyboardEventSystem_t2538007188_0_0_0;
static const Il2CppType* GenInst_KeyboardEventSystem_t2538007188_0_0_0_Types[] = { &KeyboardEventSystem_t2538007188_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyboardEventSystem_t2538007188_0_0_0 = { 1, GenInst_KeyboardEventSystem_t2538007188_0_0_0_Types };
extern const Il2CppType MouseEventProccessor_t442073356_0_0_0;
static const Il2CppType* GenInst_MouseEventProccessor_t442073356_0_0_0_Types[] = { &MouseEventProccessor_t442073356_0_0_0 };
extern const Il2CppGenericInst GenInst_MouseEventProccessor_t442073356_0_0_0 = { 1, GenInst_MouseEventProccessor_t442073356_0_0_0_Types };
extern const Il2CppType BoxCollider2D_t948534547_0_0_0;
static const Il2CppType* GenInst_BoxCollider2D_t948534547_0_0_0_Types[] = { &BoxCollider2D_t948534547_0_0_0 };
extern const Il2CppGenericInst GenInst_BoxCollider2D_t948534547_0_0_0 = { 1, GenInst_BoxCollider2D_t948534547_0_0_0_Types };
extern const Il2CppType GridRep_t2017325921_0_0_0;
static const Il2CppType* GenInst_GridRep_t2017325921_0_0_0_Types[] = { &GridRep_t2017325921_0_0_0 };
extern const Il2CppGenericInst GenInst_GridRep_t2017325921_0_0_0 = { 1, GenInst_GridRep_t2017325921_0_0_0_Types };
extern const Il2CppType SoundManagerEventProccessor_t3825530019_0_0_0;
static const Il2CppType* GenInst_SoundManagerEventProccessor_t3825530019_0_0_0_Types[] = { &SoundManagerEventProccessor_t3825530019_0_0_0 };
extern const Il2CppGenericInst GenInst_SoundManagerEventProccessor_t3825530019_0_0_0 = { 1, GenInst_SoundManagerEventProccessor_t3825530019_0_0_0_Types };
extern const Il2CppType PuzzlePiece_t1912707670_0_0_0;
static const Il2CppType* GenInst_PuzzlePiece_t1912707670_0_0_0_Types[] = { &PuzzlePiece_t1912707670_0_0_0 };
extern const Il2CppGenericInst GenInst_PuzzlePiece_t1912707670_0_0_0 = { 1, GenInst_PuzzlePiece_t1912707670_0_0_0_Types };
extern const Il2CppType Cleaner_t1604682602_0_0_0;
static const Il2CppType* GenInst_Cleaner_t1604682602_0_0_0_Types[] = { &Cleaner_t1604682602_0_0_0 };
extern const Il2CppGenericInst GenInst_Cleaner_t1604682602_0_0_0 = { 1, GenInst_Cleaner_t1604682602_0_0_0_Types };
extern const Il2CppType Pencil_t4115486279_0_0_0;
static const Il2CppType* GenInst_Pencil_t4115486279_0_0_0_Types[] = { &Pencil_t4115486279_0_0_0 };
extern const Il2CppGenericInst GenInst_Pencil_t4115486279_0_0_0 = { 1, GenInst_Pencil_t4115486279_0_0_0_Types };
extern const Il2CppType SongBubble_t2997910009_0_0_0;
static const Il2CppType* GenInst_SongBubble_t2997910009_0_0_0_Types[] = { &SongBubble_t2997910009_0_0_0 };
extern const Il2CppGenericInst GenInst_SongBubble_t2997910009_0_0_0 = { 1, GenInst_SongBubble_t2997910009_0_0_0_Types };
extern const Il2CppType PianoButton_t3912953253_0_0_0;
static const Il2CppType* GenInst_PianoButton_t3912953253_0_0_0_Types[] = { &PianoButton_t3912953253_0_0_0 };
extern const Il2CppGenericInst GenInst_PianoButton_t3912953253_0_0_0 = { 1, GenInst_PianoButton_t3912953253_0_0_0_Types };
extern const Il2CppType Note_t686167636_0_0_0;
static const Il2CppType* GenInst_Note_t686167636_0_0_0_Types[] = { &Note_t686167636_0_0_0 };
extern const Il2CppGenericInst GenInst_Note_t686167636_0_0_0 = { 1, GenInst_Note_t686167636_0_0_0_Types };
extern const Il2CppType MoveSnowflake_t721823583_0_0_0;
static const Il2CppType* GenInst_MoveSnowflake_t721823583_0_0_0_Types[] = { &MoveSnowflake_t721823583_0_0_0 };
extern const Il2CppGenericInst GenInst_MoveSnowflake_t721823583_0_0_0 = { 1, GenInst_MoveSnowflake_t721823583_0_0_0_Types };
extern const Il2CppType CircleCollider2D_t13116344_0_0_0;
static const Il2CppType* GenInst_CircleCollider2D_t13116344_0_0_0_Types[] = { &CircleCollider2D_t13116344_0_0_0 };
extern const Il2CppGenericInst GenInst_CircleCollider2D_t13116344_0_0_0 = { 1, GenInst_CircleCollider2D_t13116344_0_0_0_Types };
extern const Il2CppType PencilLine_t2072928649_0_0_0;
static const Il2CppType* GenInst_PencilLine_t2072928649_0_0_0_Types[] = { &PencilLine_t2072928649_0_0_0 };
extern const Il2CppGenericInst GenInst_PencilLine_t2072928649_0_0_0 = { 1, GenInst_PencilLine_t2072928649_0_0_0_Types };
extern const Il2CppType GiftSlot_t3101111170_0_0_0;
static const Il2CppType* GenInst_GiftSlot_t3101111170_0_0_0_Types[] = { &GiftSlot_t3101111170_0_0_0 };
extern const Il2CppGenericInst GenInst_GiftSlot_t3101111170_0_0_0 = { 1, GenInst_GiftSlot_t3101111170_0_0_0_Types };
extern const Il2CppType RingTossController_t956255555_0_0_0;
static const Il2CppType* GenInst_RingTossController_t956255555_0_0_0_Types[] = { &RingTossController_t956255555_0_0_0 };
extern const Il2CppGenericInst GenInst_RingTossController_t956255555_0_0_0 = { 1, GenInst_RingTossController_t956255555_0_0_0_Types };
extern const Il2CppType DotPencil_t1876295642_0_0_0;
static const Il2CppType* GenInst_DotPencil_t1876295642_0_0_0_Types[] = { &DotPencil_t1876295642_0_0_0 };
extern const Il2CppGenericInst GenInst_DotPencil_t1876295642_0_0_0 = { 1, GenInst_DotPencil_t1876295642_0_0_0_Types };
extern const Il2CppType ItemClickProcessor_t208213687_0_0_0;
static const Il2CppType* GenInst_ItemClickProcessor_t208213687_0_0_0_Types[] = { &ItemClickProcessor_t208213687_0_0_0 };
extern const Il2CppGenericInst GenInst_ItemClickProcessor_t208213687_0_0_0 = { 1, GenInst_ItemClickProcessor_t208213687_0_0_0_Types };
extern const Il2CppType DummyMouseEventImplementation_t4020916073_0_0_0;
static const Il2CppType* GenInst_DummyMouseEventImplementation_t4020916073_0_0_0_Types[] = { &DummyMouseEventImplementation_t4020916073_0_0_0 };
extern const Il2CppGenericInst GenInst_DummyMouseEventImplementation_t4020916073_0_0_0 = { 1, GenInst_DummyMouseEventImplementation_t4020916073_0_0_0_Types };
extern const Il2CppType Yey_t2293551375_0_0_0;
static const Il2CppType* GenInst_Yey_t2293551375_0_0_0_Types[] = { &Yey_t2293551375_0_0_0 };
extern const Il2CppGenericInst GenInst_Yey_t2293551375_0_0_0 = { 1, GenInst_Yey_t2293551375_0_0_0_Types };
extern const Il2CppType Spawner_t534830648_0_0_0;
static const Il2CppType* GenInst_Spawner_t534830648_0_0_0_Types[] = { &Spawner_t534830648_0_0_0 };
extern const Il2CppGenericInst GenInst_Spawner_t534830648_0_0_0 = { 1, GenInst_Spawner_t534830648_0_0_0_Types };
extern const Il2CppType ObjectPool_t2689037807_0_0_0;
static const Il2CppType* GenInst_ObjectPool_t2689037807_0_0_0_Types[] = { &ObjectPool_t2689037807_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_t2689037807_0_0_0 = { 1, GenInst_ObjectPool_t2689037807_0_0_0_Types };
extern const Il2CppType Bird_t1163357383_0_0_0;
static const Il2CppType* GenInst_Bird_t1163357383_0_0_0_Types[] = { &Bird_t1163357383_0_0_0 };
extern const Il2CppGenericInst GenInst_Bird_t1163357383_0_0_0 = { 1, GenInst_Bird_t1163357383_0_0_0_Types };
extern const Il2CppType RepeatingBackground_t2924144931_0_0_0;
static const Il2CppType* GenInst_RepeatingBackground_t2924144931_0_0_0_Types[] = { &RepeatingBackground_t2924144931_0_0_0 };
extern const Il2CppGenericInst GenInst_RepeatingBackground_t2924144931_0_0_0 = { 1, GenInst_RepeatingBackground_t2924144931_0_0_0_Types };
extern const Il2CppType Building_t2809767176_0_0_0;
static const Il2CppType* GenInst_Building_t2809767176_0_0_0_Types[] = { &Building_t2809767176_0_0_0 };
extern const Il2CppGenericInst GenInst_Building_t2809767176_0_0_0 = { 1, GenInst_Building_t2809767176_0_0_0_Types };
extern const Il2CppType DefaultRandomRule_t4265547936_0_0_0;
static const Il2CppType* GenInst_DefaultRandomRule_t4265547936_0_0_0_Types[] = { &DefaultRandomRule_t4265547936_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultRandomRule_t4265547936_0_0_0 = { 1, GenInst_DefaultRandomRule_t4265547936_0_0_0_Types };
extern const Il2CppType FlashingObject_t2247968655_0_0_0;
static const Il2CppType* GenInst_FlashingObject_t2247968655_0_0_0_Types[] = { &FlashingObject_t2247968655_0_0_0 };
extern const Il2CppGenericInst GenInst_FlashingObject_t2247968655_0_0_0 = { 1, GenInst_FlashingObject_t2247968655_0_0_0_Types };
extern const Il2CppType GiftSpawnPoint_t604323357_0_0_0;
static const Il2CppType* GenInst_GiftSpawnPoint_t604323357_0_0_0_Types[] = { &GiftSpawnPoint_t604323357_0_0_0 };
extern const Il2CppGenericInst GenInst_GiftSpawnPoint_t604323357_0_0_0 = { 1, GenInst_GiftSpawnPoint_t604323357_0_0_0_Types };
extern const Il2CppType Slide_t3421086917_0_0_0;
static const Il2CppType* GenInst_Slide_t3421086917_0_0_0_Types[] = { &Slide_t3421086917_0_0_0 };
extern const Il2CppGenericInst GenInst_Slide_t3421086917_0_0_0 = { 1, GenInst_Slide_t3421086917_0_0_0_Types };
extern const Il2CppType DestroySound_t724042623_0_0_0;
static const Il2CppType* GenInst_DestroySound_t724042623_0_0_0_Types[] = { &DestroySound_t724042623_0_0_0 };
extern const Il2CppGenericInst GenInst_DestroySound_t724042623_0_0_0 = { 1, GenInst_DestroySound_t724042623_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Vector3_t2243707580_0_0_0, &Il2CppObject_0_0_0, &PathOptions_t2659884781_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0_Types };
static const Il2CppType* GenInst_GiftObject_t2144675780_0_0_0_GiftObject_t2144675780_0_0_0_Types[] = { &GiftObject_t2144675780_0_0_0, &GiftObject_t2144675780_0_0_0 };
extern const Il2CppGenericInst GenInst_GiftObject_t2144675780_0_0_0_GiftObject_t2144675780_0_0_0 = { 2, GenInst_GiftObject_t2144675780_0_0_0_GiftObject_t2144675780_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0, &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0, &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const Il2CppType* GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0_Types[] = { &AnimatorClipInfo_t3905751349_0_0_0, &AnimatorClipInfo_t3905751349_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0 = { 2, GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0_Types };
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_Types[] = { &Color_t2020392075_0_0_0, &Color_t2020392075_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0 = { 2, GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0, &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0 = { 2, GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0, &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0 = { 2, GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0, &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0 = { 2, GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0, &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0 = { 2, GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0, &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0 = { 2, GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0, &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0 = { 2, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types };
static const Il2CppType* GenInst_ScaleOn_t3059270671_0_0_0_ScaleOn_t3059270671_0_0_0_Types[] = { &ScaleOn_t3059270671_0_0_0, &ScaleOn_t3059270671_0_0_0 };
extern const Il2CppGenericInst GenInst_ScaleOn_t3059270671_0_0_0_ScaleOn_t3059270671_0_0_0 = { 2, GenInst_ScaleOn_t3059270671_0_0_0_ScaleOn_t3059270671_0_0_0_Types };
static const Il2CppType* GenInst_ScaleOn_t3059270671_0_0_0_Il2CppObject_0_0_0_Types[] = { &ScaleOn_t3059270671_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ScaleOn_t3059270671_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ScaleOn_t3059270671_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1258057682_0_0_0_KeyValuePair_2_t1258057682_0_0_0_Types[] = { &KeyValuePair_2_t1258057682_0_0_0, &KeyValuePair_2_t1258057682_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1258057682_0_0_0_KeyValuePair_2_t1258057682_0_0_0 = { 2, GenInst_KeyValuePair_2_t1258057682_0_0_0_KeyValuePair_2_t1258057682_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1258057682_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1258057682_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1258057682_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1258057682_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3720882578_0_0_0_KeyValuePair_2_t3720882578_0_0_0_Types[] = { &KeyValuePair_2_t3720882578_0_0_0, &KeyValuePair_2_t3720882578_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3720882578_0_0_0_KeyValuePair_2_t3720882578_0_0_0 = { 2, GenInst_KeyValuePair_2_t3720882578_0_0_0_KeyValuePair_2_t3720882578_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3720882578_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3720882578_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3720882578_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3720882578_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t901022143_0_0_0_KeyValuePair_2_t901022143_0_0_0_Types[] = { &KeyValuePair_2_t901022143_0_0_0, &KeyValuePair_2_t901022143_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t901022143_0_0_0_KeyValuePair_2_t901022143_0_0_0 = { 2, GenInst_KeyValuePair_2_t901022143_0_0_0_KeyValuePair_2_t901022143_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t901022143_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t901022143_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t901022143_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t901022143_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyCode_t2283395152_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3653809873_0_0_0_KeyValuePair_2_t3653809873_0_0_0_Types[] = { &KeyValuePair_2_t3653809873_0_0_0, &KeyValuePair_2_t3653809873_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653809873_0_0_0_KeyValuePair_2_t3653809873_0_0_0 = { 2, GenInst_KeyValuePair_2_t3653809873_0_0_0_KeyValuePair_2_t3653809873_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3653809873_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3653809873_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653809873_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3653809873_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType SemiRandomRule_t1463926147_0_0_0;
static const Il2CppType* GenInst_SemiRandomRule_t1463926147_0_0_0_Types[] = { &SemiRandomRule_t1463926147_0_0_0 };
extern const Il2CppGenericInst GenInst_SemiRandomRule_t1463926147_0_0_0 = { 1, GenInst_SemiRandomRule_t1463926147_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[909] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0,
	&GenInst_Char_t3454481338_0_0_0,
	&GenInst_Int64_t909078037_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0,
	&GenInst_SByte_t454417549_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t908092482_0_0_0,
	&GenInst_IComparable_t1857082765_0_0_0,
	&GenInst_IEnumerable_t2911409499_0_0_0,
	&GenInst_ICloneable_t3853279282_0_0_0,
	&GenInst_IComparable_1_t3861059456_0_0_0,
	&GenInst_IEquatable_1_t4233202402_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3412036974_0_0_0,
	&GenInst__Type_t102776839_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0,
	&GenInst__MemberInfo_t332722161_0_0_0,
	&GenInst_Double_t4078015681_0_0_0,
	&GenInst_Single_t2076509932_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0,
	&GenInst_Delegate_t3022476291_0_0_0,
	&GenInst_ISerializable_t1245643778_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0,
	&GenInst__ParameterInfo_t470209990_0_0_0,
	&GenInst_ParameterModifier_t1820634920_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2511231167_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3642518830_0_0_0,
	&GenInst_MethodBase_t904190842_0_0_0,
	&GenInst__MethodBase_t1935530873_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0,
	&GenInst__ConstructorInfo_t3269099341_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t2011406615_0_0_0,
	&GenInst_TailoringInfo_t1449609243_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_Link_t2723257478_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_Contraction_t1673853792_0_0_0,
	&GenInst_Level2Map_t3322505726_0_0_0,
	&GenInst_BigInteger_t925946152_0_0_0,
	&GenInst_KeySizes_t3144736271_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Slot_t2022531261_0_0_0,
	&GenInst_Slot_t2267560602_0_0_0,
	&GenInst_StackFrame_t2050294881_0_0_0,
	&GenInst_Calendar_t585061108_0_0_0,
	&GenInst_ModuleBuilder_t4156028127_0_0_0,
	&GenInst__ModuleBuilder_t1075102050_0_0_0,
	&GenInst_Module_t4282841206_0_0_0,
	&GenInst__Module_t2144668161_0_0_0,
	&GenInst_ParameterBuilder_t3344728474_0_0_0,
	&GenInst__ParameterBuilder_t2251638747_0_0_0,
	&GenInst_TypeU5BU5D_t1664964607_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t91669223_0_0_0,
	&GenInst_IList_t3321498491_0_0_0,
	&GenInst_IList_1_t1844743827_0_0_0,
	&GenInst_ICollection_1_t2255878531_0_0_0,
	&GenInst_IEnumerable_1_t1595930271_0_0_0,
	&GenInst_IList_1_t3952977575_0_0_0,
	&GenInst_ICollection_1_t69144983_0_0_0,
	&GenInst_IEnumerable_1_t3704164019_0_0_0,
	&GenInst_IList_1_t643717440_0_0_0,
	&GenInst_ICollection_1_t1054852144_0_0_0,
	&GenInst_IEnumerable_1_t394903884_0_0_0,
	&GenInst_IList_1_t289070565_0_0_0,
	&GenInst_ICollection_1_t700205269_0_0_0,
	&GenInst_IEnumerable_1_t40257009_0_0_0,
	&GenInst_IList_1_t1043143288_0_0_0,
	&GenInst_ICollection_1_t1454277992_0_0_0,
	&GenInst_IEnumerable_1_t794329732_0_0_0,
	&GenInst_IList_1_t873662762_0_0_0,
	&GenInst_ICollection_1_t1284797466_0_0_0,
	&GenInst_IEnumerable_1_t624849206_0_0_0,
	&GenInst_IList_1_t3230389896_0_0_0,
	&GenInst_ICollection_1_t3641524600_0_0_0,
	&GenInst_IEnumerable_1_t2981576340_0_0_0,
	&GenInst_ILTokenInfo_t149559338_0_0_0,
	&GenInst_LabelData_t3712112744_0_0_0,
	&GenInst_LabelFixup_t4090909514_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0,
	&GenInst_TypeBuilder_t3308873219_0_0_0,
	&GenInst__TypeBuilder_t2783404358_0_0_0,
	&GenInst_MethodBuilder_t644187984_0_0_0,
	&GenInst__MethodBuilder_t3932949077_0_0_0,
	&GenInst_ConstructorBuilder_t700974433_0_0_0,
	&GenInst__ConstructorBuilder_t1236878896_0_0_0,
	&GenInst_FieldBuilder_t2784804005_0_0_0,
	&GenInst__FieldBuilder_t1895266044_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1567586598_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeData_t3093286891_0_0_0,
	&GenInst_ResourceInfo_t3933049236_0_0_0,
	&GenInst_ResourceCacheItem_t333236149_0_0_0,
	&GenInst_IContextProperty_t287246399_0_0_0,
	&GenInst_Header_t2756440555_0_0_0,
	&GenInst_ITrackingHandler_t2759960940_0_0_0,
	&GenInst_IContextAttribute_t2439121372_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0,
	&GenInst_TypeTag_t141209596_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2988747270_0_0_0,
	&GenInst_IBuiltInEvidence_t1114073477_0_0_0,
	&GenInst_IIdentityPermissionFactory_t2988326850_0_0_0,
	&GenInst_DateTimeOffset_t1362988906_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t1755874712_0_0_0,
	&GenInst_BigInteger_t925946153_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_IList_1_t4224045037_0_0_0,
	&GenInst_ICollection_1_t340212445_0_0_0,
	&GenInst_IEnumerable_1_t3975231481_0_0_0,
	&GenInst_X509Certificate_t283079845_0_0_0,
	&GenInst_IDeserializationCallback_t327125377_0_0_0,
	&GenInst_ClientCertificateType_t4001384466_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_X509ChainStatus_t4278378721_0_0_0,
	&GenInst_Capture_t4157900610_0_0_0,
	&GenInst_Group_t3761430853_0_0_0,
	&GenInst_Mark_t2724874473_0_0_0,
	&GenInst_UriScheme_t1876590943_0_0_0,
	&GenInst_Link_t865133271_0_0_0,
	&GenInst_Object_t1021602117_0_0_0,
	&GenInst_Camera_t189460977_0_0_0,
	&GenInst_Behaviour_t955675639_0_0_0,
	&GenInst_Component_t3819376471_0_0_0,
	&GenInst_Display_t3666191348_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AchievementDescription_t3110978151_0_0_0,
	&GenInst_IAchievementDescription_t3498529102_0_0_0,
	&GenInst_UserProfile_t3365630962_0_0_0,
	&GenInst_IUserProfile_t4108565527_0_0_0,
	&GenInst_GcLeaderboard_t453887929_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0,
	&GenInst_IAchievementU5BU5D_t2709554645_0_0_0,
	&GenInst_IAchievement_t1752291260_0_0_0,
	&GenInst_GcAchievementData_t1754866149_0_0_0,
	&GenInst_Achievement_t1333316625_0_0_0,
	&GenInst_IScoreU5BU5D_t3237304636_0_0_0,
	&GenInst_IScore_t513966369_0_0_0,
	&GenInst_GcScoreData_t3676783238_0_0_0,
	&GenInst_Score_t2307748940_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3461248430_0_0_0,
	&GenInst_Material_t193706927_0_0_0,
	&GenInst_Touch_t407273883_0_0_0,
	&GenInst_Keyframe_t1449471340_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0,
	&GenInst_Color32_t874517518_0_0_0,
	&GenInst_Color_t2020392075_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0,
	&GenInst_ContactPoint_t1376425630_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0,
	&GenInst_Rigidbody2D_t502193897_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_ContactPoint2D_t3659330976_0_0_0,
	&GenInst_AnimatorClipInfo_t3905751349_0_0_0,
	&GenInst_AnimatorControllerParameter_t1381019216_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0,
	&GenInst_Font_t4239498691_0_0_0,
	&GenInst_GUILayoutOption_t4183744904_0_0_0,
	&GenInst_GUILayoutEntry_t3828586629_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_Event_t3028476042_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t3799506081_0_0_0,
	&GenInst_KeyValuePair_2_t3799506081_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_DisallowMultipleComponent_t2656950_0_0_0,
	&GenInst_Attribute_t542643598_0_0_0,
	&GenInst__Attribute_t1557664299_0_0_0,
	&GenInst_ExecuteInEditMode_t3043633143_0_0_0,
	&GenInst_RequireComponent_t864575032_0_0_0,
	&GenInst_HitInfo_t1761367055_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PersistentCall_t3793436469_0_0_0,
	&GenInst_BaseInvokableCall_t2229564840_0_0_0,
	&GenInst_MessageTypeSubscribers_t2291506050_0_0_0,
	&GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_MessageEventArgs_t301283622_0_0_0,
	&GenInst_TweenCallback_t3697142134_0_0_0,
	&GenInst_LogBehaviour_t3505725029_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_FloatOptions_t1421548266_0_0_0,
	&GenInst_Quaternion_t4030073918_0_0_0_Vector3_t2243707580_0_0_0_QuaternionOptions_t466049668_0_0_0,
	&GenInst_Quaternion_t4030073918_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3U5BU5D_t1172311765_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0,
	&GenInst_Tween_t278478013_0_0_0,
	&GenInst_ABSSequentiable_t2284140720_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_StringOptions_t2885323933_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_StringOptions_t2885323933_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_VectorOptions_t293385261_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_VectorOptions_t293385261_0_0_0,
	&GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_ColorOptions_t2213017305_0_0_0,
	&GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0_RectOptions_t3393635162_0_0_0,
	&GenInst_Rect_t3681755626_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0,
	&GenInst_Color2_t232726623_0_0_0_Color2_t232726623_0_0_0_ColorOptions_t2213017305_0_0_0,
	&GenInst_Color2_t232726623_0_0_0,
	&GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0,
	&GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_KeyValuePair_2_t2686133794_0_0_0,
	&GenInst_KeyValuePair_2_t2686133794_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0_RectOffset_t3387826427_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0_UInt32_t2149682021_0_0_0_UintOptions_t2267095136_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_VectorOptions_t293385261_0_0_0,
	&GenInst_ControlPoint_t168081159_0_0_0,
	&GenInst_BaseInputModule_t1295781545_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0,
	&GenInst_IDeselectHandler_t3182198310_0_0_0,
	&GenInst_IEventSystemHandler_t2741188318_0_0_0,
	&GenInst_List_1_t2110309450_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0,
	&GenInst_List_1_t3188497603_0_0_0,
	&GenInst_ISelectHandler_t2812555161_0_0_0,
	&GenInst_BaseRaycaster_t2336171397_0_0_0,
	&GenInst_Entry_t3365010046_0_0_0,
	&GenInst_BaseEventData_t2681005625_0_0_0,
	&GenInst_IPointerEnterHandler_t193164956_0_0_0,
	&GenInst_IPointerExitHandler_t461019860_0_0_0,
	&GenInst_IPointerDownHandler_t3929046918_0_0_0,
	&GenInst_IPointerUpHandler_t1847764461_0_0_0,
	&GenInst_IPointerClickHandler_t96169666_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0,
	&GenInst_IBeginDragHandler_t3135127860_0_0_0,
	&GenInst_IDragHandler_t2583993319_0_0_0,
	&GenInst_IEndDragHandler_t1349123600_0_0_0,
	&GenInst_IDropHandler_t2390101210_0_0_0,
	&GenInst_IScrollHandler_t3834677510_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3778909353_0_0_0,
	&GenInst_IMoveHandler_t2611925506_0_0_0,
	&GenInst_ISubmitHandler_t525803901_0_0_0,
	&GenInst_ICancelHandler_t1980319651_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0,
	&GenInst_BaseInput_t621514313_0_0_0,
	&GenInst_UIBehaviour_t3960014691_0_0_0,
	&GenInst_MonoBehaviour_t1158329972_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_ButtonState_t2688375492_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ColorBlock_t2652774230_0_0_0,
	&GenInst_OptionData_t2420267500_0_0_0,
	&GenInst_DropdownItem_t4139978805_0_0_0,
	&GenInst_FloatTween_t2986189219_0_0_0,
	&GenInst_Sprite_t309593783_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0,
	&GenInst_List_1_t3873494194_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0,
	&GenInst_Text_t356221433_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_ColorTween_t3438117476_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_Type_t3352948571_0_0_0,
	&GenInst_FillMethod_t1640962579_0_0_0,
	&GenInst_ContentType_t1028629049_0_0_0,
	&GenInst_LineType_t2931319356_0_0_0,
	&GenInst_InputType_t1274231802_0_0_0,
	&GenInst_TouchScreenKeyboardType_t875112366_0_0_0,
	&GenInst_CharacterValidation_t3437478890_0_0_0,
	&GenInst_Mask_t2977958238_0_0_0,
	&GenInst_List_1_t2347079370_0_0_0,
	&GenInst_RectMask2D_t1156185964_0_0_0,
	&GenInst_List_1_t525307096_0_0_0,
	&GenInst_Navigation_t1571958496_0_0_0,
	&GenInst_IClippable_t1941276057_0_0_0,
	&GenInst_Direction_t3696775921_0_0_0,
	&GenInst_Selectable_t1490392188_0_0_0,
	&GenInst_Transition_t605142169_0_0_0,
	&GenInst_SpriteState_t1353336012_0_0_0,
	&GenInst_CanvasGroup_t3296560743_0_0_0,
	&GenInst_Direction_t1525323322_0_0_0,
	&GenInst_MatEntry_t3157325053_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_AspectMode_t1166448724_0_0_0,
	&GenInst_FitMode_t4030874534_0_0_0,
	&GenInst_RectTransform_t3349966182_0_0_0,
	&GenInst_LayoutRebuilder_t2155218138_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_List_1_t1612828712_0_0_0,
	&GenInst_List_1_t243638650_0_0_0,
	&GenInst_List_1_t1612828711_0_0_0,
	&GenInst_List_1_t1612828713_0_0_0,
	&GenInst_List_1_t1440998580_0_0_0,
	&GenInst_List_1_t573379950_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_FieldWithTarget_t2256174789_0_0_0,
	&GenInst_Hashtable_t909839986_0_0_0,
	&GenInst_GameOneManager_t2648179911_0_0_0,
	&GenInst_SoundManager_t654432262_0_0_0,
	&GenInst_NavigationManager_t4055369543_0_0_0,
	&GenInst_GameTenManager_t1577134516_0_0_0,
	&GenInst_SpriteRenderer_t1209076198_0_0_0,
	&GenInst_Renderer_t257310565_0_0_0,
	&GenInst_GameElevenManager_t2036900502_0_0_0,
	&GenInst_GameTwelveManager_t2786305482_0_0_0,
	&GenInst_GameThirteenManager_t3231626088_0_0_0,
	&GenInst_SnowManMatch_t3305190334_0_0_0,
	&GenInst_MemoryGame_t4018179393_0_0_0,
	&GenInst_MemoryGamePiece_t2964149615_0_0_0,
	&GenInst_GameFifteenManager_t1527188406_0_0_0,
	&GenInst_LightColors_t1169232587_0_0_0,
	&GenInst_LightPuzzle_t891176437_0_0_0,
	&GenInst_LightCompleted_t2236120434_0_0_0,
	&GenInst_LightBulb_t3998556073_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_GameSixteenManager_t358793919_0_0_0,
	&GenInst_Game17Manager_t3515664785_0_0_0,
	&GenInst_Difference_t980400781_0_0_0,
	&GenInst_DifferenceCollection_t1428269411_0_0_0,
	&GenInst_GameEighteenManager_t3662215866_0_0_0,
	&GenInst_GameRounds_t1482069856_0_0_0,
	&GenInst_SoundClip_t2598932235_0_0_0,
	&GenInst_GameNineteenManager_t942308577_0_0_0,
	&GenInst_Song_t1848967459_0_0_0,
	&GenInst_GameTwoManager_t1572221819_0_0_0,
	&GenInst_TransformGrid_t2312775536_0_0_0,
	&GenInst_GameTwentyManager_t1246478874_0_0_0,
	&GenInst_GameTwentyoneManager_t426707788_0_0_0,
	&GenInst_GiftObject_t2144675780_0_0_0,
	&GenInst_GameTwentytwoManager_t3731809278_0_0_0,
	&GenInst_DraggableOrnament_t3124044711_0_0_0,
	&GenInst_ClickObject_t2003887325_0_0_0,
	&GenInst_GiftSpawner_t1625374518_0_0_0,
	&GenInst_Game24Manager_t283638021_0_0_0,
	&GenInst_Gift_t450896550_0_0_0,
	&GenInst_ObstacleBase_t365467716_0_0_0,
	&GenInst_GameThreeManager_t1469036085_0_0_0,
	&GenInst_HatEnabler_t1910487184_0_0_0,
	&GenInst_PlatformEffector2D_t3299381139_0_0_0,
	&GenInst_Effector2D_t2551982566_0_0_0,
	&GenInst_GameFourManager_t2429316785_0_0_0,
	&GenInst_GameFiveManager_t3955517211_0_0_0,
	&GenInst_GameSixManager_t2271637217_0_0_0,
	&GenInst_GameSevenManager_t3596770590_0_0_0,
	&GenInst_SnowMan_t902289717_0_0_0,
	&GenInst_GameEightManager_t903757396_0_0_0,
	&GenInst_BackgroundsHolder_t4153806707_0_0_0,
	&GenInst_StartingSlot_t3631029828_0_0_0,
	&GenInst_GameNineManager_t3632946731_0_0_0,
	&GenInst_AudioClip_t1932558630_0_0_0,
	&GenInst_DOTweenAnimation_t858634588_0_0_0,
	&GenInst_ABSAnimationComponent_t2205594551_0_0_0,
	&GenInst_RocketChar_t1772935162_0_0_0_Collider2D_t646061738_0_0_0,
	&GenInst_RocketChar_t1772935162_0_0_0_Collider2D_t646061738_0_0_0_Collision2D_t1539500754_0_0_0,
	&GenInst_MandatoryDummyImplmplementation_t1329551270_0_0_0,
	&GenInst_TwoDArray_1_t4208505166_0_0_0,
	&GenInst_ArbitraryComponent_t2637609733_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0,
	&GenInst_GridPoint_t3774252050_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_TwoDGameObjectArray_t1262067236_0_0_0,
	&GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0,
	&GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_KeyValuePair_2_t922534120_0_0_0,
	&GenInst_KeyValuePair_2_t922534120_0_0_0,
	&GenInst_DelayedWriteReference_t933455898_0_0_0,
	&GenInst_JSONNode_t1250409636_0_0_0,
	&GenInst_KeyValuePair_2_t922534120_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_String_t_0_0_0_JSONNode_t1250409636_0_0_0_String_t_0_0_0,
	&GenInst_ArbitraryEvent_t766134336_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t901022143_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_KeyCode_t2283395152_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t901022143_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0,
	&GenInst_KeyValuePair_2_t3653809873_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0_KeyCode_t2283395152_0_0_0_KeyValuePair_2_t3653809873_0_0_0,
	&GenInst_KeyValuePair_2_t3653809873_0_0_0_KeyCode_t2283395152_0_0_0,
	&GenInst_KeyValuePair_2_t3653809873_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IMouseEventSystem_t2547859815_0_0_0,
	&GenInst_AudibleSound_t2302054751_0_0_0,
	&GenInst_SoundClip_t3743947423_0_0_0,
	&GenInst_SoundLayer_t2188462586_0_0_0,
	&GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_KeyValuePair_2_t1258057682_0_0_0,
	&GenInst_ScaleOn_t3059270671_0_0_0,
	&GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_ScaleOn_t3059270671_0_0_0,
	&GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ScaleOn_t3059270671_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t1258057682_0_0_0,
	&GenInst_ObjectSequence_t1807435812_0_0_0,
	&GenInst_InputManager_t1610719423_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_KeyValuePair_2_t2213344945_0_0_0,
	&GenInst_KeyValuePair_2_t2213344945_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2844223813_0_0_0,
	&GenInst_KeyValuePair_2_t2844223813_0_0_0,
	&GenInst_StartupPool_t2587102281_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_List_1_t1125654279_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_List_1_t1125654279_0_0_0_List_1_t1125654279_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0,
	&GenInst_MaskableGraphic_t540192618_0_0_0,
	&GenInst_IMaskable_t1431842707_0_0_0,
	&GenInst_IMaterialModifier_t3028564983_0_0_0,
	&GenInst_AudioSource_t1135106623_0_0_0,
	&GenInst_String_t_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_KeyValuePair_2_t3720882578_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t3720882578_0_0_0,
	&GenInst_String_t_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t1748634416_0_0_0,
	&GenInst_KeyValuePair_2_t1748634416_0_0_0,
	&GenInst_ParticleSystem_t3394631041_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_List_1_t1125654279_0_0_0_KeyValuePair_2_t2185792432_0_0_0,
	&GenInst_KeyValuePair_2_t2185792432_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_CFX_AutoDestructShuriken_t1500114366_0_0_0,
	&GenInst_CFX_LightIntensityFade_t4221734619_0_0_0,
	&GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0,
	&GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0,
	&GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0,
	&GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0,
	&GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m52621935_gp_0_0_0_0,
	&GenInst_Array_Sort_m3546416104_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0,
	&GenInst_Array_compare_m940423571_gp_0_0_0_0,
	&GenInst_Array_qsort_m565008110_gp_0_0_0_0,
	&GenInst_Array_Resize_m1201602141_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0,
	&GenInst_Array_ForEach_m3775633118_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0,
	&GenInst_Array_FindAll_m982349212_gp_0_0_0_0,
	&GenInst_Array_Exists_m1825464757_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0,
	&GenInst_Array_Find_m2529971459_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3929249453_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0,
	&GenInst_IList_1_t3737699284_gp_0_0_0_0,
	&GenInst_ICollection_1_t1552160836_gp_0_0_0_0,
	&GenInst_Nullable_1_t1398937014_gp_0_0_0_0,
	&GenInst_Comparer_1_t1036860714_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3074655092_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0,
	&GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3434615342_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1766400012_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t4174120762_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0,
	&GenInst_Enumerator_t1292967705_gp_0_0_0_0,
	&GenInst_Collection_1_t686054069_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0,
	&GenInst_Stack_1_t4016656541_gp_0_0_0_0,
	&GenInst_Enumerator_t546412149_gp_0_0_0_0,
	&GenInst_HashSet_1_t2624254809_gp_0_0_0_0,
	&GenInst_Enumerator_t2109956843_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3424417428_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m665396702_gp_0_0_0_0,
	&GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0,
	&GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0,
	&GenInst_Enumerable_First_m1693250038_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_QuickSort_1_t1290221672_gp_0_0_0_0,
	&GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0,
	&GenInst_SortContext_1_t4088581714_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t476640868_gp_0_0_0_0,
	&GenInst_UnityAction_1_t2490859068_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0,
	&GenInst_DOTween_To_m3442317795_gp_0_0_0_0_DOTween_To_m3442317795_gp_1_0_0_0_DOTween_To_m3442317795_gp_2_0_0_0,
	&GenInst_DOTween_To_m3442317795_gp_0_0_0_0,
	&GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0,
	&GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_DOTween_ApplyTo_m4058900771_gp_1_0_0_0_DOTween_ApplyTo_m4058900771_gp_2_0_0_0,
	&GenInst_Tween_OnTweenCallback_m4208167374_gp_0_0_0_0,
	&GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Tweener_Setup_m1281370100_gp_1_0_0_0_Tweener_Setup_m1281370100_gp_2_0_0_0,
	&GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0,
	&GenInst_Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0,
	&GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Tweener_DoStartup_m3383447813_gp_1_0_0_0_Tweener_DoStartup_m3383447813_gp_2_0_0_0,
	&GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0,
	&GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0,
	&GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0,
	&GenInst_Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0,
	&GenInst_Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0,
	&GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_ABSTweenPlugin_3_t670396699_gp_1_0_0_0_ABSTweenPlugin_3_t670396699_gp_2_0_0_0,
	&GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0,
	&GenInst_PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0,
	&GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0,
	&GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0,
	&GenInst_Extensions_NoFrom_m2168138502_gp_0_0_0_0_Extensions_NoFrom_m2168138502_gp_1_0_0_0_Extensions_NoFrom_m2168138502_gp_2_0_0_0,
	&GenInst_TweenManager_GetTweener_m346233533_gp_0_0_0_0_TweenManager_GetTweener_m346233533_gp_1_0_0_0_TweenManager_GetTweener_m346233533_gp_2_0_0_0,
	&GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0,
	&GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_TweenerCore_3_t3922192912_gp_1_0_0_0_TweenerCore_3_t3922192912_gp_2_0_0_0,
	&GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ListPool_1_t1984115411_gp_0_0_0_0,
	&GenInst_List_1_t2000868992_0_0_0,
	&GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0,
	&GenInst_DDManagerBase_1_t2960862101_gp_0_0_0_0,
	&GenInst_ArrayUtils_randomizeList_m2699713364_gp_0_0_0_0,
	&GenInst_ArrayUtils_randomizeListSameOrder_m3179219044_gp_0_0_0_0,
	&GenInst_ArrayUtils_printArray_m1474984333_gp_0_0_0_0,
	&GenInst_ArrayUtils_printList_m3464464492_gp_0_0_0_0,
	&GenInst_ArrayUtils_compareArrays_m4204017686_gp_0_0_0_0,
	&GenInst_ArrayUtils_compareOrderedArrays_m2308127077_gp_0_0_0_0,
	&GenInst_ArrayUtils_getCommonElements_m1688440392_gp_0_0_0_0,
	&GenInst_ArrayUtils_ArrayToString_m1945898527_gp_0_0_0_0,
	&GenInst_GameObjectExtension_GetOrAddComponent_m1609223567_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4277882758_gp_0_0_0_0,
	&GenInst_TwoDArray_1_t2102069470_gp_0_0_0_0,
	&GenInst_GenericMouseEvent_1_t523005203_gp_0_0_0_0,
	&GenInst_MouseDelegate_t3478847027_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_1_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1131675484_gp_0_0_0_0,
	&GenInst_GridRepGenereic_2_t959362555_gp_0_0_0_0,
	&GenInst_GridRepGenereic_2_t959362555_gp_0_0_0_0_GridRepGenereic_2_t959362555_gp_1_0_0_0,
	&GenInst_ObjectPool_Spawn_m3379636899_gp_0_0_0_0,
	&GenInst_ObjectPool_Spawn_m3029418448_gp_0_0_0_0,
	&GenInst_ObjectPool_Spawn_m941763866_gp_0_0_0_0,
	&GenInst_ObjectPool_Spawn_m2099581555_gp_0_0_0_0,
	&GenInst_ObjectPool_Spawn_m3044749625_gp_0_0_0_0,
	&GenInst_ObjectPool_Spawn_m2976207054_gp_0_0_0_0,
	&GenInst_ObjectPool_GetPooled_m3585561301_gp_0_0_0_0,
	&GenInst_ObjectPool_GetSpawned_m4048446810_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_CreatePool_m2099972984_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_CreatePool_m152801013_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_Spawn_m3658873873_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_Spawn_m359633822_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_Spawn_m881325688_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_Spawn_m1216740145_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_Spawn_m2877761083_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_Spawn_m2085758064_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_Recycle_m1959930187_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_RecycleAll_m2567932538_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_CountPooled_m1364441074_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_CountSpawned_m1385273839_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_GetSpawned_m2878770644_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_GetSpawned_m1321398291_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_GetSpawned_m410148605_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_GetPooled_m3776185483_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_GetPooled_m24459516_gp_0_0_0_0,
	&GenInst_ObjectPoolExtensions_GetPooled_m4125519546_gp_0_0_0_0,
	&GenInst_SingletonBase_1_t2432853269_gp_0_0_0_0,
	&GenInst_Utils_Shuffle_m726398728_gp_0_0_0_0,
	&GenInst_DefaultExecutionOrder_t2717914595_0_0_0,
	&GenInst_PlayerConnection_t3517219175_0_0_0,
	&GenInst_GUILayer_t3254902478_0_0_0,
	&GenInst_DOTweenComponent_t696744215_0_0_0,
	&GenInst_TweenerCore_3_t3793077019_0_0_0,
	&GenInst_PathPlugin_t4171842066_0_0_0_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0,
	&GenInst_TweenerCore_3_t2279406887_0_0_0,
	&GenInst_TweenerCore_3_t2998039394_0_0_0,
	&GenInst_TweenerCore_3_t3065187631_0_0_0,
	&GenInst_Tweener_t760404022_0_0_0,
	&GenInst_TweenerCore_3_t1672798003_0_0_0,
	&GenInst_TweenerCore_3_t3035488489_0_0_0,
	&GenInst_TweenerCore_3_t1108663466_0_0_0,
	&GenInst_EventSystem_t3466835263_0_0_0,
	&GenInst_AxisEventData_t1524870173_0_0_0,
	&GenInst_Image_t2042527209_0_0_0,
	&GenInst_Button_t2872111280_0_0_0,
	&GenInst_RawImage_t2749640213_0_0_0,
	&GenInst_Slider_t297367283_0_0_0,
	&GenInst_Scrollbar_t3248359358_0_0_0,
	&GenInst_InputField_t1631627530_0_0_0,
	&GenInst_ScrollRect_t1199013257_0_0_0,
	&GenInst_Dropdown_t1985816271_0_0_0,
	&GenInst_GraphicRaycaster_t410733016_0_0_0,
	&GenInst_CanvasRenderer_t261436805_0_0_0,
	&GenInst_Corner_t1077473318_0_0_0,
	&GenInst_Axis_t1431825778_0_0_0,
	&GenInst_Constraint_t3558160636_0_0_0,
	&GenInst_SubmitEvent_t907918422_0_0_0,
	&GenInst_OnChangeEvent_t2863344003_0_0_0,
	&GenInst_OnValidateInput_t1946318473_0_0_0,
	&GenInst_LayoutElement_t2808691390_0_0_0,
	&GenInst_TextAnchor_t112990806_0_0_0,
	&GenInst_AnimationTriggers_t3244928895_0_0_0,
	&GenInst_Animator_t69676727_0_0_0,
	&GenInst_Rigidbody_t4233889191_0_0_0,
	&GenInst_GUITexture_t1909122990_0_0_0,
	&GenInst_GUIText_t2411476300_0_0_0,
	&GenInst_Light_t494725636_0_0_0,
	&GenInst_iTween_t488923914_0_0_0,
	&GenInst_CFX_Demo_Translate_t3218432890_0_0_0,
	&GenInst_Collider_t3497673348_0_0_0,
	&GenInst_CFX_AutoStopLoopedEffect_t3214280257_0_0_0,
	&GenInst_SortingGroup_t3832012067_0_0_0,
	&GenInst_ColliderEventSystem_t795033329_0_0_0,
	&GenInst_TrailRenderer_t2490637367_0_0_0,
	&GenInst_Collider2D_t646061738_0_0_0,
	&GenInst_MouseEventSystem_t3454497406_0_0_0,
	&GenInst_FlappyChar_t3600499100_0_0_0,
	&GenInst_RedundantFileWriter_t3761749544_0_0_0,
	&GenInst_KeyboardEventSystem_t2538007188_0_0_0,
	&GenInst_MouseEventProccessor_t442073356_0_0_0,
	&GenInst_BoxCollider2D_t948534547_0_0_0,
	&GenInst_GridRep_t2017325921_0_0_0,
	&GenInst_SoundManagerEventProccessor_t3825530019_0_0_0,
	&GenInst_PuzzlePiece_t1912707670_0_0_0,
	&GenInst_Cleaner_t1604682602_0_0_0,
	&GenInst_Pencil_t4115486279_0_0_0,
	&GenInst_SongBubble_t2997910009_0_0_0,
	&GenInst_PianoButton_t3912953253_0_0_0,
	&GenInst_Note_t686167636_0_0_0,
	&GenInst_MoveSnowflake_t721823583_0_0_0,
	&GenInst_CircleCollider2D_t13116344_0_0_0,
	&GenInst_PencilLine_t2072928649_0_0_0,
	&GenInst_GiftSlot_t3101111170_0_0_0,
	&GenInst_RingTossController_t956255555_0_0_0,
	&GenInst_DotPencil_t1876295642_0_0_0,
	&GenInst_ItemClickProcessor_t208213687_0_0_0,
	&GenInst_DummyMouseEventImplementation_t4020916073_0_0_0,
	&GenInst_Yey_t2293551375_0_0_0,
	&GenInst_Spawner_t534830648_0_0_0,
	&GenInst_ObjectPool_t2689037807_0_0_0,
	&GenInst_Bird_t1163357383_0_0_0,
	&GenInst_RepeatingBackground_t2924144931_0_0_0,
	&GenInst_Building_t2809767176_0_0_0,
	&GenInst_DefaultRandomRule_t4265547936_0_0_0,
	&GenInst_FlashingObject_t2247968655_0_0_0,
	&GenInst_GiftSpawnPoint_t604323357_0_0_0,
	&GenInst_Slide_t3421086917_0_0_0,
	&GenInst_DestroySound_t724042623_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0,
	&GenInst_GiftObject_t2144675780_0_0_0_GiftObject_t2144675780_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0,
	&GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0,
	&GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0,
	&GenInst_ScaleOn_t3059270671_0_0_0_ScaleOn_t3059270671_0_0_0,
	&GenInst_ScaleOn_t3059270671_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1258057682_0_0_0_KeyValuePair_2_t1258057682_0_0_0,
	&GenInst_KeyValuePair_2_t1258057682_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3720882578_0_0_0_KeyValuePair_2_t3720882578_0_0_0,
	&GenInst_KeyValuePair_2_t3720882578_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_KeyValuePair_2_t901022143_0_0_0_KeyValuePair_2_t901022143_0_0_0,
	&GenInst_KeyValuePair_2_t901022143_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3653809873_0_0_0_KeyValuePair_2_t3653809873_0_0_0,
	&GenInst_KeyValuePair_2_t3653809873_0_0_0_Il2CppObject_0_0_0,
	&GenInst_SemiRandomRule_t1463926147_0_0_0,
};
