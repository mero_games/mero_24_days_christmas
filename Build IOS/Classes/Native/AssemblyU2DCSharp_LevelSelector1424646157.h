﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_MemoryGame4018179393.h"

// MemoryGame[]
struct MemoryGameU5BU5D_t1238759580;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSelector
struct  LevelSelector_t1424646157  : public MemoryGame_t4018179393
{
public:
	// MemoryGame[] LevelSelector::levels
	MemoryGameU5BU5D_t1238759580* ___levels_10;
	// UnityEngine.GameObject[] LevelSelector::bgs
	GameObjectU5BU5D_t3057952154* ___bgs_11;

public:
	inline static int32_t get_offset_of_levels_10() { return static_cast<int32_t>(offsetof(LevelSelector_t1424646157, ___levels_10)); }
	inline MemoryGameU5BU5D_t1238759580* get_levels_10() const { return ___levels_10; }
	inline MemoryGameU5BU5D_t1238759580** get_address_of_levels_10() { return &___levels_10; }
	inline void set_levels_10(MemoryGameU5BU5D_t1238759580* value)
	{
		___levels_10 = value;
		Il2CppCodeGenWriteBarrier(&___levels_10, value);
	}

	inline static int32_t get_offset_of_bgs_11() { return static_cast<int32_t>(offsetof(LevelSelector_t1424646157, ___bgs_11)); }
	inline GameObjectU5BU5D_t3057952154* get_bgs_11() const { return ___bgs_11; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_bgs_11() { return &___bgs_11; }
	inline void set_bgs_11(GameObjectU5BU5D_t3057952154* value)
	{
		___bgs_11 = value;
		Il2CppCodeGenWriteBarrier(&___bgs_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
