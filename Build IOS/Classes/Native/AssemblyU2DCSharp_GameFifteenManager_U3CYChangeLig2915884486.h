﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameFifteenManager
struct GameFifteenManager_t1527188406;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameFifteenManager/<YChangeLights>c__Iterator0
struct  U3CYChangeLightsU3Ec__Iterator0_t2915884486  : public Il2CppObject
{
public:
	// System.Single GameFifteenManager/<YChangeLights>c__Iterator0::<count>__0
	float ___U3CcountU3E__0_0;
	// System.Single GameFifteenManager/<YChangeLights>c__Iterator0::<curSpeed>__0
	float ___U3CcurSpeedU3E__0_1;
	// System.Single GameFifteenManager/<YChangeLights>c__Iterator0::<curWait>__0
	float ___U3CcurWaitU3E__0_2;
	// System.Int32 GameFifteenManager/<YChangeLights>c__Iterator0::row
	int32_t ___row_3;
	// GameFifteenManager GameFifteenManager/<YChangeLights>c__Iterator0::$this
	GameFifteenManager_t1527188406 * ___U24this_4;
	// System.Object GameFifteenManager/<YChangeLights>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean GameFifteenManager/<YChangeLights>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 GameFifteenManager/<YChangeLights>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CcountU3E__0_0() { return static_cast<int32_t>(offsetof(U3CYChangeLightsU3Ec__Iterator0_t2915884486, ___U3CcountU3E__0_0)); }
	inline float get_U3CcountU3E__0_0() const { return ___U3CcountU3E__0_0; }
	inline float* get_address_of_U3CcountU3E__0_0() { return &___U3CcountU3E__0_0; }
	inline void set_U3CcountU3E__0_0(float value)
	{
		___U3CcountU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcurSpeedU3E__0_1() { return static_cast<int32_t>(offsetof(U3CYChangeLightsU3Ec__Iterator0_t2915884486, ___U3CcurSpeedU3E__0_1)); }
	inline float get_U3CcurSpeedU3E__0_1() const { return ___U3CcurSpeedU3E__0_1; }
	inline float* get_address_of_U3CcurSpeedU3E__0_1() { return &___U3CcurSpeedU3E__0_1; }
	inline void set_U3CcurSpeedU3E__0_1(float value)
	{
		___U3CcurSpeedU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CcurWaitU3E__0_2() { return static_cast<int32_t>(offsetof(U3CYChangeLightsU3Ec__Iterator0_t2915884486, ___U3CcurWaitU3E__0_2)); }
	inline float get_U3CcurWaitU3E__0_2() const { return ___U3CcurWaitU3E__0_2; }
	inline float* get_address_of_U3CcurWaitU3E__0_2() { return &___U3CcurWaitU3E__0_2; }
	inline void set_U3CcurWaitU3E__0_2(float value)
	{
		___U3CcurWaitU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_row_3() { return static_cast<int32_t>(offsetof(U3CYChangeLightsU3Ec__Iterator0_t2915884486, ___row_3)); }
	inline int32_t get_row_3() const { return ___row_3; }
	inline int32_t* get_address_of_row_3() { return &___row_3; }
	inline void set_row_3(int32_t value)
	{
		___row_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CYChangeLightsU3Ec__Iterator0_t2915884486, ___U24this_4)); }
	inline GameFifteenManager_t1527188406 * get_U24this_4() const { return ___U24this_4; }
	inline GameFifteenManager_t1527188406 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(GameFifteenManager_t1527188406 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CYChangeLightsU3Ec__Iterator0_t2915884486, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CYChangeLightsU3Ec__Iterator0_t2915884486, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CYChangeLightsU3Ec__Iterator0_t2915884486, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
