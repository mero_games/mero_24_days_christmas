﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HTTPLoaderExample
struct  HTTPLoaderExample_t3175293487  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField HTTPLoaderExample::urlField
	InputField_t1631627530 * ___urlField_2;
	// UnityEngine.UI.InputField HTTPLoaderExample::resultField
	InputField_t1631627530 * ___resultField_3;
	// UnityEngine.UI.Button HTTPLoaderExample::loadBtn
	Button_t2872111280 * ___loadBtn_4;

public:
	inline static int32_t get_offset_of_urlField_2() { return static_cast<int32_t>(offsetof(HTTPLoaderExample_t3175293487, ___urlField_2)); }
	inline InputField_t1631627530 * get_urlField_2() const { return ___urlField_2; }
	inline InputField_t1631627530 ** get_address_of_urlField_2() { return &___urlField_2; }
	inline void set_urlField_2(InputField_t1631627530 * value)
	{
		___urlField_2 = value;
		Il2CppCodeGenWriteBarrier(&___urlField_2, value);
	}

	inline static int32_t get_offset_of_resultField_3() { return static_cast<int32_t>(offsetof(HTTPLoaderExample_t3175293487, ___resultField_3)); }
	inline InputField_t1631627530 * get_resultField_3() const { return ___resultField_3; }
	inline InputField_t1631627530 ** get_address_of_resultField_3() { return &___resultField_3; }
	inline void set_resultField_3(InputField_t1631627530 * value)
	{
		___resultField_3 = value;
		Il2CppCodeGenWriteBarrier(&___resultField_3, value);
	}

	inline static int32_t get_offset_of_loadBtn_4() { return static_cast<int32_t>(offsetof(HTTPLoaderExample_t3175293487, ___loadBtn_4)); }
	inline Button_t2872111280 * get_loadBtn_4() const { return ___loadBtn_4; }
	inline Button_t2872111280 ** get_address_of_loadBtn_4() { return &___loadBtn_4; }
	inline void set_loadBtn_4(Button_t2872111280 * value)
	{
		___loadBtn_4 = value;
		Il2CppCodeGenWriteBarrier(&___loadBtn_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
