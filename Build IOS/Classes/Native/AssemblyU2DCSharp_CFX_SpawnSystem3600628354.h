﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// CFX_SpawnSystem
struct CFX_SpawnSystem_t3600628354;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t133479914;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1079703083;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_SpawnSystem
struct  CFX_SpawnSystem_t3600628354  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] CFX_SpawnSystem::objectsToPreload
	GameObjectU5BU5D_t3057952154* ___objectsToPreload_3;
	// System.Int32[] CFX_SpawnSystem::objectsToPreloadTimes
	Int32U5BU5D_t3030399641* ___objectsToPreloadTimes_4;
	// System.Boolean CFX_SpawnSystem::hideObjectsInHierarchy
	bool ___hideObjectsInHierarchy_5;
	// System.Boolean CFX_SpawnSystem::spawnAsChildren
	bool ___spawnAsChildren_6;
	// System.Boolean CFX_SpawnSystem::onlyGetInactiveObjects
	bool ___onlyGetInactiveObjects_7;
	// System.Boolean CFX_SpawnSystem::instantiateIfNeeded
	bool ___instantiateIfNeeded_8;
	// System.Boolean CFX_SpawnSystem::allObjectsLoaded
	bool ___allObjectsLoaded_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>> CFX_SpawnSystem::instantiatedObjects
	Dictionary_2_t133479914 * ___instantiatedObjects_10;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CFX_SpawnSystem::poolCursors
	Dictionary_2_t1079703083 * ___poolCursors_11;

public:
	inline static int32_t get_offset_of_objectsToPreload_3() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3600628354, ___objectsToPreload_3)); }
	inline GameObjectU5BU5D_t3057952154* get_objectsToPreload_3() const { return ___objectsToPreload_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_objectsToPreload_3() { return &___objectsToPreload_3; }
	inline void set_objectsToPreload_3(GameObjectU5BU5D_t3057952154* value)
	{
		___objectsToPreload_3 = value;
		Il2CppCodeGenWriteBarrier(&___objectsToPreload_3, value);
	}

	inline static int32_t get_offset_of_objectsToPreloadTimes_4() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3600628354, ___objectsToPreloadTimes_4)); }
	inline Int32U5BU5D_t3030399641* get_objectsToPreloadTimes_4() const { return ___objectsToPreloadTimes_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_objectsToPreloadTimes_4() { return &___objectsToPreloadTimes_4; }
	inline void set_objectsToPreloadTimes_4(Int32U5BU5D_t3030399641* value)
	{
		___objectsToPreloadTimes_4 = value;
		Il2CppCodeGenWriteBarrier(&___objectsToPreloadTimes_4, value);
	}

	inline static int32_t get_offset_of_hideObjectsInHierarchy_5() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3600628354, ___hideObjectsInHierarchy_5)); }
	inline bool get_hideObjectsInHierarchy_5() const { return ___hideObjectsInHierarchy_5; }
	inline bool* get_address_of_hideObjectsInHierarchy_5() { return &___hideObjectsInHierarchy_5; }
	inline void set_hideObjectsInHierarchy_5(bool value)
	{
		___hideObjectsInHierarchy_5 = value;
	}

	inline static int32_t get_offset_of_spawnAsChildren_6() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3600628354, ___spawnAsChildren_6)); }
	inline bool get_spawnAsChildren_6() const { return ___spawnAsChildren_6; }
	inline bool* get_address_of_spawnAsChildren_6() { return &___spawnAsChildren_6; }
	inline void set_spawnAsChildren_6(bool value)
	{
		___spawnAsChildren_6 = value;
	}

	inline static int32_t get_offset_of_onlyGetInactiveObjects_7() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3600628354, ___onlyGetInactiveObjects_7)); }
	inline bool get_onlyGetInactiveObjects_7() const { return ___onlyGetInactiveObjects_7; }
	inline bool* get_address_of_onlyGetInactiveObjects_7() { return &___onlyGetInactiveObjects_7; }
	inline void set_onlyGetInactiveObjects_7(bool value)
	{
		___onlyGetInactiveObjects_7 = value;
	}

	inline static int32_t get_offset_of_instantiateIfNeeded_8() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3600628354, ___instantiateIfNeeded_8)); }
	inline bool get_instantiateIfNeeded_8() const { return ___instantiateIfNeeded_8; }
	inline bool* get_address_of_instantiateIfNeeded_8() { return &___instantiateIfNeeded_8; }
	inline void set_instantiateIfNeeded_8(bool value)
	{
		___instantiateIfNeeded_8 = value;
	}

	inline static int32_t get_offset_of_allObjectsLoaded_9() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3600628354, ___allObjectsLoaded_9)); }
	inline bool get_allObjectsLoaded_9() const { return ___allObjectsLoaded_9; }
	inline bool* get_address_of_allObjectsLoaded_9() { return &___allObjectsLoaded_9; }
	inline void set_allObjectsLoaded_9(bool value)
	{
		___allObjectsLoaded_9 = value;
	}

	inline static int32_t get_offset_of_instantiatedObjects_10() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3600628354, ___instantiatedObjects_10)); }
	inline Dictionary_2_t133479914 * get_instantiatedObjects_10() const { return ___instantiatedObjects_10; }
	inline Dictionary_2_t133479914 ** get_address_of_instantiatedObjects_10() { return &___instantiatedObjects_10; }
	inline void set_instantiatedObjects_10(Dictionary_2_t133479914 * value)
	{
		___instantiatedObjects_10 = value;
		Il2CppCodeGenWriteBarrier(&___instantiatedObjects_10, value);
	}

	inline static int32_t get_offset_of_poolCursors_11() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3600628354, ___poolCursors_11)); }
	inline Dictionary_2_t1079703083 * get_poolCursors_11() const { return ___poolCursors_11; }
	inline Dictionary_2_t1079703083 ** get_address_of_poolCursors_11() { return &___poolCursors_11; }
	inline void set_poolCursors_11(Dictionary_2_t1079703083 * value)
	{
		___poolCursors_11 = value;
		Il2CppCodeGenWriteBarrier(&___poolCursors_11, value);
	}
};

struct CFX_SpawnSystem_t3600628354_StaticFields
{
public:
	// CFX_SpawnSystem CFX_SpawnSystem::instance
	CFX_SpawnSystem_t3600628354 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3600628354_StaticFields, ___instance_2)); }
	inline CFX_SpawnSystem_t3600628354 * get_instance_2() const { return ___instance_2; }
	inline CFX_SpawnSystem_t3600628354 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CFX_SpawnSystem_t3600628354 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
