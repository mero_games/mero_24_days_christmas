﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObstacleBase
struct  ObstacleBase_t365467716  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ObstacleBase::minY
	float ___minY_2;
	// System.Single ObstacleBase::maxY
	float ___maxY_3;

public:
	inline static int32_t get_offset_of_minY_2() { return static_cast<int32_t>(offsetof(ObstacleBase_t365467716, ___minY_2)); }
	inline float get_minY_2() const { return ___minY_2; }
	inline float* get_address_of_minY_2() { return &___minY_2; }
	inline void set_minY_2(float value)
	{
		___minY_2 = value;
	}

	inline static int32_t get_offset_of_maxY_3() { return static_cast<int32_t>(offsetof(ObstacleBase_t365467716, ___maxY_3)); }
	inline float get_maxY_3() const { return ___maxY_3; }
	inline float* get_address_of_maxY_3() { return &___maxY_3; }
	inline void set_maxY_3(float value)
	{
		___maxY_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
