﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PencilLine
struct  PencilLine_t2072928649  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PencilLine::toMove
	bool ___toMove_2;
	// UnityEngine.AudioSource PencilLine::drawSound
	AudioSource_t1135106623 * ___drawSound_3;

public:
	inline static int32_t get_offset_of_toMove_2() { return static_cast<int32_t>(offsetof(PencilLine_t2072928649, ___toMove_2)); }
	inline bool get_toMove_2() const { return ___toMove_2; }
	inline bool* get_address_of_toMove_2() { return &___toMove_2; }
	inline void set_toMove_2(bool value)
	{
		___toMove_2 = value;
	}

	inline static int32_t get_offset_of_drawSound_3() { return static_cast<int32_t>(offsetof(PencilLine_t2072928649, ___drawSound_3)); }
	inline AudioSource_t1135106623 * get_drawSound_3() const { return ___drawSound_3; }
	inline AudioSource_t1135106623 ** get_address_of_drawSound_3() { return &___drawSound_3; }
	inline void set_drawSound_3(AudioSource_t1135106623 * value)
	{
		___drawSound_3 = value;
		Il2CppCodeGenWriteBarrier(&___drawSound_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
