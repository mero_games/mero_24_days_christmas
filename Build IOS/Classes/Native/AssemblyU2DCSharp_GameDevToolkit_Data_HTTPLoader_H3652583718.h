﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Data.HTTPLoader/HTTPResult
struct  HTTPResult_t3652583718  : public Il2CppObject
{
public:
	// System.String GameDevToolkit.Data.HTTPLoader/HTTPResult::content
	String_t* ___content_0;
	// System.String GameDevToolkit.Data.HTTPLoader/HTTPResult::error
	String_t* ___error_1;
	// System.Byte[] GameDevToolkit.Data.HTTPLoader/HTTPResult::bytes
	ByteU5BU5D_t3397334013* ___bytes_2;

public:
	inline static int32_t get_offset_of_content_0() { return static_cast<int32_t>(offsetof(HTTPResult_t3652583718, ___content_0)); }
	inline String_t* get_content_0() const { return ___content_0; }
	inline String_t** get_address_of_content_0() { return &___content_0; }
	inline void set_content_0(String_t* value)
	{
		___content_0 = value;
		Il2CppCodeGenWriteBarrier(&___content_0, value);
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(HTTPResult_t3652583718, ___error_1)); }
	inline String_t* get_error_1() const { return ___error_1; }
	inline String_t** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(String_t* value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier(&___error_1, value);
	}

	inline static int32_t get_offset_of_bytes_2() { return static_cast<int32_t>(offsetof(HTTPResult_t3652583718, ___bytes_2)); }
	inline ByteU5BU5D_t3397334013* get_bytes_2() const { return ___bytes_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_bytes_2() { return &___bytes_2; }
	inline void set_bytes_2(ByteU5BU5D_t3397334013* value)
	{
		___bytes_2 = value;
		Il2CppCodeGenWriteBarrier(&___bytes_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
