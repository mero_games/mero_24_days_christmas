﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen1428831875.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameFourManager
struct  GameFourManager_t2429316785  : public SingletonBase_1_t1428831875
{
public:
	// UnityEngine.GameObject GameFourManager::pencil
	GameObject_t1756533147 * ___pencil_4;
	// UnityEngine.Transform GameFourManager::drawingBoardsHolder
	Transform_t3275118058 * ___drawingBoardsHolder_5;
	// UnityEngine.Transform GameFourManager::drawingsHolder
	Transform_t3275118058 * ___drawingsHolder_6;
	// UnityEngine.Transform GameFourManager::correctColliders
	Transform_t3275118058 * ___correctColliders_7;
	// UnityEngine.Transform GameFourManager::finishedBody
	Transform_t3275118058 * ___finishedBody_8;
	// UnityEngine.Transform GameFourManager::finishedHead
	Transform_t3275118058 * ___finishedHead_9;
	// UnityEngine.SpriteRenderer GameFourManager::exampleBody
	SpriteRenderer_t1209076198 * ___exampleBody_10;
	// UnityEngine.SpriteRenderer GameFourManager::exampleHead
	SpriteRenderer_t1209076198 * ___exampleHead_11;
	// System.Int32 GameFourManager::numberOfDrawingsToMake
	int32_t ___numberOfDrawingsToMake_12;
	// System.Int32 GameFourManager::numberOfDrawingsDone
	int32_t ___numberOfDrawingsDone_13;
	// System.Int32 GameFourManager::correctCollidersLeft
	int32_t ___correctCollidersLeft_14;
	// System.Int32 GameFourManager::wrongCollidersHit
	int32_t ___wrongCollidersHit_15;
	// System.Boolean GameFourManager::canDraw
	bool ___canDraw_16;
	// System.Int32 GameFourManager::shapesDrawn
	int32_t ___shapesDrawn_17;
	// System.Int32 GameFourManager::bodyIndex
	int32_t ___bodyIndex_18;
	// System.Int32 GameFourManager::headIndex
	int32_t ___headIndex_19;
	// UnityEngine.GameObject GameFourManager::shapeToDraw
	GameObject_t1756533147 * ___shapeToDraw_20;
	// UnityEngine.Transform GameFourManager::currentBoard
	Transform_t3275118058 * ___currentBoard_21;
	// UnityEngine.GameObject GameFourManager::iPencil
	GameObject_t1756533147 * ___iPencil_22;
	// UnityEngine.GameObject GameFourManager::finishedShape
	GameObject_t1756533147 * ___finishedShape_23;

public:
	inline static int32_t get_offset_of_pencil_4() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___pencil_4)); }
	inline GameObject_t1756533147 * get_pencil_4() const { return ___pencil_4; }
	inline GameObject_t1756533147 ** get_address_of_pencil_4() { return &___pencil_4; }
	inline void set_pencil_4(GameObject_t1756533147 * value)
	{
		___pencil_4 = value;
		Il2CppCodeGenWriteBarrier(&___pencil_4, value);
	}

	inline static int32_t get_offset_of_drawingBoardsHolder_5() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___drawingBoardsHolder_5)); }
	inline Transform_t3275118058 * get_drawingBoardsHolder_5() const { return ___drawingBoardsHolder_5; }
	inline Transform_t3275118058 ** get_address_of_drawingBoardsHolder_5() { return &___drawingBoardsHolder_5; }
	inline void set_drawingBoardsHolder_5(Transform_t3275118058 * value)
	{
		___drawingBoardsHolder_5 = value;
		Il2CppCodeGenWriteBarrier(&___drawingBoardsHolder_5, value);
	}

	inline static int32_t get_offset_of_drawingsHolder_6() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___drawingsHolder_6)); }
	inline Transform_t3275118058 * get_drawingsHolder_6() const { return ___drawingsHolder_6; }
	inline Transform_t3275118058 ** get_address_of_drawingsHolder_6() { return &___drawingsHolder_6; }
	inline void set_drawingsHolder_6(Transform_t3275118058 * value)
	{
		___drawingsHolder_6 = value;
		Il2CppCodeGenWriteBarrier(&___drawingsHolder_6, value);
	}

	inline static int32_t get_offset_of_correctColliders_7() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___correctColliders_7)); }
	inline Transform_t3275118058 * get_correctColliders_7() const { return ___correctColliders_7; }
	inline Transform_t3275118058 ** get_address_of_correctColliders_7() { return &___correctColliders_7; }
	inline void set_correctColliders_7(Transform_t3275118058 * value)
	{
		___correctColliders_7 = value;
		Il2CppCodeGenWriteBarrier(&___correctColliders_7, value);
	}

	inline static int32_t get_offset_of_finishedBody_8() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___finishedBody_8)); }
	inline Transform_t3275118058 * get_finishedBody_8() const { return ___finishedBody_8; }
	inline Transform_t3275118058 ** get_address_of_finishedBody_8() { return &___finishedBody_8; }
	inline void set_finishedBody_8(Transform_t3275118058 * value)
	{
		___finishedBody_8 = value;
		Il2CppCodeGenWriteBarrier(&___finishedBody_8, value);
	}

	inline static int32_t get_offset_of_finishedHead_9() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___finishedHead_9)); }
	inline Transform_t3275118058 * get_finishedHead_9() const { return ___finishedHead_9; }
	inline Transform_t3275118058 ** get_address_of_finishedHead_9() { return &___finishedHead_9; }
	inline void set_finishedHead_9(Transform_t3275118058 * value)
	{
		___finishedHead_9 = value;
		Il2CppCodeGenWriteBarrier(&___finishedHead_9, value);
	}

	inline static int32_t get_offset_of_exampleBody_10() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___exampleBody_10)); }
	inline SpriteRenderer_t1209076198 * get_exampleBody_10() const { return ___exampleBody_10; }
	inline SpriteRenderer_t1209076198 ** get_address_of_exampleBody_10() { return &___exampleBody_10; }
	inline void set_exampleBody_10(SpriteRenderer_t1209076198 * value)
	{
		___exampleBody_10 = value;
		Il2CppCodeGenWriteBarrier(&___exampleBody_10, value);
	}

	inline static int32_t get_offset_of_exampleHead_11() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___exampleHead_11)); }
	inline SpriteRenderer_t1209076198 * get_exampleHead_11() const { return ___exampleHead_11; }
	inline SpriteRenderer_t1209076198 ** get_address_of_exampleHead_11() { return &___exampleHead_11; }
	inline void set_exampleHead_11(SpriteRenderer_t1209076198 * value)
	{
		___exampleHead_11 = value;
		Il2CppCodeGenWriteBarrier(&___exampleHead_11, value);
	}

	inline static int32_t get_offset_of_numberOfDrawingsToMake_12() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___numberOfDrawingsToMake_12)); }
	inline int32_t get_numberOfDrawingsToMake_12() const { return ___numberOfDrawingsToMake_12; }
	inline int32_t* get_address_of_numberOfDrawingsToMake_12() { return &___numberOfDrawingsToMake_12; }
	inline void set_numberOfDrawingsToMake_12(int32_t value)
	{
		___numberOfDrawingsToMake_12 = value;
	}

	inline static int32_t get_offset_of_numberOfDrawingsDone_13() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___numberOfDrawingsDone_13)); }
	inline int32_t get_numberOfDrawingsDone_13() const { return ___numberOfDrawingsDone_13; }
	inline int32_t* get_address_of_numberOfDrawingsDone_13() { return &___numberOfDrawingsDone_13; }
	inline void set_numberOfDrawingsDone_13(int32_t value)
	{
		___numberOfDrawingsDone_13 = value;
	}

	inline static int32_t get_offset_of_correctCollidersLeft_14() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___correctCollidersLeft_14)); }
	inline int32_t get_correctCollidersLeft_14() const { return ___correctCollidersLeft_14; }
	inline int32_t* get_address_of_correctCollidersLeft_14() { return &___correctCollidersLeft_14; }
	inline void set_correctCollidersLeft_14(int32_t value)
	{
		___correctCollidersLeft_14 = value;
	}

	inline static int32_t get_offset_of_wrongCollidersHit_15() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___wrongCollidersHit_15)); }
	inline int32_t get_wrongCollidersHit_15() const { return ___wrongCollidersHit_15; }
	inline int32_t* get_address_of_wrongCollidersHit_15() { return &___wrongCollidersHit_15; }
	inline void set_wrongCollidersHit_15(int32_t value)
	{
		___wrongCollidersHit_15 = value;
	}

	inline static int32_t get_offset_of_canDraw_16() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___canDraw_16)); }
	inline bool get_canDraw_16() const { return ___canDraw_16; }
	inline bool* get_address_of_canDraw_16() { return &___canDraw_16; }
	inline void set_canDraw_16(bool value)
	{
		___canDraw_16 = value;
	}

	inline static int32_t get_offset_of_shapesDrawn_17() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___shapesDrawn_17)); }
	inline int32_t get_shapesDrawn_17() const { return ___shapesDrawn_17; }
	inline int32_t* get_address_of_shapesDrawn_17() { return &___shapesDrawn_17; }
	inline void set_shapesDrawn_17(int32_t value)
	{
		___shapesDrawn_17 = value;
	}

	inline static int32_t get_offset_of_bodyIndex_18() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___bodyIndex_18)); }
	inline int32_t get_bodyIndex_18() const { return ___bodyIndex_18; }
	inline int32_t* get_address_of_bodyIndex_18() { return &___bodyIndex_18; }
	inline void set_bodyIndex_18(int32_t value)
	{
		___bodyIndex_18 = value;
	}

	inline static int32_t get_offset_of_headIndex_19() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___headIndex_19)); }
	inline int32_t get_headIndex_19() const { return ___headIndex_19; }
	inline int32_t* get_address_of_headIndex_19() { return &___headIndex_19; }
	inline void set_headIndex_19(int32_t value)
	{
		___headIndex_19 = value;
	}

	inline static int32_t get_offset_of_shapeToDraw_20() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___shapeToDraw_20)); }
	inline GameObject_t1756533147 * get_shapeToDraw_20() const { return ___shapeToDraw_20; }
	inline GameObject_t1756533147 ** get_address_of_shapeToDraw_20() { return &___shapeToDraw_20; }
	inline void set_shapeToDraw_20(GameObject_t1756533147 * value)
	{
		___shapeToDraw_20 = value;
		Il2CppCodeGenWriteBarrier(&___shapeToDraw_20, value);
	}

	inline static int32_t get_offset_of_currentBoard_21() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___currentBoard_21)); }
	inline Transform_t3275118058 * get_currentBoard_21() const { return ___currentBoard_21; }
	inline Transform_t3275118058 ** get_address_of_currentBoard_21() { return &___currentBoard_21; }
	inline void set_currentBoard_21(Transform_t3275118058 * value)
	{
		___currentBoard_21 = value;
		Il2CppCodeGenWriteBarrier(&___currentBoard_21, value);
	}

	inline static int32_t get_offset_of_iPencil_22() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___iPencil_22)); }
	inline GameObject_t1756533147 * get_iPencil_22() const { return ___iPencil_22; }
	inline GameObject_t1756533147 ** get_address_of_iPencil_22() { return &___iPencil_22; }
	inline void set_iPencil_22(GameObject_t1756533147 * value)
	{
		___iPencil_22 = value;
		Il2CppCodeGenWriteBarrier(&___iPencil_22, value);
	}

	inline static int32_t get_offset_of_finishedShape_23() { return static_cast<int32_t>(offsetof(GameFourManager_t2429316785, ___finishedShape_23)); }
	inline GameObject_t1756533147 * get_finishedShape_23() const { return ___finishedShape_23; }
	inline GameObject_t1756533147 ** get_address_of_finishedShape_23() { return &___finishedShape_23; }
	inline void set_finishedShape_23(GameObject_t1756533147 * value)
	{
		___finishedShape_23 = value;
		Il2CppCodeGenWriteBarrier(&___finishedShape_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
