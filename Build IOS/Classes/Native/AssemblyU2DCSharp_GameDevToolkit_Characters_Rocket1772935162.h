﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Events.UnityAction`3<GameDevToolkit.Characters.RocketChar,UnityEngine.Collider2D,UnityEngine.Collision2D>
struct UnityAction_3_t1429690495;
// UnityEngine.Events.UnityAction`2<GameDevToolkit.Characters.RocketChar,UnityEngine.Collider2D>
struct UnityAction_2_t2584181222;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Characters.RocketChar
struct  RocketChar_t1772935162  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Events.UnityAction`3<GameDevToolkit.Characters.RocketChar,UnityEngine.Collider2D,UnityEngine.Collision2D> GameDevToolkit.Characters.RocketChar::RocketCharHit
	UnityAction_3_t1429690495 * ___RocketCharHit_2;
	// UnityEngine.Events.UnityAction`2<GameDevToolkit.Characters.RocketChar,UnityEngine.Collider2D> GameDevToolkit.Characters.RocketChar::RocketCharTriggered
	UnityAction_2_t2584181222 * ___RocketCharTriggered_3;
	// System.String[] GameDevToolkit.Characters.RocketChar::exceptionNames
	StringU5BU5D_t1642385972* ___exceptionNames_4;
	// System.Boolean GameDevToolkit.Characters.RocketChar::canMove
	bool ___canMove_5;
	// System.Single GameDevToolkit.Characters.RocketChar::minX
	float ___minX_6;
	// System.Single GameDevToolkit.Characters.RocketChar::maxX
	float ___maxX_7;
	// UnityEngine.GameObject GameDevToolkit.Characters.RocketChar::leftArrow
	GameObject_t1756533147 * ___leftArrow_8;
	// UnityEngine.GameObject GameDevToolkit.Characters.RocketChar::rightArrow
	GameObject_t1756533147 * ___rightArrow_9;
	// System.Single GameDevToolkit.Characters.RocketChar::horizontalSpeed
	float ___horizontalSpeed_10;
	// System.Single GameDevToolkit.Characters.RocketChar::jumpMaxForce
	float ___jumpMaxForce_11;
	// UnityEngine.Rigidbody2D GameDevToolkit.Characters.RocketChar::body
	Rigidbody2D_t502193897 * ___body_12;
	// System.Boolean GameDevToolkit.Characters.RocketChar::goLeft
	bool ___goLeft_13;
	// System.Boolean GameDevToolkit.Characters.RocketChar::goRight
	bool ___goRight_14;

public:
	inline static int32_t get_offset_of_RocketCharHit_2() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___RocketCharHit_2)); }
	inline UnityAction_3_t1429690495 * get_RocketCharHit_2() const { return ___RocketCharHit_2; }
	inline UnityAction_3_t1429690495 ** get_address_of_RocketCharHit_2() { return &___RocketCharHit_2; }
	inline void set_RocketCharHit_2(UnityAction_3_t1429690495 * value)
	{
		___RocketCharHit_2 = value;
		Il2CppCodeGenWriteBarrier(&___RocketCharHit_2, value);
	}

	inline static int32_t get_offset_of_RocketCharTriggered_3() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___RocketCharTriggered_3)); }
	inline UnityAction_2_t2584181222 * get_RocketCharTriggered_3() const { return ___RocketCharTriggered_3; }
	inline UnityAction_2_t2584181222 ** get_address_of_RocketCharTriggered_3() { return &___RocketCharTriggered_3; }
	inline void set_RocketCharTriggered_3(UnityAction_2_t2584181222 * value)
	{
		___RocketCharTriggered_3 = value;
		Il2CppCodeGenWriteBarrier(&___RocketCharTriggered_3, value);
	}

	inline static int32_t get_offset_of_exceptionNames_4() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___exceptionNames_4)); }
	inline StringU5BU5D_t1642385972* get_exceptionNames_4() const { return ___exceptionNames_4; }
	inline StringU5BU5D_t1642385972** get_address_of_exceptionNames_4() { return &___exceptionNames_4; }
	inline void set_exceptionNames_4(StringU5BU5D_t1642385972* value)
	{
		___exceptionNames_4 = value;
		Il2CppCodeGenWriteBarrier(&___exceptionNames_4, value);
	}

	inline static int32_t get_offset_of_canMove_5() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___canMove_5)); }
	inline bool get_canMove_5() const { return ___canMove_5; }
	inline bool* get_address_of_canMove_5() { return &___canMove_5; }
	inline void set_canMove_5(bool value)
	{
		___canMove_5 = value;
	}

	inline static int32_t get_offset_of_minX_6() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___minX_6)); }
	inline float get_minX_6() const { return ___minX_6; }
	inline float* get_address_of_minX_6() { return &___minX_6; }
	inline void set_minX_6(float value)
	{
		___minX_6 = value;
	}

	inline static int32_t get_offset_of_maxX_7() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___maxX_7)); }
	inline float get_maxX_7() const { return ___maxX_7; }
	inline float* get_address_of_maxX_7() { return &___maxX_7; }
	inline void set_maxX_7(float value)
	{
		___maxX_7 = value;
	}

	inline static int32_t get_offset_of_leftArrow_8() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___leftArrow_8)); }
	inline GameObject_t1756533147 * get_leftArrow_8() const { return ___leftArrow_8; }
	inline GameObject_t1756533147 ** get_address_of_leftArrow_8() { return &___leftArrow_8; }
	inline void set_leftArrow_8(GameObject_t1756533147 * value)
	{
		___leftArrow_8 = value;
		Il2CppCodeGenWriteBarrier(&___leftArrow_8, value);
	}

	inline static int32_t get_offset_of_rightArrow_9() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___rightArrow_9)); }
	inline GameObject_t1756533147 * get_rightArrow_9() const { return ___rightArrow_9; }
	inline GameObject_t1756533147 ** get_address_of_rightArrow_9() { return &___rightArrow_9; }
	inline void set_rightArrow_9(GameObject_t1756533147 * value)
	{
		___rightArrow_9 = value;
		Il2CppCodeGenWriteBarrier(&___rightArrow_9, value);
	}

	inline static int32_t get_offset_of_horizontalSpeed_10() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___horizontalSpeed_10)); }
	inline float get_horizontalSpeed_10() const { return ___horizontalSpeed_10; }
	inline float* get_address_of_horizontalSpeed_10() { return &___horizontalSpeed_10; }
	inline void set_horizontalSpeed_10(float value)
	{
		___horizontalSpeed_10 = value;
	}

	inline static int32_t get_offset_of_jumpMaxForce_11() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___jumpMaxForce_11)); }
	inline float get_jumpMaxForce_11() const { return ___jumpMaxForce_11; }
	inline float* get_address_of_jumpMaxForce_11() { return &___jumpMaxForce_11; }
	inline void set_jumpMaxForce_11(float value)
	{
		___jumpMaxForce_11 = value;
	}

	inline static int32_t get_offset_of_body_12() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___body_12)); }
	inline Rigidbody2D_t502193897 * get_body_12() const { return ___body_12; }
	inline Rigidbody2D_t502193897 ** get_address_of_body_12() { return &___body_12; }
	inline void set_body_12(Rigidbody2D_t502193897 * value)
	{
		___body_12 = value;
		Il2CppCodeGenWriteBarrier(&___body_12, value);
	}

	inline static int32_t get_offset_of_goLeft_13() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___goLeft_13)); }
	inline bool get_goLeft_13() const { return ___goLeft_13; }
	inline bool* get_address_of_goLeft_13() { return &___goLeft_13; }
	inline void set_goLeft_13(bool value)
	{
		___goLeft_13 = value;
	}

	inline static int32_t get_offset_of_goRight_14() { return static_cast<int32_t>(offsetof(RocketChar_t1772935162, ___goRight_14)); }
	inline bool get_goRight_14() const { return ___goRight_14; }
	inline bool* get_address_of_goRight_14() { return &___goRight_14; }
	inline void set_goRight_14(bool value)
	{
		___goRight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
