﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArrayUtilsExample
struct  ArrayUtilsExample_t1078130992  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] ArrayUtilsExample::exampleArrayForPrintAndToString
	GameObjectU5BU5D_t3057952154* ___exampleArrayForPrintAndToString_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ArrayUtilsExample::exampleListForPrintAndToString
	List_1_t1125654279 * ___exampleListForPrintAndToString_3;

public:
	inline static int32_t get_offset_of_exampleArrayForPrintAndToString_2() { return static_cast<int32_t>(offsetof(ArrayUtilsExample_t1078130992, ___exampleArrayForPrintAndToString_2)); }
	inline GameObjectU5BU5D_t3057952154* get_exampleArrayForPrintAndToString_2() const { return ___exampleArrayForPrintAndToString_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_exampleArrayForPrintAndToString_2() { return &___exampleArrayForPrintAndToString_2; }
	inline void set_exampleArrayForPrintAndToString_2(GameObjectU5BU5D_t3057952154* value)
	{
		___exampleArrayForPrintAndToString_2 = value;
		Il2CppCodeGenWriteBarrier(&___exampleArrayForPrintAndToString_2, value);
	}

	inline static int32_t get_offset_of_exampleListForPrintAndToString_3() { return static_cast<int32_t>(offsetof(ArrayUtilsExample_t1078130992, ___exampleListForPrintAndToString_3)); }
	inline List_1_t1125654279 * get_exampleListForPrintAndToString_3() const { return ___exampleListForPrintAndToString_3; }
	inline List_1_t1125654279 ** get_address_of_exampleListForPrintAndToString_3() { return &___exampleListForPrintAndToString_3; }
	inline void set_exampleListForPrintAndToString_3(List_1_t1125654279 * value)
	{
		___exampleListForPrintAndToString_3 = value;
		Il2CppCodeGenWriteBarrier(&___exampleListForPrintAndToString_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
