﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<GameDevToolkit.Sound.SoundLayer>
struct List_1_t1557583718;
// GameDevToolkit.Sound.SoundLayer
struct SoundLayer_t2188462586;
// System.Collections.Generic.List`1<GameDevToolkit.Sound.AudibleSound>
struct List_1_t1671175883;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// GameDevToolkit.Sound.SoundManager
struct SoundManager_t13585936;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Sound.SoundManager
struct  SoundManager_t13585936  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<GameDevToolkit.Sound.SoundLayer> GameDevToolkit.Sound.SoundManager::layers
	List_1_t1557583718 * ___layers_3;
	// GameDevToolkit.Sound.SoundLayer GameDevToolkit.Sound.SoundManager::defaultLayer
	SoundLayer_t2188462586 * ___defaultLayer_4;
	// System.Collections.Generic.List`1<GameDevToolkit.Sound.AudibleSound> GameDevToolkit.Sound.SoundManager::currentlyPlayingSounds
	List_1_t1671175883 * ___currentlyPlayingSounds_5;
	// UnityEngine.GameObject GameDevToolkit.Sound.SoundManager::defaultGameObject
	GameObject_t1756533147 * ___defaultGameObject_6;

public:
	inline static int32_t get_offset_of_layers_3() { return static_cast<int32_t>(offsetof(SoundManager_t13585936, ___layers_3)); }
	inline List_1_t1557583718 * get_layers_3() const { return ___layers_3; }
	inline List_1_t1557583718 ** get_address_of_layers_3() { return &___layers_3; }
	inline void set_layers_3(List_1_t1557583718 * value)
	{
		___layers_3 = value;
		Il2CppCodeGenWriteBarrier(&___layers_3, value);
	}

	inline static int32_t get_offset_of_defaultLayer_4() { return static_cast<int32_t>(offsetof(SoundManager_t13585936, ___defaultLayer_4)); }
	inline SoundLayer_t2188462586 * get_defaultLayer_4() const { return ___defaultLayer_4; }
	inline SoundLayer_t2188462586 ** get_address_of_defaultLayer_4() { return &___defaultLayer_4; }
	inline void set_defaultLayer_4(SoundLayer_t2188462586 * value)
	{
		___defaultLayer_4 = value;
		Il2CppCodeGenWriteBarrier(&___defaultLayer_4, value);
	}

	inline static int32_t get_offset_of_currentlyPlayingSounds_5() { return static_cast<int32_t>(offsetof(SoundManager_t13585936, ___currentlyPlayingSounds_5)); }
	inline List_1_t1671175883 * get_currentlyPlayingSounds_5() const { return ___currentlyPlayingSounds_5; }
	inline List_1_t1671175883 ** get_address_of_currentlyPlayingSounds_5() { return &___currentlyPlayingSounds_5; }
	inline void set_currentlyPlayingSounds_5(List_1_t1671175883 * value)
	{
		___currentlyPlayingSounds_5 = value;
		Il2CppCodeGenWriteBarrier(&___currentlyPlayingSounds_5, value);
	}

	inline static int32_t get_offset_of_defaultGameObject_6() { return static_cast<int32_t>(offsetof(SoundManager_t13585936, ___defaultGameObject_6)); }
	inline GameObject_t1756533147 * get_defaultGameObject_6() const { return ___defaultGameObject_6; }
	inline GameObject_t1756533147 ** get_address_of_defaultGameObject_6() { return &___defaultGameObject_6; }
	inline void set_defaultGameObject_6(GameObject_t1756533147 * value)
	{
		___defaultGameObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___defaultGameObject_6, value);
	}
};

struct SoundManager_t13585936_StaticFields
{
public:
	// GameDevToolkit.Sound.SoundManager GameDevToolkit.Sound.SoundManager::instance
	SoundManager_t13585936 * ___instance_7;

public:
	inline static int32_t get_offset_of_instance_7() { return static_cast<int32_t>(offsetof(SoundManager_t13585936_StaticFields, ___instance_7)); }
	inline SoundManager_t13585936 * get_instance_7() const { return ___instance_7; }
	inline SoundManager_t13585936 ** get_address_of_instance_7() { return &___instance_7; }
	inline void set_instance_7(SoundManager_t13585936 * value)
	{
		___instance_7 = value;
		Il2CppCodeGenWriteBarrier(&___instance_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
