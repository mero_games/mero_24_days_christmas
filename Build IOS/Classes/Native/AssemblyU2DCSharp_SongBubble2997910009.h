﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.Collider2D
struct Collider2D_t646061738;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SongBubble
struct  SongBubble_t2997910009  : public MonoBehaviour_t1158329972
{
public:
	// System.Single SongBubble::lifetime
	float ___lifetime_2;
	// UnityEngine.ParticleSystem SongBubble::particle
	ParticleSystem_t3394631041 * ___particle_3;
	// UnityEngine.Collider2D SongBubble::col
	Collider2D_t646061738 * ___col_4;

public:
	inline static int32_t get_offset_of_lifetime_2() { return static_cast<int32_t>(offsetof(SongBubble_t2997910009, ___lifetime_2)); }
	inline float get_lifetime_2() const { return ___lifetime_2; }
	inline float* get_address_of_lifetime_2() { return &___lifetime_2; }
	inline void set_lifetime_2(float value)
	{
		___lifetime_2 = value;
	}

	inline static int32_t get_offset_of_particle_3() { return static_cast<int32_t>(offsetof(SongBubble_t2997910009, ___particle_3)); }
	inline ParticleSystem_t3394631041 * get_particle_3() const { return ___particle_3; }
	inline ParticleSystem_t3394631041 ** get_address_of_particle_3() { return &___particle_3; }
	inline void set_particle_3(ParticleSystem_t3394631041 * value)
	{
		___particle_3 = value;
		Il2CppCodeGenWriteBarrier(&___particle_3, value);
	}

	inline static int32_t get_offset_of_col_4() { return static_cast<int32_t>(offsetof(SongBubble_t2997910009, ___col_4)); }
	inline Collider2D_t646061738 * get_col_4() const { return ___col_4; }
	inline Collider2D_t646061738 ** get_address_of_col_4() { return &___col_4; }
	inline void set_col_4(Collider2D_t646061738 * value)
	{
		___col_4 = value;
		Il2CppCodeGenWriteBarrier(&___col_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
