﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Boolean[]
struct BooleanU5BU5D_t3568034315;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameFifteenManager/LightCompleted
struct  LightCompleted_t2236120434 
{
public:
	// System.Boolean GameFifteenManager/LightCompleted::completed
	bool ___completed_0;
	// System.Boolean[] GameFifteenManager/LightCompleted::list
	BooleanU5BU5D_t3568034315* ___list_1;

public:
	inline static int32_t get_offset_of_completed_0() { return static_cast<int32_t>(offsetof(LightCompleted_t2236120434, ___completed_0)); }
	inline bool get_completed_0() const { return ___completed_0; }
	inline bool* get_address_of_completed_0() { return &___completed_0; }
	inline void set_completed_0(bool value)
	{
		___completed_0 = value;
	}

	inline static int32_t get_offset_of_list_1() { return static_cast<int32_t>(offsetof(LightCompleted_t2236120434, ___list_1)); }
	inline BooleanU5BU5D_t3568034315* get_list_1() const { return ___list_1; }
	inline BooleanU5BU5D_t3568034315** get_address_of_list_1() { return &___list_1; }
	inline void set_list_1(BooleanU5BU5D_t3568034315* value)
	{
		___list_1 = value;
		Il2CppCodeGenWriteBarrier(&___list_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GameFifteenManager/LightCompleted
struct LightCompleted_t2236120434_marshaled_pinvoke
{
	int32_t ___completed_0;
	int32_t* ___list_1;
};
// Native definition for COM marshalling of GameFifteenManager/LightCompleted
struct LightCompleted_t2236120434_marshaled_com
{
	int32_t ___completed_0;
	int32_t* ___list_1;
};
