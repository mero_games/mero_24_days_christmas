﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen2596285680.h"

// UnityEngine.Sprite
struct Sprite_t309593783;
// SnowMan[]
struct SnowManU5BU5D_t1663851096;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSevenManager
struct  GameSevenManager_t3596770590  : public SingletonBase_1_t2596285680
{
public:
	// System.Boolean GameSevenManager::isPlaying
	bool ___isPlaying_4;
	// System.Int32 GameSevenManager::hitCounter
	int32_t ___hitCounter_5;
	// System.Int32 GameSevenManager::hitsToLand
	int32_t ___hitsToLand_6;
	// System.Int32 GameSevenManager::chanceOfHappy
	int32_t ___chanceOfHappy_7;
	// System.Int32 GameSevenManager::risenCounter
	int32_t ___risenCounter_8;
	// System.Single GameSevenManager::upStandTime
	float ___upStandTime_9;
	// System.Single GameSevenManager::moveTime
	float ___moveTime_10;
	// System.Single GameSevenManager::riseInterval
	float ___riseInterval_11;
	// UnityEngine.Sprite GameSevenManager::mouthHappy
	Sprite_t309593783 * ___mouthHappy_12;
	// UnityEngine.Sprite GameSevenManager::mouthSad
	Sprite_t309593783 * ___mouthSad_13;
	// SnowMan[] GameSevenManager::snowmen
	SnowManU5BU5D_t1663851096* ___snowmen_14;
	// UnityEngine.GameObject GameSevenManager::snowBall
	GameObject_t1756533147 * ___snowBall_15;

public:
	inline static int32_t get_offset_of_isPlaying_4() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___isPlaying_4)); }
	inline bool get_isPlaying_4() const { return ___isPlaying_4; }
	inline bool* get_address_of_isPlaying_4() { return &___isPlaying_4; }
	inline void set_isPlaying_4(bool value)
	{
		___isPlaying_4 = value;
	}

	inline static int32_t get_offset_of_hitCounter_5() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___hitCounter_5)); }
	inline int32_t get_hitCounter_5() const { return ___hitCounter_5; }
	inline int32_t* get_address_of_hitCounter_5() { return &___hitCounter_5; }
	inline void set_hitCounter_5(int32_t value)
	{
		___hitCounter_5 = value;
	}

	inline static int32_t get_offset_of_hitsToLand_6() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___hitsToLand_6)); }
	inline int32_t get_hitsToLand_6() const { return ___hitsToLand_6; }
	inline int32_t* get_address_of_hitsToLand_6() { return &___hitsToLand_6; }
	inline void set_hitsToLand_6(int32_t value)
	{
		___hitsToLand_6 = value;
	}

	inline static int32_t get_offset_of_chanceOfHappy_7() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___chanceOfHappy_7)); }
	inline int32_t get_chanceOfHappy_7() const { return ___chanceOfHappy_7; }
	inline int32_t* get_address_of_chanceOfHappy_7() { return &___chanceOfHappy_7; }
	inline void set_chanceOfHappy_7(int32_t value)
	{
		___chanceOfHappy_7 = value;
	}

	inline static int32_t get_offset_of_risenCounter_8() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___risenCounter_8)); }
	inline int32_t get_risenCounter_8() const { return ___risenCounter_8; }
	inline int32_t* get_address_of_risenCounter_8() { return &___risenCounter_8; }
	inline void set_risenCounter_8(int32_t value)
	{
		___risenCounter_8 = value;
	}

	inline static int32_t get_offset_of_upStandTime_9() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___upStandTime_9)); }
	inline float get_upStandTime_9() const { return ___upStandTime_9; }
	inline float* get_address_of_upStandTime_9() { return &___upStandTime_9; }
	inline void set_upStandTime_9(float value)
	{
		___upStandTime_9 = value;
	}

	inline static int32_t get_offset_of_moveTime_10() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___moveTime_10)); }
	inline float get_moveTime_10() const { return ___moveTime_10; }
	inline float* get_address_of_moveTime_10() { return &___moveTime_10; }
	inline void set_moveTime_10(float value)
	{
		___moveTime_10 = value;
	}

	inline static int32_t get_offset_of_riseInterval_11() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___riseInterval_11)); }
	inline float get_riseInterval_11() const { return ___riseInterval_11; }
	inline float* get_address_of_riseInterval_11() { return &___riseInterval_11; }
	inline void set_riseInterval_11(float value)
	{
		___riseInterval_11 = value;
	}

	inline static int32_t get_offset_of_mouthHappy_12() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___mouthHappy_12)); }
	inline Sprite_t309593783 * get_mouthHappy_12() const { return ___mouthHappy_12; }
	inline Sprite_t309593783 ** get_address_of_mouthHappy_12() { return &___mouthHappy_12; }
	inline void set_mouthHappy_12(Sprite_t309593783 * value)
	{
		___mouthHappy_12 = value;
		Il2CppCodeGenWriteBarrier(&___mouthHappy_12, value);
	}

	inline static int32_t get_offset_of_mouthSad_13() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___mouthSad_13)); }
	inline Sprite_t309593783 * get_mouthSad_13() const { return ___mouthSad_13; }
	inline Sprite_t309593783 ** get_address_of_mouthSad_13() { return &___mouthSad_13; }
	inline void set_mouthSad_13(Sprite_t309593783 * value)
	{
		___mouthSad_13 = value;
		Il2CppCodeGenWriteBarrier(&___mouthSad_13, value);
	}

	inline static int32_t get_offset_of_snowmen_14() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___snowmen_14)); }
	inline SnowManU5BU5D_t1663851096* get_snowmen_14() const { return ___snowmen_14; }
	inline SnowManU5BU5D_t1663851096** get_address_of_snowmen_14() { return &___snowmen_14; }
	inline void set_snowmen_14(SnowManU5BU5D_t1663851096* value)
	{
		___snowmen_14 = value;
		Il2CppCodeGenWriteBarrier(&___snowmen_14, value);
	}

	inline static int32_t get_offset_of_snowBall_15() { return static_cast<int32_t>(offsetof(GameSevenManager_t3596770590, ___snowBall_15)); }
	inline GameObject_t1756533147 * get_snowBall_15() const { return ___snowBall_15; }
	inline GameObject_t1756533147 ** get_address_of_snowBall_15() { return &___snowBall_15; }
	inline void set_snowBall_15(GameObject_t1756533147 * value)
	{
		___snowBall_15 = value;
		Il2CppCodeGenWriteBarrier(&___snowBall_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
