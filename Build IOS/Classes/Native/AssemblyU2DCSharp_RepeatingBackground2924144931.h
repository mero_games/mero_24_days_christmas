﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.BoxCollider2D
struct BoxCollider2D_t948534547;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RepeatingBackground
struct  RepeatingBackground_t2924144931  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.BoxCollider2D RepeatingBackground::boxCollider
	BoxCollider2D_t948534547 * ___boxCollider_2;
	// System.Single RepeatingBackground::backgroundLenght
	float ___backgroundLenght_3;

public:
	inline static int32_t get_offset_of_boxCollider_2() { return static_cast<int32_t>(offsetof(RepeatingBackground_t2924144931, ___boxCollider_2)); }
	inline BoxCollider2D_t948534547 * get_boxCollider_2() const { return ___boxCollider_2; }
	inline BoxCollider2D_t948534547 ** get_address_of_boxCollider_2() { return &___boxCollider_2; }
	inline void set_boxCollider_2(BoxCollider2D_t948534547 * value)
	{
		___boxCollider_2 = value;
		Il2CppCodeGenWriteBarrier(&___boxCollider_2, value);
	}

	inline static int32_t get_offset_of_backgroundLenght_3() { return static_cast<int32_t>(offsetof(RepeatingBackground_t2924144931, ___backgroundLenght_3)); }
	inline float get_backgroundLenght_3() const { return ___backgroundLenght_3; }
	inline float* get_address_of_backgroundLenght_3() { return &___backgroundLenght_3; }
	inline void set_backgroundLenght_3(float value)
	{
		___backgroundLenght_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
