﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_ParentalControl_Type3248676223.h"

// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParentalControl
struct  ParentalControl_t2040855302  : public MonoBehaviour_t1158329972
{
public:
	// ParentalControl/Type ParentalControl::type
	int32_t ___type_2;
	// UnityEngine.Collider2D ParentalControl::buttonLeft
	Collider2D_t646061738 * ___buttonLeft_3;
	// UnityEngine.Collider2D ParentalControl::buttonMiddle
	Collider2D_t646061738 * ___buttonMiddle_4;
	// UnityEngine.Collider2D ParentalControl::buttonRight
	Collider2D_t646061738 * ___buttonRight_5;
	// UnityEngine.Collider2D ParentalControl::xButton
	Collider2D_t646061738 * ___xButton_6;
	// UnityEngine.UI.Text ParentalControl::equationText
	Text_t356221433 * ___equationText_7;
	// UnityEngine.UI.Text[] ParentalControl::texts
	TextU5BU5D_t4216439300* ___texts_8;
	// System.Int32 ParentalControl::number1
	int32_t ___number1_9;
	// System.Int32 ParentalControl::number2
	int32_t ___number2_10;
	// System.Int32 ParentalControl::correctAnswer
	int32_t ___correctAnswer_11;
	// System.Int32 ParentalControl::wrongAnswer1
	int32_t ___wrongAnswer1_12;
	// System.Int32 ParentalControl::wrongAnswer2
	int32_t ___wrongAnswer2_13;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_buttonLeft_3() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___buttonLeft_3)); }
	inline Collider2D_t646061738 * get_buttonLeft_3() const { return ___buttonLeft_3; }
	inline Collider2D_t646061738 ** get_address_of_buttonLeft_3() { return &___buttonLeft_3; }
	inline void set_buttonLeft_3(Collider2D_t646061738 * value)
	{
		___buttonLeft_3 = value;
		Il2CppCodeGenWriteBarrier(&___buttonLeft_3, value);
	}

	inline static int32_t get_offset_of_buttonMiddle_4() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___buttonMiddle_4)); }
	inline Collider2D_t646061738 * get_buttonMiddle_4() const { return ___buttonMiddle_4; }
	inline Collider2D_t646061738 ** get_address_of_buttonMiddle_4() { return &___buttonMiddle_4; }
	inline void set_buttonMiddle_4(Collider2D_t646061738 * value)
	{
		___buttonMiddle_4 = value;
		Il2CppCodeGenWriteBarrier(&___buttonMiddle_4, value);
	}

	inline static int32_t get_offset_of_buttonRight_5() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___buttonRight_5)); }
	inline Collider2D_t646061738 * get_buttonRight_5() const { return ___buttonRight_5; }
	inline Collider2D_t646061738 ** get_address_of_buttonRight_5() { return &___buttonRight_5; }
	inline void set_buttonRight_5(Collider2D_t646061738 * value)
	{
		___buttonRight_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonRight_5, value);
	}

	inline static int32_t get_offset_of_xButton_6() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___xButton_6)); }
	inline Collider2D_t646061738 * get_xButton_6() const { return ___xButton_6; }
	inline Collider2D_t646061738 ** get_address_of_xButton_6() { return &___xButton_6; }
	inline void set_xButton_6(Collider2D_t646061738 * value)
	{
		___xButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___xButton_6, value);
	}

	inline static int32_t get_offset_of_equationText_7() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___equationText_7)); }
	inline Text_t356221433 * get_equationText_7() const { return ___equationText_7; }
	inline Text_t356221433 ** get_address_of_equationText_7() { return &___equationText_7; }
	inline void set_equationText_7(Text_t356221433 * value)
	{
		___equationText_7 = value;
		Il2CppCodeGenWriteBarrier(&___equationText_7, value);
	}

	inline static int32_t get_offset_of_texts_8() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___texts_8)); }
	inline TextU5BU5D_t4216439300* get_texts_8() const { return ___texts_8; }
	inline TextU5BU5D_t4216439300** get_address_of_texts_8() { return &___texts_8; }
	inline void set_texts_8(TextU5BU5D_t4216439300* value)
	{
		___texts_8 = value;
		Il2CppCodeGenWriteBarrier(&___texts_8, value);
	}

	inline static int32_t get_offset_of_number1_9() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___number1_9)); }
	inline int32_t get_number1_9() const { return ___number1_9; }
	inline int32_t* get_address_of_number1_9() { return &___number1_9; }
	inline void set_number1_9(int32_t value)
	{
		___number1_9 = value;
	}

	inline static int32_t get_offset_of_number2_10() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___number2_10)); }
	inline int32_t get_number2_10() const { return ___number2_10; }
	inline int32_t* get_address_of_number2_10() { return &___number2_10; }
	inline void set_number2_10(int32_t value)
	{
		___number2_10 = value;
	}

	inline static int32_t get_offset_of_correctAnswer_11() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___correctAnswer_11)); }
	inline int32_t get_correctAnswer_11() const { return ___correctAnswer_11; }
	inline int32_t* get_address_of_correctAnswer_11() { return &___correctAnswer_11; }
	inline void set_correctAnswer_11(int32_t value)
	{
		___correctAnswer_11 = value;
	}

	inline static int32_t get_offset_of_wrongAnswer1_12() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___wrongAnswer1_12)); }
	inline int32_t get_wrongAnswer1_12() const { return ___wrongAnswer1_12; }
	inline int32_t* get_address_of_wrongAnswer1_12() { return &___wrongAnswer1_12; }
	inline void set_wrongAnswer1_12(int32_t value)
	{
		___wrongAnswer1_12 = value;
	}

	inline static int32_t get_offset_of_wrongAnswer2_13() { return static_cast<int32_t>(offsetof(ParentalControl_t2040855302, ___wrongAnswer2_13)); }
	inline int32_t get_wrongAnswer2_13() const { return ___wrongAnswer2_13; }
	inline int32_t* get_address_of_wrongAnswer2_13() { return &___wrongAnswer2_13; }
	inline void set_wrongAnswer2_13(int32_t value)
	{
		___wrongAnswer2_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
