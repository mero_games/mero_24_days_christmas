﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<Difference>
struct List_1_t349521913;
// System.Action`1<Difference>
struct Action_1_t782200163;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DifferenceCollection
struct  DifferenceCollection_t1428269411  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<Difference> DifferenceCollection::differences
	List_1_t349521913 * ___differences_2;

public:
	inline static int32_t get_offset_of_differences_2() { return static_cast<int32_t>(offsetof(DifferenceCollection_t1428269411, ___differences_2)); }
	inline List_1_t349521913 * get_differences_2() const { return ___differences_2; }
	inline List_1_t349521913 ** get_address_of_differences_2() { return &___differences_2; }
	inline void set_differences_2(List_1_t349521913 * value)
	{
		___differences_2 = value;
		Il2CppCodeGenWriteBarrier(&___differences_2, value);
	}
};

struct DifferenceCollection_t1428269411_StaticFields
{
public:
	// System.Action`1<Difference> DifferenceCollection::<>f__am$cache0
	Action_1_t782200163 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(DifferenceCollection_t1428269411_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Action_1_t782200163 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Action_1_t782200163 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Action_1_t782200163 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
