﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen245993964.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTwentyManager
struct  GameTwentyManager_t1246478874  : public SingletonBase_1_t245993964
{
public:
	// System.Boolean GameTwentyManager::canDraw
	bool ___canDraw_4;
	// UnityEngine.GameObject GameTwentyManager::pencil
	GameObject_t1756533147 * ___pencil_5;
	// System.Int32 GameTwentyManager::wrongCollidersHit
	int32_t ___wrongCollidersHit_6;
	// System.Int32 GameTwentyManager::correctCollidersLeft
	int32_t ___correctCollidersLeft_7;
	// UnityEngine.Transform GameTwentyManager::shapes
	Transform_t3275118058 * ___shapes_8;
	// UnityEngine.Transform GameTwentyManager::drawingsHolder
	Transform_t3275118058 * ___drawingsHolder_9;
	// UnityEngine.Transform GameTwentyManager::correctColliders
	Transform_t3275118058 * ___correctColliders_10;
	// System.Int32 GameTwentyManager::shapeIndex
	int32_t ___shapeIndex_11;
	// UnityEngine.GameObject GameTwentyManager::iPencil
	GameObject_t1756533147 * ___iPencil_12;

public:
	inline static int32_t get_offset_of_canDraw_4() { return static_cast<int32_t>(offsetof(GameTwentyManager_t1246478874, ___canDraw_4)); }
	inline bool get_canDraw_4() const { return ___canDraw_4; }
	inline bool* get_address_of_canDraw_4() { return &___canDraw_4; }
	inline void set_canDraw_4(bool value)
	{
		___canDraw_4 = value;
	}

	inline static int32_t get_offset_of_pencil_5() { return static_cast<int32_t>(offsetof(GameTwentyManager_t1246478874, ___pencil_5)); }
	inline GameObject_t1756533147 * get_pencil_5() const { return ___pencil_5; }
	inline GameObject_t1756533147 ** get_address_of_pencil_5() { return &___pencil_5; }
	inline void set_pencil_5(GameObject_t1756533147 * value)
	{
		___pencil_5 = value;
		Il2CppCodeGenWriteBarrier(&___pencil_5, value);
	}

	inline static int32_t get_offset_of_wrongCollidersHit_6() { return static_cast<int32_t>(offsetof(GameTwentyManager_t1246478874, ___wrongCollidersHit_6)); }
	inline int32_t get_wrongCollidersHit_6() const { return ___wrongCollidersHit_6; }
	inline int32_t* get_address_of_wrongCollidersHit_6() { return &___wrongCollidersHit_6; }
	inline void set_wrongCollidersHit_6(int32_t value)
	{
		___wrongCollidersHit_6 = value;
	}

	inline static int32_t get_offset_of_correctCollidersLeft_7() { return static_cast<int32_t>(offsetof(GameTwentyManager_t1246478874, ___correctCollidersLeft_7)); }
	inline int32_t get_correctCollidersLeft_7() const { return ___correctCollidersLeft_7; }
	inline int32_t* get_address_of_correctCollidersLeft_7() { return &___correctCollidersLeft_7; }
	inline void set_correctCollidersLeft_7(int32_t value)
	{
		___correctCollidersLeft_7 = value;
	}

	inline static int32_t get_offset_of_shapes_8() { return static_cast<int32_t>(offsetof(GameTwentyManager_t1246478874, ___shapes_8)); }
	inline Transform_t3275118058 * get_shapes_8() const { return ___shapes_8; }
	inline Transform_t3275118058 ** get_address_of_shapes_8() { return &___shapes_8; }
	inline void set_shapes_8(Transform_t3275118058 * value)
	{
		___shapes_8 = value;
		Il2CppCodeGenWriteBarrier(&___shapes_8, value);
	}

	inline static int32_t get_offset_of_drawingsHolder_9() { return static_cast<int32_t>(offsetof(GameTwentyManager_t1246478874, ___drawingsHolder_9)); }
	inline Transform_t3275118058 * get_drawingsHolder_9() const { return ___drawingsHolder_9; }
	inline Transform_t3275118058 ** get_address_of_drawingsHolder_9() { return &___drawingsHolder_9; }
	inline void set_drawingsHolder_9(Transform_t3275118058 * value)
	{
		___drawingsHolder_9 = value;
		Il2CppCodeGenWriteBarrier(&___drawingsHolder_9, value);
	}

	inline static int32_t get_offset_of_correctColliders_10() { return static_cast<int32_t>(offsetof(GameTwentyManager_t1246478874, ___correctColliders_10)); }
	inline Transform_t3275118058 * get_correctColliders_10() const { return ___correctColliders_10; }
	inline Transform_t3275118058 ** get_address_of_correctColliders_10() { return &___correctColliders_10; }
	inline void set_correctColliders_10(Transform_t3275118058 * value)
	{
		___correctColliders_10 = value;
		Il2CppCodeGenWriteBarrier(&___correctColliders_10, value);
	}

	inline static int32_t get_offset_of_shapeIndex_11() { return static_cast<int32_t>(offsetof(GameTwentyManager_t1246478874, ___shapeIndex_11)); }
	inline int32_t get_shapeIndex_11() const { return ___shapeIndex_11; }
	inline int32_t* get_address_of_shapeIndex_11() { return &___shapeIndex_11; }
	inline void set_shapeIndex_11(int32_t value)
	{
		___shapeIndex_11 = value;
	}

	inline static int32_t get_offset_of_iPencil_12() { return static_cast<int32_t>(offsetof(GameTwentyManager_t1246478874, ___iPencil_12)); }
	inline GameObject_t1756533147 * get_iPencil_12() const { return ___iPencil_12; }
	inline GameObject_t1756533147 ** get_address_of_iPencil_12() { return &___iPencil_12; }
	inline void set_iPencil_12(GameObject_t1756533147 * value)
	{
		___iPencil_12 = value;
		Il2CppCodeGenWriteBarrier(&___iPencil_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
