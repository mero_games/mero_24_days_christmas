﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Events.ColliderEventSystem/ColliderDelegate
struct ColliderDelegate_t2027680195;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Events.ColliderEventSystem
struct  ColliderEventSystem_t795033329  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Events.ColliderEventSystem/ColliderDelegate GameDevToolkit.Events.ColliderEventSystem::TriggerEntered
	ColliderDelegate_t2027680195 * ___TriggerEntered_2;
	// GameDevToolkit.Events.ColliderEventSystem/ColliderDelegate GameDevToolkit.Events.ColliderEventSystem::TriggerExited
	ColliderDelegate_t2027680195 * ___TriggerExited_3;
	// GameDevToolkit.Events.ColliderEventSystem/ColliderDelegate GameDevToolkit.Events.ColliderEventSystem::ColliderEntered
	ColliderDelegate_t2027680195 * ___ColliderEntered_4;
	// GameDevToolkit.Events.ColliderEventSystem/ColliderDelegate GameDevToolkit.Events.ColliderEventSystem::ColliderExited
	ColliderDelegate_t2027680195 * ___ColliderExited_5;

public:
	inline static int32_t get_offset_of_TriggerEntered_2() { return static_cast<int32_t>(offsetof(ColliderEventSystem_t795033329, ___TriggerEntered_2)); }
	inline ColliderDelegate_t2027680195 * get_TriggerEntered_2() const { return ___TriggerEntered_2; }
	inline ColliderDelegate_t2027680195 ** get_address_of_TriggerEntered_2() { return &___TriggerEntered_2; }
	inline void set_TriggerEntered_2(ColliderDelegate_t2027680195 * value)
	{
		___TriggerEntered_2 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerEntered_2, value);
	}

	inline static int32_t get_offset_of_TriggerExited_3() { return static_cast<int32_t>(offsetof(ColliderEventSystem_t795033329, ___TriggerExited_3)); }
	inline ColliderDelegate_t2027680195 * get_TriggerExited_3() const { return ___TriggerExited_3; }
	inline ColliderDelegate_t2027680195 ** get_address_of_TriggerExited_3() { return &___TriggerExited_3; }
	inline void set_TriggerExited_3(ColliderDelegate_t2027680195 * value)
	{
		___TriggerExited_3 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerExited_3, value);
	}

	inline static int32_t get_offset_of_ColliderEntered_4() { return static_cast<int32_t>(offsetof(ColliderEventSystem_t795033329, ___ColliderEntered_4)); }
	inline ColliderDelegate_t2027680195 * get_ColliderEntered_4() const { return ___ColliderEntered_4; }
	inline ColliderDelegate_t2027680195 ** get_address_of_ColliderEntered_4() { return &___ColliderEntered_4; }
	inline void set_ColliderEntered_4(ColliderDelegate_t2027680195 * value)
	{
		___ColliderEntered_4 = value;
		Il2CppCodeGenWriteBarrier(&___ColliderEntered_4, value);
	}

	inline static int32_t get_offset_of_ColliderExited_5() { return static_cast<int32_t>(offsetof(ColliderEventSystem_t795033329, ___ColliderExited_5)); }
	inline ColliderDelegate_t2027680195 * get_ColliderExited_5() const { return ___ColliderExited_5; }
	inline ColliderDelegate_t2027680195 ** get_address_of_ColliderExited_5() { return &___ColliderExited_5; }
	inline void set_ColliderExited_5(ColliderDelegate_t2027680195 * value)
	{
		___ColliderExited_5 = value;
		Il2CppCodeGenWriteBarrier(&___ColliderExited_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
