﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SnowMan
struct  SnowMan_t902289717  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.SpriteRenderer SnowMan::mouth
	SpriteRenderer_t1209076198 * ___mouth_2;
	// System.Boolean SnowMan::isHappy
	bool ___isHappy_3;
	// System.Boolean SnowMan::wasHit
	bool ___wasHit_4;
	// System.Boolean SnowMan::isDown
	bool ___isDown_5;

public:
	inline static int32_t get_offset_of_mouth_2() { return static_cast<int32_t>(offsetof(SnowMan_t902289717, ___mouth_2)); }
	inline SpriteRenderer_t1209076198 * get_mouth_2() const { return ___mouth_2; }
	inline SpriteRenderer_t1209076198 ** get_address_of_mouth_2() { return &___mouth_2; }
	inline void set_mouth_2(SpriteRenderer_t1209076198 * value)
	{
		___mouth_2 = value;
		Il2CppCodeGenWriteBarrier(&___mouth_2, value);
	}

	inline static int32_t get_offset_of_isHappy_3() { return static_cast<int32_t>(offsetof(SnowMan_t902289717, ___isHappy_3)); }
	inline bool get_isHappy_3() const { return ___isHappy_3; }
	inline bool* get_address_of_isHappy_3() { return &___isHappy_3; }
	inline void set_isHappy_3(bool value)
	{
		___isHappy_3 = value;
	}

	inline static int32_t get_offset_of_wasHit_4() { return static_cast<int32_t>(offsetof(SnowMan_t902289717, ___wasHit_4)); }
	inline bool get_wasHit_4() const { return ___wasHit_4; }
	inline bool* get_address_of_wasHit_4() { return &___wasHit_4; }
	inline void set_wasHit_4(bool value)
	{
		___wasHit_4 = value;
	}

	inline static int32_t get_offset_of_isDown_5() { return static_cast<int32_t>(offsetof(SnowMan_t902289717, ___isDown_5)); }
	inline bool get_isDown_5() const { return ___isDown_5; }
	inline bool* get_address_of_isDown_5() { return &___isDown_5; }
	inline void set_isDown_5(bool value)
	{
		___isDown_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
