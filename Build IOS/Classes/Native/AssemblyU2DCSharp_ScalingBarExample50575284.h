﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.Visual.ScalingBar
struct ScalingBar_t3437833994;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScalingBarExample
struct  ScalingBarExample_t50575284  : public MonoBehaviour_t1158329972
{
public:
	// GameDevToolkit.Visual.ScalingBar ScalingBarExample::progressBar
	ScalingBar_t3437833994 * ___progressBar_2;

public:
	inline static int32_t get_offset_of_progressBar_2() { return static_cast<int32_t>(offsetof(ScalingBarExample_t50575284, ___progressBar_2)); }
	inline ScalingBar_t3437833994 * get_progressBar_2() const { return ___progressBar_2; }
	inline ScalingBar_t3437833994 ** get_address_of_progressBar_2() { return &___progressBar_2; }
	inline void set_progressBar_2(ScalingBar_t3437833994 * value)
	{
		___progressBar_2 = value;
		Il2CppCodeGenWriteBarrier(&___progressBar_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
