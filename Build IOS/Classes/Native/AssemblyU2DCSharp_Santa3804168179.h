﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// FlashingObject
struct FlashingObject_t2247968655;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Santa
struct  Santa_t3804168179  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Santa::force
	float ___force_2;
	// UnityEngine.ParticleSystem Santa::ps
	ParticleSystem_t3394631041 * ___ps_3;
	// UnityEngine.Transform Santa::particlePosition
	Transform_t3275118058 * ___particlePosition_4;
	// UnityEngine.Rigidbody2D Santa::rb
	Rigidbody2D_t502193897 * ___rb_5;
	// FlashingObject Santa::flash
	FlashingObject_t2247968655 * ___flash_6;
	// System.Boolean Santa::wasHit
	bool ___wasHit_7;
	// System.Boolean Santa::isAtTop
	bool ___isAtTop_8;
	// UnityEngine.Rect Santa::cameraRect
	Rect_t3681755626  ___cameraRect_9;
	// UnityEngine.Bounds Santa::santaBounds
	Bounds_t3033363703  ___santaBounds_10;

public:
	inline static int32_t get_offset_of_force_2() { return static_cast<int32_t>(offsetof(Santa_t3804168179, ___force_2)); }
	inline float get_force_2() const { return ___force_2; }
	inline float* get_address_of_force_2() { return &___force_2; }
	inline void set_force_2(float value)
	{
		___force_2 = value;
	}

	inline static int32_t get_offset_of_ps_3() { return static_cast<int32_t>(offsetof(Santa_t3804168179, ___ps_3)); }
	inline ParticleSystem_t3394631041 * get_ps_3() const { return ___ps_3; }
	inline ParticleSystem_t3394631041 ** get_address_of_ps_3() { return &___ps_3; }
	inline void set_ps_3(ParticleSystem_t3394631041 * value)
	{
		___ps_3 = value;
		Il2CppCodeGenWriteBarrier(&___ps_3, value);
	}

	inline static int32_t get_offset_of_particlePosition_4() { return static_cast<int32_t>(offsetof(Santa_t3804168179, ___particlePosition_4)); }
	inline Transform_t3275118058 * get_particlePosition_4() const { return ___particlePosition_4; }
	inline Transform_t3275118058 ** get_address_of_particlePosition_4() { return &___particlePosition_4; }
	inline void set_particlePosition_4(Transform_t3275118058 * value)
	{
		___particlePosition_4 = value;
		Il2CppCodeGenWriteBarrier(&___particlePosition_4, value);
	}

	inline static int32_t get_offset_of_rb_5() { return static_cast<int32_t>(offsetof(Santa_t3804168179, ___rb_5)); }
	inline Rigidbody2D_t502193897 * get_rb_5() const { return ___rb_5; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb_5() { return &___rb_5; }
	inline void set_rb_5(Rigidbody2D_t502193897 * value)
	{
		___rb_5 = value;
		Il2CppCodeGenWriteBarrier(&___rb_5, value);
	}

	inline static int32_t get_offset_of_flash_6() { return static_cast<int32_t>(offsetof(Santa_t3804168179, ___flash_6)); }
	inline FlashingObject_t2247968655 * get_flash_6() const { return ___flash_6; }
	inline FlashingObject_t2247968655 ** get_address_of_flash_6() { return &___flash_6; }
	inline void set_flash_6(FlashingObject_t2247968655 * value)
	{
		___flash_6 = value;
		Il2CppCodeGenWriteBarrier(&___flash_6, value);
	}

	inline static int32_t get_offset_of_wasHit_7() { return static_cast<int32_t>(offsetof(Santa_t3804168179, ___wasHit_7)); }
	inline bool get_wasHit_7() const { return ___wasHit_7; }
	inline bool* get_address_of_wasHit_7() { return &___wasHit_7; }
	inline void set_wasHit_7(bool value)
	{
		___wasHit_7 = value;
	}

	inline static int32_t get_offset_of_isAtTop_8() { return static_cast<int32_t>(offsetof(Santa_t3804168179, ___isAtTop_8)); }
	inline bool get_isAtTop_8() const { return ___isAtTop_8; }
	inline bool* get_address_of_isAtTop_8() { return &___isAtTop_8; }
	inline void set_isAtTop_8(bool value)
	{
		___isAtTop_8 = value;
	}

	inline static int32_t get_offset_of_cameraRect_9() { return static_cast<int32_t>(offsetof(Santa_t3804168179, ___cameraRect_9)); }
	inline Rect_t3681755626  get_cameraRect_9() const { return ___cameraRect_9; }
	inline Rect_t3681755626 * get_address_of_cameraRect_9() { return &___cameraRect_9; }
	inline void set_cameraRect_9(Rect_t3681755626  value)
	{
		___cameraRect_9 = value;
	}

	inline static int32_t get_offset_of_santaBounds_10() { return static_cast<int32_t>(offsetof(Santa_t3804168179, ___santaBounds_10)); }
	inline Bounds_t3033363703  get_santaBounds_10() const { return ___santaBounds_10; }
	inline Bounds_t3033363703 * get_address_of_santaBounds_10() { return &___santaBounds_10; }
	inline void set_santaBounds_10(Bounds_t3033363703  value)
	{
		___santaBounds_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
