﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.GameUtil.RandomizerRules.SemiRandomRule
struct  SemiRandomRule_t1463926147  : public Il2CppObject
{
public:
	// System.Int32 GameDevToolkit.GameUtil.RandomizerRules.SemiRandomRule::asked
	int32_t ___asked_0;
	// System.Int32 GameDevToolkit.GameUtil.RandomizerRules.SemiRandomRule::min
	int32_t ___min_1;
	// System.Int32 GameDevToolkit.GameUtil.RandomizerRules.SemiRandomRule::lastBlock
	int32_t ___lastBlock_2;

public:
	inline static int32_t get_offset_of_asked_0() { return static_cast<int32_t>(offsetof(SemiRandomRule_t1463926147, ___asked_0)); }
	inline int32_t get_asked_0() const { return ___asked_0; }
	inline int32_t* get_address_of_asked_0() { return &___asked_0; }
	inline void set_asked_0(int32_t value)
	{
		___asked_0 = value;
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(SemiRandomRule_t1463926147, ___min_1)); }
	inline int32_t get_min_1() const { return ___min_1; }
	inline int32_t* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(int32_t value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_lastBlock_2() { return static_cast<int32_t>(offsetof(SemiRandomRule_t1463926147, ___lastBlock_2)); }
	inline int32_t get_lastBlock_2() const { return ___lastBlock_2; }
	inline int32_t* get_address_of_lastBlock_2() { return &___lastBlock_2; }
	inline void set_lastBlock_2(int32_t value)
	{
		___lastBlock_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
