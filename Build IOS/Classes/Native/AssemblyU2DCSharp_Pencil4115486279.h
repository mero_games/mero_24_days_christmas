﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pencil
struct  Pencil_t4115486279  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource Pencil::drawLoopSound
	AudioSource_t1135106623 * ___drawLoopSound_2;
	// System.Boolean Pencil::toMove
	bool ___toMove_3;

public:
	inline static int32_t get_offset_of_drawLoopSound_2() { return static_cast<int32_t>(offsetof(Pencil_t4115486279, ___drawLoopSound_2)); }
	inline AudioSource_t1135106623 * get_drawLoopSound_2() const { return ___drawLoopSound_2; }
	inline AudioSource_t1135106623 ** get_address_of_drawLoopSound_2() { return &___drawLoopSound_2; }
	inline void set_drawLoopSound_2(AudioSource_t1135106623 * value)
	{
		___drawLoopSound_2 = value;
		Il2CppCodeGenWriteBarrier(&___drawLoopSound_2, value);
	}

	inline static int32_t get_offset_of_toMove_3() { return static_cast<int32_t>(offsetof(Pencil_t4115486279, ___toMove_3)); }
	inline bool get_toMove_3() const { return ___toMove_3; }
	inline bool* get_address_of_toMove_3() { return &___toMove_3; }
	inline void set_toMove_3(bool value)
	{
		___toMove_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
