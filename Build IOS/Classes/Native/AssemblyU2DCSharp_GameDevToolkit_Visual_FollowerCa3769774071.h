﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Visual.FollowerCam
struct  FollowerCam_t3769774071  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform GameDevToolkit.Visual.FollowerCam::target
	Transform_t3275118058 * ___target_2;
	// System.Boolean GameDevToolkit.Visual.FollowerCam::followOnY
	bool ___followOnY_3;
	// System.Boolean GameDevToolkit.Visual.FollowerCam::followOnX
	bool ___followOnX_4;
	// System.Boolean GameDevToolkit.Visual.FollowerCam::constrainedOnX
	bool ___constrainedOnX_5;
	// System.Boolean GameDevToolkit.Visual.FollowerCam::constrainedOnY
	bool ___constrainedOnY_6;
	// System.Single GameDevToolkit.Visual.FollowerCam::minY
	float ___minY_7;
	// System.Single GameDevToolkit.Visual.FollowerCam::maxY
	float ___maxY_8;
	// System.Single GameDevToolkit.Visual.FollowerCam::minX
	float ___minX_9;
	// System.Single GameDevToolkit.Visual.FollowerCam::maxX
	float ___maxX_10;
	// UnityEngine.Vector3 GameDevToolkit.Visual.FollowerCam::offset
	Vector3_t2243707580  ___offset_11;
	// UnityEngine.Vector3 GameDevToolkit.Visual.FollowerCam::originalPosition
	Vector3_t2243707580  ___originalPosition_12;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___target_2)); }
	inline Transform_t3275118058 * get_target_2() const { return ___target_2; }
	inline Transform_t3275118058 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3275118058 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier(&___target_2, value);
	}

	inline static int32_t get_offset_of_followOnY_3() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___followOnY_3)); }
	inline bool get_followOnY_3() const { return ___followOnY_3; }
	inline bool* get_address_of_followOnY_3() { return &___followOnY_3; }
	inline void set_followOnY_3(bool value)
	{
		___followOnY_3 = value;
	}

	inline static int32_t get_offset_of_followOnX_4() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___followOnX_4)); }
	inline bool get_followOnX_4() const { return ___followOnX_4; }
	inline bool* get_address_of_followOnX_4() { return &___followOnX_4; }
	inline void set_followOnX_4(bool value)
	{
		___followOnX_4 = value;
	}

	inline static int32_t get_offset_of_constrainedOnX_5() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___constrainedOnX_5)); }
	inline bool get_constrainedOnX_5() const { return ___constrainedOnX_5; }
	inline bool* get_address_of_constrainedOnX_5() { return &___constrainedOnX_5; }
	inline void set_constrainedOnX_5(bool value)
	{
		___constrainedOnX_5 = value;
	}

	inline static int32_t get_offset_of_constrainedOnY_6() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___constrainedOnY_6)); }
	inline bool get_constrainedOnY_6() const { return ___constrainedOnY_6; }
	inline bool* get_address_of_constrainedOnY_6() { return &___constrainedOnY_6; }
	inline void set_constrainedOnY_6(bool value)
	{
		___constrainedOnY_6 = value;
	}

	inline static int32_t get_offset_of_minY_7() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___minY_7)); }
	inline float get_minY_7() const { return ___minY_7; }
	inline float* get_address_of_minY_7() { return &___minY_7; }
	inline void set_minY_7(float value)
	{
		___minY_7 = value;
	}

	inline static int32_t get_offset_of_maxY_8() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___maxY_8)); }
	inline float get_maxY_8() const { return ___maxY_8; }
	inline float* get_address_of_maxY_8() { return &___maxY_8; }
	inline void set_maxY_8(float value)
	{
		___maxY_8 = value;
	}

	inline static int32_t get_offset_of_minX_9() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___minX_9)); }
	inline float get_minX_9() const { return ___minX_9; }
	inline float* get_address_of_minX_9() { return &___minX_9; }
	inline void set_minX_9(float value)
	{
		___minX_9 = value;
	}

	inline static int32_t get_offset_of_maxX_10() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___maxX_10)); }
	inline float get_maxX_10() const { return ___maxX_10; }
	inline float* get_address_of_maxX_10() { return &___maxX_10; }
	inline void set_maxX_10(float value)
	{
		___maxX_10 = value;
	}

	inline static int32_t get_offset_of_offset_11() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___offset_11)); }
	inline Vector3_t2243707580  get_offset_11() const { return ___offset_11; }
	inline Vector3_t2243707580 * get_address_of_offset_11() { return &___offset_11; }
	inline void set_offset_11(Vector3_t2243707580  value)
	{
		___offset_11 = value;
	}

	inline static int32_t get_offset_of_originalPosition_12() { return static_cast<int32_t>(offsetof(FollowerCam_t3769774071, ___originalPosition_12)); }
	inline Vector3_t2243707580  get_originalPosition_12() const { return ___originalPosition_12; }
	inline Vector3_t2243707580 * get_address_of_originalPosition_12() { return &___originalPosition_12; }
	inline void set_originalPosition_12(Vector3_t2243707580  value)
	{
		___originalPosition_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
