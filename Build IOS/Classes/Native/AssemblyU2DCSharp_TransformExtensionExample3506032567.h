﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransformExtensionExample
struct  TransformExtensionExample_t3506032567  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform TransformExtensionExample::goodLookAtChar
	Transform_t3275118058 * ___goodLookAtChar_2;
	// UnityEngine.Transform TransformExtensionExample::badLookAtChar
	Transform_t3275118058 * ___badLookAtChar_3;
	// System.Int32 TransformExtensionExample::currentLogRate
	int32_t ___currentLogRate_5;

public:
	inline static int32_t get_offset_of_goodLookAtChar_2() { return static_cast<int32_t>(offsetof(TransformExtensionExample_t3506032567, ___goodLookAtChar_2)); }
	inline Transform_t3275118058 * get_goodLookAtChar_2() const { return ___goodLookAtChar_2; }
	inline Transform_t3275118058 ** get_address_of_goodLookAtChar_2() { return &___goodLookAtChar_2; }
	inline void set_goodLookAtChar_2(Transform_t3275118058 * value)
	{
		___goodLookAtChar_2 = value;
		Il2CppCodeGenWriteBarrier(&___goodLookAtChar_2, value);
	}

	inline static int32_t get_offset_of_badLookAtChar_3() { return static_cast<int32_t>(offsetof(TransformExtensionExample_t3506032567, ___badLookAtChar_3)); }
	inline Transform_t3275118058 * get_badLookAtChar_3() const { return ___badLookAtChar_3; }
	inline Transform_t3275118058 ** get_address_of_badLookAtChar_3() { return &___badLookAtChar_3; }
	inline void set_badLookAtChar_3(Transform_t3275118058 * value)
	{
		___badLookAtChar_3 = value;
		Il2CppCodeGenWriteBarrier(&___badLookAtChar_3, value);
	}

	inline static int32_t get_offset_of_currentLogRate_5() { return static_cast<int32_t>(offsetof(TransformExtensionExample_t3506032567, ___currentLogRate_5)); }
	inline int32_t get_currentLogRate_5() const { return ___currentLogRate_5; }
	inline int32_t* get_address_of_currentLogRate_5() { return &___currentLogRate_5; }
	inline void set_currentLogRate_5(int32_t value)
	{
		___currentLogRate_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
