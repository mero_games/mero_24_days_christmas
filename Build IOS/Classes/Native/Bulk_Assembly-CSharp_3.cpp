﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_UIButtonBack1890699263.h"
#include "mscorlib_System_Void1841601450.h"
#include "AssemblyU2DCSharp_UIButton3377238306.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharp_NavigationManager4055369543.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "AssemblyU2DCSharp_UIButtonLoadGame1239272532.h"
#include "AssemblyU2DCSharp_EventManager2792515701.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect1199013257.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_UIButtonLoadGame_U3CYLoadSceneU33230658609.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_UIButtonOpenParental1943914589.h"
#include "AssemblyU2DCSharp_UIButtonOpenParental_Type1384202272.h"
#include "AssemblyU2DCSharp_ParentalControl2040855302.h"
#include "AssemblyU2DCSharp_ParentalControl_Type3248676223.h"
#include "AssemblyU2DCSharp_UIButtonShowSlideshow175868935.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_UIButtonTutorial123001136.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "AssemblyU2DCSharp_UIButtonTutorial_U3CYCloseTutori1054761711.h"
#include "AssemblyU2DCSharp_UIButtonTutorial_U3CYOpenTutorial230414610.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EaseType818674011.h"
#include "AssemblyU2DCSharp_UIButtonUpdate766659849.h"
#include "AssemblyU2DCSharp_Utils4194145797.h"
#include "mscorlib_System_Random1044426839.h"
#include "AssemblyU2DCSharp_WaterSnow2237366536.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "AssemblyU2DCSharp_GameElevenManager2036900502.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_SingletonBase_1_gen1036415592.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "AssemblyU2DCSharp_SoundManager654432262.h"
#include "AssemblyU2DCSharp_SoundClip2598932235.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "mscorlib_System_UInt642909196914.h"
#include "UnityEngine_UnityEngine_CircleCollider2D13116344.h"
#include "AssemblyU2DCSharp_WaterSnow_U3CYSnowFallU3Ec__Iter3674066925.h"
#include "AssemblyU2DCSharp_Yey2293551375.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134.h"
#include "AssemblyU2DCSharp_Yey_U3CScaleU3Ec__AnonStorey01001303329.h"

// UIButtonBack
struct UIButtonBack_t1890699263;
// UIButton
struct UIButton_t3377238306;
// System.Action
struct Action_t3226471752;
// System.Object
struct Il2CppObject;
// System.Delegate
struct Delegate_t3022476291;
// NavigationManager
struct NavigationManager_t4055369543;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// UIButtonLoadGame
struct UIButtonLoadGame_t1239272532;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.String
struct String_t;
// UIButtonLoadGame/<YLoadScene>c__Iterator0
struct U3CYLoadSceneU3Ec__Iterator0_t3230658609;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// UIButtonOpenParental
struct UIButtonOpenParental_t1943914589;
// UIButtonShowSlideshow
struct UIButtonShowSlideshow_t175868935;
// UIButtonTutorial
struct UIButtonTutorial_t123001136;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Camera
struct Camera_t189460977;
// UIButtonTutorial/<YCloseTutorial>c__Iterator0
struct U3CYCloseTutorialU3Ec__Iterator0_t1054761711;
// UIButtonTutorial/<YOpenTutorial>c__Iterator1
struct U3CYOpenTutorialU3Ec__Iterator1_t230414610;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UIButtonUpdate
struct UIButtonUpdate_t766659849;
// System.Random
struct Random_t1044426839;
// WaterSnow
struct WaterSnow_t2237366536;
// GameElevenManager
struct GameElevenManager_t2036900502;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// SingletonBase`1<GameElevenManager>
struct SingletonBase_1_t1036415592;
// SingletonBase`1<System.Object>
struct SingletonBase_1_t1688964385;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Object
struct Object_t1021602117;
// SoundManager
struct SoundManager_t654432262;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t13116344;
// WaterSnow/<YSnowFall>c__Iterator0
struct U3CYSnowFallU3Ec__Iterator0_t3674066925;
// Yey
struct Yey_t2293551375;
// Yey/<Scale>c__AnonStorey0
struct U3CScaleU3Ec__AnonStorey0_t1001303329;
// DG.Tweening.Tweener
struct Tweener_t760404022;
// DG.Tweening.TweenCallback
struct TweenCallback_t3697142134;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* UIButtonBack_HandleClickDown_m354909178_MethodInfo_var;
extern const uint32_t UIButtonBack_Start_m2436397088_MetadataUsageId;
extern const uint32_t UIButtonBack_OnDestroy_m3231308269_MetadataUsageId;
extern const MethodInfo* SingletonBase_1_get_Instance_m2426628856_MethodInfo_var;
extern const uint32_t UIButtonBack_HandleClickDown_m354909178_MetadataUsageId;
extern Il2CppClass* EventManager_t2792515701_il2cpp_TypeInfo_var;
extern const MethodInfo* UIButtonLoadGame_HandleMouseDown_m3494089378_MethodInfo_var;
extern const MethodInfo* UIButtonLoadGame_HandleWindowClicked_m2103064840_MethodInfo_var;
extern const uint32_t UIButtonLoadGame_Start_m2647689525_MetadataUsageId;
extern const uint32_t UIButtonLoadGame_OnDestroy_m1356067410_MetadataUsageId;
extern const MethodInfo* Component_GetComponentInParent_TisScrollRect_t1199013257_m2829548702_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2836116641;
extern const uint32_t UIButtonLoadGame_HandleMouseDown_m3494089378_MetadataUsageId;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t UIButtonLoadGame_CanLoad_m445845939_MetadataUsageId;
extern Il2CppClass* U3CYLoadSceneU3Ec__Iterator0_t3230658609_il2cpp_TypeInfo_var;
extern const uint32_t UIButtonLoadGame_YLoadScene_m275200602_MetadataUsageId;
extern Il2CppClass* iTween_t488923914_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const uint32_t U3CYLoadSceneU3Ec__Iterator0_MoveNext_m1911685122_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CYLoadSceneU3Ec__Iterator0_Reset_m3894531573_MetadataUsageId;
extern const MethodInfo* UIButtonOpenParental_Handle_OnClickDown_m3774271238_MethodInfo_var;
extern const uint32_t UIButtonOpenParental_Start_m797946686_MetadataUsageId;
extern const uint32_t UIButtonOpenParental_OnDestroy_m851912579_MetadataUsageId;
extern const MethodInfo* UIButtonShowSlideshow_HandleMouseDown_m2418457507_MethodInfo_var;
extern const uint32_t UIButtonShowSlideshow_Start_m77866188_MetadataUsageId;
extern const uint32_t UIButtonShowSlideshow_OnDestroy_m1926202393_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m351711267_MethodInfo_var;
extern const uint32_t UIButtonShowSlideshow_HandleMouseDown_m2418457507_MetadataUsageId;
extern const MethodInfo* Component_GetComponentInParent_TisCanvas_t209405766_m2415242607_MethodInfo_var;
extern const MethodInfo* UIButtonTutorial_HandleClickDown_m130974315_MethodInfo_var;
extern const uint32_t UIButtonTutorial_Start_m4181513489_MetadataUsageId;
extern Il2CppClass* U3CYCloseTutorialU3Ec__Iterator0_t1054761711_il2cpp_TypeInfo_var;
extern const uint32_t UIButtonTutorial_YCloseTutorial_m3320199036_MetadataUsageId;
extern Il2CppClass* U3CYOpenTutorialU3Ec__Iterator1_t230414610_il2cpp_TypeInfo_var;
extern const uint32_t UIButtonTutorial_YOpenTutorial_m2084445550_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t UIButtonTutorial_Done_m2373690901_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* EaseType_t818674011_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2388227761;
extern Il2CppCodeGenString* _stringLiteral3457519049;
extern Il2CppCodeGenString* _stringLiteral2239369704;
extern Il2CppCodeGenString* _stringLiteral4076500576;
extern Il2CppCodeGenString* _stringLiteral1414245946;
extern const uint32_t U3CYCloseTutorialU3Ec__Iterator0_MoveNext_m2508067212_MetadataUsageId;
extern const uint32_t U3CYCloseTutorialU3Ec__Iterator0_Reset_m987029975_MetadataUsageId;
extern const uint32_t U3CYOpenTutorialU3Ec__Iterator1_MoveNext_m1238345603_MetadataUsageId;
extern const uint32_t U3CYOpenTutorialU3Ec__Iterator1_Reset_m517888270_MetadataUsageId;
extern const MethodInfo* UIButtonUpdate_HandleMouseDown_m3144446185_MethodInfo_var;
extern const uint32_t UIButtonUpdate_Start_m1534861910_MetadataUsageId;
extern const uint32_t UIButtonUpdate_HandleMouseDown_m3144446185_MetadataUsageId;
extern Il2CppClass* Random_t1044426839_il2cpp_TypeInfo_var;
extern Il2CppClass* Utils_t4194145797_il2cpp_TypeInfo_var;
extern const uint32_t Utils__cctor_m2850823503_MetadataUsageId;
extern const MethodInfo* SingletonBase_1_get_Instance_m3475080457_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const MethodInfo* WaterSnow_HandleMouseDown_m2406703338_MethodInfo_var;
extern const uint32_t WaterSnow_Start_m3757215907_MetadataUsageId;
extern const uint32_t WaterSnow_OnDestroy_m3504045010_MetadataUsageId;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const MethodInfo* SingletonBase_1_get_MainCamera_m3861158774_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider2D_t646061738_m2950827934_MethodInfo_var;
extern const MethodInfo* SingletonBase_1_get_Instance_m3325444209_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442_MethodInfo_var;
extern const uint32_t WaterSnow_HandleMouseDown_m2406703338_MetadataUsageId;
extern Il2CppClass* U3CYSnowFallU3Ec__Iterator0_t3674066925_il2cpp_TypeInfo_var;
extern const uint32_t WaterSnow_YSnowFall_m467672314_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2670825768;
extern const uint32_t WaterSnow_OnTriggerEnter2D_m2400851115_MetadataUsageId;
extern const uint32_t U3CYSnowFallU3Ec__Iterator0_Reset_m1409936625_MetadataUsageId;
extern Il2CppClass* U3CScaleU3Ec__AnonStorey0_t1001303329_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* Transform_t3275118058_il2cpp_TypeInfo_var;
extern Il2CppClass* TweenCallback_t3697142134_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CScaleU3Ec__AnonStorey0_U3CU3Em__0_m3438436187_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386_MethodInfo_var;
extern const uint32_t Yey_Scale_m11203238_MetadataUsageId;

// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Sprite_t309593783 * m_Items[1];

public:
	inline Sprite_t309593783 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_t309593783 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_t309593783 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Sprite_t309593783 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_t309593783 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_t309593783 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// T SingletonBase`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * SingletonBase_1_get_Instance_m2217304019_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m360069213_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, Transform_t3275118058 * p3, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// UnityEngine.Camera SingletonBase`1<System.Object>::get_MainCamera()
extern "C"  Camera_t189460977 * SingletonBase_1_get_MainCamera_m1659347230_gshared (SingletonBase_1_t1688964385 * __this, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnComplete_TisIl2CppObject_m2769923597_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);

// System.Void UIButton::.ctor()
extern "C"  void UIButton__ctor_m4190358109 (UIButton_t3377238306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m2606471964 (Action_t3226471752 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Combine_m3791207084 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Remove_m2626518725 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T SingletonBase`1<NavigationManager>::get_Instance()
#define SingletonBase_1_get_Instance_m2426628856(__this /* static, unused */, method) ((  NavigationManager_t4055369543 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonBase_1_get_Instance_m2217304019_gshared)(__this /* static, unused */, method)
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.Int32)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsync_m2131479547 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3275118058 * Transform_GetChild_m3838588184 (Transform_t3275118058 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInParent<UnityEngine.UI.ScrollRect>()
#define Component_GetComponentInParent_TisScrollRect_t1199013257_m2829548702(__this, method) ((  ScrollRect_t1199013257 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared)(__this, method)
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m4079055610 (Behaviour_t955675639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonLoadGame::CanLoad()
extern "C"  bool UIButtonLoadGame_CanLoad_m445845939 (UIButtonLoadGame_t1239272532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2887581199 (GameObject_t1756533147 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventManager::TriggerWindowClicked()
extern "C"  void EventManager_TriggerWindowClicked_m2906656459 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UIButtonLoadGame::YLoadScene()
extern "C"  Il2CppObject * UIButtonLoadGame_YLoadScene_m275200602 (UIButtonLoadGame_t1239272532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C"  void Application_OpenURL_m3882634228 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::get_UtcNow()
extern "C"  DateTime_t693205669  DateTime_get_UtcNow_m1309841468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::get_DayOfYear()
extern "C"  int32_t DateTime_get_DayOfYear_m1065070811 (DateTime_t693205669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::get_Month()
extern "C"  int32_t DateTime_get_Month_m1464831817 (DateTime_t693205669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonLoadGame/<YLoadScene>c__Iterator0::.ctor()
extern "C"  void U3CYLoadSceneU3Ec__Iterator0__ctor_m3449339970 (U3CYLoadSceneU3Ec__Iterator0_t3230658609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(__this, method) ((  SpriteRenderer_t1209076198 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
extern "C"  void Renderer_set_sortingOrder_m809829562 (Renderer_t257310565 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_root()
extern "C"  Transform_t3275118058 * Transform_get_root_m3891257842 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3275118058 * Transform_get_parent_m147407266 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m4124909910 (Transform_t3275118058 * __this, Transform_t3275118058 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween::MoveTo(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void iTween_MoveTo_m1717402415 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * p0, Vector3_t2243707580  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween::ScaleTo(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void iTween_ScaleTo_m2352084610 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * p0, Vector3_t2243707580  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m1990515539 (WaitForSeconds_t3839502067 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NavigationManager::LoadScene(System.Int32)
extern "C"  void NavigationManager_LoadScene_m310679623 (NavigationManager_t4055369543 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m1033555130 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
#define Object_Instantiate_TisGameObject_t1756533147_m351711267(__this /* static, unused */, p0, p1, p2, p3, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m360069213_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// !!0 UnityEngine.Component::GetComponentInParent<UnityEngine.Canvas>()
#define Component_GetComponentInParent_TisCanvas_t209405766_m2415242607(__this, method) ((  Canvas_t209405766 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared)(__this, method)
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m475173995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Canvas::set_worldCamera(UnityEngine.Camera)
extern "C"  void Canvas_set_worldCamera_m3682884685 (Canvas_t209405766 * __this, Camera_t189460977 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1684909666  SceneManager_GetActiveScene_m2964039490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m3735680091 (Scene_t1684909666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UIButtonTutorial::YCloseTutorial()
extern "C"  Il2CppObject * UIButtonTutorial_YCloseTutorial_m3320199036 (UIButtonTutorial_t123001136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonTutorial/<YCloseTutorial>c__Iterator0::.ctor()
extern "C"  void U3CYCloseTutorialU3Ec__Iterator0__ctor_m3548905532 (U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonTutorial/<YOpenTutorial>c__Iterator1::.ctor()
extern "C"  void U3CYOpenTutorialU3Ec__Iterator1__ctor_m1347123021 (U3CYOpenTutorialU3Ec__Iterator1_t230414610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UIButtonTutorial::YOpenTutorial()
extern "C"  Il2CppObject * UIButtonTutorial_YOpenTutorial_m2084445550 (UIButtonTutorial_t123001136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable iTween::Hash(System.Object[])
extern "C"  Hashtable_t909839986 * iTween_Hash_m2103874480 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween::MoveTo(UnityEngine.GameObject,System.Collections.Hashtable)
extern "C"  void iTween_MoveTo_m2276113879 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * p0, Hashtable_t909839986 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Random::.ctor()
extern "C"  void Random__ctor_m1561335652 (Random_t1044426839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T SingletonBase`1<GameElevenManager>::get_Instance()
#define SingletonBase_1_get_Instance_m3475080457(__this /* static, unused */, method) ((  GameElevenManager_t2036900502 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonBase_1_get_Instance_m2217304019_gshared)(__this /* static, unused */, method)
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m694320887 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_set_sprite_m617298623 (SpriteRenderer_t1209076198 * __this, Sprite_t309593783 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, method) ((  Rigidbody2D_t502193897 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// UnityEngine.Camera SingletonBase`1<GameElevenManager>::get_MainCamera()
#define SingletonBase_1_get_MainCamera_m3861158774(__this, method) ((  Camera_t189460977 * (*) (SingletonBase_1_t1036415592 *, const MethodInfo*))SingletonBase_1_get_MainCamera_m1659347230_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t2243707580  Input_get_mousePosition_m146923508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t2469606224  Camera_ScreenPointToRay_m614889538 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single)
extern "C"  RaycastHit2D_t4063908774  Physics2D_GetRayIntersection_m2295701053 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RaycastHit2D::op_Implicit(UnityEngine.RaycastHit2D)
extern "C"  bool RaycastHit2D_op_Implicit_m596912073 (Il2CppObject * __this /* static, unused */, RaycastHit2D_t4063908774  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t646061738 * RaycastHit2D_get_collider_m2568504212 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider2D>()
#define Component_GetComponent_TisCollider2D_t646061738_m2950827934(__this, method) ((  Collider2D_t646061738 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T SingletonBase`1<SoundManager>::get_Instance()
#define SingletonBase_1_get_Instance_m3325444209(__this /* static, unused */, method) ((  SoundManager_t654432262 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonBase_1_get_Instance_m2217304019_gshared)(__this /* static, unused */, method)
// UnityEngine.AudioSource SoundManager::Play(UnityEngine.AudioClip,UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.UInt64,System.Boolean)
extern "C"  AudioSource_t1135106623 * SoundManager_Play_m53396906 (SoundManager_t654432262 * __this, AudioClip_t1932558630 * ___ac0, Transform_t3275118058 * ___trans1, Vector3_t2243707580  ___position2, float ___volume3, uint64_t ___delay4, bool ___loop5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody2D_set_isKinematic_m548319077 (Rigidbody2D_t502193897 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Vector2_op_Implicit_m1064335535 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m3592751374 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CircleCollider2D>()
#define Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442(__this, method) ((  CircleCollider2D_t13116344 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Single UnityEngine.CircleCollider2D::get_radius()
extern "C"  float CircleCollider2D_get_radius_m3114753530 (CircleCollider2D_t13116344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CircleCollider2D::set_radius(System.Single)
extern "C"  void CircleCollider2D_set_radius_m1315641707 (CircleCollider2D_t13116344 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WaterSnow::YSnowFall()
extern "C"  Il2CppObject * WaterSnow_YSnowFall_m467672314 (WaterSnow_t2237366536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterSnow/<YSnowFall>c__Iterator0::.ctor()
extern "C"  void U3CYSnowFallU3Ec__Iterator0__ctor_m703817188 (U3CYSnowFallU3Ec__Iterator0_t3674066925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m357168014 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameElevenManager::IncrementSnowCount(System.Int32)
extern "C"  void GameElevenManager_IncrementSnowCount_m3930045437 (GameElevenManager_t2036900502 * __this, int32_t ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m4279412553 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C"  void Transform_Translate_m3316827744 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C"  Il2CppObject * Transform_GetEnumerator_m3479720613 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Yey/<Scale>c__AnonStorey0::.ctor()
extern "C"  void U3CScaleU3Ec__AnonStorey0__ctor_m1495609928 (U3CScaleU3Ec__AnonStorey0_t1001303329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t2243707580  Vector3_get_one_m627547232 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1351554733 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOScale_m2754995414 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Vector3_t2243707580  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback__ctor_m3479200459 (TweenCallback_t3697142134 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Tweener>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386(__this /* static, unused */, p0, p1, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisIl2CppObject_m2769923597_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIButtonBack::.ctor()
extern "C"  void UIButtonBack__ctor_m3957355980 (UIButtonBack_t1890699263 * __this, const MethodInfo* method)
{
	{
		UIButton__ctor_m4190358109(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIButtonBack::Start()
extern "C"  void UIButtonBack_Start_m2436397088 (UIButtonBack_t1890699263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonBack_Start_m2436397088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t3226471752 * L_0 = ((UIButton_t3377238306 *)__this)->get_OnClickDown_2();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIButtonBack_HandleClickDown_m354909178_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_3 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UIButton_t3377238306 *)__this)->set_OnClickDown_2(((Action_t3226471752 *)CastclassSealed(L_3, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIButtonBack::OnDestroy()
extern "C"  void UIButtonBack_OnDestroy_m3231308269 (UIButtonBack_t1890699263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonBack_OnDestroy_m3231308269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t3226471752 * L_0 = ((UIButton_t3377238306 *)__this)->get_OnClickDown_2();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIButtonBack_HandleClickDown_m354909178_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_3 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UIButton_t3377238306 *)__this)->set_OnClickDown_2(((Action_t3226471752 *)CastclassSealed(L_3, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIButtonBack::HandleClickDown()
extern "C"  void UIButtonBack_HandleClickDown_m354909178 (UIButtonBack_t1890699263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonBack_HandleClickDown_m354909178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NavigationManager_t4055369543 * L_0 = SingletonBase_1_get_Instance_m2426628856(NULL /*static, unused*/, /*hidden argument*/SingletonBase_1_get_Instance_m2426628856_MethodInfo_var);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_mainInterfaceBuildIndex_4();
		SceneManager_LoadSceneAsync_m2131479547(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIButtonLoadGame::.ctor()
extern "C"  void UIButtonLoadGame__ctor_m541880981 (UIButtonLoadGame_t1239272532 * __this, const MethodInfo* method)
{
	{
		UIButton__ctor_m4190358109(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIButtonLoadGame::Start()
extern "C"  void UIButtonLoadGame_Start_m2647689525 (UIButtonLoadGame_t1239272532 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonLoadGame_Start_m2647689525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t3226471752 * L_0 = ((UIButton_t3377238306 *)__this)->get_OnClickUp_3();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIButtonLoadGame_HandleMouseDown_m3494089378_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_3 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UIButton_t3377238306 *)__this)->set_OnClickUp_3(((Action_t3226471752 *)CastclassSealed(L_3, Action_t3226471752_il2cpp_TypeInfo_var)));
		Action_t3226471752 * L_4 = ((EventManager_t2792515701_StaticFields*)EventManager_t2792515701_il2cpp_TypeInfo_var->static_fields)->get_OnWindowClicked_2();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)UIButtonLoadGame_HandleWindowClicked_m2103064840_MethodInfo_var);
		Action_t3226471752 * L_6 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_6, __this, L_5, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_7 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		((EventManager_t2792515701_StaticFields*)EventManager_t2792515701_il2cpp_TypeInfo_var->static_fields)->set_OnWindowClicked_2(((Action_t3226471752 *)CastclassSealed(L_7, Action_t3226471752_il2cpp_TypeInfo_var)));
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Transform_GetChild_m3838588184(L_8, 0, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		__this->set_bg_7(L_10);
		return;
	}
}
// System.Void UIButtonLoadGame::OnDestroy()
extern "C"  void UIButtonLoadGame_OnDestroy_m1356067410 (UIButtonLoadGame_t1239272532 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonLoadGame_OnDestroy_m1356067410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t3226471752 * L_0 = ((UIButton_t3377238306 *)__this)->get_OnClickUp_3();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIButtonLoadGame_HandleMouseDown_m3494089378_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_3 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UIButton_t3377238306 *)__this)->set_OnClickUp_3(((Action_t3226471752 *)CastclassSealed(L_3, Action_t3226471752_il2cpp_TypeInfo_var)));
		Action_t3226471752 * L_4 = ((EventManager_t2792515701_StaticFields*)EventManager_t2792515701_il2cpp_TypeInfo_var->static_fields)->get_OnWindowClicked_2();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)UIButtonLoadGame_HandleWindowClicked_m2103064840_MethodInfo_var);
		Action_t3226471752 * L_6 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_6, __this, L_5, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_7 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		((EventManager_t2792515701_StaticFields*)EventManager_t2792515701_il2cpp_TypeInfo_var->static_fields)->set_OnWindowClicked_2(((Action_t3226471752 *)CastclassSealed(L_7, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIButtonLoadGame::HandleMouseDown()
extern "C"  void UIButtonLoadGame_HandleMouseDown_m3494089378 (UIButtonLoadGame_t1239272532 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonLoadGame_HandleMouseDown_m3494089378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ScrollRect_t1199013257 * L_0 = Component_GetComponentInParent_TisScrollRect_t1199013257_m2829548702(__this, /*hidden argument*/Component_GetComponentInParent_TisScrollRect_t1199013257_m2829548702_MethodInfo_var);
		NullCheck(L_0);
		bool L_1 = Behaviour_get_enabled_m4079055610(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0132;
		}
	}
	{
		bool L_2 = UIButtonLoadGame_CanLoad_m445845939(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00bf;
		}
	}
	{
		bool L_3 = __this->get_isOpen_5();
		if (L_3)
		{
			goto IL_0093;
		}
	}
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_4, 3, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Transform_GetChild_m3838588184(L_7, 4, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Transform_GetChild_m3838588184(L_10, 2, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = Transform_GetChild_m3838588184(L_13, 1, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = Component_get_gameObject_m3105766835(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		GameObject_SetActive_m2887581199(L_15, (bool)0, /*hidden argument*/NULL);
		EventManager_TriggerWindowClicked_m2906656459(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_isOpen_5((bool)1);
		goto IL_00ba;
	}

IL_0093:
	{
		bool L_16 = __this->get_notDone_6();
		if (L_16)
		{
			goto IL_00b0;
		}
	}
	{
		Il2CppObject * L_17 = UIButtonLoadGame_YLoadScene_m275200602(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_17, /*hidden argument*/NULL);
		goto IL_00ba;
	}

IL_00b0:
	{
		Application_OpenURL_m3882634228(NULL /*static, unused*/, _stringLiteral2836116641, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		goto IL_0132;
	}

IL_00bf:
	{
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = Transform_GetChild_m3838588184(L_18, 3, /*hidden argument*/NULL);
		NullCheck(L_19);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = Transform_GetChild_m3838588184(L_21, 4, /*hidden argument*/NULL);
		NullCheck(L_22);
		GameObject_t1756533147 * L_23 = Component_get_gameObject_m3105766835(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameObject_SetActive_m2887581199(L_23, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = Transform_GetChild_m3838588184(L_24, 2, /*hidden argument*/NULL);
		NullCheck(L_25);
		GameObject_t1756533147 * L_26 = Component_get_gameObject_m3105766835(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		GameObject_SetActive_m2887581199(L_26, (bool)0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_27 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = Transform_GetChild_m3838588184(L_27, 1, /*hidden argument*/NULL);
		NullCheck(L_28);
		GameObject_t1756533147 * L_29 = Component_get_gameObject_m3105766835(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		GameObject_SetActive_m2887581199(L_29, (bool)1, /*hidden argument*/NULL);
		bool L_30 = __this->get_isOpen_5();
		if (L_30)
		{
			goto IL_012b;
		}
	}
	{
		EventManager_TriggerWindowClicked_m2906656459(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_012b:
	{
		__this->set_isOpen_5((bool)1);
	}

IL_0132:
	{
		return;
	}
}
// System.Void UIButtonLoadGame::HandleWindowClicked()
extern "C"  void UIButtonLoadGame_HandleWindowClicked_m2103064840 (UIButtonLoadGame_t1239272532 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isOpen_5();
		if (!L_0)
		{
			goto IL_006e;
		}
	}
	{
		__this->set_isOpen_5((bool)0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Transform_GetChild_m3838588184(L_1, 3, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_4, 4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Transform_GetChild_m3838588184(L_7, 2, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Transform_GetChild_m3838588184(L_10, 1, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)1, /*hidden argument*/NULL);
	}

IL_006e:
	{
		return;
	}
}
// System.Boolean UIButtonLoadGame::CanLoad()
extern "C"  bool UIButtonLoadGame_CanLoad_m445845939 (UIButtonLoadGame_t1239272532 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonLoadGame_CanLoad_m445845939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	DateTime_t693205669  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_0 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = DateTime_get_DayOfYear_m1065070811((&V_0), /*hidden argument*/NULL);
		NavigationManager_t4055369543 * L_2 = SingletonBase_1_get_Instance_m2426628856(NULL /*static, unused*/, /*hidden argument*/SingletonBase_1_get_Instance_m2426628856_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = L_2->get_yearNumberStart_5();
		int32_t L_4 = __this->get_sceneToLoad_4();
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)((int32_t)L_3+(int32_t)L_4)))))
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_5 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = DateTime_get_Month_m1464831817((&V_1), /*hidden argument*/NULL);
		if ((((int32_t)L_6) >= ((int32_t)((int32_t)11))))
		{
			goto IL_0039;
		}
	}

IL_0037:
	{
		return (bool)1;
	}

IL_0039:
	{
		return (bool)0;
	}
}
// System.Collections.IEnumerator UIButtonLoadGame::YLoadScene()
extern "C"  Il2CppObject * UIButtonLoadGame_YLoadScene_m275200602 (UIButtonLoadGame_t1239272532 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonLoadGame_YLoadScene_m275200602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CYLoadSceneU3Ec__Iterator0_t3230658609 * V_0 = NULL;
	{
		U3CYLoadSceneU3Ec__Iterator0_t3230658609 * L_0 = (U3CYLoadSceneU3Ec__Iterator0_t3230658609 *)il2cpp_codegen_object_new(U3CYLoadSceneU3Ec__Iterator0_t3230658609_il2cpp_TypeInfo_var);
		U3CYLoadSceneU3Ec__Iterator0__ctor_m3449339970(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CYLoadSceneU3Ec__Iterator0_t3230658609 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CYLoadSceneU3Ec__Iterator0_t3230658609 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UIButtonLoadGame/<YLoadScene>c__Iterator0::.ctor()
extern "C"  void U3CYLoadSceneU3Ec__Iterator0__ctor_m3449339970 (U3CYLoadSceneU3Ec__Iterator0_t3230658609 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIButtonLoadGame/<YLoadScene>c__Iterator0::MoveNext()
extern "C"  bool U3CYLoadSceneU3Ec__Iterator0_MoveNext_m1911685122 (U3CYLoadSceneU3Ec__Iterator0_t3230658609 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CYLoadSceneU3Ec__Iterator0_MoveNext_m1911685122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00df;
			}
		}
	}
	{
		goto IL_00fb;
	}

IL_0021:
	{
		UIButtonLoadGame_t1239272532 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Transform_GetChild_m3838588184(L_3, 0, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		SpriteRenderer_t1209076198 * L_6 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_5, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		NullCheck(L_6);
		Renderer_set_sortingOrder_m809829562(L_6, ((int32_t)100), /*hidden argument*/NULL);
		UIButtonLoadGame_t1239272532 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Transform_GetChild_m3838588184(L_8, 0, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = GameObject_get_transform_m909382139(L_10, /*hidden argument*/NULL);
		UIButtonLoadGame_t1239272532 * L_12 = __this->get_U24this_0();
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = Transform_get_root_m3891257842(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Transform_get_parent_m147407266(L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_SetParent_m4124909910(L_11, L_15, /*hidden argument*/NULL);
		UIButtonLoadGame_t1239272532 * L_16 = __this->get_U24this_0();
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = L_16->get_bg_7();
		Vector3_t2243707580  L_18 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t488923914_il2cpp_TypeInfo_var);
		iTween_MoveTo_m1717402415(NULL /*static, unused*/, L_17, L_18, (0.6f), /*hidden argument*/NULL);
		UIButtonLoadGame_t1239272532 * L_19 = __this->get_U24this_0();
		NullCheck(L_19);
		GameObject_t1756533147 * L_20 = L_19->get_bg_7();
		Vector3_t2243707580  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2638739322(&L_21, (0.93f), (0.95f), (1.0f), /*hidden argument*/NULL);
		iTween_ScaleTo_m2352084610(NULL /*static, unused*/, L_20, L_21, (0.6f), /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_22 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_22, (0.6f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_22);
		bool L_23 = __this->get_U24disposing_2();
		if (L_23)
		{
			goto IL_00da;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_00da:
	{
		goto IL_00fd;
	}

IL_00df:
	{
		NavigationManager_t4055369543 * L_24 = SingletonBase_1_get_Instance_m2426628856(NULL /*static, unused*/, /*hidden argument*/SingletonBase_1_get_Instance_m2426628856_MethodInfo_var);
		UIButtonLoadGame_t1239272532 * L_25 = __this->get_U24this_0();
		NullCheck(L_25);
		int32_t L_26 = L_25->get_sceneToLoad_4();
		NullCheck(L_24);
		NavigationManager_LoadScene_m310679623(L_24, L_26, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_00fb:
	{
		return (bool)0;
	}

IL_00fd:
	{
		return (bool)1;
	}
}
// System.Object UIButtonLoadGame/<YLoadScene>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CYLoadSceneU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m608642924 (U3CYLoadSceneU3Ec__Iterator0_t3230658609 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object UIButtonLoadGame/<YLoadScene>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CYLoadSceneU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3473350676 (U3CYLoadSceneU3Ec__Iterator0_t3230658609 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void UIButtonLoadGame/<YLoadScene>c__Iterator0::Dispose()
extern "C"  void U3CYLoadSceneU3Ec__Iterator0_Dispose_m2625460819 (U3CYLoadSceneU3Ec__Iterator0_t3230658609 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UIButtonLoadGame/<YLoadScene>c__Iterator0::Reset()
extern "C"  void U3CYLoadSceneU3Ec__Iterator0_Reset_m3894531573 (U3CYLoadSceneU3Ec__Iterator0_t3230658609 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CYLoadSceneU3Ec__Iterator0_Reset_m3894531573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UIButtonOpenParental::.ctor()
extern "C"  void UIButtonOpenParental__ctor_m2930629806 (UIButtonOpenParental_t1943914589 * __this, const MethodInfo* method)
{
	{
		UIButton__ctor_m4190358109(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIButtonOpenParental::Start()
extern "C"  void UIButtonOpenParental_Start_m797946686 (UIButtonOpenParental_t1943914589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonOpenParental_Start_m797946686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t3226471752 * L_0 = ((UIButton_t3377238306 *)__this)->get_OnClickDown_2();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIButtonOpenParental_Handle_OnClickDown_m3774271238_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_3 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UIButton_t3377238306 *)__this)->set_OnClickDown_2(((Action_t3226471752 *)CastclassSealed(L_3, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIButtonOpenParental::OnDestroy()
extern "C"  void UIButtonOpenParental_OnDestroy_m851912579 (UIButtonOpenParental_t1943914589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonOpenParental_OnDestroy_m851912579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t3226471752 * L_0 = ((UIButton_t3377238306 *)__this)->get_OnClickDown_2();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIButtonOpenParental_Handle_OnClickDown_m3774271238_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_3 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UIButton_t3377238306 *)__this)->set_OnClickDown_2(((Action_t3226471752 *)CastclassSealed(L_3, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIButtonOpenParental::Handle_OnClickDown()
extern "C"  void UIButtonOpenParental_Handle_OnClickDown_m3774271238 (UIButtonOpenParental_t1943914589 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_buttonType_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0029;
		}
	}
	{
		ParentalControl_t2040855302 * L_1 = __this->get_parental_4();
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		ParentalControl_t2040855302 * L_3 = __this->get_parental_4();
		NullCheck(L_3);
		L_3->set_type_2(1);
	}

IL_0029:
	{
		int32_t L_4 = __this->get_buttonType_5();
		if (L_4)
		{
			goto IL_0051;
		}
	}
	{
		ParentalControl_t2040855302 * L_5 = __this->get_parental_4();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)1, /*hidden argument*/NULL);
		ParentalControl_t2040855302 * L_7 = __this->get_parental_4();
		NullCheck(L_7);
		L_7->set_type_2(0);
	}

IL_0051:
	{
		return;
	}
}
// System.Void UIButtonShowSlideshow::.ctor()
extern "C"  void UIButtonShowSlideshow__ctor_m1479095768 (UIButtonShowSlideshow_t175868935 * __this, const MethodInfo* method)
{
	{
		UIButton__ctor_m4190358109(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIButtonShowSlideshow::Start()
extern "C"  void UIButtonShowSlideshow_Start_m77866188 (UIButtonShowSlideshow_t175868935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonShowSlideshow_Start_m77866188_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t3226471752 * L_0 = ((UIButton_t3377238306 *)__this)->get_OnClickDown_2();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIButtonShowSlideshow_HandleMouseDown_m2418457507_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_3 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UIButton_t3377238306 *)__this)->set_OnClickDown_2(((Action_t3226471752 *)CastclassSealed(L_3, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIButtonShowSlideshow::OnDestroy()
extern "C"  void UIButtonShowSlideshow_OnDestroy_m1926202393 (UIButtonShowSlideshow_t175868935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonShowSlideshow_OnDestroy_m1926202393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t3226471752 * L_0 = ((UIButton_t3377238306 *)__this)->get_OnClickDown_2();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIButtonShowSlideshow_HandleMouseDown_m2418457507_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_3 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UIButton_t3377238306 *)__this)->set_OnClickDown_2(((Action_t3226471752 *)CastclassSealed(L_3, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIButtonShowSlideshow::HandleMouseDown()
extern "C"  void UIButtonShowSlideshow_HandleMouseDown_m2418457507 (UIButtonShowSlideshow_t175868935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonShowSlideshow_HandleMouseDown_m2418457507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_slideShow_4();
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_slideShow_4();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Quaternion_t4030073918  L_4 = Transform_get_rotation_m1033555130(L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = __this->get_slidesHolder_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m351711267(NULL /*static, unused*/, L_0, L_1, L_4, L_5, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m351711267_MethodInfo_var);
		return;
	}
}
// System.Void UIButtonTutorial::.ctor()
extern "C"  void UIButtonTutorial__ctor_m598054521 (UIButtonTutorial_t123001136 * __this, const MethodInfo* method)
{
	{
		__this->set_minX_7((2.4f));
		__this->set_maxX_8((18.9f));
		__this->set_minXtetris_9((8.1f));
		__this->set_maxXtetris_10((32.5f));
		UIButton__ctor_m4190358109(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIButtonTutorial::Start()
extern "C"  void UIButtonTutorial_Start_m4181513489 (UIButtonTutorial_t123001136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonTutorial_Start_m4181513489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Canvas_t209405766 * L_0 = Component_GetComponentInParent_TisCanvas_t209405766_m2415242607(__this, /*hidden argument*/Component_GetComponentInParent_TisCanvas_t209405766_m2415242607_MethodInfo_var);
		Camera_t189460977 * L_1 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Canvas_set_worldCamera_m3682884685(L_0, L_1, /*hidden argument*/NULL);
		Text_t356221433 * L_2 = __this->get_helpText_5();
		StringU5BU5D_t1642385972* L_3 = __this->get_tutorialTexts_4();
		Scene_t1684909666  L_4 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = Scene_get_buildIndex_m3735680091((&V_0), /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)((int32_t)L_5-(int32_t)2));
		String_t* L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_7);
		Action_t3226471752 * L_8 = ((UIButton_t3377238306 *)__this)->get_OnClickDown_2();
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)UIButtonTutorial_HandleClickDown_m130974315_MethodInfo_var);
		Action_t3226471752 * L_10 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_10, __this, L_9, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_11 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		((UIButton_t3377238306 *)__this)->set_OnClickDown_2(((Action_t3226471752 *)CastclassSealed(L_11, Action_t3226471752_il2cpp_TypeInfo_var)));
		Il2CppObject * L_12 = UIButtonTutorial_YCloseTutorial_m3320199036(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UIButtonTutorial::YCloseTutorial()
extern "C"  Il2CppObject * UIButtonTutorial_YCloseTutorial_m3320199036 (UIButtonTutorial_t123001136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonTutorial_YCloseTutorial_m3320199036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * V_0 = NULL;
	{
		U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * L_0 = (U3CYCloseTutorialU3Ec__Iterator0_t1054761711 *)il2cpp_codegen_object_new(U3CYCloseTutorialU3Ec__Iterator0_t1054761711_il2cpp_TypeInfo_var);
		U3CYCloseTutorialU3Ec__Iterator0__ctor_m3548905532(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator UIButtonTutorial::YOpenTutorial()
extern "C"  Il2CppObject * UIButtonTutorial_YOpenTutorial_m2084445550 (UIButtonTutorial_t123001136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonTutorial_YOpenTutorial_m2084445550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CYOpenTutorialU3Ec__Iterator1_t230414610 * V_0 = NULL;
	{
		U3CYOpenTutorialU3Ec__Iterator1_t230414610 * L_0 = (U3CYOpenTutorialU3Ec__Iterator1_t230414610 *)il2cpp_codegen_object_new(U3CYOpenTutorialU3Ec__Iterator1_t230414610_il2cpp_TypeInfo_var);
		U3CYOpenTutorialU3Ec__Iterator1__ctor_m1347123021(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CYOpenTutorialU3Ec__Iterator1_t230414610 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CYOpenTutorialU3Ec__Iterator1_t230414610 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UIButtonTutorial::Done()
extern "C"  void UIButtonTutorial_Done_m2373690901 (UIButtonTutorial_t123001136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonTutorial_Done_m2373690901_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_canClick_6((bool)1);
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIButtonTutorial::HandleClickDown()
extern "C"  void UIButtonTutorial_HandleClickDown_m130974315 (UIButtonTutorial_t123001136 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_canClick_6();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		__this->set_canClick_6((bool)0);
		Il2CppObject * L_1 = UIButtonTutorial_YOpenTutorial_m2084445550(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void UIButtonTutorial/<YCloseTutorial>c__Iterator0::.ctor()
extern "C"  void U3CYCloseTutorialU3Ec__Iterator0__ctor_m3548905532 (U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIButtonTutorial/<YCloseTutorial>c__Iterator0::MoveNext()
extern "C"  bool U3CYCloseTutorialU3Ec__Iterator0_MoveNext_m2508067212 (U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CYCloseTutorialU3Ec__Iterator0_MoveNext_m2508067212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Scene_t1684909666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_01ad;
	}

IL_0021:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_01af;
	}

IL_0045:
	{
		Scene_t1684909666  L_4 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = Scene_get_buildIndex_m3735680091((&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_5-(int32_t)1))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0102;
		}
	}
	{
		UIButtonTutorial_t123001136 * L_6 = __this->get_U24this_0();
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_8 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral2388227761);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2388227761);
		ObjectU5BU5D_t3614634134* L_9 = L_8;
		UIButtonTutorial_t123001136 * L_10 = __this->get_U24this_0();
		NullCheck(L_10);
		float L_11 = L_10->get_maxXtetris_10();
		UIButtonTutorial_t123001136 * L_12 = __this->get_U24this_0();
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_position_m1104419803(L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		float L_15 = (&V_2)->get_y_2();
		UIButtonTutorial_t123001136 * L_16 = __this->get_U24this_0();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_position_m1104419803(L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		float L_19 = (&V_3)->get_z_3();
		Vector3_t2243707580  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m2638739322(&L_20, L_11, L_15, L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = L_20;
		Il2CppObject * L_22 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_22);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = L_9;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3457519049);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3457519049);
		ObjectU5BU5D_t3614634134* L_24 = L_23;
		float L_25 = (0.3f);
		Il2CppObject * L_26 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_26);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_26);
		ObjectU5BU5D_t3614634134* L_27 = L_24;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral2239369704);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2239369704);
		ObjectU5BU5D_t3614634134* L_28 = L_27;
		int32_t L_29 = ((int32_t)((int32_t)21));
		Il2CppObject * L_30 = Box(EaseType_t818674011_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_30);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_30);
		ObjectU5BU5D_t3614634134* L_31 = L_28;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral4076500576);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral4076500576);
		ObjectU5BU5D_t3614634134* L_32 = L_31;
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral1414245946);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral1414245946);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t488923914_il2cpp_TypeInfo_var);
		Hashtable_t909839986 * L_33 = iTween_Hash_m2103874480(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		iTween_MoveTo_m2276113879(NULL /*static, unused*/, L_7, L_33, /*hidden argument*/NULL);
		goto IL_01a6;
	}

IL_0102:
	{
		UIButtonTutorial_t123001136 * L_34 = __this->get_U24this_0();
		NullCheck(L_34);
		GameObject_t1756533147 * L_35 = Component_get_gameObject_m3105766835(L_34, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_36 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, _stringLiteral2388227761);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2388227761);
		ObjectU5BU5D_t3614634134* L_37 = L_36;
		UIButtonTutorial_t123001136 * L_38 = __this->get_U24this_0();
		NullCheck(L_38);
		float L_39 = L_38->get_maxX_8();
		UIButtonTutorial_t123001136 * L_40 = __this->get_U24this_0();
		NullCheck(L_40);
		Transform_t3275118058 * L_41 = Component_get_transform_m2697483695(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t2243707580  L_42 = Transform_get_position_m1104419803(L_41, /*hidden argument*/NULL);
		V_4 = L_42;
		float L_43 = (&V_4)->get_y_2();
		UIButtonTutorial_t123001136 * L_44 = __this->get_U24this_0();
		NullCheck(L_44);
		Transform_t3275118058 * L_45 = Component_get_transform_m2697483695(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		Vector3_t2243707580  L_46 = Transform_get_position_m1104419803(L_45, /*hidden argument*/NULL);
		V_5 = L_46;
		float L_47 = (&V_5)->get_z_3();
		Vector3_t2243707580  L_48;
		memset(&L_48, 0, sizeof(L_48));
		Vector3__ctor_m2638739322(&L_48, L_39, L_43, L_47, /*hidden argument*/NULL);
		Vector3_t2243707580  L_49 = L_48;
		Il2CppObject * L_50 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_50);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_50);
		ObjectU5BU5D_t3614634134* L_51 = L_37;
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, _stringLiteral3457519049);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3457519049);
		ObjectU5BU5D_t3614634134* L_52 = L_51;
		float L_53 = (0.3f);
		Il2CppObject * L_54 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_53);
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, L_54);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_54);
		ObjectU5BU5D_t3614634134* L_55 = L_52;
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, _stringLiteral2239369704);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2239369704);
		ObjectU5BU5D_t3614634134* L_56 = L_55;
		int32_t L_57 = ((int32_t)((int32_t)21));
		Il2CppObject * L_58 = Box(EaseType_t818674011_il2cpp_TypeInfo_var, &L_57);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_58);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_58);
		ObjectU5BU5D_t3614634134* L_59 = L_56;
		NullCheck(L_59);
		ArrayElementTypeCheck (L_59, _stringLiteral4076500576);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral4076500576);
		ObjectU5BU5D_t3614634134* L_60 = L_59;
		NullCheck(L_60);
		ArrayElementTypeCheck (L_60, _stringLiteral1414245946);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral1414245946);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t488923914_il2cpp_TypeInfo_var);
		Hashtable_t909839986 * L_61 = iTween_Hash_m2103874480(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		iTween_MoveTo_m2276113879(NULL /*static, unused*/, L_35, L_61, /*hidden argument*/NULL);
	}

IL_01a6:
	{
		__this->set_U24PC_3((-1));
	}

IL_01ad:
	{
		return (bool)0;
	}

IL_01af:
	{
		return (bool)1;
	}
}
// System.Object UIButtonTutorial/<YCloseTutorial>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CYCloseTutorialU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m628254314 (U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object UIButtonTutorial/<YCloseTutorial>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CYCloseTutorialU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2362211714 (U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void UIButtonTutorial/<YCloseTutorial>c__Iterator0::Dispose()
extern "C"  void U3CYCloseTutorialU3Ec__Iterator0_Dispose_m881884665 (U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UIButtonTutorial/<YCloseTutorial>c__Iterator0::Reset()
extern "C"  void U3CYCloseTutorialU3Ec__Iterator0_Reset_m987029975 (U3CYCloseTutorialU3Ec__Iterator0_t1054761711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CYCloseTutorialU3Ec__Iterator0_Reset_m987029975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UIButtonTutorial/<YOpenTutorial>c__Iterator1::.ctor()
extern "C"  void U3CYOpenTutorialU3Ec__Iterator1__ctor_m1347123021 (U3CYOpenTutorialU3Ec__Iterator1_t230414610 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIButtonTutorial/<YOpenTutorial>c__Iterator1::MoveNext()
extern "C"  bool U3CYOpenTutorialU3Ec__Iterator1_MoveNext_m1238345603 (U3CYOpenTutorialU3Ec__Iterator1_t230414610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CYOpenTutorialU3Ec__Iterator1_MoveNext_m1238345603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Scene_t1684909666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0186;
			}
		}
	}
	{
		goto IL_01a4;
	}

IL_0021:
	{
		Scene_t1684909666  L_2 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Scene_get_buildIndex_m3735680091((&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_3-(int32_t)1))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_00ce;
		}
	}
	{
		UIButtonTutorial_t123001136 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_6 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral2388227761);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2388227761);
		ObjectU5BU5D_t3614634134* L_7 = L_6;
		UIButtonTutorial_t123001136 * L_8 = __this->get_U24this_0();
		NullCheck(L_8);
		float L_9 = L_8->get_minXtetris_9();
		UIButtonTutorial_t123001136 * L_10 = __this->get_U24this_0();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = Transform_get_position_m1104419803(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = (&V_2)->get_y_2();
		UIButtonTutorial_t123001136 * L_14 = __this->get_U24this_0();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_position_m1104419803(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		float L_17 = (&V_3)->get_z_3();
		Vector3_t2243707580  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2638739322(&L_18, L_9, L_13, L_17, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = L_18;
		Il2CppObject * L_20 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_20);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_20);
		ObjectU5BU5D_t3614634134* L_21 = L_7;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral3457519049);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3457519049);
		ObjectU5BU5D_t3614634134* L_22 = L_21;
		float L_23 = (0.3f);
		Il2CppObject * L_24 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_24);
		ObjectU5BU5D_t3614634134* L_25 = L_22;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral2239369704);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2239369704);
		ObjectU5BU5D_t3614634134* L_26 = L_25;
		int32_t L_27 = ((int32_t)((int32_t)21));
		Il2CppObject * L_28 = Box(EaseType_t818674011_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_28);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t488923914_il2cpp_TypeInfo_var);
		Hashtable_t909839986 * L_29 = iTween_Hash_m2103874480(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		iTween_MoveTo_m2276113879(NULL /*static, unused*/, L_5, L_29, /*hidden argument*/NULL);
		goto IL_0162;
	}

IL_00ce:
	{
		UIButtonTutorial_t123001136 * L_30 = __this->get_U24this_0();
		NullCheck(L_30);
		GameObject_t1756533147 * L_31 = Component_get_gameObject_m3105766835(L_30, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_32 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral2388227761);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2388227761);
		ObjectU5BU5D_t3614634134* L_33 = L_32;
		UIButtonTutorial_t123001136 * L_34 = __this->get_U24this_0();
		NullCheck(L_34);
		float L_35 = L_34->get_minX_7();
		UIButtonTutorial_t123001136 * L_36 = __this->get_U24this_0();
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = Component_get_transform_m2697483695(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t2243707580  L_38 = Transform_get_position_m1104419803(L_37, /*hidden argument*/NULL);
		V_4 = L_38;
		float L_39 = (&V_4)->get_y_2();
		UIButtonTutorial_t123001136 * L_40 = __this->get_U24this_0();
		NullCheck(L_40);
		Transform_t3275118058 * L_41 = Component_get_transform_m2697483695(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t2243707580  L_42 = Transform_get_position_m1104419803(L_41, /*hidden argument*/NULL);
		V_5 = L_42;
		float L_43 = (&V_5)->get_z_3();
		Vector3_t2243707580  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Vector3__ctor_m2638739322(&L_44, L_35, L_39, L_43, /*hidden argument*/NULL);
		Vector3_t2243707580  L_45 = L_44;
		Il2CppObject * L_46 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_46);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_46);
		ObjectU5BU5D_t3614634134* L_47 = L_33;
		NullCheck(L_47);
		ArrayElementTypeCheck (L_47, _stringLiteral3457519049);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3457519049);
		ObjectU5BU5D_t3614634134* L_48 = L_47;
		float L_49 = (0.3f);
		Il2CppObject * L_50 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_50);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_50);
		ObjectU5BU5D_t3614634134* L_51 = L_48;
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, _stringLiteral2239369704);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2239369704);
		ObjectU5BU5D_t3614634134* L_52 = L_51;
		int32_t L_53 = ((int32_t)((int32_t)21));
		Il2CppObject * L_54 = Box(EaseType_t818674011_il2cpp_TypeInfo_var, &L_53);
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, L_54);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_54);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t488923914_il2cpp_TypeInfo_var);
		Hashtable_t909839986 * L_55 = iTween_Hash_m2103874480(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		iTween_MoveTo_m2276113879(NULL /*static, unused*/, L_31, L_55, /*hidden argument*/NULL);
	}

IL_0162:
	{
		WaitForSeconds_t3839502067 * L_56 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_56, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_56);
		bool L_57 = __this->get_U24disposing_2();
		if (L_57)
		{
			goto IL_0181;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0181:
	{
		goto IL_01a6;
	}

IL_0186:
	{
		UIButtonTutorial_t123001136 * L_58 = __this->get_U24this_0();
		UIButtonTutorial_t123001136 * L_59 = __this->get_U24this_0();
		NullCheck(L_59);
		Il2CppObject * L_60 = UIButtonTutorial_YCloseTutorial_m3320199036(L_59, /*hidden argument*/NULL);
		NullCheck(L_58);
		MonoBehaviour_StartCoroutine_m2470621050(L_58, L_60, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_01a4:
	{
		return (bool)0;
	}

IL_01a6:
	{
		return (bool)1;
	}
}
// System.Object UIButtonTutorial/<YOpenTutorial>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CYOpenTutorialU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2296715611 (U3CYOpenTutorialU3Ec__Iterator1_t230414610 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object UIButtonTutorial/<YOpenTutorial>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CYOpenTutorialU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3525481059 (U3CYOpenTutorialU3Ec__Iterator1_t230414610 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void UIButtonTutorial/<YOpenTutorial>c__Iterator1::Dispose()
extern "C"  void U3CYOpenTutorialU3Ec__Iterator1_Dispose_m2380291780 (U3CYOpenTutorialU3Ec__Iterator1_t230414610 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UIButtonTutorial/<YOpenTutorial>c__Iterator1::Reset()
extern "C"  void U3CYOpenTutorialU3Ec__Iterator1_Reset_m517888270 (U3CYOpenTutorialU3Ec__Iterator1_t230414610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CYOpenTutorialU3Ec__Iterator1_Reset_m517888270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UIButtonUpdate::.ctor()
extern "C"  void UIButtonUpdate__ctor_m86915294 (UIButtonUpdate_t766659849 * __this, const MethodInfo* method)
{
	{
		UIButton__ctor_m4190358109(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIButtonUpdate::Start()
extern "C"  void UIButtonUpdate_Start_m1534861910 (UIButtonUpdate_t766659849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonUpdate_Start_m1534861910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t3226471752 * L_0 = ((UIButton_t3377238306 *)__this)->get_OnClickDown_2();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIButtonUpdate_HandleMouseDown_m3144446185_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_3 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UIButton_t3377238306 *)__this)->set_OnClickDown_2(((Action_t3226471752 *)CastclassSealed(L_3, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIButtonUpdate::HandleMouseDown()
extern "C"  void UIButtonUpdate_HandleMouseDown_m3144446185 (UIButtonUpdate_t766659849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIButtonUpdate_HandleMouseDown_m3144446185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Application_OpenURL_m3882634228(NULL /*static, unused*/, _stringLiteral2836116641, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Utils::.cctor()
extern "C"  void Utils__cctor_m2850823503 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils__cctor_m2850823503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Random_t1044426839 * L_0 = (Random_t1044426839 *)il2cpp_codegen_object_new(Random_t1044426839_il2cpp_TypeInfo_var);
		Random__ctor_m1561335652(L_0, /*hidden argument*/NULL);
		((Utils_t4194145797_StaticFields*)Utils_t4194145797_il2cpp_TypeInfo_var->static_fields)->set_rng_0(L_0);
		return;
	}
}
// System.Void WaterSnow::.ctor()
extern "C"  void WaterSnow__ctor_m2964919991 (WaterSnow_t2237366536 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterSnow::Start()
extern "C"  void WaterSnow_Start_m3757215907 (WaterSnow_t2237366536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterSnow_Start_m3757215907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Transform_GetChild_m3838588184(L_0, 0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		__this->set_waterDrop_4(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Transform_GetChild_m3838588184(L_3, 1, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		__this->set_snowFlake_5(L_5);
		GameObject_t1756533147 * L_6 = __this->get_snowFlake_5();
		NullCheck(L_6);
		SpriteRenderer_t1209076198 * L_7 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_6, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		GameElevenManager_t2036900502 * L_8 = SingletonBase_1_get_Instance_m3475080457(NULL /*static, unused*/, /*hidden argument*/SingletonBase_1_get_Instance_m3475080457_MethodInfo_var);
		NullCheck(L_8);
		SpriteU5BU5D_t3359083662* L_9 = L_8->get_snowflakeSprites_12();
		GameElevenManager_t2036900502 * L_10 = SingletonBase_1_get_Instance_m3475080457(NULL /*static, unused*/, /*hidden argument*/SingletonBase_1_get_Instance_m3475080457_MethodInfo_var);
		NullCheck(L_10);
		SpriteU5BU5D_t3359083662* L_11 = L_10->get_snowflakeSprites_12();
		NullCheck(L_11);
		int32_t L_12 = Random_Range_m694320887(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_13 = L_12;
		Sprite_t309593783 * L_14 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_7);
		SpriteRenderer_set_sprite_m617298623(L_7, L_14, /*hidden argument*/NULL);
		Rigidbody2D_t502193897 * L_15 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set_rb2d_6(L_15);
		Action_t3226471752 * L_16 = ((EventManager_t2792515701_StaticFields*)EventManager_t2792515701_il2cpp_TypeInfo_var->static_fields)->get_OnMouseDown_0();
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)WaterSnow_HandleMouseDown_m2406703338_MethodInfo_var);
		Action_t3226471752 * L_18 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_18, __this, L_17, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_19 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_16, L_18, /*hidden argument*/NULL);
		((EventManager_t2792515701_StaticFields*)EventManager_t2792515701_il2cpp_TypeInfo_var->static_fields)->set_OnMouseDown_0(((Action_t3226471752 *)CastclassSealed(L_19, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WaterSnow::OnDestroy()
extern "C"  void WaterSnow_OnDestroy_m3504045010 (WaterSnow_t2237366536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterSnow_OnDestroy_m3504045010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_t3226471752 * L_0 = ((EventManager_t2792515701_StaticFields*)EventManager_t2792515701_il2cpp_TypeInfo_var->static_fields)->get_OnMouseDown_0();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)WaterSnow_HandleMouseDown_m2406703338_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_3 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((EventManager_t2792515701_StaticFields*)EventManager_t2792515701_il2cpp_TypeInfo_var->static_fields)->set_OnMouseDown_0(((Action_t3226471752 *)CastclassSealed(L_3, Action_t3226471752_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WaterSnow::HandleMouseDown()
extern "C"  void WaterSnow_HandleMouseDown_m2406703338 (WaterSnow_t2237366536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterSnow_HandleMouseDown_m2406703338_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t4063908774  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameElevenManager_t2036900502 * L_0 = SingletonBase_1_get_Instance_m3475080457(NULL /*static, unused*/, /*hidden argument*/SingletonBase_1_get_Instance_m3475080457_MethodInfo_var);
		NullCheck(L_0);
		Camera_t189460977 * L_1 = SingletonBase_1_get_MainCamera_m3861158774(L_0, /*hidden argument*/SingletonBase_1_get_MainCamera_m3861158774_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_2 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Ray_t2469606224  L_3 = Camera_ScreenPointToRay_m614889538(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Ray_t2469606224  L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_5 = Physics2D_GetRayIntersection_m2295701053(NULL /*static, unused*/, L_4, (1000.0f), /*hidden argument*/NULL);
		V_1 = L_5;
		RaycastHit2D_t4063908774  L_6 = V_1;
		bool L_7 = RaycastHit2D_op_Implicit_m596912073(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00e5;
		}
	}
	{
		Collider2D_t646061738 * L_8 = RaycastHit2D_get_collider_m2568504212((&V_1), /*hidden argument*/NULL);
		Collider2D_t646061738 * L_9 = Component_GetComponent_TisCollider2D_t646061738_m2950827934(__this, /*hidden argument*/Component_GetComponent_TisCollider2D_t646061738_m2950827934_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00e5;
		}
	}
	{
		bool L_11 = __this->get_isSnow_3();
		if (L_11)
		{
			goto IL_00e5;
		}
	}
	{
		SoundManager_t654432262 * L_12 = SingletonBase_1_get_Instance_m3325444209(NULL /*static, unused*/, /*hidden argument*/SingletonBase_1_get_Instance_m3325444209_MethodInfo_var);
		SoundManager_t654432262 * L_13 = SingletonBase_1_get_Instance_m3325444209(NULL /*static, unused*/, /*hidden argument*/SingletonBase_1_get_Instance_m3325444209_MethodInfo_var);
		NullCheck(L_13);
		SoundClip_t2598932235 * L_14 = L_13->get_waterToSnowChange_37();
		NullCheck(L_14);
		AudioClip_t1932558630 * L_15 = L_14->get_sound_0();
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_position_m1104419803(L_17, /*hidden argument*/NULL);
		NullCheck(L_12);
		SoundManager_Play_m53396906(L_12, L_15, L_16, L_18, (1.0f), (((int64_t)((int64_t)0))), (bool)0, /*hidden argument*/NULL);
		Rigidbody2D_t502193897 * L_19 = __this->get_rb2d_6();
		NullCheck(L_19);
		Rigidbody2D_set_isKinematic_m548319077(L_19, (bool)1, /*hidden argument*/NULL);
		Rigidbody2D_t502193897 * L_20 = __this->get_rb2d_6();
		Vector3_t2243707580  L_21 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_22 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Rigidbody2D_set_velocity_m3592751374(L_20, L_22, /*hidden argument*/NULL);
		__this->set_isSnow_3((bool)1);
		GameObject_t1756533147 * L_23 = __this->get_waterDrop_4();
		NullCheck(L_23);
		GameObject_SetActive_m2887581199(L_23, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_24 = __this->get_snowFlake_5();
		NullCheck(L_24);
		GameObject_SetActive_m2887581199(L_24, (bool)1, /*hidden argument*/NULL);
		CircleCollider2D_t13116344 * L_25 = Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442(__this, /*hidden argument*/Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442_MethodInfo_var);
		CircleCollider2D_t13116344 * L_26 = L_25;
		NullCheck(L_26);
		float L_27 = CircleCollider2D_get_radius_m3114753530(L_26, /*hidden argument*/NULL);
		NullCheck(L_26);
		CircleCollider2D_set_radius_m1315641707(L_26, ((float)((float)L_27/(float)(2.0f))), /*hidden argument*/NULL);
		Il2CppObject * L_28 = WaterSnow_YSnowFall_m467672314(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_28, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		return;
	}
}
// System.Collections.IEnumerator WaterSnow::YSnowFall()
extern "C"  Il2CppObject * WaterSnow_YSnowFall_m467672314 (WaterSnow_t2237366536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterSnow_YSnowFall_m467672314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CYSnowFallU3Ec__Iterator0_t3674066925 * V_0 = NULL;
	{
		U3CYSnowFallU3Ec__Iterator0_t3674066925 * L_0 = (U3CYSnowFallU3Ec__Iterator0_t3674066925 *)il2cpp_codegen_object_new(U3CYSnowFallU3Ec__Iterator0_t3674066925_il2cpp_TypeInfo_var);
		U3CYSnowFallU3Ec__Iterator0__ctor_m703817188(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CYSnowFallU3Ec__Iterator0_t3674066925 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CYSnowFallU3Ec__Iterator0_t3674066925 * L_2 = V_0;
		return L_2;
	}
}
// System.Void WaterSnow::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void WaterSnow_OnTriggerEnter2D_m2400851115 (WaterSnow_t2237366536 * __this, Collider2D_t646061738 * ___collision0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterSnow_OnTriggerEnter2D_m2400851115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___collision0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m357168014(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral2670825768, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005b;
		}
	}
	{
		bool L_3 = __this->get_isSnow_3();
		if (!L_3)
		{
			goto IL_0040;
		}
	}
	{
		GameElevenManager_t2036900502 * L_4 = SingletonBase_1_get_Instance_m3475080457(NULL /*static, unused*/, /*hidden argument*/SingletonBase_1_get_Instance_m3475080457_MethodInfo_var);
		NullCheck(L_4);
		GameElevenManager_IncrementSnowCount_m3930045437(L_4, 1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_5, (0.2f), /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0040:
	{
		GameElevenManager_t2036900502 * L_6 = SingletonBase_1_get_Instance_m3475080457(NULL /*static, unused*/, /*hidden argument*/SingletonBase_1_get_Instance_m3475080457_MethodInfo_var);
		NullCheck(L_6);
		GameElevenManager_IncrementSnowCount_m3930045437(L_6, (-1), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_7, (0.2f), /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void WaterSnow/<YSnowFall>c__Iterator0::.ctor()
extern "C"  void U3CYSnowFallU3Ec__Iterator0__ctor_m703817188 (U3CYSnowFallU3Ec__Iterator0_t3674066925 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WaterSnow/<YSnowFall>c__Iterator0::MoveNext()
extern "C"  bool U3CYSnowFallU3Ec__Iterator0_MoveNext_m1581898272 (U3CYSnowFallU3Ec__Iterator0_t3674066925 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_010b;
			}
		}
	}
	{
		goto IL_0134;
	}

IL_0021:
	{
		__this->set_U3CvalU3E__0_0(1);
		int32_t L_2 = Random_Range_m694320887(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (!L_3)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)1)))
		{
			goto IL_004e;
		}
	}
	{
		goto IL_005a;
	}

IL_0042:
	{
		__this->set_U3CvalU3E__0_0(1);
		goto IL_005a;
	}

IL_004e:
	{
		__this->set_U3CvalU3E__0_0((-1));
		goto IL_005a;
	}

IL_005a:
	{
		__this->set_U3CtimerU3E__0_1((0.0f));
		goto IL_010b;
	}

IL_006a:
	{
		float L_5 = __this->get_U3CtimerU3E__0_1();
		float L_6 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtimerU3E__0_1(((float)((float)L_5+(float)L_6)));
		WaterSnow_t2237366536 * L_7 = __this->get_U24this_2();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		WaterSnow_t2237366536 * L_9 = __this->get_U24this_2();
		NullCheck(L_9);
		float L_10 = L_9->get_snowFlakeSpeed_2();
		int32_t L_11 = __this->get_U3CvalU3E__0_0();
		float L_12 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		WaterSnow_t2237366536 * L_13 = __this->get_U24this_2();
		NullCheck(L_13);
		float L_14 = L_13->get_snowFlakeSpeed_2();
		float L_15 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2638739322(&L_16, ((float)((float)((float)((float)((float)((float)L_10*(float)(((float)((float)L_11)))))*(float)L_12))/(float)(2.0f))), ((float)((float)((-L_14))*(float)L_15)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_Translate_m3316827744(L_8, L_16, /*hidden argument*/NULL);
		float L_17 = __this->get_U3CtimerU3E__0_1();
		if ((!(((float)L_17) >= ((float)(0.5f)))))
		{
			goto IL_00f0;
		}
	}
	{
		int32_t L_18 = __this->get_U3CvalU3E__0_0();
		__this->set_U3CvalU3E__0_0(((int32_t)((int32_t)L_18*(int32_t)(-1))));
		__this->set_U3CtimerU3E__0_1((0.0f));
	}

IL_00f0:
	{
		__this->set_U24current_3(NULL);
		bool L_19 = __this->get_U24disposing_4();
		if (L_19)
		{
			goto IL_0106;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0106:
	{
		goto IL_0136;
	}

IL_010b:
	{
		WaterSnow_t2237366536 * L_20 = __this->get_U24this_2();
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_position_m1104419803(L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		float L_23 = (&V_2)->get_y_2();
		if ((((float)L_23) > ((float)(-7.0f))))
		{
			goto IL_006a;
		}
	}
	{
		__this->set_U24PC_5((-1));
	}

IL_0134:
	{
		return (bool)0;
	}

IL_0136:
	{
		return (bool)1;
	}
}
// System.Object WaterSnow/<YSnowFall>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CYSnowFallU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m857497630 (U3CYSnowFallU3Ec__Iterator0_t3674066925 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object WaterSnow/<YSnowFall>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CYSnowFallU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2877398534 (U3CYSnowFallU3Ec__Iterator0_t3674066925 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void WaterSnow/<YSnowFall>c__Iterator0::Dispose()
extern "C"  void U3CYSnowFallU3Ec__Iterator0_Dispose_m2557195935 (U3CYSnowFallU3Ec__Iterator0_t3674066925 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void WaterSnow/<YSnowFall>c__Iterator0::Reset()
extern "C"  void U3CYSnowFallU3Ec__Iterator0_Reset_m1409936625 (U3CYSnowFallU3Ec__Iterator0_t3674066925 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CYSnowFallU3Ec__Iterator0_Reset_m1409936625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Yey::.ctor()
extern "C"  void Yey__ctor_m738196528 (Yey_t2293551375 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Yey::Scale()
extern "C"  void Yey_Scale_m11203238 (Yey_t2293551375 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Yey_Scale_m11203238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	U3CScaleU3Ec__AnonStorey0_t1001303329 * V_1 = NULL;
	Tweener_t760404022 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = Transform_GetEnumerator_m3479720613(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005b;
		}

IL_0011:
		{
			U3CScaleU3Ec__AnonStorey0_t1001303329 * L_2 = (U3CScaleU3Ec__AnonStorey0_t1001303329 *)il2cpp_codegen_object_new(U3CScaleU3Ec__AnonStorey0_t1001303329_il2cpp_TypeInfo_var);
			U3CScaleU3Ec__AnonStorey0__ctor_m1495609928(L_2, /*hidden argument*/NULL);
			V_1 = L_2;
			U3CScaleU3Ec__AnonStorey0_t1001303329 * L_3 = V_1;
			Il2CppObject * L_4 = V_0;
			NullCheck(L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_4);
			NullCheck(L_3);
			L_3->set_t_0(((Transform_t3275118058 *)CastclassClass(L_5, Transform_t3275118058_il2cpp_TypeInfo_var)));
			U3CScaleU3Ec__AnonStorey0_t1001303329 * L_6 = V_1;
			NullCheck(L_6);
			Transform_t3275118058 * L_7 = L_6->get_t_0();
			Vector3_t2243707580  L_8 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
			Vector3_t2243707580  L_9 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_8, (1.2f), /*hidden argument*/NULL);
			Tweener_t760404022 * L_10 = ShortcutExtensions_DOScale_m2754995414(NULL /*static, unused*/, L_7, L_9, (0.1f), /*hidden argument*/NULL);
			V_2 = L_10;
			Tweener_t760404022 * L_11 = V_2;
			U3CScaleU3Ec__AnonStorey0_t1001303329 * L_12 = V_1;
			IntPtr_t L_13;
			L_13.set_m_value_0((void*)(void*)U3CScaleU3Ec__AnonStorey0_U3CU3Em__0_m3438436187_MethodInfo_var);
			TweenCallback_t3697142134 * L_14 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
			TweenCallback__ctor_m3479200459(L_14, L_12, L_13, /*hidden argument*/NULL);
			TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386_MethodInfo_var);
		}

IL_005b:
		{
			Il2CppObject * L_15 = V_0;
			NullCheck(L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0011;
			}
		}

IL_0066:
		{
			IL2CPP_LEAVE(0x7F, FINALLY_006b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006b;
	}

FINALLY_006b:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_17 = V_0;
			Il2CppObject * L_18 = ((Il2CppObject *)IsInst(L_17, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_3 = L_18;
			if (!L_18)
			{
				goto IL_007e;
			}
		}

IL_0078:
		{
			Il2CppObject * L_19 = V_3;
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_19);
		}

IL_007e:
		{
			IL2CPP_END_FINALLY(107)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(107)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007f:
	{
		return;
	}
}
// System.Void Yey/<Scale>c__AnonStorey0::.ctor()
extern "C"  void U3CScaleU3Ec__AnonStorey0__ctor_m1495609928 (U3CScaleU3Ec__AnonStorey0_t1001303329 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Yey/<Scale>c__AnonStorey0::<>m__0()
extern "C"  void U3CScaleU3Ec__AnonStorey0_U3CU3Em__0_m3438436187 (U3CScaleU3Ec__AnonStorey0_t1001303329 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_t_0();
		Vector3_t2243707580  L_1 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		ShortcutExtensions_DOScale_m2754995414(NULL /*static, unused*/, L_0, L_1, (0.2f), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
