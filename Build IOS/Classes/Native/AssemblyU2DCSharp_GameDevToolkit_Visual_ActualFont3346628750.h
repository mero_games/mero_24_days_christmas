﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Visual.ActualFontSize
struct  ActualFontSize_t3346628750  : public MonoBehaviour_t1158329972
{
public:
	// System.Single GameDevToolkit.Visual.ActualFontSize::FontSize
	float ___FontSize_2;

public:
	inline static int32_t get_offset_of_FontSize_2() { return static_cast<int32_t>(offsetof(ActualFontSize_t3346628750, ___FontSize_2)); }
	inline float get_FontSize_2() const { return ___FontSize_2; }
	inline float* get_address_of_FontSize_2() { return &___FontSize_2; }
	inline void set_FontSize_2(float value)
	{
		___FontSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
