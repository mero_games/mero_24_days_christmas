﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonBase_1_gen1271152307.h"

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSixManager
struct  GameSixManager_t2271637217  : public SingletonBase_1_t1271152307
{
public:
	// System.Boolean GameSixManager::isPlaying
	bool ___isPlaying_4;
	// System.Int32 GameSixManager::candiesToCollect
	int32_t ___candiesToCollect_5;
	// System.Int32 GameSixManager::candiesCollected
	int32_t ___candiesCollected_6;
	// System.Int32 GameSixManager::chanceToSpawnCoal
	int32_t ___chanceToSpawnCoal_7;
	// System.Single GameSixManager::xSpawnLimit
	float ___xSpawnLimit_8;
	// System.Single GameSixManager::candySpawnRate
	float ___candySpawnRate_9;
	// System.Single GameSixManager::candySpawnHeight
	float ___candySpawnHeight_10;
	// UnityEngine.Sprite[] GameSixManager::candySprites
	SpriteU5BU5D_t3359083662* ___candySprites_11;
	// UnityEngine.Transform GameSixManager::boot
	Transform_t3275118058 * ___boot_12;
	// UnityEngine.GameObject GameSixManager::candy
	GameObject_t1756533147 * ___candy_13;
	// UnityEngine.GameObject GameSixManager::coal
	GameObject_t1756533147 * ___coal_14;
	// UnityEngine.Coroutine GameSixManager::bootCoroutine
	Coroutine_t2299508840 * ___bootCoroutine_15;

public:
	inline static int32_t get_offset_of_isPlaying_4() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___isPlaying_4)); }
	inline bool get_isPlaying_4() const { return ___isPlaying_4; }
	inline bool* get_address_of_isPlaying_4() { return &___isPlaying_4; }
	inline void set_isPlaying_4(bool value)
	{
		___isPlaying_4 = value;
	}

	inline static int32_t get_offset_of_candiesToCollect_5() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___candiesToCollect_5)); }
	inline int32_t get_candiesToCollect_5() const { return ___candiesToCollect_5; }
	inline int32_t* get_address_of_candiesToCollect_5() { return &___candiesToCollect_5; }
	inline void set_candiesToCollect_5(int32_t value)
	{
		___candiesToCollect_5 = value;
	}

	inline static int32_t get_offset_of_candiesCollected_6() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___candiesCollected_6)); }
	inline int32_t get_candiesCollected_6() const { return ___candiesCollected_6; }
	inline int32_t* get_address_of_candiesCollected_6() { return &___candiesCollected_6; }
	inline void set_candiesCollected_6(int32_t value)
	{
		___candiesCollected_6 = value;
	}

	inline static int32_t get_offset_of_chanceToSpawnCoal_7() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___chanceToSpawnCoal_7)); }
	inline int32_t get_chanceToSpawnCoal_7() const { return ___chanceToSpawnCoal_7; }
	inline int32_t* get_address_of_chanceToSpawnCoal_7() { return &___chanceToSpawnCoal_7; }
	inline void set_chanceToSpawnCoal_7(int32_t value)
	{
		___chanceToSpawnCoal_7 = value;
	}

	inline static int32_t get_offset_of_xSpawnLimit_8() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___xSpawnLimit_8)); }
	inline float get_xSpawnLimit_8() const { return ___xSpawnLimit_8; }
	inline float* get_address_of_xSpawnLimit_8() { return &___xSpawnLimit_8; }
	inline void set_xSpawnLimit_8(float value)
	{
		___xSpawnLimit_8 = value;
	}

	inline static int32_t get_offset_of_candySpawnRate_9() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___candySpawnRate_9)); }
	inline float get_candySpawnRate_9() const { return ___candySpawnRate_9; }
	inline float* get_address_of_candySpawnRate_9() { return &___candySpawnRate_9; }
	inline void set_candySpawnRate_9(float value)
	{
		___candySpawnRate_9 = value;
	}

	inline static int32_t get_offset_of_candySpawnHeight_10() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___candySpawnHeight_10)); }
	inline float get_candySpawnHeight_10() const { return ___candySpawnHeight_10; }
	inline float* get_address_of_candySpawnHeight_10() { return &___candySpawnHeight_10; }
	inline void set_candySpawnHeight_10(float value)
	{
		___candySpawnHeight_10 = value;
	}

	inline static int32_t get_offset_of_candySprites_11() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___candySprites_11)); }
	inline SpriteU5BU5D_t3359083662* get_candySprites_11() const { return ___candySprites_11; }
	inline SpriteU5BU5D_t3359083662** get_address_of_candySprites_11() { return &___candySprites_11; }
	inline void set_candySprites_11(SpriteU5BU5D_t3359083662* value)
	{
		___candySprites_11 = value;
		Il2CppCodeGenWriteBarrier(&___candySprites_11, value);
	}

	inline static int32_t get_offset_of_boot_12() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___boot_12)); }
	inline Transform_t3275118058 * get_boot_12() const { return ___boot_12; }
	inline Transform_t3275118058 ** get_address_of_boot_12() { return &___boot_12; }
	inline void set_boot_12(Transform_t3275118058 * value)
	{
		___boot_12 = value;
		Il2CppCodeGenWriteBarrier(&___boot_12, value);
	}

	inline static int32_t get_offset_of_candy_13() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___candy_13)); }
	inline GameObject_t1756533147 * get_candy_13() const { return ___candy_13; }
	inline GameObject_t1756533147 ** get_address_of_candy_13() { return &___candy_13; }
	inline void set_candy_13(GameObject_t1756533147 * value)
	{
		___candy_13 = value;
		Il2CppCodeGenWriteBarrier(&___candy_13, value);
	}

	inline static int32_t get_offset_of_coal_14() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___coal_14)); }
	inline GameObject_t1756533147 * get_coal_14() const { return ___coal_14; }
	inline GameObject_t1756533147 ** get_address_of_coal_14() { return &___coal_14; }
	inline void set_coal_14(GameObject_t1756533147 * value)
	{
		___coal_14 = value;
		Il2CppCodeGenWriteBarrier(&___coal_14, value);
	}

	inline static int32_t get_offset_of_bootCoroutine_15() { return static_cast<int32_t>(offsetof(GameSixManager_t2271637217, ___bootCoroutine_15)); }
	inline Coroutine_t2299508840 * get_bootCoroutine_15() const { return ___bootCoroutine_15; }
	inline Coroutine_t2299508840 ** get_address_of_bootCoroutine_15() { return &___bootCoroutine_15; }
	inline void set_bootCoroutine_15(Coroutine_t2299508840 * value)
	{
		___bootCoroutine_15 = value;
		Il2CppCodeGenWriteBarrier(&___bootCoroutine_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
