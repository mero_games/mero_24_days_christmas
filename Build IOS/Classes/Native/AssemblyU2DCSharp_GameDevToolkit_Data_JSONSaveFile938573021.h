﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SimpleJSON_JSONNode1250409636.h"

// SimpleJSON.JSONClass
struct JSONClass_t1609506608;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Data.JSONSaveFile
struct  JSONSaveFile_t938573021  : public JSONNode_t1250409636
{
public:
	// SimpleJSON.JSONClass GameDevToolkit.Data.JSONSaveFile::DEFAULT_VALUES
	JSONClass_t1609506608 * ___DEFAULT_VALUES_0;
	// SimpleJSON.JSONClass GameDevToolkit.Data.JSONSaveFile::curSaveFile
	JSONClass_t1609506608 * ___curSaveFile_1;
	// System.Boolean GameDevToolkit.Data.JSONSaveFile::newSaveFile
	bool ___newSaveFile_2;
	// System.Boolean GameDevToolkit.Data.JSONSaveFile::isProtected
	bool ___isProtected_3;
	// System.Boolean GameDevToolkit.Data.JSONSaveFile::encrypted
	bool ___encrypted_4;
	// System.String GameDevToolkit.Data.JSONSaveFile::fName
	String_t* ___fName_5;
	// System.String GameDevToolkit.Data.JSONSaveFile::encryptionKey
	String_t* ___encryptionKey_6;

public:
	inline static int32_t get_offset_of_DEFAULT_VALUES_0() { return static_cast<int32_t>(offsetof(JSONSaveFile_t938573021, ___DEFAULT_VALUES_0)); }
	inline JSONClass_t1609506608 * get_DEFAULT_VALUES_0() const { return ___DEFAULT_VALUES_0; }
	inline JSONClass_t1609506608 ** get_address_of_DEFAULT_VALUES_0() { return &___DEFAULT_VALUES_0; }
	inline void set_DEFAULT_VALUES_0(JSONClass_t1609506608 * value)
	{
		___DEFAULT_VALUES_0 = value;
		Il2CppCodeGenWriteBarrier(&___DEFAULT_VALUES_0, value);
	}

	inline static int32_t get_offset_of_curSaveFile_1() { return static_cast<int32_t>(offsetof(JSONSaveFile_t938573021, ___curSaveFile_1)); }
	inline JSONClass_t1609506608 * get_curSaveFile_1() const { return ___curSaveFile_1; }
	inline JSONClass_t1609506608 ** get_address_of_curSaveFile_1() { return &___curSaveFile_1; }
	inline void set_curSaveFile_1(JSONClass_t1609506608 * value)
	{
		___curSaveFile_1 = value;
		Il2CppCodeGenWriteBarrier(&___curSaveFile_1, value);
	}

	inline static int32_t get_offset_of_newSaveFile_2() { return static_cast<int32_t>(offsetof(JSONSaveFile_t938573021, ___newSaveFile_2)); }
	inline bool get_newSaveFile_2() const { return ___newSaveFile_2; }
	inline bool* get_address_of_newSaveFile_2() { return &___newSaveFile_2; }
	inline void set_newSaveFile_2(bool value)
	{
		___newSaveFile_2 = value;
	}

	inline static int32_t get_offset_of_isProtected_3() { return static_cast<int32_t>(offsetof(JSONSaveFile_t938573021, ___isProtected_3)); }
	inline bool get_isProtected_3() const { return ___isProtected_3; }
	inline bool* get_address_of_isProtected_3() { return &___isProtected_3; }
	inline void set_isProtected_3(bool value)
	{
		___isProtected_3 = value;
	}

	inline static int32_t get_offset_of_encrypted_4() { return static_cast<int32_t>(offsetof(JSONSaveFile_t938573021, ___encrypted_4)); }
	inline bool get_encrypted_4() const { return ___encrypted_4; }
	inline bool* get_address_of_encrypted_4() { return &___encrypted_4; }
	inline void set_encrypted_4(bool value)
	{
		___encrypted_4 = value;
	}

	inline static int32_t get_offset_of_fName_5() { return static_cast<int32_t>(offsetof(JSONSaveFile_t938573021, ___fName_5)); }
	inline String_t* get_fName_5() const { return ___fName_5; }
	inline String_t** get_address_of_fName_5() { return &___fName_5; }
	inline void set_fName_5(String_t* value)
	{
		___fName_5 = value;
		Il2CppCodeGenWriteBarrier(&___fName_5, value);
	}

	inline static int32_t get_offset_of_encryptionKey_6() { return static_cast<int32_t>(offsetof(JSONSaveFile_t938573021, ___encryptionKey_6)); }
	inline String_t* get_encryptionKey_6() const { return ___encryptionKey_6; }
	inline String_t** get_address_of_encryptionKey_6() { return &___encryptionKey_6; }
	inline void set_encryptionKey_6(String_t* value)
	{
		___encryptionKey_6 = value;
		Il2CppCodeGenWriteBarrier(&___encryptionKey_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
