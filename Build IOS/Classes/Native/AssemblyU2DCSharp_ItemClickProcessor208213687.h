﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemClickProcessor
struct  ItemClickProcessor_t208213687  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ItemClickProcessor::beenOpened
	bool ___beenOpened_2;
	// System.Int32 ItemClickProcessor::clickCounter
	int32_t ___clickCounter_3;
	// System.Int32 ItemClickProcessor::clickThreshold
	int32_t ___clickThreshold_4;

public:
	inline static int32_t get_offset_of_beenOpened_2() { return static_cast<int32_t>(offsetof(ItemClickProcessor_t208213687, ___beenOpened_2)); }
	inline bool get_beenOpened_2() const { return ___beenOpened_2; }
	inline bool* get_address_of_beenOpened_2() { return &___beenOpened_2; }
	inline void set_beenOpened_2(bool value)
	{
		___beenOpened_2 = value;
	}

	inline static int32_t get_offset_of_clickCounter_3() { return static_cast<int32_t>(offsetof(ItemClickProcessor_t208213687, ___clickCounter_3)); }
	inline int32_t get_clickCounter_3() const { return ___clickCounter_3; }
	inline int32_t* get_address_of_clickCounter_3() { return &___clickCounter_3; }
	inline void set_clickCounter_3(int32_t value)
	{
		___clickCounter_3 = value;
	}

	inline static int32_t get_offset_of_clickThreshold_4() { return static_cast<int32_t>(offsetof(ItemClickProcessor_t208213687, ___clickThreshold_4)); }
	inline int32_t get_clickThreshold_4() const { return ___clickThreshold_4; }
	inline int32_t* get_address_of_clickThreshold_4() { return &___clickThreshold_4; }
	inline void set_clickThreshold_4(int32_t value)
	{
		___clickThreshold_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
