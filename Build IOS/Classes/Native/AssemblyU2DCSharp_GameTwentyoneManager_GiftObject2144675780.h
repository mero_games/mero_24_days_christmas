﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// UnityEngine.Texture
struct Texture_t2243626319;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTwentyoneManager/GiftObject
struct  GiftObject_t2144675780 
{
public:
	// UnityEngine.Texture GameTwentyoneManager/GiftObject::gift
	Texture_t2243626319 * ___gift_0;
	// UnityEngine.Texture GameTwentyoneManager/GiftObject::wrapper
	Texture_t2243626319 * ___wrapper_1;

public:
	inline static int32_t get_offset_of_gift_0() { return static_cast<int32_t>(offsetof(GiftObject_t2144675780, ___gift_0)); }
	inline Texture_t2243626319 * get_gift_0() const { return ___gift_0; }
	inline Texture_t2243626319 ** get_address_of_gift_0() { return &___gift_0; }
	inline void set_gift_0(Texture_t2243626319 * value)
	{
		___gift_0 = value;
		Il2CppCodeGenWriteBarrier(&___gift_0, value);
	}

	inline static int32_t get_offset_of_wrapper_1() { return static_cast<int32_t>(offsetof(GiftObject_t2144675780, ___wrapper_1)); }
	inline Texture_t2243626319 * get_wrapper_1() const { return ___wrapper_1; }
	inline Texture_t2243626319 ** get_address_of_wrapper_1() { return &___wrapper_1; }
	inline void set_wrapper_1(Texture_t2243626319 * value)
	{
		___wrapper_1 = value;
		Il2CppCodeGenWriteBarrier(&___wrapper_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GameTwentyoneManager/GiftObject
struct GiftObject_t2144675780_marshaled_pinvoke
{
	Texture_t2243626319 * ___gift_0;
	Texture_t2243626319 * ___wrapper_1;
};
// Native definition for COM marshalling of GameTwentyoneManager/GiftObject
struct GiftObject_t2144675780_marshaled_com
{
	Texture_t2243626319 * ___gift_0;
	Texture_t2243626319 * ___wrapper_1;
};
