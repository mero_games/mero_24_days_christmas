﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GameDevToolkit.GameUtil.TwoDGameObjectArray[]
struct TwoDGameObjectArrayU5BU5D_t797249741;
// GameDevToolkit.GameUtil.Randomizer
struct Randomizer_t1796594675;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.GameUtil.GridRepGenereic`2<UnityEngine.GameObject,GameDevToolkit.GameUtil.TwoDGameObjectArray>
struct  GridRepGenereic_2_t1563951573  : public MonoBehaviour_t1158329972
{
public:
	// T2[] GameDevToolkit.GameUtil.GridRepGenereic`2::grid
	TwoDGameObjectArrayU5BU5D_t797249741* ___grid_2;
	// GameDevToolkit.GameUtil.Randomizer GameDevToolkit.GameUtil.GridRepGenereic`2::rowRandom
	Randomizer_t1796594675 * ___rowRandom_3;
	// GameDevToolkit.GameUtil.Randomizer GameDevToolkit.GameUtil.GridRepGenereic`2::cellRandom
	Randomizer_t1796594675 * ___cellRandom_4;

public:
	inline static int32_t get_offset_of_grid_2() { return static_cast<int32_t>(offsetof(GridRepGenereic_2_t1563951573, ___grid_2)); }
	inline TwoDGameObjectArrayU5BU5D_t797249741* get_grid_2() const { return ___grid_2; }
	inline TwoDGameObjectArrayU5BU5D_t797249741** get_address_of_grid_2() { return &___grid_2; }
	inline void set_grid_2(TwoDGameObjectArrayU5BU5D_t797249741* value)
	{
		___grid_2 = value;
		Il2CppCodeGenWriteBarrier(&___grid_2, value);
	}

	inline static int32_t get_offset_of_rowRandom_3() { return static_cast<int32_t>(offsetof(GridRepGenereic_2_t1563951573, ___rowRandom_3)); }
	inline Randomizer_t1796594675 * get_rowRandom_3() const { return ___rowRandom_3; }
	inline Randomizer_t1796594675 ** get_address_of_rowRandom_3() { return &___rowRandom_3; }
	inline void set_rowRandom_3(Randomizer_t1796594675 * value)
	{
		___rowRandom_3 = value;
		Il2CppCodeGenWriteBarrier(&___rowRandom_3, value);
	}

	inline static int32_t get_offset_of_cellRandom_4() { return static_cast<int32_t>(offsetof(GridRepGenereic_2_t1563951573, ___cellRandom_4)); }
	inline Randomizer_t1796594675 * get_cellRandom_4() const { return ___cellRandom_4; }
	inline Randomizer_t1796594675 ** get_address_of_cellRandom_4() { return &___cellRandom_4; }
	inline void set_cellRandom_4(Randomizer_t1796594675 * value)
	{
		___cellRandom_4 = value;
		Il2CppCodeGenWriteBarrier(&___cellRandom_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
