﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameDevToolkit.Sound.SoundClip
struct SoundClip_t3743947423;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// GameDevToolkit.Sound.SoundManager
struct SoundManager_t13585936;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Sound.SoundManager/<PlayWithDelay>c__Iterator0
struct  U3CPlayWithDelayU3Ec__Iterator0_t711187121  : public Il2CppObject
{
public:
	// System.Single GameDevToolkit.Sound.SoundManager/<PlayWithDelay>c__Iterator0::delayInSeconds
	float ___delayInSeconds_0;
	// GameDevToolkit.Sound.SoundClip GameDevToolkit.Sound.SoundManager/<PlayWithDelay>c__Iterator0::sound
	SoundClip_t3743947423 * ___sound_1;
	// UnityEngine.GameObject GameDevToolkit.Sound.SoundManager/<PlayWithDelay>c__Iterator0::playOn
	GameObject_t1756533147 * ___playOn_2;
	// GameDevToolkit.Sound.SoundManager GameDevToolkit.Sound.SoundManager/<PlayWithDelay>c__Iterator0::$this
	SoundManager_t13585936 * ___U24this_3;
	// System.Object GameDevToolkit.Sound.SoundManager/<PlayWithDelay>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean GameDevToolkit.Sound.SoundManager/<PlayWithDelay>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 GameDevToolkit.Sound.SoundManager/<PlayWithDelay>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_delayInSeconds_0() { return static_cast<int32_t>(offsetof(U3CPlayWithDelayU3Ec__Iterator0_t711187121, ___delayInSeconds_0)); }
	inline float get_delayInSeconds_0() const { return ___delayInSeconds_0; }
	inline float* get_address_of_delayInSeconds_0() { return &___delayInSeconds_0; }
	inline void set_delayInSeconds_0(float value)
	{
		___delayInSeconds_0 = value;
	}

	inline static int32_t get_offset_of_sound_1() { return static_cast<int32_t>(offsetof(U3CPlayWithDelayU3Ec__Iterator0_t711187121, ___sound_1)); }
	inline SoundClip_t3743947423 * get_sound_1() const { return ___sound_1; }
	inline SoundClip_t3743947423 ** get_address_of_sound_1() { return &___sound_1; }
	inline void set_sound_1(SoundClip_t3743947423 * value)
	{
		___sound_1 = value;
		Il2CppCodeGenWriteBarrier(&___sound_1, value);
	}

	inline static int32_t get_offset_of_playOn_2() { return static_cast<int32_t>(offsetof(U3CPlayWithDelayU3Ec__Iterator0_t711187121, ___playOn_2)); }
	inline GameObject_t1756533147 * get_playOn_2() const { return ___playOn_2; }
	inline GameObject_t1756533147 ** get_address_of_playOn_2() { return &___playOn_2; }
	inline void set_playOn_2(GameObject_t1756533147 * value)
	{
		___playOn_2 = value;
		Il2CppCodeGenWriteBarrier(&___playOn_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CPlayWithDelayU3Ec__Iterator0_t711187121, ___U24this_3)); }
	inline SoundManager_t13585936 * get_U24this_3() const { return ___U24this_3; }
	inline SoundManager_t13585936 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SoundManager_t13585936 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CPlayWithDelayU3Ec__Iterator0_t711187121, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CPlayWithDelayU3Ec__Iterator0_t711187121, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CPlayWithDelayU3Ec__Iterator0_t711187121, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
