﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameSevenManager3596770590.h"
#include "AssemblyU2DCSharp_GameSevenManager_U3CYSnowmenRais1043289944.h"
#include "AssemblyU2DCSharp_SnowBall3204273942.h"
#include "AssemblyU2DCSharp_SnowMan902289717.h"
#include "AssemblyU2DCSharp_SnowMan_U3CYUpStandTimerU3Ec__It2603733714.h"
#include "AssemblyU2DCSharp_StartingSlot3631029828.h"
#include "AssemblyU2DCSharp_BackgroundsHolder4153806707.h"
#include "AssemblyU2DCSharp_GameEightManager903757396.h"
#include "AssemblyU2DCSharp_GameEightManager_U3CY2by2U3Ec__It520614058.h"
#include "AssemblyU2DCSharp_GameEightManager_U3CY2by3U3Ec__I2076692560.h"
#include "AssemblyU2DCSharp_GameEightManager_U3CY3by3U3Ec__I3800956998.h"
#include "AssemblyU2DCSharp_GameEightManager_U3CMoveToSlotsU2359960924.h"
#include "AssemblyU2DCSharp_PuzzlePiece1912707670.h"
#include "AssemblyU2DCSharp_PuzzlePiece_U3CYStartDraggingU3E4263427530.h"
#include "AssemblyU2DCSharp_AvatarCreator3048088315.h"
#include "AssemblyU2DCSharp_GameNineManager3632946731.h"
#include "AssemblyU2DCSharp_GameNineManager_U3CYShrinkElfU3E3480698838.h"
#include "AssemblyU2DCSharp_GameNineManager_U3CYDanceSequence833372137.h"
#include "AssemblyU2DCSharp_SongBubble2997910009.h"
#include "AssemblyU2DCSharp_UIButtonUpdate766659849.h"
#include "AssemblyU2DCSharp_AnimateHouse300882193.h"
#include "AssemblyU2DCSharp_AnimateHouse_U3CStartU3Ec__Itera1857383417.h"
#include "AssemblyU2DCSharp_BGMusic3486147098.h"
#include "AssemblyU2DCSharp_BGMusic_U3CStartU3Ec__Iterator01713471636.h"
#include "AssemblyU2DCSharp_BGMusic_U3CYSongPlayerU3Ec__Iter3007034830.h"
#include "AssemblyU2DCSharp_BGMusic_U3CYFadeInU3Ec__Iterator1740319868.h"
#include "AssemblyU2DCSharp_BGMusic_U3CYFadeOutU3Ec__Iterato1176020762.h"
#include "AssemblyU2DCSharp_DG_Tweening_DOTweenAnimation858634588.h"
#include "AssemblyU2DCSharp_DG_Tweening_DOTweenAnimationExte4140352346.h"
#include "AssemblyU2DCSharp_DestroyParticle3576907776.h"
#include "AssemblyU2DCSharp_DestroyParticle_U3CYDestroyU3Ec_1854615267.h"
#include "AssemblyU2DCSharp_DestroyParticle_U3CYDestroyU3Ec__674293687.h"
#include "AssemblyU2DCSharp_DestroySound724042623.h"
#include "AssemblyU2DCSharp_ButtonPlaySoundOnStart3790818244.h"
#include "AssemblyU2DCSharp_ButtonPlaySoundOnStart_U3CPlayTh3748255788.h"
#include "AssemblyU2DCSharp_EndScreen1192627425.h"
#include "AssemblyU2DCSharp_EventManager2792515701.h"
#include "AssemblyU2DCSharp_FlashingObject2247968655.h"
#include "AssemblyU2DCSharp_FlashingObject_U3CFlashU3Ec__Ite2208059419.h"
#include "AssemblyU2DCSharp_FlappyCharExample4200876794.h"
#include "AssemblyU2DCSharp_FlappyCharExample_U3CdoStartU3Ec2987183723.h"
#include "AssemblyU2DCSharp_TapExceptionExample2422957020.h"
#include "AssemblyU2DCSharp_RocketCharExample1513856186.h"
#include "AssemblyU2DCSharp_ArrayUtilsExample1078130992.h"
#include "AssemblyU2DCSharp_GetOrAddExample841780186.h"
#include "AssemblyU2DCSharp_ReadOnlyAttributeExample603047684.h"
#include "AssemblyU2DCSharp_TransformExtensionExample3506032567.h"
#include "AssemblyU2DCSharp_MandatoryDummyImplmplementation1329551270.h"
#include "AssemblyU2DCSharp_TwoDArrayExample2315501275.h"
#include "AssemblyU2DCSharp_CryptoExample1829615961.h"
#include "AssemblyU2DCSharp_HTTPLoaderExample3175293487.h"
#include "AssemblyU2DCSharp_JSONSaveFileExample3967010229.h"
#include "AssemblyU2DCSharp_RedundantFileWriterExample3754595982.h"
#include "AssemblyU2DCSharp_ColliderEventSystemExample224613953.h"
#include "AssemblyU2DCSharp_DummyMouseEventImplementation4020916073.h"
#include "AssemblyU2DCSharp_GenericMouseEventExample856766798.h"
#include "AssemblyU2DCSharp_KeyboardEventSystemExample362032968.h"
#include "AssemblyU2DCSharp_ParticleSystemEventExample1578308911.h"
#include "AssemblyU2DCSharp_ArbitraryComponent2637609733.h"
#include "AssemblyU2DCSharp_GridExamples561734721.h"
#include "AssemblyU2DCSharp_RandomizerExamples274280640.h"
#include "AssemblyU2DCSharp_GetMidPontExample420734737.h"
#include "AssemblyU2DCSharp_LaunchObjectExample608304890.h"
#include "AssemblyU2DCSharp_LaunchObjectExample_U3CResetU3Ec4145257719.h"
#include "AssemblyU2DCSharp_LifeHUDExample2672890885.h"
#include "AssemblyU2DCSharp_ObjectSequenceExample3751909220.h"
#include "AssemblyU2DCSharp_ScalingBarExample50575284.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Characters_ArrowK1318051673.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Characters_Flappy3600499100.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Characters_Rocket1772935162.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Common_ArrayUtils1556298404.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Common_EDUCam524974491.h"
#include "AssemblyU2DCSharp_GameObjectExtension2084883162.h"
#include "AssemblyU2DCSharp_ReadOnlyAttribute3809417938.h"
#include "AssemblyU2DCSharp_TransformExtension690161491.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Data_Crypto569229097.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Data_HTTPLoader2960633167.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Data_HTTPLoader_H3652583718.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Data_HTTPLoader_L2670511940.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Data_HTTPLoader_U2369083150.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Data_JSONSaveFile938573021.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Data_RedundantFil3761749544.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Data_RedundantFile933455898.h"
#include "AssemblyU2DCSharp_GameDevToolkit_Data_RedundantFil4220271987.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONBinaryTag3856753551.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONNode1250409636.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONNode_U3CU3Ec__Iter803137928.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONNode_U3CU3Ec__Ite3532021283.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONArray3986483147.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONArray_U3CU3Ec__It4017688111.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONArray_U3CGetEnume1167240694.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONClass1609506608.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONClass_U3CRemoveU32562212806.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONClass_U3CU3Ec__Ite466495836.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONClass_U3CGetEnumer201440115.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONData531041784.h"
#include "AssemblyU2DCSharp_SimpleJSON_JSONLazyCreator3575446586.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (GameSevenManager_t3596770590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[12] = 
{
	GameSevenManager_t3596770590::get_offset_of_isPlaying_4(),
	GameSevenManager_t3596770590::get_offset_of_hitCounter_5(),
	GameSevenManager_t3596770590::get_offset_of_hitsToLand_6(),
	GameSevenManager_t3596770590::get_offset_of_chanceOfHappy_7(),
	GameSevenManager_t3596770590::get_offset_of_risenCounter_8(),
	GameSevenManager_t3596770590::get_offset_of_upStandTime_9(),
	GameSevenManager_t3596770590::get_offset_of_moveTime_10(),
	GameSevenManager_t3596770590::get_offset_of_riseInterval_11(),
	GameSevenManager_t3596770590::get_offset_of_mouthHappy_12(),
	GameSevenManager_t3596770590::get_offset_of_mouthSad_13(),
	GameSevenManager_t3596770590::get_offset_of_snowmen_14(),
	GameSevenManager_t3596770590::get_offset_of_snowBall_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (U3CYSnowmenRaiserU3Ec__Iterator0_t1043289944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[5] = 
{
	U3CYSnowmenRaiserU3Ec__Iterator0_t1043289944::get_offset_of_U3CtimerU3E__0_0(),
	U3CYSnowmenRaiserU3Ec__Iterator0_t1043289944::get_offset_of_U24this_1(),
	U3CYSnowmenRaiserU3Ec__Iterator0_t1043289944::get_offset_of_U24current_2(),
	U3CYSnowmenRaiserU3Ec__Iterator0_t1043289944::get_offset_of_U24disposing_3(),
	U3CYSnowmenRaiserU3Ec__Iterator0_t1043289944::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (SnowBall_t3204273942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (SnowMan_t902289717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[4] = 
{
	SnowMan_t902289717::get_offset_of_mouth_2(),
	SnowMan_t902289717::get_offset_of_isHappy_3(),
	SnowMan_t902289717::get_offset_of_wasHit_4(),
	SnowMan_t902289717::get_offset_of_isDown_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (U3CYUpStandTimerU3Ec__Iterator0_t2603733714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[5] = 
{
	U3CYUpStandTimerU3Ec__Iterator0_t2603733714::get_offset_of_U3CtimerU3E__0_0(),
	U3CYUpStandTimerU3Ec__Iterator0_t2603733714::get_offset_of_U24this_1(),
	U3CYUpStandTimerU3Ec__Iterator0_t2603733714::get_offset_of_U24current_2(),
	U3CYUpStandTimerU3Ec__Iterator0_t2603733714::get_offset_of_U24disposing_3(),
	U3CYUpStandTimerU3Ec__Iterator0_t2603733714::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (StartingSlot_t3631029828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[2] = 
{
	StartingSlot_t3631029828::get_offset_of_slotTransform_0(),
	StartingSlot_t3631029828::get_offset_of_isTaken_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (BackgroundsHolder_t4153806707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[2] = 
{
	BackgroundsHolder_t4153806707::get_offset_of_wholeBackground_0(),
	BackgroundsHolder_t4153806707::get_offset_of_slicedPieces_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (GameEightManager_t903757396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[14] = 
{
	GameEightManager_t903757396::get_offset_of_counter2by2_4(),
	GameEightManager_t903757396::get_offset_of_counter2by3_5(),
	GameEightManager_t903757396::get_offset_of_counter3by3_6(),
	GameEightManager_t903757396::get_offset_of_grid2by2_7(),
	GameEightManager_t903757396::get_offset_of_grid2by3_8(),
	GameEightManager_t903757396::get_offset_of_grid3by3_9(),
	GameEightManager_t903757396::get_offset_of_background_10(),
	GameEightManager_t903757396::get_offset_of_puzzlePieces3by3_11(),
	GameEightManager_t903757396::get_offset_of_puzzlePieces2by3_12(),
	GameEightManager_t903757396::get_offset_of_puzzlePieces2by2_13(),
	GameEightManager_t903757396::get_offset_of_backgrounds2by2_14(),
	GameEightManager_t903757396::get_offset_of_backgrounds2by3_15(),
	GameEightManager_t903757396::get_offset_of_backgrounds3by3_16(),
	GameEightManager_t903757396::get_offset_of_startingSlots_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (U3CY2by2U3Ec__Iterator0_t520614058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[5] = 
{
	U3CY2by2U3Ec__Iterator0_t520614058::get_offset_of_U3CtempU3E__0_0(),
	U3CY2by2U3Ec__Iterator0_t520614058::get_offset_of_U24this_1(),
	U3CY2by2U3Ec__Iterator0_t520614058::get_offset_of_U24current_2(),
	U3CY2by2U3Ec__Iterator0_t520614058::get_offset_of_U24disposing_3(),
	U3CY2by2U3Ec__Iterator0_t520614058::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (U3CY2by3U3Ec__Iterator1_t2076692560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[5] = 
{
	U3CY2by3U3Ec__Iterator1_t2076692560::get_offset_of_U3CtempU3E__0_0(),
	U3CY2by3U3Ec__Iterator1_t2076692560::get_offset_of_U24this_1(),
	U3CY2by3U3Ec__Iterator1_t2076692560::get_offset_of_U24current_2(),
	U3CY2by3U3Ec__Iterator1_t2076692560::get_offset_of_U24disposing_3(),
	U3CY2by3U3Ec__Iterator1_t2076692560::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (U3CY3by3U3Ec__Iterator2_t3800956998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[5] = 
{
	U3CY3by3U3Ec__Iterator2_t3800956998::get_offset_of_U3CtempU3E__0_0(),
	U3CY3by3U3Ec__Iterator2_t3800956998::get_offset_of_U24this_1(),
	U3CY3by3U3Ec__Iterator2_t3800956998::get_offset_of_U24current_2(),
	U3CY3by3U3Ec__Iterator2_t3800956998::get_offset_of_U24disposing_3(),
	U3CY3by3U3Ec__Iterator2_t3800956998::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (U3CMoveToSlotsU3Ec__Iterator3_t2359960924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[7] = 
{
	U3CMoveToSlotsU3Ec__Iterator3_t2359960924::get_offset_of_pieces_0(),
	U3CMoveToSlotsU3Ec__Iterator3_t2359960924::get_offset_of_U24locvar0_1(),
	U3CMoveToSlotsU3Ec__Iterator3_t2359960924::get_offset_of_U24locvar1_2(),
	U3CMoveToSlotsU3Ec__Iterator3_t2359960924::get_offset_of_U24this_3(),
	U3CMoveToSlotsU3Ec__Iterator3_t2359960924::get_offset_of_U24current_4(),
	U3CMoveToSlotsU3Ec__Iterator3_t2359960924::get_offset_of_U24disposing_5(),
	U3CMoveToSlotsU3Ec__Iterator3_t2359960924::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (PuzzlePiece_t1912707670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[5] = 
{
	PuzzlePiece_t1912707670::get_offset_of_startSlotPosition_2(),
	PuzzlePiece_t1912707670::get_offset_of_isDragging_3(),
	PuzzlePiece_t1912707670::get_offset_of_beenDropped_4(),
	PuzzlePiece_t1912707670::get_offset_of_col_5(),
	PuzzlePiece_t1912707670::get_offset_of_overlapCollider_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (U3CYStartDraggingU3Ec__Iterator0_t4263427530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[5] = 
{
	U3CYStartDraggingU3Ec__Iterator0_t4263427530::get_offset_of_U3CtargetPosU3E__1_0(),
	U3CYStartDraggingU3Ec__Iterator0_t4263427530::get_offset_of_U24this_1(),
	U3CYStartDraggingU3Ec__Iterator0_t4263427530::get_offset_of_U24current_2(),
	U3CYStartDraggingU3Ec__Iterator0_t4263427530::get_offset_of_U24disposing_3(),
	U3CYStartDraggingU3Ec__Iterator0_t4263427530::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (AvatarCreator_t3048088315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[3] = 
{
	AvatarCreator_t3048088315::get_offset_of_faceCollider_2(),
	AvatarCreator_t3048088315::get_offset_of_eyeCollider_3(),
	AvatarCreator_t3048088315::get_offset_of_hairCollider_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (GameNineManager_t3632946731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[21] = 
{
	GameNineManager_t3632946731::get_offset_of_isPlaying_4(),
	GameNineManager_t3632946731::get_offset_of_faceIndex_5(),
	GameNineManager_t3632946731::get_offset_of_neckIndex_6(),
	GameNineManager_t3632946731::get_offset_of_eyeIndex_7(),
	GameNineManager_t3632946731::get_offset_of_hairIndex_8(),
	GameNineManager_t3632946731::get_offset_of_songNoteSpawnInterval_9(),
	GameNineManager_t3632946731::get_offset_of_elfShrinkTime_10(),
	GameNineManager_t3632946731::get_offset_of_elfTargetScale_11(),
	GameNineManager_t3632946731::get_offset_of_elfTargetPosition_12(),
	GameNineManager_t3632946731::get_offset_of_songBubble_13(),
	GameNineManager_t3632946731::get_offset_of_elf_14(),
	GameNineManager_t3632946731::get_offset_of_animationNames_15(),
	GameNineManager_t3632946731::get_offset_of_faces_16(),
	GameNineManager_t3632946731::get_offset_of_necks_17(),
	GameNineManager_t3632946731::get_offset_of_eyes_18(),
	GameNineManager_t3632946731::get_offset_of_hairs_19(),
	GameNineManager_t3632946731::get_offset_of_face_20(),
	GameNineManager_t3632946731::get_offset_of_neck_21(),
	GameNineManager_t3632946731::get_offset_of_eye_22(),
	GameNineManager_t3632946731::get_offset_of_hair_23(),
	GameNineManager_t3632946731::get_offset_of_elfAnimator_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (U3CYShrinkElfU3Ec__Iterator0_t3480698838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[4] = 
{
	U3CYShrinkElfU3Ec__Iterator0_t3480698838::get_offset_of_U24this_0(),
	U3CYShrinkElfU3Ec__Iterator0_t3480698838::get_offset_of_U24current_1(),
	U3CYShrinkElfU3Ec__Iterator0_t3480698838::get_offset_of_U24disposing_2(),
	U3CYShrinkElfU3Ec__Iterator0_t3480698838::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (U3CYDanceSequenceU3Ec__Iterator1_t833372137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[6] = 
{
	U3CYDanceSequenceU3Ec__Iterator1_t833372137::get_offset_of_U3CtimerU3E__0_0(),
	U3CYDanceSequenceU3Ec__Iterator1_t833372137::get_offset_of_U3Ctimer2U3E__0_1(),
	U3CYDanceSequenceU3Ec__Iterator1_t833372137::get_offset_of_U24this_2(),
	U3CYDanceSequenceU3Ec__Iterator1_t833372137::get_offset_of_U24current_3(),
	U3CYDanceSequenceU3Ec__Iterator1_t833372137::get_offset_of_U24disposing_4(),
	U3CYDanceSequenceU3Ec__Iterator1_t833372137::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (SongBubble_t2997910009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[3] = 
{
	SongBubble_t2997910009::get_offset_of_lifetime_2(),
	SongBubble_t2997910009::get_offset_of_particle_3(),
	SongBubble_t2997910009::get_offset_of_col_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (UIButtonUpdate_t766659849), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (AnimateHouse_t300882193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (U3CStartU3Ec__Iterator0_t1857383417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[4] = 
{
	U3CStartU3Ec__Iterator0_t1857383417::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1857383417::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1857383417::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1857383417::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (BGMusic_t3486147098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[4] = 
{
	BGMusic_t3486147098::get_offset_of_backgroundSongs_2(),
	BGMusic_t3486147098::get_offset_of_fadeTime_3(),
	BGMusic_t3486147098::get_offset_of_songsCounter_4(),
	BGMusic_t3486147098::get_offset_of_audioSource_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (U3CStartU3Ec__Iterator0_t1713471636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[4] = 
{
	U3CStartU3Ec__Iterator0_t1713471636::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1713471636::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1713471636::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1713471636::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (U3CYSongPlayerU3Ec__Iterator1_t3007034830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[4] = 
{
	U3CYSongPlayerU3Ec__Iterator1_t3007034830::get_offset_of_U24this_0(),
	U3CYSongPlayerU3Ec__Iterator1_t3007034830::get_offset_of_U24current_1(),
	U3CYSongPlayerU3Ec__Iterator1_t3007034830::get_offset_of_U24disposing_2(),
	U3CYSongPlayerU3Ec__Iterator1_t3007034830::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (U3CYFadeInU3Ec__Iterator2_t1740319868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[5] = 
{
	U3CYFadeInU3Ec__Iterator2_t1740319868::get_offset_of_song_0(),
	U3CYFadeInU3Ec__Iterator2_t1740319868::get_offset_of_U3CtimerU3E__0_1(),
	U3CYFadeInU3Ec__Iterator2_t1740319868::get_offset_of_U24current_2(),
	U3CYFadeInU3Ec__Iterator2_t1740319868::get_offset_of_U24disposing_3(),
	U3CYFadeInU3Ec__Iterator2_t1740319868::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (U3CYFadeOutU3Ec__Iterator3_t1176020762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[5] = 
{
	U3CYFadeOutU3Ec__Iterator3_t1176020762::get_offset_of_U3CtimerU3E__0_0(),
	U3CYFadeOutU3Ec__Iterator3_t1176020762::get_offset_of_song_1(),
	U3CYFadeOutU3Ec__Iterator3_t1176020762::get_offset_of_U24current_2(),
	U3CYFadeOutU3Ec__Iterator3_t1176020762::get_offset_of_U24disposing_3(),
	U3CYFadeOutU3Ec__Iterator3_t1176020762::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (DOTweenAnimation_t858634588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[34] = 
{
	DOTweenAnimation_t858634588::get_offset_of_delay_19(),
	DOTweenAnimation_t858634588::get_offset_of_duration_20(),
	DOTweenAnimation_t858634588::get_offset_of_easeType_21(),
	DOTweenAnimation_t858634588::get_offset_of_easeCurve_22(),
	DOTweenAnimation_t858634588::get_offset_of_loopType_23(),
	DOTweenAnimation_t858634588::get_offset_of_loops_24(),
	DOTweenAnimation_t858634588::get_offset_of_id_25(),
	DOTweenAnimation_t858634588::get_offset_of_isRelative_26(),
	DOTweenAnimation_t858634588::get_offset_of_isFrom_27(),
	DOTweenAnimation_t858634588::get_offset_of_isIndependentUpdate_28(),
	DOTweenAnimation_t858634588::get_offset_of_autoKill_29(),
	DOTweenAnimation_t858634588::get_offset_of_isActive_30(),
	DOTweenAnimation_t858634588::get_offset_of_isValid_31(),
	DOTweenAnimation_t858634588::get_offset_of_target_32(),
	DOTweenAnimation_t858634588::get_offset_of_animationType_33(),
	DOTweenAnimation_t858634588::get_offset_of_targetType_34(),
	DOTweenAnimation_t858634588::get_offset_of_forcedTargetType_35(),
	DOTweenAnimation_t858634588::get_offset_of_autoPlay_36(),
	DOTweenAnimation_t858634588::get_offset_of_useTargetAsV3_37(),
	DOTweenAnimation_t858634588::get_offset_of_endValueFloat_38(),
	DOTweenAnimation_t858634588::get_offset_of_endValueV3_39(),
	DOTweenAnimation_t858634588::get_offset_of_endValueV2_40(),
	DOTweenAnimation_t858634588::get_offset_of_endValueColor_41(),
	DOTweenAnimation_t858634588::get_offset_of_endValueString_42(),
	DOTweenAnimation_t858634588::get_offset_of_endValueRect_43(),
	DOTweenAnimation_t858634588::get_offset_of_endValueTransform_44(),
	DOTweenAnimation_t858634588::get_offset_of_optionalBool0_45(),
	DOTweenAnimation_t858634588::get_offset_of_optionalFloat0_46(),
	DOTweenAnimation_t858634588::get_offset_of_optionalInt0_47(),
	DOTweenAnimation_t858634588::get_offset_of_optionalRotationMode_48(),
	DOTweenAnimation_t858634588::get_offset_of_optionalScrambleMode_49(),
	DOTweenAnimation_t858634588::get_offset_of_optionalString_50(),
	DOTweenAnimation_t858634588::get_offset_of__tweenCreated_51(),
	DOTweenAnimation_t858634588::get_offset_of__playCount_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (DOTweenAnimationExtensions_t4140352346), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (DestroyParticle_t3576907776), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (U3CYDestroyU3Ec__Iterator0_t1854615267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[5] = 
{
	U3CYDestroyU3Ec__Iterator0_t1854615267::get_offset_of_U24this_0(),
	U3CYDestroyU3Ec__Iterator0_t1854615267::get_offset_of_U24current_1(),
	U3CYDestroyU3Ec__Iterator0_t1854615267::get_offset_of_U24disposing_2(),
	U3CYDestroyU3Ec__Iterator0_t1854615267::get_offset_of_U24PC_3(),
	U3CYDestroyU3Ec__Iterator0_t1854615267::get_offset_of_U24locvar0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (U3CYDestroyU3Ec__AnonStorey1_t674293687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[2] = 
{
	U3CYDestroyU3Ec__AnonStorey1_t674293687::get_offset_of_alive_0(),
	U3CYDestroyU3Ec__AnonStorey1_t674293687::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (DestroySound_t724042623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (ButtonPlaySoundOnStart_t3790818244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[3] = 
{
	ButtonPlaySoundOnStart_t3790818244::get_offset_of_sound_2(),
	ButtonPlaySoundOnStart_t3790818244::get_offset_of_myTime_3(),
	ButtonPlaySoundOnStart_t3790818244::get_offset_of_myColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (U3CPlayTheSoundU3Ec__Iterator0_t3748255788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[4] = 
{
	U3CPlayTheSoundU3Ec__Iterator0_t3748255788::get_offset_of_U24this_0(),
	U3CPlayTheSoundU3Ec__Iterator0_t3748255788::get_offset_of_U24current_1(),
	U3CPlayTheSoundU3Ec__Iterator0_t3748255788::get_offset_of_U24disposing_2(),
	U3CPlayTheSoundU3Ec__Iterator0_t3748255788::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (EndScreen_t1192627425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[8] = 
{
	EndScreen_t1192627425::get_offset_of_spwanPoints_2(),
	EndScreen_t1192627425::get_offset_of_star_3(),
	EndScreen_t1192627425::get_offset_of_menuBtns_4(),
	EndScreen_t1192627425::get_offset_of_starDestroyEffect_5(),
	EndScreen_t1192627425::get_offset_of_candies_6(),
	EndScreen_t1192627425::get_offset_of_tempStar_7(),
	EndScreen_t1192627425::get_offset_of_sound_8(),
	EndScreen_t1192627425::get_offset_of_occupied_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (EventManager_t2792515701), -1, sizeof(EventManager_t2792515701_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2137[3] = 
{
	EventManager_t2792515701_StaticFields::get_offset_of_OnMouseDown_0(),
	EventManager_t2792515701_StaticFields::get_offset_of_OnMouseUp_1(),
	EventManager_t2792515701_StaticFields::get_offset_of_OnWindowClicked_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (FlashingObject_t2247968655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[2] = 
{
	FlashingObject_t2247968655::get_offset_of__colors_2(),
	FlashingObject_t2247968655::get_offset_of_material_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (U3CFlashU3Ec__Iterator0_t2208059419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[9] = 
{
	U3CFlashU3Ec__Iterator0_t2208059419::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CFlashU3Ec__Iterator0_t2208059419::get_offset_of_U3CindexU3E__0_1(),
	U3CFlashU3Ec__Iterator0_t2208059419::get_offset_of_time_2(),
	U3CFlashU3Ec__Iterator0_t2208059419::get_offset_of_intervalTime_3(),
	U3CFlashU3Ec__Iterator0_t2208059419::get_offset_of_callbackFunction_4(),
	U3CFlashU3Ec__Iterator0_t2208059419::get_offset_of_U24this_5(),
	U3CFlashU3Ec__Iterator0_t2208059419::get_offset_of_U24current_6(),
	U3CFlashU3Ec__Iterator0_t2208059419::get_offset_of_U24disposing_7(),
	U3CFlashU3Ec__Iterator0_t2208059419::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (FlappyCharExample_t4200876794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[1] = 
{
	FlappyCharExample_t4200876794::get_offset_of_character_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (U3CdoStartU3Ec__Iterator0_t2987183723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[5] = 
{
	U3CdoStartU3Ec__Iterator0_t2987183723::get_offset_of_delay_0(),
	U3CdoStartU3Ec__Iterator0_t2987183723::get_offset_of_U24this_1(),
	U3CdoStartU3Ec__Iterator0_t2987183723::get_offset_of_U24current_2(),
	U3CdoStartU3Ec__Iterator0_t2987183723::get_offset_of_U24disposing_3(),
	U3CdoStartU3Ec__Iterator0_t2987183723::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (TapExceptionExample_t2422957020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (RocketCharExample_t1513856186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[1] = 
{
	RocketCharExample_t1513856186::get_offset_of_character_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (ArrayUtilsExample_t1078130992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[2] = 
{
	ArrayUtilsExample_t1078130992::get_offset_of_exampleArrayForPrintAndToString_2(),
	ArrayUtilsExample_t1078130992::get_offset_of_exampleListForPrintAndToString_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (GetOrAddExample_t841780186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (ReadOnlyAttributeExample_t603047684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[2] = 
{
	ReadOnlyAttributeExample_t603047684::get_offset_of_normalField_2(),
	ReadOnlyAttributeExample_t603047684::get_offset_of_readOnlyField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (TransformExtensionExample_t3506032567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[4] = 
{
	TransformExtensionExample_t3506032567::get_offset_of_goodLookAtChar_2(),
	TransformExtensionExample_t3506032567::get_offset_of_badLookAtChar_3(),
	0,
	TransformExtensionExample_t3506032567::get_offset_of_currentLogRate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (MandatoryDummyImplmplementation_t1329551270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (TwoDArrayExample_t2315501275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[1] = 
{
	TwoDArrayExample_t2315501275::get_offset_of_array_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (CryptoExample_t1829615961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[5] = 
{
	CryptoExample_t1829615961::get_offset_of_keyField_2(),
	CryptoExample_t1829615961::get_offset_of_sourceTextField_3(),
	CryptoExample_t1829615961::get_offset_of_encryptedTextField_4(),
	CryptoExample_t1829615961::get_offset_of_encryptBtn_5(),
	CryptoExample_t1829615961::get_offset_of_decryptBtn_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (HTTPLoaderExample_t3175293487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[3] = 
{
	HTTPLoaderExample_t3175293487::get_offset_of_urlField_2(),
	HTTPLoaderExample_t3175293487::get_offset_of_resultField_3(),
	HTTPLoaderExample_t3175293487::get_offset_of_loadBtn_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (JSONSaveFileExample_t3967010229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[1] = 
{
	JSONSaveFileExample_t3967010229::get_offset_of_saveFile_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (RedundantFileWriterExample_t3754595982), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (ColliderEventSystemExample_t224613953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[1] = 
{
	ColliderEventSystemExample_t224613953::get_offset_of_square_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (DummyMouseEventImplementation_t4020916073), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (GenericMouseEventExample_t856766798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[1] = 
{
	GenericMouseEventExample_t856766798::get_offset_of_square_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (KeyboardEventSystemExample_t362032968), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (ParticleSystemEventExample_t1578308911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[1] = 
{
	ParticleSystemEventExample_t1578308911::get_offset_of_pSystem_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (ArbitraryComponent_t2637609733), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (GridExamples_t561734721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[1] = 
{
	GridExamples_t561734721::get_offset_of_grid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (RandomizerExamples_t274280640), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (GetMidPontExample_t420734737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[2] = 
{
	GetMidPontExample_t420734737::get_offset_of_point1_2(),
	GetMidPontExample_t420734737::get_offset_of_point2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (LaunchObjectExample_t608304890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[4] = 
{
	LaunchObjectExample_t608304890::get_offset_of_cannon_2(),
	LaunchObjectExample_t608304890::get_offset_of_projectileTemplate_3(),
	LaunchObjectExample_t608304890::get_offset_of_canShoot_4(),
	LaunchObjectExample_t608304890::get_offset_of_projectile_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (U3CResetU3Ec__Iterator0_t4145257719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[4] = 
{
	U3CResetU3Ec__Iterator0_t4145257719::get_offset_of_U24this_0(),
	U3CResetU3Ec__Iterator0_t4145257719::get_offset_of_U24current_1(),
	U3CResetU3Ec__Iterator0_t4145257719::get_offset_of_U24disposing_2(),
	U3CResetU3Ec__Iterator0_t4145257719::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (LifeHUDExample_t2672890885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[1] = 
{
	LifeHUDExample_t2672890885::get_offset_of_lifes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (ObjectSequenceExample_t3751909220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	ObjectSequenceExample_t3751909220::get_offset_of_manualChange_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (ScalingBarExample_t50575284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[1] = 
{
	ScalingBarExample_t50575284::get_offset_of_progressBar_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (ArrowKeyMover_t1318051673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[19] = 
{
	ArrowKeyMover_t1318051673::get_offset_of_canMove_2(),
	ArrowKeyMover_t1318051673::get_offset_of_canMoveUp_3(),
	ArrowKeyMover_t1318051673::get_offset_of_canMoveDown_4(),
	ArrowKeyMover_t1318051673::get_offset_of_canMoveRight_5(),
	ArrowKeyMover_t1318051673::get_offset_of_canMoveLeft_6(),
	ArrowKeyMover_t1318051673::get_offset_of_horizontalSpeed_7(),
	ArrowKeyMover_t1318051673::get_offset_of_verticalSpeed_8(),
	ArrowKeyMover_t1318051673::get_offset_of_minX_9(),
	ArrowKeyMover_t1318051673::get_offset_of_maxX_10(),
	ArrowKeyMover_t1318051673::get_offset_of_minY_11(),
	ArrowKeyMover_t1318051673::get_offset_of_maxY_12(),
	ArrowKeyMover_t1318051673::get_offset_of_upArrow_13(),
	ArrowKeyMover_t1318051673::get_offset_of_downArrow_14(),
	ArrowKeyMover_t1318051673::get_offset_of_leftArrow_15(),
	ArrowKeyMover_t1318051673::get_offset_of_rightArrow_16(),
	ArrowKeyMover_t1318051673::get_offset_of_goLeft_17(),
	ArrowKeyMover_t1318051673::get_offset_of_goRight_18(),
	ArrowKeyMover_t1318051673::get_offset_of_goUp_19(),
	ArrowKeyMover_t1318051673::get_offset_of_goDown_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (FlappyChar_t3600499100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[5] = 
{
	FlappyChar_t3600499100::get_offset_of_hitExceptionScriptNames_2(),
	FlappyChar_t3600499100::get_offset_of_forceToAdd_3(),
	FlappyChar_t3600499100::get_offset_of_speed_4(),
	FlappyChar_t3600499100::get_offset_of_dead_5(),
	FlappyChar_t3600499100::get_offset_of_body_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (RocketChar_t1772935162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[13] = 
{
	RocketChar_t1772935162::get_offset_of_RocketCharHit_2(),
	RocketChar_t1772935162::get_offset_of_RocketCharTriggered_3(),
	RocketChar_t1772935162::get_offset_of_exceptionNames_4(),
	RocketChar_t1772935162::get_offset_of_canMove_5(),
	RocketChar_t1772935162::get_offset_of_minX_6(),
	RocketChar_t1772935162::get_offset_of_maxX_7(),
	RocketChar_t1772935162::get_offset_of_leftArrow_8(),
	RocketChar_t1772935162::get_offset_of_rightArrow_9(),
	RocketChar_t1772935162::get_offset_of_horizontalSpeed_10(),
	RocketChar_t1772935162::get_offset_of_jumpMaxForce_11(),
	RocketChar_t1772935162::get_offset_of_body_12(),
	RocketChar_t1772935162::get_offset_of_goLeft_13(),
	RocketChar_t1772935162::get_offset_of_goRight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (ArrayUtils_t1556298404), -1, sizeof(ArrayUtils_t1556298404_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2171[1] = 
{
	ArrayUtils_t1556298404_StaticFields::get_offset_of_lastOrder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (EDUCam_t524974491), -1, sizeof(EDUCam_t524974491_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2172[4] = 
{
	EDUCam_t524974491::get_offset_of_attachedCamera_2(),
	EDUCam_t524974491::get_offset_of_targetaspect_3(),
	EDUCam_t524974491::get_offset_of_levelLoadRequested_4(),
	EDUCam_t524974491_StaticFields::get_offset_of_Instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (GameObjectExtension_t2084883162), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (ReadOnlyAttribute_t3809417938), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (TransformExtension_t690161491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (Crypto_t569229097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (HTTPLoader_t2960633167), -1, sizeof(HTTPLoader_t2960633167_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2179[5] = 
{
	HTTPLoader_t2960633167_StaticFields::get_offset_of_forceNoCache_0(),
	HTTPLoader_t2960633167::get_offset_of_LoadComplete_1(),
	HTTPLoader_t2960633167::get_offset_of_formData_2(),
	HTTPLoader_t2960633167::get_offset_of_url_3(),
	HTTPLoader_t2960633167::get_offset_of_loader_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (HTTPResult_t3652583718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[3] = 
{
	HTTPResult_t3652583718::get_offset_of_content_0(),
	HTTPResult_t3652583718::get_offset_of_error_1(),
	HTTPResult_t3652583718::get_offset_of_bytes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (LoadCompleteListener_t2670511940), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (U3CloadU3Ec__Iterator0_t2369083150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[4] = 
{
	U3CloadU3Ec__Iterator0_t2369083150::get_offset_of_U24this_0(),
	U3CloadU3Ec__Iterator0_t2369083150::get_offset_of_U24current_1(),
	U3CloadU3Ec__Iterator0_t2369083150::get_offset_of_U24disposing_2(),
	U3CloadU3Ec__Iterator0_t2369083150::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (JSONSaveFile_t938573021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[7] = 
{
	JSONSaveFile_t938573021::get_offset_of_DEFAULT_VALUES_0(),
	JSONSaveFile_t938573021::get_offset_of_curSaveFile_1(),
	JSONSaveFile_t938573021::get_offset_of_newSaveFile_2(),
	JSONSaveFile_t938573021::get_offset_of_isProtected_3(),
	JSONSaveFile_t938573021::get_offset_of_encrypted_4(),
	JSONSaveFile_t938573021::get_offset_of_fName_5(),
	JSONSaveFile_t938573021::get_offset_of_encryptionKey_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (RedundantFileWriter_t3761749544), -1, sizeof(RedundantFileWriter_t3761749544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2184[3] = 
{
	RedundantFileWriter_t3761749544::get_offset_of_writes_2(),
	RedundantFileWriter_t3761749544_StaticFields::get_offset_of_instance_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (DelayedWriteReference_t933455898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[2] = 
{
	DelayedWriteReference_t933455898::get_offset_of_delay_0(),
	DelayedWriteReference_t933455898::get_offset_of_path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (U3CDelayWriteU3Ec__Iterator0_t4220271987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[10] = 
{
	U3CDelayWriteU3Ec__Iterator0_t4220271987::get_offset_of_path_0(),
	U3CDelayWriteU3Ec__Iterator0_t4220271987::get_offset_of_contents_1(),
	U3CDelayWriteU3Ec__Iterator0_t4220271987::get_offset_of_testJSON_2(),
	U3CDelayWriteU3Ec__Iterator0_t4220271987::get_offset_of_encrypted_3(),
	U3CDelayWriteU3Ec__Iterator0_t4220271987::get_offset_of_encryptionKey_4(),
	U3CDelayWriteU3Ec__Iterator0_t4220271987::get_offset_of_retryCount_5(),
	U3CDelayWriteU3Ec__Iterator0_t4220271987::get_offset_of_U24this_6(),
	U3CDelayWriteU3Ec__Iterator0_t4220271987::get_offset_of_U24current_7(),
	U3CDelayWriteU3Ec__Iterator0_t4220271987::get_offset_of_U24disposing_8(),
	U3CDelayWriteU3Ec__Iterator0_t4220271987::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (JSONBinaryTag_t3856753551)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2187[8] = 
{
	JSONBinaryTag_t3856753551::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (JSONNode_t1250409636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (U3CU3Ec__Iterator0_t803137928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[3] = 
{
	U3CU3Ec__Iterator0_t803137928::get_offset_of_U24current_0(),
	U3CU3Ec__Iterator0_t803137928::get_offset_of_U24disposing_1(),
	U3CU3Ec__Iterator0_t803137928::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (U3CU3Ec__Iterator1_t3532021283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[8] = 
{
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U3CCU3E__1_1(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U3CDU3E__2_3(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator1_t3532021283::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (JSONArray_t3986483147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[1] = 
{
	JSONArray_t3986483147::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (U3CU3Ec__Iterator0_t4017688111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[6] = 
{
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U3CNU3E__1_1(),
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t4017688111::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (U3CGetEnumeratorU3Ec__Iterator1_t1167240694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[6] = 
{
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U3CNU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U24this_2(),
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U24disposing_4(),
	U3CGetEnumeratorU3Ec__Iterator1_t1167240694::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (JSONClass_t1609506608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[1] = 
{
	JSONClass_t1609506608::get_offset_of_m_Dict_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (U3CRemoveU3Ec__AnonStorey2_t2562212806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[1] = 
{
	U3CRemoveU3Ec__AnonStorey2_t2562212806::get_offset_of_aNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (U3CU3Ec__Iterator0_t466495836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[6] = 
{
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U3CNU3E__1_1(),
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t466495836::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (U3CGetEnumeratorU3Ec__Iterator1_t201440115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[6] = 
{
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U3CNU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U24this_2(),
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U24disposing_4(),
	U3CGetEnumeratorU3Ec__Iterator1_t201440115::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (JSONData_t531041784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[1] = 
{
	JSONData_t531041784::get_offset_of_m_Data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (JSONLazyCreator_t3575446586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[2] = 
{
	JSONLazyCreator_t3575446586::get_offset_of_m_Node_0(),
	JSONLazyCreator_t3575446586::get_offset_of_m_Key_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
