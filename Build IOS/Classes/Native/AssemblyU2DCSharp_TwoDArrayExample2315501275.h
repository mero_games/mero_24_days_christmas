﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// MandatoryDummyImplmplementation[]
struct MandatoryDummyImplmplementationU5BU5D_t2697135875;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoDArrayExample
struct  TwoDArrayExample_t2315501275  : public MonoBehaviour_t1158329972
{
public:
	// MandatoryDummyImplmplementation[] TwoDArrayExample::array
	MandatoryDummyImplmplementationU5BU5D_t2697135875* ___array_2;

public:
	inline static int32_t get_offset_of_array_2() { return static_cast<int32_t>(offsetof(TwoDArrayExample_t2315501275, ___array_2)); }
	inline MandatoryDummyImplmplementationU5BU5D_t2697135875* get_array_2() const { return ___array_2; }
	inline MandatoryDummyImplmplementationU5BU5D_t2697135875** get_address_of_array_2() { return &___array_2; }
	inline void set_array_2(MandatoryDummyImplmplementationU5BU5D_t2697135875* value)
	{
		___array_2 = value;
		Il2CppCodeGenWriteBarrier(&___array_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
