﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransformGrid
struct  TransformGrid_t2312775536  : public Il2CppObject
{
public:
	// UnityEngine.Transform[] TransformGrid::columns
	TransformU5BU5D_t3764228911* ___columns_0;

public:
	inline static int32_t get_offset_of_columns_0() { return static_cast<int32_t>(offsetof(TransformGrid_t2312775536, ___columns_0)); }
	inline TransformU5BU5D_t3764228911* get_columns_0() const { return ___columns_0; }
	inline TransformU5BU5D_t3764228911** get_address_of_columns_0() { return &___columns_0; }
	inline void set_columns_0(TransformU5BU5D_t3764228911* value)
	{
		___columns_0 = value;
		Il2CppCodeGenWriteBarrier(&___columns_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
