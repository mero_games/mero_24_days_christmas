﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundsHolder
struct  BackgroundsHolder_t4153806707  : public Il2CppObject
{
public:
	// UnityEngine.Sprite BackgroundsHolder::wholeBackground
	Sprite_t309593783 * ___wholeBackground_0;
	// UnityEngine.Sprite[] BackgroundsHolder::slicedPieces
	SpriteU5BU5D_t3359083662* ___slicedPieces_1;

public:
	inline static int32_t get_offset_of_wholeBackground_0() { return static_cast<int32_t>(offsetof(BackgroundsHolder_t4153806707, ___wholeBackground_0)); }
	inline Sprite_t309593783 * get_wholeBackground_0() const { return ___wholeBackground_0; }
	inline Sprite_t309593783 ** get_address_of_wholeBackground_0() { return &___wholeBackground_0; }
	inline void set_wholeBackground_0(Sprite_t309593783 * value)
	{
		___wholeBackground_0 = value;
		Il2CppCodeGenWriteBarrier(&___wholeBackground_0, value);
	}

	inline static int32_t get_offset_of_slicedPieces_1() { return static_cast<int32_t>(offsetof(BackgroundsHolder_t4153806707, ___slicedPieces_1)); }
	inline SpriteU5BU5D_t3359083662* get_slicedPieces_1() const { return ___slicedPieces_1; }
	inline SpriteU5BU5D_t3359083662** get_address_of_slicedPieces_1() { return &___slicedPieces_1; }
	inline void set_slicedPieces_1(SpriteU5BU5D_t3359083662* value)
	{
		___slicedPieces_1 = value;
		Il2CppCodeGenWriteBarrier(&___slicedPieces_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
