﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveSnowflake
struct  MoveSnowflake_t721823583  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Collider2D MoveSnowflake::col
	Collider2D_t646061738 * ___col_2;
	// UnityEngine.GameObject MoveSnowflake::thisLetter
	GameObject_t1756533147 * ___thisLetter_3;
	// UnityEngine.Vector3 MoveSnowflake::originalPos
	Vector3_t2243707580  ___originalPos_4;
	// System.Int32 MoveSnowflake::xScaler
	int32_t ___xScaler_5;

public:
	inline static int32_t get_offset_of_col_2() { return static_cast<int32_t>(offsetof(MoveSnowflake_t721823583, ___col_2)); }
	inline Collider2D_t646061738 * get_col_2() const { return ___col_2; }
	inline Collider2D_t646061738 ** get_address_of_col_2() { return &___col_2; }
	inline void set_col_2(Collider2D_t646061738 * value)
	{
		___col_2 = value;
		Il2CppCodeGenWriteBarrier(&___col_2, value);
	}

	inline static int32_t get_offset_of_thisLetter_3() { return static_cast<int32_t>(offsetof(MoveSnowflake_t721823583, ___thisLetter_3)); }
	inline GameObject_t1756533147 * get_thisLetter_3() const { return ___thisLetter_3; }
	inline GameObject_t1756533147 ** get_address_of_thisLetter_3() { return &___thisLetter_3; }
	inline void set_thisLetter_3(GameObject_t1756533147 * value)
	{
		___thisLetter_3 = value;
		Il2CppCodeGenWriteBarrier(&___thisLetter_3, value);
	}

	inline static int32_t get_offset_of_originalPos_4() { return static_cast<int32_t>(offsetof(MoveSnowflake_t721823583, ___originalPos_4)); }
	inline Vector3_t2243707580  get_originalPos_4() const { return ___originalPos_4; }
	inline Vector3_t2243707580 * get_address_of_originalPos_4() { return &___originalPos_4; }
	inline void set_originalPos_4(Vector3_t2243707580  value)
	{
		___originalPos_4 = value;
	}

	inline static int32_t get_offset_of_xScaler_5() { return static_cast<int32_t>(offsetof(MoveSnowflake_t721823583, ___xScaler_5)); }
	inline int32_t get_xScaler_5() const { return ___xScaler_5; }
	inline int32_t* get_address_of_xScaler_5() { return &___xScaler_5; }
	inline void set_xScaler_5(int32_t value)
	{
		___xScaler_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
