﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEighteenManager/GameRounds
struct  GameRounds_t1482069856 
{
public:
	// System.Int32 GameEighteenManager/GameRounds::colors
	int32_t ___colors_0;
	// System.Int32 GameEighteenManager/GameRounds::length
	int32_t ___length_1;
	// System.Single GameEighteenManager/GameRounds::speed
	float ___speed_2;

public:
	inline static int32_t get_offset_of_colors_0() { return static_cast<int32_t>(offsetof(GameRounds_t1482069856, ___colors_0)); }
	inline int32_t get_colors_0() const { return ___colors_0; }
	inline int32_t* get_address_of_colors_0() { return &___colors_0; }
	inline void set_colors_0(int32_t value)
	{
		___colors_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(GameRounds_t1482069856, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(GameRounds_t1482069856, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
