﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchObjectExample
struct  LaunchObjectExample_t608304890  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform LaunchObjectExample::cannon
	Transform_t3275118058 * ___cannon_2;
	// UnityEngine.GameObject LaunchObjectExample::projectileTemplate
	GameObject_t1756533147 * ___projectileTemplate_3;
	// System.Boolean LaunchObjectExample::canShoot
	bool ___canShoot_4;
	// UnityEngine.GameObject LaunchObjectExample::projectile
	GameObject_t1756533147 * ___projectile_5;

public:
	inline static int32_t get_offset_of_cannon_2() { return static_cast<int32_t>(offsetof(LaunchObjectExample_t608304890, ___cannon_2)); }
	inline Transform_t3275118058 * get_cannon_2() const { return ___cannon_2; }
	inline Transform_t3275118058 ** get_address_of_cannon_2() { return &___cannon_2; }
	inline void set_cannon_2(Transform_t3275118058 * value)
	{
		___cannon_2 = value;
		Il2CppCodeGenWriteBarrier(&___cannon_2, value);
	}

	inline static int32_t get_offset_of_projectileTemplate_3() { return static_cast<int32_t>(offsetof(LaunchObjectExample_t608304890, ___projectileTemplate_3)); }
	inline GameObject_t1756533147 * get_projectileTemplate_3() const { return ___projectileTemplate_3; }
	inline GameObject_t1756533147 ** get_address_of_projectileTemplate_3() { return &___projectileTemplate_3; }
	inline void set_projectileTemplate_3(GameObject_t1756533147 * value)
	{
		___projectileTemplate_3 = value;
		Il2CppCodeGenWriteBarrier(&___projectileTemplate_3, value);
	}

	inline static int32_t get_offset_of_canShoot_4() { return static_cast<int32_t>(offsetof(LaunchObjectExample_t608304890, ___canShoot_4)); }
	inline bool get_canShoot_4() const { return ___canShoot_4; }
	inline bool* get_address_of_canShoot_4() { return &___canShoot_4; }
	inline void set_canShoot_4(bool value)
	{
		___canShoot_4 = value;
	}

	inline static int32_t get_offset_of_projectile_5() { return static_cast<int32_t>(offsetof(LaunchObjectExample_t608304890, ___projectile_5)); }
	inline GameObject_t1756533147 * get_projectile_5() const { return ___projectile_5; }
	inline GameObject_t1756533147 ** get_address_of_projectile_5() { return &___projectile_5; }
	inline void set_projectile_5(GameObject_t1756533147 * value)
	{
		___projectile_5 = value;
		Il2CppCodeGenWriteBarrier(&___projectile_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
