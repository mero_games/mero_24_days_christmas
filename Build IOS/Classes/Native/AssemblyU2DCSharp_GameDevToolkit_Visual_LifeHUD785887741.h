﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t1098056643;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDevToolkit.Visual.LifeHUD
struct  LifeHUD_t785887741  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.SpriteRenderer[] GameDevToolkit.Visual.LifeHUD::hearts
	SpriteRendererU5BU5D_t1098056643* ___hearts_2;
	// System.Int32 GameDevToolkit.Visual.LifeHUD::currentLifes
	int32_t ___currentLifes_3;

public:
	inline static int32_t get_offset_of_hearts_2() { return static_cast<int32_t>(offsetof(LifeHUD_t785887741, ___hearts_2)); }
	inline SpriteRendererU5BU5D_t1098056643* get_hearts_2() const { return ___hearts_2; }
	inline SpriteRendererU5BU5D_t1098056643** get_address_of_hearts_2() { return &___hearts_2; }
	inline void set_hearts_2(SpriteRendererU5BU5D_t1098056643* value)
	{
		___hearts_2 = value;
		Il2CppCodeGenWriteBarrier(&___hearts_2, value);
	}

	inline static int32_t get_offset_of_currentLifes_3() { return static_cast<int32_t>(offsetof(LifeHUD_t785887741, ___currentLifes_3)); }
	inline int32_t get_currentLifes_3() const { return ___currentLifes_3; }
	inline int32_t* get_address_of_currentLifes_3() { return &___currentLifes_3; }
	inline void set_currentLifes_3(int32_t value)
	{
		___currentLifes_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
