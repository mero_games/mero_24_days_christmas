/*
 * Developer:  Barabas Tamas
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    /// <summary>
    /// Minutes to play.
    /// </summary>
    public float minutesToPlay = 3;

    private float timeToPlay;
    private float timer = 0;

    private void Awake()
    {
        timeToPlay = minutesToPlay * 60;
        timer = 0;
    }

    void Update()
    {
        if (Game24Manager.Instance.IsPlaying)
        {
            timer += Time.deltaTime;
            if (timer > timeToPlay)
            {
                Game24Manager.Instance.ShowGameOverScreen();
            }
        }
    }

}