/*
 * Developer:  Barabas Tamas
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftManager : MonoBehaviour
{
    public Transform spawnPostion;
    public GameObject yeyPrefab;
    public List<Gift> gifts;

    public bool canGiveGift = true;

    private int currentIndex = 0;

    private void Start()
    {
        gifts.Shuffle();
    }

    public void Spawn(Transform position)
    {
        Gift gift = gifts[currentIndex];
        RepositionInList(gift);

        StartCoroutine(Thrown(gift, position));
    }

    IEnumerator Thrown(Gift gift, Transform position)
    {
        yield return new WaitForSeconds(Mathf.Abs(Game24Manager.Instance.obstacleSpeed.x * 0.6f));
        if (!canGiveGift)
        {
            yield break;
        }
        GameObject toSpawn = ObjectPool.Spawn(
            gift.gameObject,
            spawnPostion
        );
        toSpawn.GetComponent<Gift>().yeyPosition = position;

        yield return new WaitForSeconds(0.2f);
        GameObject yey = ObjectPool.Spawn(
            Game24Manager.Instance.giftManager.yeyPrefab,
            position.parent,
            new Vector3(0.7f, 5.2f, 0.0f),
            Quaternion.identity
            );
        yey.GetComponent<Yey>().Scale();
        yield return new WaitForSeconds(5);
        toSpawn.Recycle();
    }

    void RepositionInList(Gift gift)
    {
        gifts.Remove(gift);
        gifts.Insert(gifts.Count, gift);
    }
}