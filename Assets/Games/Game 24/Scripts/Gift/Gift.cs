/*
 * Developer:  Barabas Tamas
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Gift : MonoBehaviour
{
    [Range(0, 500)]
    public float force = 200f;
    public Transform yeyPosition;

    private Rigidbody2D rb;

    public Rigidbody2D Rb
    {
        get
        {
            if (rb == null)
                rb = GetComponent<Rigidbody2D>();
            return rb;
        }
        set
        {
            rb = value;
        }
    }

    private void OnEnable()
    {
        if (Game24Manager.Instance.giftManager.canGiveGift)
        {
            Throw();
        }
        else
        {
            Game24Manager.Instance.giftManager.gifts.ForEach((prefab) => prefab.Recycle());
        }
    }

    public void Throw()
    {
        if (SoundManager.Instance != null) { SoundManager.Instance.Play(SoundManager.Instance.giftSpawnSound.sound, transform, transform.position); }
        Rb.velocity = Vector2.zero;
        Rb.AddForce(new Vector2(0, force));
    }
}