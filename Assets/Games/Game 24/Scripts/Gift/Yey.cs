/*
 * Developer:  Barabas Tamas
 */

using UnityEngine;
using DG.Tweening;

public class Yey : MonoBehaviour
{

    public void Scale()
    {
        foreach (Transform t in transform)
        {
            Tweener tween = t.DOScale(Vector3.one * 1.2f, 0.1f);
            tween.OnComplete(() =>
            {
                t.DOScale(Vector3.one, 0.2f);
            });
        }
    }

}