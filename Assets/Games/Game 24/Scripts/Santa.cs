/*
 * Developer:  Barabas Tamas
 */

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Santa : MonoBehaviour
{
    [Header("Tap")]
    [Range(0, 500)]
    public float force = 200;
    public float tapDelay = 0.2f;

    [Header("Particle")]
    public ParticleSystem ps;
    public Transform particlePosition;


    private Rigidbody2D rb;
    private FlashingObject flash;
    private bool wasHit;
    private bool isAtTop = false;
    private Rect cameraRect;
    private Bounds santaBounds;
    private float lastTapTime;


    private void Awake()
    {
        santaBounds = GetComponentInChildren<SpriteRenderer>().bounds;
    }

    void Start()
    {
        lastTapTime = Time.time;

        rb = GetComponent<Rigidbody2D>();
        flash = GetComponent<FlashingObject>();

        EventManager.OnMouseDown += EventManager_OnMouseDown;

        var bottomLeft = Game24Manager.Instance.MainCamera.ScreenToWorldPoint(Vector3.zero);
        var topRight = Game24Manager.Instance.MainCamera.ScreenToWorldPoint(new Vector3(
            Game24Manager.Instance.MainCamera.pixelWidth, Game24Manager.Instance.MainCamera.pixelHeight));

        cameraRect = new Rect(
            bottomLeft.x,
            bottomLeft.y,
            topRight.x - bottomLeft.x,
            topRight.y - bottomLeft.y);

    }

    void OnDestroy()
    {
        EventManager.OnMouseDown -= EventManager_OnMouseDown;
    }

    void EventManager_OnMouseDown()
    {
        if (Game24Manager.Instance.IsPlaying && !isAtTop && Time.time >= lastTapTime)
        {
            lastTapTime = Time.time + tapDelay;
            rb.velocity = Vector2.zero;
            rb.AddForce(Vector2.up * force);

            if (SoundManager.Instance != null) { SoundManager.Instance.Play(SoundManager.Instance.tapSound.sound, transform, transform.position); }

            ParticleSystem toSpawn = ObjectPool.Spawn(
                ps,
                transform,
                particlePosition.localPosition,
                Quaternion.Euler(-180, 90, 0)
            );
            toSpawn.Play();
            StartCoroutine(RecylceParticle(toSpawn));
        }
    }

    void Update()
    {
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, cameraRect.xMin, cameraRect.xMax),
            Mathf.Clamp(transform.position.y, cameraRect.yMin, cameraRect.yMax - santaBounds.size.y / 2),
            transform.position.z
        );

        isAtTop = transform.position.y >= ((cameraRect.yMax - GetComponentInChildren<SpriteRenderer>().bounds.size.y / 2) - 0.5f);
    }

    IEnumerator RecylceParticle(ParticleSystem obj)
    {
        yield return new WaitForSeconds(ps.main.startLifetime.constantMax);
        obj.Recycle();
    }

    void HitSomething()
    {
        if (!wasHit)
        {
            wasHit = true;
            if (SoundManager.Instance != null) { SoundManager.Instance.Play(SoundManager.Instance.hitSound.sound, transform, transform.position); }
            Game24Manager.Instance.giftManager.canGiveGift = false;
            flash.StartFlashing(0.1f, 0.25f, () =>
            {
                Game24Manager.Instance.giftManager.canGiveGift = true;
                wasHit = false;
                return true;
            });

            iTween.ShakePosition(gameObject, Vector3.one * 0.1f, 0.2f);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<ObstacleBase>() != null)
        {
            HitSomething();
        }
        else if (collision.GetComponent<GiftSpawnPoint>() != null && Game24Manager.Instance.giftManager.canGiveGift)
        {
            Game24Manager.Instance.SpawnGift(collision.GetComponent<GiftSpawnPoint>().transform);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            HitSomething();
        }
    }

}