/*
 * Developer:  Barabas Tamas
 */

using UnityEngine;

public class Game24Manager : SingletonBase<Game24Manager>
{
    public Santa santa;
    public GiftManager giftManager;
    public Vector2 backgroundSpeed = new Vector2(-2.3f, 0.0f);
    public Vector2 obstacleSpeed = new Vector2(-2.7f, 0.0f);

    private bool isPlaying;

    public bool IsPlaying
    {
        get
        {
            return isPlaying;
        }
        set
        {
            isPlaying = value;
        }
    }

    public void Start()
    {
        isPlaying = true;
    }

    public void SpawnGift(Transform position)
    {
        giftManager.Spawn(position);
    }

    public void ShowGameOverScreen()
    {
        IsPlaying = false;
        Debug.Log("End");
        if (NavigationManager.Instance != null)
        {
            NavigationManager.Instance.InstantiateEndScreen(transform);
        }
    }
}