/*
 * Developer:  Barabas Tamas
 */

using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class RepeatingBackground : MonoBehaviour
{
    private BoxCollider2D boxCollider;
    private float backgroundLenght;

    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        backgroundLenght = boxCollider.size.x;
    }

    void Update()
    {
        if (transform.position.x < -backgroundLenght)
        {
            Reposition();
        }
    }

    /// <summary>
    /// Reposition background.
    /// </summary>
    private void Reposition()
    {
        Vector2 offset = new Vector2(backgroundLenght * 2.0f, 0);
        transform.position = (Vector2)transform.position + offset;
    }
}