/*
 * Developer:  Barabas Tamas
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ObjectScroller : MonoBehaviour
{
    private Rigidbody2D rb;
    public Vector2 desiredVelocity;

    private bool isScrolling = false;

    private void OnEnable()
    {
        rb.velocity = Vector2.zero;

        if (GetComponent<ObstacleBase>() != null)
        {
            desiredVelocity = Game24Manager.Instance.obstacleSpeed;
            if (GetComponent<Bird>() != null)
            {
                desiredVelocity *= 1.2f;
            }
        }
        else if (GetComponent<RepeatingBackground>() != null)
        {
            desiredVelocity = Game24Manager.Instance.backgroundSpeed;
        }

        StartScroll();
    }

    private void OnDisable()
    {
        if (GetComponent<ObstacleBase>() != null)
        {
            StopScroll();
        }
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.isKinematic = true;
    }

    void Update()
    {
        if (!Game24Manager.Instance.IsPlaying)
        {
            StopScroll(true);
        }
    }

    /// <summary>
    /// Start scrolling the object
    ///
    public void StartScroll()
    {
        if (!isScrolling)
        {
            isScrolling = true;
            rb.velocity = desiredVelocity;
        }
    }

    /// <summary>
    /// Stop scrolling the object
    ///
    public void StopScroll(bool end = false)
    {
        if (end)
        {
            StartCoroutine(YStopScroll());
            return;
        }

        if (isScrolling)
        {
            isScrolling = false;
            rb.velocity = Vector2.zero;
        }
    }

    IEnumerator YStopScroll()
    {
        while (rb.velocity != Vector2.zero)
        {
            float velocityX = Mathf.SmoothStep(rb.velocity.x, 0, Time.deltaTime);
            rb.velocity = new Vector2(velocityX, 0);
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }
}