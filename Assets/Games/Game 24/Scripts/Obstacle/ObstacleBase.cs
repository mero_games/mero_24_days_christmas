/*
 * Developer:  Barabas Tamas
 */

using UnityEngine;

[RequireComponent(typeof(ObjectScroller))]
public class ObstacleBase : MonoBehaviour
{
    public float minY;
    public float maxY;
}