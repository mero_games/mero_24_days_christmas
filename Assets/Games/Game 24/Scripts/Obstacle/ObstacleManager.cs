/*
 * Developer:  Barabas Tamas
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    public float positionX = 6.0f;
    public float spawnTime = 5.0f;

    public List<ObstacleBase> obstacles;

    private int currentIndex = 0;
    private float lastTimeSpawned = 0;

    void Start()
    {
        obstacles.Shuffle();
    }

    void Update()
    {
        if (Game24Manager.Instance.IsPlaying)
        {
            lastTimeSpawned += Time.deltaTime;
            if (lastTimeSpawned > spawnTime)
            {
                lastTimeSpawned = 0;
                spawnTime = Random.Range(3, 5);
                Spawn();
            }
        }
    }

    void Spawn()
    {
        ObstacleBase obstacle = obstacles[currentIndex];
        RepositionInList(obstacle);

        float positionY = Random.Range(obstacle.minY, obstacle.maxY);

        GameObject toSpawn = ObjectPool.Spawn(
            obstacle.gameObject,
            new Vector3(positionX, positionY, 0)
        );

        StartCoroutine(RecycleSpawnedObstacle(toSpawn));
    }

    IEnumerator RecycleSpawnedObstacle(GameObject obj)
    {
        yield return new WaitForSeconds(17);
        obj.Recycle(() =>
        {
            if (obj.GetComponent<Building>() != null && obj.GetComponentInChildren<Yey>() != null)
            {
                Destroy(obj.GetComponentInChildren<Yey>().gameObject);
            }
            return true;
        });
    }

    void RepositionInList(ObstacleBase obstacle)
    {
        obstacles.Remove(obstacle);
        obstacles.Insert(obstacles.Count, obstacle);
    }
}