using System;
using System.Collections;
using UnityEngine;

public class Piece : MonoBehaviour
{
    //Time of the last fall, used to auto fall after 
    private float lastFall;

    //Last key pressed time, to handle long press behavior
    private float lastKeyDown;
    private float timeKeyPressed;

    private Vector3 fp;   //First touch position
    private Vector3 lp;   //Last touch position
    private float dragDistance;  //minimum distance for a swipe to be registered

    private Vector3 touchPosWorld;

    private void Start()
    {
        dragDistance = Screen.height * 15 / 100; //dragDistance is 15% height of the screen

        lastFall = Time.time;
        lastKeyDown = Time.time;
        timeKeyPressed = Time.time;
        if (IsValidGridPos())
        {
            InsertOnGrid();
            if (ScoreManager.score <= 0)
            {
                GameOver();
            }
        }
        else
        {
            Debug.Log("KILLED ON START");
            GameOver();
        }
    }

    public void AlignCenter()
    {
        transform.position += transform.position - TetrisUtils.Center(gameObject);
    }

    private bool IsValidGridPos()
    {
        foreach (Transform child in transform)
        {
            Vector2 v = Grid.RoundVector2(child.position);

            //Not inside Border?
            if (!Grid.InsideBorder(v))
                return false;

            //Block in grid cell (and not par of same group)?
            if (Grid.grid[(int)(v.x), (int)(v.y)] != null &&
                Grid.grid[(int)(v.x), (int)(v.y)].parent != transform)
            {
                return false;
            }
        }
        return true;
    }

    //Update the grid
    private void UpdateGrid()
    {
        //Remove old children from grid
        for (int y = 0; y < Grid.h; ++y)
        {
            for (int x = 0; x < Grid.w; ++x)
            {
                if (Grid.grid[x, y] != null &&
                    Grid.grid[x, y].parent == transform)
                {
                    Grid.grid[x, y] = null;
                }
            }
        }
        InsertOnGrid();
    }

    private void InsertOnGrid()
    {
        //Add new children to grid
        foreach (Transform child in transform)
        {
            Vector2 v = Grid.RoundVector2(child.position);
            Grid.grid[(int)v.x, (int)v.y] = child;
        }
    }

    private void GameOver()
    {
        Debug.Log("GAME OVER!");
        while (!IsValidGridPos())
        {
            transform.position += new Vector3(0, 1, 0);
        }
        UpdateGrid(); // to not overleap invalid groups
        enabled = false; // disable script when dies

        GameSixteenManager.Instance.GameOver();
    }

    private void TryChangePos(Vector3 v)
    {
        transform.position += v;

        //See if valid
        if (IsValidGridPos())
            UpdateGrid();
        else
            transform.position -= v;
    }

    private void FallGroup()
    {
        //Modify
        transform.position += new Vector3(0, -1, 0);

        if (IsValidGridPos())
        {
            //It's valid. Update grid... again
            UpdateGrid();
        }
        else
        {
            //It's not valid. revert
            transform.position += new Vector3(0, 1, 0);

            //Clear filled horizontal lines
            Grid.DeleteFullRows();

            FindObjectOfType<Spawner>().SpawnNext();

            //Disable script
            enabled = false;
        }
        lastFall = Time.time;
    }

    //getKey if is pressed now on longer pressed by 0.5 seconds | if that true apply the key each 0.05f while is pressed
    private bool GetKey(KeyCode key)
    {
        bool keyDown = Input.GetKeyDown(key);
        bool pressed = Input.GetKey(key) && Time.time - lastKeyDown > 0.5f && Time.time - timeKeyPressed > 0.05f;

        if (keyDown)
            lastKeyDown = Time.time;
        if (pressed)
            timeKeyPressed = Time.time;

        return keyDown || pressed;
    }

    private void Update()
    {
        //Automatic falling of the pieces
        if((Time.time - lastFall) >= 1f)
        {
            FallGroup();
        }

        //Controlls for Touch
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                fp = touch.position;
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                lp = touch.position;

                touchPosWorld = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                Vector2 touchPosWorld2D = new Vector2(touchPosWorld.x, touchPosWorld.y);
                RaycastHit2D hitInformation = Physics2D.Raycast(touchPosWorld2D, Camera.main.transform.forward);

                //Check if drag distance is greater than 20% of the screen height
                if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
                {
                    if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
                    {
                        if ((lp.x > fp.x))
                        {
                            Debug.Log("Right Swipe");
                            TryChangePos(new Vector3(1, 0, 0));
                            GameSixteenManager.Instance.PlaySound(SoundManager.Instance.moveSound.sound);
                        }
                        else
                        {
                            Debug.Log("Left Swipe");
                            TryChangePos(new Vector3(-1, 0, 0));
                            GameSixteenManager.Instance.PlaySound(SoundManager.Instance.moveSound.sound);
                        }
                    }
                    else
                    {
                        if (lp.y > fp.y)
                        {
                            Debug.Log("Up Swipe");
                            if (gameObject.tag != "Cube")
                            {
                                transform.Rotate(0, 0, -90);

                                if (IsValidGridPos())
                                {
                                    UpdateGrid();
                                }
                                else
                                {
                                    transform.Rotate(0, 0, 90);
                                }
                            }
                            GameSixteenManager.Instance.PlaySound(SoundManager.Instance.rotateSound.sound);
                        }
                        else
                        {
                            Debug.Log("Down Swipe");
                            while (enabled)
                            {
                                //Fall until the bottom 
                                FallGroup();
                                GameSixteenManager.Instance.PlaySound(SoundManager.Instance.fallSound.sound);
                            }
                        }
                    }
                }
                else
                {
                    if (hitInformation.collider != null)
                    {
                        //We should have hit something with a 2D Physics collider!
                        GameObject touchedObject = hitInformation.transform.gameObject;
                        //touchedObject should be the object someone touched.
                        Debug.Log("Touched " + touchedObject.transform.name);
                        if (touchedObject == gameObject)
                        {
                            Debug.Log("Tap");
                            if (gameObject.tag != "Cube")
                            {
                                transform.Rotate(0, 0, -90);

                                // see if valid
                                if (IsValidGridPos())
                                {
                                    //It's valid. Update grid
                                    UpdateGrid();
                                }
                                else
                                {
                                    // it's not valid. revert
                                    transform.Rotate(0, 0, 90);
                                }
                            }
                            GameSixteenManager.Instance.PlaySound(SoundManager.Instance.rotateSound.sound);
                        }
                    }

                    //Debug.Log("Tap");
                    //if (gameObject.tag != "Cube")
                    //{
                    //    transform.Rotate(0, 0, -90);

                    //    // see if valid
                    //    if (IsValidGridPos())
                    //    {
                    //        //It's valid. Update grid
                    //        UpdateGrid();
                    //    }
                    //    else
                    //    {
                    //        // it's not valid. revert
                    //        transform.Rotate(0, 0, 90);
                    //    }
                    //}
                    //GameSixteenManager.Instance.PlaySound(SoundManager.Instance.rotateSound.sound);
                }
            }
        }


        //Controlls for Keyboard
        if (GetKey(KeyCode.LeftArrow))
        {
            TryChangePos(new Vector3(-1, 0, 0));
            GameSixteenManager.Instance.PlaySound(SoundManager.Instance.moveSound.sound);
        }
        else if (GetKey(KeyCode.RightArrow))
        {  
            // Move right
            TryChangePos(new Vector3(1, 0, 0));
            GameSixteenManager.Instance.PlaySound(SoundManager.Instance.moveSound.sound);
        }
        else if (GetKey(KeyCode.UpArrow))
        {
            if(gameObject.tag != "Cube")
            {
                // Rotate
                transform.Rotate(0, 0, -90);

                // see if valid
                if (IsValidGridPos())
                {
                    //It's valid. Update grid
                    UpdateGrid();
                }
                else
                {
                    // it's not valid. revert
                    transform.Rotate(0, 0, 90);
                }
            }
            GameSixteenManager.Instance.PlaySound(SoundManager.Instance.rotateSound.sound);
        }
        else if (GetKey(KeyCode.DownArrow))
        {
            FallGroup();
            GameSixteenManager.Instance.PlaySound(SoundManager.Instance.moveSound.sound);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            while (enabled)
            { 
                //Fall until the bottom 
                FallGroup();
            }
            GameSixteenManager.Instance.PlaySound(SoundManager.Instance.fallSound.sound);
        }
    }
}