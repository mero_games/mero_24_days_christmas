using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] pieces;
    public int nextId;

    private void Start()
    {
        nextId = Random.Range(0, pieces.Length);
        SpawnNext();
    }

    public GameObject CreatePiece(Vector3 v)
    {
        GameObject group = Instantiate(pieces[nextId], v, Quaternion.identity);
        //group.transform.SetParent(GameObject.FindGameObjectWithTag("Board").transform);
        //group.transform.position *= canvas.scaleFactor; bug bug bug everywhere
        // solved in another way: just adjust the UI HUD to scale and keep this shit constant
        return group;
    }

    //SpawnNext piece
    public void SpawnNext()
    {
        //Spawn Group at current Position
        CreatePiece(transform.position);
        nextId = Random.Range(0, pieces.Length);
    }
}