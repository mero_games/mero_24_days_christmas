using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    Text scoreText;

    private void Awake()
    {
        scoreText = GetComponent<Text>();
    }

    private void Start()
    {
        score = 30;
    }

    private void Update()
    {
        scoreText.text = System.String.Format("{0:D2}", score);
        if (score <= 0)
        {
            score = 0;
        }
    }
}