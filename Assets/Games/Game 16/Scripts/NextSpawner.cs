using UnityEngine;

public class NextSpawner : MonoBehaviour
{
    private Spawner spawner;
    private GameObject currentPieceObject;
    private int currentPieceId;

    private void Awake()
    {
        spawner = FindObjectOfType<Spawner>();
    }

    private void Start()
    {
        CreateStoppedPiece();
    }

    private void Update()
    {
        if (currentPieceId != spawner.nextId)
        {
            DeleteCurrentPiece();
            CreateStoppedPiece();
        }
    }

    private void CreateStoppedPiece()
    {
        currentPieceObject = spawner.CreatePiece(transform.position);
        currentPieceId = spawner.nextId;
        //Debug.LogFormat("Object position: {0}", currentPieceObject.transform.position);
        //Debug.LogFormat("Center position: {0}", TetrisUtils.GetBounds(currentPieceObject).center);
        var piece = (Piece)currentPieceObject.GetComponent(typeof(Piece));
        // put the group align with its center
        piece.AlignCenter();
        piece.enabled = false;
    }

    private void DeleteCurrentPiece()
    {
        Destroy(currentPieceObject);
    }
}