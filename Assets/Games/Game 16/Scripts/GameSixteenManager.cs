using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSixteenManager : SingletonBase<GameSixteenManager>
{
    public Transform endScreenContainer;
    public GameObject gridPrefab;
    public int gridWidth = 5;
    public int gridHeight = 10;

    private Vector2 gridSquareSize;

    private void Start()
    {
        CreateGrid();
    }

    private void CreateGrid()
    {
        for (int i = -gridWidth/2; i < gridWidth/2; i++)
        {
            for (int j = -gridHeight/2; j < gridHeight/2; j++)
            {
                GameObject gridSquare = Instantiate(gridPrefab, transform);
                gridSquareSize = gridSquare.GetComponent<SpriteRenderer>().sprite.bounds.size;
                gridSquare.name = "square_" + i + "/" + j;
                gridSquare.transform.localPosition = new Vector2(i + gridSquareSize.x / 2, j + gridSquareSize.y / 2);
            }
        }
    }

    public void GameOver()
    {
        GameObject endScr = Instantiate(NavigationManager.Instance.endScreen, endScreenContainer); ;
    }

    public void PlaySound(AudioClip sound)
    {
        SoundManager.Instance.Play(sound, transform, transform.position,1);
    }
}