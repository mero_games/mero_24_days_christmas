using UnityEngine;

public class Grid : MonoBehaviour
{
    //The grid itself
    public static int w = 10;
    public static int h = 20;

    //Grid storing the Transform element
    public static Transform[,] grid = new Transform[w, h];

    //Convert a real vector to discret coordinates using Mathf.Round
    public static Vector2 RoundVector2(Vector2 v)
    {
        return new Vector2(Mathf.Round(v.x), Mathf.Round(v.y));
    }

    //Check if some vector is inside the limits of game (borders left, right and down)
    public static bool InsideBorder(Vector2 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x < w && (int)pos.y >= 0);
    }

    //Destroy the row at y line
    public static void DeleteRow(int y)
    {
        for (int x = 0; x < w; x++)
        {
            Destroy(grid[x, y].gameObject);
            grid[x, y] = null;
        }
    }

    //Whenever a row was deleted, the above rows should fall towards the bottom by one unit. 
    //The following function will take care of that:
    public static void DecreaseRow(int y)
    {
        for (int x = 0; x < w; x++)
        {
            if (grid[x, y] != null)
            {
                // move one twoards bottom
                grid[x, y - 1] = grid[x, y];
                grid[x, y] = null;

                // Update block position
                grid[x, y - 1].position += new Vector3(0, -1, 0);
            }
        }
    }

    //Whenever a row is deleted, all the above rows should be descreased by 1
    public static void DecreaseRowAbove(int y)
    {
        for (int i = y; i < h; i++)
        {
            DecreaseRow(i);
        }
    }

    //Check if a row is full and, then, can be deleted (score +1)
    public static bool IsRowFull(int y)
    {
        for (int x = 0; x < w; x++)
        {
            if (grid[x, y] == null)
                return false;
        }
        return true;
    }

    public static void DeleteFullRows()
    {
        for (int y = 0; y < h; y++)
        {
            if (IsRowFull(y))
            {
                DeleteRow(y);
                DecreaseRowAbove(y + 1);
                // add new points to score when a row is deleted
                ScoreManager.score -= 1;
                --y;
                // NOTE: --y decreases y by one whenever a row was deleted.
                // it's to make sure that the next step of the for loop continues
                // at the correct index (which must be decreased by one, because we just deleted a row).
                GameSixteenManager.Instance.PlaySound(SoundManager.Instance.lineSound.sound);
            }
        }
    }
}