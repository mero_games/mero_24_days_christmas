using System.Collections;
using UnityEngine;

public class MoveSnowflake : MonoBehaviour
{
    private Collider2D col;
    private GameObject thisLetter;
    private Vector3 originalPos;
    private int xScaler = 1;

    private void Start()
    {
        col = GetComponent<Collider2D>();
        thisLetter = transform.GetChild(0).gameObject;
        EventManager.OnMouseDown += HandleMouseDown;
        originalPos = transform.localPosition;
        //iTween.MoveTo(gameObject, iTween.Hash(
        //"position", new Vector3(transform.position.x, -8f, transform.position.z),
        //"time", GameOneManager.Instance.fallingFlakeLifetime,
        //"easetype", iTween.EaseType.linear,
        //"oncomplete", "DestroyFlake",
        //"onupdate", "UpdateXPos",
        //"onupdatetarget", gameObject));
        StartCoroutine(YTimer());
        iTween.ValueTo(gameObject, iTween.Hash(
            "from", transform.parent.position,
            "to", new Vector3(transform.parent.position.x, -8f, transform.parent.position.z),
            "time", GameOneManager.Instance.fallingFlakeLifetime,
            "easetype", iTween.EaseType.linear,
            "oncomplete", "DestroyFlake",
            "onupdate", "UpdateXPos",
            "onupdatetarget", gameObject));
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }

    private void HandleMouseDown()
    {
        Ray ray = GameOneManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);
        if (hit.collider == col)
        {
            Instantiate(GameOneManager.Instance.snowPopParticle, transform.position, GameOneManager.Instance.snowPopParticle.transform.rotation, transform.parent);
            if (GameOneManager.Instance.IsCenterLetter(thisLetter))
            {
                CorrectChoice();
            }
            else
            {
                IncorrectChoice();
            }
        }
    }

    private void UpdateXPos()
    {
        transform.localPosition = new Vector3(transform.localPosition.x + (.02f * xScaler), transform.localPosition.y - .05f, transform.localPosition.z);
    }

    private IEnumerator YTimer()
    {
        float timer = 0;
        while (GameOneManager.Instance.isPlaying)
        {
            timer += Time.deltaTime;
            if (timer >= .5f)
            {
                xScaler *= -1;
                timer = 0;
            }
            yield return null;
        }
        Destroy(gameObject);
    }

    private void CorrectChoice()
    {
        col.enabled = false;
        iTween.Stop(gameObject);

        GameOneManager.Instance.ProcessCorrectChoice();

        Destroy(gameObject);
    }

    private void IncorrectChoice()
    {
        col.enabled = false;
        iTween.Stop(gameObject);

        GameOneManager.Instance.ProcessIncorrectChoice();

        Destroy(gameObject);
    }

    private void DestroyFlake()
    {
        Destroy(gameObject);
    }

}