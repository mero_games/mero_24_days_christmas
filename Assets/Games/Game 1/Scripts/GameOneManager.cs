using System.Collections;
using UnityEngine;

public class GameOneManager : SingletonBase<GameOneManager>
{
    #region FIELDS
    public bool isPlaying = true;

    public float centerScaleFactor;
    public float movingScaleFactor;
    public float fallingFlakeLifetime;
    public float snowflakeSpawnRate;

    public int chanceOfCorrectSpawn;
    public int correctChoicesToMake;
    public int nrOfLettersToDo;

    public Transform centerPieceHolder;
    public Transform leftSpawnPoint;
    public Transform rightSpawnPoint;

    public GameObject currentCenterLetter;

    public GameObject[] snowFlakes;
    public GameObject[] letters;

    public ParticleSystem snowPopParticle;

    private int correctChoiceCounter = 1;
    private int lettersDone = 1;
    private bool[] usedLetters;
    #endregion


    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        usedLetters = new bool[letters.Length];
        GenerateCenterPiece();
        Coroutine spawnerLeft = StartCoroutine(YSnowflakeSpawner(leftSpawnPoint, snowflakeSpawnRate / 2));
        Coroutine spawnerRight = StartCoroutine(YSnowflakeSpawner(rightSpawnPoint, snowflakeSpawnRate));
    }

    public bool IsCenterLetter(GameObject letter)
    {
        if (currentCenterLetter.name == letter.name)
        {
            return true;
        }
        return false;
    }

    public void ProcessCorrectChoice()
    {
        SoundManager.Instance.Play(SoundManager.Instance.correctChoice.sound, transform, transform.position);
        if (correctChoiceCounter < correctChoicesToMake)
        {
            correctChoiceCounter++;
        }
        else
        {
            if (lettersDone < nrOfLettersToDo)
            {
                lettersDone++;
                correctChoiceCounter = 1;
                GenerateCenterPiece();
            }
            else
            {
                HandleGameOver();
            }
        }
    }

    private void HandleGameOver()
    {
        Debug.Log("GAME 1 OVER");
        isPlaying = false;
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    public void ProcessIncorrectChoice()
    {
        SoundManager.Instance.Play(SoundManager.Instance.incorrectChoice.sound, transform, transform.position);
    }

    private void GenerateCenterPiece()
    {
        SoundManager.Instance.Play(SoundManager.Instance.centerLetterChange.sound, transform, transform.position);
        if (centerPieceHolder.childCount > 0)
        {
            for (int i = 0; i < centerPieceHolder.childCount; i++)
            {
                Destroy(centerPieceHolder.GetChild(i).gameObject);
            }
        }
        var centerFlake = Instantiate(snowFlakes[UnityEngine.Random.Range(0, snowFlakes.Length)], centerPieceHolder.position, Quaternion.identity, centerPieceHolder);

        int temp = UnityEngine.Random.Range(0, letters.Length);

        while (usedLetters[temp])
        {
            temp = UnityEngine.Random.Range(0, letters.Length);
        }
        usedLetters[temp] = true;
        var centerLetter = Instantiate(letters[UnityEngine.Random.Range(0, letters.Length)], centerFlake.transform.position, Quaternion.identity, centerFlake.transform);
        centerLetter.name = centerLetter.name.Replace("(Clone)", "");
        currentCenterLetter = centerLetter;
        centerFlake.transform.localScale = Vector3.zero;
        iTween.ScaleTo(centerFlake, Vector3.one * centerScaleFactor, .3f);
    }

    private IEnumerator YSnowflakeSpawner(Transform holder, float startTime)
    {
        float timer = startTime;

        while (isPlaying)
        {
            timer += Time.deltaTime;
            if (timer >= snowflakeSpawnRate)
            {
                SpawnFallingFlake(snowFlakes[UnityEngine.Random.Range(0, snowFlakes.Length)], holder);
                timer = 0;
            }
            yield return null;
        }

    }

    private void SpawnFallingFlake(GameObject flake, Transform holder)
    {
        GameObject letterToSpawn;
        int percentage = UnityEngine.Random.Range(0, 100);

        if (percentage < chanceOfCorrectSpawn)
        {
            letterToSpawn = currentCenterLetter;
        }
        else
        {
            letterToSpawn = letters[UnityEngine.Random.Range(0, letters.Length)];
        }

        GameObject tempFlake = Instantiate(flake, holder.position, Quaternion.identity, holder);
        tempFlake.transform.localScale *= movingScaleFactor;
        GameObject tempLetter = Instantiate(letterToSpawn, tempFlake.transform.position, Quaternion.identity, tempFlake.transform);
        tempLetter.name = tempLetter.name.Replace("(Clone)", "");
        tempFlake.AddComponent<MoveSnowflake>();
        tempFlake.AddComponent<CircleCollider2D>();
    }
}