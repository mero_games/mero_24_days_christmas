using UnityEngine;

public class Pencil : MonoBehaviour
{
    public AudioSource drawLoopSound;
    public bool toMove = true;

    private void Start()
    {
        drawLoopSound = SoundManager.Instance.Play(SoundManager.Instance.drawingLoop.sound, transform, transform.position, 1, 0, true);
        GetComponent<TrailRenderer>().sortingLayerName = "Background";
        GetComponent<TrailRenderer>().sortingOrder = 1;
    }

    private void Update()
    {
        if (toMove)
        {
            transform.position = new Vector3(GameFourManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition).x,
                                             GameFourManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition).y,
                                             0f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "WrongCollider")
        {
            GameFourManager.Instance.wrongCollidersHit++;
        }
        if (collision.tag == "CorrectCollider")
        {
            GameFourManager.Instance.correctCollidersLeft--;
            collision.gameObject.SetActive(false);
        }
    }
}