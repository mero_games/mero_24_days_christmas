using System.Collections;
using UnityEngine;

public class GameFourManager : SingletonBase<GameFourManager>
{

    public GameObject pencil;

    public Transform drawingBoardsHolder;
    public Transform drawingsHolder;
    public Transform correctColliders;
    public Transform finishedBody;
    public Transform finishedHead;

    public SpriteRenderer exampleBody;
    public SpriteRenderer exampleHead;

    public int numberOfDrawingsToMake;
    public int numberOfDrawingsDone;
    public int correctCollidersLeft;
    public int wrongCollidersHit;

    private bool canDraw;

    private int shapesDrawn;
    private int bodyIndex;
    private int headIndex;

    private GameObject shapeToDraw;

    private Transform currentBoard;

    private GameObject iPencil;
    private GameObject finishedShape;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        GenerateNewSnowman();
        EventManager.OnMouseDown += HandleMouseDown;
        EventManager.OnMouseUp += HandleMouseUp;
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
        EventManager.OnMouseUp -= HandleMouseUp;
    }

    private void GenerateNewSnowman()
    {
        shapesDrawn = 0;
        bodyIndex = Random.Range(0, drawingBoardsHolder.childCount);
        headIndex = Random.Range(0, drawingBoardsHolder.childCount);
        exampleBody.sprite = drawingBoardsHolder.GetChild(bodyIndex).GetChild(0).GetComponent<SpriteRenderer>().sprite;
        exampleHead.sprite = drawingBoardsHolder.GetChild(headIndex).GetChild(0).GetComponent<SpriteRenderer>().sprite;
        SetupDrawingBoard(bodyIndex);
        correctCollidersLeft = correctColliders.childCount;
        SetChildren(correctColliders, true);
    }

    private void HandleMouseDown()
    {
        if (canDraw)
        {
            iPencil = Instantiate(pencil,
                                  new Vector3(MainCamera.ScreenToWorldPoint(Input.mousePosition).x,
                                              MainCamera.ScreenToWorldPoint(Input.mousePosition).y,
                                              0),
                                  Quaternion.identity,
                                  transform);
        }
    }

    private void HandleMouseUp()
    {
        if (iPencil)
        {
            canDraw = false;
            iPencil.GetComponent<Pencil>().toMove = false;
            if (iPencil.GetComponent<Pencil>().drawLoopSound)
            {
                iPencil.GetComponent<Pencil>().drawLoopSound.Stop();
            }
            iPencil.transform.SetParent(drawingsHolder);
            if (wrongCollidersHit > 0)
            {
                SoundManager.Instance.Play(SoundManager.Instance.drawDelete.sound, transform, transform.position);
                DeleteDrawings();
                SetChildren(correctColliders, true);
                correctCollidersLeft = correctColliders.childCount;
                wrongCollidersHit = 0;
                canDraw = true;
            }

            if (wrongCollidersHit == 0 && correctCollidersLeft > 0)
            {
                canDraw = true;
            }

            if (wrongCollidersHit == 0 && correctCollidersLeft == 0)
            {
                SoundManager.Instance.Play(SoundManager.Instance.correctDraw.sound, transform, transform.position);
                HandleShapeDrawn();
            }
        }
    }

    private void SetupDrawingBoard(int index)
    {
        for (int i = 0; i < drawingBoardsHolder.childCount; i++)
        {
            drawingBoardsHolder.GetChild(i).gameObject.SetActive(false);
        }
        currentBoard = drawingBoardsHolder.GetChild(index);
        shapeToDraw = currentBoard.GetChild(0).gameObject;
        correctColliders = currentBoard.GetChild(2);
        SetChildren(correctColliders, true);
        correctCollidersLeft = correctColliders.childCount;
        currentBoard.gameObject.SetActive(true);
        canDraw = true;
    }

    private void DeleteDrawings()
    {
        while (drawingsHolder.childCount > 0)
        {
            DestroyImmediate(drawingsHolder.GetChild(0).gameObject);
        }
    }

    private void SetChildren(Transform trans, bool setTo)
    {
        foreach (var t in trans.GetComponentsInChildren<Transform>(true))
        {
            if (t != trans)
            {
                t.gameObject.SetActive(setTo);
            }
        }
    }

    private void HandleShapeDrawn()
    {
        shapesDrawn++;
        DeleteDrawings();
        if (shapesDrawn == 1)
        {
            SetDrawingToPlace(shapeToDraw, finishedBody);
        }

        if (shapesDrawn == 2)
        {
            SetDrawingToPlace(shapeToDraw, finishedHead);
        }

    }

    private void SetDrawingToPlace(GameObject drawing, Transform parent)
    {
        finishedShape = Instantiate(drawing, drawing.transform.position, drawing.transform.rotation);
        currentBoard.gameObject.SetActive(false);
        finishedShape.GetComponent<SpriteRenderer>().color = new Color(finishedShape.GetComponent<SpriteRenderer>().color.r,
                                                                       finishedShape.GetComponent<SpriteRenderer>().color.g,
                                                                       finishedShape.GetComponent<SpriteRenderer>().color.b,
                                                                       1f);
        iTween.MoveTo(finishedShape, iTween.Hash("position", parent.position,
                                                 "time", 1f,
                                                 "easetype", iTween.EaseType.linear,
                                                 "oncomplete", "DrawingDone",
                                                 "oncompletetarget", gameObject));
        iTween.ScaleTo(finishedShape, new Vector3(.7f, .7f, 1f), 1f);

    }

    private void DrawingDone()
    {
        Debug.Log("DONE");
        if (shapesDrawn == 1)
        {
            finishedBody.gameObject.SetActive(true);
            finishedBody.GetComponent<SpriteRenderer>().sprite = finishedShape.GetComponent<SpriteRenderer>().sprite;
            finishedBody.GetComponent<SpriteRenderer>().enabled = true;
            Destroy(finishedShape);
            SetupDrawingBoard(headIndex);
            canDraw = true;
        }

        if (shapesDrawn == 2)
        {
            numberOfDrawingsDone++;
            finishedHead.gameObject.SetActive(true);
            finishedHead.GetComponent<SpriteRenderer>().sprite = finishedShape.GetComponent<SpriteRenderer>().sprite;
            finishedHead.GetComponent<SpriteRenderer>().enabled = true;
            Destroy(finishedShape);
            SetChildren(finishedBody.parent, true);
            if (numberOfDrawingsDone == numberOfDrawingsToMake)
            {
                HandleGameOver();
            }
            else
            {
                StartCoroutine(YStartNextDrawing());
            }
        }
    }

    private IEnumerator YStartNextDrawing()
    {
        yield return new WaitForSeconds(2f);
        SetChildren(finishedBody.parent, false);
        GenerateNewSnowman();

    }

    private void HandleGameOver()
    {
        NavigationManager.Instance.InstantiateEndScreen(transform);
        Debug.Log("GAME 4 OVER");
    }
}