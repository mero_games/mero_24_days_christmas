using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTenManager : SingletonBase<GameTenManager>
{
    public int correctCounter;

    public Transform drawings;

    public Collider2D startCollider;

    public Color[] drawColors;

    public Sprite[] unwrappedGifts;
    public Sprite[] wrappedGifts;

    public SpriteRenderer[] topSlots;
    public SpriteRenderer[] bottomSlots;

    public GameObject pencil;

    private GameObject iPencil;

    private bool[] beenUsed;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        EventManager.OnMouseUp += HandleMouseUp;
        beenUsed = new bool[wrappedGifts.Length];
        StartCoroutine(PopulateSlots());
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
        EventManager.OnMouseUp -= HandleMouseUp;
    }

    private void HandleMouseDown()
    {
        Ray ray = MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);
        if (hit && hit.collider.tag == "CorrectCollider")
        {
            Vector3 targetPos = new Vector3(MainCamera.ScreenToWorldPoint(Input.mousePosition).x,
                                            MainCamera.ScreenToWorldPoint(Input.mousePosition).y,
                                            0f);
            iPencil = Instantiate(pencil, targetPos, Quaternion.identity, transform);
            startCollider = hit.collider;
        }
    }

    private void HandleMouseUp()
    {
        if (iPencil && iPencil.GetComponent<PencilLine>().toMove)
        {
            Destroy(iPencil);
        }
    }

    public void CallPopulateSlots()
    {
        StartCoroutine(PopulateSlots());
    }

    public void HandleGameOver()
    {
        Debug.Log("GAME 10 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    private IEnumerator PopulateSlots()
    {
        for (int i = 0; i < drawings.childCount; i++)
        {
            Destroy(drawings.GetChild(i).gameObject);
        }
        if (topSlots[0].transform.localScale.x > 0)
        {
            for (int i = 0; i < 3; i++)
            {
                iTween.ScaleTo(bottomSlots[i].gameObject, Vector3.zero, .5f);
                iTween.ScaleTo(topSlots[i].gameObject, Vector3.zero, .5f);
            }
            yield return new WaitForSeconds(.5f);
        }
        bool[] tempUsed = new bool[3];
        int temp = Random.Range(0, wrappedGifts.Length);
        for (int i = 0; i < 3; i++)
        {
            while (beenUsed[temp])
            {
                temp = Random.Range(0, wrappedGifts.Length);
            }
            topSlots[i].sprite = unwrappedGifts[temp];
            topSlots[i].GetComponent<GiftSlot>().slotIndex = i;
            beenUsed[temp] = true;
            int tempTwo = Random.Range(0, tempUsed.Length);
            while (tempUsed[tempTwo])
            {
                tempTwo = Random.Range(0, tempUsed.Length);
            }
            Debug.Log(tempTwo);
            bottomSlots[tempTwo].sprite = wrappedGifts[temp];
            bottomSlots[tempTwo].GetComponent<GiftSlot>().slotIndex = i;
            tempUsed[tempTwo] = true;
            iTween.ScaleTo(bottomSlots[i].gameObject, new Vector3(.7f, .7f, 1f), .5f);
            iTween.ScaleTo(topSlots[i].gameObject, new Vector3(.7f, .7f, 1f), .5f);
        }
        yield return new WaitForSeconds(.5f);
    }

}