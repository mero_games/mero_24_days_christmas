using UnityEngine;

public class PencilLine : MonoBehaviour
{
    public bool toMove = true;
    public AudioSource drawSound;

    private void Start()
    {
        drawSound = SoundManager.Instance.Play(SoundManager.Instance.lineDrawingLoop.sound, transform, transform.position, 1, 0, true);
        Color tempColor = GameTenManager.Instance.drawColors[Random.Range(0, GameTenManager.Instance.drawColors.Length)];
        GetComponent<SpriteRenderer>().color = tempColor;
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<TrailRenderer>().materials[0].color = tempColor;
        GetComponent<TrailRenderer>().startColor = tempColor;
        GetComponent<TrailRenderer>().endColor = tempColor;
        GetComponent<TrailRenderer>().sortingLayerName = "Background";
        GetComponent<TrailRenderer>().sortingOrder = 1;
    }

    private void Update()
    {
        if (toMove)
        {
            transform.position = new Vector3(GameTenManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition).x,
                                             GameTenManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition).y,
                                             0f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision != GameTenManager.Instance.startCollider)
        {
            if (collision.tag == "WrongCollider")
            {
                SoundManager.Instance.Play(SoundManager.Instance.incorrectLine.sound, transform, transform.position);
                if (drawSound)
                {
                    drawSound.Stop();
                }
                Destroy(gameObject);
                return;
            }
            if (collision.GetComponent<GiftSlot>().slotIndex == GameTenManager.Instance.startCollider.GetComponent<GiftSlot>().slotIndex)
            {
                if (drawSound)
                {
                    drawSound.Stop();
                }
                SoundManager.Instance.Play(SoundManager.Instance.correctLine.sound, transform, transform.position);
                GameTenManager.Instance.correctCounter++;
                toMove = false;
                transform.SetParent(GameTenManager.Instance.drawings);
                if (GameTenManager.Instance.correctCounter % 3 == 0)
                {
                    if (GameTenManager.Instance.correctCounter == 18)
                    {
                        GameTenManager.Instance.HandleGameOver();
                    }
                    else
                    {
                        GameTenManager.Instance.CallPopulateSlots();
                    }
                }
            }
            else
            {
                SoundManager.Instance.Play(SoundManager.Instance.incorrectLine.sound, transform, transform.position);
                if (drawSound)
                {
                    drawSound.Stop();
                }
                Destroy(gameObject);
            }
        }
    }
}