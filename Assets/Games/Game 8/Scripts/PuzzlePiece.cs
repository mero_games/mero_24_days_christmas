using System.Collections;
using UnityEngine;

public class PuzzlePiece : MonoBehaviour
{

    public Vector3 startSlotPosition;

    private bool isDragging;
    private bool beenDropped;

    private Collider2D col;
    private Collider2D overlapCollider;

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        EventManager.OnMouseUp += HandleMouseUp;
        col = GetComponent<Collider2D>();
    }

    private void OnDisable()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
        EventManager.OnMouseUp -= HandleMouseUp;
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
        EventManager.OnMouseUp -= HandleMouseUp;
    }

    private void HandleMouseDown()
    {
        Ray ray = GameEightManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);
        if (hit)
        {
            if (hit.collider == col && hit.collider.tag == "Puzzle")
            {
                GetComponent<SpriteRenderer>().sortingOrder = 100;
                isDragging = true;
                StartCoroutine(YStartDragging());
            }
        }
    }


    private void HandleMouseUp()
    {
        Debug.Log("wtf");
        if (isDragging)
        {
            GetComponent<SpriteRenderer>().sortingOrder = 0;
            if (overlapCollider && overlapCollider == transform.parent.GetComponent<Collider2D>())
            {
                SoundManager.Instance.Play(SoundManager.Instance.correctPlacement.sound, transform, transform.position);
                transform.position = transform.parent.position;
                GameEightManager.Instance.IncrementCounter();
                col.enabled = false;
                transform.parent.GetComponent<Collider2D>().enabled = false;
                this.enabled = false;
                isDragging = false;
            }
            else
            {
                if (isDragging)
                {
                    SoundManager.Instance.Play(SoundManager.Instance.incorrectPlacement.sound, transform, transform.position);
                }
                transform.position = startSlotPosition;
                isDragging = false;
            }
        }
    }

    private IEnumerator YStartDragging()
    {
        while (isDragging)
        {
            Vector3 targetPos = new Vector3(GameEightManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition).x,
                                            GameEightManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition).y,
                                            0f);
            transform.position = targetPos;
            yield return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Untagged" && collision == transform.parent.GetComponent<Collider2D>())
        {
            overlapCollider = collision;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Untagged" && collision == transform.parent.GetComponent<Collider2D>())
        {
            overlapCollider = null;
        }
    }
}