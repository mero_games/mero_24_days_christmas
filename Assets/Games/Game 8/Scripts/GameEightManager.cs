using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class StartingSlot
{
    public Transform slotTransform;
    public bool isTaken;
}

[System.Serializable]
public class BackgroundsHolder
{
    public Sprite wholeBackground;
    public Sprite[] slicedPieces;
}

public class GameEightManager : SingletonBase<GameEightManager>
{

    public int counter2by2;
    public int counter2by3;
    public int counter3by3;

    public GameObject grid2by2;
    public GameObject grid2by3;
    public GameObject grid3by3;

    public Image background;

    public Transform[] puzzlePieces3by3;
    public Transform[] puzzlePieces2by3;
    public Transform[] puzzlePieces2by2;

    public BackgroundsHolder[] backgrounds2by2;
    public BackgroundsHolder[] backgrounds2by3;
    public BackgroundsHolder[] backgrounds3by3;

    public StartingSlot[] startingSlots;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        StartCoroutine(Y2by2());
    }

    private IEnumerator Y2by2()
    {
        int temp = Random.Range(0, backgrounds2by2.Length);
        background.sprite = backgrounds2by2[temp].wholeBackground;
        for (int i = 0; i < puzzlePieces2by2.Length; i++)
        {
            puzzlePieces2by2[i].GetComponent<SpriteRenderer>().sprite = backgrounds2by2[temp].slicedPieces[i];
        }
        grid2by2.SetActive(true);
        yield return new WaitForSeconds(1f);
        SoundManager.Instance.Play(SoundManager.Instance.levelStartShake.sound, transform, transform.position);
        iTween.ShakeRotation(grid2by2, new Vector3(0f, 0f, 7f), .5f);
        yield return new WaitForSeconds(.5f);
        StartCoroutine(MoveToSlots(puzzlePieces2by2));
        yield return new WaitUntil(() => counter2by2 == 4);
        yield return new WaitForSeconds(1f);
        iTween.ScaleTo(grid2by2, Vector3.zero, .5f);
        yield return new WaitForSeconds(.7f);
        grid2by2.SetActive(false);
        SoundManager.Instance.Play(SoundManager.Instance.levelFinished.sound, transform, transform.position);
        StartCoroutine(Y2by3());
    }

    private IEnumerator Y2by3()
    {
        int temp = Random.Range(0, backgrounds2by3.Length);
        background.sprite = backgrounds2by3[temp].wholeBackground;
        for (int i = 0; i < puzzlePieces2by3.Length; i++)
        {
            puzzlePieces2by3[i].GetComponent<SpriteRenderer>().sprite = backgrounds2by3[temp].slicedPieces[i];
        }
        grid2by3.SetActive(true);
        yield return new WaitForSeconds(1f);
        SoundManager.Instance.Play(SoundManager.Instance.levelStartShake.sound, transform, transform.position);
        iTween.ShakeRotation(grid2by3, new Vector3(0f, 0f, 7f), .5f);
        yield return new WaitForSeconds(.5f);
        StartCoroutine(MoveToSlots(puzzlePieces2by3));
        yield return new WaitUntil(() => counter2by3 == 6);
        yield return new WaitForSeconds(1f);
        iTween.ScaleTo(grid2by3, Vector3.zero, .5f);
        yield return new WaitForSeconds(.7f);
        grid2by3.SetActive(false);
        SoundManager.Instance.Play(SoundManager.Instance.levelFinished.sound, transform, transform.position);
        StartCoroutine(Y3by3());
    }

    private IEnumerator Y3by3()
    {
        int temp = Random.Range(0, backgrounds3by3.Length);
        background.sprite = backgrounds3by3[temp].wholeBackground;
        for (int i = 0; i < puzzlePieces3by3.Length; i++)
        {
            puzzlePieces3by3[i].GetComponent<SpriteRenderer>().sprite = backgrounds3by3[temp].slicedPieces[i];
        }
        grid3by3.SetActive(true);
        yield return new WaitForSeconds(1f);
        SoundManager.Instance.Play(SoundManager.Instance.levelStartShake.sound, transform, transform.position);
        iTween.ShakeRotation(grid3by3, new Vector3(0f, 0f, 7f), .5f);
        yield return new WaitForSeconds(.5f);
        StartCoroutine(MoveToSlots(puzzlePieces3by3));
        yield return new WaitUntil(() => counter3by3 == 9);
        SoundManager.Instance.Play(SoundManager.Instance.levelFinished.sound, transform, transform.position);
        HandleGameOver();
    }

    private void HandleGameOver()
    {
        Debug.Log("GAME 8 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    private IEnumerator MoveToSlots(Transform[] pieces)
    {
        for (int i = 0; i < startingSlots.Length; i++)
        {
            startingSlots[i].isTaken = false;
        }
        for (int i = 0; i < pieces.Length; i++)
        {
            int temp = Random.Range(0, startingSlots.Length);
            while (startingSlots[temp].isTaken)
            {
                temp = Random.Range(0, startingSlots.Length);
            }
            iTween.MoveTo(pieces[i].gameObject, startingSlots[temp].slotTransform.position, .6f);
            startingSlots[temp].isTaken = true;
            pieces[i].GetComponent<PuzzlePiece>().startSlotPosition = startingSlots[temp].slotTransform.position;
        }
        yield return new WaitForSeconds(.6f);

        foreach (var p in pieces)
        {
            p.GetComponent<Collider2D>().enabled = true;
        }
    }

    public void IncrementCounter()
    {
        if (grid2by2.activeSelf)
        {
            counter2by2++;
        }
        if (grid2by3.activeSelf)
        {
            counter2by3++;
        }
        if (grid3by3.activeSelf)
        {
            counter3by3++;
        }
    }

}