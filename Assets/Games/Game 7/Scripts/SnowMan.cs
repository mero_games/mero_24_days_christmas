using System.Collections;
using UnityEngine;

public class SnowMan : MonoBehaviour
{

    public SpriteRenderer mouth;

    public bool isHappy;
    public bool wasHit;
    public bool isDown = true;

    public void MoveDown()
    {
        GameSevenManager.Instance.risenCounter--;
        StopAllCoroutines();
        if (wasHit)
        {
            iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(transform.position.x, transform.position.y - 1.5f, 0f),
                                                  "time", GameSevenManager.Instance.moveTime,
                                                  "delay", .4f,
                                                  "oncomplete", "MoveDownComplete"));
        }
        else
        {
            iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(transform.position.x, transform.position.y - 1.5f, 0f),
                                                  "time", GameSevenManager.Instance.moveTime,
                                                  "oncomplete", "MoveDownComplete"));
        }
        wasHit = false;
    }

    private void MoveDownComplete()
    {
        isDown = true;
    }


    public void MoveUp()
    {
        SoundManager.Instance.Play(SoundManager.Instance.snowmanRise.sound, transform, transform.position);
        StartCoroutine(YUpStandTimer());
        int temp = Random.Range(0, 100);
        if (temp < GameSevenManager.Instance.chanceOfHappy)
        {
            mouth.sprite = GameSevenManager.Instance.mouthHappy;
            isHappy = true;
        }
        else
        {
            mouth.sprite = GameSevenManager.Instance.mouthSad;
            isHappy = false;
        }

        iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(transform.position.x, transform.position.y + 1.5f, 0f),
                                              "time", GameSevenManager.Instance.moveTime,
                                              "oncomplete", "MoveUpComplete"));
    }

    private void MoveUpComplete()
    {
        isDown = false;
        if (wasHit)
        {
            MoveDown();
            wasHit = false;
        }
    }

    private IEnumerator YUpStandTimer()
    {
        float timer = 0;
        while (timer <= GameSevenManager.Instance.upStandTime)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        MoveDown();
    }

}