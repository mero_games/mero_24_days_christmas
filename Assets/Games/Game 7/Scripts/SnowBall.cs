using UnityEngine;

public class SnowBall : MonoBehaviour
{
    private void Start()
    {
        SoundManager.Instance.Play(SoundManager.Instance.snowballThrow.sound, transform, transform.position, 1, 0, true);
        if (transform.position.y < -0.3f)
        {
            SpawnWithSpot();
        }
        else
        {
            SpawnWithoutSpot();
        }
        Destroy(gameObject, .7f);
    }

    private void SpawnWithoutSpot()
    {
        iTween.ScaleTo(gameObject, Vector3.zero, .6f);
    }

    private void SpawnWithSpot()
    {
        iTween.ScaleTo(transform.GetChild(0).gameObject, iTween.Hash("scale", new Vector3(.4f, .4f, .4f),
                                                                     "time", .3f,
                                                                     "oncomplete", "ThrowComplete",
                                                                     "oncompletetarget", gameObject));
    }

    private void ThrowComplete()
    {
        SoundManager.Instance.Play(SoundManager.Instance.snowballSplash.sound, transform, transform.position);
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(true);
    }
}