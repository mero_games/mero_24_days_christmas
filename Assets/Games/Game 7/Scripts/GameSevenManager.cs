using System.Collections;
using UnityEngine;

public class GameSevenManager : SingletonBase<GameSevenManager>
{

    public bool isPlaying = true;

    public int hitCounter;
    public int hitsToLand;
    public int chanceOfHappy;
    public int risenCounter;

    public float upStandTime;
    public float moveTime;
    public float riseInterval;

    public Sprite mouthHappy;
    public Sprite mouthSad;

    public SnowMan[] snowmen;

    public GameObject snowBall;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        StartCoroutine(YSnowmenRaiser());
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }

    private void HandleMouseDown()
    {
        if (isPlaying)
        {
            Vector3 pos = new Vector3(MainCamera.ScreenToWorldPoint(Input.mousePosition).x, MainCamera.ScreenToWorldPoint(Input.mousePosition).y, 0f);
            Instantiate(snowBall, pos, Quaternion.identity, transform);
            Ray ray = MainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);

            if (hit)
            {
                SnowMan hitSnowMan = hit.collider.GetComponentInParent<SnowMan>();
                hitSnowMan.wasHit = true;
                if (!hitSnowMan.isDown)
                {
                    Debug.Log("HIT");
                    hit.collider.GetComponentInParent<SnowMan>().MoveDown();
                }
                if (!hitSnowMan.isHappy)
                {
                    SoundManager.Instance.Play(SoundManager.Instance.goodSnowballThrow.sound, transform, transform.position);
                    hitCounter++;
                    hitSnowMan.isHappy = true;
                    if (hitCounter >= hitsToLand)
                    {
                        isPlaying = false;
                        HandleGameOver();
                    }
                }
                else
                {
                    SoundManager.Instance.Play(SoundManager.Instance.badSnowballThrow.sound, transform, transform.position);
                }
            }
        }
    }

    private IEnumerator YSnowmenRaiser()
    {
        float timer = riseInterval;

        while (isPlaying)
        {
            timer += Time.deltaTime;
            if (timer >= riseInterval)
            {
                timer = 0f;
                RaiseSnowman();
            }
            yield return null;
        }
    }

    private void HandleGameOver()
    {
        Debug.Log("GAME 8 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    private void RaiseSnowman()
    {
        int temp = Random.Range(0, snowmen.Length);
        if (risenCounter == snowmen.Length - 1)
        {
            return;
        }
        while (!snowmen[temp].isDown)
        {
            temp = Random.Range(0, snowmen.Length);
        }
        snowmen[temp].MoveUp();
        risenCounter++;
    }
}