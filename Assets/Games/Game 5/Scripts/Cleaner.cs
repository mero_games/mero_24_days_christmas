using System.Collections;
using UnityEngine;

public class Cleaner : MonoBehaviour
{
    private AudioSource cleanSound;

    private Collider2D currentCollider;

    public ParticleSystem smokePuff;

    private void OnEnable()
    {
        StartCoroutine(YFollowMouse());
    }

    private IEnumerator YFollowMouse()
    {
        cleanSound = SoundManager.Instance.Play(SoundManager.Instance.cleaningSoundLoop.sound, transform, transform.position, 1, 0, true);
        cleanSound.volume = 0f;
        float timer = 0f;
        while (gameObject)
        {
            timer += Time.deltaTime;
            if (timer >= .15f && currentCollider && GameFiveManager.Instance.isPlaying)
            {
                Instantiate(smokePuff, transform.position, smokePuff.transform.rotation, transform.parent);
                timer = 0f;
            }
            transform.position = GameFiveManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition);
            yield return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision);
        if (collision.tag == "CorrectCollider" && GameFiveManager.Instance.isPlaying)
        {
            currentCollider = collision;
            cleanSound.volume = 1f;
        }
        if (collision.tag == "Dirt")
        {
            cleanSound.volume = 1f;
        }
        else
        {
            if (cleanSound)
            {
                //cleanSound.Stop();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "CorrectCollider")
        {
            currentCollider = null;
            if (cleanSound)
            {
                cleanSound.volume = 0f;
            }
        }
    }

    private void OnDestroy()
    {
        if (cleanSound)
        {
            Destroy(cleanSound.gameObject);
        }
    }
}