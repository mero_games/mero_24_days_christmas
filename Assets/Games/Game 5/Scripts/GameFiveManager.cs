using System.Collections;
using UnityEngine;

public class GameFiveManager : SingletonBase<GameFiveManager>
{
    public bool isPlaying = true;

    public Color[] bootColorsMale;
    public Color[] bootColorsFemale;

    public SpriteRenderer femaleBoot;
    public SpriteRenderer maleBoot;

    public int numberOfDirtFlecks;
    public int dirtFlecksCleaned;

    public Cleaner cleaner;
    private Cleaner iCleaner;

    private Vector3 lastMousePos;

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        EventManager.OnMouseUp += HandleMouseUp;
        int temp = Random.Range(0, 2);
        if (temp == 0)
        {
            femaleBoot.gameObject.SetActive(true);
            femaleBoot.color = bootColorsFemale[Random.Range(0, bootColorsFemale.Length)];
        }
        else
        {
            maleBoot.gameObject.SetActive(true);
            maleBoot.color = bootColorsMale[Random.Range(0, bootColorsMale.Length)];
        }
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
        EventManager.OnMouseUp -= HandleMouseUp;
    }

    private void HandleMouseDown()
    {
        iCleaner = Instantiate(cleaner, MainCamera.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity, transform);
        lastMousePos = MainCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    private void HandleMouseUp()
    {
        Destroy(iCleaner.gameObject);
    }


    public void StartCleaning(Dirt dirt)
    {
        StartCoroutine(YCleanDirt(dirt));
    }


    public void StopCleaning()
    {
        StopAllCoroutines();
    }

    private IEnumerator YCleanDirt(Dirt dirt)
    {
        while (dirt.cleanTime > 0)
        {
            if (Vector3.Distance(lastMousePos, MainCamera.ScreenToWorldPoint(Input.mousePosition)) >= .01f)
            {
                dirt.cleanTime -= Time.deltaTime;
                dirt.GetComponent<SpriteRenderer>().color = new Color(dirt.GetComponent<SpriteRenderer>().color.r,
                                                                      dirt.GetComponent<SpriteRenderer>().color.g,
                                                                      dirt.GetComponent<SpriteRenderer>().color.b,
                                                                      dirt.cleanTime / dirt.TotalCleanTime);
                lastMousePos = MainCamera.ScreenToWorldPoint(Input.mousePosition);
                if (dirt.GetComponent<SpriteRenderer>().color.a < .2f)
                {
                    break;
                }
            }
            yield return null;
        }

        SoundManager.Instance.Play(SoundManager.Instance.dirtCleaned.sound, transform, transform.position);

        dirt.cleanTime = 0;
        dirt.GetComponent<SpriteRenderer>().color = new Color(dirt.GetComponent<SpriteRenderer>().color.r,
                                                                  dirt.GetComponent<SpriteRenderer>().color.g,
                                                                  dirt.GetComponent<SpriteRenderer>().color.b,
                                                                  0);
        Destroy(dirt.gameObject);
        dirtFlecksCleaned++;
        if (dirtFlecksCleaned == numberOfDirtFlecks)
        {
            HandleGameOver();
        }
    }

    private void HandleGameOver()
    {
        isPlaying = false;
        Debug.Log("GAME 5 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }


}