﻿using UnityEngine;

public class Dirt : MonoBehaviour
{
    public float cleanTime;
    public float TotalCleanTime { get; set; }

    private void Start()
    {
        TotalCleanTime = cleanTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Cleaner" && GetComponent<SpriteRenderer>().color.a > 0)
        {
            GameFiveManager.Instance.StartCleaning(this);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Cleaner")
        {
            GameFiveManager.Instance.StopCleaning();
        }
    }
}
