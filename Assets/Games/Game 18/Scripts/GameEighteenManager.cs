using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameEighteenManager : SingletonBase<GameEighteenManager>
{

    [System.Serializable]
    public struct GameRounds
    {
        public int colors;
        public int length;
        public float speed;
    }


    public Color[] colors;
    //public Color offColor;
    public List<Transform> bulbs;
    public GameRounds[] rounds;
    public bool swapColors = false;
    public LayerMask mask;
    public float waitTime = 0.5f;

    private int curRound = 0;
    bool canClick = false;
    List<int> clicks;
    List<int> oldClicks;
    //List<Color> newColors; 

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        //newColors = new List<Color> ();
        clicks = new List<int>();
        oldClicks = new List<int>();
        ClearAll();
        RandomizeColors();
        NextGame();
    }

    private void RandomizeColors()
    {
        List<Color> tempColors = colors.ToList();
        //newColors.Clear ();
        SpriteRenderer[] renderers;
        int k = 0;
        while (tempColors.Count > 0)
        {
            if (tempColors.Count == 0)
            {
                break;
            }
            int index = Random.Range(0, tempColors.Count);
            renderers = bulbs[k].GetChild(0).GetComponentsInChildren<SpriteRenderer>(true);
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].color = tempColors[index];
            }

            //newColors.Add (tempColors [index]);
            tempColors.RemoveAt(index);
            k++;
        }
    }

    public void SetAll()
    {
        for (int i = 0; i < bulbs.Count; i++)
        {
            bulbs[i].GetChild(0).gameObject.SetActive(true);
        }
    }


    public void ClearAll()
    {
        for (int i = 0; i < bulbs.Count; i++)
        {
            bulbs[i].GetChild(0).gameObject.SetActive(false);
        }
    }


    public void NextGame()
    {
        canClick = false;
        curRound++;
        if (curRound > rounds.Length)
        {
            StopAllCoroutines();
            HandleGameOver();

        }
        else
        {

            StartCoroutine(YShowGame(curRound - 1));
        }

    }

    void SetColliders(bool en)
    {
        for (int i = 0; i < bulbs.Count; i++)
        {
            bulbs[i].GetComponent<BoxCollider2D>().enabled = en;

        }
    }

    void ResetColliders()
    {
        SetColliders(true);
        ClearAll();
    }



    void Update()
    {
        if (Input.GetMouseButtonDown(0) && canClick)
        {
            int ind = GetBulb();
            print(ind);
            if (ind >= 0 && ind < bulbs.Count)
            {
                if (clicks[0] == ind)
                {
                    ClearAll();

                    bulbs[clicks[0]].GetChild(0).gameObject.SetActive(true);
                    PlaySound(clicks[0]);
                    clicks.RemoveAt(0);
                    PrintList(clicks);
                    if (clicks.Count == 0)
                    {

                        NextGame();
                    }
                    else
                    {
                        SetColliders(false);
                        Invoke("ResetColliders", waitTime);
                    }
                }
                else
                {
                    StartCoroutine(YShowGame(curRound - 1, false));
                }
            }
        }
    }

    int GetBulb()
    {

        Vector3 pos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        Vector3 clickPos = Camera.main.ScreenToWorldPoint(pos);
        RaycastHit2D hit = Physics2D.Raycast(clickPos, Vector2.zero, 1000, mask);

        if (hit != null && hit.collider != null)
        {
            print(" " + hit.collider.transform.name);
            return bulbs.FindIndex(t => t.name == hit.collider.name);

        }
        return -1;
    }

    void PrintList(List<int> l)
    {
        string s = "";
        for (int i = 0; i < l.Count; i++)
        {
            s += " " + l[i];

        }
        print(s);
    }

    void PlaySound(AudioClip sound)
    {
        if (sound != null && SoundManager.Instance != null)
        {
            SoundManager.Instance.Play(sound, transform, transform.position);

        }
    }

    void PlaySound(int i = 0)
    {
        if (SoundManager.Instance.sounds != null && SoundManager.Instance != null)
        {
            if (SoundManager.Instance.sounds.Length > i && SoundManager.Instance.sounds[i] != null)
            {
                SoundManager.Instance.Play(SoundManager.Instance.sounds[i].sound, transform, transform.position);

            }
        }
    }

    IEnumerator YShowGame(int ind, bool random = true)
    {
        canClick = false;

        GameRounds round = rounds[ind];
        if (random)
        {
            clicks.Clear();
        }
        else
        {
            Handheld.Vibrate();
            iTween.ShakePosition(bulbs[0].parent.parent.gameObject, new Vector3(0.25f, 0.25f, 0), 0.2f);
        }
        if (random == true && ind != 0)
        {
            PlaySound(SoundManager.Instance.correctSound.sound);
            yield return new WaitForSeconds(waitTime);
            for (int k = 0; k < 4; k++)
            {
                for (int i = 0; i < bulbs.Count; i++)
                {
                    ClearAll();
                    bulbs[i].GetChild(0).gameObject.SetActive(true);
                    yield return new WaitForSeconds(0.05f);
                }
            }
            ClearAll();
            yield return new WaitForSeconds(1.8f);

        }
        else if (random == false)
        {
            PlaySound(SoundManager.Instance.badSound.sound);

        }

        yield return new WaitForSeconds(0.6f);
        ClearAll();
        if (swapColors)
        {
            RandomizeColors();
        }
        int rand = 0;
        if (random)
        {
            oldClicks.Clear();
            for (int i = 0; i < round.length; i++)
            {
                rand = Random.Range(0, round.colors);
                clicks.Add(rand);
                oldClicks.Add(rand);
                PlaySound(rand);
                bulbs[rand].transform.GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(1.5f / round.speed);
                bulbs[rand].transform.GetChild(0).gameObject.SetActive(false);
                yield return new WaitForSeconds(0.5f / round.speed);
            }
            PrintList(clicks);
        }
        else
        {
            clicks.Clear();
            for (int i = 0; i < oldClicks.Count; i++)
            {
                clicks.Add(oldClicks[i]);
                PlaySound(oldClicks[i]);
                bulbs[oldClicks[i]].transform.GetChild(0).gameObject.SetActive(true);
                yield return new WaitForSeconds(1.5f / round.speed);
                bulbs[oldClicks[i]].transform.GetChild(0).gameObject.SetActive(false);
                yield return new WaitForSeconds(2.0f / round.speed);
            }
        }
        ClearAll();

        canClick = true;
    }



    private void HandleGameOver()
    {
        Debug.Log("GAME 18 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }


}