using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class GameFifteenManager : SingletonBase<GameFifteenManager>
{
    [System.Serializable]
    public struct LightColors
    {
        [HideInInspector]
        public Color newcolor;
        public Color color;

    }

    [System.Serializable]
    public struct LightCompleted
    {
        [HideInInspector]
        public bool completed;
        public bool[] list;
    }

    [System.Serializable]
    public struct LightPuzzle
    {
        public int showLights;
        [HideInInspector]
        public int[] scheme;
    }

    [Range(1, 40)]
    public int rounds = 1;
    public LightColors[] lights;
    public Color offLight;

    public float roundDelay = 2;
    public float waitTime = 0.5f;
    public float newWaitTime = 0.1f;
    public float speed = 1.5f;
    public float newspeed = 5f;
    public Transform lightsParent;
    public LayerMask mask;

    public LightPuzzle[] puzzle;

    private LightCompleted[] completed;
    private int lines;
    private bool canClick;
    private List<LightBulb> list;
    private LightBulb curBulb;
    private int curRound;
    private bool canGlow;

    private void Start()
    {
        curRound = 0;
        canClick = false;
        int j = 0;
        list = new List<LightBulb>();
        if (lightsParent != null)
        {
            lines = lightsParent.childCount;
            completed = new LightCompleted[lines];
            list = new List<LightBulb>();
            int k = 0;
            for (int i = 0; i < lines; i++)
            {
                //list [i] = new Collider2D[lightsParent.GetChild (i).childCount];
                j = 0;
                foreach (LightBulb l in lightsParent.GetChild(i).GetComponentsInChildren<LightBulb>())
                {
                    k = k % 2;
                    l.type = k;
                    l.row = i;
                    l.col = j;

                    list.Add(l);
                    j++;
                    k++;
                }
                //k++;
                completed[i].completed = false;
                completed[i].list = new bool[j];
            }

        }
        //print ("j=" + j);
        SetPuzzle(j);
        SetNewRound();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        CancelInvoke();
    }


    void RandomizeColors()
    {
        List<int> clist = new List<int>();
        for (int i = 0; i < lights.Length; i++)
        {
            clist.Add(i);
        }
        clist = clist.OrderBy(i => Random.value).ToList();
        for (int i = 0; i < lights.Length; i++)
        {
            lights[i].newcolor = lights[clist[i]].color;
        }
    }

    IEnumerator YChangeLights(int row = 0)
    {
        //print ("glow start");
        float count = 0;
        float curSpeed = speed;
        float curWait = waitTime;
        yield return new WaitForSeconds(1);
        while (count < 1)
        {
            //print (count);
            count = Mathf.Clamp(count + Time.deltaTime * curSpeed, 0, 1);
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].row == row)
                {
                    if (list[i].type == 0)
                    {
                        list[i].color = Color.Lerp(list[i].color, offLight, count);
                        list[i].lightUp = true;
                    }
                    else
                    {
                        list[i].lightUp = false;
                    }
                }
            }
            yield return null;
        }
        //////////
        while (canGlow)
        {
            count = 0;
            while (count < 1)
            {
                count = Mathf.Clamp(count + Time.deltaTime * curSpeed, 0, 1);
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].canGlow && list[i].row == row)
                    {
                        if (list[i].lightUp)
                        {
                            list[i].sprite.color = Color.Lerp(list[i].baseColor, offLight, count);
                            list[i].spriteGlow.color = Color.Lerp(list[i].baseColor, Color.clear, count);
                        }
                        else
                        {
                            list[i].sprite.color = Color.Lerp(list[i].baseColor, offLight, 1 - count);
                            list[i].spriteGlow.color = Color.Lerp(list[i].baseColor, Color.clear, 1 - count);
                        }
                    }
                }
                yield return null;
            }


            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].row == row)
                    list[i].lightUp = !list[i].lightUp;
            }
            yield return new WaitForSeconds(curWait);
            //for (int i = 0; i < completed.Length; i++) {
            if (completed[row].completed)
            {
                print(row + " speed");
                curWait = newWaitTime;
                curSpeed = newspeed;
                for (int j = 0; j < list.Count; j++)
                {
                    if (list[j].row == row)
                    {
                        if (list[j].canGlow == false)
                        {
                            list[j].baseColor = list[j].sprite.color;
                            if (list[j].lightUp)
                            {
                                list[j].sprite.color = offLight;
                                list[j].spriteGlow.color = Color.clear;
                            }
                            list[j].canGlow = true;
                        }
                    }
                }
                //}
            }
            else
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].row == row && list[i].canGlow == false && completed[row].list[list[i].col] == true)
                    {
                        list[i].baseColor = list[i].sprite.color;
                        list[i].canClick = false;
                        if (list[i].lightUp)
                        {
                            list[i].sprite.color = offLight;
                            list[i].spriteGlow.color = Color.clear;
                        }
                        list[i].canGlow = true;
                    }
                }
            }



        }/**/

        yield return null;
    }

    void RowCanGlow(int row)
    {
        for (int i = 0; i < list.Count; i++)
        {

        }
    }

    void SetPuzzle(int num)
    {
        int k = 0;
        int[] p1 = new int[] { 1, 2 };
        int[] p2 = new int[] { 1, 2, 3 };
        int[] p3 = new int[] { 1, 0, 2, 3 };
        for (int i = 0; i < puzzle.Length; i++)
        {
            puzzle[i].scheme = new int[num];

            if (i == 0)
            {
                for (int j = 0; j < num; j++)
                {
                    k = j % p1.Length;
                    puzzle[i].scheme[j] = p1[k];
                }
            }
            else
            if (i == 1)
            {
                for (int j = 0; j < num; j++)
                {
                    k = j % p2.Length;
                    puzzle[i].scheme[j] = p2[k];
                }
            }
            else if (i == 2)
            {
                for (int j = 0; j < num; j++)
                {
                    k = j % p3.Length;
                    puzzle[i].scheme[j] = p3[k];
                }
            }


        }
    }

    void DrawColors()
    {
        int row = 0, col = 0;
        for (int i = 0; i < list.Count; i++)
        {
            row = list[i].row;
            col = list[i].col;

            if (row < puzzle.Length && puzzle[row].scheme != null && col < puzzle[row].scheme.Length)
            {
                //print ("r=" + row + " c=" + col + " len=" + puzzle [row].showLights+" | "+(col <= puzzle [row].showLights));
                if (col < puzzle[row].showLights)
                {
                    list[i].hasGlow = true;

                    list[i].color = lights[puzzle[row].scheme[col]].newcolor;
                    list[i].num = puzzle[row].scheme[col];
                    completed[row].list[col] = true;
                    list[i].canGlow = true;
                    list[i].canClick = false;
                }
                else
                {
                    list[i].canClick = true;
                    list[i].hasGlow = false;
                    list[i].color = offLight;
                    list[i].num = lights.Length + 1;
                    completed[row].list[col] = false;
                    list[i].canGlow = false;
                }

                list[i].baseColor = list[i].color;
            }
        }
    }

    bool IsCompletedRow(int row)
    {
        bool comp = true;
        if (completed[row].completed == true)
        {
            return true;
        }
        for (int i = 0; i < completed[row].list.Length; i++)
        {
            comp = comp && completed[row].list[i];
        }
        completed[row].completed = comp;
        /*if (comp == true) {
			for (int i = 0; i < list.Count; i++) {
				if (list [i].row == row) {
					list [i].baseColor = list [i].color;
					list [i].color = list [i].color;
					list [i].canGlow = true;

				}
			}
		}/**/
        return comp;
    }

    bool IsCompleted()
    {
        bool comp = true;
        for (int i = 0; i < completed.Length; i++)
        {
            comp = comp && completed[i].completed;
        }
        return comp;
    }

    void SetNewRound()
    {
        canGlow = false;
        StopAllCoroutines();
        if (curRound >= rounds)
        {
            HandleGameOver();
        }
        else
        {
            for (int i = 0; i < completed.Length; i++)
            {
                completed[i].completed = false;
            }

            RandomizeColors();
            canGlow = true;
            DrawColors();
            canClick = true;
            for (int i = 0; i < completed.Length; i++)
            {
                StartCoroutine(YChangeLights(i));
            }
        }
    }

    void WaitNextRound()
    {
        curRound++;
        SetNewRound();
    }

    void LateUpdate()
    {

        if (canClick && Input.GetMouseButtonDown(0))
        {
            //print ("clicked");
            if ((curBulb = GetBulb()) != null)
            {
                if (curBulb.row < puzzle.Length && completed[curBulb.row].completed == false && curBulb.col >= puzzle[curBulb.row].showLights && curBulb.canClick)
                {
                    SoundManager.Instance.Play(SoundManager.Instance.bulbTapSound.sound, transform, transform.position);
                    curBulb.num = (curBulb.num + 1) % lights.Length;
                    if (curBulb.num >= lights.Length)
                    {
                        curBulb.color = offLight;
                        curBulb.hasGlow = false;

                    }
                    else
                    {
                        curBulb.color = lights[curBulb.num].newcolor;
                        curBulb.hasGlow = true;
                    }
                    if (curBulb.num == puzzle[curBulb.row].scheme[curBulb.col])
                    {
                        completed[curBulb.row].list[curBulb.col] = true;
                    }
                    else
                    {
                        completed[curBulb.row].list[curBulb.col] = false;
                    }
                    if (IsCompletedRow(curBulb.row))
                    {
                        SoundManager.Instance.Play(SoundManager.Instance.lineWin.sound, transform, transform.position);
                    }
                    if (IsCompleted())
                    {
                        SoundManager.Instance.Play(SoundManager.Instance.roundWin.sound, transform, transform.position);
                        canClick = false;
                        Invoke("WaitNextRound", roundDelay);
                    }

                }
            }
        }
    }

    LightBulb GetBulb()
    {
        print("clicked2");
        Vector3 pos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        Vector3 clickPos = Camera.main.ScreenToWorldPoint(pos);
        RaycastHit2D hit = Physics2D.Raycast(clickPos, Vector2.zero, 1000, mask);
        if (hit != null && hit.collider != null)
        {
            return hit.collider.transform.parent.GetComponent<LightBulb>();
        }
        return null;
    }


    private void HandleGameOver()
    {
        Debug.Log("GAME 15 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }


}