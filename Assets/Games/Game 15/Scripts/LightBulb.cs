using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBulb : MonoBehaviour {
	[HideInInspector]
	public bool canGlow;
	[HideInInspector]
	public bool canClick;

	private SpriteRenderer _sprite;
	private SpriteRenderer _spriteGlow;
	private Collider2D _col;

	public int type {
		get;
		set;
	}

	public int row {
		get;
		set;
	}

	public int num {
		get;set;
	}

	public int col {
		get;set;
	}

	public bool lightUp {
		get;
		set;
	}

	public Color baseColor {
		set;
		get;
	}

	public SpriteRenderer spriteGlow {
		get {
			if (_spriteGlow == null && transform.GetChild(1) != null) {
				_spriteGlow = transform.GetChild(1).GetComponent<SpriteRenderer> ();
			}
			return _spriteGlow;
		}
	}

	public bool hasGlow {
		get {
			if (spriteGlow != null)
				return spriteGlow.transform.gameObject.activeSelf;
			return false;
		}
		set { 
			if (spriteGlow != null) {
				spriteGlow.transform.gameObject.SetActive (value);
			}
		}
				
	}

	public SpriteRenderer sprite {
		get {
			if (_sprite == null) {
				_sprite = transform.GetChild(0).GetComponent<SpriteRenderer> ();
			}
			return _sprite;
		}
	}

	public Color color {
		set {
			//print(this.name+" color "+value);
			if (this.sprite != null) {
				this.sprite.color = value;
			}
			if (this.spriteGlow != null) {
				this.spriteGlow.color = value;
			}
		}
		get {
			if (this.sprite != null) {
				return this.sprite.color;
			} else if (this.spriteGlow != null) {
				return this.spriteGlow.color;
			}
			return Color.clear;
		}
	}

	public Collider2D hitbox {
		get {
			if (_col == null) {
				_col = GetComponentInChildren<Collider2D> ();
			}
			return _col;
		}
	}
}