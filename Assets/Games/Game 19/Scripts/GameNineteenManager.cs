using System.Collections;
using UnityEngine;

[System.Serializable]
public class Song
{
    public int[] songSequence;
}

public class GameNineteenManager : SingletonBase<GameNineteenManager>
{

    public float noteInterval;

    public int currentNote;
    public int feedbackIntervalCounter;
    public int feedbackInterval;

    public bool toHappy;
    public bool canSetMood = true;

    public GameObject note;
    public GameObject correctParticle;

    public Transform startPosition;

    public Color[] noteColors;
    public Note noteToPress;

    //0 is pause;
    public Song[] songs;
    public Song currentSong;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        //currentSong = songs[Random.Range(0, songs.Length)];
        //StartCoroutine(YNoteSpawner());
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }

    private void HandleMouseDown()
    {
        Ray ray = MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);
        if (hit)
        {
            if (!hit.collider.isTrigger)
            {
                hit.collider.GetComponent<PianoButton>().ProcessClick();
            }
        }
    }

    private IEnumerator YNoteSpawner()
    {
        float timer = noteInterval;
        int i = 0;

        while (i < currentSong.songSequence.Length)
        {
            timer += Time.deltaTime;
            if (timer >= noteInterval)
            {
                SpawnNote(currentSong.songSequence[i]);
                i++;
                timer = 0;
            }
            yield return null;
        }
        yield return new WaitUntil(() => startPosition.childCount == 0);
        HandleGameOver();

    }

    private void SpawnNote(int colorIndex)
    {
        GameObject spawnedNote = Instantiate(note, startPosition.position, note.transform.rotation, startPosition);
        spawnedNote.GetComponent<SpriteRenderer>().color = noteColors[colorIndex];
        spawnedNote.GetComponent<Note>().noteIndex = colorIndex;
        if (colorIndex == 0)
        {
            spawnedNote.GetComponent<Collider2D>().enabled = false;
        }
    }

    private void HandleGameOver()
    {
        Debug.Log("GAME 19 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    public void EnableHappyFeedback()
    {
        Debug.Log("HAPPY");
    }

    public void EnableSadFeedback()
    {
        Debug.Log("SAD");
    }

}