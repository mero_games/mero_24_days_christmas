﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    public float speed;

    public int noteIndex;

    private Vector2 startPos;

    private float amplitudeY = 1.5f;
    private float omegaY = 5.0f;
    private float index;

    private void OnEnable()
    {
        startPos = transform.position;
        //GetComponent<SpriteRenderer>().color = Color.black;
        //StartCoroutine(YMove());
    }

    public void Update()
    {
        index += Time.deltaTime;
        float x = transform.localPosition.x - speed * Time.deltaTime;
        float y = Mathf.Abs(amplitudeY * Mathf.Sin(omegaY * index));
        transform.localPosition = new Vector3(x, y, 0);
        if (transform.position.x < -11)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameNineteenManager.Instance.noteToPress = this;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        GameNineteenManager.Instance.feedbackIntervalCounter++;
        if (GameNineteenManager.Instance.feedbackIntervalCounter == GameNineteenManager.Instance.feedbackInterval)
        {
            GameNineteenManager.Instance.canSetMood = true;
            GameNineteenManager.Instance.feedbackIntervalCounter = 0;
            if (GameNineteenManager.Instance.toHappy)
            {
                GameNineteenManager.Instance.EnableHappyFeedback();
            }

            else
            {
                GameNineteenManager.Instance.EnableSadFeedback();
            }
        }
        GameNineteenManager.Instance.noteToPress = null;
    }
}
