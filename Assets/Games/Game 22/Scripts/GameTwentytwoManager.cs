using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTwentytwoManager : SingletonBase<GameTwentytwoManager>
{

    public Text numberOneText;
    public Text numberTwoText;
    public Text operatorText;

    public DraggableOrnament[] ornaments;
    public DraggableOrnament[] junkOrnaments;

    public Transform ornamentsHolder;
    public Transform targetSockets;

    public GameObject star;

    public int result;

    private int numberOne;
    private int numberTwo;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        GenerateNewSet();
    }

    public void GenerateNewSet()
    {
        DestroyChildren();
        if (ornamentsHolder.childCount == 0)
        {
            TurnTheLightsOn();
            return;
        }
        result = Random.Range(1, 32);
        while (ornaments[result - 1].beenUsed)
        {
            result = Random.Range(1, 32);
        }
        int tempType = Random.Range(0, 2);
        if (tempType == 0)
        {
            GenerateNumbersForAddition(result);
            operatorText.text = "+";
        }
        else
        {
            GenerateNumbersForExtraction(result);
            operatorText.text = "-";
        }
        numberOneText.text = numberOne + "";
        numberTwoText.text = numberTwo + "";
        int tempOne = Random.Range(1, 32);
        while (tempOne == result)
        {
            tempOne = Random.Range(1, 32);
        }
        int tempTwo = Random.Range(1, 32);
        while (tempTwo == result || tempTwo == tempOne)
        {
            tempTwo = Random.Range(1, 32);
        }
        int parentIndex = Random.Range(0, 3);
        int index = 0;
        ornaments[result - 1].transform.SetParent(targetSockets.GetChild(parentIndex));
        ornaments[result - 1].transform.position = targetSockets.GetChild(parentIndex).position;
        for (int i = 0; i < targetSockets.childCount; i++)
        {
            //Debug.Log(targetSockets.GetChild(i).childCount);
            if (targetSockets.GetChild(i).childCount == 0)
            {
                DraggableOrnament o = Instantiate(junkOrnaments[Random.Range(0, junkOrnaments.Length)], targetSockets.GetChild(i).position, Quaternion.identity, targetSockets.GetChild(i));
                if (index == 0)
                {
                    o.number = tempOne;
                    index++;
                }
                else
                {
                    o.number = tempTwo;
                }
                o.gameObject.SetActive(true);
            }
        }
        ornaments[result - 1].number = result;
        ornaments[result - 1].beenUsed = true;
        ornaments[result - 1].gameObject.SetActive(true);

    }

    private void TurnTheLightsOn()
    {
        for (int i = 0; i < ornaments.Length; i++)
        {
            ornaments[i].LightUp();
        }
        iTween.ScaleTo(star, iTween.Hash("scale", new Vector3(.5f, .5f, 1f),
                                         "time", .5f,
                                         "easetype", iTween.EaseType.linear,
                                         "oncomplete", "HandleGameOver",
                                         "oncompletetarget", gameObject));
    }

    private void HandleGameOver()
    {
        Debug.Log("GAME 22 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    private void GenerateNumbersForAddition(int result)
    {
        numberOne = Random.Range(0, result + 1);
        numberTwo = result - numberOne;
        while (numberOne % 10 + numberTwo % 10 > 10)
        {
            numberOne = Random.Range(0, result + 1);
            numberTwo = result - numberOne;
        }
    }

    private void GenerateNumbersForExtraction(int result)
    {
        numberOne = Random.Range(result, 32);
        numberTwo = numberOne - result;
        while (numberOne % 10 < numberTwo % 10)
        {
            numberOne = Random.Range(result, 32);
            numberTwo = numberOne - result;
        }
    }

    private void DestroyChildren()
    {
        for (int i = 0; i < targetSockets.childCount; i++)
        {
            if (targetSockets.GetChild(i).childCount > 0)
            {
                DestroyImmediate(targetSockets.GetChild(i).GetChild(0).gameObject);
            }
        }
    }

}