using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DraggableOrnament : MonoBehaviour
{

    public bool beenUsed;
    public int number;

    private Collider2D overlapCollider;

    private bool canClick;
    private SpriteRenderer light;

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        EventManager.OnMouseUp += HandleMouseUp;
        if (transform.childCount > 1)
        {
            light = transform.GetChild(1).GetComponent<SpriteRenderer>();
        }
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
        EventManager.OnMouseUp -= HandleMouseUp;
    }

    private void HandleMouseDown()
    {
        Ray ray = GameTwentytwoManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);

        if (hit)
        {
            if (hit.collider == GetComponent<Collider2D>())
            {
                StartCoroutine(YFollowMouse());
            }
        }

    }

    private IEnumerator YFollowMouse()
    {
        while (true)
        {
            Vector3 newPos = new Vector3(GameTwentytwoManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition).x,
                                         GameTwentytwoManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition).y, 0f);
            transform.position = newPos;
            yield return null;
        }
    }

    private void HandleMouseUp()
    {
        if (overlapCollider && number == GameTwentytwoManager.Instance.result)
        {
            SoundManager.Instance.Play(SoundManager.Instance.correctResult.sound, transform, transform.position);
            StopAllCoroutines();
            transform.position = overlapCollider.transform.position;
            transform.SetParent(overlapCollider.transform);
            overlapCollider.enabled = false;
            GetComponent<Collider2D>().enabled = false;
            GameTwentytwoManager.Instance.GenerateNewSet();
            this.enabled = false;
        }
        else
        {
            SoundManager.Instance.Play(SoundManager.Instance.incorrectResult.sound, transform, transform.position);
            StopAllCoroutines();
            MoveBack();
        }
    }

    private void OnEnable()
    {
        //number = Int32.Parse(transform.GetChild(0).GetChild(0).GetComponent<Text>().text);
        GetComponentInChildren<Text>().text = number + "";
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one,
                                               "time", .3f,
                                               "easetype", iTween.EaseType.linear,
                                               "oncomplete", "Done"));
    }

    private void MoveBack()
    {
        canClick = false;
        iTween.MoveTo(gameObject, iTween.Hash("position", transform.parent.position,
                                               "time", .25f,
                                               "easetype", iTween.EaseType.linear,
                                               "oncomplete", "Done"));
    }

    private void Done()
    {
        canClick = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "CorrectCollider")
        {
            overlapCollider = collision;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "CorrectCollider" && collision == overlapCollider)
        {
            overlapCollider = null;
        }
    }

    public void LightUp()
    {
        StartCoroutine(YLightUp());
    }

    private IEnumerator YLightUp()
    {
        float timer = 0f;
        while (timer <= 1f)
        {
            timer += Time.deltaTime;
            Color newColor = new Color(light.color.r, light.color.g, light.color.b, timer);
            light.color = newColor;
            yield return null;
        }
    }

}