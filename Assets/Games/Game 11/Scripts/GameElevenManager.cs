using System.Collections;
using UnityEngine;

public class GameElevenManager : SingletonBase<GameElevenManager>
{
    public bool isPlaying = true;

    public float riseAmount;
    public float riseSpeed;
    public float spawnRate;
    public float xThreshold;

    public int flakesToHit;

    public Transform snowPile;

    public GameObject waterSnow;

    public Sprite[] snowflakeSprites;

    private int snowHitCount;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        StartCoroutine(YWaterSnowSpawner());
    }

    public void IncrementSnowCount(int amount)
    {
        snowHitCount += amount;
        if (snowHitCount < 0)
        {
            snowHitCount = 0;
        }
        if (snowHitCount == flakesToHit)
        {
            RaiseSnowLevel();
            snowHitCount = 0;
        }
    }

    private void RaiseSnowLevel()
    {
        Vector3 newPos = new Vector3(snowPile.position.x, snowPile.position.y + riseAmount, snowPile.position.z);
        iTween.MoveTo(snowPile.gameObject, newPos, riseSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "CorrectCollider")
        {
            HandleGameOver();
        }
    }

    private IEnumerator YWaterSnowSpawner()
    {
        float timer = spawnRate;
        while (isPlaying)
        {
            timer += Time.deltaTime;
            if (timer >= spawnRate)
            {
                Vector3 spawnPos = new Vector3(Random.Range(-xThreshold, xThreshold), 7f, 0f);
                Instantiate(waterSnow, spawnPos, waterSnow.transform.rotation, transform);
                timer = 0;
            }
            yield return null;
        }
    }

    private void HandleGameOver()
    {
        isPlaying = false;
        Debug.Log("GAME 11 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }
}