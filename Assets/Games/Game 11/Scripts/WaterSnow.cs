using System.Collections;
using UnityEngine;

public class WaterSnow : MonoBehaviour
{

    public float snowFlakeSpeed;

    private bool isSnow;

    private GameObject waterDrop;
    private GameObject snowFlake;

    private Rigidbody2D rb2d;

    private void Start()
    {
        waterDrop = transform.GetChild(0).gameObject;
        snowFlake = transform.GetChild(1).gameObject;
        snowFlake.GetComponent<SpriteRenderer>().sprite = GameElevenManager.Instance.snowflakeSprites[Random.Range(0, GameElevenManager.Instance.snowflakeSprites.Length)];
        rb2d = GetComponent<Rigidbody2D>();
        EventManager.OnMouseDown += HandleMouseDown;
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }

    private void HandleMouseDown()
    {
        Ray ray = GameElevenManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);

        if (hit)
        {
            if (hit.collider == GetComponent<Collider2D>() && !isSnow)
            {
                SoundManager.Instance.Play(SoundManager.Instance.waterToSnowChange.sound, transform, transform.position);
                rb2d.isKinematic = true;
                rb2d.velocity = Vector3.zero;
                isSnow = true;
                waterDrop.SetActive(false);
                snowFlake.SetActive(true);
                GetComponent<CircleCollider2D>().radius /= 2;
                StartCoroutine(YSnowFall());
            }
        }
    }

    private IEnumerator YSnowFall()
    {
        int val = 1;
        switch (Random.Range(0, 2))
        {
            case 0:
                val = 1;
                break;
            case 1:
                val = -1;
                break;
        }
        float timer = 0f;
        while (transform.position.y > -7f)
        {
            timer += Time.deltaTime;
            transform.Translate(new Vector3(snowFlakeSpeed * val * Time.deltaTime / 2f, -snowFlakeSpeed * Time.deltaTime, 0f));
            if (timer >= .5f)
            {
                val *= -1;
                timer = 0f;
            }
            yield return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "CorrectCollider")
        {
            if (isSnow)
            {
                GameElevenManager.Instance.IncrementSnowCount(1);
                Destroy(gameObject, .2f);
            }
            else
            {
                GameElevenManager.Instance.IncrementSnowCount(-1);
                Destroy(gameObject, .2f);
            }
        }
    }

}