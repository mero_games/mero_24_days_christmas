// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Cg shader using discard" 
{
Properties {
    _MainTex ("Particle Texture", 2D) = "white" {}
}
   SubShader {
      Pass {
       Blend DstColor Zero
         Cull Off // turn off triangle culling, alternatives are:
         // Cull Back (or nothing): cull only back faces 
         // Cull Front : cull only front faces
 
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
 
             #include "UnityCG.cginc"
 
             sampler2D _MainTex;
 
             struct v2f {
                 float4 pos : SV_POSITION;
                 float4 weights : COLOR;
                 float2 uv1 : TEXCOORD0;
             };
 
             float4 _MainTex_ST;
 
             v2f vert (appdata_full v)
             {
                 v2f o;
                 o.weights = v.color;
                 o.pos = UnityObjectToClipPos (v.vertex);
                 o.uv1 = TRANSFORM_TEX (v.texcoord, _MainTex);
                 return o; 
             }
 
             fixed4 frag (v2f i) : SV_Target
             {
                 float4 c1 = tex2D (_MainTex, i.uv1);
                 return float4(1, 1, 1,c1.a);
             }
             ENDCG
           // return float4(0.0, 0, 0.0,1- input.a); // green
      }
   }
}