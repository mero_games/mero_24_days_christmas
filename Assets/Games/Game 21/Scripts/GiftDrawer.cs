using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class GiftDrawer : MonoBehaviour {

	//public float waitTime = 3;
	[HideInInspector]
	public Texture wrapSprite;
	[HideInInspector]
	public Texture giftSprite;
	//public Material material;
	public RawImage img;
	public RawImage img2;
	public Texture clear;
	public Material clearMaterial;
	[HideInInspector]

	private float time = 0;
	private Vector2 startPos;
	private Vector2 endPos;

	public bool canDraw {
		get;set;
	}


	int texwidth = 2000;
	int texheight = 2000;
	Vector2 size;
	float width, height, minbordx, minbordy, maxbordx, maxbordy;
	RenderTexture texture;
	//RawImage _img;
	Vector2 oldPos, pos;
	float count = 0; 
	Texture2D testTex;


	// Use this for initialization


	void Start () {
		startPos = Vector2.zero;
		endPos = new Vector2 (-1000, -1000);
		img.rectTransform.localScale = new Vector3(0.01f,0.01f);
		img2.rectTransform.localScale = new Vector3(0.01f,0.01f);
		//NewGame (wrapSprite, giftSprite);
	}

	public void NewGame(GameTwentyoneManager.GiftObject gift) {
		NewGame (gift.gift, gift.wrapper);
	}

	public void NewGame(Texture gift, Texture wrap) {
		
		this.wrapSprite = gift;
		this.giftSprite = wrap;
		CancelInvoke ();

		//Texture stext = img.material.GetTexture ("_Text1");
		//print (stext);

		img.texture = gift;
		img2.texture = wrap;
		texture = new RenderTexture(gift.width, gift.height, 16, RenderTextureFormat.ARGB32);

		texture.name = "giftboard";

		width = img.rectTransform.rect.width;
		height = img.rectTransform.rect.height;

		Graphics.SetRenderTarget(texture);
		GL.Clear (true, true, Color.clear);

		GL.PushMatrix ();
		GL.LoadPixelMatrix (0, img.texture.width, img.texture.height, 0);

		Graphics.DrawTexture (new Rect(0,0, img.texture.width,img.texture.height),img.texture);

		//Graphics.DrawTexture (new Rect(100,100,100,100),clear,clearMaterial);

		GL.PopMatrix ();
		Graphics.SetRenderTarget(null);

		img.texture=texture;
		StartCoroutine (YFadeIn ());

	}

	/*public void OnDrag(PointerEventData data) {
		
	}*/



	Vector2 GetMousePos() {
		Vector2 pos = Input.mousePosition;
		Vector2 point=Vector2.zero;
		RectTransformUtility.ScreenPointToLocalPointInRectangle (GetComponent<RawImage> ().rectTransform, pos, Camera.main, out point);

		Rect rect = GetComponent<RawImage> ().rectTransform.rect;
		point.x = point.x / rect.width;
		point.y = point.y / rect.height;
		point.x *= img.texture.width;
		point.y *= img.texture.height;

		point.x += img.texture.width/2;
		point.y += img.texture.height/2;
		point.y = img.texture.height - point.y;
		return point;
	}

	void ClearImage() {
		Graphics.SetRenderTarget(texture);
		GL.Clear (true, true, Color.clear);

		GL.PushMatrix ();
		GL.LoadPixelMatrix (0, img.texture.width, img.texture.height, 0);

	
		GL.PopMatrix ();
		Graphics.SetRenderTarget(null);
	}

	void TestResult() {
		CancelInvoke ();
		Color[] colors= null;
		int num = 1;
		bool cont = true;
		float res = 1;
		if (texture != null) {
			RenderTexture.active = texture;
			testTex = new Texture2D (texture.width, texture.height);
			testTex.ReadPixels (new Rect (0, 0, texture.width, texture.height), 0, 0);
			RenderTexture.active = null;
			num = 0;
			colors = testTex.GetPixels ();
			for (int i = 0; i < colors.Length; i++) {
				if (colors [i].a == 0) {
					num++;
				}
			}
		}
		if(colors != null && colors.Length != 0) {
			res = (float)num/(float)colors.Length;
			if(res > GameTwentyoneManager.Instance.imageComplete) {
				cont = false;
			}
		}
		if(cont) {
			Invoke ("TestResult", GameTwentyoneManager.Instance.checkTime);
		} else {
			canDraw = false;

			ClearImage();
			StartCoroutine (YFadeOut ());

		}
	}

	IEnumerator YFadeOut() {
		if(img == null || img2 == null) {
			yield break;
			canDraw = true;
		}
		Vector3 startPos = Vector3.one;
		Vector3 endPos = new Vector3 (0.01f, 0.01f, 1);
		float count = 0;
		while (count < 1) {
			count = Mathf.Clamp (count + Time.deltaTime * GameTwentyoneManager.Instance.fadeSpeed, 0, 1);
			img.rectTransform.localScale = Vector3.Lerp (startPos, endPos,count);
			img2.rectTransform.localScale = Vector3.Lerp (startPos, endPos,count);
			yield return null;
		}

		GameTwentyoneManager.Instance.NextGame();
	}

	IEnumerator YFadeIn() {
		if(img == null || img2 == null) {
			yield break;
			canDraw = true;
		}
		Vector3 startPos = new Vector3 (0.01f, 0.01f, 1);
		Vector3 endPos = Vector3.one;
		float count = 0;
		while (count < 1) {
			count = Mathf.Clamp (count + Time.deltaTime * GameTwentyoneManager.Instance.fadeSpeed, 0, 1);
			img.rectTransform.localScale = Vector3.Lerp (startPos, endPos,count);
			img2.rectTransform.localScale = Vector3.Lerp (startPos, endPos,count);
			yield return null;
		}
		img.rectTransform.localScale = Vector3.one;
		img2.rectTransform.localScale = Vector3.one;
		Invoke("TestResult", 5);

		canDraw = true;

	}

	void Update() {
		if (Input.GetMouseButton (0)) {
			if (time > 0.05f) {
				startPos = GetMousePos ();
				if (endPos.x == -1000 && endPos.y == -1000) {
					DrawLine (pos);
				} else {
					DrawLine (endPos, startPos);
				}
				endPos = startPos;
				time = 0;
			} else {
				time += Time.deltaTime;
			}
			//}
		

		}
	}

	/*void LateUpdate() {
		if (count > waitTime) {
			count = 0;
			if ( TestResult ()) {
				//canDraw = false;

			}
		} else {
			count += Time.deltaTime;
		}
	}*/


	void DrawLine(Vector2 start, Vector2 end, float scale = 100) {
		if (img == null || texture == null) {
			return;
		}
		Graphics.SetRenderTarget(texture);
		GL.PushMatrix ();
		GL.LoadPixelMatrix (0, img.texture.width, img.texture.height, 0);
		Vector2 pos;
		for (float i = 0; i <= 1; i += 0.05f) {
			pos = Vector2.Lerp (start, end, i);
			Graphics.DrawTexture (new Rect (pos.x, pos.y, GameTwentyoneManager.Instance.scale, GameTwentyoneManager.Instance.scale), clear, clearMaterial);
		}
		GL.PopMatrix ();
		Graphics.SetRenderTarget(null);
	}

	void DrawLine(Vector2 start, float scale =100) 
	{
		if (img == null || texture == null) {
			return;
		}
		
		Graphics.SetRenderTarget(texture);
		GL.PushMatrix ();
		GL.LoadPixelMatrix (0, img.texture.width, img.texture.height, 0);

		Graphics.DrawTexture (new Rect(start.x,start.y,GameTwentyoneManager.Instance.scale,GameTwentyoneManager.Instance.scale),clear,clearMaterial);

		GL.PopMatrix ();
		Graphics.SetRenderTarget(null);
	}


	/*private void DrawLine(Vector2 localStart, Vector2 localEnd, float scale, Color drawcol, bool smooth = false)
	{
		localStart.x /= width;
		localEnd.x /= width;

		localStart.y /= height;
		localEnd.y /= height;

		Graphics.SetRenderTarget(texture);
		GL.PushMatrix();
		GL.LoadOrtho();
		GL.Begin(GL.QUADS);
		drawMaterial.SetPass(0);
		GL.Color(drawcol);

		Vector2 direction = localEnd - localStart;
		Vector2 perpendicular = new Vector2(direction.y, -direction.x).normalized * scale;

		Vector3 ptOne = localStart + perpendicular;
		Vector3 ptTwo = localStart - perpendicular;
		Vector3 ptThree = localEnd - perpendicular;
		Vector3 ptFour = localEnd + perpendicular;

		GL.Vertex3(ptOne.x, ptOne.y, 0);
		GL.Vertex3(ptTwo.x, ptTwo.y, 0);
		GL.Vertex3(ptThree.x, ptThree.y, 0);
		GL.Vertex3(ptFour.x, ptFour.y, 0);

		if (smooth)
		{
			Color copy = drawcol;
			copy.a /= 3;
			GL.Color(copy);
			ptOne = localStart + perpendicular * 1.3f;
			ptTwo = localStart - perpendicular * 1.3f;
			ptThree = localEnd - perpendicular * 1.3f;
			ptFour = localEnd + perpendicular * 1.3f;
			GL.Vertex3(ptOne.x, ptOne.y, 0);
			GL.Vertex3(ptTwo.x, ptTwo.y, 0);
			GL.Vertex3(ptThree.x, ptThree.y, 0);
			GL.Vertex3(ptFour.x, ptFour.y, 0);
		}

		GL.End();
		GL.PopMatrix();
		Graphics.SetRenderTarget(null);
	}/**/
	

}