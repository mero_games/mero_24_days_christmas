using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTwentyoneManager : SingletonBase<GameTwentyoneManager>
{

    [System.Serializable]
    public struct GiftObject
    {
        public Texture gift;
        public Texture wrapper;
    }
    [Range(0f, 1f)]
    public float imageComplete = 0.99f;
    public int rounds = 2;
    public float fadeSpeed = 3;
    public float scale = 60;
    public float checkTime = 3;
    public List<GiftObject> gifts;
    public GiftDrawer giftDrawer;

    private List<GiftObject> _gifts;
    private int curRound = 0;

    private AudioSource drawLoopSound;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        _gifts = gifts;
        NextGame();
        EventManager.OnMouseDown += HandleMouseDown;
        EventManager.OnMouseUp += HandleMouseUp;
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
        EventManager.OnMouseUp -= HandleMouseUp;
    }

    private void HandleMouseDown()
    {
        drawLoopSound = SoundManager.Instance.Play(SoundManager.Instance.wrappingSoundLoop.sound, transform, transform.position, 1, 0, true);
    }

    private void HandleMouseUp()
    {
        Destroy(drawLoopSound.gameObject);
    }


    public void NextGame()
    {
        if (curRound != 0)
        {
            SoundManager.Instance.Play(SoundManager.Instance.goodMatch.sound, this.transform, this.transform.position, SoundManager.Instance.goodMatch.volume);
        }
        curRound++;
        GiftObject gift;
        if (curRound > rounds || _gifts.Count == 0)
        {
            HandleGameOver();
        }
        else
        {
            int i = Random.Range(0, _gifts.Count);
            gift = _gifts[i];
            _gifts.RemoveAt(i);
            giftDrawer.NewGame(gift);
        }
    }

    private void HandleGameOver()
    {
        giftDrawer.canDraw = false;
        Debug.Log("GAME 21 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }


}