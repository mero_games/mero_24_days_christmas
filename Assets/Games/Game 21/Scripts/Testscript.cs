using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.EventSystems;

public class Testscript : MonoBehaviour {

	public float waitTime = 3;
	public Sprite wrapSprite;
	public Sprite giftSprite;
	public Material material;
	int texwidth = 2000;
	int texheight = 2000;
	Vector2 size;
	float width, height, minbordx, minbordy, maxbordx, maxbordy;
	RenderTexture texture;
	RawImage img;
	Vector2 oldPos, pos;
	float count = 0; 
	Texture2D testTex;

	public Texture clear;
	public Material clearMaterial;

	void Start () {
		img = GetComponent<RawImage> ();
		oldPos = new Vector2 (-1, -1);


		texture = new RenderTexture(img.texture.width, img.texture.height, 16, RenderTextureFormat.ARGB32);

		texture.name = "giftboard";
		width = img.rectTransform.rect.width;
		height = img.rectTransform.rect.height;

		Graphics.SetRenderTarget(texture);
		GL.Clear (true, true, Color.clear);

		GL.PushMatrix ();
		GL.LoadPixelMatrix (0, img.texture.width, img.texture.height, 0);

		Graphics.DrawTexture (new Rect(0,0, img.texture.width,img.texture.height),img.texture);

		Graphics.DrawTexture (new Rect(100,100,100,100),clear,clearMaterial);

		GL.PopMatrix ();
		Graphics.SetRenderTarget(null);

		img.texture=texture;
		InvokeRepeating ("Test", 6, 3);
	}

	void Test() {
		RenderTexture.active = texture;
		testTex = new Texture2D (texture.width, texture.height);
		testTex.ReadPixels (new Rect (0, 0, texture.width, texture.height), 0, 0);
		RenderTexture.active = null;
		int num = 0;
		Color[] colors = testTex.GetPixels();
		for (int i = 0; i < colors.Length; i++) {
			if (colors [i].a == 0) {
				num++;
			}
		}
		print (((float)num / (float)colors.Length)+"%");
	}
		
	void Update() {
		if (Input.GetMouseButton (0)) 
		{
			

				//targetRectArea is the rectangle transform you want your
				//input to be inside, the above will return true if it is inside.
			Vector2 pos = Input.mousePosition;
			Vector2 point=Vector2.zero;
			RectTransformUtility.ScreenPointToLocalPointInRectangle (GetComponent<RawImage> ().rectTransform, pos, Camera.main, out point);

			Rect rect = GetComponent<RawImage> ().rectTransform.rect;
			point.x = point.x / rect.width;
			point.y = point.y / rect.height;
			point.x *= img.texture.width;
			point.y *= img.texture.height;

			point.x += img.texture.width/2;
			point.y += img.texture.height/2;
			point.y = img.texture.height - point.y;

			print (point);
				DrawLine (point);
				Debug.Log ("1");


		}
	}

	/*#region IPointerDownHandler implementation
	public void OnPointerDown (PointerEventData eventData)
	{

		if (RectTransformUtility.RectangleContainsScreenPoint (GetComponent<RawImage>().rectTransform, eventData.position, eventData.pressEventCamera)) 
		{
			Vector2 point=Vector2.zero;
			RectTransformUtility.ScreenPointToLocalPointInRectangle (GetComponent<RawImage> ().rectTransform, eventData.position, eventData.pressEventCamera, out point);

			Rect rect = GetComponent<RawImage> ().rectTransform.rect;
			point.x = point.x / rect.width;
			point.y = point.y / rect.height;
			point.x *= img.texture.width;
			point.y *= img.texture.height;

			point.x += img.texture.width/2;
			point.y += img.texture.height/2;
			point.y = img.texture.height - point.y;

			Debug.Log (point);
			DrawLine(point);
		}



	}
	#endregion*/

	Vector2 GetMousePos() {
		Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		return new Vector2 (pos.x , pos.y );
	}

	void DrawLine(Vector2 start, float scale =240) 
	{
		Graphics.SetRenderTarget(texture);
		GL.PushMatrix ();
		GL.LoadPixelMatrix (0, img.texture.width, img.texture.height, 0);

		Graphics.DrawTexture (new Rect(start.x,start.y,scale,scale),clear,clearMaterial);

		GL.PopMatrix ();
		Graphics.SetRenderTarget(null);
	}



	/*private void DrawLine(Vector2 localStart, Vector2 localEnd, float scale, Color drawcol, bool smooth = false)
	{
		localStart.x /= width;
		localEnd.x /= width;

		localStart.y /= height;
		localEnd.y /= height;

		Graphics.SetRenderTarget(texture);
		GL.PushMatrix();
		GL.LoadOrtho();
		GL.Begin(GL.QUADS);
		drawMaterial.SetPass(0);
		GL.Color(drawcol);

		Vector2 direction = localEnd - localStart;
		Vector2 perpendicular = new Vector2(direction.y, -direction.x).normalized * scale;

		Vector3 ptOne = localStart + perpendicular;
		Vector3 ptTwo = localStart - perpendicular;
		Vector3 ptThree = localEnd - perpendicular;
		Vector3 ptFour = localEnd + perpendicular;

		GL.Vertex3(ptOne.x, ptOne.y, 0);
		GL.Vertex3(ptTwo.x, ptTwo.y, 0);
		GL.Vertex3(ptThree.x, ptThree.y, 0);
		GL.Vertex3(ptFour.x, ptFour.y, 0);

		if (smooth)
		{
			Color copy = drawcol;
			copy.a /= 3;
			GL.Color(copy);
			ptOne = localStart + perpendicular * 1.3f;
			ptTwo = localStart - perpendicular * 1.3f;
			ptThree = localEnd - perpendicular * 1.3f;
			ptFour = localEnd + perpendicular * 1.3f;
			GL.Vertex3(ptOne.x, ptOne.y, 0);
			GL.Vertex3(ptTwo.x, ptTwo.y, 0);
			GL.Vertex3(ptThree.x, ptThree.y, 0);
			GL.Vertex3(ptFour.x, ptFour.y, 0);
		}

		GL.End();
		GL.PopMatrix();
		Graphics.SetRenderTarget(null);
	}/**/
	

}