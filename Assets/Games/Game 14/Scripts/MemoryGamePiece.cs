using GameDevToolkit.Visual;
using UnityEngine;

public class MemoryGamePiece : MonoBehaviour
{
    public ObjectSequence back;
    public ObjectSequence front;
}