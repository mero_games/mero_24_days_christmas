using System.Collections;
using System.Collections.Generic;
using GameDevToolkit.Events;
using DG.Tweening;
using GameDevToolkit.GameUtil;
using UnityEngine;
using GameDevToolkit.Common;

public class MemoryGame : MonoBehaviour
{

    public MemoryGamePiece[] pieces;
    //private bool opened = false; - this is for the warning
    //private int openCounter = 0;
    private MemoryGamePiece clicked1;
    private MemoryGamePiece clicked2;
    public bool canClick = true;
    private List<GameObject> cantClickAnyMore = new List<GameObject>();
    [ReadOnly] public int matchCount = 0;
    [ReadOnly] public int LevelNumber = 0;
    //private static List<WinDelegate> myListener = new List<WinDelegate>();


    public delegate void WinDelegate();
    public event WinDelegate DoneMemory;





    void Start()
    {
        List<int> cardPictureIndexes = new List<int>();
        if (pieces.Length % 2 != 0)
        {
            Debug.LogError("Number of cards on screen needs to be an even number smartass!!!");
        }
        Randomizer giveYourVariablesActualNames = new Randomizer(pieces[0].front.children);
        for (int i = 0; i < pieces.Length / 2; i++)
        {
            int randomNumber = giveYourVariablesActualNames.getRandom();
            cardPictureIndexes.Add(randomNumber);
            cardPictureIndexes.Add(randomNumber);
        }
        // Debug.Log(ArrayUtils.ArrayToString<int>(cardPictureIndexes.ToArray()));
        ArrayUtils.randomizeList<int>(ref cardPictureIndexes);
        //Debug.Log(ArrayUtils.ArrayToString<int>(cardPictureIndexes.ToArray()));
        for (int i = 0; i < pieces.Length; i++)
        {
            pieces[i].front.setCurrentChildIndex(cardPictureIndexes[i]);
            pieces[i].gameObject.AddComponent<MouseEventSystem>().MouseEvent += doClick;
        }
    }


    private void doClick(GameObject target, MouseEventType type)
    {
        if (type == MouseEventType.CLICK && canClick && !cantClickAnyMore.Contains(target))
        {
            MemoryGamePiece piece = target.GetComponent<MemoryGamePiece>();
            StartCoroutine(flipToFront(piece));
            if (clicked1 == null)
            {
                clicked1 = piece;
                target.GetComponent<BoxCollider2D>().enabled = false;
            }

            else
            {
                if (target != clicked1)
                {
                    canClick = false;
                    clicked2 = piece;
                }
            }
        }

    }
    private IEnumerator flipToFront(MemoryGamePiece clicked)
    {
        SoundManager.Instance.Play(SoundManager.Instance.flipSound.sound, transform, transform.position);
        clicked.back.transform.DOScaleX(0, 0.2f);
        yield return new WaitForSeconds(0.2f);
        clicked.front.transform.DOScale(1, 0.2f);
        yield return new WaitForSeconds(0.6f);
        if (clicked == clicked2)
            doCheck();
    }

    private IEnumerator flipToBack(MemoryGamePiece clicked, bool enableCanClickWhenDone)
    {
        SoundManager.Instance.Play(SoundManager.Instance.flipSound.sound, transform, transform.position);
        clicked.front.transform.DOScaleX(0, 0.2f);
        yield return new WaitForSeconds(0.2f);
        clicked.back.transform.DOScale(1, 0.2f);
        yield return new WaitForSeconds(0.5f);
        if (enableCanClickWhenDone)
            canClick = true;
    }

    private void doCheck()
    {
        for (int i = 0; i < pieces.Length; i++)
        {
            pieces[i].gameObject.GetComponent<BoxCollider2D>().enabled = true;
        }
        if (clicked1 != null && clicked2 != null)
        {
            if (clicked1.front.CurrentChildIndex == clicked2.front.CurrentChildIndex)
            {
                SoundManager.Instance.Play(SoundManager.Instance.goodMatch.sound, transform, transform.position);
                cantClickAnyMore.Add(clicked1.gameObject);
                cantClickAnyMore.Add(clicked2.gameObject);
                //Debug.Log("good click!");
                matchCount++;
                if (matchCount < pieces.Length / 2)
                {
                    canClick = true;
                }
                else
                {
                    if (DoneMemory != null)
                    {
                        DoneMemory();
                    }
                    for (int i = 0; i < pieces.Length; i++)
                        pieces[i].gameObject.GetOrAddComponent<MouseEventSystem>().MouseEvent += doClick;
                }
            }
            else
            {
                SoundManager.Instance.Play(SoundManager.Instance.badMatch.sound, transform, transform.position);
                StartCoroutine(flipToBack(clicked1, false));
                StartCoroutine(flipToBack(clicked2, true));
            }
            clicked1 = null;
            clicked2 = null;
        }
    }
}