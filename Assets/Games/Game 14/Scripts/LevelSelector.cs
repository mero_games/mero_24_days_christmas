using System.Collections;
using UnityEngine;

public class LevelSelector : MemoryGame
{

    public MemoryGame[] levels;
    public GameObject[] bgs;

    void Start()
    {
        foreach (MemoryGame lvls in levels)
        {
            lvls.DoneMemory += ChangeLevel;
        }
    }

    private void ChangeLevel()
    {
        LevelNumber++;
        StartCoroutine(ChangeTheLevel());
    }

    private IEnumerator ChangeTheLevel()
    {
        SoundManager.Instance.Play(SoundManager.Instance.gridLevelEnd.sound, transform, transform.position);
        yield return new WaitForSeconds(0.5f);

        if (LevelNumber == levels.Length)
        {
            levels[levels.Length - 1].gameObject.SetActive(false);
            HandleGameOver();
        }
        else
        {
            //bgs[LevelNumber - 1].gameObject.SetActive(false);
            //bgs[LevelNumber].gameObject.SetActive(true);
            levels[LevelNumber - 1].gameObject.SetActive(false);
            levels[LevelNumber].gameObject.SetActive(true);
        }
    }

    private void HandleGameOver()
    {
        Debug.Log("GAME 14 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }
}