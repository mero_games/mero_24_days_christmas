using System.Collections;
using UnityEngine;

public class GameThirteenManager : SingletonBase<GameThirteenManager>
{

    public int numberOfSetsToDo;
    public int setsDone;

    public Vector3 snowManSize;

    public GameObject exampleSnowMan;

    public SnowManMatch[] snowMen;

    public Sprite[] snowMenParts;

    public bool canClick;

    private Sprite originalHead;
    private Sprite originalBody;

    private int spriteOneCount = 3;
    private int spriteTwoCount = 3;


    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        GenerateNewSet();
    }

    public void GenerateNewSet()
    {
        StartCoroutine(YGenerateNewSet());
    }

    private IEnumerator YGenerateNewSet()
    {
        canClick = false;
        if (setsDone > 0)
        {
            yield return StartCoroutine(YResetSnowMen());
            if (setsDone == numberOfSetsToDo)
            {
                HandleGameOver();
                StopAllCoroutines();
                yield break;
            }
        }
        GenerateExampleSnowMan();
        SetupClickableSnowMen();
        GameObject correctSnowman = null;
        for (int i = 0; i < snowMen.Length; i++)
        {
            if (snowMen[i].IsCorrect)
            {
                correctSnowman = snowMen[i].gameObject;
                break;
            }
        }
        SoundManager.Instance.Play(SoundManager.Instance.snowmanAppearingSound.sound, transform, transform.position);
        GrowItem(exampleSnowMan, .4f);
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < snowMen.Length; i++)
        {
            SoundManager.Instance.Play(SoundManager.Instance.snowmanAppearingSound.sound, transform, transform.position);
            GrowItem(snowMen[i].gameObject, .4f);
            yield return new WaitForSeconds(.1f);
        }
        yield return new WaitForSeconds(.5f);
        canClick = true;
    }

    private void HandleGameOver()
    {
        Debug.Log("GAME 13 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    private IEnumerator YResetSnowMen()
    {
        GameObject correctSnowman = null;
        for (int i = 0; i < snowMen.Length; i++)
        {
            if (!snowMen[i].IsCorrect)
            {
                ShrinkItem(snowMen[i].gameObject, .4f);
            }
            else
            {
                correctSnowman = snowMen[i].gameObject;
            }
        }
        yield return new WaitForSeconds(1f);
        ShrinkItem(correctSnowman, .4f);
        ShrinkItem(exampleSnowMan, .4f);
        yield return new WaitForSeconds(.6f);
    }

    private void GenerateExampleSnowMan()
    {
        originalHead = snowMenParts[Random.Range(0, snowMenParts.Length)];
        exampleSnowMan.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = originalHead;
        Sprite tempBody = snowMenParts[Random.Range(0, snowMenParts.Length)];
        while (tempBody == originalHead)
        {
            tempBody = snowMenParts[Random.Range(0, snowMenParts.Length)];
        }
        originalBody = tempBody;
        exampleSnowMan.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = originalBody;

    }

    private void SetupClickableSnowMen()
    {
        for (int i = 0; i < snowMen.Length; i++)
        {
            snowMen[i].IsCorrect = false;
        }
        int temp = Random.Range(0, snowMen.Length);
        snowMen[temp].transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = originalHead;
        snowMen[temp].transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = originalBody;
        snowMen[temp].IsCorrect = true;
        for (int i = 0; i < snowMen.Length; i++)
        {
            if (!snowMen[i].IsCorrect)
            {
                SetupSnowman(snowMen[i].transform.GetChild(0).GetComponent<SpriteRenderer>(), snowMen[i].transform.GetChild(1).GetComponent<SpriteRenderer>());
            }
        }
    }

    private void SetupSnowman(SpriteRenderer head, SpriteRenderer body)
    {
        int tempSprite = Random.Range(0, 2);
        int tempBodyPart = Random.Range(0, 2);
        if (tempSprite == 0)
        {
            if (spriteOneCount > 0)
            {
                if (tempBodyPart == 0)
                {
                    head.sprite = originalHead;
                    spriteOneCount--;
                    int temp = Random.Range(0, snowMenParts.Length);
                    while (snowMenParts[temp] == originalHead || snowMenParts[temp] == originalBody)
                    {
                        temp = Random.Range(0, snowMenParts.Length);
                    }
                    body.sprite = snowMenParts[temp];
                }
                else
                {
                    body.sprite = originalHead;
                    spriteOneCount--;
                    int temp = Random.Range(0, snowMenParts.Length);
                    while (snowMenParts[temp] == originalHead || snowMenParts[temp] == originalBody)
                    {
                        temp = Random.Range(0, snowMenParts.Length);
                    }
                    head.sprite = snowMenParts[temp];
                }
            }
            else
            {
                if (tempBodyPart == 0)
                {
                    head.sprite = originalBody;
                    spriteTwoCount--;
                    int temp = Random.Range(0, snowMenParts.Length);
                    while (snowMenParts[temp] == originalHead || snowMenParts[temp] == originalBody)
                    {
                        temp = Random.Range(0, snowMenParts.Length);
                    }
                    body.sprite = snowMenParts[temp];
                }
                else
                {
                    body.sprite = originalBody;
                    spriteTwoCount--;
                    int temp = Random.Range(0, snowMenParts.Length);
                    while (snowMenParts[temp] == originalHead || snowMenParts[temp] == originalBody)
                    {
                        temp = Random.Range(0, snowMenParts.Length);
                    }
                    head.sprite = snowMenParts[temp];
                }
            }
        }
        else if (tempSprite == 1)
        {
            if (spriteTwoCount > 0)
            {
                if (tempBodyPart == 0)
                {
                    head.sprite = originalBody;
                    spriteTwoCount--;
                    int temp = Random.Range(0, snowMenParts.Length);
                    while (snowMenParts[temp] == originalHead || snowMenParts[temp] == originalBody)
                    {
                        temp = Random.Range(0, snowMenParts.Length);
                    }
                    body.sprite = snowMenParts[temp];
                }
                else
                {
                    body.sprite = originalBody;
                    spriteTwoCount--;
                    int temp = Random.Range(0, snowMenParts.Length);
                    while (snowMenParts[temp] == originalHead || snowMenParts[temp] == originalBody)
                    {
                        temp = Random.Range(0, snowMenParts.Length);
                    }
                    head.sprite = snowMenParts[temp];
                }
            }
            else
            {
                if (tempBodyPart == 0)
                {
                    head.sprite = originalHead;
                    spriteOneCount--;
                    int temp = Random.Range(0, snowMenParts.Length);
                    while (snowMenParts[temp] == originalHead || snowMenParts[temp] == originalBody)
                    {
                        temp = Random.Range(0, snowMenParts.Length);
                    }
                    body.sprite = snowMenParts[temp];
                }
                else
                {
                    body.sprite = originalHead;
                    spriteOneCount--;
                    int temp = Random.Range(0, snowMenParts.Length);
                    while (snowMenParts[temp] == originalHead || snowMenParts[temp] == originalBody)
                    {
                        temp = Random.Range(0, snowMenParts.Length);
                    }
                    head.sprite = snowMenParts[temp];
                }
            }
        }
    }

    public void ShrinkItem(GameObject targetObj, float shrinkTime)
    {
        iTween.ScaleTo(targetObj, Vector3.zero, shrinkTime);
    }

    public void GrowItem(GameObject targetObj, float growTime)
    {
        iTween.ScaleTo(targetObj, snowManSize, growTime);
    }

}