using UnityEngine;


public class SnowManMatch : MonoBehaviour
{

    public bool IsCorrect { get; set; }

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }

    private void HandleMouseDown()
    {
        if (GameThirteenManager.Instance.canClick)
        {
            Ray ray = GameThirteenManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);
            if (hit)
            {
                if (hit.collider == GetComponent<Collider2D>())
                {
                    if (IsCorrect)
                    {
                        SoundManager.Instance.Play(SoundManager.Instance.correctSnowman.sound, transform, transform.position);
                        GameThirteenManager.Instance.setsDone++;
                        GameThirteenManager.Instance.GenerateNewSet();
                    }
                    else
                    {
                        Debug.Log("WRONG");
                        Handheld.Vibrate();
                        iTween.ShakeScale(gameObject, new Vector3(.1f, .1f, 0), .2f);
                    }
                }
            }
        }
    }

}