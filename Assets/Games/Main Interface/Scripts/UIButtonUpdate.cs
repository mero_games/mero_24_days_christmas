using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonUpdate : UIButton
{

    private void Start()
    {
        base.OnClickDown += HandleMouseDown;
    }

    private void HandleMouseDown()
    {
#if UNITY_ANDROID
        Application.OpenURL("market://details?id=com.merogames.christmas");
#elif UNITY_IPHONE
        Application.OpenURL("itms-apps://itunes.apple.com/app/id1317796973?mt=8");
#endif
    }
}