using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTwelveManager : SingletonBase<GameTwelveManager>
{

    private AudioSource tickingSound;
    private AudioSource tickingSoundFast;

    public int itemsToFind;
    public int itemsFound;

    public Text textTimer;

    public float secondsToPlay;

    public Vector3 exampleSize;

    public SpriteRenderer[] itemSprites;
    public bool[] spriteUsed;

    public SpriteRenderer exampleItem;

    private bool canClick;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        spriteUsed = new bool[itemSprites.Length];
        EventManager.OnMouseDown += HandleMouseDown;
        StartCoroutine(YSetupItem());
        StartCoroutine(YGameCountDown());
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }


    private void HandleMouseDown()
    {
        if (canClick)
        {
            Ray ray = MainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);

            if (hit)
            {
                if (hit.collider.GetComponent<SpriteRenderer>().sprite == exampleItem.sprite)
                {
                    iTween.ScaleTo(hit.collider.gameObject, Vector3.zero, .5f);
                    SoundManager.Instance.Play(SoundManager.Instance.correctFound.sound, transform, transform.position);
                    CorrectChoice();
                }
                else
                {
                    SoundManager.Instance.Play(SoundManager.Instance.incorrectTap.sound, transform, transform.position);
                }
            }
        }
    }

    private void CorrectChoice()
    {
        itemsFound++;
        StartCoroutine(YSetupItem());
    }

    private IEnumerator HandleGameOver()
    {
        canClick = false;
        iTween.ScaleTo(exampleItem.gameObject, Vector3.zero, .5f);
        for (int i = 0; i < itemSprites.Length; i++)
        {
            if (!spriteUsed[i] || exampleItem.sprite == itemSprites[i].sprite)
            {
                iTween.ScaleTo(itemSprites[i].gameObject, Vector3.zero, .5f);
            }
        }
        yield return new WaitForSeconds(.5f);


        for (int i = 0; i < itemSprites.Length; i++)
        {
            if (spriteUsed[i] && itemSprites[i].sprite != exampleItem.sprite)
            {
                iTween.ScaleTo(itemSprites[i].gameObject, new Vector3(.6f, .6f, 1f), .5f);
            }
        }

        Debug.Log("GAME 12 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    private IEnumerator YSetupItem()
    {
        canClick = false;

        if (exampleItem.transform.localScale.x > .1f)
        {
            iTween.ScaleTo(exampleItem.gameObject, Vector3.zero, .5f);
            yield return new WaitForSeconds(.5f);
        }

        int temp = Random.Range(0, itemSprites.Length);

        while (spriteUsed[temp])
        {
            temp = Random.Range(0, itemSprites.Length);
        }

        exampleItem.sprite = itemSprites[temp].sprite;
        spriteUsed[temp] = true;

        iTween.ScaleTo(exampleItem.gameObject, exampleSize, .5f);
        yield return new WaitForSeconds(.5f);

        canClick = true;

    }

    private IEnumerator YGameCountDown()
    {
        float timer = secondsToPlay;
        tickingSound = SoundManager.Instance.Play(SoundManager.Instance.clockTicking.sound, transform, transform.position, 1, 0, true);
        bool fastStarted = false;

        while (timer > 0)
        {
            yield return null;
            timer -= Time.deltaTime;
            textTimer.text = Mathf.Ceil(timer) + "";
            int intSecond = (int)Mathf.Ceil(timer);
            if (intSecond == 5 && !fastStarted)
            {
                fastStarted = true;
                if (tickingSound)
                {
                    tickingSound.Stop();
                }
                tickingSoundFast = SoundManager.Instance.Play(SoundManager.Instance.clockTickingFast.sound, transform, transform.position, 1, 0, true);
            }
        }
        canClick = false;
        textTimer.text = 0 + "";
        if (tickingSoundFast)
        {
            tickingSoundFast.Stop();
        }
        StartCoroutine(HandleGameOver());
    }

}