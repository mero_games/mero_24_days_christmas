using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTwoManager : SingletonBase<GameTwoManager>
{

    public bool isPlaying;

    public GameObject snow;
    public GameObject[] elements;
    public Transform[] startPositions;
    public TransformGrid[] rows;

    public ParticleSystem digEffect;

    private List<GameObject> spawnedObjects = new List<GameObject>();
    private bool[] elementsUsed;
    private Collider2D lastClickedCollider;
    private int itemCounter;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        elementsUsed = new bool[elements.Length];
        EventManager.OnMouseDown += HandleMouseDown;
        StartCoroutine(YSetupItems());

    }

    private static void OnDestroy(GameTwoManager instance)
    {
        EventManager.OnMouseDown -= instance.HandleMouseDown;
    }

    private void HandleMouseDown()
    {
        if (isPlaying)
        {
            Instantiate(digEffect, MainCamera.ScreenToWorldPoint(Input.mousePosition), digEffect.transform.rotation, transform);
            Ray ray = MainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);

            if (lastClickedCollider == null || lastClickedCollider == hit.collider)
            {
                SoundManager.Instance.Play(SoundManager.Instance.digSound.sound, transform, transform.position);
                lastClickedCollider = hit.collider;
                if (hit)
                {
                    if (!hit.collider.GetComponent<ItemClickProcessor>().beenOpened)
                    {
                        hit.collider.GetComponent<ItemClickProcessor>().clickCounter++;
                        if (lastClickedCollider.GetComponent<ItemClickProcessor>().clickCounter >= lastClickedCollider.GetComponent<ItemClickProcessor>().clickThreshold)
                        {
                            ProcessClick(hit.transform);
                            lastClickedCollider.GetComponent<ItemClickProcessor>().ResetClick();
                        }
                    }
                    else { return; }
                }
            }
            else
            {
                if (lastClickedCollider)
                {
                    lastClickedCollider.GetComponent<ItemClickProcessor>().ResetClick();
                }
                lastClickedCollider = hit.collider;
            }
        }

    }


    private void ProcessClick(Transform trans)
    {
        if (trans.childCount == 0)
        {
            //slot is empty, spawn snow;
            Instantiate(snow, trans.position, Quaternion.identity, trans);
        }
        else if (trans.childCount == 1)
        {
            if (trans.GetChild(0).childCount == 0)
            {
                //slot isn't empty, spawn snow;
                Instantiate(snow, trans.position, Quaternion.identity, trans);
                trans.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            }
            if (trans.GetChild(0).childCount > 0)
            {
                //slot is empty, snow already spawned, do nothing;
                return;
            }
        }
        else if (trans.childCount > 1)
        {
            //slot isn't empty, but snow was already spawned, enlarge the hole
            itemCounter++;
            trans.GetComponent<Collider2D>().enabled = false;
            trans.GetChild(1).GetChild(1).gameObject.SetActive(false);
            trans.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Effect";
            trans.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 3;
            SoundManager.Instance.Play(SoundManager.Instance.itemFound.sound, transform, transform.position);
            if (itemCounter >= startPositions.Length)
            {
                HandleGameOver();
            }
        }
    }

    private void HandleGameOver()
    {
        isPlaying = false;
        EventManager.OnMouseDown -= HandleMouseDown;
        Debug.Log("GAME 2 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    private IEnumerator YSetupItems()
    {
        int temp = Random.Range(0, elements.Length);
        for (int i = 0; i < startPositions.Length; i++)
        {
            while (elementsUsed[temp])
            {
                temp = Random.Range(0, elements.Length);
            }
            elementsUsed[temp] = true;
            GameObject go = Instantiate(elements[temp], startPositions[i].position, Quaternion.identity, startPositions[i]);
        }
        yield return new WaitForSeconds(2f);
        for (int i = 0; i < startPositions.Length; i++)
        {
            GameObject objectToMove = startPositions[i].GetChild(0).gameObject;
            int tempRow = Random.Range(0, rows.Length);
            int tempCol = Random.Range(0, rows[tempRow].columns.Length);
            while (rows[tempRow].columns[tempCol].childCount > 0)
            {
                tempCol = Random.Range(0, rows[tempRow].columns.Length);
            }
            objectToMove.transform.SetParent(rows[tempRow].columns[tempCol]);
            iTween.MoveTo(objectToMove, objectToMove.transform.parent.position, .2f);
            iTween.ScaleTo(objectToMove, Vector3.zero, .35f);
            spawnedObjects.Add(objectToMove);
        }

        yield return new WaitForSeconds(.5f);
        for (int i = 0; i < spawnedObjects.Count; i++)
        {
            spawnedObjects[i].GetComponent<SpriteRenderer>().enabled = false;
            spawnedObjects[i].transform.localScale = Vector3.one;
        }
        isPlaying = true;
    }

}