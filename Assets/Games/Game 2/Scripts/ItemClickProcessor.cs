using UnityEngine;

public class ItemClickProcessor : MonoBehaviour
{
    public bool beenOpened;
    public int clickCounter;
    public int clickThreshold;

    private void Start()
    {
        clickThreshold = 1;
    }

    public void ResetClick()
    {
        clickCounter = 1;
    }
}