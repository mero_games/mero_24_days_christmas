using UnityEngine;

public class GiftSpawner : MonoBehaviour
{

    public GameObject prefab;
    public int spawnNum = -1;

    int curspawn = 0;
    GameObject temp;
    // Use this for initialization
    void Start()
    {

    }

    public void Respawn()
    {
        if (!HasObject())
        {
            SpawnObject();
        }
    }



    void SpawnObject()
    {
        if (prefab != null && (curspawn <= spawnNum || spawnNum == -1))
        {
            temp = Instantiate(prefab);
            temp.transform.parent = this.transform;
            temp.transform.localPosition = Vector3.zero;
            curspawn++;
        }
    }

    bool HasObject()
    {
        if (GetComponentInChildren<ClickObject>() == null)
        {
            return false;
        }
        return true;
    }

}