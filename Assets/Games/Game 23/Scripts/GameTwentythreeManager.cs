using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

public class GameTwentythreeManager : MonoBehaviour
{

    public Transform backgrounds;
    public Transform tree;
    public Transform text;
    public LayerMask mask;
    public LayerMask treeMask;
    public float swipeSpeed = 4;
    public ClickObject nextButton;
    public ClickObject shareButton;
    public ClickObject exitButton;
    public Transform gameLogo;
    public Transform treeBG;
    public Transform tutorial;

    private Vector3 fp;   //First touch position
    private Vector3 lp;   //Last touch position
    private float dragDistance;  //minimum distance for a swipe to be registered
    bool canSwipe;
    bool canDrag;
    bool canClick;
    SpriteRenderer[] bg;
    List<ClickObject> ornaments;
    ClickObject dragObj;
    ClickObject lastClick;
    GiftSpawner[] spawners;

    int curImage = 0;
    int curStage = 0;

    void Start()
    {
        canSwipe = false;
        bg = backgrounds.GetComponentsInChildren<SpriteRenderer>(true);
        ornaments = tree.GetComponentsInChildren<ClickObject>().ToList();
        shareButton.gameObject.SetActive(false);
        exitButton.gameObject.SetActive(false);
        gameLogo.gameObject.SetActive(false);
        int i = 0;
        bg[0].sortingOrder = 1;
        bg[0].transform.position = Vector3.zero;
        for (i = 1; i < bg.Length; i++)
        {
            bg[i].transform.position = new Vector3(25, 0);
            bg[i].sortingOrder = 2;
        }
        i = 0;
        while (true)
        {
            if (i >= ornaments.Count)
            {
                break;
            }
            if (ornaments[i].type != ClickObject.ClickType.DRAG)
            {
                ornaments.RemoveAt(i);
            }
            else
            {
                i++;
            }
        }
        spawners = tree.GetComponentsInChildren<GiftSpawner>(true);
        RespawnAll();
        dragDistance = Screen.height * 15 / 100; //dragDistance is 15% height of the screen
        canClick = true;
        //print ("swipe " + canSwipe);
        exitButton.gameObject.SetActive(false);
        nextButton.gameObject.SetActive(true);
        SetStage(curStage);
    }

    void RespawnAll()
    {
        for (int i = 0; i < spawners.Length; i++)
        {
            spawners[i].Respawn();
        }
    }

    void SetStage(int i)
    {
        switch (i)
        {
            case 0:
                canSwipe = true;
                canDrag = false;
                backgrounds.gameObject.SetActive(true);
                tree.gameObject.SetActive(false);
                text.gameObject.SetActive(false);
                break;
            case 1:
                canSwipe = false;
                canDrag = true;
                backgrounds.gameObject.SetActive(true);
                tree.gameObject.SetActive(true);
                text.gameObject.SetActive(false);
                break;
            case 2:
                canSwipe = false;
                canDrag = false;
                HideAll();
                shareButton.gameObject.SetActive(true);
                exitButton.gameObject.SetActive(true);
                ResortTree();
                backgrounds.gameObject.SetActive(true);
                tree.gameObject.SetActive(true);
                // text.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(false);
                shareButton.gameObject.SetActive(true);
                exitButton.gameObject.SetActive(true);
                break;
        }
    }

    void HideAll()
    {
        if (dragObj != null)
        {
            DestroyObject(dragObj.gameObject);
        }
        for (int i = 0; i < spawners.Length; i++)
        {
            spawners[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < bg.Length; i++)
        {
            if (i != curImage)
            {
                bg[i].gameObject.SetActive(false);
            }
        }
    }

    Vector3 GetMousePosition()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return new Vector3(pos.x, pos.y, 0);

    }

    IEnumerator YSwipe(bool left = false)
    {
        canClick = false;
        canSwipe = false;
        int nextImage = 0;
        Vector3 startPos = new Vector3(25, 0, 0);
        Vector3 endPos = Vector3.zero;

        if (left)
        {
            if (curImage - 1 < 0)
            {
                nextImage = bg.Length - curImage - 1;
            }
            else
            {
                nextImage = curImage - 1;
            }

        }
        else
        {
            if (curImage + 1 >= bg.Length)
            {
                nextImage = 0;
            }
            else
            {
                nextImage = curImage + 1;
            }
            startPos = startPos * -1;
        }
        bg[curImage].sortingOrder = 1;
        bg[nextImage].sortingOrder = 2;
        float count = 0;

        while (count < 1)
        {
            count = Mathf.Clamp(count + Time.deltaTime * swipeSpeed, 0, 1);
            bg[nextImage].transform.position = Vector3.Lerp(startPos, endPos, count);
            yield return null;
        }
        for (int i = 0; i < bg.Length; i++)
        {
            if (i == nextImage)
            {
                bg[i].sortingOrder = 1;
                bg[i].transform.position = Vector3.zero;
            }
            else
            {
                bg[i].transform.position = new Vector3(25, 0, 0);
                bg[i].sortingOrder = 2;
            }
        }
        curImage = nextImage;
        canSwipe = true;
        canClick = true;
        yield return null;
    }

    void NextStage()
    {
        curStage++;
        SetStage(curStage);
    }

    ClickObject GetClickable(Vector3 pos)
    {
        Vector3 position = new Vector3(pos.x, pos.y, -Camera.main.transform.position.z);
        RaycastHit2D hit = Physics2D.Raycast(position, Vector2.zero, 1000, mask);
        if (hit.collider != null)
        {
            //print ("col="+ hit.collider);
            return hit.collider.GetComponent<ClickObject>();
        }
        return null;
    }

    bool IsOnTree(Vector2 pos)
    {
        RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero, 100, treeMask);
        if (hit.collider != null)
        {
            return true;
        }
        return false;
    }

    void ResortTree()
    {
        ClickObject[] gifts = treeBG.GetComponentsInChildren<ClickObject>();

        for (int i = 0; i < gifts.Length; i++)
        {
            gifts[i].order = i + 7;
        }
    }

    void ExitScene()
    {
        canClick = false;
        canDrag = false;
        canSwipe = false;
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    void Share()
    {
        canClick = false;
        canDrag = false;
        canSwipe = false;
        shareButton.gameObject.SetActive(false);
        exitButton.gameObject.SetActive(false);
        gameLogo.gameObject.SetActive(true);
        text.gameObject.SetActive(true);
        tutorial.gameObject.SetActive(false);
        ShareScreenshot();
    }

    public void ShareScreenshot()
    {
        string screenShotPath = Application.persistentDataPath + "/" + "GiftCard";
        if (File.Exists(screenShotPath)) File.Delete(screenShotPath);

        Application.CaptureScreenshot("GiftCard");

        StartCoroutine(delayedShare(screenShotPath, ""));
    }

    //CaptureScreenshot runs asynchronously, so you'll need to either capture the screenshot early and wait a fixed time
    //for it to save, or set a unique image name and check if the file has been created yet before sharing.
    IEnumerator delayedShare(string screenShotPath, string text)
    {
        while (!File.Exists(screenShotPath))
        {
            yield return new WaitForSeconds(.05f);
        }

        NativeShare.Share(text, screenShotPath + ".png", "", "", "image/png", true, "");
        yield return new WaitForSeconds(2f);
        tutorial.gameObject.SetActive(true);
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            fp = Input.mousePosition;
            lp = Input.mousePosition;
            dragObj = GetClickable(GetMousePosition());
            if (dragObj != null && dragObj.type != ClickObject.ClickType.DRAG)
            {
                dragObj = null;
            }
            else
            {
                if (dragObj != null)
                {
                    dragObj.transform.parent = tree.transform;
                }
            }

        }
        if (Input.GetMouseButton(0))
        {
            //lp = GetMousePosition ();
            if (dragObj != null)
            {
                dragObj.transform.position = GetMousePosition();
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            lp = Input.mousePosition;

            //Check if drag distance is greater than 20% of the screen height

            if ((Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance) && canSwipe)
            {//It's a drag
             //check if the drag is vertical or horizontal
                if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
                {   //If the horizontal movement is greater than the vertical movement...
                    if ((lp.x > fp.x))
                    {  //If the movement was to the right)//Right swipe
                       //	print("swipe 1");
                        StartCoroutine(YSwipe(false));
                    }
                    else
                    {   //Left swipe
                        //	print("swipe 2");
                        StartCoroutine(YSwipe(true));
                    }
                }/* else {   //the vertical movement is greater than the horizontal movement
					if (lp.y > fp.y) {  //If the movement was up//Up swipe
						Debug.Log ("Up Swipe");
					} else {   //Down swipe
						Debug.Log ("Down Swipe");
					}
				}*/
            }

            if (canClick)
            {   //It's a tap as the drag distance is less than 20% of the screen height
                bool onClickObject = false;
                lastClick = GetClickable(GetMousePosition());
                if (lastClick != null)
                {
                    if (lastClick.type == ClickObject.ClickType.OKBUTTON)
                    {
                        onClickObject = true;
                        NextStage();
                    }
                    if (lastClick.type == ClickObject.ClickType.EXITBUTTON)
                    {
                        onClickObject = true;
                        ExitScene();
                    }
                    if (lastClick.type == ClickObject.ClickType.SHAREBUTTON)
                    {
                        onClickObject = true;
                        Share();
                    }

                }
                if (!onClickObject)
                {
                    if (dragObj != null && canDrag)
                    {
                        //print ("check tree " + dragObj);
                        if (IsOnTree(dragObj.transform.position))
                        {
                            dragObj.transform.parent = treeBG.transform;
                            dragObj.order = 20000;
                            ResortTree();
                            //	print ("on tree");
                        }
                        else
                        {
                            //	print ("not on tree");
                            DestroyObject(dragObj.gameObject);
                        }
                        RespawnAll();
                    }
                    else if (dragObj != null)
                    {
                        DestroyObject(dragObj.gameObject);
                    }
                }
            }
        }


    }
}