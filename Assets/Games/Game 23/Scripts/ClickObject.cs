using UnityEngine;
using UnityEngine.Rendering;

public class ClickObject : MonoBehaviour
{

    public enum ClickType { OKBUTTON, EXITBUTTON, SHAREBUTTON, DRAG }

    public SpriteRenderer sprite
    {
        get
        {
            if (_sprite == null)
            {
                _sprite = GetComponent<SpriteRenderer>();
            }
            return _sprite;
        }
    }

    public int order
    {
        get
        {
            if (_order == null)
            {
                _order = GetComponent<SortingGroup>();
                if (_order != null)
                {
                    return _order.sortingOrder;
                }
            }
            else
            {
                return _order.sortingOrder;
            }
            return 0;
        }
        set
        {
            if (_order == null)
            {
                _order = GetComponent<SortingGroup>();
                if (_order != null)
                {

                    _order.sortingOrder = value;
                }
            }
            else
            {
                _order.sortingOrder = value;
            }
        }
    }

    public ClickType type;
    public int num;

    private SortingGroup _order;
    private SpriteRenderer _sprite;


}