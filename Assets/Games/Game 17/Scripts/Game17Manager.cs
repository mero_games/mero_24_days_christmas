/*
 * Developer:  Barabas Tamas
 */

using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Game17Manager : SingletonBase<Game17Manager>
{
    [Header("Differences")]
    public List<DifferenceCollection> differenceCollections;
    public int remaining = 5;
    public bool started = false;

    [Header("Particle System")]
    public ParticleSystem particlePrefab;

    [Header("UI")]
    public Text remainingText;

    public bool CanFind
    {
        get
        {
            return remaining > 0;
        }
    }

    void Start()
    {
        int index = Random.Range(0, differenceCollections.Count);

        differenceCollections.ForEach((obj) =>
        {
            obj.Hide();
        });
        differenceCollections[index].Show();

        RefreshUI();
        started = true;
    }

    public void DiffrenceFound()
    {
        if (remaining > 0)
        {
            remaining--;
            RefreshUI();
        }
        if (remaining == 0)
        {
            Debug.Log("All diffrences found. Showing end screen");
            if (NavigationManager.Instance != null)
            {
                NavigationManager.Instance.InstantiateEndScreen(transform);
            }
        }
    }

    public void PlayEffect(Vector3 position)
    {
        Instantiate(
            particlePrefab,
            position,
            Quaternion.identity
        );
    }

    private void RefreshUI()
    {
        remainingText.text = "Remaining: " + remaining.ToString();
    }

}