/*
 * Developer:  Barabas Tamas
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Twin : MonoBehaviour
{
    public Difference twin;

    public void OnMouseUp()
    {
        twin.OnMouseUp();
    }

}