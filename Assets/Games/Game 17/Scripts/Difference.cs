/*
 * Developer:  Barabas Tamas
 */

using UnityEngine;

public class Difference : MonoBehaviour
{
    public enum StartType
    {
        SHOWN,
        HIDDEN,
        ROTATED
    }

    public enum OnClickType
    {
        HIDE, SHOW, ROTATE
    }

    public StartType startType;
    public OnClickType onClickType;
    public float rotateBy = 0;
    public bool selected = false;
    public bool found = false;

    bool isVisible;

    public bool IsVisible
    {
        get
        {
            return isVisible;
        }

        set
        {
            isVisible = value;
            GetComponentInChildren<SpriteRenderer>().enabled = value;
        }
    }

    private void OnEnable()
    {

    }

    public void Select()
    {
        selected = true;
        if (startType == StartType.HIDDEN)
        {
            IsVisible = false;
        }
        else if (startType == StartType.ROTATED)
        {
            transform.rotation = Quaternion.Euler(0, 0, rotateBy);
        }
    }

    public void OnMouseUp()
    {
        if (Game17Manager.Instance.CanFind && !found && selected)
        {
            found = true;
            Found();
        }
    }

    void Found()
    {
        Game17Manager.Instance.PlayEffect(transform.position);
        if (SoundManager.Instance != null) { SoundManager.Instance.Play(SoundManager.Instance.foundSound.sound, transform, transform.position); }
        Setup();
        Game17Manager.Instance.DiffrenceFound();
    }

    public void Setup()
    {
        transform.rotation = Quaternion.Euler(Vector3.zero);
        if (onClickType == OnClickType.HIDE)
        {
            IsVisible = false;
        }
        else if (onClickType == OnClickType.SHOW)
        {
            IsVisible = true;
        }
        else if (onClickType == OnClickType.ROTATE)
        {

        }
    }

    public void Reset()
    {
        IsVisible = startType != StartType.HIDDEN;
    }

}