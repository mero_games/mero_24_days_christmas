/*
 * Developer:  Barabas Tamas
 */

using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DifferenceCollection : MonoBehaviour
{
    public List<Difference> differences;

    void Awake()
    {
        differences = GetComponentsInChildren<Difference>().ToList();
        differences.Shuffle();
    }

    public void Show()
    {
        gameObject.SetActive(true);
        int index = 0;
        for (int i = 0; i < Game17Manager.Instance.remaining; i++)
        {
            index = Random.Range(0, differences.Count);
            while (differences[index].selected)
            {
                index = Random.Range(0, differences.Count);
            }
            differences[index].Select();
        }

        differences.ForEach((obj) =>
        {
            if (!obj.selected)
                obj.Setup();
        });
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

}