using UnityEngine;

public class Candy : MonoBehaviour
{

    public enum Type
    {
        Coal,
        Candy
    }

    public Type type;

    private void Start()
    {
        Destroy(gameObject, 5f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (type == Type.Candy)
        {
            SoundManager.Instance.Play(SoundManager.Instance.goodCollect.sound, transform.parent, transform.position);
            GameSixManager.Instance.candiesCollected++;
        }

        if (type == Type.Coal)
        {
            SoundManager.Instance.Play(SoundManager.Instance.badCollect.sound, transform.parent, transform.position);
            GameSixManager.Instance.candiesCollected--;
        }
        GameSixManager.Instance.BootBloopEffect();
        Destroy(gameObject);
        if (GameSixManager.Instance.candiesCollected == GameSixManager.Instance.candiesToCollect)
        {
            GameSixManager.Instance.HandleGameOver();
        }
    }
}