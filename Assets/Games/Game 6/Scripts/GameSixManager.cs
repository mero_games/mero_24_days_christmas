using System.Collections;
using UnityEngine;

public class GameSixManager : SingletonBase<GameSixManager>
{

    public bool isPlaying = true;

    public int candiesToCollect;
    public int candiesCollected;
    public int chanceToSpawnCoal;

    public float xSpawnLimit;
    public float candySpawnRate;
    public float candySpawnHeight;

    public Sprite[] candySprites;

    public Transform boot;

    public GameObject candy;
    public GameObject coal;

    private Coroutine bootCoroutine;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        EventManager.OnMouseUp += HandleMouseUp;
        StartCoroutine(YCandySpawner());
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
        EventManager.OnMouseUp -= HandleMouseUp;
    }

    private void HandleMouseDown()
    {
        bootCoroutine = StartCoroutine(YFollowMouse());
    }

    private void HandleMouseUp()
    {
        StopCoroutine(bootCoroutine);
    }

    private IEnumerator YFollowMouse()
    {
        while (boot)
        {
            boot.position = new Vector3(MainCamera.ScreenToWorldPoint(Input.mousePosition).x, boot.position.y, 0f);
            //iTween.MoveTo(boot.gameObject, new Vector3(MainCamera.ScreenToWorldPoint(Input.mousePosition).x, boot.position.y, 0f), .05f);
            yield return null;
        }
    }

    private IEnumerator YCandySpawner()
    {
        float timer = candySpawnRate;
        while (isPlaying)
        {
            timer += Time.deltaTime;
            if (timer >= candySpawnRate)
            {
                int temp = Random.Range(0, 100);
                if (temp < chanceToSpawnCoal)
                {
                    SpawnItem(coal, true);
                }
                else
                {
                    SpawnItem(candy);
                }
                timer = 0;
            }
            yield return null;
        }
    }

    private void SpawnItem(GameObject item, bool spawnCoal = false)
    {
        float spawnPosX = Random.Range(-xSpawnLimit, xSpawnLimit);
        Vector3 spawnPos = new Vector3(spawnPosX, candySpawnHeight, 0f);

        GameObject spawnedItem = Instantiate(item, spawnPos, item.transform.rotation, transform);

        if (!spawnCoal)
        {
            spawnedItem.GetComponent<SpriteRenderer>().sprite = candySprites[Random.Range(0, candySprites.Length)];
        }

    }


    public void HandleGameOver()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
        EventManager.OnMouseUp -= HandleMouseUp;
        StopAllCoroutines();
        isPlaying = false;
        Debug.Log("GAME 6 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    public void BootBloopEffect()
    {
        iTween.PunchScale(boot.gameObject, new Vector3(.1f, .1f, .1f), .6f);
    }

}