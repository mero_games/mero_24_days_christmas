using System.Collections;
using UnityEngine;

public class GameThreeManager : SingletonBase<GameThreeManager>
{

    private bool isPlaying = true;

    public int ringsToToss;
    public int landedRingsCounter;
    public float minSpawnRate;
    public float maxSpawnRate;

    public GameObject ring;
    public Vector3 ringSpawnPos;
    public float ringScale;

    public HatEnabler[] hats;

    private int numberOfStandingHats;
    private float standUpRate;
    private RingTossController ringScript;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        GenerateRing();
        StartCoroutine(YHatController());
    }

    public void GenerateRing()
    {
        if (isPlaying)
        {
            GameObject spawnedRingObj = Instantiate(ring, ringSpawnPos, ring.transform.rotation, transform);
            ringScript = spawnedRingObj.GetComponent<RingTossController>();
            ringScript.enabled = false;
            iTween.ScaleTo(spawnedRingObj, iTween.Hash("scale", Vector3.one * ringScale,
                                                       "time", .3f,
                                                       "oncomplete", "EnableRing",
                                                       "oncompletetarget", gameObject,
                                                       "easetype", iTween.EaseType.linear));
        }

    }

    private IEnumerator YHatController()
    {
        standUpRate = 1f;
        float timer = 0f;

        while (isPlaying && numberOfStandingHats < hats.Length)
        {
            timer += Time.deltaTime;
            if (timer >= standUpRate)
            {
                timer = 0;
                standUpRate = Random.Range(minSpawnRate, maxSpawnRate);
                int tempHat = Random.Range(0, hats.Length);
                while (hats[tempHat].isStanding)
                {
                    tempHat = Random.Range(0, hats.Length);
                }
                SoundManager.Instance.Play(SoundManager.Instance.hatRise.sound, transform, transform.position);
                hats[tempHat].EnableStanding();
                numberOfStandingHats++;
            }
            yield return null;
        }

    }

    public void HandleGameOver()
    {
        //TODO: Do some game over shit;
        Debug.Log("GAME 3 OVER");
        isPlaying = false;
        Destroy(transform.GetChild(transform.childCount - 1).gameObject);
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    private void EnableRing()
    {
        ringScript.enabled = true;
    }

}