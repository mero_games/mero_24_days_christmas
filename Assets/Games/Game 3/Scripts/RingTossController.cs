using System.Collections;
using UnityEngine;

public class RingTossController : MonoBehaviour
{
    private bool canToss = true;
    public float force;
    public SpriteRenderer topRing;
    public CircleCollider2D col1;
    public CircleCollider2D col2;

    private Rigidbody2D rb2d;
    private Vector3 startPos;
    private Vector3 endPos;
    private HatEnabler hat;
    private bool hasCollided;
    private bool toSleep;

    private PlatformEffector2D[] effectors;

    private void Start()
    {
        col1.enabled = false;
        col2.enabled = false;
        EventManager.OnMouseDown += HandleMouseDown;
        EventManager.OnMouseUp += HandleMouseUp;
        effectors = GetComponentsInChildren<PlatformEffector2D>(true);
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.isKinematic = true;
    }

    private void Update()
    {
        if (rb2d.velocity.y < 0)
        {
            rb2d.mass = 2f;
            topRing.sortingOrder = -1;
        }
        if (transform.position.y < -7)
        {
            Destroy(gameObject);
        }
    }

    private void HandleMouseDown()
    {
        if (canToss)
        {
            startPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        }
    }

    private void HandleMouseUp()
    {
        if (canToss && col1 && col2)
        {
            SoundManager.Instance.Play(SoundManager.Instance.ringThrow.sound, transform, transform.position);
            canToss = false;
            endPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            Vector3 directionVector = Vector3.ClampMagnitude(endPos - startPos, .4f);
            if (rb2d)
            {
                rb2d.isKinematic = false;
            }
            col1.enabled = true;
            col2.enabled = true;
            rb2d.AddForce(force * directionVector, ForceMode2D.Impulse);
            GameThreeManager.Instance.GenerateRing();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision != null)
        {
            if (rb2d.velocity.y <= 0 && !hasCollided && collision.gameObject.GetComponentInParent<Rigidbody2D>().isKinematic && rb2d)
            {
                toSleep = true;
                Debug.Log(collision.transform.parent);
                Collider2D tempCol = collision.collider;
                hat = collision.transform.parent.GetComponent<HatEnabler>();
                hasCollided = true;
                //rb2d.velocity = Vector2.zero;
                //StartCoroutine(YSlowRing());
                StartCoroutine(YSleepHat(collision));
                EventManager.OnMouseDown -= HandleMouseDown;
                EventManager.OnMouseUp -= HandleMouseUp;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        toSleep = false;
    }

    private void Nothing()
    {
    }

    private void RingStopped()
    {
        Debug.Log("WTF");
        foreach (var e in effectors)
        {
            e.enabled = true;
        }
        rb2d.isKinematic = true;
        rb2d.velocity = Vector2.zero;
        col1.enabled = false;
        col2.enabled = false;

        SoundManager.Instance.Play(SoundManager.Instance.correctThrow.sound, transform, transform.position);
        GameThreeManager.Instance.landedRingsCounter++;
        if (GameThreeManager.Instance.landedRingsCounter == GameThreeManager.Instance.hats.Length)
        {
            GameThreeManager.Instance.HandleGameOver();
        }
        hat.EnableSleeping();
        this.enabled = false;
    }

    private IEnumerator YSleepHat(Collision2D collision)
    {
        yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => toSleep);
        if (Mathf.Abs(collision.gameObject.transform.position.x - transform.position.x) < .7f)
        {
            iTween.ValueTo(gameObject, iTween.Hash("from", rb2d.velocity,
                                                   "to", Vector2.zero,
                                                   "time", .3f,
                                                   "oncomplete", "RingStopped",
                                                   "onupdate", "Nothing"));
        }
    }

}