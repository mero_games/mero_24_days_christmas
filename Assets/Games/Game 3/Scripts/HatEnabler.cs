﻿using UnityEngine;

public class HatEnabler : MonoBehaviour
{
    private GameObject sleepingState;
    private GameObject standingState;

    public bool isStanding;

    private void Start()
    {
        sleepingState = transform.GetChild(0).gameObject;
        standingState = transform.GetChild(1).gameObject;
    }

    public void EnableStanding()
    {
        sleepingState.SetActive(false);
        standingState.SetActive(true);
        isStanding = true;
    }

    public void EnableSleeping()
    {
        sleepingState.SetActive(true);
        standingState.SetActive(false);
    }

}
