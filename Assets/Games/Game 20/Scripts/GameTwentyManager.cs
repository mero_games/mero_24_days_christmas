using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTwentyManager : SingletonBase<GameTwentyManager>
{

    public bool canDraw;

    public GameObject pencil;

    public int wrongCollidersHit;
    public int correctCollidersLeft;

    public Transform shapes;
    public Transform drawingsHolder;
    public Transform correctColliders;

    private int shapeIndex;

    private GameObject iPencil;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        EventManager.OnMouseUp += HandleMouseUp;
        SetupShape();
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
        EventManager.OnMouseUp -= HandleMouseUp;
    }

    private void HandleMouseDown()
    {
        if (canDraw)
        {
            iPencil = Instantiate(pencil,
                                  new Vector3(MainCamera.ScreenToWorldPoint(Input.mousePosition).x,
                                              MainCamera.ScreenToWorldPoint(Input.mousePosition).y,
                                              0),
                                  Quaternion.identity,
                                  transform);
        }
    }

    private void HandleMouseUp()
    {
        canDraw = false;
        if (iPencil)
        {
            iPencil.GetComponent<DotPencil>().toMove = false;
            //if (iPencil.GetComponent<DotPencil>().drawLoopSound)
            //{
            //    iPencil.GetComponent<DotPencil>().drawLoopSound.Stop();
            //}
            iPencil.transform.SetParent(drawingsHolder);
            if (wrongCollidersHit > 0)
            {
                SoundManager.Instance.Play(SoundManager.Instance.incorrectDrawn.sound, transform, transform.position);
                DeleteDrawings();
                SetChildren(correctColliders, true);
                correctCollidersLeft = correctColliders.childCount;
                wrongCollidersHit = 0;
                canDraw = true;
            }

            if (wrongCollidersHit == 0 && correctCollidersLeft > 0)
            {
                canDraw = true;
            }

            if (wrongCollidersHit == 0 && correctCollidersLeft == 0)
            {
                //SoundManager.Instance.Play(SoundManager.Instance.correctDraw.sound, transform, transform.position);
                HandleShapeDrawn();
            }
        }
    }

    private void SetupShape()
    {
        if (shapeIndex < shapes.childCount)
        {
            for (int i = 0; i < shapes.childCount; i++)
            {
                shapes.GetChild(i).gameObject.SetActive(false);
            }
            shapes.GetChild(shapeIndex).gameObject.SetActive(true);
            correctColliders = shapes.GetChild(shapeIndex).GetChild(0);
            correctCollidersLeft = shapes.GetChild(shapeIndex).GetChild(0).childCount;
            canDraw = true;
        }
        else
        {
            HandleGameOver();
        }
    }

    private void HandleShapeDrawn()
    {
        SoundManager.Instance.Play(SoundManager.Instance.correctDrawn.sound, transform, transform.position);
        StartCoroutine(YDrawingDone());
    }

    private IEnumerator YDrawingDone()
    {
        DeleteDots();
        float alphaValue = 0f;
        while (alphaValue <= 1f)
        {
            Color newColor = new Color(shapes.GetChild(shapeIndex).GetComponent<SpriteRenderer>().color.r,
                                       shapes.GetChild(shapeIndex).GetComponent<SpriteRenderer>().color.g,
                                       shapes.GetChild(shapeIndex).GetComponent<SpriteRenderer>().color.b,
                                       alphaValue);
            shapes.GetChild(shapeIndex).GetComponent<SpriteRenderer>().color = newColor;
            alphaValue += Time.deltaTime;
            yield return null;
        }
        yield return new WaitForSeconds(2f);
        shapeIndex++;
        SetupShape();
    }

    private void DeleteDots()
    {
        for (int i = 0; i < correctColliders.childCount; i++)
        {
            correctColliders.GetChild(i).gameObject.SetActive(false);
        }
        DeleteDrawings();
    }

    private void DeleteDrawings()
    {
        while (drawingsHolder.childCount > 0)
        {
            DestroyImmediate(drawingsHolder.GetChild(0).gameObject);
        }
    }

    private void SetChildren(Transform trans, bool setTo)
    {
        foreach (var t in trans.GetComponentsInChildren<Transform>(true))
        {
            if (t != trans)
            {
                t.GetComponent<Collider2D>().enabled = setTo;
            }
        }
    }

    private void HandleGameOver()
    {
        Debug.Log("GAME 20 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

}