using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotPencil : MonoBehaviour
{

    //public AudioSource drawLoopSound;
    public bool toMove = true;

    private void Start()
    {
        //drawLoopSound = SoundManager.Instance.Play(SoundManager.Instance.drawingLoop.sound, transform, transform.position, 1, 0, true);
        GetComponent<TrailRenderer>().sortingLayerName = "Foreground 0";
        GetComponent<TrailRenderer>().sortingOrder = 1;
    }

    private void Update()
    {
        if (toMove)
        {
            transform.position = new Vector3(GameTwentyManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition).x,
                                             GameTwentyManager.Instance.MainCamera.ScreenToWorldPoint(Input.mousePosition).y,
                                             0f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "WrongCollider")
        {
            GameTwentyManager.Instance.wrongCollidersHit++;
        }
        if (collision.tag == "CorrectCollider")
        {
            GameTwentyManager.Instance.correctCollidersLeft--;
            collision.enabled = false;
        }
    }
}