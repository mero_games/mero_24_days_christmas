using UnityEngine;

public class AvatarCreator : MonoBehaviour
{
    public Collider2D faceCollider;
    public Collider2D eyeCollider;
    public Collider2D hairCollider;

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }

    private void OnDisable()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }

    private void HandleMouseDown()
    {
        Ray ray = GameNineManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);

        if (hit)
        {
            switch (hit.collider.tag)
            {
                case "Eyes":
                    SoundManager.Instance.Play(SoundManager.Instance.featureChange.sound, transform, transform.position);
                    ChangeEyes();
                    break;
                case "Face":
                    SoundManager.Instance.Play(SoundManager.Instance.featureChange.sound, transform, transform.position);
                    ChangeFace();
                    break;
                case "Hair":
                    SoundManager.Instance.Play(SoundManager.Instance.featureChange.sound, transform, transform.position);
                    ChangeHair();
                    break;
                default:
                    hit.collider.gameObject.SetActive(false);
                    faceCollider.enabled = false;
                    eyeCollider.enabled = false;
                    hairCollider.enabled = false;
                    GameNineManager.Instance.StartDanceSequence();
                    this.enabled = false;
                    break;
            }
        }
    }

    private void ChangeEyes()
    {
        GameNineManager.Instance.eye.sprite = GameNineManager.Instance.eyes[GameNineManager.Instance.eyeIndex];
        GameNineManager.Instance.eyeIndex++;
        if (GameNineManager.Instance.eyeIndex == GameNineManager.Instance.eyes.Length)
        {
            GameNineManager.Instance.eyeIndex = 0;
        }
    }

    private void ChangeFace()
    {
        GameNineManager.Instance.face.sprite = GameNineManager.Instance.faces[GameNineManager.Instance.faceIndex];
        GameNineManager.Instance.faceIndex++;
        if (GameNineManager.Instance.faceIndex == GameNineManager.Instance.faces.Length)
        {
            GameNineManager.Instance.faceIndex = 0;
        }

        GameNineManager.Instance.neck.sprite = GameNineManager.Instance.necks[GameNineManager.Instance.neckIndex];
        GameNineManager.Instance.neckIndex++;
        if (GameNineManager.Instance.neckIndex == GameNineManager.Instance.necks.Length)
        {
            GameNineManager.Instance.neckIndex = 0;
        }
    }

    private void ChangeHair()
    {
        GameNineManager.Instance.hair.sprite = GameNineManager.Instance.hairs[GameNineManager.Instance.hairIndex];
        GameNineManager.Instance.hairIndex++;
        if (GameNineManager.Instance.hairIndex == GameNineManager.Instance.hairs.Length)
        {
            GameNineManager.Instance.hairIndex = 0;
        }
    }

}