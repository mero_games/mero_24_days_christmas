using UnityEngine;

public class SongBubble : MonoBehaviour
{

    public float lifetime;

    public ParticleSystem particle;

    private Collider2D col;

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, .5f, .8f);
        col = GetComponent<Collider2D>();
        Vector3 targetPos = new Vector3(transform.position.x, -7f, transform.position.z);
        iTween.MoveTo(gameObject, iTween.Hash("position", targetPos,
                                              "time", lifetime,
                                              "easetype", iTween.EaseType.linear));
        Destroy(gameObject, lifetime);
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }

    private void HandleMouseDown()
    {
        Ray ray = GameNineManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);

        if (hit)
        {
            if (hit.collider == col)
            {
                SoundManager.Instance.Play(SoundManager.Instance.bubbleTap.sound, transform.parent, transform.position);
                Instantiate(particle, transform.position, particle.transform.rotation, transform.parent);
                GameNineManager.Instance.DoDanceMove();
                Destroy(gameObject);
            }
        }
    }

}