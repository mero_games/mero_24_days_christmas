using System.Collections;
using UnityEngine;

public class GameNineManager : SingletonBase<GameNineManager>
{
    public bool isPlaying = true;

    public int faceIndex = 1;
    public int neckIndex = 1;
    public int eyeIndex = 1;
    public int hairIndex = 1;

    public float songNoteSpawnInterval;
    public float elfShrinkTime;

    public Vector3 elfTargetScale;
    public Vector3 elfTargetPosition;

    public SongBubble songBubble;

    public GameObject elf;

    public string[] animationNames;

    public Sprite[] faces;
    public Sprite[] necks;
    public Sprite[] eyes;
    public Sprite[] hairs;

    public SpriteRenderer face;
    public SpriteRenderer neck;
    public SpriteRenderer eye;
    public SpriteRenderer hair;

    private Animator elfAnimator;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        elfAnimator = elf.GetComponent<Animator>();
    }

    public void StartDanceSequence()
    {
        StartCoroutine(YShrinkElf());
    }

    private IEnumerator YShrinkElf()
    {
        iTween.MoveTo(elf, elfTargetPosition, elfShrinkTime);
        iTween.ScaleTo(elf, elfTargetScale, elfShrinkTime);
        yield return new WaitForSeconds(elfShrinkTime);
        StartCoroutine(YDanceSequence());
    }

    private IEnumerator YDanceSequence()
    {
        elfAnimator.enabled = true;
        //AudioSource song = SoundManager.Instance.Play(SoundManager.Instance.danceSong.sound, transform, Vector3.zero, SoundManager.Instance.danceSong.volume);
        //song.gameObject.AddComponent<GetSongData>();
        float timer = songNoteSpawnInterval;
        float timer2 = 0f;

        while (timer2 <= 60f)
        {
            timer2 += Time.deltaTime;
            timer += Time.deltaTime;
            if (timer >= songNoteSpawnInterval)
            //if (song.GetComponent<GetSongData>().dbValue > -5)
            {
                //Debug.Log(song.GetComponent<GetSongData>().dbValue);
                timer = 0;
                SpawnNote();
                //yield return new WaitForSeconds(.5f);
            }
            yield return null;
        }
        HandleGameOver();
    }

    private void HandleGameOver()
    {
        isPlaying = false;
        elfAnimator.Play("Default");
        Debug.Log("GAME 9 OVER");
        NavigationManager.Instance.InstantiateEndScreen(transform);
    }

    private void SpawnNote()
    {
        Instantiate(songBubble, new Vector3(Random.Range(-6f, 6f), 7f, 0f), Quaternion.identity, transform);
    }

    public void DoDanceMove()
    {
        if (isPlaying)
        {
            elfAnimator.Play(animationNames[Random.Range(0, animationNames.Length)]);
        }
    }

}