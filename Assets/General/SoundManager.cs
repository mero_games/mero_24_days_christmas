using UnityEngine;

[System.Serializable]
public class SoundClip
{
    public AudioClip sound;
    public float volume;
}

public class SoundManager : DDManagerBase<SoundManager>
{

    #region SOUNDS
    [Header("Temp Sounds")]
    public SoundClip tempSound;

    [Space(5)]
    [Header("End Screen Sounds")]
    public SoundClip buttonAppear;
    public SoundClip candyAppear;
    public SoundClip candyDestroy;

    [Space(5)]
    [Header("Game 1")]
    public SoundClip correctChoice;
    public SoundClip incorrectChoice;
    public SoundClip centerLetterChange;

    [Space(5)]
    [Header("Game 2")]
    public SoundClip digSound;
    public SoundClip itemFound;

    [Space(5)]
    [Header("Game 3")]
    public SoundClip hatRise;
    public SoundClip ringThrow;
    public SoundClip correctThrow;

    [Space(5)]
    [Header("Game 4")]
    /// <summary>
    /// The drawing loop.
    /// </summary>
    public SoundClip drawingLoop; //LOOP
    public SoundClip correctDraw;
    public SoundClip drawDelete;

    [Space(5)]
    [Header("Game 5")]
    /// <summary>
    /// The cleaning sound loop.
    /// </summary>
    public SoundClip cleaningSoundLoop; //LOOP
    public SoundClip dirtCleaned;

    [Space(5)]
    [Header("Game 6")]
    public SoundClip goodCollect;
    public SoundClip badCollect;

    [Space(5)]
    [Header("Game 7")]
    public SoundClip snowmanRise;
    public SoundClip goodSnowballThrow;
    public SoundClip badSnowballThrow;
    public SoundClip snowballThrow;
    public SoundClip snowballSplash;

    [Space(5)]
    [Header("Game 8")]
    public SoundClip levelStartShake;
    public SoundClip correctPlacement;
    public SoundClip incorrectPlacement;
    public SoundClip levelFinished;

    [Space(5)]
    [Header("Game 9")]
    public SoundClip featureChange;
    public SoundClip bubbleTap;

    [Space(5)]
    [Header("Game 10")]
    /// <summary>
    /// The line drawing loop.
    /// </summary>
    public SoundClip lineDrawingLoop; //LOOP
    public SoundClip correctLine;
    public SoundClip incorrectLine;

    [Space(5)]
    [Header("Game 11")]
    public SoundClip waterToSnowChange;

    [Space(5)]
    [Header("Game 12")]
    public SoundClip clockTicking; //LOOP
    public SoundClip clockTickingFast; //LOOP
    public SoundClip correctFound;
    public SoundClip incorrectTap;

    [Space(5)]
    [Header("Game 13")]
    public SoundClip snowmanAppearingSound;
    public SoundClip correctSnowman;

    [Space(5)]
    [Header("Game 14")]
    public SoundClip flipSound;
    public SoundClip badMatch;
    public SoundClip goodMatch;
    public SoundClip gridLevelEnd;

    [Space(5)]
    [Header("Game 15")]
    public SoundClip bulbTapSound;
    public SoundClip lineWin;
    public SoundClip roundWin;

    [Space(5)]
    [Header("Game 16")]
    public SoundClip moveSound;
    public SoundClip fallSound;
    public SoundClip rotateSound;
    public SoundClip lineSound;

    [Space(5)]
    [Header("Game 17")]
    public SoundClip foundSound;

    [Space(5)]
    [Header("Game 18")]
    public SoundClip[] sounds;
    public SoundClip badSound;
    public SoundClip correctSound;

    [Space(5)]
    [Header("Game 19")]
    public SoundClip[] noteSounds;
    public SoundClip endHappyElf;

    [Space(5)]
    [Header("Game 20")]
    public SoundClip correctDrawn;
    public SoundClip incorrectDrawn;

    [Space(5)]
    [Header("Game 21")]
    public SoundClip wrappingSoundLoop;

    [Space(5)]
    [Header("Game 22")]
    public SoundClip correctResult;
    public SoundClip incorrectResult;

    [Space(5)]
    [Header("Game 24")]
    public SoundClip tapSound;
    public SoundClip giftSpawnSound; //correct
    public SoundClip hitSound; //bad

    #endregion

    protected override void Awake()
    {
        base.Awake();
    }

    private AudioSource GetNewAudioItem(Transform trans)
    {
        AudioSource audioItem = null;
        foreach (AudioSource audio in GetComponentsInChildren<AudioSource>())
        {
            if (!audio.isPlaying)
            {
                audioItem = audio;
            }
        }

        if (audioItem == null)
        {
            GameObject go = new GameObject();
            go.transform.SetParent(trans);
            go.AddComponent<AudioSource>();
            audioItem = go.GetComponent<AudioSource>();
        }

        return audioItem;

    }

    public AudioSource Play(AudioClip ac, Transform trans, Vector3 position, float volume = 1, ulong delay = 0, bool loop = false)
    {
        if (ac)
        {
            AudioSource audioItem = GetNewAudioItem(trans);
            audioItem.clip = ac;
            audioItem.volume = volume;
            audioItem.Play(delay);
            audioItem.loop = loop;
            audioItem.transform.position = position;
            audioItem.gameObject.AddComponent<DestroySound>();
            return audioItem;
        }
        return null;
    }
}