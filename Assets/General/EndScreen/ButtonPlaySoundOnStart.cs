using GameDevToolkit.Sound;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class ButtonPlaySoundOnStart : MonoBehaviour
{

    public AudioClip sound;
    public float myTime;

    private Color myColor;

    void Start()
    {
        myColor = GetComponent<SpriteRenderer>().color;
        if (DateTime.UtcNow.DayOfYear < NavigationManager.Instance.yearNumberStart + SceneManager.GetActiveScene().buildIndex && gameObject.name == "next")
        {
            GetComponent<SpriteRenderer>().color = new Color(myColor.r, myColor.g, myColor.b, .2f);
        }
        else
        {
            GetComponent<SpriteRenderer>().color = myColor;
        }
        //SoundManager.getInstance().addSound(sound, SoundManager.getInstance().getLayerByName(SoundManager.SFX));
        StartCoroutine(PlayTheSound());


    }

    private IEnumerator PlayTheSound()
    {
        yield return new WaitForSeconds(myTime);
        SoundManager.Instance.Play(SoundManager.Instance.buttonAppear.sound, transform, transform.position);
        //SoundManager.getInstance().getLayerByName(SoundManager.SFX).getSoundByName(sound.name).Play();
    }
}