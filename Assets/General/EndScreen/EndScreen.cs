using System.Collections;
using System.Collections.Generic;
using GameDevToolkit.Events;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameDevToolkit.GameUtil;
using GameDevToolkit.Sound;
using System;
//using System;

public class EndScreen : MonoBehaviour
{

    public GameObject[] spwanPoints;
    public GameObject star;
    public GameObject[] menuBtns;
    public GameObject starDestroyEffect;
    public Sprite[] candies;
    private GameObject tempStar;
    public AudioClip[] sound;
    private List<int> occupied = new List<int>();

    void Start()
    {
        //SoundManager.getInstance().addSounds(sound, SoundManager.getInstance().getLayerByName(SoundManager.SFX));
        for (int i = 0; i < menuBtns.Length; i++)
        {
            menuBtns[i].GetOrAddComponent<MouseEventSystem>().MouseEvent += menuDoClick;
        }
        InvokeRepeating("SpawnStars", 1.2f, UnityEngine.Random.Range(0f, 0.5f));
    }

    private void menuDoClick(GameObject target, MouseEventType type)
    {
        if (type == MouseEventType.CLICK)
        {
            if (target.name == "mainMenuBtn")
            {

                NavigationManager.Instance.LoadScene(NavigationManager.Instance.mainInterfaceBuildIndex);
            }
            if (target.name == "replayBtn")
            {
                //string curScene = SceneManager.GetActiveScene().name;
                //Nav.getInstance().LoadScene(curScene);
                NavigationManager.Instance.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
            }
            if (target.name == "next")
            {
                if (DateTime.UtcNow.DayOfYear >= NavigationManager.Instance.yearNumberStart + SceneManager.GetActiveScene().buildIndex)
                {
                    NavigationManager.Instance.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
            }

        }
    }
    private void SpawnStars()
    {
        tempStar = Instantiate(star);
        tempStar.GetComponent<SpriteRenderer>().sprite = candies[UnityEngine.Random.Range(0, candies.Length)];
        SoundManager.Instance.Play(SoundManager.Instance.candyAppear.sound, transform, transform.position);
        //SoundManager.getInstance().getLayerByName(SoundManager.SFX).getSoundByName(sound[0].name).Play();
        Randomizer getPoint = new Randomizer(spwanPoints);
        int point = getPoint.getRandom();
        if (occupied.Count != spwanPoints.Length)
        {
            while (occupied.Contains(point))
                point = getPoint.getRandom();
            occupied.Add(point);
        }
        else
        {
            CancelInvoke();
        }
        tempStar.transform.position = spwanPoints[point].transform.position;
        tempStar.AddComponent<MouseEventSystem>().MouseEvent += tapOnStar;
    }

    private void tapOnStar(GameObject target, MouseEventType type)
    {
        if (type == MouseEventType.CLICK)
        {
            Instantiate(starDestroyEffect, target.transform.position, starDestroyEffect.transform.rotation, transform);
            SoundManager.Instance.Play(SoundManager.Instance.candyDestroy.sound, transform, transform.position);
            //SoundManager.getInstance().getLayerByName(SoundManager.SFX).getSoundByName(sound[1].name).Play();
            Destroy(target);
        }
    }
}