﻿using UnityEngine;
using System.Collections;
using System.IO;
using SimpleJSON;
using System.Collections.Generic;

namespace GameDevToolkit.Data
{
    /// <summary>
    /// Should be used to write files to disk in a redudant fation. It will attempt to simply write the file to disk and verify it.
    /// If verification fails it will retry once per frame up to a maximum of 3.
    /// This class follows the singelton pattern. If no instance exists it will create a new GameObject and attach self to it.
    /// </summary>
    public class RedundantFileWriter : MonoBehaviour
    {
        class DelayedWriteReference
        {
            public Coroutine delay;
            public string path;
            public DelayedWriteReference(Coroutine delay, string path)
            {
                this.delay = delay;
                this.path = path;
            }
        }
        private List<DelayedWriteReference> writes = new List<DelayedWriteReference>();
        private static RedundantFileWriter instance;
        private const int MAX_RETRY_COUNT = 3;
        private void Awake()
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        /// <summary>
        /// Gets the current instance, or creates it if one does not exist.
        /// </summary>
        /// <returns></returns>
        public static RedundantFileWriter getInstance()
        {
            if (instance == null)
            {
                GameObject temp = new GameObject();
                temp.name = "REDUNDANT_FILE_WRITER";
                instance = temp.AddComponent<RedundantFileWriter>();
            }
            return instance;
        }
        /// <summary>
        /// Writes a file to disk
        /// </summary>
        /// <param name="path">Path to write to</param>
        /// <param name="contents">Contents of a file.</param>
        /// <param name="testJSON">Wether it should try to parse the written file as json when verifying if write was successfull</param>
        /// <param name="encrypted">Use this to specify wether file is encrypted(in case if testJSON set to true)</param>
        /// <param name="retryCount">Current retry count. this parameter should not be used when calling manualy.</param>
        public void WriteFile(string path, string contents, bool testJSON = false, bool encrypted = false, string encryptionKey = "", int retryCount = 0)
        {
            if (encrypted && string.IsNullOrEmpty(encryptionKey))
                throw (new System.Exception("Must specify encryption key for encrypted file!"));
            List<DelayedWriteReference> toRemove = new List<DelayedWriteReference>();
            foreach(DelayedWriteReference dRef in writes)
            {
                if (dRef.path == path)
                    toRemove.Add(dRef);
            }
            foreach (DelayedWriteReference dRef in toRemove)
            {
                StopCoroutine(dRef.delay);
                toRemove.Remove(dRef);
            }
                bool success = true;
            if (encrypted)
                contents = Crypto.Encrypt(contents, encryptionKey);
            try
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    writer.Write(contents);
                    writer.Flush();
                }
            }
            catch
            {
                success = false;
            }
            
            TextReader tr = null;
            string writtenData = "";
            try
            {
                using (tr = new StreamReader(path))
                {
                    writtenData = tr.ReadToEnd();
                }
            }
            catch
            {
                success = false;
            }
            if (testJSON && success)
            {
                try
                {
                    using (tr = new StreamReader(path))
                    {
                        if (encrypted)
                            JSON.Parse(Crypto.Decrypt(writtenData, encryptionKey));
                        else
                            JSON.Parse(writtenData);
                    }
                }
                catch
                {
                    if (tr != null)
                    {
                        tr.Close();
                    }                        
                    success = false;
                }
            }
            if(success)
            {
                if(writtenData!=contents)
                {
                    success = false;
                }
            }
            if(!success)
            {
                if(retryCount<=MAX_RETRY_COUNT)
                {
                    Debug.LogWarning("Write to: " + path + " failed temporaraly, will retry next frame.(retry count: " + retryCount + ")");
                    writes.Add(new DelayedWriteReference(StartCoroutine(DelayWrite(path, contents,encryptionKey, testJSON, encrypted, retryCount + 1)),path));
                    
                }
                else
                {
                    Debug.LogError("Write to: " + path + " failed permenantly!");
                }
            }
        }
        /// <summary>
        /// Wait one frame and try to call WriteFile again.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="contents"></param>
        /// <param name="testJSON"></param>
        /// <param name="encrypted"></param>
        /// <param name="retryCount"></param>
        /// <returns></returns>
        public IEnumerator DelayWrite(string path, string contents, string encryptionKey, bool testJSON = false, bool encrypted = false, int retryCount = 0)
        {
            yield return new WaitForEndOfFrame();
            WriteFile(path, contents, testJSON, encrypted, encryptionKey, retryCount);
        }
    }
}