using UnityEngine;
using System.IO;
using System;
using SimpleJSON;
namespace GameDevToolkit.Data
{
    /// <summary>
    /// used for storing data on disk
    /// </summary>
    public class JSONSaveFile : JSONNode
    {	
        /// <summary>
        /// Defines default values for any flags that are not already defined in saved file
        /// </summary>
    	public JSONClass DEFAULT_VALUES = (JSONClass)JSON.Parse ("{}");
        /// <summary>
        /// Reference to the current file as it was loaded to memory.
        /// </summary>
    	public JSONClass curSaveFile;
        /// <summary>
        /// Will be true if the file didnt exist and was created.
        /// </summary>
    	public bool newSaveFile = false;
        /// <summary>
        /// If set to true the treatment of undefined flags will be treated more strictly if default is not defined.
        /// </summary>
    	public bool isProtected = false;
        /// <summary>
        /// Wether file on disk should be encrypted.
        /// </summary>
        public bool encrypted = false;
    	
        #if !UNITY_ANDROID && !UNITY_IOS
        /// <summary>
        /// The name of the software as it will apear in My Documents, does not apply on android.
        /// </summary>
    	private string _softN;
        #endif
        /// <summary>
        /// File name to be used
        /// </summary>
    	private string fName;
        private string encryptionKey;
       
        /// <summary>
        /// Intialize a new isntance, reads file from disk if it exists, decrypts it, parses it. If any error occurs or file
        /// does not exist a new empty file will be created and newSaveFile will be set to true.
        /// </summary>
        /// <param name="softName">Name of software, will be used as folder name if applicable</param>
        /// <param name="fName">File name</param>
        /// <param name="encrypted"></param>
    	public JSONSaveFile(string softName, string fName,bool encrypted=false, string encryptionKey = "")
    	{
            if(encrypted && string.IsNullOrEmpty(encryptionKey))
                throw (new Exception("Must specify encryption key for encrypted file!"));
            this.encryptionKey = encryptionKey;
            this.encrypted = encrypted;
            #if !UNITY_ANDROID && !UNITY_IOS
    		_softN = softName;
            #endif
    		//_fN = fName;
            string nativeDir;
            string nativeFilePath;
            if (fName.EndsWith(".sav"))
                fName = fName.Replace(".sav", "");
            this.fName = fName;

#if UNITY_ANDROID || UNITY_IOS
            nativeDir=Application.persistentDataPath+"/";
            nativeFilePath =  Application.persistentDataPath+"/"+ fName + ".sav";
#else
            nativeDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/" + softName + "/";
            nativeFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/" + softName + "/"+ fName + ".sav";
            #endif
            bool exists = File.Exists(nativeFilePath);
    		if (!exists)
    		{
                CreateFile(nativeDir, nativeFilePath);
    		}else{
                try
                {
                    string result;
                    using (TextReader tr = new StreamReader(nativeFilePath))
                    {
                        result = tr.ReadToEnd();
                        if (encrypted)
                            curSaveFile = (JSONClass)JSON.Parse(Crypto.Decrypt(result, encryptionKey));
                        else
                            curSaveFile = (JSONClass)JSON.Parse(result);
                    }
                    newSaveFile = false;
                    if (result.Trim() == "")
                        throw (new IOException("Read error!"));
                }
                catch
                {
                    Debug.Log("Error while parsing save file, trying to fix by recreating file!");
                    CreateFile(nativeDir,nativeFilePath);
                    newSaveFile = true;
                }
    		}
    	}
        /// <summary>
        /// Creates a new file on disk.
        /// </summary>
        /// <param name="nativeDir"></param>
        /// <param name="nativeFilePath"></param>
        private void CreateFile(string nativeDir, string nativeFilePath)
        {
            if (!Directory.Exists(nativeDir))
                Directory.CreateDirectory(nativeDir);
            FileStream f = File.Create(nativeFilePath);
            f.Close();
            curSaveFile = (JSONClass)JSON.Parse("{}");
            newSaveFile = true;
        }
    	/// <summary>
        /// Writes current state to disk using <seealso cref="GameDevToolkit.Data.RedundantFileWriter"/>
        /// </summary>
    	public void flush()
    	{
            Debug.Log("Flushing");
            string nativeDir;
            string nativeFilePath;
            #if UNITY_ANDROID || UNITY_IOS
            nativeDir=Application.persistentDataPath+"/";
            nativeFilePath =  Application.persistentDataPath+"/"+ this.fName + ".sav";
            #else
            nativeDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/" + _softN + "/";
            nativeFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/" + _softN + "/"+ fName + ".sav";
            #endif
    		if (Directory.Exists(nativeDir))
    		{
    			if (File.Exists(nativeFilePath))
    			{
                    File.Delete(nativeFilePath);
    			}
    		}
    		else
    		{
    			Directory.CreateDirectory(nativeDir);
    		}
			Debug.Log ("Loading from:" + nativeDir);
    		Debug.Log(curSaveFile.ToString());
            string data= curSaveFile.ToString();
            RedundantFileWriter.getInstance().WriteFile(nativeFilePath, data, true, encrypted, encryptionKey);
    	}
    	/// <summary>
        /// Used to access/change data stored in currentSaveFile
        /// </summary>
        /// <param name="aKey"></param>
        /// <returns></returns>
    	public override JSONNode this[string aKey]
    	{
    		get
    		{
    			if (curSaveFile.m_Dict.ContainsKey(aKey)){
    				return curSaveFile.m_Dict[aKey];
    			}else{
    				if(DEFAULT_VALUES.m_Dict.ContainsKey(aKey)){
    					curSaveFile[aKey] = DEFAULT_VALUES.m_Dict[aKey];
    					return DEFAULT_VALUES.m_Dict[aKey];
    				}else{
    					if(isProtected){
    						throw(new Exception("Value "+aKey+" does not exist and was not declared in DEFAULT_VALUES"));
    					}else{
    						return new JSONLazyCreator(curSaveFile, aKey);
    					}
    				}
    			}
    		}
    		set
    		{
    			if (curSaveFile.m_Dict.ContainsKey(aKey)){
    				curSaveFile.m_Dict[aKey] = value;
    			}else{
    				curSaveFile.m_Dict.Add(aKey,value);
    			}
    		}
    	}
    }
}