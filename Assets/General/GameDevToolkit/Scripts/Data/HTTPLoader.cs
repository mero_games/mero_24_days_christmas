using UnityEngine;
using System.Collections;

namespace GameDevToolkit.Data
{
    /// <summary>
    /// Helper class for loading data from web servers. Uses WWW so should work on all platforms WWW works on.
    /// </summary>
    public class HTTPLoader
    {
        /// <summary>
        /// Represent the result of a request.
        /// </summary>
    	public class HTTPResult
    	{
            /// <summary>
            /// Content of the response.
            /// </summary>
    		public string content;
            /// <summary>
            /// Error reason if applicable.
            /// </summary>
    		public string error;
            /// <summary>
            /// Bytes of content.
            /// </summary>
            public byte[] bytes;
    		public HTTPResult (string _content, string _error, byte[] _bytes)
    		{
    			content = _content;
                bytes=_bytes;
    			error = _error;
    		}
    	}
        /// <summary>
        /// Number of bytes downloaded
        /// </summary>
        public int loadedBytes
        {
            get
            {
                if(loader!=null)
                {
                    return loader.bytesDownloaded;
                }
                return 0;
            }
        }
        /// <summary>
        /// If true will use a random get parameter to circumvent cahcing.
        /// </summary>
    	public static bool forceNoCache=false;
        /// <summary>
        /// Delegate used by the <seealso cref="GameDevToolkit.Data.HTTPLoader.LoadComplete"/>
        /// </summary>
        /// <param name="result"></param>
    	public delegate void LoadCompleteListener (HTTPResult result);
        /// <summary>
        /// Triggered when a request is completed.
        /// </summary>
    	public event LoadCompleteListener LoadComplete;
        /// <summary>
        /// Form data to be sent by current request
        /// </summary>
    	private WWWForm formData;
        /// <summary>
        /// URL for the current request
        /// </summary>
    	private string url;
        /// <summary>
        /// Underlying isntnace of UnityEngine.WWW
        /// </summary>
        private WWW loader;
        /// <summary>
        /// Returns the URL for the current request.
        /// </summary>
    	public string address
    	{
    		get{return url;}
    	}
        /// <summary>
        /// Creates an instance of the class to be used to request a response from a webserver.
        /// Parameters are self explanatory.
        /// </summary>
        /// <param name="address"></param>
    	public HTTPLoader(string address)
    	{
    		url=address;
    		if(forceNoCache)
    		{
                if (url.Contains("?"))
                    url += "&r=" + Random.Range(1, 10000);
                else
                    url += "?r=" + Random.Range(1, 10000);
            }
    	}
        /// <summary>
        /// Creates an instance of the class to be used to request a response from a webserver.
        /// Parameters are self explanatory.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="data"></param>
        public HTTPLoader(string address, WWWForm data)
    	{
    		url=address;
    		if(forceNoCache)
    		{
    			if(url.Contains("?"))
                    url += "&r=" + Random.Range(1, 10000);
                else
                    url += "?r=" + Random.Range(1, 10000);
            }
    		formData=data;
    	}
        /// <summary>
        /// Should be called from a subclass of MonoBehaviour to start the request.
        /// </summary>
        /// <returns></returns>
        public IEnumerator load()
    	{
    		//Network.useProxy=true;
    		//Network.proxyIP="192.168.3.24";
    		//Network.proxyPort=3128;
    		
    		if(formData!=null)			
    			loader=new WWW(url,formData);
    		else
    			loader=new WWW(url);
    		yield return loader;
    		//Debug.Log(url);
    		yield return new WaitForSeconds(0.5f);
    		if(LoadComplete!=null)
    		{
    			//Debug.Log("3");
    			//yield return dispatchEvent(loader);	
    			if(loader.error=="" || loader.error==null)
    				LoadComplete(new HTTPResult(loader.text, "",loader.bytes));
    			else
    				LoadComplete(new HTTPResult("",loader.error,loader.bytes));
    			//Debug.Log("4");
    		}
    	}
    }
}