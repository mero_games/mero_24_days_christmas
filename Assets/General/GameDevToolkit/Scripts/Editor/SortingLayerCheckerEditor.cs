﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
/// <summary>
/// Adds Menu items for helping with checking the sorting layers of all sprites in the current scene
/// </summary>
public class SortingLayerCheckerEditor : ScriptableObject
{
    [MenuItem("Tools/Check Layers")]
    static void CheckLayers()
    {
        SpriteRenderer[] spriteRenderers = Resources.FindObjectsOfTypeAll(typeof(SpriteRenderer)) as SpriteRenderer[];
        string defaultLayerObjectNames = "";
        foreach (SpriteRenderer spriteRenderer in spriteRenderers)
        {
            if (spriteRenderer.sortingLayerName == "default" || spriteRenderer.sortingLayerName == "Default")
            {
                if (spriteRenderer.sortingOrder == 0)
                    defaultLayerObjectNames += ((defaultLayerObjectNames == "") ? "" : ", ") + spriteRenderer.name;
            }
        }
        Debug.Log(defaultLayerObjectNames);
    }

    [MenuItem("Tools/Check Prefab Layers")]
    static void CheckPrefabLayers()
    {
        List<string> result = new List<string>();
        string path = @"Assets\Prefabs";
        string[] allPrefabs = Directory.GetFiles(path, "*.prefab", SearchOption.AllDirectories);
        foreach (string prefab in allPrefabs)
        {
            GameObject prefabGameObject = (GameObject)AssetDatabase.LoadAssetAtPath(prefab, typeof(GameObject));
            RecursiveCheckChildren(prefabGameObject.transform, prefab, result);
        }

        if (result.Count > 0)
            Debug.Log(string.Join(", ", result.ToArray()));
        else
            Debug.Log("All prefabs are ok");
    }

    private static void RecursiveCheckChildren(Transform parent, string prefabPath, List<string> result)
    {
        foreach (Transform child in parent)
        {
            SpriteRenderer childSpriteRenderer = child.GetComponent<SpriteRenderer>();

            if (childSpriteRenderer != null)
            {
                if (childSpriteRenderer.sortingLayerName == "default" || childSpriteRenderer.sortingLayerName == "Default")
                {
                    if (childSpriteRenderer.sortingOrder == 0)
                        result.Add(prefabPath + "." + childSpriteRenderer.name);
                }
            }
            RecursiveCheckChildren(child, prefabPath, result);
        }
    }
}