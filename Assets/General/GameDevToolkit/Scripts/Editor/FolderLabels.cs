using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
/// <summary>
/// Editor extension adding a menu item (Assets/Add path labels to children)
/// By adding an item to the Assets menu, it will also be avaialbe in the right click context menu.
/// When menu item is clicked the selected folder from the project panes detailed section(not the folder tree) will be used
/// to iterate through all of its sub-folders and files and labels will be added to all of them.
/// The label will be FolderName/SubFulderName/FileName.
/// </summary>
public class FolderLabels
{

    /// <summary>
    /// Logic
    /// </summary>           
	[MenuItem("Assets/Add path labels to children")]
	public static void addPathLabels()
	{
		if (Selection.activeObject is DefaultAsset) {
			string path=AssetDatabase.GetAssetPath(Selection.activeObject);
			//Debug.Log(path);
			if(Directory.Exists(path))
			{
				//Debug.Log("Found folder!");
				//string rootName=Selection.activeObject.name;
				//Debug.Log("Root name is: "+rootName);
				string[] allChildren=Directory.GetFiles(path,"*.*",SearchOption.AllDirectories);
				foreach(string actualChild in allChildren)
				{
					if(!actualChild.EndsWith(".meta"))
					{
						string child=actualChild.Replace("Assets/","");
						string childRootDir=child.Substring(0,child.LastIndexOf("\\"));
						//Debug.Log(child);
						//Debug.Log(childRootDir);
						string[] labelElements=childRootDir.Split('\\');
						string[] labels=new string[labelElements.Length];
						for(int i=0; i<labelElements.Length; i++)
						{
							string[] tempLabels=new string[i+1];
							System.Array.Copy(labelElements,0,tempLabels,0,i+1);
							labels[i]=string.Join("\\",tempLabels);
							//Debug.Log(labels[i]);
						}
						AssetDatabase.SetLabels(AssetDatabase.LoadAssetAtPath(actualChild,typeof(Object)),labels);
					}
				}
			}
		}
		/*foreach (Object obj in Selection.activeObject) {
			Debug.Log(obj);
		}*/
	}
}