using UnityEngine;
using System.Collections;
using UnityEditor;
/// <summary>
/// Adds Menu items for helping with polygon colliders
/// </summary>
public class PolyEditor
{
    /// <summary>
    /// Adds a menu item called Polygon tools/Add Path.
    /// This will try to add a square shaped polygon path to the currently selected gameobjects Polygon Collider.
    /// </summary>
    [MenuItem("Polygon tools/Add Path")]
    public static void addPath()
    {
        PolygonCollider2D poly = Selection.activeGameObject.GetComponent<PolygonCollider2D>();
        if (poly == null)
            return;
        poly.pathCount++;
        poly.SetPath(poly.pathCount-1, new Vector2[]{new Vector2(0f,0f),new Vector2(1f,0f),new Vector2(1f,1f),new Vector2(0f,1f)});
    }
    /// <summary>
    /// Adds menu item called Polygon tools/Remove all paths.
    /// This will remove all paths from the selected gameobjects Polygon Collider
    /// </summary>
    [MenuItem("Polygon tools/Remove all paths")]
    public static void removePath()
    {
        PolygonCollider2D poly = Selection.activeGameObject.GetComponent<PolygonCollider2D>();
        if (poly == null)
            return;
        while(poly.pathCount>0)
            poly.pathCount--;
    }
}