using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.Collections.Generic;

public class Vector3ArrayGenerator
{
    /// <summary>
    /// Adds menu item called Tools/Generate Vector 3 Array.
    /// Once selected it will itareate through all the children of the selected gameObject and generate a Vector3 array from there positions.
    /// The resulting array will be placed in Console
    /// </summary>
	[MenuItem ("Tools/Generate Vector 3 Array")]
	public static void genVector3Array()
	{
		GameObject[] selected = Selection.gameObjects;
		if (selected != null) {
			if(selected.Length!=0)
			{
				List<string> vectors= new List<string>();
				Array.Sort<GameObject>(selected,sortBySiblingIndex);
				for(int i =0; i<selected.Length; i++)
				{
					vectors.Add("new Vector3("+selected[i].transform.position.x+"f, "+selected[i].transform.position.y+"f, "+selected[i].transform.position.z+"f)");
				}
				Debug.Log(string.Join(", ",vectors.ToArray()));
			}
		}
	}
    /// <summary>
    /// Used to place gameObjects in the specific order they are present in the hierarchy.
    /// </summary>
    /// <param name="t1"></param>
    /// <param name="t2"></param>
    /// <returns></returns>
	private static int sortBySiblingIndex(GameObject t1, GameObject t2)
	{
		if (t1.transform.GetSiblingIndex() == t2.transform.GetSiblingIndex())
			return 0;
		if (t1.transform.GetSiblingIndex() < t2.transform.GetSiblingIndex())
			return -1;
		else
			return 1;
	}
}