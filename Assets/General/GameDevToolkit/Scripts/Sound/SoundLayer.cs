using System.Collections.Generic;
using UnityEngine;
namespace GameDevToolkit.Sound
{
    [System.Serializable]
    public class SoundLayer
    {
        public delegate void VolumeDelegate(float newVolume);
        public event VolumeDelegate VolumeChangedEvent;
        [SerializeField]
        private string _name;
        /// <summary>
        /// Unique name of the layer
        /// </summary>
        public string Name
        {
            get { return _name;}
        }
        [SerializeField]
        private float _volume=1f;
        /// <summary>
        /// Determines if layer and its sounds are tracked by the current instace of SoundManager
        /// </summary>
        internal bool isActive
        {
            get
            {
                return SoundManager.getInstance().getLayers().Contains(this);
            }
        }
        /// <summary>
        /// Layer volume level
        /// </summary>
        public float Volume
        {
			get{ return _volume;}
            set
            {
				_volume=value;
				if(_volume<0f)
					_volume=0f;
				if(_volume>1f)
					_volume=1f;
                if(VolumeChangedEvent!=null)
					VolumeChangedEvent(_volume);
            }           
        }

        /// <summary>
        /// List of SoundClips on this layer.
        /// </summary>
        [SerializeField]
        internal List<SoundClip> sounds = new List<SoundClip>();
        public SoundLayer(string name): this(name,null){}

        public SoundLayer(string name, List<SoundClip> soundsList)
        {
            _name = name;
            if (soundsList == null)
                sounds = new List<SoundClip>();
            else
                sounds = new List<SoundClip>(soundsList);
        }
        /// <summary>
        /// Adds sound to layer.
        /// </summary>
        /// <param name="sound"></param>
        public void addSound(SoundClip sound)
        {
            sounds.Add(sound);
            sound.ParentLayer = this;
        }
        /// <summary>
        /// Returns a clone of the internal SoundClip list.
        /// </summary>
        /// <returns></returns>
        public List<SoundClip> getSoundList()
        {
            return new List<SoundClip>(sounds);
        }
        /// <summary>
        /// Gets a sound clip by name, if not found it will return null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public SoundClip getSoundByName(string name)
        {
            foreach (SoundClip sound in sounds)
            {
                if(sound.Name==name)
                    return sound;
            }
            return null;
        }
        /// <summary>
        /// Checks if given SoundClip is on this layer.
        /// </summary>
        /// <param name="sound"></param>
        /// <returns></returns>
        public bool hasSound(SoundClip sound)
        {
            foreach (SoundClip mySound in sounds)
            {
                if(mySound==sound)
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Checks if layer has a SoundClip with given name. Usess getSoundByName
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool hasSound(string name)
        {
            SoundClip sound = getSoundByName(name);
            if (sound == null)
                return false;
            else
                return hasSound(sound);
        }
    }
}