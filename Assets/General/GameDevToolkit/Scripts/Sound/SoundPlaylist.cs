using UnityEngine;
using System.Collections;
namespace GameDevToolkit.Sound
{
    public class SoundPlaylist
    {
        public delegate void PlaylistDelegate(SoundPlaylist playlist);
        public event PlaylistDelegate PlaylistComplete;
        public event PlaylistDelegate PlaylistJump;
        private bool _isPlaying=false;
        public bool isPlaying
        {
            get
            {
                return _isPlaying;
            }
        }

        public int currentSoundIndex
        {
            get
            {
                return _currentSoundIndex;
            }
        }

        public float totalLength
        {
            get
            {
                float len = 0f;
                foreach (SoundClip sound in sounds)
                {
                    len += sound.Length;
                }
                return len;
            }
        }

        private SoundManagerEventProccessor proccessor;
        private AudibleSound _currentSound;
        private int _currentSoundIndex =0;
        private SoundClip[] sounds;

        public SoundPlaylist(SoundClip[] sounds, GameObject target)
        {
            proccessor = target.GetOrAddComponent<SoundManagerEventProccessor>();
            this.sounds = sounds;
        }

        public void Play()
        {
            _isPlaying = true;
            _currentSoundIndex = 0;
            PlayNext();
        }

        public void PlayNext()
        {
            _isPlaying = true;
            _currentSound = SoundManager.getInstance().Play(sounds [_currentSoundIndex], proccessor.gameObject);
            _currentSound.SoundComplete+= currentSoundDone;
        }

        public void stopAndGoto(int soundIndexInPlaylist)
        {
            if (soundIndexInPlaylist >= sounds.Length)
                throw(new System.Exception("Index " + soundIndexInPlaylist + " is out of range. Max index is: " + (sounds.Length - 1)));
            StopCurrent();
            _currentSoundIndex = soundIndexInPlaylist;
            PlayNext();
            if(PlaylistJump!=null)
                PlaylistJump(this);
        }

        public void StopCurrent()
        {
            _isPlaying = false;
            _currentSound.Stop();
        }

        public void Stop()
        {
            _isPlaying = false;
			if (_currentSound != null) {
				_currentSound.SoundComplete -= currentSoundDone;
				_currentSound.Stop ();
			}
        }

        public void PausePlaylist(bool pauseCurrent=true)
        {
            if (pauseCurrent)
            {
                _currentSound.Pause();
            }
            _isPlaying = false;
        }

        public void UnPausePlayList(bool unPauseCurrentSound=true)
        {
            _isPlaying = true;
            if (unPauseCurrentSound)
            {
                _currentSound.Play();
            }
        }

        private void currentSoundDone (AudibleSound sound)
        {
            if (!_isPlaying)
                return;
            if (_currentSoundIndex + 1 >= sounds.Length)
            {
                _isPlaying=false;
                if(PlaylistComplete!=null)
                    PlaylistComplete(this);
            } else
            {
                _currentSoundIndex++;
                PlayNext();
                if(PlaylistJump!=null)
                    PlaylistJump(this);
            }
        }


    }
}