using UnityEngine;
using System.Collections;

namespace GameDevToolkit.Sound
{
    public class AudibleSound
    {
        public delegate void AudibleSoundDelegate(AudibleSound sound);
        public event AudibleSoundDelegate SoundComplete;
        /// <summary>
        /// Gets and sets wether this AudibleSound should loop. NOTE: this value should not be set before AudibleSound.Play is called!
        /// </summary>
        public bool loop
        {
            get
            {
                if (source == null)
                    return false;
                return source.loop;
            }
            set
            {
                if (source == null)
                {
                    Debug.LogError("Cannot set loop, source does not yet exist. (Call play first)");
                }
                else
                {
                    source.loop = value;
                }
            }
        }
        private SoundClip _sound;
        private bool _updateVolumeFromLayer = true;
        private Coroutine dispatcher;
        /// <summary>
        /// Gets or sets if layer volume changes should be applied while this sound is playing, default is null.
        /// </summary>
        public bool UpdateVolumeFromLayer
        {
            get
            {
                return _updateVolumeFromLayer;
            }
            set
            {
                _updateVolumeFromLayer=value;
                if(value)
                    _sound.ParentLayer.VolumeChangedEvent+= HandleVolumeChangedEvent;
                else
                {
                    try
                    {
                        _sound.ParentLayer.VolumeChangedEvent-= HandleVolumeChangedEvent;
                    }
                    catch
                    {

                    }
                }
            }
        }
        /// <summary>
        /// Checks if this sound is currently playing
        /// </summary>
        public bool isPlaying
        {
            get
            {
                if(source==null)
                    return false;
                return source.isPlaying;
            }
        }
        /// <summary>
        /// Returns the length of the underlying SoundClip
        /// </summary>
        public float Length
        {
            get { return _sound.Clip.length;}
        }

        private AudioSource source;
        private GameObject targetGameObject;
        private SoundManagerEventProccessor proccessor;
        /// <summary>
        /// Gets and sets actual volume on AudioSource 
        /// </summary>
        public float ActualVolume
        {
            get
            {
                return source.volume;
            }
            set
            {
                source.volume = value;
            }
        }
        /// <summary>
        /// Gets the playhead position in seconds.
        /// </summary>
        public float Position
        {
            get
            {
                return source.time;
            }
            set
            {
                float toSet = value;
                if (toSet > _sound.Length)
                {
                    toSet = _sound.Length;
                }
                source.time = toSet;
                if (dispatcher != null)
                {
                    proccessor.StopCoroutine(dispatcher);
                    dispatcher = proccessor.StartCoroutine(dispatchCompleteEvent());
                }
            }
        }
        /// <summary>
        /// Gets reference to the SoundClip this instance is based on
        /// </summary>
        public SoundClip Sound
        {
            get
            {
                return _sound;
            }
        }
        /// <summary>
        /// Name based on underlying SoundClip
        /// </summary>
        public string Name
        {
            get
            {
                return _sound.Name;
            }
        }

        public AudibleSound(SoundClip sound, GameObject target)
        {
            try
            {
                _sound = sound;
                targetGameObject = target;
                proccessor = target.GetComponent<SoundManagerEventProccessor>();
                _sound.ParentLayer.VolumeChangedEvent+= HandleVolumeChangedEvent;
                if (proccessor == null)
                    proccessor = target.AddComponent<SoundManagerEventProccessor>();
                Play();
            }
            catch
            {
                Debug.LogWarning("Sound contruction failed!");
            }
        }

        void HandleVolumeChangedEvent (float newVolume)
        {
            if (source != null)
                source.volume = newVolume*_sound.RelativeVolume;
            else
            {
                _sound.ParentLayer.VolumeChangedEvent-= HandleVolumeChangedEvent;
                SoundManager.getInstance().currentlyPlayingSounds.Remove(this);
            }
        }
        /// <summary>
        /// Handles actualy playing sound, auto-creates all neceserry Unity components.
        /// </summary>
        public void Play()
        {
            if (proccessor == null)
            {
                Debug.LogWarning("Dew to construcor failure sound cannot play!");
            }
            if (!SoundManager.getInstance().currentlyPlayingSounds.Contains(this))
            {
                SoundManager.getInstance().currentlyPlayingSounds.Add(this);
            }
            if (source == null)
            {
                source=targetGameObject.AddComponent<AudioSource>();
                source.clip=_sound.Clip;
                source.volume=_sound.ParentLayer.Volume*_sound.RelativeVolume;
            }
            source.loop = this.loop;
            source.Play();
            dispatcher = proccessor.StartCoroutine(dispatchCompleteEvent());

        }

        private IEnumerator dispatchCompleteEvent()
        {
            yield return new WaitForSeconds(_sound.Clip.length - source.time);
            if(!loop)
            {
                SoundManager.getInstance().currentlyPlayingSounds.Remove(this);
                if(SoundComplete!=null)
                    SoundComplete(this);
            }
        }
        /// <summary>
        /// Pauses playback
        /// </summary>
        /// <param name="removeFromPlaying">If true, this will remove this instance from current playing sounds list in sound manager, default is false.</param>
        public void Pause(bool removeFromPlaying=false)
        {
            if (source == null)
                return;
            //proccessor.StopCoroutine(
            source.Pause();
            if(removeFromPlaying)
            SoundManager.getInstance().currentlyPlayingSounds.Remove(this);
            this.proccessor.StopCoroutine(dispatcher);
        }
        /// <summary>
        /// Stops playback, removes instance from list of currently playing sounds in sound manager.
        /// </summary>
        public void Stop()
        {
            try
            {
                if (dispatcher != null)
                    proccessor.StopCoroutine(dispatcher);
            }
            catch
            {

            }
            SoundManager.getInstance().currentlyPlayingSounds.Remove(this);
            if (source == null)
                return;
            source.Stop();
        }
    }
}