using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GameDevToolkit.Sound
{
	public class SoundManager
	{
        public const string BACKGROUND="BACKGROUND";
        public const string VOICE="VOICE";
        public const string SFX="SFX";
        private List<SoundLayer> layers= new List<SoundLayer>();
        /// <summary>
        /// Default layer for addSound overload without layer
        /// </summary>
        public SoundLayer defaultLayer;
        /// <summary>
        /// List of current playing sounds
        /// </summary>
        internal List<AudibleSound> currentlyPlayingSounds = new List<AudibleSound>();
        /// <summary>
        /// Automaticly created for Play overloads that dont take a playOn paramter, usualy used for all audio sources.
        /// </summary>
        private GameObject defaultGameObject;
        private static SoundManager instance;
		private SoundManager()
        {
            addLayer(BACKGROUND);
            addLayer(VOICE);
            defaultLayer = addLayer(SFX);
        }

        public static SoundManager getInstance()
        {
            if (instance == null)
                instance = new SoundManager();
            if(instance.defaultGameObject==null)
                instance.defaultGameObject=new GameObject("DEFAULT_SOUND_PLAYER");
            return instance;
        }

        public static void reset()
        {
            instance = null;
        }
        /// <summary>
        /// Create a new SoundLayer with given name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public SoundLayer addLayer(string name)
        {
            return addLayer(new SoundLayer(name));
        }
        /// <summary>
        /// Adds a SoundLayer to internaly manged layers, must be called before any sound can be played on the given layer.
        /// All overloads of addLayer method should come here.
        /// </summary>
        /// <param name="layer"></param>
        /// <returns></returns>
        public SoundLayer addLayer(SoundLayer layer)
        {
            foreach (SoundLayer xLayer in layers)
            {
                if (xLayer.Name == layer.Name)
                    throw (new System.Exception("Layer names should be unique, layer with name " + layer.Name + " already exists."));
            }
            layers.Add(layer);
            return layer;
        }
        /// <summary>
        /// Creates SoundClip instance and adds to it the defaultLayer
        /// </summary>
        /// <param name="clip"></param>
        /// <returns>SoundClip</returns>
        public SoundClip addSound(AudioClip clip)
        {
            return addSound(clip, defaultLayer);
        }
        /// <summary>
        /// Creates SoundClip instance and adds to the layer named in parameter.
        /// </summary>
        /// <param name="clip"></param>
        /// <param name="layer"></param>
        /// <returns>SoundClip</returns>
        public SoundClip addSound(AudioClip clip, string layer)
        {
            return addSound(clip, getLayerByName(layer));
        }
        /// <summary>
        /// Creates SoundClip instance and adds it to the layer in parameter.
        /// All overloads of addSound should lead here.
        /// </summary>
        /// <param name="clip"></param>
        /// <param name="layer"></param>
        /// <returns>SoundClip</returns>
        public SoundClip addSound(AudioClip clip, SoundLayer layer)
        {
            SoundClip soundCLip= new SoundClip(clip);
            layer.addSound(soundCLip);
            return soundCLip;
        }
        /// <summary>
        /// Creates SoundClip instances for each AudioClip given in parameter and adds them to the given layer.
        /// All overloads of addSounds should lead here.
        /// </summary>
        /// <param name="clips"></param>
        /// <returns>List<SoundClip></returns>
        public List<SoundClip> addSounds(AudioClip[] clips, SoundLayer layer)
        {
            List<SoundClip> result = new List<SoundClip>();
            foreach (AudioClip clip in clips)
            {
                result.Add(addSound(clip,layer));
            }
            return result;
        }

        /// <summary>
        /// Creates SoundClip instances for each AudioClip given in parameter and adds them to the given layer.
        /// </summary>
        /// <param name="clips"></param>
        /// <returns>List<SoundClip></returns>
        public List<SoundClip> addSounds(List<AudioClip> clips, SoundLayer layer)
        {
            return addSounds(clips.ToArray(),layer);
        }
        /// <summary>
        /// Creates SoundClip instances for each AudioClip given in parameter and adds them to the defaultLayer.
        /// </summary>
        /// <param name="clips"></param>
        /// <returns>List<SoundClip></returns>
        public List<SoundClip> addSounds(AudioClip[] clips)
        {
            return addSounds(clips, defaultLayer);
        }
        /// <summary>
        /// Creates SoundClip instances for each AudioClip given in parameter and adds them to the defaultLayer.
        /// </summary>
        /// <param name="clips"></param>
        /// <returns>List<SoundClip></returns>
        public List<SoundClip> addSounds(List<AudioClip> clips)
        {
            return addSounds(clips.ToArray(),defaultLayer);
        }
        /// <summary>
        /// Returns a clone of the internal layers lists.
        /// </summary>
        /// <returns></returns>
        public List<SoundLayer> getLayers()
        {
            return new List<SoundLayer>(layers);
        }
        /// <summary>
        /// Returns SoundLayer with name specified in parameter, will return null if its not found.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public SoundLayer getLayerByName(string name)
        {
            foreach (SoundLayer layer in layers)
            {
                if(layer.Name==name)
                    return layer;
            }
            return null;
        }
        /// <summary>
        /// Plays the given SoundClip on the given gameObject
        /// All overloads of Play should lead here.
        /// </summary>
        /// <param name="sound"></param>
        /// <param name="playOn"></param>
        /// <returns></returns>
        public AudibleSound Play(SoundClip sound, GameObject playOn)
        {
            if (sound == null || playOn == null)
            {
                Debug.LogWarning("Invalid parameters:" + sound + "," + playOn);
                return null;
            }
            if(sound.ParentLayer==null)
            {
                Debug.LogWarning("Cannot play " + sound.Name + " because it's not on a layer!");
                return null;
            }
            if (sound.ParentLayer.isActive)
            {
                AudibleSound result = new AudibleSound(sound, playOn);
                currentlyPlayingSounds.Add(result);
                return result;
            }
            else
            {
                Debug.LogWarning("Cannot play " + sound.Name + " because its layer is not managed by SoundManager, returning null.");
                return null;
            }
        }
        /// <summary>
        /// Plays given SoundClip on defaultGameObject
        /// </summary>
        /// <param name="sound"></param>
        /// <returns></returns>
        public AudibleSound Play(SoundClip sound)
        {
            return Play(sound,defaultGameObject);
        }
        /// <summary>
        /// Same as <see cref="SoundManager.Play(SoundClip, GameObject)">SoundManager.Play</see> with added delay.
        /// </summary>
        /// <param name="sound"></param>
        /// <param name="playOn"></param>
        /// <param name="delayInSeconds"></param>
        /// <returns></returns>
        public IEnumerator PlayWithDelay(SoundClip sound,GameObject playOn, float delayInSeconds)
        {
            yield return new WaitForSeconds(delayInSeconds);
            Play(sound, playOn);
        }
        /// <summary>
        /// Same as <see cref="SoundManager.Play(SoundClip)">SoundManager.Play</see> with added delay.
        /// </summary>
        /// <param name="sound"></param>
        /// <param name="delayInSeconds"></param>
        /// <returns></returns>
        public IEnumerator PlayWithDelay(SoundClip sound,float delayInSeconds)
        {
            yield return new WaitForSeconds(delayInSeconds);
            Play(sound, defaultGameObject);
        }



        public SoundPlaylist createPlaylist(List<SoundClip> sounds)
        {
            return createPlaylist(sounds.ToArray(),defaultGameObject);
        }
        /// <summary>
        /// Creates a SoundPlaylist instance with given sounds, that will be played on given gameObject.
        /// </summary>
        /// <param name="sounds"></param>
        /// <param name="playOn"></param>
        /// <returns></returns>
        public SoundPlaylist createPlaylist(List<SoundClip> sounds, GameObject playOn)
        {
            return createPlaylist(sounds.ToArray(), playOn);
        }


        /// <summary>
        /// Creates a SoundPlaylist instance with given sounds, that will be played on defaultGameObject.
        /// </summary>
        /// <param name="sounds"></param>
        /// <returns></returns>
        public SoundPlaylist createPlaylist(SoundClip[] sounds)
        {
            return createPlaylist(sounds,defaultGameObject);
        }
        /// <summary>
        /// Creates a SoundPlaylist instance with given sounds, that will be played on given gameObject.
        /// All overloads of createPlaylist should lead here.
        /// </summary>
        /// <param name="sounds"></param>
        /// <param name="playOn"></param>
        /// <returns></returns>
        public SoundPlaylist createPlaylist(SoundClip[] sounds, GameObject playOn)
        {
            return new SoundPlaylist(sounds, playOn);
        }
        /// <summary>
        /// Returns a clone of the internally tracked AudibleSounds list
        /// </summary>
        /// <returns></returns>
        public List<AudibleSound> getAudibleSounds()
        {
            return new List<AudibleSound>(currentlyPlayingSounds);
        }
        /// <summary>
        /// Returns a list of internally tracked AudibleSounds that match given criteria
        /// </summary>
        /// <returns></returns>
        public List<AudibleSound> getAudibleSoundByName(string name)
        {
            List<AudibleSound> result = new List<AudibleSound>();
            foreach (AudibleSound sound in currentlyPlayingSounds)
            {
                if(sound.Name==name)
                    result.Add(sound);
            }
            return result;
        }
        /// <summary>
        /// Returns a list of internally tracked AudibleSounds that match given criteria
        /// </summary>
        /// <returns></returns>
        public List<AudibleSound> getAudibleSoundsByLayer(SoundLayer layer)
        {
            return getAudibleSoundsByLayer(layer.Name);
        }
        /// <summary>
        /// Returns a list of internally tracked AudibleSounds that match given criteria
        /// All overloads of getAudibleSoundsByLayer should lead here.
        /// </summary>
        /// <returns></returns>
        public List<AudibleSound> getAudibleSoundsByLayer(string layerName)
        {
            List<AudibleSound> result = new List<AudibleSound>();
            foreach (AudibleSound sound in currentlyPlayingSounds)
            {
                if (sound.Sound.ParentLayer.Name == layerName)
                {
                    result.Add(sound);
                }
            }
            return result;
        }
        /// <summary>
        /// Stop all internaly tracked Audible sounds
        /// </summary>
        public void StopAll()
        {
            List<AudibleSound> tempList = new List<AudibleSound>();
            tempList.AddRange(currentlyPlayingSounds.ToArray());
            foreach(AudibleSound tempSound in tempList)
            {
                tempSound.Stop();
            }
        }
        /// <summary>
        /// Checks if an internaly tracked AudibleSound exists for the given SoundClip.
        /// Returns true if one exists and .isPlaying is true, false in all other cases.
        /// </summary>
        /// <param name="soundClip"></param>
        /// <returns></returns>
        public bool IsPlaying(SoundClip soundClip)
        {
            bool isPlaying = false;
            List<AudibleSound> tempList = new List<AudibleSound>();
            tempList.AddRange(currentlyPlayingSounds.ToArray());
            foreach(AudibleSound tempSound in tempList)
            {
                if (tempSound.Sound == soundClip)
                {
                    if (tempSound.isPlaying)
                    {
                        isPlaying = true;
                        break;
                    }
                }
            }
            return isPlaying;
        }

        private SoundManagerEventProccessor getProcessor(GameObject target)
        {
            return target.GetOrAddComponent<SoundManagerEventProccessor>();
        }

	}
}