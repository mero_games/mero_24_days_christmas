using UnityEngine;

namespace GameDevToolkit.Sound
{
    [System.Serializable]
    public class SoundClip
    {
        [System.NonSerialized]
        private SoundLayer _parentLayer;
        /// <summary>
        /// Gets or sets Layer this clip resides on, may be null.
        /// </summary>
        public SoundLayer ParentLayer
        {
            get
            {
                return _parentLayer;
            }
            set
            {
                if(value!=_parentLayer)
                {
                    enforceLayer(value, _parentLayer);
                    _parentLayer = value;
                }
            }
        }
        [SerializeField]
        private AudioClip _clip;
        [SerializeField]
        private float _relativeVolume = 1f;
        /// <summary>
        /// Gets or sets the volume of this sound, relative to the volume defined by layer.
        /// </summary>
        public float RelativeVolume
        {
            get { return _relativeVolume; }
            set
            {
                _relativeVolume = value;
                if (_relativeVolume < 0f)
                    _relativeVolume = 0f;
                if (_relativeVolume > 1f)
                    _relativeVolume = 1f;

            }
        }
        /// <summary>
        /// AudiClip this instance is based on.
        /// </summary>
        public AudioClip Clip
        {
            get
            {
                return _clip;
            }
        }
        /// <summary>
        /// Name of SoundClip based on underlying AudioClip
        /// </summary>
        public string Name
        {
            get
            {
                return _clip.name;
            }
        }
        /// <summary>
        /// Length of SoundClip
        /// </summary>
        public float Length
        {
            get { return _clip.length;}
        }

        private void enforceLayer(SoundLayer newLayer, SoundLayer oldLayer=null)
        {
            if (oldLayer != null)
                oldLayer.sounds.Remove(this);
            
            if (!newLayer.hasSound(this))
                newLayer.sounds.Add(this);
        }

        public SoundClip(AudioClip clip)
        {
            _clip = clip;
        }
        /// <summary>
        /// Play this soundclip on defaultGameObject, may return null if SoundClip does not have a ParentLayer
        /// </summary>
        /// <returns></returns>
        public AudibleSound Play()
        {
            if (_parentLayer != null)
            {
                enforceLayer(_parentLayer, null);
                return SoundManager.getInstance().Play(this);
            }
            return null;
        }
    }
}