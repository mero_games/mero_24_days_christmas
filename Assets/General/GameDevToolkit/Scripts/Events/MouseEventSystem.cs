using UnityEngine;
using System.Collections;
namespace GameDevToolkit.Events
{
    /// <summary>
    /// Predfined GenericMouseEvent with GameObject target type.
    /// </summary>
    [DisallowMultipleComponent]
    public class MouseEventSystem : GenericMouseEvent<GameObject>
    {
        
    }
}