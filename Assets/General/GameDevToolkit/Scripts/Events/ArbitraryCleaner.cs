using UnityEngine;
using System.Collections;
using GameDevToolkit.Events;
using UnityEngine.Events;

namespace GameDevToolkit.Events
{
    /// <summary>
    /// Used to destroy a gameObject when the attached component/animation calls ArbitraryCleaner.dispatchEvent
    /// </summary>
    public class ArbitraryCleaner : MonoBehaviour
    {
        public event UnityAction<GameObject> Done;

        public void dispatchEvent()
        {
            if (Done != null)
                Done(gameObject);
            StartCoroutine(destroyMe());
        }

        private IEnumerator destroyMe()
        {
            yield return new WaitForEndOfFrame();
            Destroy(gameObject);
        }


    }
}