using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace GameDevToolkit.Events
{
    /// <summary>
    /// Used for dispatching events at the end(or other predefined moments) of/in animations/tweens.
    /// </summary>
    public class ArbitraryEvent : MonoBehaviour
    {
        public delegate void TweenCallbackDelegate<T>(T argumeny);
        public event TweenCallbackDelegate<GameObject> EventGameObject;
        public event TweenCallbackDelegate<string> EventString;
        public event TweenCallbackDelegate<ArbitraryEvent> EventTarget;

        public void dispatchWithGameObject(GameObject target)
        {
            //Debug.Log("Should dispatch tween event!");
            if (EventGameObject != null)
            {
                if (target != null)
                    EventGameObject(target);
                else
                    EventGameObject(gameObject);
            }
        }

        public void dispatchWithString(string target)
        {
            //Debug.Log("Should dispatch tween event!");
            if (EventString != null)
                EventString(target);
        }

        public void dispatchWithCurrent()
        {
            if (EventTarget != null)
                EventTarget(this);
        }
    }
}