using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class AddEventSymbol: AssetPostprocessor
{
    private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        List<string> symbols = new List<string>(getCurrentDefineArray());
        if(!symbols.Contains("HAS_EVENTS"))
        {
            symbols.Add("HAS_EVENTS");
            setCurrentDefineArray(symbols.ToArray());
        }
    }

    private static void setCurrentDefineArray(string[] symbols)
    {
#if UNITY_STANDALONE
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, string.Join(";", symbols));
#endif
#if UNITY_ANDROID
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, string.Join(";", symbols));
#endif
#if UNITY_IOS
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, string.Join(";", symbols));
#endif
    }

    private static string[] getCurrentDefineArray()
    {
#if UNITY_STANDALONE
        return PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone).Split(';');
#endif
#if UNITY_ANDROID
        return PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Split(';');
#endif
#if UNITY_IOS
        return PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS).Split(';');
#endif
    }
}