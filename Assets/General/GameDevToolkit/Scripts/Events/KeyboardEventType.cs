namespace GameDevToolkit.Events
{
    public enum KeyboardEventType
    {
        UP,
        DOWN,
        PRESSED
    }
}