using GameDevToolkit.GameUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameDevToolkit.GameUtil.RandomizerRules
{
    /// <summary>
    /// Permits any number of occurences of all numbers in range but without the same number being given twice in a row.
    /// For example 1,2 is ok but 2,2 is not ok.
    /// </summary>
    public class SemiRandomRule : IRandomizerRule<int>
    {
        private int asked;
        private int min;
        private int lastBlock;
        public SemiRandomRule(int min, int max)
        {
            this.min = min;
        }

        public void Reset()
        {
            asked = min - 1;
        }

        public bool IsBlocked(int arg)
        {
            return asked==arg;
        }

        public void addBlock(int arg)
        {
            asked = arg;
            lastBlock = arg;
        }

        public int getLastBlock()
        {
            return lastBlock;
        }

        public void removeBlock(int arg)
        {
            Reset();
        }

        public bool hasNumbersLeft()
        {
            return true;
        }

        public int getNumbersLeft()
        {
            return int.MaxValue;
        }
    }
}