using UnityEngine;
using System.Collections;
using GameDevToolkit.Common;

namespace GameDevToolkit.GameUtil
{
    /// <summary>
    /// Explicitly defined dummy class for MultiDimensionalArray<GameObject>
    /// </summary>
    [System.Serializable]
    public class TwoDGameObjectArray : TwoDArray<GameObject>
    {
        public TwoDGameObjectArray(int size):base(size)
        {

        }
    }
}