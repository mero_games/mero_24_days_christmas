using UnityEngine;
using System.Collections;
namespace GameDevToolkit.GameUtil
{
    /// <summary>
    /// Used for creating objects that look at the current mouse position.
    /// </summary>
    public class LookAtMouse : MonoBehaviour
    {
        Camera cam;
        Transform my;
        private bool _isSleeping=false;
        private Coroutine sleepRoutine;
        public bool IsSleeping
        {
            get { return _isSleeping;}
        }
        void Awake ()
        {
            cam = Camera.main;
            my = GetComponent <Transform> ();
        }
        
        
        void Update ()
        {
            LookAtCoord(cam.ScreenToWorldPoint(Input.mousePosition));        
            #if UNITY_ANDROID || UNITY_IOS
            if(Input.touches.Length>0)
            {
                foreach(Touch touch in Input.touches)
                {
                    if(touch.phase== TouchPhase.Began)
                    {
                        LookAtCoord(new Vector3(touch.rawPosition.x, touch.rawPosition.y));
                        return;
                    }
                }
            }
            #endif
        }
        /// <summary>
        /// Do not rotate for given number of seconds
        /// </summary>
        /// <param name="seconds"></param>
        public void Sleep(float seconds)
        {
            sleepRoutine=StartCoroutine(doSleep(seconds));
        }
        /// <summary>
        /// Forcibly wake up and start rotating again.
        /// </summary>
        public void WakeUp()
        {
            if (sleepRoutine != null)
            {
                StopCoroutine(sleepRoutine);
            }
            _isSleeping = false;
        }

        public IEnumerator doSleep (float seconds)
        {
            yield return new WaitForEndOfFrame();
            _isSleeping = true;
            yield return new WaitForSeconds(seconds);
            sleepRoutine = null;
            _isSleeping = false;
        }

        public void LookAtObject(GameObject obj)
        {
            LookAtCoord(obj.transform.position);
        }

        public void LookAtCoord(Vector3 targetCoord)
        {
            if (_isSleeping)
                return;
            Vector3 diff = targetCoord - transform.position;
            diff.Normalize();
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            my.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
    }
}