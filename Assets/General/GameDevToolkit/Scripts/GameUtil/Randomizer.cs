using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDevToolkit.GameUtil.RandomizerRules;
namespace GameDevToolkit.GameUtil
{
    /// <summary>
    /// Used for the random generation of int numbers based on IRandomizerRule implementations
    /// </summary>
    public class Randomizer
    {
        /// <summary>
        /// The IRandomizerRule implementation being used.
        /// </summary>
        public IRandomizerRule<int> randomRule;
        private int min;
        private int max;
        public int Min
        {
            get
            {
                return min;
            }
        }

        public int Max
        {
            get
            {
                return max;
            }
        }
        /// <summary>
        /// Creates a new Randomizer
        /// </summary>
        /// <typeparam name="T">Type of explicit IRandomizerRule implementation to be used by the new instance.</typeparam>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Randomizer CreateRandomizer<T>(int min, int max) where T : IRandomizerRule<int>
        {
            return new Randomizer(min, max, (T)System.Activator.CreateInstance(typeof(T), new object[] { min, max }));
        }
        /// <summary>
        /// Creates a new Randomizer
        /// </summary>
        /// <typeparam name="T">Type of explicit IRandomizerRule implementation to be used by the new instance.</typeparam>
        /// <param name="array">Min will be 0, max will be array.Length-1</param>
        /// <returns></returns>
        public static Randomizer CreateRandomizer<T>(System.Array array) where T : IRandomizerRule<int>
        {
            return new Randomizer(0, array.Length - 1, (T)System.Activator.CreateInstance(typeof(T), new object[] { 0, array.Length - 1 }));
        }

        /// <summary>
        /// Creates a new Randomizer
        /// </summary>
        /// <typeparam name="T">Type of explicit IRandomizerRule implementation to be used by the new instance.</typeparam>
        /// <param name="array">Min will be 0, max will be array.Length-1</param>
        /// <returns></returns>
        public static Randomizer CreateRandomizer<T>(IList array) where T : IRandomizerRule<int>
        {
            return new Randomizer(0, array.Count - 1, (T)System.Activator.CreateInstance(typeof(T), new object[] { 0, array.Count - 1 }));
        }
        /// <summary>
        /// Creates a new Randomizer use DefaultRandomRule
        /// </summary>
        /// <param name="array">Min will be 0, max will be array.Length-1</param>
        public Randomizer(IList array)
            : this(0, array.Count - 1, new DefaultRandomRule(0, array.Count - 1))
        {

        }
        /// <summary>
        /// Creates a new Randomizer use DefaultRandomRule
        /// </summary>
        /// <param name="array">Min will be 0, max will be array.Length-1</param>
        public Randomizer(System.Array array)
            : this(0, array.Length - 1, new DefaultRandomRule(0, array.Length - 1))
        {

        }
        /// <summary>
        /// Creates a new Randomizer use DefaultRandomRule
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public Randomizer(int min, int max)
            : this(min, max, new DefaultRandomRule(min, max))
        {

        }
        /// <summary>
        /// Creates a new Randomizer
        /// </summary>
        /// <param name="array">>Min will be 0, max will be array.Length-1</param>
        /// <param name="rules"></param>
        public Randomizer(System.Array array, IRandomizerRule<int> rules) : this(0, array.Length - 1, rules)
        {

        }
        /// <summary>
        /// Creates a new Randomizer
        /// </summary>
        /// <param name="array">>Min will be 0, max will be array.Length-1</param>
        /// <param name="rules"></param>
        public Randomizer(IList array, IRandomizerRule<int> rules)
            : this(0, array.Count - 1, rules)
        {

        }
        /// <summary>
        /// Creates a new Randomizer
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="rules"></param>
        public Randomizer(int min, int max, IRandomizerRule<int> rules)
        {
            this.min = min;
            this.max = max;
            this.randomRule = rules;
        }

        public Randomizer()
        {
        }

        /// <summary>
        /// Gets a random number using the rule this instance was created with.
        /// </summary>
        /// <param name="doNotUseRules">If true will return a number within the given range but ignoring any rules. Default is false</param>
        /// <returns></returns>
        public int getRandom(bool doNotUseRules = false)
        {
            if (doNotUseRules)
            {
                return Random.Range(min, max + 1);
            }
            else
            {
                return getRandom(randomRule);
            }
        }
        /// <summary>
        /// Gets a random number using the given rule.
        /// </summary>
        /// <param name="rule"></param>
        /// <returns></returns>
        public int getRandom(IRandomizerRule<int> rule)
        {
            if (!randomRule.hasNumbersLeft())
                throw (new System.Exception("No more numbers left, should use randomRule.Reset maybe?"));
            int number = Random.Range(min, max + 1);
            while (rule.IsBlocked(number))
            {
                number = Random.Range(min, max + 1);
            }
            rule.addBlock(number);
            return number;
        }
    }
}