using UnityEngine;
using System.Collections;
namespace GameDevToolkit.Common
{
    /// <summary>
    /// Logic for camera
    /// </summary>
    public class EDUCam : MonoBehaviour
    {
        /// <summary>
        /// Will be initalized by awake with the Camera component attached to the same GameObject
        /// </summary>
        public Camera attachedCamera;
        /// <summary>
        /// The float value of the desired aspect ratio.
        /// </summary>
        private float targetaspect = 16.0f / 9.0f;
        /// <summary>
        /// Depracted, should remove?
        /// </summary>
        public bool levelLoadRequested = false;
        /// <summary>
        /// Current mouse position in screen coordinates
        /// </summary>
        public Vector3 mousePosition
        {
            get
            {
                return getMousePosition();
            }
        }
        /// <summary>
        /// Current Instance of this Script
        /// </summary>
        public static EDUCam Instance;
        /// <summary>
        /// Intialize attachedCamera, Instance and calls updateAspect
        /// </summary>
    	void Awake()
        {
            attachedCamera = GetComponent<Camera>();
            Instance = this;
            updateAspect();
            //SaveDataManager.getInstance().SettingsChanged+= HandleSettingsChanged;
        }

        public void updateAspect()
        {
            updateAspect(Screen.width, Screen.height);
        }
        /// <summary>
        /// Actual logic for calculating and changing the view rect of the attached camera based on the desired aspect ratio.
        /// </summary>
        /// <param name="sWidth">screen width</param>
        /// <param name="sHeight">screen heigth</param>
        public void updateAspect(int sWidth, int sHeight)
        {
            float windowaspect = (float)sWidth / sHeight;
            // current viewport height should be scaled by this amount
            float scaleheight = windowaspect / targetaspect;

            // obtain camera component so we can modify its viewport
            //Camera camera = GetComponent<Camera>();

            // if scaled height is less than current height, add letterbox
            if (scaleheight < 1.0f)
            {
                Rect rect = attachedCamera.rect;

                rect.width = 1.0f;
                rect.height = scaleheight;
                rect.x = 0;
                rect.y = (1.0f - scaleheight) / 2.0f;

                attachedCamera.rect = rect;
            }
            else // add pillarbox
            {
                float scalewidth = 1.0f / scaleheight;

                Rect rect = attachedCamera.rect;

                rect.width = scalewidth;
                rect.height = 1.0f;
                rect.x = (1.0f - scalewidth) / 2.0f;
                rect.y = 0;

                attachedCamera.rect = rect;
            }
        }
        /// <summary>
        /// Currently simple returns Input.mousePosition.
        /// Originaly it was used to counteract a mousePosition bug withing Unity, may be usefull at some other point.
        /// Also should consider mapping screen position to world space using attached camera.
        /// </summary>
        /// <returns></returns>
        Vector3 getMousePosition()
        {
            Vector3 _mousePosition = Input.mousePosition;
            return _mousePosition;
            /*//if (!Screen.fullScreen || monitorAspect == -1f) { return; }
            
            float sw = Screen.width;
            float sh = Screen.height;
            
            float currentAspect = sw / sh;
            
            if (!Screen.fullScreen) { 
                //Debug.Log("Bypassing hack, not in fullscreen!");
               
            }
            
            // HACK Workaround for Unity bug
            if (targetaspect > currentAspect)
            {
                float wrongWidth = sh * targetaspect;
                _mousePosition.x = Mathf.Round((_mousePosition.x / wrongWidth) * sw);
            }
            else if (targetaspect < currentAspect)
            {
                float wrongHeight = sw / targetaspect;
                _mousePosition.y += wrongHeight - sh;
                _mousePosition.y = Mathf.Round((_mousePosition.y / wrongHeight) * sh);
            }
            //Debug.Log("Returning hacked position");
            return _mousePosition;*/
        }
    }
}