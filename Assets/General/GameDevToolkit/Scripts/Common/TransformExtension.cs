using UnityEngine;
/// <summary>
/// Transfrom help extension class.
/// Adds 2D specific functions to Transform
/// </summary>
public static class TransformExtension
{
    /// <summary>
    /// Perfrom a 2D lookAt (Z axis is ignored)
    /// </summary>
    /// <param name="currentTransform"></param>
    /// <param name="target"></param>
    public static void LookAt2D(this Transform currentTransform,Transform target)
    {
        Vector3 dir = target.position - currentTransform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        currentTransform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
    /// <summary>
    /// Perfrom a 2D lookAt (Z axis is ignored)
    /// </summary>
    /// <param name="currentTransform"></param>
    /// <param name="target"></param>
    public static void LookAt2D(this Transform currentTransform,Vector3 target)
    {
        Vector3 dir = target - currentTransform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        currentTransform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
    /// <summary>
    /// Calculate a 2D lookAt rotation (Z axis is ignored)
    /// </summary>
    /// <param name="currentTransform"></param>
    /// <param name="target"></param>
    public static Vector3 GetLookRotaion2D(this Transform currentTransform, Transform target)
    {
        return GetLookRotaion2D(currentTransform, target.position);
    }
    /// <summary>
    /// Calculate a 2D lookAt rotation (Z axis is ignored)
    /// </summary>
    /// <param name="currentTransform"></param>
    /// <param name="target"></param>
    public static Vector3 GetLookRotaion2D(this Transform currentTransform, Vector3 target)
    {
        Vector3 dir = target - currentTransform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        return Quaternion.AngleAxis(angle, Vector3.forward).eulerAngles;
    }
    /// <summary>
    /// Applies a 2d rotation (Z axis only)
    /// </summary>
    /// <param name="currentTransform"></param>
    /// <param name="target"></param>
    public static void set2DRotation(this Transform currentTransform, float rotation)
    {
        Vector3 rot = currentTransform.eulerAngles;
        rot.z = rotation;
        currentTransform.eulerAngles = rot;
    }
    /// <summary>
    /// Gets a 2D rotation (float Z axis only)
    /// </summary>
    /// <param name="currentTransform"></param>
    /// <returns></returns>
    public static float get2DRotation(this Transform currentTransform)
    {
        Vector3 rot = currentTransform.eulerAngles;
        return rot.z;
    }
    /// <summary>
    /// Applies a 2d local rotation (Z axis only)
    /// </summary>
    /// <param name="currentTransform"></param>
    /// <param name="target"></param>
    public static void setLocal2DRotation(this Transform currentTransform, float rotation)
    {
        Vector3 rot = currentTransform.localEulerAngles;
        rot.z = rotation;
        currentTransform.localEulerAngles = rot;
    }
}