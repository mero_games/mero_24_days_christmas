using UnityEngine;

/// <summary>
/// Extention class for GameObject
/// </summary>
public static class GameObjectExtension
{
    /// <summary>
    /// Trys to get attached component of type T, if no such component is attached, attaches one.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="target"></param>
    /// <returns></returns>
    public static T GetOrAddComponent<T>(this GameObject target) where T:Component
    {
        T component = target.GetComponent<T>();
        if (component != null)
            return component;
        else
            return target.AddComponent<T>();
    }
}