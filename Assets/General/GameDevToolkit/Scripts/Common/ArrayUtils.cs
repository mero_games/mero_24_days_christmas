﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Version 0.1
/// CHANGES
/// 0.1 - changed compareArrays and compareOrderedArrays to return false automaticly if lengths dont match
/// </summary>
namespace GameDevToolkit.Common
{
    /// <summary>
    /// Helper class containing functions for manipulating arrays and Lists
    /// </summary>
	public class ArrayUtils {
	
		public static int[] lastOrder;
		/// <summary>
        /// Randomize the position of items in an array, note that no rules are applied to this randomization,
        /// so if input is 1,2,3 output can  be 1,2,3 as well.
        /// Each time this function is called randomized positions are saved for use with <seealso cref="GameDevToolkit.GameUtil.ArrayUtils.randomizeSameOrder{T}(ref T[])"/>
        /// </summary>
        /// <typeparam name="T">Type of array</typeparam>
        /// <param name="arrToRandomize">array to randomize.</param>
		public static void randomize<T>(ref T[] arrToRandomize)
		{
			lastOrder = new int[arrToRandomize.Length];
			for(int i=0; i<arrToRandomize.Length; i++)
			{
				T temp = arrToRandomize[i];
				int curP = UnityEngine.Random.Range(0,arrToRandomize.Length);
				lastOrder[i] = curP;
				arrToRandomize[i] = arrToRandomize[curP];
				arrToRandomize[curP] = temp;
			}
		}
        /// <summary>
        /// Same as <seealso cref="GameDevToolkit.GameUtil.ArrayUtils.randomize{T}(ref T[])"/> but used for Lists not arrays.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="listToRandomize"></param>
        public static void randomizeList<T>(ref List<T> listToRandomize)
		{
			lastOrder = new int[listToRandomize.Count];
			for(int i=0; i<listToRandomize.Count; i++)
			{
				T temp = listToRandomize[i];
				int curP = UnityEngine.Random.Range(0,listToRandomize.Count);
				lastOrder[i] = curP;
				listToRandomize[i] = listToRandomize[curP];
				listToRandomize[curP] = temp;
			}
		}
		/// <summary>
        /// Randomize the given array based on order already determined by <seealso cref="GameDevToolkit.GameUtil.ArrayUtils.randomize{T}(ref T[])"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arrToRandomize"></param>
		public static void randomizeSameOrder<T>(ref T[] arrToRandomize)
		{
			if(lastOrder == null) throw new System.Exception("You need to use randomize first to use randomizeSameOrder!");
			if(lastOrder.Length != arrToRandomize.Length) throw new System.Exception("The given array needs to be same length as previously randomized array!");
			
			for(int i=0; i<arrToRandomize.Length; i++)
			{
				T temp = arrToRandomize[i];
				int curP = lastOrder[i];
				lastOrder[i] = curP;
				arrToRandomize[i] = arrToRandomize[curP];
				arrToRandomize[curP] = temp;
			}
		}
        /// <summary>
        /// Same as <seealso cref="GameDevToolkit.GameUtil.ArrayUtils.randomizeSameOrder{T}(ref T[])"/> but used for Lists not arrays.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="listToRandomize"></param>
        public static void randomizeListSameOrder<T>(ref List<T> listToRandomize)
		{
			if(lastOrder == null) throw new System.Exception("You need to use randomize first to use randomizeListSameOrder!");
			if(lastOrder.Length != listToRandomize.Count) throw new System.Exception("The given list needs to be same length as previously randomized list!");
			
			for(int i=0; i<listToRandomize.Count; i++)
			{
				T temp = listToRandomize[i];
				int curP = lastOrder[i];
				lastOrder[i] = curP;
				listToRandomize[i] = listToRandomize[curP];
				listToRandomize[curP] = temp;
			}
		}
		/// <summary>
        /// Prints a string representation of array(not friendly for non type values) to Debug console.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arrToPrint"></param>
		public static void printArray<T>(T[] arrToPrint)
		{
			Debug.Log(ArrayToString<T>(arrToPrint));
		}
        /// <summary>
        /// Prints a string representation of List(not friendly for non type values) to Debug console.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arrToPrint"></param>
        public static void printList<T>(List<T> arrToPrint)
		{
			Debug.Log(ArrayToString<T>(arrToPrint.ToArray()));
		}
        /// <summary>
        /// Compare two arrays.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arrToPrint"></param>
        /// <returns>True only if both arrays contians the same elements and the same number of elements. Element order is not considered.
        /// For example if a=1,2,3 and b=3,2,1 result will be true, but if b=3,3,1,2 result will be false.</returns>
        public static bool compareArrays<T>(T[] array1, T[] array2)
		{
            if (array1.Length != array2.Length)
                return false;
			for(int i = 0; i<array1.Length; i++)
			{
				if(System.Array.IndexOf(array2, array1[i]) == -1)
				{
					return false;
				}
			}
			return true;
		}
        /// <summary>
        /// Compare two arrays.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array1"></param>
        /// <param name="array2"></param>
        /// <returns>True only if the given arrays contains the same elements in the same positions. (Length ofc needs to equal for a positive result)</returns>
        public static bool compareOrderedArrays<T>(T[] array1, T[] array2)
		{
            if (array1.Length != array2.Length)
                return false;
			for(int i = 0; i<array1.Length; i++)
			{
				if(!EqualityComparer<T>.Default.Equals(array1[i], array2[i])) return false;
			}
			return true;
		}
		/// <summary>
        /// Gets common elements between two arrays.
        /// </summary>
        /// <typeparam name="T">Type of input arrays and returned list</typeparam>
        /// <param name="array1"></param>
        /// <param name="array2"></param>
        /// <returns>a new List of type T containg either nothing, or the common elements of the two given arrays.</returns>
		public static List<T> getCommonElements<T>(T[] array1, T[] array2)
		{
			List<T> ret_arr = new List<T>();
			
			for(int i = 0; i<array1.Length; i++)
			{
				if(System.Array.IndexOf(array2, array1[i]) != -1){
					ret_arr.Add(array1[i]);
				}
			}
			
			return ret_arr;
		}
        /// <summary>
        /// Returns a string reperesentation of the array. ToStirng is called on each element, making this non-primitve type friendly.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <returns></returns>
        public static string ArrayToString<T>(T[] array)
        {
            return ArrayToString<T>(array, ",");
        }
        /// <summary>
        /// Returns a string reperesentation of the array. ToStirng is called on each element, making this non-primitve type friendly.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array you want to convert to string</param>
        /// <param name="separator">Separator to use between elements</param>
        /// <returns></returns>
        public static string ArrayToString<T>(T[] array, string separator)
        {
            string[] result = new string[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                result[i] = array[i].ToString();
            }
            return string.Join(separator, result);
        }

    }
}