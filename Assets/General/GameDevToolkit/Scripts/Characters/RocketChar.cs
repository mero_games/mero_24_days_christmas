﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;


#if (UNITY_ANDROID || UNITY_IOS) && HAS_EVENTS
using GameDevToolkit.Events;
#endif
namespace GameDevToolkit.Characters
{
    /// <summary>
    /// A character that moves from bottom to top.</br>
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class RocketChar : MonoBehaviour
    {
        public event UnityAction<RocketChar, Collider2D, Collision2D> RocketCharHit;
        /// <summary>
        /// Dispatched when character enters a trigger
        /// </summary>
        public event UnityAction<RocketChar, Collider2D> RocketCharTriggered;
        /// <summary>
        /// GameObject name or partial name that will not trigger a RocketCharTriggered event.
        /// For example with the predefined value of "Arrow" a GameObject called downArrow will not trigger the event.
        /// </summary>
        public string[] exceptionNames = new string[] { "Arrow" };
        /// <summary>
        /// Wether the character can move.
        /// </summary>
        public bool canMove = true;
        /// <summary>
        /// The minimum X coordinate of the character
        /// </summary>
        public float minX;
        /// <summary>
        /// The maximum X coordinate of the character
        /// </summary>
        public float maxX;
        /// <summary>
        /// On screen left arrow GameObject for Android and iOS
        /// </summary>
        public GameObject leftArrow;
        /// <summary>
        /// On screen right arrow GameObject for Android and iOS
        /// </summary>
        public GameObject rightArrow;
        /// <summary>
        /// Movement speed from left to right.
        /// </summary>
        public float horizontalSpeed = 0.5f;
        /// <summary>
        /// Maximom force the character will have for movement upward.</br>
        /// This is effectivly the maximum physical energy of the character.</br>
        /// If jumpMaxForce=500 and current velocity is 200, only 300 will be added.
        /// </summary>
        public float jumpMaxForce = 500f;
        /// <summary>
        /// Attached RigidBody2D
        /// </summary>
        [ReadOnly]
        public Rigidbody2D body;
        /// <summary>
        /// Is character going left?
        /// </summary>
        private bool goLeft = false;
        /// <summary>
        /// Is the character going right?
        /// </summary>
        private bool goRight = false;

        /// <summary>
        /// Do setup
        /// </summary>
        private void Awake()
        {
            body = GetComponent<Rigidbody2D>();
            //sprite = gameObject.GetComponent<ObjectSequence>();
#if UNITY_STANDALONE
            if (leftArrow != null)
            {
                leftArrow.SetActive(false);
            }
            if (rightArrow != null)
            {
                rightArrow.SetActive(false);
            }
#endif
#if (UNITY_ANDROID || UNITY_IOS) && HAS_EVENTS

        leftArrow.SetActive(true);
        leftArrow.GetOrAddComponent<MouseEventSystem>().MouseEvent+= leftMouseEvent;
        rightArrow.SetActive(true);
        rightArrow.GetOrAddComponent<MouseEventSystem>().MouseEvent+= rightMouseEvent;
#endif
        }
#if (UNITY_ANDROID || UNITY_IOS) && HAS_EVENTS
        void leftMouseEvent(GameObject target, MouseEventType type)
        {
            if (type == MouseEventType.DOWN)
            {
                goRight = false;
                goLeft = true;
            }
            if (type == MouseEventType.UP)
            {
                goLeft = false;
            }
        }

        void rightMouseEvent(GameObject target, MouseEventType type)
        {
            if (type == MouseEventType.DOWN)
            {
                goLeft = false;
                goRight = true;
            }
            if (type == MouseEventType.UP)
            {
                goRight = false;
            }
        }
#endif
        /// <summary>
        /// Adds a "jump" by maxing out horizontal velocity based on jumpMaxForce
        /// </summary>
        public void addJump()
        {
            if (body.velocity.y < 0)
                body.velocity = Vector2.zero;
            //float v = (jumpMaxForce/body.mass)*Time.fixedDeltaTime;
            float currentVelocityForce = (body.velocity.y / Time.fixedDeltaTime) * body.mass;
            if (jumpMaxForce - currentVelocityForce > 0)
            {
                //Debug.Log("Will add:" + (jumpMaxForce - currentVelocityForce) + " force.");
                body.AddForce(new Vector2(0f, jumpMaxForce - currentVelocityForce));
            }
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {            
#if UNITY_ANDROID || UNITY_IOS
            foreach (string exceptionName in exceptionNames)
            {
                if (collider.name.Contains(exceptionName))
                    return;
            }
#endif
            if (RocketCharTriggered != null)
                RocketCharTriggered(this, collider);
        }

        private void OnCollisionEnter2D(Collision2D col)
        {
            if (RocketCharHit != null)
                RocketCharHit(this,col.collider, col);
        }        

        protected virtual void FixedUpdate()
        {
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || goLeft)
            {
                moveLeft();
            }
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D) || goRight)
            {
                moveRight();
            }
        }
        /// <summary>
        /// Move character left by 1 increment (based on horizontalSpeed)
        /// </summary>
        public void moveLeft()
        {
            if (!canMove)
                return;
            float horizontalChange = horizontalSpeed * Time.deltaTime;
            Vector3 cPosition = gameObject.transform.position;
            if (cPosition.x - horizontalChange >= minX)
                cPosition.x -= horizontalChange;
            gameObject.transform.position = cPosition;
        }
        /// <summary>
        /// Move character right by 1 increment (based on horizontalSpeed)
        /// </summary>
        public void moveRight()
        {
            if (!canMove)
                return;
            float horizontalChange = horizontalSpeed * Time.deltaTime;
            Vector3 cPosition = gameObject.transform.position;
            if (cPosition.x + horizontalChange <= maxX)
                cPosition.x += horizontalChange;
            gameObject.transform.position = cPosition;
        }
    }
}