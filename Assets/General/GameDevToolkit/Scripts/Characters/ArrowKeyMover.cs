﻿using UnityEngine;
using System.Collections;
#if (UNITY_ANDROID || UNITY_IOS) && HAS_EVENTS
using GameDevToolkit.Events;
#endif

namespace GameDevToolkit.Characters
{
    /// <summary>
    /// A character can move in 4 directions (up, down,left, right). Any direction can be enabled or disabled
    /// </summary>
    public class ArrowKeyMover : MonoBehaviour
    {
        /// <summary>
        /// Wether character can move in any direction
        /// </summary>
        public bool canMove=true;
        /// <summary>
        /// Wether character can move up
        /// </summary>
        public bool canMoveUp=false;
        /// <summary>
        /// Wether character can move down
        /// </summary>
        public bool canMoveDown=false;
        /// <summary>
        /// Wether character can move right
        /// </summary>
        public bool canMoveRight=false;
        /// <summary>
        /// Wether character can move down
        /// </summary>
        public bool canMoveLeft=false;
        public float horizontalSpeed;
        public float verticalSpeed;
        
        public float minX;
        public float maxX;
        public float minY;
        public float maxY;
#if (UNITY_ANDROID || UNITY_IOS) && HAS_EVENTS
        public Collider2D upArrow;
        public Collider2D downArrow;
        public Collider2D leftArrow;
        public Collider2D rightArrow;
#endif

        private bool goLeft = false;
        private bool goRight = false;
        private bool goUp = false;
        private bool goDown = false;
#if (UNITY_ANDROID || UNITY_IOS) && HAS_EVENTS
        private void Start()
        {

            if (upArrow != null)
            {
                upArrow.gameObject.SetActive(true);
                upArrow.gameObject.GetOrAddComponent<MouseEventSystem>().MouseEvent += UpArrowEvent;
            }
            if (downArrow != null)
            {
                downArrow.gameObject.SetActive(true);
                downArrow.gameObject.GetOrAddComponent<MouseEventSystem>().MouseEvent += DownArrowEvent;
            }
            if (leftArrow != null)
            {
                leftArrow.gameObject.SetActive(true);
                leftArrow.gameObject.GetOrAddComponent<MouseEventSystem>().MouseEvent += LeftArrowEvent;
            }
            if (rightArrow != null)
            {
                rightArrow.gameObject.SetActive(true);
                rightArrow.gameObject.GetOrAddComponent<MouseEventSystem>().MouseEvent += RightArrowEvent;
            }
        }

        void UpArrowEvent (GameObject target, MouseEventType type)
        {
            if (type == MouseEventType.UP)
            {
                goUp = false;
            }
            else if (type == MouseEventType.DOWN)
            {
                goUp = true;
            }
        }
        void DownArrowEvent (GameObject target, MouseEventType type)
        {
            if (type == MouseEventType.UP)
            {
                goDown = false;
            }
            else if (type == MouseEventType.DOWN)
            {
                goDown = true;
            }
        }
        void LeftArrowEvent (GameObject target, MouseEventType type)
        {
            if (type == MouseEventType.UP)
            {
                goLeft = false;
            }
            else if (type == MouseEventType.DOWN)
            {
                goLeft = true;
            }
        }
        void RightArrowEvent (GameObject target, MouseEventType type)
        {
            if (type == MouseEventType.UP)
            {
                goRight = false;
            }
            else if (type == MouseEventType.DOWN)
            {
                goRight = true;
            }
        }
#endif

        private void Update()
        {
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || goLeft)
            {
                moveLeft();
            }
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D) || goRight)
            {
                moveRight();
            }
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || goUp)
            {
                moveUp();
            }
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) || goDown)
            {
                moveDown();
            }
        }

        public void moveLeft()
        {
            if (!canMove || !canMoveLeft)
                return;
            float horizontalChange = horizontalSpeed * Time.deltaTime;
            Vector3 cPosition = gameObject.transform.position;
            if (cPosition.x - horizontalChange >= minX)
                cPosition.x -= horizontalChange;
            gameObject.transform.position = cPosition;
        }

        public void moveRight()
        {
            if (!canMove || !canMoveRight)
                return;
            float horizontalChange = horizontalSpeed * Time.deltaTime;
            Vector3 cPosition = gameObject.transform.position;
            if (cPosition.x + horizontalChange <= maxX)
                cPosition.x += horizontalChange;
            gameObject.transform.position = cPosition;
        }

        public void moveUp()
        {
            if (!canMove || !canMoveUp)
                return;
            float verticalChange = verticalSpeed * Time.deltaTime;
            Vector3 cPosition = gameObject.transform.position;
            if (cPosition.y + verticalChange <= maxY)
                cPosition.y += verticalChange;
            gameObject.transform.position = cPosition;
        }

        public void moveDown()
        {
            if (!canMove || !canMoveDown)
                return;
            float verticalChange = verticalSpeed * Time.deltaTime;
            Vector3 cPosition = gameObject.transform.position;
            if (cPosition.y - verticalChange >= minY)
                cPosition.y -= verticalChange;
            gameObject.transform.position = cPosition;
        }
    }
}