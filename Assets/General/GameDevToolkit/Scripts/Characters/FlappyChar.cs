using UnityEngine;
using System.Collections;
using GameDevToolkit.Common;

namespace GameDevToolkit.Characters
{
    /// <summary>
    /// Flappy bird character
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class FlappyChar: MonoBehaviour
    {
        /// <summary>
        /// GameObjects with a collider and a script that has its name in this array will be ignored when screen is being clicked/tapped.
        /// </summary>
        public string[] hitExceptionScriptNames = new string[] { "GameCloseButton" };
        /// <summary>
        /// Fource to add when screen is clicked/tapped
        /// </summary>
        public float forceToAdd = 2f;
        /// <summary>
        /// Horizontal speed
        /// </summary>
        public float speed = 4f;
        /// <summary>
        /// If character is "dead" it will not move (rigidbody will be set to kinematic);
        /// </summary>
        public bool dead = true;
        private Rigidbody2D body;
        protected void Awake()
        {
            body = gameObject.GetComponent<Rigidbody2D>();
        }
        /// <summary>
        /// Call this function to "start" the character.
        /// </summary>
        public void Init()
        {
            dead = false;
        }

        private void Update()
        {
            if (dead)
            {
                if (!body.isKinematic)
                {
                    body.isKinematic = true;
                    body.simulated = false;
                }
                return;
            }
            else
            {
                body.isKinematic = false;
                body.simulated = true;
            }

            if (Input.GetMouseButtonDown(0))
            {
                Vector3 position = EDUCam.Instance.mousePosition;
                Ray ray = Camera.main.ScreenPointToRay(position);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
                if (hit.collider != null)
                {
                    foreach(string scriptName in hitExceptionScriptNames)
                    {
                        Component component = hit.collider.gameObject.GetComponent(scriptName);
                        if (component != null)
                            return;
                    }
                }
                body.AddForce(new Vector2(0f, forceToAdd));
            }
#if UNITY_ANDROID || UNITY_IOS
            if(Input.touchCount>0)
            {
                foreach(Touch touch in Input.touches)
                {
                    if(touch.phase==TouchPhase.Ended)
                    {
                        Vector3 position=EDUCam.Instance.mousePosition;
                        Ray ray = Camera.main.ScreenPointToRay(position);
                        RaycastHit2D hit=Physics2D.Raycast(ray.origin,ray.direction);
                        if(hit.collider != null)
                        {
                            foreach (string scriptName in hitExceptionScriptNames)
                            {
                                Component component = hit.collider.gameObject.GetComponent(scriptName);
                                if (component != null)
                                    return;
                            }
                        }
                        body.AddForce(new Vector2(0f, forceToAdd));
                        body.AddForce(new Vector2(0f,forceToAdd));
                        return;
                    }

                }
            }
#endif
        }

        /// <summary>
        /// Forces constant hotrizontal speed.
        /// Body Rotation is clamped between -45 and +45 degrees.
        /// </summary>
        private void LateUpdate()
        {
            if (dead)
                return;
            body.velocity = new Vector2(speed, body.velocity.y);
            body.rotation = Mathf.Clamp(body.rotation, -45f, 45f);
        }
    }
}