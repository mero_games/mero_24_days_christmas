using UnityEngine;
using System.Collections;
using System;

namespace GameDevToolkit.Visual
{
	public class Geom
	{
		/// <summary>
        /// Calls <see cref="GameDevToolkit.Visual.Geom.launchObj(Rigidbody2D, Vector3, Vector3, float)"/>
        /// There for given GameObject must have a RigidBody2D attached, or this will fail.
        /// </summary>
        /// <param name="objToLaunch"></param>
        /// <param name="launchPoint"></param>
        /// <param name="target"></param>
        /// <param name="velocity"></param>
		public static void launchObj(GameObject objToLaunch, Vector3 launchPoint, Vector3 target, float velocity)
		{
			if(objToLaunch.GetComponent<Rigidbody2D>() == null) throw new Exception("You can only launch an object with rigidbody2D!");
            launchObj(objToLaunch.GetComponent<Rigidbody2D>(), launchPoint, target, velocity);
		}

        /// <summary>
        /// Launches the given body  from launchPoint towards target using given velocity
        /// </summary>
        /// <param name="objToLaunch"></param>
        /// <param name="launchPoint"></param>
        /// <param name="target"></param>
        /// <param name="velocity"></param>
        public static void launchObj(Rigidbody2D objToLaunch, Vector3 launchPoint, Vector3 target, float velocity)
        {
            Vector2 direction = new Vector2(target.x, target.y);
            direction = direction - new Vector2(launchPoint.x, launchPoint.y);
            direction.Normalize();
            direction = Vector2.Scale(direction, new Vector2(velocity, velocity));
            objToLaunch.AddForce(direction);
        }
        /// <summary>
        /// Normalize degrees given in Unity format (that may be smnaller than 0 and greater than 360) to 360 degrees rotation of same actual value.
        /// May need some correction to handle rotations that are over 360, or under 0 by multiple factors
        /// </summary>
        /// <param name="rot"></param>
        /// <returns></returns>
        public static float normalRotation(float rot)
		{
			if(rot < 0)
			{
				return 360+rot;
			}else{
				return rot;
			}
		}        

        /// <summary>
        /// Calculates the central point on segment formed by the given points
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static Vector3 getMidPoint(Vector3 p1, Vector3 p2)
        {
            Vector3 result = new Vector3((p1.x + p2.x) / 2f, (p1.y + p2.y) / 2f, (p1.z + p2.z) / 2f);
            return result;
        }
	}
}