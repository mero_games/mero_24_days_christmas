using UnityEngine;
using System.Collections;
using GameDevToolkit.GameUtil;
using GameDevToolkit.Common;

namespace GameDevToolkit.Visual
{
    /// <summary>
    /// Explicitly defined dummy class for MultiDimensionalArray<ObjectSequence>
    /// </summary>
    [System.Serializable]
    public class TwoDSequenceArray : TwoDArray<ObjectSequence>
    {
        public TwoDSequenceArray(int size):base(size)
        {

        }
    }
}