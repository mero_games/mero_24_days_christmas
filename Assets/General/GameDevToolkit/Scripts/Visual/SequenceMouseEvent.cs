#if HAS_EVENTS
using GameDevToolkit.Events;
#endif
using GameDevToolkit.Visual;
using UnityEngine;
namespace GameDevToolkit.Visual
{
#if HAS_EVENTS
    /// <summary>
    /// Predfined GenericMouseEvent with ObjectSequence target type.
    /// </summary>
    public class SequenceMouseEvent : GenericMouseEvent<ObjectSequence>{}
#else
    public class SequenceMouseEvent : MonoBehaviour { }
#endif
}