﻿using UnityEngine;
using UnityEngine.UI;
namespace GameDevToolkit.Visual
{ 
    /// <summary>
    /// Editor Utility script to determine the actual fontSize of used by an attached UI text that uses Best Fit
    /// </summary>
    [ExecuteInEditMode]
    public class ActualFontSize : MonoBehaviour {

	    [ReadOnly]public float FontSize;// Use this for initialization
	    void Start () {
	
	    }
	
	    // Update is called once per frame
	    void Update () {
		    FontSize=GetComponent<Text> ().cachedTextGenerator.fontSizeUsedForBestFit;
	    }
    }
}