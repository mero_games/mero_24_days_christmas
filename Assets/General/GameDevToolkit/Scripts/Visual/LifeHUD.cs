using UnityEngine;
using System.Collections;
namespace GameDevToolkit.Visual
{
    /// <summary>
    /// Used for prefab created in Editor. Used for monitoring lives in games.
    /// </summary>
    public class LifeHUD : MonoBehaviour
    {
        public SpriteRenderer[] hearts;
        [ReadOnly]public int currentLifes;
		
		private void Awake(){
			currentLifes = hearts.Length;
		}
		
        public int decreaseLifes()
        {
            currentLifes--;
            if (currentLifes < 0)
            {
                currentLifes = 0;
                Debug.LogError("Lives tryed to diip below 0. Bug?");
            }
            hearts [currentLifes].enabled = false;
            return currentLifes;
        }
       
    }
}