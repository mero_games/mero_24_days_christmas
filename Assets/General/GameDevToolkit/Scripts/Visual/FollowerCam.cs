using UnityEngine;
using System.Collections;
namespace GameDevToolkit.Visual
{
    /// <summary>
    /// Follows to given target on specified axis
    /// </summary>
    public class FollowerCam : MonoBehaviour
    {
        public Transform target;
        public bool followOnY = false;
        public bool followOnX = false;
        public bool constrainedOnX = false;
        public bool constrainedOnY = false;
        public float minY = 0f;
        public float maxY = 0f;
        public float minX = 0f;
        public float maxX = 0f;
        [ReadOnly]
        public Vector3 offset;

        private Vector3 originalPosition;
        private void Start()
        {
            originalPosition = transform.position;
            offset = transform.position - target.position;
        }

        private void Update()
        {
            if ((!followOnX && !followOnY) || target==null)
                return;
            transform.position = target.position + offset;
            Vector3 normalizedPosition = transform.position;
            if (followOnX)
            {
                if (constrainedOnX)
                {
                    normalizedPosition.x = Mathf.Clamp(normalizedPosition.x, minX, maxX);
                }
            }
            else
                normalizedPosition.x = originalPosition.x;
            if (followOnY)
            {
                if (constrainedOnY)
                {
                    normalizedPosition.y = Mathf.Clamp(normalizedPosition.y, minY, maxY);
                }
            }
            else
                normalizedPosition.y = originalPosition.y;
            normalizedPosition.z = originalPosition.z;
            transform.position = normalizedPosition;

        }
    }
}