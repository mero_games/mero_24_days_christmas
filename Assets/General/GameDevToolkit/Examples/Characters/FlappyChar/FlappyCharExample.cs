using GameDevToolkit.Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(FlappyChar))]
public class FlappyCharExample : MonoBehaviour
{
    private FlappyChar character;
    private void Start()
    {
        character = gameObject.GetComponent<FlappyChar>();
        StartCoroutine(doStart(3f));
    }

    private IEnumerator doStart(float delay)
    {
        yield return new WaitForSeconds(delay);
        character.Init();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Square bird is dead!");
        character.dead = true;
    }

    private void Update()
    {
        if (character.dead)
            return;
        if(transform.position.x>=22f)
        {
            Debug.Log("Square bird finished level!");
            character.dead = true;
        }
    }
}