using GameDevToolkit.Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketCharExample : MonoBehaviour
{
    public RocketChar character;
    private void Start()
    {
        character.RocketCharTriggered += Character_RocketCharTriggered;
        character.RocketCharHit += Character_RocketCharHit;
        character.addJump();        
    }

    private void Update()
    {
        if (!character.canMove)
            return;
        if(character.transform.position.y>=33f)
        {
            disableCharacter();
            Debug.Log("Game finished!");
        }
    }

    private void Character_RocketCharHit(RocketChar arg0, Collider2D arg1, Collision2D arg2)
    {
        Debug.Log("rocket char dead!");
        disableCharacter();
    }

    private void disableCharacter()
    {
        character.canMove = false;
        character.body.bodyType = RigidbodyType2D.Kinematic;
        character.body.simulated = false;
    }

    private void Character_RocketCharTriggered(RocketChar arg0, Collider2D arg1)
    {
        Debug.Log("Adding jump force!");
        character.addJump();
        Destroy(arg1.gameObject);
    }
}