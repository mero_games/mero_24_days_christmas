using GameDevToolkit.Visual;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchObjectExample : MonoBehaviour
{
    public Transform cannon;
    public GameObject projectileTemplate;
    private bool canShoot=true;
    private GameObject projectile;
    private void Update()
    {
        Vector3 mPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        cannon.LookAt2D(mPosition);
        if(Input.GetMouseButton(0) && canShoot)
        {
            canShoot = false;
            projectile = Instantiate(projectileTemplate);
            projectile.transform.position = cannon.position;
            Geom.launchObj(projectile, projectile.transform.position, mPosition, 300f);
            StartCoroutine(Reset());
        }
    }

    private IEnumerator Reset()
    {
        yield return new WaitForSeconds(5f);
        Destroy(projectile);
        canShoot = true;
        Debug.Log("You can shoot again!");
    }
}