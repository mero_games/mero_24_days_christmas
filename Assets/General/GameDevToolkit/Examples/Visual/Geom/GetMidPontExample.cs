using GameDevToolkit.Visual;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetMidPontExample : MonoBehaviour
{
    public Transform point1;
    public Transform point2;

    private void Start()
    {
        GameObject center = Instantiate(point1.gameObject);
        center.transform.position = Geom.getMidPoint(point1.position, point2.position);
    }
}