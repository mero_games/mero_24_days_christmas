using GameDevToolkit.Visual;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSequenceExample : MonoBehaviour
{
    public ObjectSequence manualChange;

    private void Start()
    {
        manualChange.CurrentChildChanged += ManualChange_CurrentChildChanged;
        //There is more than one way of changing the active child:
        manualChange.CurrentChildIndex = 0;
        int setTo = 2;
        //The following line will not work since setTo is int and CurrentChildIndex is int.
        //manualChange.CurrentChildIndex = setTo;
        //To avoid the need of conversion you can use:
        manualChange.setCurrentChildIndex(setTo);
        Debug.Log("Current child index is:" + manualChange.CurrentChildIndex + " The childs name is:" + manualChange.CurrentChild.name);
    }

    private void ManualChange_CurrentChildChanged(GameObject parent, uint index)
    {
        Debug.Log("Changed " + parent.name + " to index:" + index);
    }
}