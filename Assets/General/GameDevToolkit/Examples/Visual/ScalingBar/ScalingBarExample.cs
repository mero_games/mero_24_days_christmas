using GameDevToolkit.Visual;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScalingBarExample : MonoBehaviour
{
    public ScalingBar progressBar;

    private void Update()
    {
        if (progressBar.precentage >= 100)
            progressBar.precentage = 0;
        else
            progressBar.precentage++;
    }
}