using GameDevToolkit.Visual;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeHUDExample : MonoBehaviour
{
    public LifeHUD lifes;

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Space))
        {
            if (lifes.decreaseLifes() == 0)
                Debug.Log("end game!");
        }

    }
}