using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Used in examples. Where this component is used, any other component could be used instead of it!
/// </summary>
public class ArbitraryComponent : MonoBehaviour
{
    private void Awake()
    {
        Debug.Log("Created instance!");
    }

    public void doSomething()
    {
        Debug.Log("Doing something with: " + this.GetInstanceID());
    }
}