using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevToolkit.Data;
using UnityEngine.UI;
using System;

public class HTTPLoaderExample : MonoBehaviour
{
    public InputField urlField;
    public InputField resultField;
    public Button loadBtn;
    private void Start()
    {
        loadBtn.onClick.AddListener(doLoad);
    }

    private void doLoad()
    {
        if(string.IsNullOrEmpty(urlField.text))
        {
            Debug.Log("URL Field is mandatory!");
            return;
        }
        HTTPLoader loader = new HTTPLoader(urlField.text);
        loader.LoadComplete += doLoadComplete;
        StartCoroutine(loader.load());
        loadBtn.interactable = false;
    }

    private void doLoadComplete(HTTPLoader.HTTPResult result)
    {
        loadBtn.interactable = true;
        if (!string.IsNullOrEmpty(result.error))
        {
            resultField.text = result.error;
        }
        else
        {
            resultField.text = result.content;
        }
    }
}