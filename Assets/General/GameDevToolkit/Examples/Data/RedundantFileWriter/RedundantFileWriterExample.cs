using GameDevToolkit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedundantFileWriterExample : MonoBehaviour
{
    private void Start()
    {
        RedundantFileWriter writer = RedundantFileWriter.getInstance();
        writer.WriteFile("test.txt", "hello world");
        System.Diagnostics.Process.Start("explorer.exe", ".");
    }
}