using GameDevToolkit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JSONSaveFileExample : MonoBehaviour {

    private JSONSaveFile saveFile;

    void Start ()
    {
        //Create instance
        saveFile = new JSONSaveFile("GameDevToolkitExample", "actualFileNameOnFileSystem", true, "mySuperSecretKey");
        //Check if file already existed or was just created
        if(saveFile.newSaveFile)
        {
            //if created define default values
            saveFile["myIntFlag"].AsInt = 0;
            saveFile["myFloatFlag"].AsFloat = 47f;
            saveFile["myBoolFlag"].AsBool = false;
            saveFile["myVectorFlag"].AsVector3 = Vector3.down;
            saveFile["myStringFlag"].Value = "Hello world!";
            saveFile.flush();//write data to disk;
        }
        //Changeing flags does not have to be in Start, it can be anywhere.
        //For data to be written to disk flush() has to be called;
        //increment myIntFlag by 1 and invert myBoolFlag
        saveFile["myIntFlag"].AsInt += 1;
        saveFile["myBoolFlag"].AsBool = !saveFile["myBoolFlag"].AsBool;
        saveFile.flush();

        Debug.Log(saveFile["myIntFlag"].AsInt);
        Debug.Log(saveFile["myFloatFlag"].AsFloat);
        Debug.Log(saveFile["myBoolFlag"].AsBool);
        Debug.Log(saveFile["myVectorFlag"].AsVector3);
        Debug.Log(saveFile["myStringFlag"].Value);
    }


}