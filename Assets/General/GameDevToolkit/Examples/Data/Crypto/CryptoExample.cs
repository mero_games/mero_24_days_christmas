using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameDevToolkit.Data;
public class CryptoExample : MonoBehaviour
{
    public InputField keyField;
    public InputField sourceTextField;
    public InputField encryptedTextField;
    public Button encryptBtn;
    public Button decryptBtn;

    private void Start()
    {
        encryptBtn.onClick.AddListener(doEncrypt);
        decryptBtn.onClick.AddListener(doDencrypt);
    }

    private void doEncrypt()
    {
        if(string.IsNullOrEmpty(keyField.text) || string.IsNullOrEmpty(sourceTextField.text))
        {
            Debug.Log("Both the \"Encryption key\" field and the \"Text\" fields are mandatory!");
            return;
        }
        encryptedTextField.text = Crypto.Encrypt(sourceTextField.text, keyField.text);
    }

    private void doDencrypt()
    {
        if (string.IsNullOrEmpty(keyField.text) || string.IsNullOrEmpty(sourceTextField.text))
        {
            Debug.Log("Both the \"Encryption key\" field and the \"Text\" fields are mandatory!");
            return;
        }
        encryptedTextField.text = Crypto.Decrypt(sourceTextField.text, keyField.text);
    }
}