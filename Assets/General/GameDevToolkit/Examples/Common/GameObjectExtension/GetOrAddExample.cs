using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetOrAddExample : MonoBehaviour
{
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            gameObject.GetOrAddComponent<ArbitraryComponent>().doSomething();
        }
    }
}