using GameDevToolkit.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayUtilsExample : MonoBehaviour
{
    public GameObject[] exampleArrayForPrintAndToString;
    public List<GameObject> exampleListForPrintAndToString;
    void Start ()
    {
        #region RANDOMIZE AND RANDOMIZERSAMEORDER EXAMPLES
        string[] exampleArrayForRandomize = new string[] { "John", "Bob", "Larry", "Bill" };
        int[] exampleArrayForRandomizeSameOrder = new int[] { 1, 2, 3, 4 };
        Debug.Log("Array before randomize:" + ArrayUtils.ArrayToString<string>(exampleArrayForRandomize));
        Debug.Log("Int Array before randomizeSameOrder:" + ArrayUtils.ArrayToString<int>(exampleArrayForRandomizeSameOrder));

        ArrayUtils.randomize<string>(ref exampleArrayForRandomize);
        ArrayUtils.randomizeSameOrder<int>(ref exampleArrayForRandomizeSameOrder);

        Debug.Log("Array after randomizer:" + ArrayUtils.ArrayToString<string>(exampleArrayForRandomize));
        Debug.Log("Int Array After randomizeSameOrder:" + ArrayUtils.ArrayToString<int>(exampleArrayForRandomizeSameOrder));
        #endregion
        #region PRINTARRAY EXAMPLE
        string output = ArrayUtils.ArrayToString<GameObject>(exampleArrayForPrintAndToString);
        Debug.Log(output);
        ArrayUtils.printArray<GameObject>(exampleArrayForPrintAndToString);
        output = ArrayUtils.ArrayToString<GameObject>(exampleListForPrintAndToString.ToArray());
        Debug.Log(output);
        #endregion
        #region Array comparison examples
        int[] array1 = new int[] { 1, 2, 3 };
        int[] array2 = new int[] { 3, 1, 2 };
        int[] array3 = new int[] { 3, 3, 1, 2 };
        int[] array4 = new int[] { 2,3, 4, 5, 6, 7 };
        bool firstComparison = ArrayUtils.compareArrays<int>(array1, array2);
        bool secondComparison = ArrayUtils.compareOrderedArrays<int>(array1, array2);
        bool thirdComparison = ArrayUtils.compareArrays<int>(array2, array3);
        List<int> commonElements = ArrayUtils.getCommonElements<int>(array1, array4);
        string commentElementsAsString = ArrayUtils.ArrayToString<int>(commonElements.ToArray());
        Debug.Log("compareArrays between array1 and array2 is: " + firstComparison);
        Debug.Log("compareOrderedArrays between array1 and array2 is: " + secondComparison);
        Debug.Log("compareArrays between array2 and array3 is: " + thirdComparison);
        Debug.Log("Common elements between array3 and array4 are: " + commentElementsAsString);
        #endregion
    }
}