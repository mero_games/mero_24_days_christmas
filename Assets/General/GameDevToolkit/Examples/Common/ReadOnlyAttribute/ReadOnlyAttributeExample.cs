using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadOnlyAttributeExample : MonoBehaviour
{
    public float normalField=8f;
    [ReadOnly]public float readOnlyField = 9f;
}