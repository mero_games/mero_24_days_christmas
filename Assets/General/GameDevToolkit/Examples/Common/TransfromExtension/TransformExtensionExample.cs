using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevToolkit.Common;

public class TransformExtensionExample : MonoBehaviour
{
    public Transform goodLookAtChar;
    public Transform badLookAtChar;
    private const int LOG_RATE_LIMIT = 30;
    private int currentLogRate = 0;
    private void Update()
    {
        currentLogRate++;
        if (currentLogRate == int.MaxValue)
            currentLogRate = 0;
        Vector3 lookAtTarget = Input.mousePosition;
        lookAtTarget = Camera.main.ScreenToWorldPoint(lookAtTarget);
        if (currentLogRate % LOG_RATE_LIMIT == 0)
        {
            Debug.Log("Rotating to:" + goodLookAtChar.GetLookRotaion2D(lookAtTarget));
        }
        goodLookAtChar.LookAt2D(lookAtTarget);
        //With our without setting the Z axis to the same plane as you sprites LookAt will not work
        //lookAtTarget.z = 0f;
        badLookAtChar.LookAt(lookAtTarget);
    }

    public void rotationExample()
    {
        //to add 90 degrees to an objects 2d rotation (on Z axis) you nomraly would do:
        Vector3 currentRotation = transform.eulerAngles;
        currentRotation.z += 90;
        transform.eulerAngles = currentRotation;
        //Now you can instead do:
        transform.set2DRotation(transform.get2DRotation() + 90);
    }
}
	