#pragma warning disable 219
using GameDevToolkit.GameUtil;
using GameDevToolkit.GameUtil.RandomizerRules;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizerExamples : MonoBehaviour
{
    private void Start()
    {
        //Creating a Randomizer that will give numbers between 1 and 10 the simplest way possible(Will use DefaultRandomRule):
        Randomizer random = new Randomizer(1, 10);
        //Creating a Randomizer with the same range but a specific rule:
        Randomizer random1 = Randomizer.CreateRandomizer<DefaultRandomRule>(1, 10);
        //The DefaultRandomRule is that the Randomizer can never repeat the same number (unless the rule is reset)
        //The other predefined rule is SemiRandomRule this permits numbers to repeat but not in a row.
        //For example with SemiRandomRule 1,3,1 can occur, but 1,1,3 will not.

        //Getting a random number:
        Debug.Log("One random number:"+random1.getRandom());
        //With DefaultRandomRule you must take care how many numbers you request.
        //Since number cannot repeat the Randomizer can and will run out of numbers if to many are requested.
        //For example:
        for (int i = 0; i < 11; i++)
        {
            try
            {
                Debug.Log("Random number in a loop: " + random.getRandom() + " where i=" + i);
            }
            catch
            {
                Debug.Log("Ran out of numbers!");
            }
        }
        //You can avoid the error like this:
        for (int i = 0; i < 11; i++)
        {
            if (!random.randomRule.hasNumbersLeft())
                random.randomRule.Reset();
            Debug.Log("Random number in a loop: " + random.getRandom() + " where i=" + i);
        }
        //This ofcourse means that after Reset() number can repeat!
        //Randomizer is used many times to give random elements from an array. To help with this
        //There are special/shortcut constructors defined.
        //Examples:
        string[] arbitraryContent = new string[] { "hello", "good","morning","afternoon", "world" };
        List<string> arbitraryContentList = new List<string>(new string[] { "hello", "good", "morning", "afternoon", "world" });
        Randomizer random2 = new Randomizer(arbitraryContent);
        Randomizer random3 = new Randomizer(arbitraryContentList);
        //Same goes for the CreateInstance function:
        Randomizer random4 = Randomizer.CreateRandomizer<DefaultRandomRule>(arbitraryContent);
        Randomizer random5 = Randomizer.CreateRandomizer<DefaultRandomRule>(arbitraryContentList);
        //And ofcourse getting a random element form the given array:
        Debug.Log("Random string from array:" + arbitraryContent[random2.getRandom()]);
    }
}