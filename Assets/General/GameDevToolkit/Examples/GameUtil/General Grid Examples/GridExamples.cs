using GameDevToolkit.Common;
using GameDevToolkit.GameUtil;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridExamples : MonoBehaviour
{
    public GridRep grid;

    private void Start()
    {
        //Accessing a cell at a given position
        GameObject topLeftCell = grid[0][0];
        //Getting the position of a known cell
        GridPoint topLeftPosition = grid.getPositionOfCell(topLeftCell); // In this case this could be new GridPoint(0,0);
        //Getting neighbors of a known position:
        GridPoint[] directNeighborPositions = grid.getNeighbors(topLeftPosition); //Will be 0_1 and 1_0
        GridPoint[] allNeighborPositions = grid.getNeighbors(topLeftPosition, true); //Will be 0_1 and 1_0 as well as 1_1
        //Accessing multiple chidren based on there positions
        GameObject[] directNeighbors = grid.getCells(directNeighborPositions);
        GameObject[] allNeighbors = grid.getCells(allNeighborPositions);
        string directNeighborsString = ArrayUtils.ArrayToString<GameObject>(directNeighbors);
        string allNeighborsString = ArrayUtils.ArrayToString<GameObject>(allNeighbors);
        Debug.Log("Neighbors of 0_0: " + directNeighborsString);
        Debug.Log("All neighbors (including diagonals) of 0_0: " + allNeighborsString);
        //Shortuct to check if a given grid coordinate is valid
        GridPoint goodPosition = new GridPoint(4, 1);
        GridPoint badPosition = new GridPoint(5, 1);
        Debug.Log(goodPosition.ToString() + " Exists:" + grid.Exists(goodPosition) + " " + badPosition.ToString() + " Exists:" + grid.Exists(badPosition));
        //Check if a coordinate has a cell in a certain direction:
        Debug.Log(goodPosition.ToString() +" hasBelow: "+ grid.hasBelow(goodPosition));
        Debug.Log(goodPosition.ToString() + "hasAbove" + grid.hasAbove(goodPosition));
        Debug.Log(goodPosition.ToString() + "hasToLeft" + grid.hasToLeft(goodPosition));
        Debug.Log(goodPosition.ToString() + "hasToRight" + grid.hasToRight(goodPosition));
        //Get a cell in a given direction:
        grid.getAbove(topLeftCell);
        grid.getAbove(new GridPoint(0, 0));
        grid.getBelow(topLeftPosition);
        grid.getToRight(goodPosition);
        grid.getToLeft(goodPosition);
        //IMPORTANT: getPositionAbove getPositionBelow etc are also defined and they return a GridPoint instead of a cell
        //loop trough all cells:
        foreach (GameObject cell in grid)
        {
            cell.ToString();
        }


    }

    
}