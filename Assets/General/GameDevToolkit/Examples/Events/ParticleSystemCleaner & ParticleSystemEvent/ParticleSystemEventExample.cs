using GameDevToolkit.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemEventExample : MonoBehaviour
{
    /// <summary>
    /// Note that ParticleSystemCleaner extends ParticleSystemEvent. So all code here works the same for both components.
    /// (Other than the variable type obviously)
    /// </summary>
    public ParticleSystemCleaner pSystem;

    private void Start()
    {
        pSystem.ParticleSystemDone += PSystem_ParticleSystemDone;
    }

    private void PSystem_ParticleSystemDone(ParticleSystemEvent target)
    {
        Debug.Log("Particle system done!");
    }
}