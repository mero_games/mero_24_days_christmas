using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericMouseEventExample : MonoBehaviour
{
    public GameObject square;

    private void Start()
    {
        square.AddComponent<DummyMouseEventImplementation>().MouseEvent += GenericMouseEventExample_MouseEvent;
    }

    private void GenericMouseEventExample_MouseEvent(ArbitraryComponent target, GameDevToolkit.Events.MouseEventType type)
    {
        if(type== GameDevToolkit.Events.MouseEventType.CLICK)
        {
            target.doSomething();
        }
    }
}