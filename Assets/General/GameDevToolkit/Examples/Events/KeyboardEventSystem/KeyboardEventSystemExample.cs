using GameDevToolkit.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardEventSystemExample : MonoBehaviour
{
	void Start ()
    {
        KeyboardEventSystem kBoard = KeyboardEventSystem.getInstance();
        kBoard.addKey(new KeyCode[] { KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.LeftArrow, KeyCode.RightArrow });
        kBoard.addMap(KeyCode.W, KeyCode.UpArrow);
        kBoard.addMap(KeyCode.A, KeyCode.LeftArrow);
        kBoard.addMap(KeyCode.S, KeyCode.DownArrow);
        kBoard.addMap(KeyCode.D, KeyCode.RightArrow);
        kBoard.KeyBoardEvent += KBoard_KeyBoardEvent;
    }

    private void KBoard_KeyBoardEvent(KeyboardEventType keyboardEventType, KeyCode keyCode)
    {
        if (keyboardEventType == KeyboardEventType.DOWN)
            Debug.Log("User pressing:" + keyCode);
    }
}