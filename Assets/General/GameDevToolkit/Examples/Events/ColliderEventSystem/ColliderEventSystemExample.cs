using GameDevToolkit.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderEventSystemExample : MonoBehaviour
{
    public GameObject square;

    private void Start()
    {
        square.AddComponent<ColliderEventSystem>().ColliderEntered += collisionStart;
        square.AddComponent<ColliderEventSystem>().ColliderExited += collisionFinish;
        square.AddComponent<ColliderEventSystem>().TriggerEntered += triggerStart;
        square.AddComponent<ColliderEventSystem>().TriggerExited += triggerFinish;
    }

    private void triggerFinish(ColliderEventSystem eventTarget, Collider2D other)
    {
        Debug.Log(eventTarget.name + " stoped overlappoing with the trigger called: " + other.name);
    }

    private void triggerStart(ColliderEventSystem eventTarget, Collider2D other)
    {
        Debug.Log(eventTarget.name + " started overlappoing with the trigger called: " + other.name);
    }

    private void collisionFinish(ColliderEventSystem eventTarget, Collider2D other)
    {
        Debug.Log(eventTarget.name + " stoped overlappoing with the collider called: " + other.name);
    }

    private void collisionStart(ColliderEventSystem eventTarget, Collider2D other)
    {
        Debug.Log(eventTarget.name + " started overlappoing with the collider called: " + other.name);
    }

    private void Update()
    {
        Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        position.z = 0f;
        square.transform.position = position;
    }
}