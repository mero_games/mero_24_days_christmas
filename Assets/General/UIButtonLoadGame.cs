using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonLoadGame : UIButton
{
    public int sceneToLoad;

    private bool isOpen;
    public bool notDone;

    //private GameObject frameFull;
    //private GameObject frameEmpty;
    //private GameObject unavailableText;
    //private GameObject playButton;
    //private GameObject background;
    GameObject bg;

    private void Start()
    {
        base.OnClickUp += HandleMouseDown;
        EventManager.OnWindowClicked += HandleWindowClicked;
        bg = transform.GetChild(0).gameObject;
        //background = transform.GetChild(0).gameObject;
        //unavailableText = transform.GetChild(1).gameObject;
        //playButton = transform.GetChild(2).gameObject;
        //frameFull = transform.GetChild(3).gameObject;
        //frameEmpty = transform.GetChild(4).gameObject;
    }

    private void OnDestroy()
    {
        base.OnClickUp -= HandleMouseDown;
        EventManager.OnWindowClicked -= HandleWindowClicked;
    }

    private void HandleMouseDown()
    {
        if (GetComponentInParent<ScrollRect>().enabled == true)
        {
            if (CanLoad())
            {
                if (!isOpen)
                {
                    transform.GetChild(3).gameObject.SetActive(false);
                    transform.GetChild(4).gameObject.SetActive(true);
                    transform.GetChild(2).gameObject.SetActive(true);
                    transform.GetChild(1).gameObject.SetActive(false);
                    EventManager.TriggerWindowClicked();
                    isOpen = true;
                }
                else
                {
                    if (!notDone)
                    {
                        StartCoroutine(YLoadScene());
                    }
                    else
                    {
#if UNITY_ANDROID
                        Application.OpenURL("market://details?id=com.merogames.christmas");
#elif UNITY_IPHONE
                        Application.OpenURL("itms-apps://itunes.apple.com/app/id1317796973?mt=8");
#endif
                    }
                }
            }
            else
            {
                transform.GetChild(3).gameObject.SetActive(false);
                transform.GetChild(4).gameObject.SetActive(true);
                transform.GetChild(2).gameObject.SetActive(false);
                transform.GetChild(1).gameObject.SetActive(true);
                if (!isOpen)
                {
                    EventManager.TriggerWindowClicked();
                }
                isOpen = true;
            }
        }
    }

    private void HandleWindowClicked()
    {
        if (isOpen)
        {
            isOpen = false;
            transform.GetChild(3).gameObject.SetActive(true);
            transform.GetChild(4).gameObject.SetActive(false);
            transform.GetChild(2).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    private bool CanLoad()
    {
        if (DateTime.UtcNow.DayOfYear >= NavigationManager.Instance.yearNumberStart + sceneToLoad || DateTime.UtcNow.Month < 11)
        {
            return true;
        }
        return false;
    }

    private IEnumerator YLoadScene()
    {
        transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sortingOrder = 100;
        transform.GetChild(0).gameObject.transform.SetParent(transform.root.parent);


        iTween.MoveTo(bg, Vector3.zero, .6f);
        iTween.ScaleTo(bg, new Vector3(.93f, .95f, 1f), .6f);
        yield return new WaitForSeconds(.6f);
        NavigationManager.Instance.LoadScene(sceneToLoad);
    }
}