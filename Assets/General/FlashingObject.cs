/*
 * Developer:  Barabas Tamas
 */

using System.Collections;
using UnityEngine;
using System;

public class FlashingObject : MonoBehaviour
{
    public Color[] _colors = { Color.red, Color.white };

    private Material material;

    public void Awake()
    {
        material = GetComponentInChildren<SpriteRenderer>().material;
    }

    public void StartFlashing(float time, float intervalTime, Func<bool> callbackFunction = null)
    {
        StopCoroutine("Flash");
        StartCoroutine(Flash(time, intervalTime, callbackFunction));
    }

    IEnumerator Flash(float time, float intervalTime, Func<bool> callbackFunction = null)
    {
        float elapsedTime = 0f;
        int index = 0;
        while (elapsedTime < time)
        {
            material.color = _colors[index % 2];

            elapsedTime += Time.deltaTime;
            index++;
            yield return new WaitForSeconds(intervalTime);
        }
        material.color = Color.white;

        if (callbackFunction != null)
            callbackFunction();
    }
}