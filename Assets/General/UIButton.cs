using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class UIButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Action OnClickDown;
    public Action OnClickUp;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (OnClickDown != null && !eventData.dragging)
        {
            OnClickDown();
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (OnClickUp != null && !eventData.dragging)
        {
            OnClickUp();
        }
    }
}