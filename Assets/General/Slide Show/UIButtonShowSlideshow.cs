using UnityEngine;

public class UIButtonShowSlideshow : UIButton
{
    public GameObject slideShow;
    public Transform slidesHolder;

    private void Start()
    {
        base.OnClickDown += HandleMouseDown;
    }

    private void OnDestroy()
    {
        base.OnClickDown -= HandleMouseDown;
    }

    private void HandleMouseDown()
    {
        Instantiate(slideShow, Vector3.zero, slideShow.transform.rotation, slidesHolder);
    }
}