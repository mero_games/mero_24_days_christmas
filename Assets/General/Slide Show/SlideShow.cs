using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideShow : MonoBehaviour
{

    public Sprite[] backgrounds;

    public Slide slide;

    private int bgCounter;
    private int sortOrder;

    private void Start()
    {
        ShuffleArray(backgrounds);
        StartCoroutine(YStartSlideShow());
    }

    private IEnumerator YStartSlideShow()
    {
        float timer = 5f;
        while (true)
        {
            timer += Time.deltaTime;
            if (timer >= 5f)
            {
                timer = 0f;
                Slide tempSlide = Instantiate(slide, Vector3.zero, Quaternion.identity, transform);
                tempSlide.SetSprite(backgrounds[bgCounter], sortOrder);
                bgCounter++;
                sortOrder++;
                if (bgCounter == backgrounds.Length)
                {
                    bgCounter = 0;
                }
            }
            yield return null;
        }
    }

    private void ShuffleArray(Sprite[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            Sprite temp = array[i];
            int randomIndex = Random.Range(i, array.Length);
            array[i] = array[randomIndex];
            array[randomIndex] = temp;
        }
    }
}