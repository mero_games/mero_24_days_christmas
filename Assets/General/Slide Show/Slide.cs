using System.Collections;
using UnityEngine;

public class Slide : MonoBehaviour
{

    public SpriteRenderer mySpriteRenderer;

    private void Start()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        Destroy(gameObject, 10f);
        StartCoroutine(YFadeIn());
    }

    private void OnDestroy()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }

    private void HandleMouseDown()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);
        if (hit)
        {
            Destroy(transform.parent.gameObject);
        }
    }

    private IEnumerator YFadeIn()
    {
        float timer = 0f;
        while (timer <= 3f)
        {
            timer += Time.deltaTime;
            mySpriteRenderer.color = new Color(mySpriteRenderer.color.r, mySpriteRenderer.color.g, mySpriteRenderer.color.b, timer / 3f);
            yield return null;
        }
    }

    public void SetSprite(Sprite sprite, int order)
    {
        mySpriteRenderer.sprite = sprite;
        mySpriteRenderer.sortingOrder = order;
    }
}