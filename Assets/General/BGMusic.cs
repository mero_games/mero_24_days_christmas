using System.Collections;
using UnityEngine;

public class BGMusic : MonoBehaviour
{

    public AudioClip[] backgroundSongs;

    public float fadeTime;

    private int songsCounter;
    private AudioSource audioSource;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        audioSource = GetComponent<AudioSource>();
        ShuffleArray(backgroundSongs);
    }

    private IEnumerator Start()
    {
        yield return null;
        StartCoroutine(YSongPlayer());
    }

    private IEnumerator YSongPlayer()
    {
        audioSource.clip = backgroundSongs[songsCounter];
        StartCoroutine(YFadeIn(audioSource));
        while (true)
        {
            yield return new WaitForSeconds(audioSource.clip.length - 1f);
            StartCoroutine(YFadeOut(audioSource));
            songsCounter++;
            if (songsCounter == backgroundSongs.Length)
            {
                songsCounter = 0;
            }
            yield return new WaitUntil(() => audioSource.volume <= 0f);
            audioSource.clip = backgroundSongs[songsCounter];
            StartCoroutine(YFadeIn(audioSource));
        }
    }

    private void ShuffleArray(AudioClip[] songs)
    {
        for (int i = 0; i < songs.Length; i++)
        {
            AudioClip temp = songs[i];
            int randomIndex = Random.Range(i, songs.Length);
            songs[i] = songs[randomIndex];
            songs[randomIndex] = temp;
        }
    }

    private IEnumerator YFadeIn(AudioSource song)
    {
        song.Play();
        float timer = 0f;
        while (timer <= 1f)
        {
            timer += Time.deltaTime;
            song.volume = timer / 2;
            yield return null;
        }
        song.volume = .5f;
    }

    private IEnumerator YFadeOut(AudioSource song)
    {
        float timer = 1f;
        while (timer >= 0)
        {
            timer -= Time.deltaTime;
            song.volume = timer / 2;
            yield return null;
        }
        //song.Stop();
        song.volume = 0f;
    }
}