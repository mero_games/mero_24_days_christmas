using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonOpenParental : UIButton
{
    public enum Type
    {
        Mathropolis,
        Colorization
    }

    public ParentalControl parental;

    public Type buttonType;

    private void Start()
    {
        base.OnClickDown += Handle_OnClickDown;
    }

    private void OnDestroy()
    {
        base.OnClickDown -= Handle_OnClickDown;
    }

    void Handle_OnClickDown()
    {
        if (buttonType == Type.Colorization)
        {
            parental.gameObject.SetActive(true);
            parental.type = ParentalControl.Type.Colorization;
        }
        if (buttonType == Type.Mathropolis)
        {
            parental.gameObject.SetActive(true);
            parental.type = ParentalControl.Type.Mathropolis;
        }
    }
}