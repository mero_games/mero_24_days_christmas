using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParentalControl : MonoBehaviour
{

    public enum Type
    {
        Mathropolis,
        Colorization
    }

    public Type type;

    public Collider2D buttonLeft;
    public Collider2D buttonMiddle;
    public Collider2D buttonRight;
    public Collider2D xButton;

    public Text equationText;

    public Text[] texts;

    private int number1;
    private int number2;
    private int correctAnswer;
    private int wrongAnswer1;
    private int wrongAnswer2;

    private void Start()
    {
    }

    private void HandleMouseDown()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 1000);
        if (hit)
        {
            int temp = 0;
            if (hit.collider == buttonLeft)
            {
                temp = Int32.Parse(texts[0].text);
                if (temp == correctAnswer)
                {
                    if (type == Type.Mathropolis)
                    {
#if UNITY_ANDROID
                        Application.OpenURL("market://details?id=com.merogames.math.ropolis");
#elif UNITY_IPHONE
                        Application.OpenURL("itms-apps://itunes.apple.com/app/id1269265281?mt=8");
#endif
                    }
                    if (type == Type.Colorization)
                    {
#if UNITY_ANDROID
                        Application.OpenURL("market://details?id=com.merogames.color");
#elif UNITY_IPHONE
                        Application.OpenURL("itms-apps://itunes.apple.com/app/id1305794749?mt=8");
#endif
                    }
                }
                else
                {
                    GenerateNewEquation();
                }
            }
            if (hit.collider == buttonMiddle)
            {
                temp = Int32.Parse(texts[1].text);
                if (temp == correctAnswer)
                {
                    if (type == Type.Mathropolis)
                    {
#if UNITY_ANDROID
                        Application.OpenURL("market://details?id=com.merogames.math.ropolis");
#elif UNITY_IPHONE
                        Application.OpenURL("itms-apps://itunes.apple.com/app/id1269265281?mt=8");
#endif
                    }
                    if (type == Type.Colorization)
                    {
#if UNITY_ANDROID
                        Application.OpenURL("market://details?id=com.merogames.color");
#elif UNITY_IPHONE
                        Application.OpenURL("itms-apps://itunes.apple.com/app/id1305794749?mt=8");
#endif
                    }
                }
                else
                {
                    GenerateNewEquation();
                }
            }
            if (hit.collider == buttonRight)
            {
                temp = Int32.Parse(texts[2].text);
                if (temp == correctAnswer)
                {
                    if (type == Type.Mathropolis)
                    {
#if UNITY_ANDROID
                        Application.OpenURL("market://details?id=com.merogames.math.ropolis");
#elif UNITY_IPHONE
                        Application.OpenURL("itms-apps://itunes.apple.com/app/id1269265281?mt=8");
#endif
                    }
                    if (type == Type.Colorization)
                    {
#if UNITY_ANDROID
                        Application.OpenURL("market://details?id=com.merogames.color");
#elif UNITY_IPHONE
                        Application.OpenURL("itms-apps://itunes.apple.com/app/id1305794749?mt=8");
#endif
                    }
                }
                else
                {
                    GenerateNewEquation();
                }
            }
            if (hit.collider == xButton)
            {
                gameObject.SetActive(false);
            }
        }
    }

    private void OnEnable()
    {
        EventManager.OnMouseDown += HandleMouseDown;
        GenerateNewEquation();
    }

    private void OnDisable()
    {
        EventManager.OnMouseDown -= HandleMouseDown;
    }

    private void GenerateNewEquation()
    {
        number1 = UnityEngine.Random.Range(2, 10);
        number2 = UnityEngine.Random.Range(2, 10);
        correctAnswer = number1 * number2;
        wrongAnswer1 = correctAnswer % 10 + UnityEngine.Random.Range(0, 10) * 10 + UnityEngine.Random.Range(1, 10);
        wrongAnswer2 = correctAnswer / 10 + UnityEngine.Random.Range(0, 10) * 10 + UnityEngine.Random.Range(1, 10);

        equationText.text = number1 + " x " + number2 + " = ?";

        int correctIndex = UnityEngine.Random.Range(0, 3);
        int oneOrTwo = UnityEngine.Random.Range(0, 2);

        bool done = false;

        for (int i = 0; i < 3; i++)
        {
            if (i == correctIndex)
            {
                texts[i].text = correctAnswer + "";
            }
            else if (done)
            {
                if (oneOrTwo == 0)
                {
                    texts[i].text = wrongAnswer1 + "";
                    done = true;
                }
                else
                {
                    texts[i].text = wrongAnswer2 + "";
                    done = true;
                }
            }
            else
            {
                if (oneOrTwo == 0)
                {
                    texts[i].text = wrongAnswer2 + "";
                    done = true;
                }
                else
                {
                    texts[i].text = wrongAnswer1 + "";
                    done = true;
                }
            }
        }
    }
}