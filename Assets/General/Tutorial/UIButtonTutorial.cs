using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIButtonTutorial : UIButton
{

    public string[] tutorialTexts;
    public Text helpText;

    bool canClick;

    float minX = 2.4f;
    float maxX = 18.9f;

    float minXtetris = 8.1f;
    float maxXtetris = 32.5f;

    void Start()
    {
        GetComponentInParent<Canvas>().worldCamera = Camera.main;
        helpText.text = tutorialTexts[SceneManager.GetActiveScene().buildIndex - 2];
        OnClickDown += HandleClickDown;
        StartCoroutine(YCloseTutorial());
    }

    IEnumerator YCloseTutorial()
    {
        yield return new WaitForSeconds(3f);
        if (SceneManager.GetActiveScene().buildIndex - 1 == 16)
        {
            iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(maxXtetris, transform.position.y, transform.position.z),
                                                  "time", .3f,
                                                  "easetype", iTween.EaseType.linear,
                                                  "oncomplete", "Done"));
        }
        else
        {
            iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(maxX, transform.position.y, transform.position.z),
                                                  "time", .3f,
                                                  "easetype", iTween.EaseType.linear,
                                                  "oncomplete", "Done"));
        }
    }

    IEnumerator YOpenTutorial()
    {
        if (SceneManager.GetActiveScene().buildIndex - 1 == 16)
        {
            iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(minXtetris, transform.position.y, transform.position.z),
                                                  "time", .3f,
                                                  "easetype", iTween.EaseType.linear));
        }
        else
        {
            iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(minX, transform.position.y, transform.position.z),
                                                  "time", .3f,
                                                  "easetype", iTween.EaseType.linear));
        }
        yield return new WaitForSeconds(.3f);
        StartCoroutine(YCloseTutorial());
    }

    void Done()
    {
        canClick = true;
        Debug.Log(transform.position);
    }

    void HandleClickDown()
    {
        if (canClick)
        {
            canClick = false;
            StartCoroutine(YOpenTutorial());
        }
    }
}

//2.4
//18.9

//8.1
//32.5