﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySound : MonoBehaviour
{

    private void Start()
    {
        if (!GetComponent<AudioSource>().loop)
        {
            Destroy(gameObject, GetComponent<AudioSource>().clip.length + .2f);
        }
    }

}
