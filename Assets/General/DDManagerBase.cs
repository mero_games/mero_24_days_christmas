﻿using UnityEngine;

public abstract class DDManagerBase<T> : SingletonBase<T> where T : Component
{

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(this);
    }
}
