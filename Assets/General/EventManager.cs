﻿using System;

public static class EventManager
{
    #region GENERAL
    public static Action OnMouseDown;
    public static Action OnMouseUp;
    public static Action OnWindowClicked;
    #endregion

    #region TRIGGERS GENERAL
    public static void TriggerMouseDown()
    {
        if (OnMouseDown != null)
        {
            OnMouseDown();
        }
    }

    public static void TriggerMouseUp()
    {
        if (OnMouseUp != null)
        {
            OnMouseUp();
        }
    }

    public static void TriggerWindowClicked()
    {
        if (OnWindowClicked != null)
        {
            OnWindowClicked();
        }
    }
    #endregion
}
