using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AnimateHouse : MonoBehaviour
{
    private IEnumerator Start()
    {
        if (DateTime.UtcNow.DayOfYear <= NavigationManager.Instance.yearNumberStart + 14)
        {
            yield return new WaitForSeconds(.2f);
            Debug.Log("ASD");
            GetComponentInParent<ScrollRect>().enabled = false;
            iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(0f, 5f, 0f),
                                                 "time", 3f,
                                                 "easetype", iTween.EaseType.linear));
            yield return new WaitForSeconds(3f);

            GetComponentInParent<ScrollRect>().enabled = true;
        }
    }
}