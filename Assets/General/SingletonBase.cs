using System;
using UnityEngine;

public abstract class SingletonBase<T> : MonoBehaviour where T : Component
{
    private static T instance;

    public Camera MainCamera { get; set; }

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<T>();
            }

            return instance;
        }
    }

    protected virtual void Awake()
    {
        if (Camera.main)
        {
            MainCamera = Camera.main;
        }
        if (instance == null)
        {
            instance = Convert.ChangeType(this, typeof(T)) as T;
        }
    }

    public virtual void OnDestroy()
    {
        instance = null;
    }

}