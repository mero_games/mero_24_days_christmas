﻿using System.Collections;
using UnityEngine;

public class DestroyParticle : MonoBehaviour
{

    private void Start()
    {
        StartCoroutine(YDestroy());
    }

    private IEnumerator YDestroy()
    {
        bool alive = GetComponent<ParticleSystem>().IsAlive();
        yield return new WaitUntil(() => !alive);
        Destroy(gameObject);
    }

}
