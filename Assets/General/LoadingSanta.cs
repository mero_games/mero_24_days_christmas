using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingSanta : MonoBehaviour
{
    private void Start()
    {
        Vector3 newPos = new Vector3(15f, transform.position.y, transform.position.z);

        iTween.MoveTo(gameObject, iTween.Hash("position", newPos,
                                             "time", 10f,
                                             "easetype", iTween.EaseType.linear));
    }
}