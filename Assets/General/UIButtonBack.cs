﻿using UnityEngine.SceneManagement;

public class UIButtonBack : UIButton
{

    // Use this for initialization
    void Start()
    {
        OnClickDown += HandleClickDown;
    }

    private void OnDestroy()
    {
        OnClickDown -= HandleClickDown;
    }

    void HandleClickDown()
    {
        SceneManager.LoadSceneAsync(NavigationManager.Instance.mainInterfaceBuildIndex);
    }
}
