using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigationManager : DDManagerBase<NavigationManager>
{
    public int mainInterfaceBuildIndex;
    public int yearNumberStart;

    public GameObject endScreen;

    protected override void Awake()
    {
        base.Awake();
        Application.targetFrameRate = 60;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    private void Start()
    {
        //LoadScene(mainInterfaceBuildIndex);
        SceneManager.LoadSceneAsync(mainInterfaceBuildIndex);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && SceneManager.GetActiveScene().buildIndex != mainInterfaceBuildIndex)
        {
            LoadScene(mainInterfaceBuildIndex);
            return;
        }
#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape) && SceneManager.GetActiveScene().buildIndex == mainInterfaceBuildIndex)
        {
            Application.Quit();
        }
#endif
    }

    public void LoadScene(int index)
    {
        if (index == mainInterfaceBuildIndex)
        {
            SceneManager.LoadSceneAsync(index);
            return;
        }
        SceneManager.LoadScene(index + 1);
    }

    public void InstantiateEndScreen(Transform trans)
    {
        StartCoroutine(YInstantiateEndScreen(trans));
    }

    private IEnumerator YInstantiateEndScreen(Transform trans)
    {
        yield return new WaitForSeconds(.5f);
        Instantiate(endScreen, Vector3.zero, endScreen.transform.rotation, trans);
    }
}