﻿using UnityEngine;

public class InputManager : DDManagerBase<InputManager>
{

    protected override void Awake()
    {
        base.Awake();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            EventManager.TriggerMouseDown();
        }

        if (Input.GetMouseButtonUp(0))
        {
            EventManager.TriggerMouseUp();
        }
    }


}
